﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;

namespace LoggerManagerProject
{



    public static class XFiles
    {
        public static XDocument Read(string filePathFull)
        {
            //string result = Files.Read(filePathFull);
            try
            {
                return XDocument.Load(filePathFull);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            
        }
    
        #region Save Example
        /*
            // create an XML file using the structure described above
            XDocument xdoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XComment("List of Persons"),
                new XElement("Persons",
                    from xlist in CreateList()
                    select new XElement("Person",
                        new XElement("PersonID", xlist.PersonID),
                        new XElement("Name", xlist.Name),
                        new XElement("Phone", xlist.Phone),
                        new XElement("Street", xlist.Street),
                        new XElement("City", xlist.City),
                        new XElement("State", xlist.State),
                        new XElement("Zipcode", xlist.Zipcode))
                    )
                );

            // save the document
            xdoc.Save(@"c:temp2sample.xml");
        }         
         
         */
        #endregion
    }
        
    
    
    public static class Files
    {
        public static void CreateDir(string name, string path)
        {
            // Specify the directory you want to manipulate.
            //string path = @"c:\MyDir";
            path += string.IsNullOrEmpty(name) ? "" : "\\" + name;
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(path))
                {
                    Console.WriteLine("That path exists already.");
                    return;
                }

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path);
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path));

                // Delete the directory.
                //di.Delete();
                //Console.WriteLine("The directory was deleted successfully.");
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
            finally { }
        }
        public static string Read(string filePathFull)
        {
            /*
             * string text = System.IO.File.ReadAllText(@"C:\Users\Public\TestFolder\WriteText.txt");
             * string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Public\TestFolder\WriteLines2.txt");
             foreach (string line in lines)
             {
                // Use a tab to indent each line of the file.
                Console.WriteLine("\t" + line);
             }
             */
            using (FileStream stream = File.Open(filePathFull, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                string result = null;
                using (StreamReader reader = new StreamReader(stream))
                {
                    string file = reader.ReadToEnd();

                    result = file;
                    /*while (!reader.EndOfStream)
                    {

                    }*/
                }
                return result;
            }
        }
        public static void Write(string fileName, object text, bool isAppend)
        {

        }

        /// <summary>
        /// Method to Perform Xcopy to copy files/folders from Source machine to Target Machine
        /// </summary>
        /// <param name="SolutionDirectory"></param>
        /// <param name="TargetDirectory"></param>
        public static void ProcessXcopy(string SolutionDirectory, string TargetDirectory)
        {
            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            //Give the name as Xcopy
            startInfo.FileName = "xcopy";
            //make the window Hidden
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //Send the Source and destination as Arguments to the process
            startInfo.Arguments = "\"" + SolutionDirectory + "\"" + " " + "\"" + TargetDirectory + "\"" + @" /e /y /I";
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }

    }
}
