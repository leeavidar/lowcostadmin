﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LoggerManagerProject
{
    public interface IExListable
    {
        string Name { get; set; }
        Exception Ex { get; set; }
        DateTime Date { get; set; }
        string FileName { get; }
        void WriteToLogFile(string path);
        string ToStringName();
    }
    public abstract class AbsExList : IExListable
    {

        public Exception Ex { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string FileName { get { return string.Format("{1}_CruiseIt_Log_{0}.txt", GetType(), Name); } }
        public virtual void WriteToLogFile(string path)
        {
            if (this.Ex == null) return;
            string strFileName = string.Format("{1}{0}{2}", Path.DirectorySeparatorChar, path, FileName);
            try
            {
                using (StreamWriter writer = new StreamWriter(strFileName, true, Encoding.UTF8))
                {
                    writer.Write(this.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR Write to logFile [{1}] failed - WriteToLog: {0}", ex.Message, FileName);
            }
           
            //File.AppendAllText(string.Format("{1}{0}{2}", Path.DirectorySeparatorChar, path, FileName), this.ToString(), Encoding.UTF8);
        }
        public AbsExList(string name = "Default")
        {
            Name = name;
        }
        public virtual string ToStringName()
        {
            return string.Format(@"{1} - {0}", GetType(), Date.ToString());
        }
        public override string ToString()
        {
            return this.Ex.Message;
        }
    }

    public class Error : AbsExList
    {
        public Error()
            : base()
        {

        }
        public Error(string name)
            : base(name)
        {

        }
        public override string ToString()
        {
            return string.Format("{1} Error- {0}{2}", this.Ex.Message, this.Date, Environment.NewLine);
        }
    }
    public class Warrning : AbsExList
    {
        public Warrning()
            : base()
        {

        }

        public Warrning(string name)
            : base(name)
        {

        }
        public override string ToString()
        {
            return string.Format("{1} Warrning- {0}{2}", this.Ex.Message, this.Date, Environment.NewLine);
        }
    }
    public class Info : AbsExList
    {
        public Info()
            : base()
        {

        }
        public Info(string name)
            : base(name)
        {

        }
        public override string ToString()
        {
            return string.Format("{1} Info - {0}{2}", this.Ex.Message, this.Date, Environment.NewLine);
        }
    }

    public class LoggerFile
    {
        private bool Active_Errors { get; set; }
        private bool Active_Warrnings { get; set; }
        private bool Active_Info { get; set; }

        private string Name { get; set; }
        private string FileName { get { return string.Format("{0}_GeneralLoggerActivity.txt", Name); } }
        private string RootPath { get; set; }
        public List<IExListable> ExList { get; set; }

        public LoggerFile(string name = "Default", bool active_Errors = true, bool active_Warrnings = true, bool active_Info = true, string pathRoot = @"C:\Logger")
        {
            //create logger dic
            Files.CreateDir("", pathRoot);
            ExList = new List<IExListable>();
            RootPath = pathRoot;
            Name = name;
            SetActivate(active_Errors, active_Warrnings, active_Info);
            WriteInfo("Logger Init");
        }

        private void SetActivate(bool active_Errors, bool active_Warrnings, bool active_Info)
        {
            Active_Errors = active_Errors;
            Active_Warrnings = active_Warrnings;
            Active_Info = active_Info;
        }
        private void WriteToLog(string strLog)
        {
            string fullPath = string.Format("{1}{0}{2}", Path.DirectorySeparatorChar, RootPath, FileName);
            try
            {
                Console.WriteLine("Write to log - {0}", strLog);
                string strFileName = string.Format("{1}{0}{2}", Path.DirectorySeparatorChar, fullPath, FileName);
                using (StreamWriter writer = new StreamWriter(fullPath, true, Encoding.UTF8))
                {
                    writer.Write(string.Format("{0}{1}", strLog, Environment.NewLine));
                }
                //File.AppendAllText(fullPath, string.Format("{0}{1}", strLog, Environment.NewLine), Encoding.UTF8);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR Write to logFile [{1}] failed - WriteToLog: {0}", ex.Message, FileName, fullPath);
            }
        }

        private void Write(IExListable log)
        {
            log.Date = DateTime.Now;
            ExList.Add(log);
            WriteToLog(log.ToStringName());
            log.WriteToLogFile(RootPath);

        }

        private void WriteError(Exception ex)
        {
            Error err = new Error(Name);
            err.Ex = ex;
            if (Active_Errors)
            {
                Write(err);
            }
        }
        private void WriteWarrning(Exception ex)
        {
            Warrning warrning = new Warrning(Name);
            warrning.Ex = ex;
            if (Active_Warrnings)
            {
                Write(warrning);
            }
        }

        private void WriteInfo(string strInfo)
        {
            Info info = new Info(Name);
            info.Ex = new Exception(strInfo);
            if (Active_Info)
            {
                Write(info);
            }
        }

        public void WriteError(string str, Exception ex)
        {
            WriteError(new Exception(string.Format("{0} - Msg: {1}", str, ex.Message)));
        }
        public void WriteError(string format, params object[] strs)
        {
            WriteError(new Exception(string.Format(format, strs)));
        }

        public void WriteWarrning(string str)
        {
            WriteWarrning(new Exception(string.Format("{0}", str)));
        }
        public void WriteWarrning(string format, params object[] strs)
        {
            WriteWarrning(new Exception(string.Format(format, strs)));
        }

        public void WriteInfo(string format, params object[] strs)
        {
            WriteInfo(string.Format(format, strs));
        }

    }


}
