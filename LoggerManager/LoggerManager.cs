﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerManagerProject
{

    public static class LoggerManager
    {
        #region Loggers
        // You Can Add More Then One Logger (LoggerFile) To WebSite

        public static LoggerFile LoggerGeneric { get; set; }

        public static LoggerFile Logger { get; set; }
        public static bool Active_Errors { get; set; }
        public static bool Active_Warrnings { get; set; }
        public static bool Active_Info { get; set; }

        #endregion

        static LoggerManager()
        {
            //TODO: Make Logger Active from WebConfig (Error, Wornings, Info)
            Logger = new LoggerFile("Logger", true, true, true, @"C:\Logger\");
            LoggerGeneric = new LoggerFile("LoggerGeneric", true, true, true, @"C:\Logger\LoggerGeneric\");
        }
    }

}
