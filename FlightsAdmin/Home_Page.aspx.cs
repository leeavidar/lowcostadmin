﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using FlightsAdmin.MasterPage;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class Home_Page : BasePage_UI
    {
        #region Session Private fields

        private HomePage _oSessionHomePage;
        private DestinationPageContainer _oSessionDeestinationPageContainer;
        private TipContainer _oSessionTipContainer;
        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions (or from the database if its the first load of the page or the session is empty)
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || SessionManager.GetSession<HomePage>(out _oSessionHomePage) == false)
            {
                _oSessionHomePage = GetHomePageFromDB();
            }
            else
            {
                // The destinationPage should be in the session if it exist in the database. 
            }
            if (!isPostBack || SessionManager.GetSession<DestinationPageContainer>(out _oSessionDeestinationPageContainer) == false)
            {
                _oSessionDeestinationPageContainer = GetDestinationPagesFromDB();
            }

            if (!isPostBack || SessionManager.GetSession<TipContainer>(out _oSessionTipContainer) == false)
            {
                _oSessionTipContainer = GetTipsFromDB();
            }

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<HomePage>(oSessionHomePage);
            Generic.SessionManager.SetSession<DestinationPageContainer>(oSessionDestinationPages);
            Generic.SessionManager.SetSession<TipContainer>(oSessionTipContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<HomePage>(oSessionHomePage);
            Generic.SessionManager.ClearSession<DestinationPageContainer>(oSessionDestinationPages);
            Generic.SessionManager.ClearSession<TipContainer>(oSessionTipContainer);
        }

        /// <summary>
        /// A method that gets the destination page with a specified docId from the database.
        /// If there is no destination page with this ID, the result will be null.
        /// </summary>
        /// <returns>The Destination page with the specified DocId</returns>
        private HomePage GetHomePageFromDB()
        {
            //selecting the destination page with the matching id from the database
            HomePageContainer oHomePageContainer = HomePageContainer.SelectAllHomePages(1, null);  //TODO: replace with "WhiteLabelId"
            if (oHomePageContainer != null && oHomePageContainer.Count > 0)
            {
                return oHomePageContainer.Single;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// A method that gets all the destination pages from the database.
        /// </summary>
        /// <returns>The Destination page with the specified DocId</returns>
        private DestinationPageContainer GetDestinationPagesFromDB()
        {
            //selecting the destination page with the matching id from the database
            DestinationPageContainer oDestinationPageContainer = DestinationPageContainer.SelectAllDestinationPages(1, null);  //TODO: replace with "WhiteLabelId"
            if (oDestinationPageContainer != null && oDestinationPageContainer.Count > 0)
            {
                return oDestinationPageContainer;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// A method that gets all the tips from the database.
        /// </summary>
        /// <returns>All the Tips from the database</returns>
        private TipContainer GetTipsFromDB()
        {
            //selecting the destination page with the matching id from the database
            TipContainer oTips = TipContainer.SelectAllTips(1, true);  //TODO: replace with "WhiteLabelId"
            if (oTips != null && oTips.Count > 0)
            {
                return oTips;
            }
            else
            {
                return null;
            }
        }


        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        protected new void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            CallPageScript();
        }
        #endregion

    }

    public partial class Home_Page : BasePage_UI
    {

        #region Properties
        public int WhiteLabelId { get; set; }
        #region Private
        #endregion
        #region Public

        public HomePage oSessionHomePage { get { return _oSessionHomePage; } set { _oSessionHomePage = value; } }
        public DestinationPageContainer oSessionDestinationPages { get { return _oSessionDeestinationPageContainer; } set { _oSessionDeestinationPageContainer = value; } }
        public TipContainer oSessionTipContainer { get { return _oSessionTipContainer; } set { _oSessionTipContainer = value; } }

        #endregion
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            SetPage();
        }

        private void SetPage()
        {


            BindDataToRepeater();
            SetSeoPanel();
            if (!IsPostBack)
            {
                uc_ImageUploader.SetImageUploader("", "", oSessionHomePage.DocId_Value, EnumHandler.RelatedObjects.HomePage, EnumHandler.ImageTypes.Main);
                // if there is no home page object in the database, create a new one
                CreateEmptyHomePage();
                
                FillDestinationDdl();
               
                FillMainPanel();
            }
        }

        private void FillTipsDDL()
        {
            ddl_TipOfTheDay.DataSource = oSessionTipContainer.DataSource;
            ddl_TipOfTheDay.DataValueField = "DocId_UI";
            ddl_TipOfTheDay.DataTextField = "Title_UI";
            ddl_TipOfTheDay.DataBind();
            ddl_TipOfTheDay.Items.Insert(0, new ListItem() { Text = "Choose the tip of the day", Value = "" });
        }


        #region Event Handlers

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            // oSessionHomePage shoule never be null (it is initialized in the pageLoad if it is null) 
            if (oSessionHomePage != null)
            {
                oSessionHomePage.TextLine1 = txt_line1.Text;
                oSessionHomePage.TextLine2 = txt_line2.Text;
                oSessionHomePage.TextLine3 = txt_line3.Text;
                oSessionHomePage.TextLine4 = txt_line4.Text;
                oSessionHomePage.TipDocId = ConvertToValue.ConvertToInt(ddl_TipOfTheDay.SelectedValue);

                oSessionHomePage.Action(DL_Generic.DB_Actions.Update);
                uc_ImageUploader.SetImageUploader(txt_ImageTitle.Text, txt_ImageAlt.Text, oSessionHomePage.DocId_Value, EnumHandler.RelatedObjects.HomePage, EnumHandler.ImageTypes.Main);
                uc_ImageUploader.SaveImage();

                oSessionHomePage = GetHomePageFromDB();
                AddScript("SavedAlert();");

            }
        }


        protected void btn_AddSelectedDestination_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }
            // generating a number for the order (last number ibn the order + 1)
            int order = GetMaxOrder() + 1;

            // Creating a new HomePageDestinationPage object and filling it's parameters
            HomePageDestinationPage oHomePageDestinationPage = new HomePageDestinationPage();

            oHomePageDestinationPage.DestinationPageDocId = ConvertToValue.ConvertToInt(ddl_DestinationPages.SelectedValue);
            oHomePageDestinationPage.HomePageDocId = oSessionHomePage.DocId_Value;
            oHomePageDestinationPage.Order = order;

            oHomePageDestinationPage.Action(DB_Actions.Insert);

            // Remove the selected destination page from the ddl
            ddl_DestinationPages.Items.Remove(ddl_DestinationPages.Items.FindByValue(oHomePageDestinationPage.DestinationPageDocId_UI));

            BindDataToRepeater();
            Response.Redirect(Request.RawUrl);
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int HomePageDestinationPageDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(HomePageDestinationPageDocId))
            {
                HomePageDestinationPageContainer oHomePageDestinationPageContainer = oSessionHomePage.HomePageDestinationPages.SelectByID(HomePageDestinationPageDocId, 1); // TODO: replace 1 with whitelable
                if (oHomePageDestinationPageContainer != null)
                {
                    HomePageDestinationPage oHomePageDestinationPage = oHomePageDestinationPageContainer.Single;
                    if (oHomePageDestinationPage != null)
                    {
                        oHomePageDestinationPage.Action(DB_Actions.Delete);
                        oSessionHomePage = GetHomePageFromDB();
                        BindDataToRepeater();
                        // Display a "Deleted successfully" alert
                        AddScript("DeletedAlert();");
                    }
                }
            }

            // Add the deleted item back to the drop down list
            FillDestinationDdl();
        }



        protected void btn_Move_Click(object sender, EventArgs e)
        {
            LinkButton button = (LinkButton)sender;
            HomePageDestinationPage oHomePageDestinationPage = oSessionHomePage.HomePageDestinationPages.FindFirstOrDefault(h => h.DocId_UI == button.CommandArgument);

            bool SwitchWasMade = false;
            int tempOrder;
            int orderOfClickedItem;
            int maxOrder = GetMaxOrder();
            HomePageDestinationPage DestinationToSwitchWith = null;

            switch (button.CommandName)
            {
                case "MoveUp":
                    // If the order of the clicked item is already 1, it can't be moved up
                    if (oHomePageDestinationPage.Order_Value == 1)
                    {
                        return;
                    }
                    // Get the order of the item that was clicked
                    orderOfClickedItem = oHomePageDestinationPage.Order_Value;
                    // find the order number of the previous item (could be -1 or less if there is a gap)
                    int orderOfUpperItem = orderOfClickedItem - 1;
                    do
                    {
                        DestinationToSwitchWith = oSessionHomePage.HomePageDestinationPages.FindFirstOrDefault(h => h.Order_Value == orderOfUpperItem);
                        orderOfUpperItem--;
                    } while (DestinationToSwitchWith == null && orderOfUpperItem != 0);
                    // If there is a destination to switch with (upper (in the list) than the clicked item)
                    if (DestinationToSwitchWith != null)
                    {
                        // --- SWITCH THE ITEMS: ---
                        tempOrder = DestinationToSwitchWith.Order_Value;
                        DestinationToSwitchWith.Order = orderOfClickedItem;
                        oHomePageDestinationPage.Order = tempOrder;
                        SwitchWasMade = true;
                    }
                    break;

                case "MoveDown":
                    // get the order of the item that was clicked
                    orderOfClickedItem = oHomePageDestinationPage.Order_Value;
                    // find the order number of the previous item (could be -1 or less if there is a gap)
                    int orderOfLowerItem = orderOfClickedItem + 1;
                    do
                    {
                        DestinationToSwitchWith = oSessionHomePage.HomePageDestinationPages.FindFirstOrDefault(h => h.Order_Value == orderOfLowerItem);
                        orderOfLowerItem++;
                    } while (DestinationToSwitchWith == null && orderOfLowerItem <= maxOrder + 1);
                    // If there is a destination to switch with (lower (in the list) than the clicked item)
                    if (DestinationToSwitchWith != null)
                    {
                        // --- SWITCH THE ITEMS: ---
                        tempOrder = DestinationToSwitchWith.Order_Value;
                        DestinationToSwitchWith.Order = orderOfClickedItem;
                        oHomePageDestinationPage.Order = tempOrder;
                        SwitchWasMade = true;
                    }
                    break;
            }
            // update the orders of the 2 switched items (if a switch was made)
            if (SwitchWasMade)
            {
                oHomePageDestinationPage.Action(DB_Actions.Update);
                DestinationToSwitchWith.Action(DB_Actions.Update);
                BindDataToRepeater();

                // call the javascript method that hides the first up arrow and last down arrow (on the order of the destinations)
                AddScript("SetOrderArrows()");

                // updating the update panel is automatic.
            }
        }

        /// <summary>
        /// The purpose of this method is only to make assign the up and down keys (changing the order) to the script manager as async- postback controls.
        /// </summary>
        protected void itemsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // if the current iteration's item is an item (not the header or the footer)
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // Assign both the "moveUp" and "moveDown" buttons to the ScriptManager loated in the masterPage:
                // The first part ( ScriptManager.GetCurrent(this.Page) ) gets the script manager of the current page.
                ScriptManager.GetCurrent(this.Page).RegisterAsyncPostBackControl((e.Item.FindControl("btn_MoveUp") as LinkButton));
                ScriptManager.GetCurrent(this.Page).RegisterAsyncPostBackControl((e.Item.FindControl("btn_MoveDown") as LinkButton));
            }
        }

        #endregion


        #region Helping methods

        /// <summary>
        /// A method that gets the data from the datavase and binds it to the repeater.
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_DestinationPages.DataSource = oSessionHomePage.HomePageDestinationPages.DataSource.OrderBy(h => h.Order_Value);
            drp_DestinationPages.DataBind();
        }

        /// <summary>
        /// Setting the main panel with the data from the database.
        /// </summary>
        private void FillMainPanel()
        {
            oSessionHomePage = GetHomePageFromDB();

            txt_line1.Text = oSessionHomePage.TextLine1_UI;
            txt_line2.Text = oSessionHomePage.TextLine2_UI;
            txt_line3.Text = oSessionHomePage.TextLine3_UI;
            txt_line4.Text = oSessionHomePage.TextLine4_UI;

            txt_ImageTitle.Text = oSessionHomePage.Image.Title_UI;
            txt_ImageAlt.Text = oSessionHomePage.Image.Alt_UI;

            FillTipsDDL();

            ddl_TipOfTheDay.SelectedValue = oSessionHomePage.TipDocId_UI;
        }

        /// <summary>
        /// A method that finds the maximum order number in the repaetaer.
        /// </summary>
        /// <returns></returns>
        protected int GetMaxOrder()
        {
            // Geting the order of all the DestinationPageHomePages that are already selected.
            int[] selected = new int[oSessionHomePage.HomePageDestinationPages.Count];
            for (int i = 0; i < selected.Length; i++)
            {
                // Adding the destinationPageHomepage orderoHomePageDestinationPage order to the array
                selected[i] = oSessionHomePage.HomePageDestinationPages[i].Order_Value;
            }
            if (selected.Count() == 0 || selected.Max() < 1)
            {
                return 0;
            }
            return selected.Max();
        }

        /// <summary>
        /// A method that fill the drop down list of all destination pages with destinatioPage table from the DB.
        /// </summary>
        protected void FillDestinationDdl()
        {
            // Geting the DocIds of all the DestinationPages that are already selected.
            int[] selected = new int[oSessionHomePage.HomePageDestinationPages.Count];
            for (int i = 0; i < selected.Length; i++)
            {
                // Adding the destination page doc id to the array
                selected[i] = oSessionHomePage.HomePageDestinationPages[i].DestinationPageDocId_Value;
            }

            if (oSessionDestinationPages != null)
            {
                // adding only the not selected destinations to the list
                ddl_DestinationPages.DataSource = oSessionDestinationPages.DataSource.FindAll(d => !selected.Contains(d.DocId_Value));
                ddl_DestinationPages.DataValueField = "DocId";
                ddl_DestinationPages.DataTextField = "Name";
                ddl_DestinationPages.DataBind();
                ddl_DestinationPages.Items.Insert(0, new ListItem("Select a destination", "-1"));
            }
        }

        /// <summary>
        /// A method that returns the name of the destination page, or a message if there is no name.
        /// </summary>
        /// <param name="oHomePageDestinationPage">The HomePageDestinationPage object to get the name of</param>
        /// <returns>The name of the page, or a message if there is no name.</returns>
        public string GetDestinationPageName(HomePageDestinationPage oHomePageDestinationPage)
        {
            if (oHomePageDestinationPage.DestinationPageSingle != null)
            {
                return oHomePageDestinationPage.DestinationPageSingle.Name_UI;
            }
            return "The page has no name. Set it in the Destination page edit page.";
        }

        /// <summary>
        /// A method that creates an empty home page in the database.
        /// </summary>
        private void CreateEmptyHomePage()
        {
            if (oSessionHomePage == null)
            {
                HomePage oHomePage = new HomePage();
                oHomePage.Action(DL_Generic.DB_Actions.Insert);
            }
        }

        /// <summary>
        /// Set the seo user control with the seo object if there is one, and with the related home page
        /// </summary>
        private void SetSeoPanel()
        {
            if (oSessionHomePage != null)
            {
                // if there is no seo object, create one
                if (oSessionHomePage.SeoSingle == null)
                {
                    oSessionHomePage.SeoSingle = new Seo();
                    oSessionHomePage.SeoSingle.Action(DB_Actions.Insert);
                    oSessionHomePage.SeoDocId = oSessionHomePage.SeoSingle.DocId_Value;
                    oSessionHomePage.Action(DB_Actions.Update);
                }
                // send the seo to the seo user control for editing
                uc_Seo.oSeo = oSessionHomePage.SeoSingle;
            }

        }

        #endregion
    }
}