﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class OrderDetails : BasePage_UI
    {

        #region Session Private fields
        private AirlineContainer _oSessionAirlineContainer;
        private Orders _oSessionOrder;
        // private BL_LowCost.AirportContainer _oSessionAirports; // AirportContainer is also in the services
        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions (or from the database if its the first load of the page or the session is empty)
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || SessionManager.GetSession<Orders>(out _oSessionOrder) == false)
            {
                //take the data from the tables in the database
                _oSessionOrder = GetOrderFromDB();

            }
            else { /*Return from session*/}

            if (_oSessionAirlineContainer == null)
            {
                _oSessionAirlineContainer = GetAirlinesFromDB();
            }


        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<Orders>(oSessionOrder);
            // Generic.SessionManager.SetSession<AirlineContainer>(oSessionAirlinesContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<Orders>(oSessionOrder);
            //  Generic.SessionManager.ClearSession<AirlineContainer>(oSessionAirlinesContainer);
        }

        /// <summary>
        /// A method that gets an order from the database, by its order DocId
        /// </summary>
        /// <returns>The order that was found</returns>
        private Orders GetOrderFromDB()
        {
            // Get the order id from the querystring
            int orderDocId = QueryStringManager.QueryStringID(EnumHandler.QueryStrings.ID);

            // if there is no order id in the url, transfer to the order management page (the index)
            if (ConvertToValue.IsEmpty(orderDocId))
            {
                Response.Redirect(StaticStrings.path_OrderManagement);
            }
            //Select the orders with the provided id
            OrdersContainer orders = BL_LowCost.OrdersContainer.SelectByID(orderDocId, 1, true); // TODO: change to white label

            if (orders != null && orders.Count > 0)
            {
                return orders.Single;
            }
            else
            {
                return null;
            }

        }

        private AirlineContainer GetAirlinesFromDB()
        {
            //selecting all the passengers with a given white label
            AirlineContainer airlines = BL_LowCost.AirlineContainer.SelectAllAirlines(null);
            return airlines;
        }



        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion

    }
    public partial class OrderDetails : BasePage_UI
    {
        #region Properties
        public Orders oSessionOrder { get { return _oSessionOrder; } set { _oSessionOrder = value; } }
        public AirlineContainer oSessionAirlinesContainer
        {
            get
            {
                return _oSessionAirlineContainer;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPage();
        }

        private void SetPage()
        {
            FillLabels();
            SetFlightDetails();

        }

        /// <summary>
        /// Fill all the labels with data from the order in the session
        /// </summary>
        private void FillLabels()
        {
            lbl_TimeOfBook.Text = ConvertToValue.ConvertToDateTime(oSessionOrder.DateOfBook_UI).ToLongDateString();
            lbl_LowCostPnr.Text = oSessionOrder.LowCostPNR_UI;
            lbl_SupplierPnr.Text = oSessionOrder.SupplierPNR_UI;
            lbl_TotalPrice.Text = string.Format("{0} {1}", oSessionOrder.Currency_UI , oSessionOrder.TotalPrice_UI ) ;
            lbl_Payment.Text = ConvertToValue.IsEmpty(oSessionOrder.PaymentRemarks_Value) ? "" : oSessionOrder.OrderPayment_UI;
            lbl_Status.Text = ConvertToValue.IsEmpty(oSessionOrder.OrderStatus_Value) ? "" : Generic.EnumUtil.ToLangString((EnumHandler.OrderStatus)oSessionOrder.OrderStatus_Value);
            //Payment Details
            lbl_paymentStatus.Text = ConvertToValue.IsEmpty(oSessionOrder.StatusPayment_Value) ? "" : Generic.EnumUtil.ToLangString((EnumHandler.OrderPaymentStatus)oSessionOrder.StatusPayment_Value);
            if (oSessionOrder.StatusPayment_Value == (int)EnumHandler.OrderPaymentStatus.Paid)
            {
                lbl_PaymentToken.Text = ConvertToValue.IsEmpty(oSessionOrder.PaymentToken_Value) ? "" : oSessionOrder.PaymentToken_UI;
                lbl_TransactionNum.Text = ConvertToValue.IsEmpty(oSessionOrder.PaymentTransaction_Value) ? "" : oSessionOrder.PaymentTransaction_UI;

            }
            lbl_Origin.Text = OrderManager.GetFlightOrigin(oSessionOrder);
            lbl_Destination.Text = OrderManager.GetFlightDestination(oSessionOrder);

            lbl_OutwardDate.Text = OrderManager.GetOutwardDate(oSessionOrder);
            lbl_ReturnDate.Text = OrderManager.GetReturnDate(oSessionOrder);
            lbl_FlightNumber.Text = OrderManager.GetFlightNumber(oSessionOrder);

            string title = oSessionOrder.ContactDetails.Title_UI;
            if (title != "")
            { // capitalize the first letter of the title, and add a '.'
                title = string.Format("{0}{1}. ", char.ToUpper(title[0]), title.Substring(1)); 
            }
            lbl_AirlineName.Text = OrderManager.GetAirlineName(oSessionOrder, oSessionAirlinesContainer);
            lbl_FullName.Text = string.Format("{0}{1}", title, oSessionOrder.ContactDetails.FullName_UI);
            lbl_PhoneNumber.Text = OrderManager.GetContactPhoneNumber(oSessionOrder);
            lbl_SecondPhoneNumber.Text = OrderManager.GetContactPhoneNumber(oSessionOrder,true);
            lbl_HomeAddress.Text = OrderManager.GetContactAddress(oSessionOrder);
            lbl_EmailAddress.Text = OrderManager.GetContactEmail(oSessionOrder);
        }

        private void SetFlightDetails()
        {
            Orders oOrder = oSessionOrder;
            int OrderDocId = oOrder.DocId_Value;

            //find the user control (with repeater inside) of the outward and returning flight, and fill their properties:

            // -- For the outward flights:
            uc_FlightLegRepeaterOutward.oOrder = oOrder;
            uc_FlightLegRepeaterOutward.oAirlineContainer = oSessionAirlinesContainer;
            uc_FlightLegRepeaterOutward.Type = EnumHandler.FlightTypes.Outward;
            // -- for the return flight:
            uc_FlightLegRepeaterReturn.oOrder = oOrder;
            uc_FlightLegRepeaterReturn.oAirlineContainer = oSessionAirlinesContainer;
            uc_FlightLegRepeaterReturn.Type = EnumHandler.FlightTypes.Return;

            //  DropDownList ddl_ChooseStatus = (DropDownList)e.Item.FindControl("ddl_ChooseStatus");

            //TODO: after changing the status to int in the database , need to change the conversion to match (now it is not working at all)
            ddl_ChooseStatus.SelectedValue = oOrder.OrderStatus_UI;
        }

        protected void ddl_ChooseStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dropDownList = (DropDownList)sender;
            string SelectedStatus = hf_ChangedStatus.Value; // dropDownList.SelectedValue;
            string orderDocId = dropDownList.DataValueField;

            Orders oOrder = oSessionOrder;
            if (oOrder != null)
            {
                int newStatus = ConvertToValue.ConvertToInt(SelectedStatus);
                if (oOrder.OrderStatus != newStatus)
                {
                    oOrder.OrderStatus = newStatus;

                    oOrder.Action(DB_Actions.Update);

                    // change the label showing the status (automatically updates the update panel around the label)
                    lbl_Status.Text = Generic.EnumUtil.ToLangString((EnumHandler.OrderStatus)newStatus);
                    ddl_ChooseStatus.SelectedValue = newStatus.ToString();

                }

            }

        }

        /// <summary>
        /// Fill the drop down list with all status options (from the OrderStatus enum)
        /// </summary>
        private void FillStatusDDL()
        {

            // Fill the ddl with orders statuses
            List<ListItem> statuses = EnumUtil.GetEnumToListItems<EnumHandler.OrderStatus>(false);
            foreach (ListItem item in statuses)
            {
                ddl_ChooseStatus.Items.Add(item);
            }
        }
    }
}