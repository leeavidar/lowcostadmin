﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="True" CodeBehind="CompanyPageEdit.aspx.cs" Inherits="FlightsAdmin.CompanyPageEdit" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<%@ Register Src="Controls/ImageUploader.ascx" TagName="ImageUploader" TagPrefix="uc1" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link href="assets/css/ExtraStyling.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hf_DescriptionHTML" runat="server" />
    <!-- START - COMPANY PANEL -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-edit"></i>Company Page
                        <asp:Label ID="lbl_PageTitle" runat="server" Text="New Company Page" />
                    </div>
                    <div class="tools">
                        <!-- The buttons for closing and minimizing the section -->
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="control-label col-md-3">Company Name</label>
                                        <div class="col-md-9 ">
                                            <asp:TextBox ID="txt_Name" class="form-control input-medium" runat="server" />
                                            <span class="help-block ">
                                                <asp:RequiredFieldValidator ID="val_requiredCompanyName" runat="server" ErrorMessage="A name is required." ForeColor="Red" ControlToValidate="txt_Name" ValidationGroup="check_Company"></asp:RequiredFieldValidator>
                                            </span>
                                        </div>
                                        <label class="control-label col-md-3">Related Airline</label>
                                        <div class="col-md-9 ">
                                            <asp:DropDownList ID="ddl_RelatedCompany" runat="server" CssClass="form-control input-medium select2"></asp:DropDownList>
                                            <span class="help-block ">
                                                The selected airline will make the search automatically filtered to this airline.
                                            </span>
                                        </div>
                                        <label class="control-label col-md-3">Page Title</label>
                                        <div class="col-md-9 ">
                                            <asp:TextBox ID="txt_Title" class="form-control input-medium" runat="server" />
                                            <span class="help-block ">
                                                <asp:RequiredFieldValidator ID="val_requiredTitle" runat="server" ErrorMessage="A title is required." ForeColor="Red" ControlToValidate="txt_Title" ValidationGroup="check_Company"></asp:RequiredFieldValidator>
                                            </span>
                                        </div>
                                        <label class="control-label col-md-3">Company Description</label>
                                        <div class="col-md-9">
                                            <textarea id="txt_Text" name="content" runat="server" rows="10" data-width="400" class="txt_Text form-control"></textarea>
                                            <span class="help-block">Write a short description of the company
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="portlet-body form">
                                        <!-- BEGIN COVER UPLOAD-->
                                        <div class="form-horizontal form-bordered">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">
                                                        Company Logo
                                                                <br />
                                                        220 X 150
                                                    </label>
                                                    <div class="col-md-9">
                                                        <!--START - FILE UPLOADER-->
                                                        <uc1:ImageUploader ID="uc_ImageUploader" runat="server" />
                                                        <!--END - FILE UPLOADER-->
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image title: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_CoverTitle" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image Alt: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_CoverAlt" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END COVER UPLOAD-->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="portlet-body form">
                                        <!-- BEGIN LOGO UPLOAD-->
                                        <div class="form-horizontal form-bordered">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">
                                                        Main Image
                                                                <br />
                                                        1140 X 670
                                                    </label>
                                                    <div class="col-md-9">
                                                        <!--START - FILE UPLOADER-->
                                                        <uc1:ImageUploader ID="uc_ImageUploaderCover" runat="server" />
                                                        <!--END - FILE UPLOADER-->
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image title: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageTitle" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image Alt: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageAlt" runat="server" />
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- END LOGO UPLOAD-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button class="btn green" ID="btn_SaveCompanyPage" runat="server" Text="Save" ValidationGroup="check_Company" OnClientClick="GetBootstrapTextArea();" OnClick="btn_SaveCompanyPage_Click" />
                                        <asp:Button ID="btn_Cancel" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" formnovalidate />

                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- END - COMPANY PANEL -->

    <!-- START - SEO PANEL -->
    <asp:UpdatePanel ID="up_SeoPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row SeoPanel" runat="server" id="SeoPanel" style="display: none;">
                <div class="col-md-12">

                    <div class="portlet  box grey">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa  fa-edit"></i>Seo Static Page 
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div class="form-horizontal">
                                <div class="form-body">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                    URl 
                                                </label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_FriendlyUrl" CssClass="txt_FriendlyUrl form-control form-control-inline input-medium" runat="server" />
                                                    <span class="help-block">
                                                        <asp:Label ID="lbl_FriendlyUrlStatus" CssClass="lbl_FriendlyUrlStatus" runat="server" Text="Status: " />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SEO Title</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_SeoTitle" CssClass="form-control form-control-inline input-medium " runat="server" />
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SEO Description</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_SeoDescription" CssClass="form-control form-control-inline input-medium " TextMode="MultiLine" runat="server" />
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Meta data</label>
                                                <!-- Key words -->
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_SeoMetaData" CssClass="form-control form-control-inline input-medium " runat="server" />
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-9">
                                                <asp:Button ID="btn_SeoSave" runat="server" CssClass="btn green" Text="Save" OnClick="btn_SeoSave_Click" />
                                                <asp:Button ID="bt_Cancel2" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" formnovalidate />

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- END - SEO PANEL -->

    <!-- START - TEMPLATES PANEL -->
    <asp:UpdatePanel runat="server" ID="up_TemplatesPanel" UpdateMode="Conditional">
        <ContentTemplate>
            <div runat="server" id="templatesPanel" style="display: none;">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-edit"></i>Paragraphs table
                                </div>
                            </div>

                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <asp:Repeater ID="drp_TempaltesTable" runat="server" ItemType="DL_LowCost.Template">
                                        <HeaderTemplate>
                                            <thead>
                                                <tr>
                                                    <th>Order</th>
                                                    <th>Template title</th>
                                                    <th class="hidden-xs">Template Name</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <!-- The order is a priority-->
                                                <td><span id="lbl_Order"><%#Eval("Order_UI")%></span>
                                                </td>
                                                <td><span id="lbl_Title"><%#Eval("Title_UI")%></span>
                                                </td>
                                                <td class="hidden-xs"><span id="lbl_Type"><%#Eval("Type_UI")%></span>
                                                </td>
                                                <td>
                                                    <!--Edit button, that redirects to the edit page with template id in the querystring -->
                                                    <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                                    <!-- Delete button -->
                                                    <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="CheckDelete();" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <!-- Add a new template button, that redirects to the edit page with no querystring -->
                        <asp:Button ID="btn_NewTemplate" runat="server" Text="Add a new paragraph" CssClass="btn green " OnClick="btn_NewTemplate_Click" />
                        <asp:Button ID="btnCancel3" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" formnovalidate />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <script type="text/javascript" src="assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="assets/scripts/form-components.js"></script>
    <script type="text/javascript" src="Scripts/siteJS.js"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>

    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins

            FormComponents.init();
            $('.select2').select2();
        });

        var txt_CKEditor = '<%=txt_Text.ClientID%>';
        CKEDITOR.replace(txt_CKEditor, {
            toolbar: [['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'Bold', 'Italic']]
        });
        CKEDITOR.config.height = '420';

    </script>
</asp:Content>
