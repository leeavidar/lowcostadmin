﻿using BL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DL_LowCost;
using DL_Generic;
using Generic;

namespace FlightsAdmin
{
    public partial class Static_Page_Index : BasePage_UI
    {
        #region Session Private fields

        private StaticPagesContainer _oSessionStaticPages;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<StaticPagesContainer>(out _oSessionStaticPages) == false)
            {
                //take the data from the table in the database
                _oSessionStaticPages = GetStaticPagesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<StaticPagesContainer>(oSessionStaticPagesContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<StaticPagesContainer>(oSessionStaticPagesContainer);
        }

        /// <summary>
        /// A method that gets from the database only the type of pages that is provided in the query string (static or dynamic)
        /// if there is no querystring, the default is dynamic (because dynamic pages can be deleted)
        /// </summary>
        /// <returns>The container of pages from the database.</returns>
        private StaticPagesContainer GetStaticPagesFromDB()
        {
            //selecting all the static pages with a given white label
            EnumHandler.PageType type = (EnumHandler.PageType)ConvertToValue.ConvertToInt(Request.QueryString[EnumHandler.QueryStrings.Type.ToString()]);



            StaticPagesContainer allStaticPages = null;
            switch (type)
            {
                case EnumHandler.PageType.Static:
                    allStaticPages = BL_LowCost.StaticPagesContainer.SelectByKeysView_WhiteLabelDocId_Type(1, (int)EnumHandler.PageType.Static, null); // TODO: replace with "WhiteLabelId"

                    break;
                case EnumHandler.PageType.Dynamic:
                default:
                    allStaticPages = BL_LowCost.StaticPagesContainer.SelectByKeysView_WhiteLabelDocId_Type(1, (int)EnumHandler.PageType.Dynamic, null);  // TODO: replace with "WhiteLabelId"
                    break;
            }
            return allStaticPages;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }
      
        #endregion
    }

    public partial class Static_Page_Index : BasePage_UI
    {
        #region Properties
        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public StaticPagesContainer oSessionStaticPagesContainer { get { return _oSessionStaticPages; } set { _oSessionStaticPages = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                if (GetPageTypeFromQuery() == EnumHandler.PageType.Dynamic)
                {
                    lbl_PageTitle.Text = "Dynamic Pages Index";
                }
                else
                {
                        // entering the page from the "static page" link
                    // remove the "New Page" button (there ishouldl be no option to add static pages (as they are static).
                    div_NewStaticPageButton.Visible = false;
                }

            }

            // Bind the data to the repeater
            drp_StaticPagesData.DataSource = oSessionStaticPagesContainer.DataSource;
            drp_StaticPagesData.DataBind();
        }


        /// <summary>
        /// Getting the name of a white label
        /// </summary>
        /// <param name="whiteLabelId">The white label id</param>
        /// <returns>The name of the white label</returns>
        protected string GetWhiteLabelName(object whiteLabelId)
        {
            int id = ConvertToValue.ConvertToInt(whiteLabelId);
            if (!ConvertToValue.IsEmpty(id))
            {
                if (BL_LowCost.WhiteLabelContainer.SelectByKeysView_DocId(id, true).Single != null)
                {
                    string whiteLabelName = BL_LowCost.WhiteLabelContainer.SelectByKeysView_DocId(id, true).Single.Name_UI;
                    return whiteLabelName;
                }
            }
            else
            {
                //Id is empty or null
            }
            return "Error";
        }

        protected string SetEnableDisableText(object isActive)
        {
            bool active = ConvertToValue.ConvertToBool(isActive);
            if (!ConvertToValue.IsEmpty(active))
            {
                if (active)
                {
                    return "Active";
                }
                else
                {
                    return "Not Active";
                }
            }
            return "Error";
        }


        /// <summary>
        /// Set the color of the label Active (green) or Inactive (red).
        /// </summary>
        /// <param name="isActive">The state of the page</param>
        /// <returns>A CSS string for the colors of the label</returns>
        protected string SetEnableDisableColor(object isActive)
        {
            bool active = ConvertToValue.ConvertToBool(isActive);
            if (!ConvertToValue.IsEmpty(active))
            {
                if (active)
                {
                    return "label label-success label-sm";
                }
                else
                {
                    return "label label-danger label-sm";
                }
            }
            else
            {

            }
            return "Error";
        }

        #region Event Handlers

        protected void btn_ChangeActiveStatus_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string status = btn.CommandArgument;
        }


        /// <summary>
        /// Redirect to the edit static page page, with no page id (to create a new page)
        /// </summary>
        protected void btn_NewStaticPage_Click(object sender, EventArgs e)
        {
            Response.Redirect(StaticStrings.path_StaticPageEditor);
        }

        protected void btn_edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id))
            {
                // Redirecting to the static page edit page, with the id of the page to edit.
                Response.Redirect(StaticStrings.path_StaticPageEditor + "?id=" + id.ToString());
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int staticPageDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(staticPageDocId))
            {
                StaticPagesContainer oStaticPagesContainer = oSessionStaticPagesContainer.SelectByID(staticPageDocId, 1);   // TODO: change the 1 to a real white label
                if (oStaticPagesContainer != null && oStaticPagesContainer.Count > 0)
                {
                    StaticPages oStaticPages = oStaticPagesContainer.Single;
                    if (oStaticPages != null)
                    {
                        oStaticPages.Action(DB_Actions.Delete);

                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }



        protected bool CheckIfStaticEval()
        {
            if ((EnumHandler.PageType)(ConvertToValue.ConvertToInt(Request.QueryString[EnumHandler.QueryStrings.Type.ToString()])) == EnumHandler.PageType.Static)
            {
                return false;
            }
            return true;
        }



        #endregion


        protected EnumHandler.PageType GetPageTypeFromQuery()
        {
            if ((EnumHandler.PageType)(ConvertToValue.ConvertToInt(Request.QueryString[EnumHandler.QueryStrings.Type.ToString()])) == EnumHandler.PageType.Static)
                return EnumHandler.PageType.Static;
            else
                return EnumHandler.PageType.Dynamic;
        }

    }
}