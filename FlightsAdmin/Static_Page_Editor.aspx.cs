﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DL_LowCost;
using BL_LowCost;
using DL_Generic;
using Generic;

namespace FlightsAdmin
{
    public partial class Static_Page_Editor : BasePage_UI
    {
        #region Session Private fields

        private StaticPages _oSessionStaticPage;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions (or from the database if its the first load of the page or the session is empty)
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<StaticPages>(out _oSessionStaticPage) == false)
            {
                //take the data from the table in the database
                _oSessionStaticPage = GetStaticPageFromDB();
            }
            else { /*Return from session*/}

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<StaticPages>(oSessionStaticPage);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<StaticPages>(oSessionStaticPage);
        }

        private StaticPages GetStaticPageFromDB()
        {
            //selecting a static page with a given white label
            return StaticPagesContainer.SelectByID(QueryStringManager.QueryStringID(EnumHandler.QueryStrings.ID), 1, null).Single;   //TODO: replace with "WhiteLabel Id"
        }
        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class Static_Page_Editor : BasePage_UI
    {
        #region Properties
        public int WhiteLabelId { get; set; }
        #region Private
        #endregion
        #region Public
        public StaticPages oSessionStaticPage { get { return _oSessionStaticPage; } set { _oSessionStaticPage = value; } }
        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            // check if we are in an edit mode (if there is an id parameter sent in the querystring) or create a new static page:
            if (ConvertToValue.IsEmpty(Request.QueryString.ToString()))
            {
                //there is no query string parameter for id
            }
            else
            {
                //Change the page title
               if (!IsPostBack)
                    PageInit();
            }

            SetSeoPanel();
        }


        /// <summary>
        /// Initializes the page. 
        /// If the page was navigated to using the edit button in the previous page, and there is a static page id in the query string, change to 'edit' mode.
        /// In other case, stay in create a new page mode (to create a new static page)
        /// </summary>
        private void PageInit()
        {

            //Fill the edit fields with the data from the existing page and change the page title to match edit mode
            txt_PageName.Text = oSessionStaticPage.Name_UI;
            txt_CKEditor.Value = oSessionStaticPage.Text_UI;

            // if the page is a static page (not dynamic) there is no option to make it inactive.
            if (oSessionStaticPage.Type_Value == (int)EnumHandler.PageType.Static)
            {
               // cb_Active.Visible = false;
                txt_PageName.Enabled = false;
            }
            else
            {
                // set the checkbox according to the state of the page (active / inactive)
                if (oSessionStaticPage.IsActive == true)
                    cb_Active.Checked = true;
                else
                    cb_Active.Checked = false;
            }
            // Change the page title to edit (instead of new)
            lbl_PageTitle.Text = string.Format("Edit \"{0}\" Page", oSessionStaticPage.Name_UI);

        }

        /// <summary>
        /// Set the seo user control with the seo object if there is one, and with the related static or dynamic page.
        /// </summary>
        private void SetSeoPanel()
        {
            if (oSessionStaticPage != null)
            {
                // if there is a page, display the seo panel
                seo_Panel.Style.Add("display", "normal");

               // if there is no seo object, create one
                if (oSessionStaticPage.SeoSingle == null)
                {
                    //create a new seo object
                     oSessionStaticPage.SeoSingle = new Seo();
                     oSessionStaticPage.SeoSingle.Action(DB_Actions.Insert);
                    // set the seo doc id (foreign key) to the doc id of the new seo created
                     oSessionStaticPage.SeoDocId = oSessionStaticPage.SeoSingle.DocId_Value;
                    // updated the page (with the new seo doc id)
                     oSessionStaticPage.Action(DB_Actions.Update);
                }
                // send the seo to the seo user control for editing
                uc_Seo.oSeo = oSessionStaticPage.SeoSingle;
            }
           
        }

        #region Event Handlers

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            //if anything enterd is invalid
            if (!IsValid)
                return;

            if (oSessionStaticPage == null)
            {
                // In case the page is in 'New Page' mode:

                //Create a new empty static page
                oSessionStaticPage = new StaticPages();
                //Fill its title and html content
                oSessionStaticPage.Name = txt_PageName.Text;
                oSessionStaticPage.Text = txt_CKEditor.Value;
                oSessionStaticPage.Type = 2; // new pages can only be dynamic pages (not possible to add static pages) and the type is 2.
                // Change the isActive property to false (so that the page will not be automatically active)
                oSessionStaticPage.IsActive = false;
                oSessionStaticPage.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.
                oSessionStaticPage.Action(DL_Generic.DB_Actions.Insert);
            }
            else
            {
                //in case the page is in 'edit' mode:
                oSessionStaticPage.Name = txt_PageName.Text;
                oSessionStaticPage.Text = txt_CKEditor.Value;
                oSessionStaticPage.IsActive = cb_Active.Checked;
                oSessionStaticPage.Action(DL_Generic.DB_Actions.Update);
            }
            // redirect to the same page with static page id
            Response.Redirect(QueryStringManager.RedirectWithQuery(StaticStrings.path_StaticPageEditor, EnumHandler.QueryStrings.ID, oSessionStaticPage.DocId_UI));
        }

        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            if (oSessionStaticPage== null)
            {
                Response.Redirect(QueryStringManager.RedirectWithQuery(StaticStrings.path_StaticPageIndex, EnumHandler.QueryStrings.Type, "2"));
            }
            else
            {
                // in case we are in edit mode, we can be editing a static or a dynamic page, so we will use it's type to know to which page to return
                Response.Redirect(QueryStringManager.RedirectWithQuery(StaticStrings.path_StaticPageIndex, EnumHandler.QueryStrings.Type, oSessionStaticPage.Type_UI));
            }
          
        }


        #endregion

        #region Session Methods

        #endregion

    }
}