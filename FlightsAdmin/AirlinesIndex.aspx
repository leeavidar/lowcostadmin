﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="AirlinesIndex.aspx.cs" Inherits="FlightsAdmin.AirlinesIndex" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">

    <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/pages/portfolio.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />

    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
</asp:Content>
<asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- Begin: Title Row --%>
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Low Cost Flights Airlines"></asp:Label>
            </h3>
        </div>
    </div>
    <%-- End: Title Row --%>

    <%-- Begin: Search Form --%>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-search"></i>Search Airline (type at least 2 characters)
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Search Term: </label>
                                        <div class="col-md-9">
                                            <input type="text" value="" id="txt_SearchTerm" runat="server" class="txt_SearchTerm form-control form-control-inline input-medium">
                                            <span class="help-block">
                                                <span>Type part of the name or IATA code of the airline<br />
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <asp:Button ID="btn_Search" runat="server" Text="Search" CssClass="btn green" OnClick="btn_Search_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- End: Search Form --%>

    <%-- Begin: Results table--%>
    <div class="row">
        <div class="col-md-12">

            <asp:UpdatePanel ID="up_AirlinesResults" runat="server">
                <ContentTemplate>
                    <%-- While searching --%>
                    <div style="text-align: center;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true">
                            <ProgressTemplate>
                                <img src="/assets/img/ajax-loading.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </div>
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-book"></i>Airlines table
                            </div>

                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">

                                <asp:Repeater ID="drp_Airlines" runat="server" ItemType="DL_LowCost.Airline">
                                    <HeaderTemplate>
                                        <thead>
                                            <th>IATA Code</th>
                                            <th>Name</th>
                                            <th>Active Status</th>
                                            <%--<th class="hidden-xs">Date</th>--%>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <!-- The order is a priority-->
                                            <td>
                                                <span id="lbl_Order"><%# Eval("IataCode_UI") %></span>
                                            </td>
                                            <td>
                                                <span id="lbl_EnglishName"><%# Eval("Name_UI") %></span>
                                            </td>
                                            <td>
                                                <span id="lbl_ShortText"><%# Eval("IsActive_UI") %></span>
                                            </td>
                                            <%-- <td class="hidden-xs">
                                        <span id="lbl_Type"><%# Eval("Date_UI") %></span>
                                    </td>--%>
                                            <td>
                                                <!--Edit button  -->
                                                <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                                <!-- Delete button -->
                                                <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>

                            </table>
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btn_Search" EventName="click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <%-- End: Results table--%>


    <%-- Begin: Add new button --%>
    <div class="row">
        <div class="col-md-12 ">
            <!-- Add a new Airline button - The value of this button will change from "Add a new Airline" to "Cancel" and open or close the Airline panel -->
            <input type="button" id="btn_AddNewAirline" class="btn green btn_AddNewAirline" value="Add a new Airline" runat="server" />
        </div>
    </div>
    <%-- End: Add new button --%>

    <%-- Begin:  ADD/EDIT PANEL--%>
    <div id="AirlinePanel" class="row  mt10 AirlinePanel" runat="server" style="display: none;">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-search"></i>Add/Edit Airline
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">IATA Code</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_IataCode" class="form-control input-medium txt_IataCode" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Airline Name</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Name" class="form-control input-medium txt_Name" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Active Status</label>
                                        <div class="col-md-9">
                                            <asp:CheckBox ID="cb_IsActive" CssClass="cb_IsActive" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- Second row -->

                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <!-- This button's command arguments is set in a siteJS.js file  (SaveNew or SaveEdit) -->
                                        <asp:Button class="btn green btn_SaveNew" ID="btn_SaveNew" runat="server" Text="Save" ValidationGroup="check_airline" OnClick="btn_Save_New_Click" />
                                        <asp:Button class="btn green btn_SaveEdit" ID="btn_SaveEdit" runat="server" Text="Save Changes" ValidationGroup="check_airline" OnClick="btn_SaveEdit_Click" Style="display: none" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <%-- End:  ADD/EDIT PANEL--%>
</asp:Content>

<asp:Content ID="scriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/table-advanced.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="assets/scripts/form-components.js"></script>

    <script src="Scripts/siteJS.js" type="text/javascript"></script>
    <script src="Scripts/PageScripts/Airlines.js" type="text/javascript"></script>
    <script>


        function AfterUpdate() {

            TableAdvanced.init();
            FormComponents.init();
        }

        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();
            FormComponents.init();
        });
    </script>
</asp:Content>
