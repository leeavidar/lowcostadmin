﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class CompanyPageEdit : BasePage_UI
    {
        #region Session Private fields

        private CompanyPage _oSessionCompanyPage;
        private AirlineContainer _oSessionAirlineContainer;
        private Images _oSessionImage;
        #endregion

        #region Sessions Methodes
        ///<summary>
        ///load the containers fron the sessions
        ///</summary>
        ///<param name="isPostBack"></param>

        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<CompanyPage>(out _oSessionCompanyPage) == false)
            {
                // take the data from the table in the database
                _oSessionCompanyPage = GetCompanyPageFromDB();
                if (!isPostBack || SessionManager.GetSession<AirlineContainer>(out _oSessionAirlineContainer) == false)
                {
                      _oSessionAirlineContainer = GetAllAirlines();
                }
            }
            else { /*Return from session*/}
        }

        ///<summary>
        ///save the containers to the sessions
        ///</summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<CompanyPage>(oSessionCompanyPage);
        }

        ///<summary>
        ///function to reset all sessions in the page
        ///</summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<CompanyPage>(oSessionCompanyPage);
        }

        /// <summary>
        /// A method that gets a company page from the DB, by a given query string.
        /// </summary>
        /// <returns>The Companypage found in the DB.</returns>
        private CompanyPage GetCompanyPageFromDB()
        {
            //selecting the company pages with the matching id from the database
            CompanyPageContainer oCompanyPageContainer = CompanyPageContainer.SelectByID(QueryStringManager.QueryStringID(EnumHandler.QueryStrings.ID), 1, null); //TODO: replace with "WhiteLabelId"
            if (oCompanyPageContainer != null && oCompanyPageContainer.Count > 0)
            {
                return oCompanyPageContainer.Single;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// A methopd that gets a container of all the companies (for the related company field)
        /// </summary>
        /// <returns></returns>
        private AirlineContainer GetAllAirlines()
        {
            return AirlineContainer.SelectAllAirlines(null);

        }
        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion

    }


    public partial class CompanyPageEdit : BasePage_UI
    {
        #region Properties
        #region Private

        #endregion
        #region Public

        public CompanyPage oSessionCompanyPage { get { return _oSessionCompanyPage; } set { _oSessionCompanyPage = value; } }
        public AirlineContainer oSessionAirlineContainer { get { return _oSessionAirlineContainer; } set { _oSessionAirlineContainer = value; } }


        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            SetPage();
        }

        private void SetPage()
        {
            if (oSessionCompanyPage != null)
            {
                // << EDIT MODE >>
                uc_ImageUploader.SetImageUploader(txt_ImageTitle.Text, txt_ImageAlt.Text, oSessionCompanyPage.DocId_Value, EnumHandler.RelatedObjects.CompanyPage, EnumHandler.ImageTypes.Logo);
                uc_ImageUploaderCover.SetImageUploader(txt_CoverTitle.Text, txt_CoverAlt.Text, oSessionCompanyPage.DocId_Value, EnumHandler.RelatedObjects.CompanyPage, EnumHandler.ImageTypes.Cover);
                if (!IsPostBack)
                {
                    //Fill the edit fields with the existing data
                    FillGeneralPanel();
                    FillSeoPanel();

                    ChangePageTitle();

                    // Un-hiding all the editing panels
                    EditingPanelsAvailable(true);
                }
                //   Bind the data to the table of templates

                BindTemplatesRepeaterToData();
            }
            else
            {
                // << NEW MODE >>
                if (!IsPostBack)
                {
                    SetDDL();
                }
            }

        }

        private void BindTemplatesRepeaterToData()
        {
            oSessionCompanyPage = GetCompanyPageFromDB();
            drp_TempaltesTable.DataSource = oSessionCompanyPage.Templates.DataSource;
            drp_TempaltesTable.DataBind();
        }

        #region Events handlers

        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(StaticStrings.path_CompanyPageIndex);
        }

        protected void btn_SaveCompanyPage_Click(object sender, EventArgs e)
        {
            //If anything enterd is invalid
            if (!IsValid)
                return;

            if (oSessionCompanyPage == null)
            {
                //  << NEW PAGE MODE >>
                // Creating a new Destination page and filling the properties
                oSessionCompanyPage = new CompanyPage()
                {
                    Name = txt_Name.Text,
                    Title = txt_Title.Text,
                    Text =  Server.HtmlDecode(txt_Text.Value), 
                    IsActive = true,
                    IsDeleted = false,
                    WhiteLabelDocId = 1 //TODO: replace the 1 with the real white label when there is white label support.
                };
                int relatedAirlineDocId = ConvertToValue.ConvertToInt(ddl_RelatedCompany.SelectedValue);
                if (!ConvertToValue.IsEmpty(relatedAirlineDocId))
                {
                    oSessionCompanyPage.AirlineDocId = relatedAirlineDocId;
                }
                else
                {
                    oSessionCompanyPage.AirlineDocId = null;
                }

                // Add the destinationPage to the DB.
                int result = oSessionCompanyPage.Action(DL_Generic.DB_Actions.Insert);
                EditingPanelsAvailable(true);
                ChangePageTitle();

                // If the new Company page was added successfully to the database
                if (result != -1)
                {
                    // upload the images and add it to the DB:
                    uc_ImageUploader.SetImageUploader(txt_ImageTitle.Text, txt_ImageAlt.Text, oSessionCompanyPage.DocId_Value, EnumHandler.RelatedObjects.CompanyPage, EnumHandler.ImageTypes.Logo);
                    uc_ImageUploader.SaveImage();

                    uc_ImageUploaderCover.SetImageUploader(txt_CoverTitle.Text, txt_CoverAlt.Text, oSessionCompanyPage.DocId_Value, EnumHandler.RelatedObjects.CompanyPage, EnumHandler.ImageTypes.Cover);
                    uc_ImageUploaderCover.SaveImage();
                }
                // redirect to the same page with the company id in the querystring and edit options available
                Response.Redirect(String.Format("{0}?{1}={2}", StaticStrings.path_CompanyPageEdit, EnumHandler.QueryStrings.ID, oSessionCompanyPage.DocId_Value));

            }
            else
            {
                //  << EDIT MODE >>

                oSessionCompanyPage.Name = txt_Name.Text;
                oSessionCompanyPage.Title = txt_Title.Text;
                oSessionCompanyPage.Text = Server.HtmlDecode(txt_Text.Value);
                int relatedAirlineDocId = ConvertToValue.ConvertToInt(ddl_RelatedCompany.SelectedValue);
                if (!ConvertToValue.IsEmpty(relatedAirlineDocId))
                {
                    oSessionCompanyPage.AirlineDocId = relatedAirlineDocId;
                }
                else
                {
                    oSessionCompanyPage.AirlineDocId = null;
                }
                // Update the destinationPage in the DB.
                oSessionCompanyPage.Action(DL_Generic.DB_Actions.Update);

                // upload the images and add it to the DB:
                uc_ImageUploader.SetImageUploader(txt_ImageTitle.Text, txt_ImageAlt.Text, oSessionCompanyPage.DocId_Value, EnumHandler.RelatedObjects.CompanyPage, EnumHandler.ImageTypes.Logo);
                uc_ImageUploader.SaveImage();

                uc_ImageUploaderCover.SetImageUploader(txt_CoverTitle.Text, txt_CoverAlt.Text, oSessionCompanyPage.DocId_Value, EnumHandler.RelatedObjects.CompanyPage, EnumHandler.ImageTypes.Cover);
                uc_ImageUploaderCover.SaveImage();

                AddScript("SavedAlert();");
                // set statruop script
            }
           
        }


        #region SEO Panel Events


        protected void btn_SeoSave_Click(object sender, EventArgs e)
        {
            Seo oSeo = oSessionCompanyPage.SeoSingle;
          
            if (oSeo != null)
            {
                // first check the friendly url and if it's already in use, return (gives a message inside the function)
                bool friendlyUrlTaken = IsFriendlyUrlTaken(oSeo);
                if (friendlyUrlTaken)
                {
                    return;
                }

                // if there is already a SEO object for this destination page
                oSeo.SeoTitle = txt_SeoTitle.Text;
                oSeo.FriendlyUrl = txt_FriendlyUrl.Text;
                oSeo.SeoDescription = txt_SeoDescription.Text;
                oSeo.SeoKeyWords = txt_SeoMetaData.Text;

                oSeo.WhiteLabelDocId = 1; // TODO: Change to real white label

                // Update the SEO for this page in the DB
                oSeo.Action(DB_Actions.Update);

                // show a success alert
                AddScript("SavedAlert();");
             //   ScriptManager.RegisterStartupScript(up_SeoPanel, up_SeoPanel.GetType(), "MyKey", "SavedAlert();", true);
        
               
            }
            else
            {
                // first check the friendly url and if it's already in use, return (gives a message inside the function)
                bool friendlyUrlTaken = IsFriendlyUrlTaken();
                if (friendlyUrlTaken)
                {
                    return;
                }
             
                // Create a new SEO for this page
                Seo oSeoNew = new Seo()
                {
                    SeoTitle = txt_SeoTitle.Text,
                    FriendlyUrl = txt_FriendlyUrl.Text,
                    SeoDescription = txt_SeoDescription.Text,
                    SeoKeyWords = txt_SeoMetaData.Text,
                    WhiteLabelDocId = 1
                };

                // Insert the new SEO to the DB
                oSeoNew.Action(DB_Actions.Insert);
                // Add the new SEO to the Destination
                oSessionCompanyPage.SeoSingle = oSeoNew;
                oSessionCompanyPage.SeoDocId = oSeoNew.DocId_Value;
                oSessionCompanyPage.Action(DB_Actions.Update); 
            }
        }

        #endregion

        #region Templates Panel events


        protected void btn_NewTemplate_Click(object sender, EventArgs e)
        {
            // Redirecting to the tempate edit page without id in the querystring, to add a new template.
            Response.Redirect(StaticStrings.path_CompanyPageTemplateEdit + "?" + EnumHandler.QueryStrings.CompanyPageId + "=" + oSessionCompanyPage.DocId_UI);
        }




        protected void btn_edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int templateId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(templateId))
            {
                //Redirecting to the edit page with the id of the teplate to edit
                Response.Redirect(StaticStrings.path_CompanyPageTemplateEdit + "?" + EnumHandler.QueryStrings.ID + "=" + templateId.ToString());
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }



        protected void btn_Delete_Click(object sender, EventArgs e)
        {
           //  LoadFromSession(false);
            
             LinkButton rowButton = (LinkButton)sender;
            
             //Getting the id from the command argument of the button
             int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);
            
             if (!ConvertToValue.IsEmpty(id))
             {
                 TemplateContainer oTemplateContainer = oSessionCompanyPage.Templates.SelectByID(id, 1);    // TODO: change the 1 to a real white label
                 if (oTemplateContainer != null && oTemplateContainer.Count > 0)
                 {
                     Template oTemplate = oTemplateContainer.Single;
                     if (oTemplate != null)
                     {
                         //1. first delete the sub boxes if there are any:
                         DeleteRelatedObjects(oTemplate);

                         //2. second, delete the template itself:
                         oTemplate.Action(DB_Actions.Delete);

                         BindTemplatesRepeaterToData();
                         AddScript("DeletedAlert();");
                         up_TemplatesPanel.Update();
                     }
                 }
             }
             else
             {
                 //the id is empty
                 //no id was selected
             }
        }

        #endregion

        #endregion


        #region Helping methods

        /// <summary>
        /// A method that fills the general panel (the first panel) on the page.
        /// </summary>
        private void FillGeneralPanel()
        {
            SetDDL();
            ddl_RelatedCompany.SelectedValue = oSessionCompanyPage.AirlineDocId_UI;
            txt_Name.Text = oSessionCompanyPage.Name_UI;
            txt_Title.Text = oSessionCompanyPage.Title_UI;
            txt_Text.InnerHtml = oSessionCompanyPage.Text_UI;

            Images oImage = oSessionCompanyPage.Imagess.FindFirstOrDefault(img => img.ImageType_Value == EnumHandler.ImageTypes.Logo.ToString());

            if (oImage != null)
            {
                txt_ImageAlt.Text = oImage.Alt_UI;
                txt_ImageTitle.Text = oImage.Title_UI;
            }

            oImage = oSessionCompanyPage.Imagess.FindFirstOrDefault(img => img.ImageType_Value == EnumHandler.ImageTypes.Cover.ToString());
            if (oImage != null)
            {
                txt_CoverAlt.Text = oImage.Alt_UI;
                txt_CoverTitle.Text = oImage.Title_UI;
            }

        }

        /// <summary>
        /// A method that checks if there is a SEO object for the current destinatio page, and fills the seo panel on the page.
        /// </summary>
        private void FillSeoPanel()
        {
            Seo oSeo = oSessionCompanyPage.SeoSingle;
            if (oSeo != null)
            {
                // if there is a SEO object for this destination page
                txt_SeoTitle.Text = oSeo.SeoTitle;
                txt_FriendlyUrl.Text = oSeo.FriendlyUrl;
                txt_SeoDescription.Text = oSeo.SeoDescription;
                txt_SeoMetaData.Text = oSeo.SeoKeyWords;
            }
            else
            {
                // There is no SEO for this destinatio page.
            }
        }

        /// <summary>
        /// A method that changes the title of the page to edit.
        /// </summary>
        private void ChangePageTitle()
        {
            //Change the title of the page
            lbl_PageTitle.Text = string.Format("Edit \"{0}\" Page", oSessionCompanyPage.Name_UI);
        }

        private void EditingPanelsAvailable(bool isAvailable)
        {
            string displayValue = isAvailable ? "normal" : "none";
            SeoPanel.Style.Add("display", displayValue);
            templatesPanel.Style.Add("display", displayValue);
        }

        /// <summary>
        /// A method that verifies if it the entered friendly url is different then the current one, and taken
        /// </summary>
        /// <param name="oSeo">The seo to test on.</param>
        /// <returns>True - the url is taekn (not the same) and can't be used again, or false otherwise.</returns>
        private bool IsFriendlyUrlTaken(Seo oSeo = null)
        {
            if (oSeo == null)
            {
                if (SeoContainer.IsFriendlyUrlTaken(txt_FriendlyUrl.Text, 1)) // TODO: change the 1 to real whilte label
                {
                    // if the firendly url is taken
                    lbl_FriendlyUrlStatus.Text = "Error: Please select another friendly url";
                    return true;
                }
                else
                {
                    return false;
                }
            }
            string currentFriendlyUrl = oSeo.FriendlyUrl;
            if (txt_FriendlyUrl.Text == currentFriendlyUrl)
            {
                // if the new url is the same as the current one, it is allowed.
                oSeo.FriendlyUrl = txt_FriendlyUrl.Text;
                return false;
            }
            else
            {
                if (SeoContainer.IsFriendlyUrlTaken(txt_FriendlyUrl.Text, 1)) // TODO: change the 1 to real whilte label
                {
                    // if the firendly url is taken
                    lbl_FriendlyUrlStatus.Text = "Error: Please select another friendly url";
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// A method that deletes all related objects to a destination page.
        /// </summary>
        /// <param name="oDestinationPage">The template to deleted it's related object.</param>
        private static void DeleteRelatedObjects(Template oTemplate)
        {
            // Delete all related sub boxes 
            if (oTemplate != null)
            {
                for (int i = 0; i < oTemplate.SubBoxs.DataSource.Count; i++)
                {
                    if ( oTemplate.SubBoxs.DataSource[i]!=null)
                    {
                        oTemplate.SubBoxs.DataSource[i].Action(DB_Actions.Delete);
                    }
                }
            }
        }

        private void SetDDL()
        {
            ddl_RelatedCompany.DataSource = oSessionAirlineContainer.DataSource;
            ddl_RelatedCompany.DataTextField = "Name_UI";
            ddl_RelatedCompany.DataValueField = "DocId_UI";
            ddl_RelatedCompany.DataBind();
            ddl_RelatedCompany.Items.Insert(0, new ListItem("Select A Company", ""));
           
        }
        #endregion


    }
}