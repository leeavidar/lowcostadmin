﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightsAdmin
{
    public static class UserControlPathsHolder
    {
        public static string path_FlightGroupUC = @"/Controls/SearchFlights/AllFlightsInGroups/FlightGroupUC.ascx";
        public static string path_FlightGroupOneWayUC = @"/Controls/SearchFlights/AllFlightsInGroups/FlightGroupOneWayUC.ascx";
        public static string path_FlightRowUC = @"/Controls/SearchFlights/AllFlightsInGroups/FlightRowUC.ascx";
        public static string path_FlightSegmentUC = @"/Controls/SegmentFlightUC.ascx";

        public static string path_UCPassengerDetails = @"/Controls/PassengerDetailsUC.ascx";
        public static string path_UCTicketDetails = @"/Controls/TicketDetailsUC.ascx"; 
        
       
    }
}