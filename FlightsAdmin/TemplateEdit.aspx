﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="TemplateEdit.aspx.cs" Inherits="FlightsAdmin.TemplateEdit" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<%@ Register Src="Controls/ImageUploader.ascx" TagName="ImageUploader" TagPrefix="uc1" %>
<%@ Register Src="Controls/Templates/UC_SubBox.ascx" TagName="UC_SubBox" TagPrefix="uc1" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/pages/portfolio.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--Page title -->
    <div class="row">
        <div class="col-md-12">
            <h3>
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Select template to add"></asp:Label>
            </h3>
        </div>
    </div>


    <!--Choosing template type area -->
    <div id="AddNewTemplate" class="row AddNewTemplate" runat="server" style="display: normal">
        <div class="col-md-12">
            <div class="tabbable tabbable-custom boxless">
                <!-- BEGIN FILTER -->
                <div class="margin-top-10">

                    <div class="row mix-grid">
                        <div class="col-md-3 col-sm-4 mix category_1">
                            <div class="mix-inner border_light_gray">
                                <img class="img-responsive" src="Images/side_with_image.PNG" alt="">
                                <div class="mix-details">
                                    <h4>Side with Image</h4>
                                    <a class="mix-link mix-link-new" onclick="SelectTemplate('template_SideWithImage')"><i class="fa fa-plus"></i>Add template</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 mix category_2">
                            <div class="mix-inner border_light_gray">
                                <img class="img-responsive" src="Images/side_information.PNG" alt="">
                                <div class="mix-details">
                                    <h4>Side Information</h4>
                                    <a class="mix-link mix-link-new" onclick="SelectTemplate('template_SideInformation')"><i class="fa fa-plus"></i>Add template</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 mix category_3">
                            <div class="mix-inner border_light_gray">
                                <img class="img-responsive" src="Images/center_with_image.PNG" alt="">
                                <div class="mix-details">
                                    <h4>Main with Image</h4>
                                    <a class="mix-link mix-link-new" onclick="SelectTemplate('template_CenterWithImage')"><i class="fa fa-plus"></i>Add template</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 mix category_1 category_2">
                            <div class="mix-inner border_light_gray">
                                <img class="img-responsive" src="Images/center_information.PNG" alt="">
                                <div class="mix-details">
                                    <h4>Main Information</h4>
                                    <a class="mix-link mix-link-new" onclick="SelectTemplate('template_MainInformation')"><i class="fa fa-plus"></i>Add template</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
    <br />


    <!-- Template Editor: Side with image -->
    <div id="template_SideWithImage" class="row template_SideWithImage" runat="server" style="display: none">
        <div class="col-md-12">
            <!-- Side with image -->
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-search"></i>Side with image
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Main Title</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Title_tmp_SideWithImage" class="form-control input-medium" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Short Text</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_ShortText_SideWithImage" class="form-control input-medium" TextMode="MultiLine" runat="server"></asp:TextBox>
                                            <span class="help-block">This text will appear before "Read More"
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Long Text</label>
                                        <div class="col-md-9">
                                              <textarea id="txt_LongText_tmp_SideWithImage" name="content" runat="server" rows="10" data-width="400" class="form-control"></textarea>
                                            <span class="help-block">This text will replace the short text
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Order_tmp_SideWithImage" class="form-control input-medium" TextMode="Number" runat="server"></asp:TextBox>
                                            <asp:RangeValidator ID="val_OrderRange1" ValidationGroup="check_SideWithImage" runat="server" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order_tmp_SideWithImage" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                            <span class="help-block">The number must be positive, and larger than 0.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- Second row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <div class="form-horizontal form-bordered">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">
                                                        Main Image
                                                        <br />
                                                        260 X 177</label>
                                                    <div class="col-md-9" runat="server" id="ImageUploaderDiv_SideWithImage">
                                                        <!--START File uploader -->
                                                        <uc1:ImageUploader ID="uc_ImageUploader_SideWithImage" runat="server" />
                                                        <!--END File uploader -->
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image title: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageTitle_SideWithImage" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image Alt: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageAlt_SideWithImage" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button class="btn green" ID="btn_Save_SideWithImage" runat="server" Text="Save" ValidationGroup="check_SideWithImage" OnClick="btn_Save_SideWithImage_Click" />
                                        <asp:Button ID="btn_Cancel1" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END OF SEARCH AREA -->
        </div>
    </div>

    <!-- Template Editor: Side information -->
    <div id="template_SideInformation" class="row template_SideInformation" runat="server" style="display: none">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-search"></i>Side information
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Main Title</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Title_tmp_SideInformation" class="form-control input-medium " runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Short Text</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_ShortText_tmp_SideInformation" class="form-control input-medium " TextMode="MultiLine" runat="server"></asp:TextBox>
                                            <span class="help-block">This text will appear before "Read More"
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Long Text</label>
                                        <div class="col-md-9">
                                            <textarea id="txt_LongText_tmp_SideInformation" name="content" rows="10" data-width="400" class="form-control" runat="server"></textarea>
                                            <span class="help-block">This text will replace the short text
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Order_tmp_SideInformation" class="form-control input-medium" TextMode="Number" runat="server" Text="1"></asp:TextBox>
                                            <asp:RangeValidator ID="val_OrderRange2" runat="server" ValidationGroup="check_SideInformation" ErrorMessage="The number is incorrect." ForeColor="Red" Type="Integer" ControlToValidate="txt_Order_tmp_SideInformation" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                            <span class="help-block">The number must be positive, and larger than 0.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button class="btn green" ID="btn_Save_SideInformation" runat="server" Text="Save" ValidationGroup="check_SideInformation" OnClick="btn_Save_SideInformation_Click" />
                                        <asp:Button ID="btn_Cancel2" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" />

                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END OF SEARCH AREA -->
        </div>
    </div>

    <!-- Template Editor: Center with image -->
    <div id="template_CenterWithImage" class="row template_CenterWithImage" runat="server" style="display: none">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-search"></i>Main with image
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Main Title</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Title_tmp_CenterWithImage" class="form-control input-medium " runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Short Text</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_ShortText_tmp_CenterWithImage" class="form-control input-medium " TextMode="MultiLine" runat="server"></asp:TextBox>
                                            <span class="help-block">This text will appear before "Read More"
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Long Text</label>
                                        <div class="col-md-9">
                                            <textarea name="content" id="txt_LongText_tmp_CenterWithImage" rows="10" data-width="400" class="form-control" runat="server"></textarea>
                                            <span class="help-block">This text will replace the short text
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Order_tmp_CenterWithImage" class="form-control input-medium" TextMode="Number" runat="server" Text="1"></asp:TextBox>
                                            <asp:RangeValidator ID="val_OrderRange3" runat="server" ValidationGroup="check_CenterWithImage" ErrorMessage="The number is incorrect." ForeColor="Red" Type="Integer" ControlToValidate="txt_Order_tmp_CenterWithImage" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                            <span class="help-block">The number must be positive, and larger than 0.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- Second row-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <div class="form-horizontal form-bordered">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">
                                                        Main Image<br />
                                                        220 X 150</label>
                                                    <div class="col-md-9" runat="server" id="ImageUploaderDiv_CenterWithImage">
                                                        <!--START File uploader -->
                                                        <uc1:ImageUploader ID="uc_ImageUploader_CenterWithImage" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image title: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageTitle_CenterWithImage" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image Alt: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageAlt_CenterWithImage" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button class="btn green" ID="btn_Save_CenterWithImage" runat="server" Text="Save" ValidationGroup="check_CenterWithImage" OnClick="btn_Save_CenterWithImage_Click" />
                                        <asp:Button ID="btn_Cancel3" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END OF SEARCH AREA -->
        </div>
    </div>

    <!-- Template Editor: Main information -->
    <div id="template_MainInformation" class="row template_MainInformation" runat="server" style="display: none">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet  box grey">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa  fa-search"></i>Main information
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div class="form-horizontal">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Main Title</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_Title_tmp_MainInformation" class="form-control input-medium " runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Order</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_Order_tmp_MainInformation" class="form-control input-medium" TextMode="Number" runat="server"></asp:TextBox>
                                                <asp:RangeValidator ID="val_OrderRange4" runat="server" ValidationGroup="check_MainInformation" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order_tmp_MainInformation" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                                <span class="help-block">The number must be positive, and larger than 0.
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-9">
                                                <asp:Button ID="btn_Save_MainInformation" runat="server" CssClass="btn green" Text="Save" OnClick="btn_Save_MainInformation_Click" />

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>

                <!-- END OF SEARCH AREA -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--START: SUB BOXES TABLE-->
                <div id="SubBoxesTableDiv" runat="server" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box blue">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-book"></i>Sub Box
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="SubBoxesTable table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                        <asp:Repeater ID="drp_SubBoxTable" runat="server" ItemType="DL_LowCost.SubBox">
                                            <HeaderTemplate>
                                                <thead>
                                                    <th>ID</th>
                                                    <th>Sub title</th>
                                                    <th>Order</th>
                                                    <th>Options</th>
                                                </thead>
                                                <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%#Eval("DocId_UI") %>
                                                    </td>
                                                    <td>
                                                        <%#Eval("SubTitle_UI") %>
                                                    </td>
                                                    <td>
                                                        <%#Eval("Order_UI") %>
                                                    </td>
                                                    <td>
                                                        <!-- Buttons for Editing or Deleting a SubBox   -->
                                                        <asp:LinkButton ID="btn_Edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: SUB BOXES TABLE-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="button" id="btn_AddSubBox" class="btn green btn_AddSubBox" value="Add Sub Box" style="display: none;" runat="server" />
            </div>
        </div>
        <br />
        <!-- START: NEW SUB-BOX / EDIT DIV -->
        <div id="NewSubBoxDiv" style="display: none" runat="server" class="NewSubBoxDiv">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet  box grey">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa  fa-search"></i>Add / Edit Sub Box 
                            </div>

                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div class="form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Sub Title</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_SubTitle" class="txt_SubTitle form-control input-medium " runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Short Text</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_ShortText" class="txt_ShortText form-control input-medium " TextMode="MultiLine" runat="server"></asp:TextBox>
                                                    <span class="help-block">This text will appear before "Read More"
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Long Text</label>
                                                <div class="col-md-9">
                                                    <textarea id="txt_LongText" name="content" rows="10" data-width="400" class="txt_LongText form-control" runat="server"></textarea>
                                                    <span class="help-block">This text will replace the short text
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Order</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_Order" class="txt_Order form-control input-medium" TextMode="Number" runat="server"></asp:TextBox>
                                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ValidationGroup="check_MainInformation" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                                    <span class="help-block">The number must be positive, and larger than 0.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!-- Save button -->

                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-9">
                                                <asp:Button ID="btn_SaveNew" runat="server" Text="Save" CssClass="btn green btn_SaveNew" ValidationGroup="check_SubBox" OnClick="btn_SaveNew_Click" CommandArgument="" />
                                                <asp:Button ID="btn_SaveEdit" runat="server" Text="Save Changes" CssClass="btn btn-block green btn_SaveEdit" ValidationGroup="check_SubBox" OnClick="btn_SaveEdit_Click" Style="display: none" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END NEW SUB-BOX DIV -->
    </div>

    <asp:Button ID="btn_Cancel" runat="server" CssClass="btn" Text="Back to Templates page" OnClick="btn_Cancel_Click" />
</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="/assets/scripts/app.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/table-advanced.js"></script>
    <script type="text/javascript" src="assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="assets/scripts/app.js"></script>
    <script src="assets/scripts/form-components.js"></script>


    <script src="Scripts/siteJS.js" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();

            FormComponents.init();
        });

        // Setting the CKEditors for all long texts with basic buttons only
        debugger;
        var txt_CKEditor = '<%=txt_LongText_tmp_SideWithImage.ClientID%>';
        SetCkEditorBasicButtons(txt_CKEditor);
        txt_CKEditor = '<%=txt_LongText_tmp_SideInformation.ClientID%>';   
        SetCkEditorBasicButtons(txt_CKEditor);
        txt_CKEditor = '<%=txt_LongText_tmp_CenterWithImage.ClientID%>'; 
        SetCkEditorBasicButtons(txt_CKEditor);
        txt_CKEditor = '<%=txt_LongText.ClientID%>';  
        SetCkEditorBasicButtons(txt_CKEditor);

        function SetCkEditorBasicButtons(ckeditorId) {
            CKEDITOR.replace(txt_CKEditor, {
                toolbar: [['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'Bold', 'Italic']]
            });
            CKEDITOR.config.height = '420';
        }

     
    </script>

</asp:Content>
