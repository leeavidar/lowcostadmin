﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="FlightsAdmin.FAQ" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/fancybox/source/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style-metronic.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/plugins.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/pages/portfolio.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/default.css" id="style_color" />
    <link rel="stylesheet" type="text/css" href="/assets/css/custom.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE CONTENT-->

    <!-- START: TITLE ROW-->
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Questions and Answers"></asp:Label>
            </h3>
        </div>
    </div>
    <!-- END: TITLE ROW-->

    <!-- START: ADD BUTTON ROW-->
    <div class="row">
        <div class="col-md-12 center">
            <!-- Add a new TIP button - The value of this button will change from "Add a new Tip" to "Cancel" and open or close the tip panel -->
            <asp:Button ID="btn_AddNewFaq" OnClick="btn_AddNewFaq_Click" runat="server" class="btn green btn_AddNewFaq" Text="Add Question and Answer" />
            <asp:Button ID="btn_cancel" runat="server" class="btn green btn_AddNewFaq" OnClick="btn_cancel_Click" Text="Cancel" />
<%--            <input type="button" id="btn_AddNewFaq" class="btn green btn_AddNewFaq" value="Add Question and Answer" runat="server" />--%>
        </div>
    </div>
    <!-- END: ADD BUTTON ROW-->

    <!-- START - ADD/EDIT PANEL-->
    <asp:Panel ID="pnl_edit_Add" runat="server">
    <div id="FaqPanel" class="row  mt10 FaqPanel" runat="server">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-edit"></i>Add/Edit Q&A
                    </div>

                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-1">notice:</label>
                                        <div class="col-md-11">
                                            When inserting or editing text, pay attention to the HTML tags (you can see them by clicking on the [Source] button).<br />
                                            Especially to tags that doesn't have a closing tag,like a &lt;div&gt; tag that doesn't have a closing  &lt;/div&gt; tag.
                                            <br />
                                            <strong>
                                            These things can break the page when it renders on the browser.
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br /><br />
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order</label>
                                        <div class="col-md-9">
                                            <asp:HiddenField ID="IdmaxValue" runat="server" />
                                            <asp:TextBox ID="txt_Order" class="form-control input-medium txt_Order" TextMode="Number" min="1" runat="server" Text="1"></asp:TextBox>
                                            <asp:RangeValidator ID="val_OrderRange1" ValidationGroup="check_tip" runat="server" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                            <span class="help-block">The number must be positive, and not larger than <asp:Label ID="lbl_IdmaxValue" runat="server" Text="Label"></asp:Label>.
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Question</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Question" CssClass="txt_Question form-control form-control-inline input-xlarge" runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Answer</label>
                                        <div class="col-md-9">
                                            <textarea id="txt_Answer" class="txt_Answer ckeditor form-control" name="txt_CKEditor" rows="6" runat="server"></textarea>

                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- Second row -->

                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <!-- This button's command arguments is set in a siteJS.js file  (SaveNew or SaveEdit) -->
                                        <asp:Button class="btn green btn_SaveNew" ID="btn_SaveNew" runat="server" Text="Save" ValidationGroup="check_tip" OnClick="btn_SaveNew_Click" />
                                        <asp:Button class="btn green btn_SaveEdit" ID="btn_SaveEdit" runat="server" Text="Save Changes" ValidationGroup="check_tip" OnClick="btn_SaveEdit_Click" Style="display: none" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- END - ADD/EDIT PANEL-->

    </asp:Panel>


    <br />
    <br />

    <!-- START: REPATER ROW-->
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div id="tab_1" class="tab-pane active">
                    <div id="accordion1" class="panel-group">
                        <asp:Repeater ID="drp_FAQ" runat="server" ItemType="DL_LowCost.Faq">
                            <HeaderTemplate>
                                <span>Notice that the questions are sorted by the order you give them. Click 'Edit' to change this order.</span>
                            </HeaderTemplate>
                            <ItemTemplate>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">

                                            <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-xm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                            <!-- Delete button -->
                                            <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-xm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                            <!-- START: Question -->

                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href='#<%# Eval("DocId_UI") %>'>

                                                <!-- In the HREF there should be the id of the DIV around the answer-->
                                                <span><%# Eval("Order_UI") %>. <%# Eval("Question_UI") %></span>
                                                <!--Edit button  -->
                                            </a>
                                            <!-- END: Question -->

                                        </h4>
                                    </div>
                                    <!-- START: Asnwer Area (the id of this div is the id of the repeater item, so that the link of the question will know what div to open)-->
                                    <div id='<%# Eval("DocId_UI")%>' class="panel-collapse collapse">
                                        <!-- START: Answer -->
                                        <div class="panel-body">
                                            <%#Eval("Answer_UI")%>
                                        </div>
                                        <!-- END: Answer -->
                                    </div>
                                    <!-- END: Answer Area-->
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: REPATER ROW-->

    <!-- END PAGE CONTENT-->

</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">

    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap2-typeahead.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="Scripts/siteJS.js" type="text/javascript"></script>
    <script src="Scripts/PageScripts/FAQ.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();
            FormComponents.init();
        });
    </script>
</asp:Content>
