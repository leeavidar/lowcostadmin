﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="Static_Page_Index.aspx.cs" Inherits="FlightsAdmin.Static_Page_Index" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link href="/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->

</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Static Pages Index"></asp:Label>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-book"></i>
                        <span>Table of pages</span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->

                    <div class="form-horizontal">
                        <!--remove this and the next line-->
                        <div class="form-body">

                            <!-- START: REPEATER-->
                            <asp:Repeater ID="drp_StaticPagesData" runat="server" ItemType="DL_LowCost.StaticPages">
                                <HeaderTemplate>
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" ID="lbl_StaticPageId" Text='<%# Eval("DocId_UI") %>' />
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lbl_StaticPageName" Text='<%# Eval("Name_UI") %>' />
                                        </td>
                                        <td>
                                            <!--Edit button-->
                                            <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                            <!-- Delete button-->
                                            <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" OnClientClick="return CheckDelete()" Visible='<%# CheckIfStaticEval() %>' CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                            </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <!-- END: REPEATER-->
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <div id="div_NewStaticPageButton" class="row" runat="server" >
        <div class="col-md-12 ">
            <asp:Button class="btn  green" ID="btn_NewStaticPage" runat="server" Text="New Page" OnClick="btn_NewStaticPage_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="CPHMain_ScriptsButton" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap2-typeahead.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/assets/scripts/form-components.js"></script>
    <script src="/assets/scripts/table-advanced.js"></script>
    <script src="/assets/scripts/ui-extended-modals.js"></script>
    <script src="/Scripts/siteJS.js"></script>


    <script>
        jQuery(document).ready(function () {
            UIExtendedModals.init();
            TableAdvanced.init();
            FormSamples.init();
            FormComponents.init();

        });
    </script>
</asp:Content>
