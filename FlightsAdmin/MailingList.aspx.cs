﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;

namespace FlightsAdmin
{
    public partial class MailingList : BasePage_UI
    {
        #region Session Private fields

        MailAddressContainer _oMailAddressContainer;
        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<MailAddressContainer>(out _oMailAddressContainer) == false)
            {
                //take the data from the table in the database
                _oMailAddressContainer = GetMailsFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<MailAddressContainer>(oSessionMailAddressContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<MailAddressContainer>(oSessionMailAddressContainer);

        }


        /// <summary>
        /// A method that gets all the mail addresses of a white label from the database.
        /// </summary>
        /// <returns></returns>
        private MailAddressContainer GetMailsFromDB()
        {
            //selecting all the mail addresses with a given white label
            MailAddressContainer allMails = BL_LowCost.MailAddressContainer.SelectAllMailAddresss(1, null);  //TODO:  replace with "WhiteLabelId"
            return allMails;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }
        
        #endregion
    }

    public partial class MailingList : BasePage_UI
    {
        #region Properties

        #region Private

        private int _whiteLabelId;
        private bool _IsRenderForScreen = true;

        #endregion
        #region Public

        public MailAddressContainer oSessionMailAddressContainer { get { return _oMailAddressContainer; } set { _oMailAddressContainer = value; } }

        public bool IsRenderForScreen
        {
            get
            {
                return this._IsRenderForScreen;
            }
            set
            {
                this._IsRenderForScreen = value;
            }
        }

        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }

        #endregion

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SalesOperator);
            if (!IsPostBack)
            {
                SetPage();
            }
        }
     
        #region Event Handlers

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the mail address from the command argument of the button
            int MailDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(MailDocId))
            {
                MailAddressContainer oMailAddressContainer = oSessionMailAddressContainer.SelectByID(MailDocId, 1); // TODO: replace 1 with whitelable
                if (oMailAddressContainer != null)
                {
                    MailAddress oMailAddress = oMailAddressContainer.Single;
                    if (oMailAddress != null)
                    {
                        oMailAddress.Action(DB_Actions.Delete);
                        oSessionMailAddressContainer = GetMailsFromDB();
                        BindDataToRepeater();

                        AddScript("DeletedAlert();");
                     //   Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "DeletedAlert();", true);
                    }
                }
            }
        }

        #endregion

        #region Helping methods

     //  private void Export()
     //  {
     //
     //      // drp_MailingList.DataBind();
     //
     //      //  if (Request.QueryString["Export"] == "1")
     //      //  {
     //      Response.ContentType = "application/vnd.ms-excel";
     //      Response.AddHeader("content-disposition", "attachment;filename=LowCostMails.xls");
     //      Response.Charset = "";
     //      this.EnableViewState = false;
     //      System.IO.StringWriter stringwriter = new System.IO.StringWriter();
     //      System.Web.UI.HtmlTextWriter htmltextwriter = new System.Web.UI.HtmlTextWriter(stringwriter);
     //      drp_MailingList.RenderControl(htmltextwriter);
     //      Response.Write(stringwriter.ToString());
     //      Response.End();
     //      //  }
     //
     //      // if (Request.QueryString["Export"] == "2")
     //      // {
     //      //     Response.ContentType = "application/ms-word";
     //      //     Response.AddHeader("content-disposition", "attachment;filename=LowCostMails.doc");
     //      //     Response.Charset = "";
     //      //     this.EnableViewState = false;
     //      //     System.IO.StringWriter stringwriter = new System.IO.StringWriter();
     //      //     System.Web.UI.HtmlTextWriter htmltextwriter = new System.Web.UI.HtmlTextWriter(stringwriter);
     //      //     drp_MailingList.RenderControl(htmltextwriter);
     //      //     Response.Write(stringwriter.ToString());
     //      //     Response.End();
     //      // }
     //  }

        private void SetPage()
        {
            BindDataToRepeater();
        }

        /// <summary>
        /// A method tha t bind all the mail addresses to the repeater
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_MailingList.DataSource = oSessionMailAddressContainer.DataSource;
            drp_MailingList.DataBind();
        }

        #endregion
    }
}