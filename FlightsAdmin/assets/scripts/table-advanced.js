var TableAdvanced = function () {

    var initTable1 = function() {

        /* Formatting function for row details */
        function fnFormatDetails ( oTable, nTr )
        {
            var aData = oTable.fnGetData( nTr );
            var sOut = '<div style="width:100%"><div style="float:left">';
            sOut += '<table class="border: 1px solid rgb(228, 228, 228);">';
            sOut += '<tr><td colspan="2"><h3>Order Details:</h3></td></tr>';
            sOut += '<tr><td>LowCost PNR:</td><td>' + aData[2] + '</td></tr>';
            sOut += '<tr><td>PNR company:</td><td>'+aData[3]+'</td></tr>';
            sOut += '<tr><td>Payment:</td><td>'+aData[4]+'</td></tr>';
            sOut += '<tr><td>Status:</td><td>' + aData[3] + '</td></tr>';
            sOut += '<tr><td colspan="2"><h3>Contact Details:</h3></td></tr>';
            sOut += '<tr><td>Phone Number:</td><td>' + aData[2] + '</td></tr>';
            sOut += '<tr><td>Email:</td><td>' + aData[3] + '</td></tr>';
            sOut += '<tr><td>Address:</td><td>' + aData[4] + '</td></tr>';
            sOut += '</table>';
            sOut += '</div><div style="float:left; margin-left:20px;">';
            sOut += '<table class="border: 1px solid rgb(228, 228, 228);">';
            sOut += '<tr><td colspan="2"><h3>Passengers:</h3></td></tr>';
            sOut += '<tr><td colspan="2"><h5>Passengers 1:</h5></td></tr>';
            sOut += '<tr><td>name:</td><td>' + aData[2] + '</td></tr>';
            sOut += '<tr><td>Date Of Birth:</td><td>' + aData[3] + '</td></tr>';
            sOut += '<tr><td>Passport Num:</td><td>' + aData[4] + '</td></tr>';
            sOut += '<tr><td>Passport Expiry Date:</td><td>10/10/2015</td></tr>';
            sOut += '<tr><td colspan="2"><h5>Passengers 2:</h5></td></tr>';
            sOut += '<tr><td>name:</td><td>' + aData[2] + '</td></tr>';
            sOut += '<tr><td>Date Of Birth:</td><td>' + aData[3] + '</td></tr>';
            sOut += '<tr><td>Passport Num:</td><td>' + aData[4] + '</td></tr>';
            sOut += '<tr><td>Passport Expiry Date:</td><td>10/10/2015</td></tr>';
          
            sOut += '</table class="border: 1px solid rgb(228, 228, 228);">';
            sOut += '</div><div style="float:left; margin-left:20px;">';
            sOut += '<table>';
            sOut += '<tr><td colspan="2"><h3>Flight Details:</h3></td></tr>';
            sOut += '<tr><td colspan="2"></td></tr>';
            sOut += '</table>';
            sOut += '</div></div>';
             
            return sOut;
        }

        /*
         * Insert a 'details' column to the table
         */

           var nCloneTh = document.createElement( 'th' );
           var nCloneTd = document.createElement( 'td' );

           nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';
            
           $('#sample_1 thead tr').each( function () {
               this.insertBefore( nCloneTh, this.childNodes[0] );
           } );
            
           $('#sample_1 tbody tr').each( function () {
               this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
           } );
            
        /*
         * Initialize DataTables, with no sorting on the 'details' column
         */
        var oTable = $('#sample_1').dataTable( {
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": [ 0 ] }
            ],
            "aaSorting": [[1, 'asc']],
             "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
        });

        jQuery('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#sample_1_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#sample_1_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
         
        /* Add event listener for opening and closing details
         * Note that the indicator for showing which row is open is not controlled by DataTables,
         * rather it is done here
         */

              $('#sample_1').on('click', ' tbody td .row-details', function () {
                  var nTr = $(this).parents('tr')[0];
                  if ( oTable.fnIsOpen(nTr) )
                  {
                      /* This row is already open - close it */
                      $(this).addClass("row-details-close").removeClass("row-details-open");
                      oTable.fnClose( nTr );
                  }
                  else
                  {
                      /* Open this row */                
                      $(this).addClass("row-details-open").removeClass("row-details-close");
                      oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
                  }
              });
    }

    var initTable2 = function() {
        var oTable = $('#sample_2').dataTable( {           
            "aoColumnDefs": [
                { "aTargets": [ 0 ] }
            ],
            "aaSorting": [[0, 'desc']],//asc
             "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
        });

        jQuery('#sample_2_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#sample_2_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

           initTable1();
            initTable2();
        }

    };

}();