﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class AirlinesIndex : BasePage_UI
    {
        #region Session Private fields

        private AirlineContainer _oSessionAirlineContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<AirlineContainer>(out _oSessionAirlineContainer) == false)
            {
                //take the data from the table in the database
                _oSessionAirlineContainer = GetAirlinesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<AirlineContainer>(oSessionAirlineContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<AirlineContainer>(oSessionAirlineContainer);

        }


        /// <summary>
        /// A method that gets all the Companies from the database.
        /// </summary>
        /// <returns></returns>
        private AirlineContainer GetAirlinesFromDB()
        {
            //selecting all the Companies  
            AirlineContainer allCompanies = BL_LowCost.AirlineContainer.SelectAllAirlines(true);
            return allCompanies;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }


    public partial class AirlinesIndex : BasePage_UI
    {
        #region Properties
        public AirlineContainer oSessionAirlineContainer { get { return _oSessionAirlineContainer; } set { _oSessionAirlineContainer = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                AirlinePanel.Style.Add("display", "none");
            }
            SetPage();
        }

        /// <summary>
        /// Sets all the data on the page
        /// </summary>
        private void SetPage()
        {
            //  BindDataToRepeater();
        }

        #region Event Handlers

        protected void btn_Save_New_Click(object sender, EventArgs e)
        {
            // Check validations
            if (!IsValid)
            {
                return;
            }
            #region Getting the values from the fields
            string iataCode = txt_IataCode.Text;
            string name = txt_Name.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            var oAirline = new Airline()
            {
                IataCode = iataCode,
                Name = name,
                IsActive = isActive
            };
            // Save the new city to the DB, and update the session
            oAirline.Action(DB_Actions.Insert);
            oSessionAirlineContainer = GetAirlinesFromDB();

            BindDataToRepeater(null, oAirline.Name_UI);
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string iataCode = txt_IataCode.Text;
            string name = txt_Name.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            Button saveButton = (Button)sender;
            int cityDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(cityDocId))
            {
                var oAirlineContainer = AirlineContainer.SelectByID(cityDocId, null);

                if (oAirlineContainer != null && oAirlineContainer.Count > 0)
                {
                    var airlineToEdit = oAirlineContainer.Single;
                    #region Fill city properties
                    airlineToEdit.IataCode = iataCode;
                    airlineToEdit.Name = name;
                    airlineToEdit.IsActive = isActive;
                    #endregion

                    airlineToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the button that shows the panel.
                    AirlinePanel.Style.Add("display", "none");
                    btn_AddNewAirline.Value = "Add a new Airline";

                    oSessionAirlineContainer = GetAirlinesFromDB();
                    BindDataToRepeater(null, txt_SearchTerm.Value);
                }
            }
        }

        /// <summary>
        /// Fill the edit panel  with data from the Airline in the session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the Tip to edit from the command argument of the button
            int airlineDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(airlineDocId))
            {
                var oAirlineContainer = oSessionAirlineContainer.SelectByID(airlineDocId);
                if (oAirlineContainer != null)
                {
                    var oAirline = oAirlineContainer.Single;
                    if (oAirline != null)
                    {
                        #region Fill the tip panel with data.
                        txt_IataCode.Text = oAirline.IataCode_UI;
                        txt_Name.Text = oAirline.Name_UI;
                        cb_IsActive.Checked = oAirline.IsActive_Value;
                        #endregion

                        // Displaying the SaveEdit button (and hiding the other one)
                        btn_SaveNew.Style.Add("display", "none");
                        btn_SaveEdit.Style.Add("display", "normal");
                        // Unhide the city panel for editing
                        AirlinePanel.Style.Add("display", "normal");
                        btn_SaveEdit.CommandArgument = airlineDocId.ToString();
                        btn_AddNewAirline.Value = "Cancel";
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int airlineDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(airlineDocId))
            {
                AirlineContainer oAirlineContainer = oSessionAirlineContainer.SelectByID(airlineDocId);
                if (oAirlineContainer != null)
                {
                    Airline oAirline = oAirlineContainer.Single;
                    if (oAirline != null)
                    {
                        oAirline.Action(DB_Actions.Delete);
                        oSessionAirlineContainer = GetAirlinesFromDB();
                        BindDataToRepeater(null, null, true);
                    }
                }
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            // When clicking the search button

            // get the search term:
            var term = txt_SearchTerm.Value.ToLower();

            if (term.Length >= 2)
            {
                // search for the term in the session object 
                var searchResults = GetSearchResults(term);

                // fill the repeater with the cintainer of matching results
                BindDataToRepeater(searchResults);

                // fire the javascript function that inits the table.
                AddScript("AfterUpdate()");
            }
            else
            {
                // there must be at least 2 characters for he search to not return to many results.
                return;
            }

        }


        #endregion

        #region Helping methods
        private void BindDataToRepeater(AirlineContainer oAirlineContainer = null, string searchTerm = null, bool IsEmpty = false)
        {
            if (IsEmpty == true)
            {
                drp_Airlines.DataSource = new AirlineContainer().DataSource;
            }
            else if (oAirlineContainer != null)
            {
                drp_Airlines.DataSource = oAirlineContainer.DataSource;
            }
            else if (searchTerm != null && searchTerm.Length >= 2)
            {
                var searchResults = GetSearchResults(searchTerm);
                drp_Airlines.DataSource = searchResults.DataSource;
            }
            else
            {
                drp_Airlines.DataSource = oSessionAirlineContainer.DataSource;
            }
            drp_Airlines.DataBind();
        }

        /// <summary>
        /// A function that gets all matching Airlines to the search term provided.
        /// </summary>
        /// <param name="term">A string as a search term</param>
        /// <returns>All the results that containe the search term (in the code, name, city name etc.)</returns>
        private AirlineContainer GetSearchResults(string term)
        {
            // search for the term in the session object 
            var searchResults = AirlineContainer.SelectAllAirlines(null).FindAllContainer(
                a => a.IataCode_UI.ToLower().Contains(term) ||
                a.Name_UI.ToLower().Contains(term));

            // return the results
            return searchResults;
        }

        /// <summary>
        /// Set the panel for a new entry.
        /// </summary>
        private void ClearAirlinePanel()
        {
            txt_IataCode.Text = "";
            txt_Name.Text = "";
            cb_IsActive.Checked = true;
        }

        #endregion
    }
}