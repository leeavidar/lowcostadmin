﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class Admins : BasePage_UI
    {
        #region Session Private fields

        private UsersContainer _oSessionUsersContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<UsersContainer>(out _oSessionUsersContainer) == false)
            {
                //take the data from the table in the database
                _oSessionUsersContainer = GetUsersFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<UsersContainer>(oSessionUsersContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<UsersContainer>(oSessionUsersContainer);

        }


        /// <summary>
        /// A method that gets all the admin users of a white label from the database.
        /// </summary>
        /// <returns></returns>
        private UsersContainer GetUsersFromDB()
        {
            //selecting all the users with a given white label
            UsersContainer allAdmins = UsersContainer.SelectByKeysView_WhiteLabelDocId_Role(1, (int)EnumHandler.UserRoles.SalesOperator, null);  //TODO:  replace with "WhiteLabelId"
            allAdmins.Add(UsersContainer.SelectByKeysView_WhiteLabelDocId_Role(1, (int)EnumHandler.UserRoles.SiteOperator, null));

            // If the logged user is a "Chief Admin", add the users with admin role also
            if (SessionManager.GetAdminFromSession().Role == (int)EnumHandler.UserRoles.ChiefAdmin)
            {
                allAdmins.Add(UsersContainer.SelectByKeysView_WhiteLabelDocId_Role(1, (int)EnumHandler.UserRoles.Admin, null));
            }
            return allAdmins;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion

    }

    public partial class Admins : BasePage_UI
    {
        #region Private

        private int _whiteLabelId;
        #endregion

        #region Public

        public UsersContainer oSessionUsersContainer { get { return _oSessionUsersContainer; } set { _oSessionUsersContainer = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            // Handle the permission for this page (if no permissions, redirect to the Login page).
            this.Master.PagePermissions(EnumHandler.UserRoles.Admin);

            if (!IsPostBack)
            {
                BindDataToRepeater();
                SetDropDownLists();
                // Hide the edit panel
                AdminPanel.Style.Add("display", "none");
            }
        }


        #region Event Handlers

        protected void btn_Save_New_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                // Re-open the edit panel (because the data that was enterd is invalid), and needs to be corrected before sebding it again)
                AdminPanel.Style.Add("display", "normal");
                btn_AddNewAdmin.Value = "Cancel";
                return;
            }
            Button saveButton = (Button)sender;

            Users oUser = new Users()
            {
                #region Object initializer

                FirstName = txt_FirstName.Text,
                LastName = txt_LastName.Text,
                Password = txt_Password.Text,
                Email = txt_Email.Text,
                UserName = txt_UserName.Text,
                Role = ConvertToValue.ConvertToInt(ddl_Roles.SelectedValue)

                #endregion
            };

            // Insert the new object to the DB
            int result = oUser.Action(DB_Actions.Insert);

            // Get the complete table with the new object from the DB
            oSessionUsersContainer = GetUsersFromDB();
            BindDataToRepeater();

        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            // The command argument of this button is set when the user clicks on the 'edit' button of a row in the repeater. (in method: "btn_Edit_Click" ) 
            Button saveButton = (Button)sender;
            int userDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(userDocId))
            {
                Users UserToEdit = oSessionUsersContainer.SelectByID(userDocId).Single;
                if (UserToEdit != null)
                {
                    #region Fill admin properties

                    UserToEdit.FirstName = txt_FirstName.Text;
                    UserToEdit.LastName = txt_LastName.Text;
                    UserToEdit.Password = txt_Password.Text;
                    UserToEdit.Email = txt_Email.Text;
                    UserToEdit.Role = ConvertToValue.ConvertToInt(ddl_Roles.SelectedValue);

                    #endregion

                    // Update the object in the DB
                    UserToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the text on the button that shows the panel 
                    AdminPanel.Style.Add("display", "none");
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewAdmin.Value = "Add new Admin";

                    //Refresh the repeater to include the new data.
                    oSessionUsersContainer = GetUsersFromDB();
                    BindDataToRepeater();

                    AddScript("UserUpdateSucesss();");
                  //  Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "UserUpdateSucesss();", true);
                }
            }
        }

        /// <summary>
        /// Fill the sub box panel with data from the sub box in the session,
        /// and display the edit panel (the actual saving is in another event handler)
        /// </summary>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the admin to edit from the command argument of the button
            int userDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(userDocId))
            {
                Users oUser = oSessionUsersContainer.SelectByID(userDocId).Single;
                if (oUser != null)
                {
                    #region Fill the admin panel with data

                    txt_FirstName.Text = oUser.FirstName_UI;
                    txt_LastName.Text = oUser.LastName;
                    txt_UserName.Text = oUser.UserName_UI;

                    // Make username editing disabled, and disable the username validators
                    val_req_UserName.Enabled = false;
                    val_UserNameExist.Enabled = false;
                    txt_UserName.Enabled = false;

                    txt_UserName.ControlStyle.Reset();
                    txt_UserName.ControlStyle.CssClass = "form-control input-medium txt_UserName";

                    txt_Password.Text = oUser.Password_UI;
                    txt_Email.Text = oUser.Email_UI;
                    ddl_Roles.SelectedIndex = ddl_Roles.Items.IndexOf(ddl_Roles.Items.FindByValue(oUser.Role_Value.ToString()));
                    #endregion

                    // Displaying the SaveEdit button (and hiding the other one)
                    btn_SaveNew.Style.Add("display", "none");
                    btn_SaveEdit.Style.Add("display", "normal");
                    // Unhide the client panel for editing
                    AdminPanel.Style.Add("display", "normal");

                    // Setting the command argument of the saveedit button to be the docId of the user.
                    btn_SaveEdit.CommandArgument = userDocId.ToString();
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewAdmin.Value = "Cancel";
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the user from the command argument of the button
            int userDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(userDocId))
            {
                Users oUser = oSessionUsersContainer.SelectByID(userDocId).Single;
                if (oUser != null)
                {
                        oUser.Action(DB_Actions.Delete);
                      //  oSessionUsersContainer = GetUsersFromDB();

                        // It's imoprtant to hide the admin panel using display:none (and not visible=false) in to be able to display it back in javascript.
                        AdminPanel.Style.Add("display", "none");
                        // IMPORTANT: the script for this button is based on this text!
                        btn_AddNewAdmin.Value = "Add new Admin";
                        // Refresh the repeater
                        oSessionUsersContainer = GetUsersFromDB();
                        BindDataToRepeater();
                }
            }
        }

        protected void val_UserNameExist_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (oSessionUsersContainer.FindFirstOrDefault(u => u.UserName_Value == args.Value) != null)
            {
                args.IsValid = false;
                return;
            }
        }

        #endregion

        #region Helping methods

        /// <summary>
        /// Binding the drop down list of roles to the roles enum.
        /// </summary>
        private void SetDropDownLists()
        {
            ddl_Roles.DataSource = EnumUtil.GetEnumToListItems<EnumHandler.UserRoles>();
            ddl_Roles.DataValueField = "Value";
            ddl_Roles.DataTextField = "Text";
            ddl_Roles.DataBind();

            // Remove the cief admin option from the list
            ddl_Roles.Items.Remove(ddl_Roles.Items.FindByValue("0"));

            /* if the logged user is an admin, remove the admin option from the list 
            (users of role "Chief Admin" will see the admin in the DDL, and other users can't enter this page) */
            Users _AdminObject = SessionManager.GetAdminFromSession();
            if (_AdminObject.Role_Value == (int)EnumHandler.UserRoles.Admin)
            {
                ddl_Roles.Items.Remove(ddl_Roles.Items.FindByValue("1"));
            }
        }

        /// <summary>
        /// Binding the repeater of admins to the admins table from the database
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_Admins.DataSource = oSessionUsersContainer.DataSource;
            drp_Admins.DataBind();
        }

        /// <summary>
        /// Getting the role name, using 'ToLanfString' method, of a provided user.
        /// </summary>
        /// <param name="admin">The user to get the role for.</param>
        /// <returns>A string of the role.</returns>
        protected string GetRole(Users admin)
        {
            return ((EnumHandler.UserRoles)(admin.Role_Value)).ToLangString();
        }

        #endregion

    }
}