﻿<%@ WebHandler Language="C#" Class="SeoHandler" %>

using System;
using System.Web;
using BL_LowCost;

public class SeoHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        string friendlyUrl = context.Request.QueryString["Url"];
        string relatedObject = context.Request.QueryString["object"];

        if (friendlyUrl == "")
            context.Response.Write("none");
        else
        {
            if (SeoContainer.IsFriendlyUrlTaken(friendlyUrl, 1))
            {
                context.Response.Write("taken");
            }
            else
                context.Response.Write("free");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}