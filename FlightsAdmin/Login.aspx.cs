﻿using BL_FlightsGW;
using DL_Generic;
using DL_FlightsGW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;
using DL_LowCost;
using BL_LowCost;

namespace FlightsAdmin
{

    public partial class Login : BasePage_UI
    {
        #region properties
        #region Private
        #endregion
        #region Public

        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SessionManager.ResetAdminSession();
                lbl_Error.Style.Add("display", "none");
            }
        }

        #region Event Handlers

        protected void btn_Login_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            Users oAdmin = GetUsersFromDB(txt_UserName.Text);
            if (oAdmin != null)
            {
                if (oAdmin.Password_Value == txt_Password.Text)
                {
                    SessionManager.SaveAdminToSession(oAdmin);
                    Response.Redirect(StaticStrings.path_Index);
                }
                else
                {
                    DisplayError("The password is incorrect");
                }
            }
            else
            {
                //no user found
                DisplayError("The user does not exist");
            }

        }

        private void DisplayError(string error)
        {
            lbl_Error.Style.Add("display", "normal");
            lbl_Error.Text = error;
            txt_Password.Text = "";
        }
        #endregion


        #region Helpin gmethods

        /// <summary>
        /// A method that gets a user from the DB, by it's userName
        /// </summary>
        /// <param name="userName">The user name to search in the database</param>
        /// <returns>The user with the provided user name</returns>
        private Users GetUsersFromDB(string userName)
        {
            UsersContainer oUsersContainer = UsersContainer.SelectByKeysView_WhiteLabelDocId_UserName(1, userName, null); // TODO: replace 1 with real WhiteLabel
            if (oUsersContainer != null && oUsersContainer.Count > 0)
            {
                return oUsersContainer.Single;
            }
            return null;
        }

        #endregion

        internal override void LoadFromSession(bool isPostBack)
        {
        }

        internal override void SaveToSession()
        {
        }

        internal override void resetAllSessions()
        {
        }
    }
}