﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class DestinationPage_Index : BasePage_UI
    {
        #region Session Private fields

        private DestinationPageContainer _oSessionDestinationPages;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<DestinationPageContainer>(out _oSessionDestinationPages) == false)
            {
                //take the data from the table in the database
                _oSessionDestinationPages = GetDestinationPagesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<DestinationPageContainer>(oSessionDestinationPages);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            ClearDestinationPageContainer();

        }

        private void ClearDestinationPageContainer()
        {
            Generic.SessionManager.ClearSession<DestinationPageContainer>(oSessionDestinationPages);
        }

        /// <summary>
        /// A method that gets all the destination pages of a white label from the database.
        /// </summary>
        /// <returns></returns>
        private DestinationPageContainer GetDestinationPagesFromDB()
        {
            //selecting all the destination pages with a given white label
            DestinationPageContainer allDestinationPages = BL_LowCost.DestinationPageContainer.SelectAllDestinationPages(1, null);  //TODO:  replace with "WhiteLabelId"
            return allDestinationPages;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class DestinationPage_Index : BasePage_UI
    {
        #region Properties
        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public DestinationPageContainer oSessionDestinationPages { get { return _oSessionDestinationPages; } set { _oSessionDestinationPages = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            // Bind the data to the repeater
            InitRepeater();
        }

        #region Event Handlers

        protected void btn_AddNewDestinationPage_Click(object sender, EventArgs e)
        {
            Response.Redirect(StaticStrings.path_DestinationEdit);
        }

        protected void btn_edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id))
            {
                // Redirecting to the static page edit page, with the id of the page to edit.
                Response.Redirect(QueryStringManager.RedirectWithQuery(StaticStrings.path_DestinationEdit, EnumHandler.QueryStrings.ID, id.ToString()));
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id))
            {
                DestinationPageContainer oDestinationPageContainer = oSessionDestinationPages.SelectByID(id, 1);   // TODO: change the 1 to a real white label
                if (oDestinationPageContainer != null && oDestinationPageContainer.Count > 0)
                {
                    DestinationPage oDestinationPage = oDestinationPageContainer.Single;
                    if (oDestinationPage != null)
                    {
                        // deleting related object
                        DeleteRelatedObjects(oDestinationPage);

                        oDestinationPage.Action(DB_Actions.Delete);

                        oSessionDestinationPages = GetDestinationPagesFromDB();
                        InitRepeater();
                        AddScript("DeletedAlert();");
                       // Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "DeletedAlert();", true);
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }



        #endregion

        #region Extra functions

        /// <summary>
        /// A method that bind the repeater of destinatio pages to the table in the database.
        /// </summary>
        private void InitRepeater()
        {
            drp_DestinationPagesData.DataSource = oSessionDestinationPages.DataSource;
            drp_DestinationPagesData.DataBind();
        }

        /// <summary>
        /// A method that deletes all related objects to a destination page.
        /// </summary>
        /// <param name="oDestinationPage">The destination page to deleted it's related object.</param>
        private static void DeleteRelatedObjects(DestinationPage oDestinationPage)
        {
            // Delete the SEO object (to release the friendly url for use).
            if (oDestinationPage.SeoDocId != null)
            {
                oDestinationPage.SeoSingle.Action(DB_Actions.Delete);
            }
            // Delete the HomePageDestinationPage related to this destination page:
            HomePageDestinationPageContainer oHomePageDestinationPageContainer = HomePageDestinationPageContainer.SelectByKeysView_WhiteLabelDocId_DestinationPageDocId(1, oDestinationPage.DocId_Value, null); // TODO: replace the 1 with white label.
            if (oHomePageDestinationPageContainer.Count == 1)
            {
                HomePageDestinationPage oHomePageDestinationPage = oHomePageDestinationPageContainer.Single;
                oHomePageDestinationPage.Action(DB_Actions.Delete);
            }
        }

        #endregion
    }
}