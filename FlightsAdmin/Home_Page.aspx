﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="Home_Page.aspx.cs" Inherits="FlightsAdmin.Home_Page" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<%@ Register Src="Controls/ImageUploader.ascx" TagName="ImageUploader" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/UC_Seo.ascx" TagPrefix="uc1" TagName="UC_Seo" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">

    <!-- END PAGE LEVEL STYLES -->

</asp:Content>
<asp:Content ID="MainCOntent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Home Page" />
            </h3>

        </div>
    </div>

    <!-- START - MAIN PANEL -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <!-- START:  Title section of the panel -->
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-edit"></i>Edit Home Page
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <!-- END:  Title section of the panel -->


                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="row">

                                <!-- START: selecting the cover image -->
                                <div class="col-md-6">
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <div class="form-horizontal form-bordered">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Company Logo: </label>
                                                    <div class="col-md-9">
                                                        <!--START - FILE UPLOADER-->
                                                        <uc1:ImageUploader ID="uc_ImageUploader" runat="server" />
                                                        <!--END - FILE UPLOADER-->
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image title: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageTitle" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image Alt: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageAlt" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <!-- END: selecting the cover image -->


                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="control-label col-md-3">Lines of text:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_line1" class="form-control input-xlarge" runat="server" />
                                            <br />
                                            <asp:TextBox ID="txt_line2" class="form-control input-xlarge" runat="server" />
                                            <br />
                                            <asp:TextBox ID="txt_line3" class="form-control input-xlarge" runat="server" />
                                            <br />
                                            <asp:TextBox ID="txt_line4" class="form-control input-xlarge" runat="server" />
                                            <br />
                                            <span class="help-block "></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                                <%-- BEGIN: TIP of the day --%>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="control-label col-md-3">Tip of the day:</label>
                                        <div class="col-md-9">
                                            <asp:DropDownList ID="ddl_TipOfTheDay" runat="server" class="form-control input-xlarge"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%-- END: TIP of the day --%>

                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button class="btn green" ID="btn_Save" runat="server" Text="Save" ValidationGroup="check_Company" OnClick="btn_Save_Click" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- END - MAIN PANEL -->


    <!-- START - Popular Destinations PANEL -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-edit"></i>Popular destinations
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">

                    <!-- Adding a new destination page to the homepage-->
                    <span>Choose a destination page from the menu:  </span>
                    <asp:DropDownList ID="ddl_DestinationPages" CssClass="form-control input-medium" runat="server"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Please select a page" ForeColor="Red" ControlToValidate="ddl_DestinationPages" InitialValue="-1" ValidationGroup="check_DestinationPage"></asp:RequiredFieldValidator>
                    <asp:Button ID="btn_AddSelectedDestination" CssClass="btn blue" runat="server" Text="Add" OnClientClick="$(this).hide();" OnClick="btn_AddSelectedDestination_Click" ValidationGroup="check_DestinationPage" />
                    <asp:UpdatePanel ID="up_DestinationsPanel" runat="server">
                        <ContentTemplate>
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                <asp:Repeater ID="drp_DestinationPages" runat="server" ItemType="DL_LowCost.HomePageDestinationPage" OnItemDataBound="itemsRepeater_ItemDataBound">
                                    <HeaderTemplate>
                                        <thead>
                                            <tr>
                                                <th>Order</th>
                                                <th>Destination Name</th>
                                                <th class="hidden-xs"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <!-- The order is a priority-->
                                            <td>
                                                <!-- The order is for display only (it is not the actual order of the item in the database) -->
                                                <span id="lbl_Order" class="bold"><%#Container.ItemIndex +1 %> &nbsp;&nbsp;</span> 
                                                <asp:LinkButton ID="btn_MoveUp" runat="server" CommandName="MoveUp" CssClass="upArrow" OnClick="btn_Move_Click" CommandArgument='<%#Eval("DocId_UI")%>'><i class="fa fa-arrow-up"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btn_MoveDown" runat="server" CommandName="MoveDown" CssClass="downArrow" OnClick="btn_Move_Click" CommandArgument='<%#Eval("DocId_UI")%>'><i class="fa fa-arrow-down"></i></asp:LinkButton>
                                            </td>
                                            <td>
                                                <span id="lbl_Name"><%#GetDestinationPageName(Item)%></span>
                                            </td>
                                </td>
                                <td>
                                    <!-- Delete button -->
                                    <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <!-- END - Popular Destinations PANEL -->

    <!-- START - SEO PANEL -->
    <uc1:UC_Seo runat="server" ID="uc_Seo" />
    <!-- END - SEO PANEL -->
</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap2-typeahead.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/form-components.js"></script>
    <script src="/assets/scripts/table-advanced.js"></script>
    <script src="/Scripts/siteJS.js"></script>
    <script src="/Scripts/PageScripts/HomePage.js"></script>
</asp:Content>
