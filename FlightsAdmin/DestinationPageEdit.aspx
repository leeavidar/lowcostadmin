﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="DestinationPageEdit.aspx.cs" Inherits="FlightsAdmin.DestinationPageEdit" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<%@ Register Src="Controls/ImageUploader.ascx" TagName="ImageUploader" TagPrefix="uc1" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link href="assets/css/ExtraStyling.css" rel="stylesheet" />
    <script type="text/javascript">
        function ClientItemSelectedCity(sender, e) {
            $get('<%=hf_AutoCompleteCityDocID.ClientID %>').value = e.get_value();
        }
    </script>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- START MAIN DESTINATION PANEL (only this panel should be displayed when creating a new destination page)-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-edit"></i>Destination Page: 
                        <asp:Label ID="lbl_PageTitle" runat="server" Text="New Page"></asp:Label>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">City name (Hebrew):</label>
                                        <div class="col-md-9">

                                            <asp:TextBox ID="txt_CityName" CssClass="form-control input-medium" runat="server" />
                                            <asp:HiddenField ID="hf_AutoCompleteCityDocID" runat="server" />
                                            <ajaxToolkit:AutoCompleteExtender ServiceMethod="SearchCities" ServicePath="Autocomplete.asmx" ID="ac_CityName" CompletionInterval="100" EnableCaching="true" CompletionListItemCssClass="ExtenderList"
                                                CompletionListHighlightedItemCssClass="Highlight" OnClientItemSelected="ClientItemSelectedCity" CompletionListElementID="width"
                                                runat="server" CompletionSetCount="10" TargetControlID="txt_CityName" FirstRowSelected="true" MinimumPrefixLength="1">
                                            </ajaxToolkit:AutoCompleteExtender>
                                            <span class="help-block">
                                                <asp:CustomValidator ID="val_CityName" runat="server" ErrorMessage="City is not recognized" ForeColor="red" ControlToValidate="txt_CityName" ValidationGroup="check_destination" OnServerValidate="val_CityName_ServerValidate"></asp:CustomValidator>
                                                <br />
                                                Start typing and select from the list.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Menu Title:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_menuTitle" CssClass="form-control input-medium" runat="server" />
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This fields is required." ControlToValidate="txt_menuTitle" ForeColor="red" ValidationGroup="check_destination"></asp:RequiredFieldValidator>
                                                <br />
                                                the name are displayed in popular destinations menu.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Page title:
                                        </label>
                                        <div class="col-md-9">
                                            <!-- this is actually the page title -->
                                            <asp:TextBox ID="txt_Title" name="number" CssClass="form-control input-medium" runat="server" />
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="val_ReqName" runat="server" ErrorMessage="This fields is required." ControlToValidate="txt_Title" ForeColor="red" ValidationGroup="check_destination"></asp:RequiredFieldValidator>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Time difference:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Time" CssClass="form-control input-medium" runat="server" Enabled="False" placeholder="Leave empty"/>
                                            <span class="help-block">This will be automatically calculated after saving.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Local Currency:</label>
                                        <div class="col-md-9">
                                            <asp:DropDownList ID="ddl_Currency" CssClass="form-control input-medium " runat="server"></asp:DropDownList>
                                            <span class="help-block">Select a currency.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Minimum Price:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox name="number" TextMode="Number" ID="txt_Price" CssClass="form-control input-medium " runat="server" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_Price"
                                                ErrorMessage="Please Enter Only Numbers" ValidationExpression="^\d+$" ValidationGroup="check_destination"></asp:RegularExpressionValidator>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Price currency:</label>
                                        <div class="col-md-9">
                                            <asp:DropDownList ID="ddl_PriceCurrency" CssClass="form-control input-medium " runat="server"></asp:DropDownList>
                                            <span class="help-block">Select a currency.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <div class="form-horizontal form-bordered">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">
                                                        Main Image<br />
                                                        1140 X 670</label>
                                                    <div class="col-md-9">
                                                        <!--START File uploader -->
                                                        <uc1:ImageUploader ID="uc_ImageUploader" runat="server"/>
                                                        <!--END File uploader -->
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image title: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageTitle" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image Alt: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageAlt" runat="server" />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <!-- END FORM-->
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <div class="form-horizontal form-bordered">
                                            <div class="form-body">
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">
                                                        Small Image<br />
                                                        225 X 152</label>
                                                    <div class="col-md-9">
                                                        <!--START File uploader -->
                                                        <uc1:ImageUploader ID="uc_ImageUploader_Small" runat="server" />
                                                        <!--END File uploader -->
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image title: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageTitle_small" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="form-group last">
                                                    <label class="control-label col-md-3">Image Alt: </label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox CssClass="form-control form-control-inline input-medium" ID="txt_ImageAlt_small" runat="server" />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <!-- END FORM-->
                                </div>
                                <!--/span-->

                                <!-- Order is not relevat right now   -->

                                <%--<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Order" class="form-control input-medium" TextMode="Number" runat="server" Text="1"></asp:TextBox>
                                            <asp:RangeValidator ID="val_OrderRange" runat="server" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order" MinimumValue="1" MaximumValue="2147483647" ValidationGroup="check_destination"></asp:RangeValidator>
                                            <span class="help-block">The number must be positive, and larger than 0.
                                            </span>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>

                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-9">
                                    <asp:Button class="btn green" ID="btn_SaveDestination" runat="server" Text="Save" ValidationGroup="check_destination" OnClick="btn_SaveDestination_Click" />
                                    <asp:Button ID="btn_Cancel" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" formnovalidate />

                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- END MAIN DESTINATION PANEL-->



    <!-- START SEO PANEL (this panel is only disaplaed after saving the page in the database) -->
    <asp:UpdatePanel runat="server" ID="up_SeoPanel">
        <ContentTemplate>
            <div class="row" runat="server" id="SeoPanel">
                <div class="col-md-12">

                    <div class="portlet  box grey">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa  fa-edit"></i>Seo 
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div class="form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">URl Friendly</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_FriendlyUrl" CssClass="txt_FriendlyUrl form-control form-control-inline input-medium" runat="server"></asp:TextBox>
                                                    <span class="help-block">
                                                        <asp:Label ID="lbl_FriendlyUrlStatus" class="lbl_FriendlyUrlStatus" runat="server" Text="Status: "></asp:Label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SEO Title</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_SeoTitle" CssClass="form-control form-control-inline input-medium" runat="server"></asp:TextBox>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">SEO Description</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_SeoDescription" CssClass="form-control form-control-inline input-medium" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Meta data</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_SeoMetaData" CssClass="form-control form-control-inline input-medium" runat="server"></asp:TextBox>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-9">
                                                <asp:Button ID="btn_SeoSave" runat="server" CssClass="btn green" Text="Save" OnClick="btn_SeoSave_Click" />
                                                <asp:Button ID="bt_Cancel2" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" formnovalidate />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btn_SeoSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <!-- END SEO PANEL-->



    <!-- START TEMPLATES PANEL (this area is only disaplaed after saving the page in the database)-->
    <asp:UpdatePanel runat="server" ID="up_TemplatesPanel">
        <ContentTemplate>
            <div runat="server" id="templatesPanel" style="display: normal;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-edit"></i>Template table
                                </div>

                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                    <asp:Repeater ID="drp_TempaltesTable" runat="server" ItemType="DL_LowCost.Template">
                                        <HeaderTemplate>
                                            <thead>
                                                <tr>
                                                    <th>Order</th>
                                                    <th>Template title</th>
                                                    <th class="hidden-xs">Template Name</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <!-- The order is a priority-->
                                                <td><span id="lbl_Order"><%#Eval("Order_UI")%></span>
                                                </td>
                                                <td><span id="lbl_Title"><%#Eval("Title_UI")%></span>
                                                </td>
                                                <td class="hidden-xs"><span id="lbl_Type"><%#Eval("Type_UI")%></span>
                                                </td>
                                                <td>
                                                    <!--Edit button, that redirects to the edit page with template id in the querystring -->
                                                    <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                                    <!-- Delete button -->
                                                    <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12 ">
                        <!-- Add a new template button, that redirects to the edit page with no querystring -->
                        <asp:Button ID="btn_NewTemplate" runat="server" Text="Add template" CssClass="btn green " OnClick="btn_NewTemplate_Click" />
                        <asp:Button ID="btnCancel3" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" formnovalidate />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- END TEMPLATES PANEL -->

</asp:Content>

<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <script type="text/javascript" src="assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="assets/scripts/app.js"></script>
    <script src="assets/scripts/form-components.js"></script>
    <script src="assets/scripts/form-validation.js"></script>


    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="Scripts/siteJS.js"></script>



    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins

            FormComponents.init();
            FormValidation.init();


        });
    </script>

</asp:Content>
