﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="Static_Page_Editor.aspx.cs" Inherits="FlightsAdmin.Static_Page_Editor" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>

<%@ Register Src="~/Controls/UC_Seo.ascx" TagPrefix="uc1" TagName="UC_Seo" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <!-- END PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="portlet  box grey">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa  fa-edit"></i>
                <asp:Label ID="lbl_PageTitle" runat="server" Text=" New Static Page"></asp:Label>
            </div>
        </div>

        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <div class="form-horizontal">
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-2">Name</label>
                                <div class="col-md-9">
                                    <asp:TextBox ID="txt_PageName" name="name" CssClass="form-control form-control-inline input-medium" runat="server" data-required="1" />
                                    <asp:RequiredFieldValidator ID="va_RequiredPageName" runat="server" ErrorMessage="* Name is required" ControlToValidate="txt_PageName" ForeColor="red" ValidationGroup="CheckPage"></asp:RequiredFieldValidator>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <%--  <div class="col-md-6">
                            <div class="form-group">

                                <div class="checkbox-list">
                                    <div>
                                        <div class="checker">
                                            <span>
                                                <asp:CheckBox ID="cb_AddToSiteMap" runat="server" /></span>
                                        </div>
                                        Add to site map
                                    </div>
                                </div>
                            </div>
                        </div>--%>

                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea id="txt_CKEditor" class="ckeditor form-control" name="txt_CKEditor" rows="20" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-9">
                                <asp:CheckBox ID="cb_Active" runat="server" Text="Active page" style="display:none;"/>
                                <br />
                                <asp:Button ID="btn_Save" runat="server" CssClass="btn green" Text="Save" OnClick="btn_Save_Click" ValidationGroup="CheckPage" />
                                <asp:Button ID="btn_Cancel" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <span id="seo_Panel" runat="server" style="display: none;">
                    <uc1:UC_Seo runat="server" ID="uc_Seo" />
                </span>
            </div>
            <!-- END FORM-->


        </div>

    </div>
</asp:Content>
<asp:Content ID="CPHMain_ScriptsButton" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/ckeditor/ckeditor.js"></script>
    <script src="/assets/plugins/ckeditor/config.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
     <script type="text/javascript" src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/app.js"></script>

    <script src="/assets/scripts/form-components.js"></script>
    <script src="/assets/scripts/table-advanced.js"></script>



    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>

    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/scripts/form-validation.js"></script>
    <script src="Scripts/PageScripts/SeoPanel.js"></script>

    <script>
        $(function () {
            TableAdvanced.init();
            FormComponents.init();
            FormValidation.init();

            $('.demo-loading-btn').click(function () {
                var btn = $(this)
                btn.button('loading')
                setTimeout(function () {
                    btn.button('reset')
                }, 3000)
            });
        });

        CKEDITOR.config.height = '600';
    </script>

</asp:Content>
