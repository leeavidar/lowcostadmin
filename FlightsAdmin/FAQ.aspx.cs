﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class FAQ : BasePage_UI
    {
        #region Session Private fields

        private FaqContainer _oSessionFaqContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<FaqContainer>(out _oSessionFaqContainer) == false)
            {
                
                //take the data from the table in the database
                _oSessionFaqContainer = GetFaqsFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<FaqContainer>(oSessionFaqContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<FaqContainer>(oSessionFaqContainer);

        }


        /// <summary>
        /// A method that gets all the Faqs of a white label from the database.
        /// </summary>
        /// <returns></returns>
        private FaqContainer GetFaqsFromDB()
        {
            //selecting all the Faqs with a given white label
            FaqContainer allFaqs = BL_LowCost.FaqContainer.SelectAllFaqs(1, null);  //TODO:  replace with "WhiteLabelId"  - BasePage_UI.GetWhiteLabelDocID()
            return allFaqs;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion




    }


    public partial class FAQ : BasePage_UI
    {
        #region Prop
        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public FaqContainer oSessionFaqContainer { get { return _oSessionFaqContainer; } set { _oSessionFaqContainer = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        public int maxRange { get; set; }
        #endregion

        #endregion
    
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                btn_cancel.Visible = false;
                BindDataToRepeater();
                pnl_edit_Add.Visible = false;
               txt_Order.Text =  (GetMaxOrder() + 1).ToString();
            }
        }


    

        #region Event Handlers

        protected void btn_edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the Tip to edit from the command argument of the button
            int FaqDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(FaqDocId))
            {
                FaqContainer oFaqContainer = oSessionFaqContainer.SelectByID(FaqDocId, 1); // TODO: replace 1 with whitelable
                if (oFaqContainer != null)
                {
                    Faq oFaq = oFaqContainer.Single;
                    if (oFaq != null)
                    {
                        IdmaxValue.Value = (oSessionFaqContainer.Count).ToString();
                        txt_Order.Attributes.Add("max", IdmaxValue.Value);
                        lbl_IdmaxValue.Text = IdmaxValue.Value;
                        #region Fill the faq panel with data

                        txt_Question.Text = oFaq.Question_UI;
                        txt_Answer.InnerHtml = oFaq.Answer_UI;
                        txt_Order.Text = oFaq.Order_UI;
                        #endregion

                        // Displaying the SaveEdit button (and hiding the other one)
                        btn_SaveNew.Style.Add("display", "none");
                        btn_SaveEdit.Style.Add("display", "normal");
                        // Unhide the Faq panel for editing
                        pnl_edit_Add.Visible = true;
                        btn_SaveEdit.CommandArgument = FaqDocId.ToString();
                        btn_AddNewFaq.Visible = false;
                        btn_cancel.Visible = true;
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int FaqDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(FaqDocId))
            {
                FaqContainer oFaqContainer = oSessionFaqContainer.SelectByID(FaqDocId,this.WhiteLabelId); // TODO: replace 1 with whitelable
                oFaqContainer = oSessionFaqContainer.SelectByID(FaqDocId, this.WhiteLabelId); // TODO: replace 1 with whitelable
                if (oFaqContainer != null)
                {
                    Faq oFaq = oFaqContainer.Single;
                    if (oFaq != null)
                    {
                        var faqContainerToUpdateOrderPosition = oSessionFaqContainer.FindAllContainer(o => o.Order_Value >= oFaq.Order_Value);
                        foreach (var item in faqContainerToUpdateOrderPosition.DataSource)
                        {
                            item.Order--;
                            item.Action(DB_Actions.Update);
                        }
                        oFaq.Action(DB_Actions.Delete);
                        // Binding the data from the database to the repeater
                        BindDataToRepeater();
                        // Displaying a success alert
                        AddScript("DeletedAlert();");
                       // Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "DeletedAlert();", true);
                    }
                }
            }
        }

        protected void btn_SaveNew_Click(object sender, EventArgs e)
        {
            // Check validations
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string question = txt_Question.Text;
            string answer = txt_Answer.Value;
            int order = ConvertToValue.ConvertToInt(txt_Order.Text);
            var faqContainerToUpdateOrderPosition = oSessionFaqContainer.FindAllContainer(o => o.Order_Value >= order);
            foreach (var item in faqContainerToUpdateOrderPosition.DataSource)
            {
                item.Order++;
                item.Action(DB_Actions.Update);
            }
            #endregion

            Faq oFaq = new Faq()
            {
                Question = question,
                Answer = answer,
                Category = 1,
                Order= order,
                WhiteLabelDocId = 1 // TODO: replace with Whitelabel
            };
            // Save the new faq to the DB, and update the session
            oFaq.Action(DB_Actions.Insert);
            // Binding the data from the database to the repeater
            BindDataToRepeater();

            txt_Order.Text = (GetMaxOrder() + 1).ToString();
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
          
            string question = txt_Question.Text;
            string answer = txt_Answer.Value;
            int order = ConvertToValue.ConvertToInt(txt_Order.Text);

            #endregion

            Button saveButton = (Button)sender;
            int faqDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(faqDocId))
            {
                FaqContainer oFaqContainer = FaqContainer.SelectByID(faqDocId,this.WhiteLabelId, null); // TODO: replace the 1 with a real WhiteLabel

                if (oFaqContainer != null && oFaqContainer.Count > 0)
                {
                    Faq FaqToEdit = oFaqContainer.Single;
                    if (order > FaqToEdit.Order_Value) //אם המיקום משתנה לתחתית הטבהלה
                    {
                        var faqContainerToUpdateOrderPositionUp = oSessionFaqContainer.FindAllContainer(o => o.Order_Value <= order && o.Order_Value > FaqToEdit.Order_Value);
                        foreach (var item in faqContainerToUpdateOrderPositionUp.DataSource)
                        {
                            item.Order--;
                            item.Action(DB_Actions.Update);
                        }
                    }
                    else // אם המיקום מקודם בטבלה
                    {
                        var faqContainerToUpdateOrderPositionUp = oSessionFaqContainer.FindAllContainer(o => o.Order_Value >= order && o.Order_Value < FaqToEdit.Order_Value);
                        foreach (var item in faqContainerToUpdateOrderPositionUp.DataSource)
                        {
                            item.Order++;
                            item.Action(DB_Actions.Update);
                        }
                    }
                    
                   
                    #region Fill properties

                    FaqToEdit.Question = question;
                    FaqToEdit.Answer = answer;
                    FaqToEdit.Order = order;
                    #endregion
                   
                    FaqToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the button that shows the panel.
                    pnl_edit_Add.Visible = false;
                    btn_AddNewFaq.Visible = true;
                    btn_cancel.Visible = false;

                    // Binding the data from the database to the repeater
                    BindDataToRepeater();
                    // Displaying a success alert
                    Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "SavedAlert();", true);

                    txt_Order.Text = (GetMaxOrder() + 1).ToString();
                }
            }
        }
        protected void btn_AddNewFaq_Click(object sender, EventArgs e)
        {
            pnl_edit_Add.Visible = true;
            IdmaxValue.Value = (oSessionFaqContainer.Count + 1).ToString();
            txt_Order.Attributes.Add("max", IdmaxValue.Value);
            lbl_IdmaxValue.Text = IdmaxValue.Value;
            btn_cancel.Visible = true;
            btn_AddNewFaq.Visible = false;
            txt_Answer.Value = "";
            txt_Question.Text = "";
        }
        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            pnl_edit_Add.Visible = false;
            btn_AddNewFaq.Visible = true;
            btn_cancel.Visible = false;
            ClearFields();
        }

        private void ClearFields()
        {
            txt_Answer.Value = "";
            txt_Order.Text = "";
            txt_Question.Text = "";
        }

        #endregion


        #region Helping methods

        /// <summary>
        /// A method that updates the session container first, and binds the data to the repeater.
        /// </summary>
        private void BindDataToRepeater()
        {
            oSessionFaqContainer = GetFaqsFromDB();
            drp_FAQ.DataSource = oSessionFaqContainer.DataSource.OrderBy(h => h.Order_Value); 
            drp_FAQ.DataBind();
        }

        protected string GetAnswer(object item)
        {
            Faq oFaq = item as Faq;

            return oFaq.Answer_UI;
        }

        /// <summary>
        /// A method that finds that largest order number of a question.
        /// </summary>
        /// <returns>The max number found</returns>
        private int GetMaxOrder()
        {
            int max = _oSessionFaqContainer.Count;
            //for (int i = 0; i < _oSessionFaqContainer.DataSource.Count; i++)
            //{
            //    if (_oSessionFaqContainer.DataSource[i].Order_Value > max)
            //    {
            //        max = _oSessionFaqContainer.DataSource[i].Order_Value;
            //    }
            //}
            return max;
        }

        #endregion
    }
}