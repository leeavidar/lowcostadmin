﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using Generic;
using DL_Generic;
using DL_LowCost;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using Generic.ConfigurationZone;


namespace FlightsAdmin
{
    public partial class DestinationPageEdit : BasePage_UI
    {
        #region Session Private fields

        private DestinationPage _oSessionDestinationPage;

        //General data (not related to the destination page)
        private CurrencyContainer _oSessionCurrencies;
        // private CityContainer _oSessionCityContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions (or from the database if its the first load of the page or the session is empty)
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<DestinationPage>(out _oSessionDestinationPage) == false)
            {
                _oSessionDestinationPage = GetDestinationPageFromDB();
            }
            else
            {
                // The destinationPage should be in the session if it exist in the database. 
            }
            // Get the cities and currencies tables
            if (Generic.SessionManager.GetSession<CurrencyContainer>(out _oSessionCurrencies) == false)
            {
                _oSessionCurrencies = GetCurrenciesFromDB();
            }
            //if (Generic.SessionManager.GetSession<CityContainer>(out _oSessionCityContainer) == false)
            //{
            //    _oSessionCityContainer = GetCitiesFromDB();
            //}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<DestinationPage>(oSessionDestinationPage);
            Generic.SessionManager.SetSession<CurrencyContainer>(oSessionCurrencies);
            //   Generic.SessionManager.SetSession<CityContainer>(oSessionCityContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<DestinationPage>(oSessionDestinationPage);
            Generic.SessionManager.ClearSession<CurrencyContainer>(oSessionCurrencies);
            //  Generic.SessionManager.ClearSession<CityContainer>(_oSessionCityContainer);
        }

        /// <summary>
        /// A method that gets the destination page with a specified docId from the database.
        /// If there is no destination page with this ID, the result will be null.
        /// </summary>
        /// <returns>The Destination page with the specified DocId</returns>
        private DestinationPage GetDestinationPageFromDB()
        {
            //selecting the destination page with the matching id from the database
            DestinationPageContainer oDestinationPageContainer = DestinationPageContainer.SelectByID(QueryStringManager.QueryStringID(EnumHandler.QueryStrings.ID), 1, null);  //TODO: replace with "WhiteLabelId"
            if (oDestinationPageContainer != null && oDestinationPageContainer.Count > 0)
            {
                return oDestinationPageContainer.Single;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// A method that gets the entire table of currencies from the database.
        /// </summary>
        /// <returns></returns>
        private CurrencyContainer GetCurrenciesFromDB()
        {
            //selecting all the table of currencies
            return CurrencyContainer.SelectAllCurrencys(null);
        }

        /// <summary>
        /// A method that gets all the city table from the database.
        /// </summary>
        /// <returns></returns>
        //private CityContainer GetCitiesFromDB()
        //{
        //    //selecting all the table of cities
        //   // return CityContainer.SelectAllCitys(null);
        //}

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class DestinationPageEdit : BasePage_UI
    {
        #region Properties
        public int WhiteLabelId { get; set; }
        #region Private
        #endregion
        #region Public

        public DestinationPage oSessionDestinationPage { get { return _oSessionDestinationPage; } set { _oSessionDestinationPage = value; } }
        public CurrencyContainer oSessionCurrencies { get { return _oSessionCurrencies; } set { _oSessionCurrencies = value; } }
        //   public CityContainer oSessionCityContainer { get { return _oSessionCityContainer; } set { _oSessionCityContainer = value; } }

        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                SetPage();
            }
            
        }

        private void SetPage()
        {
            if (oSessionDestinationPage != null)
            {
                // << EDIT MODE >>
                uc_ImageUploader.SetImageUploader(txt_ImageTitle.Text, txt_ImageAlt.Text, oSessionDestinationPage.DocId_Value, EnumHandler.RelatedObjects.DestinationPage, EnumHandler.ImageTypes.Main);
                uc_ImageUploader_Small.SetImageUploader(txt_ImageTitle_small.Text, txt_ImageAlt_small.Text, oSessionDestinationPage.DocId_Value, EnumHandler.RelatedObjects.DestinationPage, EnumHandler.ImageTypes.Thumbnail);
                txt_menuTitle.Text = oSessionDestinationPage.Name_UI;
                if (!IsPostBack)
                {
                    FillAllData();
                    SetCurrencyDdl();
                    // if there are items in the ddl, select the first one.
                    if (ddl_Currency.Items.Count > 0)
                    {

                        var selectedItem = ddl_Currency.Items.FindByValue(oSessionDestinationPage.LocalCurrency_UI);
                        if (selectedItem != null)
                        {
                            selectedItem.Selected = true;
                        }

                    }
                    if (ddl_PriceCurrency.Items.Count > 0 &&
                        ddl_PriceCurrency.Items.FindByValue(oSessionDestinationPage.PriceCurrency_UI) != null)
                    {
                        ddl_PriceCurrency.Items.FindByValue(oSessionDestinationPage.PriceCurrency_UI).Selected = true;
                    }
                }

                // Un-hiding all the editing panels
                EditingPanelsAvailable(true);

                BindTemplatesToRepeater();

                // Get the city name and fill it in the city name text box.
                City oCity = StaticData.oCityList.Find(c => c.IataCode_Value == oSessionDestinationPage.DestinationIataCode_Value);
                if (oCity != null)
                {
                    txt_CityName.Text = oCity.Name_UI;
                }

            }
            else
            {
                // << NEW MODE >>
                if (!IsPostBack)
                {
                    SetCurrencyDdl();
                    //Hiding the editing panels
                    EditingPanelsAvailable(false);
                }
            }
        }

        #region Event Handlers

        #region Main Panel

        protected void btn_SaveDestination_Click(object sender, EventArgs e)
        {
            //If anything enterd is invalid
            if (!IsValid)
                return;

            string CityIATA = "";
            #region Get city IATA code

            // find the city by the entered text
            City oCity = StaticData.oCityList.Find(city => city.Name_UI.ToLower() == txt_CityName.Text.ToLower());
            if (oCity == null)
            {
                return;
            }
            else
            {
                CityIATA = oCity.IataCode_UI;
            }

            #endregion
            bool isImageUploaded = false;

            if (oSessionDestinationPage == null)
            {
                //  << NEW PAGE MODE >>

                // first get the local time using google api
                string localTime= GoogleApi.GetLocalTimeDiffrenceCityName(txt_CityName.Text, ConfigurationHandler.GoogleApiKey);

                // Creating a new Destination page and filling the properties
                oSessionDestinationPage = new DestinationPage()
                {
                    Name = txt_menuTitle.Text,
                    Title = txt_Title.Text,
                    Time = localTime,
                    LocalCurrency = ddl_Currency.SelectedValue,
                    Price = ConvertToValue.IsEmpty(ConvertToValue.ConvertToDouble(txt_Price.Text)) ? ConvertToValue.DoubleEmptyValue : ConvertToValue.ConvertToDouble(txt_Price.Text),
                    PriceCurrency = ddl_PriceCurrency.SelectedValue,
                    // Order = ConvertToValue.ConvertToInt(txt_Order.Text),  // Not relevant, becuase the order is determined in each page differently, and is stored in the HomepageDestinationPage table.
                    DestinationIataCode = CityIATA,
                    // Change the isActive property to false (so that the page will not be automatically active)
                    IsActive = true,
                    WhiteLabelDocId = 1 //TODO: replace the 1 with the real white label when there is white label support.
                };
                // Add the destinationPage to the DB.
                int result = oSessionDestinationPage.Action(DL_Generic.DB_Actions.Insert);
                EditingPanelsAvailable(true);
                ChangePageTitle();

                // If the new destination page was added successfully to the database
                if (result != -1)
                {
                    // upload the images and add it to the DB:
                    uc_ImageUploader.SetImageUploader(txt_ImageTitle.Text, txt_ImageAlt.Text, oSessionDestinationPage.DocId_Value, EnumHandler.RelatedObjects.DestinationPage, EnumHandler.ImageTypes.Main);
                    uc_ImageUploader.SaveImage();

                    uc_ImageUploader_Small.SetImageUploader(txt_ImageTitle_small.Text, txt_ImageAlt_small.Text, oSessionDestinationPage.DocId_Value, EnumHandler.RelatedObjects.DestinationPage, EnumHandler.ImageTypes.Thumbnail);
                    uc_ImageUploader_Small.SaveImage();
                    txt_Time.Text = string.Format("{0} Hours", ConvertToValue.ConvertToInt32(localTime) / 3600);
                }
            }
            else
            {
                //  << EDIT MODE >>
                oSessionDestinationPage.Name = txt_menuTitle.Text;
                oSessionDestinationPage.Title = txt_Title.Text;

                // first get the local time using google api
                string cityIata = StaticData.oCityList.Find(city => city.Name_UI == txt_CityName.Text).IataCode_UI;
                string localTime = GoogleApi.GetLocalTimeDiffrenceCityName(cityIata, ConfigurationHandler.GoogleApiKey);

                oSessionDestinationPage.Time = localTime;
                oSessionDestinationPage.Price = ConvertToValue.IsEmpty(ConvertToValue.ConvertToDouble(txt_Price.Text)) ? ConvertToValue.DoubleEmptyValue : ConvertToValue.ConvertToDouble(txt_Price.Text);
                oSessionDestinationPage.PriceCurrency = ddl_PriceCurrency.SelectedValue;
                //oSessionDestinationPage.Order = ConvertToValue.ConvertToInt(txt_Order.Text); // Not relevant, becuase the order is determined in each page differently, and is stored in the HomepageDestinationPage table.
                oSessionDestinationPage.LocalCurrency = ddl_Currency.SelectedValue;
                oSessionDestinationPage.DestinationIataCode = CityIATA;
                // Update the destinationPage in the DB.
                oSessionDestinationPage.Action(DL_Generic.DB_Actions.Update);

                // upload the images and add it to the DB:
                uc_ImageUploader.SetImageUploader(txt_ImageTitle.Text, txt_ImageAlt.Text, oSessionDestinationPage.DocId_Value, EnumHandler.RelatedObjects.DestinationPage, EnumHandler.ImageTypes.Main);
                uc_ImageUploader.SaveImage();

                uc_ImageUploader_Small.SetImageUploader(txt_ImageTitle_small.Text, txt_ImageAlt_small.Text, oSessionDestinationPage.DocId_Value, EnumHandler.RelatedObjects.DestinationPage, EnumHandler.ImageTypes.Thumbnail);
                isImageUploaded = uc_ImageUploader_Small.SaveImage();
                txt_Time.Text = string.Format("{0} Hours", ConvertToValue.ConvertToInt32(localTime) / 3600);

                AddScript("SavedAlert();");
            }

        }

        /// <summary>
        /// A method to validate the city name (for the custom validator of txt_CityName
        /// </summary>
        protected void val_CityName_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string str = args.Value;

            // find the city by the entered text
            City oCity = StaticData.oCityList.Find(city => city.Name_UI.ToLower() == txt_CityName.Text.ToLower());
            if (oCity == null)
            {
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

        #endregion

        #region SEO Panel Events

        protected void btn_SeoSave_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }
            Seo oSeo = oSessionDestinationPage.SeoSingle;

            if (oSeo != null)
            {
                // if there is already a SEO object for this destination page

                // the current Friendly url: 
                bool friendlyUrlTaken = IsFriendlyUrlTaken(oSeo);
                if (friendlyUrlTaken)
                {
                    return;
                }
                oSeo.FriendlyUrl = txt_FriendlyUrl.Text;
                oSeo.SeoTitle = txt_SeoTitle.Text;
                oSeo.SeoDescription = txt_SeoDescription.Text;
                oSeo.SeoKeyWords = txt_SeoMetaData.Text;
                oSeo.WhiteLabelDocId = 1; // TODO: Change to real white label

                // Update the SEO for this page in the DB
                oSeo.Action(DB_Actions.Update);

                // show a success alert
                ScriptManager.RegisterStartupScript(up_SeoPanel, up_SeoPanel.GetType(), "MyKey", "SavedAlert();", true);
            }
            else
            {
                // Create a new SEO for this page

                // first check the friendly url and if it's already in use, return (gives a message inside the function)
                bool friendlyUrlTaken = IsFriendlyUrlTaken();
                if (friendlyUrlTaken)
                {
                    return;
                }
                Seo oSeoNew = new Seo()
                      {
                          SeoTitle = txt_SeoTitle.Text,
                          FriendlyUrl = txt_FriendlyUrl.Text,
                          SeoDescription = txt_SeoDescription.Text,
                          SeoKeyWords = txt_SeoMetaData.Text,
                          WhiteLabelDocId = 1
                      };
                // Insert the new SEO to the DB
                oSeoNew.Action(DB_Actions.Insert);
                // Add the new SEO to the Destination
                oSessionDestinationPage.SeoSingle = oSeoNew;
                oSessionDestinationPage.SeoDocId = oSeoNew.DocId_Value;
                oSessionDestinationPage.Action(DB_Actions.Update);
            }
        }



        #endregion

        #region Templates Panel events

        protected void btn_edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int templateId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(templateId))
            {
                //Redirecting to the edit page with the id of the teplate to edit
                Response.Redirect(StaticStrings.path_TemplateEdit + "?" + EnumHandler.QueryStrings.ID + "=" + templateId.ToString());
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_NewTemplate_Click(object sender, EventArgs e)
        {
            // Redirecting to the tempate edit page without id in the querystring, to add a new template.
            Response.Redirect(StaticStrings.path_TemplateEdit + "?" + EnumHandler.QueryStrings.DestinationPageId + "=" + oSessionDestinationPage.DocId_UI);
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);

            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id))
            {
                TemplateContainer oTemplateContainer = oSessionDestinationPage.Templates.SelectByID(id, 1); //  oSessionTemplates.SelectByID(id, 1);   // TODO: change the 1 to a real white label
                if (oTemplateContainer != null && oTemplateContainer.Count > 0)
                {
                    Template oTemplate = oTemplateContainer.Single;
                    if (oTemplate != null)
                    {
                        oTemplate.Action(DB_Actions.Delete);
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        #endregion

        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(StaticStrings.path_DestinationIndex);
        }

        #endregion

        #region Extra methods

        /// <summary>
        /// A method that searches for a city name in the database, based on a given string.
        /// </summary>
        /// <param name="prefixText">The string (can be part of the city name)</param>
        /// <param name="count">How many letter to start the search from (because the method is called in a change event of a textbox)</param>
        /// <returns>A list of all matching resukts</returns>
        [ScriptMethod()]
        [WebMethod(EnableSession = true)]
        public List<string> SearchCities(string prefixText, int count)
        {
            CityContainer oCityContainer = (HttpContext.Current.Session["CityContainer"] != null) ? (CityContainer)HttpContext.Current.Session["CityContainer"] : CityContainer.SelectAllCitys(null);

            List<string> cities = new List<string>();
            foreach (City item in oCityContainer.DataSource)
            {
                if (item.Name_UI.ToLower().Contains(prefixText.ToLower()))
                {
                    string city = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(item.Name_UI, item.Name_UI);
                    cities.Add(city);
                }
            }
            return cities;
        }


        /// <summary>
        /// A method that checks if there is a SEO object for the current destinatio page, and fills the seo panel on the page.
        /// </summary>
        private void FillSeoPanel()
        {
            Seo oSeo = oSessionDestinationPage.SeoSingle;
            if (oSeo != null)
            {
                // if there is a SEO object for this destination page
                txt_SeoTitle.Text = oSeo.SeoTitle;
                txt_FriendlyUrl.Text = oSeo.FriendlyUrl;
                txt_SeoDescription.Text = oSeo.SeoDescription;
                txt_SeoMetaData.Text = oSeo.SeoKeyWords;
            }
            else
            {
                // There is no SEO for this destinatio page.
            }
        }

        /// <summary>
        /// A method that fills the general panel (the first panel) on the page.
        /// </summary>
        private void FillGeneralPanel()
        {
            City oCity = StaticData.oCityList.Find(c => c.IataCode_Value == oSessionDestinationPage.DestinationIataCode_Value);
                if (oCity != null)
                {
                    txt_CityName.Text = oCity.Name_UI;
                }
            txt_menuTitle.Text =  oSessionDestinationPage.Name_UI;
            txt_Title.Text = oSessionDestinationPage.Title_UI;
            // txt_Order.Text = oSessionDestinationPage.Order_UI; // Not relevant, becuase the order is determined in each page differently, and is stored in the HomepageDestinationPage table.
            txt_Price.Text = ConvertToValue.IsEmpty(oSessionDestinationPage.Price_Value) ? "" : oSessionDestinationPage.Price_UI;
            txt_Time.Text = string.Format("{0} Hours", ConvertToValue.ConvertToInt32(oSessionDestinationPage.Time_UI) / 3600);
            // If there is a main image for this page
            Images oImage = oSessionDestinationPage.ImagesContainer.FindFirstOrDefault(img => img.ImageType_Value == EnumHandler.ImageTypes.Main.ToString());
            if (oImage != null)
            {
                txt_ImageAlt.Text = oImage.Alt_UI;
                txt_ImageTitle.Text = oImage.Title_UI;
            }
            // If there is a thumbnail image for this page
            oImage = oSessionDestinationPage.ImagesContainer.FindFirstOrDefault(img => img.ImageType_Value == EnumHandler.ImageTypes.Thumbnail.ToString());
            if (oImage != null)
            {
                txt_ImageAlt_small.Text = oImage.Alt_UI;
                txt_ImageTitle_small.Text = oImage.Title_UI;
            }

            City city = StaticData.oCityList.Find(c => c.IataCode_Value == oSessionDestinationPage.DestinationIataCode_Value);
            if (city != null)
            {
                txt_CityName.Text = city.Name_UI;
            }

        }

        /// <summary>
        /// A method that changes the title of the page to edit.
        /// </summary>
        private void ChangePageTitle()
        {
            //Change the title of the page
            lbl_PageTitle.Text = string.Format("Edit \"{0}\" Page", oSessionDestinationPage.Name_UI);
        }

        /// <summary>
        /// A method that binds all the templates related to the destination page to the templates repeater
        /// </summary>
        private void BindTemplatesToRepeater()
        {
            drp_TempaltesTable.DataSource = oSessionDestinationPage.Templates.DataSource;
            drp_TempaltesTable.DataBind();
        }

        /// <summary>
        /// Setting the extra editing panels to be visible or hidden.
        /// </summary>
        /// <param name="isAvailable">Enter 'true' for visible, or 'false' for hidden</param>
        private void EditingPanelsAvailable(bool isAvailable)
        {
            string displayValue = isAvailable ? "normal" : "none";
            SeoPanel.Style.Add("display", displayValue);
            templatesPanel.Style.Add("display", displayValue);
        }

        /// <summary>
        /// A method that bind the data to the Currency drop down list.
        /// </summary>
        private void SetCurrencyDdl()
        {
            // Filling the currency DropDownList
            ddl_Currency.DataSource = oSessionCurrencies.DataSource;
            ddl_Currency.DataTextField = "CodeAndSign_UI";
            ddl_Currency.DataValueField = "Code_UI";
            ddl_Currency.DataBind();

            ddl_PriceCurrency.DataSource = oSessionCurrencies.DataSource;
            ddl_PriceCurrency.DataTextField = "CodeAndSign_UI";
            ddl_PriceCurrency.DataValueField = "Code_UI";
            ddl_PriceCurrency.DataBind();
        }

        /// <summary>
        /// This method should be used when there is already a destination page in the database (and in the session), 
        /// in order to fill the fields on the edit page with data from the database, and bind the templates table to the DB.
        /// </summary>
        private void FillAllData()
        {
            FillGeneralPanel();
            FillSeoPanel();
            ChangePageTitle();
            BindTemplatesToRepeater();
        }

        /// <summary>
        /// A method that verifies if it the entered friendly url is different then the current one, and taken
        /// </summary>
        /// <param name="oSeo">The seo to test on.</param>
        /// <returns>True - the url is taekn (not the same) and can't be used again, or false otherwise.</returns>
        private bool IsFriendlyUrlTaken(Seo oSeo = null)
        {
            if (oSeo == null)
            {
                if (SeoContainer.IsFriendlyUrlTaken(txt_FriendlyUrl.Text, 1)) // TODO: change the 1 to real whilte label
                {
                    // if the firendly url is taken
                    lbl_FriendlyUrlStatus.Text = "Error: Please select another friendly url";
                    return true;
                }
                else
                {
                    return false;
                }
            }
            string currentFriendlyUrl = oSeo.FriendlyUrl;
            if (txt_FriendlyUrl.Text == currentFriendlyUrl)
            {
                // if the new url is the same as the current one, it is allowed.
                oSeo.FriendlyUrl = txt_FriendlyUrl.Text;
                return false;
            }
            else
            {
                if (SeoContainer.IsFriendlyUrlTaken(txt_FriendlyUrl.Text, 1)) // TODO: change the 1 to real whilte label
                {
                    // if the firendly url is taken
                    lbl_FriendlyUrlStatus.Text = "Error: Please select another friendly url";
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        #endregion

    }
}