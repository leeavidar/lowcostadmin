﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class AirlineFaresIndex : BasePage_UI
    {
        #region Session Private fields

        private AirlineFareContainer _oSessionAirlineFareContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<AirlineFareContainer>(out _oSessionAirlineFareContainer) == false)
            {
                //take the data from the table in the database
                _oSessionAirlineFareContainer = GetAirlineFaresFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<AirlineFareContainer>(oSessionAirlineFareContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<AirlineFareContainer>(oSessionAirlineFareContainer);

        }


        /// <summary>
        /// A method that gets all the airline fares of a white label from the database.
        /// </summary>
        /// <returns></returns>
        private AirlineFareContainer GetAirlineFaresFromDB()
        {
            //selecting all the destination pages with a given white label
            AirlineFareContainer allAirlineFares = BL_LowCost.AirlineFareContainer.SelectAllAirlineFares(1, null).SortByKeyContainer(DL_Generic.KeyValuesType.KeyAirlineFareAirlineCode, false);  //TODO:  replace with "WhiteLabelId"
            return allAirlineFares;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion



    }

    public partial class AirlineFaresIndex : BasePage_UI
    {
        #region Prop
        public AirlineFareContainer oSessionAirlineFareContainer { get { return _oSessionAirlineFareContainer; } set { _oSessionAirlineFareContainer = value; } }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            // Handle the permission for this page (if no permissions, redirect to the Login page).
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);

            if (!IsPostBack)
            {
                BindDataToRepeater();
                // Hide the edit panel
                AirlineFarePanel.Style.Add("display", "none");
                FillDDL();
            }
        }

        private void FillDDL()
        {
            ddl_Airline.DataSource = StaticData.oAirlineList;
            ddl_Airline.DataTextField = "Name_UI";
            ddl_Airline.DataValueField = "IataCode_UI";
            ddl_Airline.DataBind();
            ddl_Airline.Items.Insert(0, new ListItem() { Text = "Choose Airline", Value = "" });

        }
        #region Event Handlers

        protected void btn_edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the airline fare to edit from the command argument of the button
            int airlineFareDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(airlineFareDocId))
            {
                AirlineFare oAirlineFare = oSessionAirlineFareContainer.SelectByID(airlineFareDocId, 1).Single;
                if (oAirlineFare != null)
                {
                    #region Fill the admin panel with data
                    ddl_Airline.SelectedValue = oAirlineFare.AirlineCode_UI;
                    txt_FareName.Text = oAirlineFare.Name_UI;
                    txt_Flexible.Text = oAirlineFare.Flexible_UI;
                    txt_Standard.Text = oAirlineFare.Standard_UI;
                    #endregion

                    // Displaying the SaveEdit button (and hiding the other one)
                    btn_SaveNew.Style.Add("display", "none");
                    btn_SaveEdit.Style.Add("display", "normal");
                    // Unhide the client panel for editing
                    AirlineFarePanel.Style.Add("display", "normal");

                    // Setting the command argument of the saveedit button to be the docId of the airline fare.
                    btn_SaveEdit.CommandArgument = airlineFareDocId.ToString();
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewFare.Value = "Cancel";
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_SaveNew_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                // Re-open the edit panel (because the data that was enterd is invalid), and needs to be corrected before sebding it again)
                AirlineFarePanel.Style.Add("display", "normal");
                btn_AddNewFare.Value = "Cancel";
                return;
            }
            Button saveButton = (Button)sender;

            AirlineFare oAirlineFare = new AirlineFare()
            {
                #region Object initializer
                AirlineCode = ddl_Airline.SelectedValue,
                Name = txt_FareName.Text,
                Standard = txt_Standard.Text,
                Flexible = txt_Flexible.Text
                #endregion
            };

            // Insert the new object to the DB
            int result = oAirlineFare.Action(DB_Actions.Insert);

            // Get the complete table with the new object from the DB
            oSessionAirlineFareContainer = GetAirlineFaresFromDB();
            BindDataToRepeater();
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            // The command argument of this button is set when the user clicks on the 'edit' button of a row in the repeater. (in method: "btn_Edit_Click" ) 
            Button saveButton = (Button)sender;
            int fareDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(fareDocId))
            {
                AirlineFare FareToEdit = oSessionAirlineFareContainer.SelectByID(fareDocId).Single;
                if (FareToEdit != null)
                {
                    #region Fill Airline Fare properties
                    FareToEdit.AirlineCode = ddl_Airline.SelectedValue;
                    FareToEdit.Name = txt_FareName.Text;
                    FareToEdit.Standard = txt_Standard.Text;
                    FareToEdit.Flexible = txt_Flexible.Text;
                    #endregion

                    // Update the object in the DB
                    FareToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the text on the button that shows the panel 
                    AirlineFarePanel.Style.Add("display", "none");
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewFare.Value = "Add new Fare";

                    //Refresh the repeater to include the new data.
                    oSessionAirlineFareContainer = GetAirlineFaresFromDB();
                    BindDataToRepeater();

                    AddScript("FareUpdateSucesss();");
                }
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the user from the command argument of the button
            int fareDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(fareDocId))
            {
                AirlineFare oAirlineFare = oSessionAirlineFareContainer.SelectByID(fareDocId).Single;
                if (oAirlineFare != null)
                {
                    oAirlineFare.Action(DB_Actions.Delete);
                  
                    // It's imoprtant to hide the admin panel using display:none (and not visible=false) in to be able to display it back in javascript.
                    AirlineFarePanel.Style.Add("display", "none");
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewFare.Value = "Add new Fare";
                    // Refresh the repeater
                    oSessionAirlineFareContainer = GetAirlineFaresFromDB();
                    BindDataToRepeater();
                }
            }
        }


        #endregion

        #region Helping Methods

        /// <summary>
        /// Binding the repeater of airline fares to the table from the database
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_AirlineFares.DataSource = oSessionAirlineFareContainer.DataSource;
            drp_AirlineFares.DataBind();
        }

        #endregion
    }
}