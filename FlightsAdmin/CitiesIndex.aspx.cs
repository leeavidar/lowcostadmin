﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class CitiesIndex : BasePage_UI
    {
        #region Session Private fields

        private CityContainer _oSessionCityContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<CityContainer>(out _oSessionCityContainer) == false)
            {
                //take the data from the table in the database
                _oSessionCityContainer = GetCitiesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<CityContainer>(oSessionCityContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<CityContainer>(oSessionCityContainer);

        }


        /// <summary>
        /// A method that gets all the Cities from the database.
        /// </summary>
        /// <returns></returns>
        private CityContainer GetCitiesFromDB()
        {
            //selecting all the cities
            CityContainer allCities = BL_LowCost.CityContainer.SelectAllCitys(null);
            return allCities;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }


    public partial class CitiesIndex : BasePage_UI
    {
        #region Properties
        public CityContainer oSessionCityContainer { get { return _oSessionCityContainer; } set { _oSessionCityContainer = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                CityPanel.Style.Add("display", "none");
            }
            SetPage();
        }

        /// <summary>
        /// Sets all the data on the page
        /// </summary>
        private void SetPage()
        {
            BindDataToRepeater();
        }

        #region Event Handlers

        protected void btn_Save_New_Click(object sender, EventArgs e)
        {
            // Check validations
            if (!IsValid)
            {
                return;
            }
            #region Getting the values from the fields
            string iataCode = txt_IataCode.Text;
            string name = txt_Name.Text;
            string englishName = txt_NameEng.Text;
            string countryCode = txt_CountryCode.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            City oCity = new City()
            {
                IataCode = iataCode,
                Name = name,
                EnglishName = englishName,
                CountryCode = countryCode,
                IsActive = isActive
            };
            // Save the new city to the DB, and update the session
            oCity.Action(DB_Actions.Insert);
            
            oSessionCityContainer = GetCitiesFromDB();
            BindDataToRepeater();
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string iataCode = txt_IataCode.Text;
            string name = txt_Name.Text;
            string englishName = txt_NameEng.Text;
            string countryCode = txt_CountryCode.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            Button saveButton = (Button)sender;
            int cityDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(cityDocId))
            {
                var oCityContainer = CityContainer.SelectByID(cityDocId, null);

                if (oCityContainer != null && oCityContainer.Count > 0)
                {
                    City cityToEdit = oCityContainer.Single;
                    #region Fill city properties
                    cityToEdit.IataCode = iataCode;
                    cityToEdit.Name = name;
                    cityToEdit.EnglishName = englishName;
                    cityToEdit.CountryCode = countryCode;
                    cityToEdit.IsActive = isActive;
                    #endregion

                    cityToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the button that shows the panel.
                    CityPanel.Style.Add("display", "none");
                    btn_AddNewCity.Value = "Add a new City";

                    oSessionCityContainer = GetCitiesFromDB();
                    BindDataToRepeater();
                }
            }
        }

        /// <summary>
        /// Fill the edit panel  with data from the city in the session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the Tip to edit from the command argument of the button
            int cityDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(cityDocId))
            {
                var oCityContainer = oSessionCityContainer.SelectByID(cityDocId);
                if (oCityContainer != null)
                {
                    City oCity = oCityContainer.Single;
                    if (oCity != null)
                    {
                        #region Fill the tip panel with data.
                        txt_IataCode.Text = oCity.IataCode_UI;
                        txt_Name.Text = oCity.Name_UI;
                        txt_NameEng.Text = oCity.EnglishName_UI; 
                        txt_CountryCode.Text = oCity.CountryCode_UI;
                        cb_IsActive.Checked = oCity.IsActive_Value;
                        #endregion

                        // Displaying the SaveEdit button (and hiding the other one)
                        btn_SaveNew.Style.Add("display", "none");
                        btn_SaveEdit.Style.Add("display", "normal");
                        // Unhide the city panel for editing
                        CityPanel.Style.Add("display", "normal");
                        btn_SaveEdit.CommandArgument = cityDocId.ToString();
                        btn_AddNewCity.Value = "Cancel";
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int cityDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(cityDocId))
            {
                CityContainer oCityContainer = oSessionCityContainer.SelectByID(cityDocId);
                if (oCityContainer != null)
                {
                    City oCity = oCityContainer.Single;
                    if (oCity != null)
                    {
                        oCity.Action(DB_Actions.Delete);
                        oSessionCityContainer = GetCitiesFromDB();
                        BindDataToRepeater();
                    }
                }
            }
        }

        #endregion

        #region Helping methods
        private void BindDataToRepeater()
        {
            drp_Cities.DataSource = oSessionCityContainer.DataSource;
            drp_Cities.DataBind();
        }

        /// <summary>
        /// Set the panel for a new entry.
        /// </summary>
        private void ClearCityPanel()
        {
            txt_IataCode.Text = "";
            txt_Name.Text = "";
            txt_NameEng.Text = "";
            txt_CountryCode.Text = "";
            cb_IsActive.Checked = true;
        }

        #endregion
    }
}