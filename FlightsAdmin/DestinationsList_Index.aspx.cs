﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class DestinationsList_Index : BasePage_UI
    {
        #region Prop
        #region Private
        PopularDestinationContainer _oSessionPopularDestinationContainer;
        #endregion
        #endregion

        #region Session Methods
        internal override void LoadFromSession(bool isPostBack)
        {

            if (!isPostBack || SessionManager.GetSession<PopularDestinationContainer>(out _oSessionPopularDestinationContainer) == false)
            {
                //take the data from the table in the database
                _oSessionPopularDestinationContainer = GetDestinatiosFromDB();
            }
        }
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            SessionManager.SetSession<PopularDestinationContainer>(oSessionPopularDestinationContainer);
        }
        internal override void resetAllSessions()
        {
            SessionManager.ClearSession<PopularDestinationContainer>(oSessionPopularDestinationContainer);
        }

        private PopularDestinationContainer GetDestinatiosFromDB()
        {
            //selecting all the destination pages with a given white label
            PopularDestinationContainer allDestinations = PopularDestinationContainer.SelectAllPopularDestinations(1, null);  //TODO:  replace with "WhiteLabelId"
            return allDestinations;
        }
        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class DestinationsList_Index : BasePage_UI
    {
        #region Prop
        public PopularDestinationContainer oSessionPopularDestinationContainer { get { return _oSessionPopularDestinationContainer; } set { _oSessionPopularDestinationContainer = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            // Handle the permission for this page (if no permissions, redirect to the Login page).
            this.Master.PagePermissions(EnumHandler.UserRoles.Admin);

            if (!IsPostBack)
            {
                BindDataToRepeater();
                SetDropDownLists();
                // Hide the edit panel
                DestinationPanel.Style.Add("display", "none");
            }
        }


        /// <summary>
        /// Fill the sub box panel with data from the sub box in the session,
        /// and display the edit panel (the actual saving is in another event handler)
        /// </summary>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the admin to edit from the command argument of the button
            int destinationDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(destinationDocId))
            {
                PopularDestination oPopularDestination = oSessionPopularDestinationContainer.SelectByID(destinationDocId).Single;
                if (oPopularDestination != null)
                {
                    #region Fill the destination edit panel with data

                    txt_Name.Text = oPopularDestination.Name_UI;
                    txt_IataCode.Text = oPopularDestination.IataCode_UI;
                    txt_CityIataCode.Text = oPopularDestination.CityIataCode_UI;

                    // Make username editing disabled, and disable the username validators
                    // val_req_UserName.Enabled = false;
                    // val_UserNameExist.Enabled = false;
                    // txt_UserName.Enabled = false;

                    // txt_UserName.ControlStyle.Reset();
                    // txt_UserName.ControlStyle.CssClass = "form-control input-medium txt_UserName";

                    //  ddl_Roles.SelectedIndex = ddl_Roles.Items.IndexOf(ddl_Roles.Items.FindByValue(oUser.Role_Value.ToString()));
                    #endregion

                    // Displaying the SaveEdit button (and hiding the other one)
                    btn_SaveNew.Style.Add("display", "none");
                    btn_SaveEdit.Style.Add("display", "normal");
                    // Unhide the client panel for editing
                    DestinationPanel.Style.Add("display", "normal");

                    // Setting the command argument of the saveedit button to be the docId of the Destination.
                    btn_SaveEdit.CommandArgument = destinationDocId.ToString();
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewDestination.Value = "Cancel";
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_SaveNew_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                // Re-open the edit panel (because the data that was enterd is invalid), and needs to be corrected before sebding it again)
                DestinationPanel.Style.Add("display", "normal");
                btn_AddNewDestination.Value = "Cancel";
                return;
            }
            Button saveButton = (Button)sender;

            PopularDestination oPopularDestination = new PopularDestination()
            {
                Name = txt_Name.Text,
                IataCode = txt_IataCode.Text,
                CityIataCode = txt_CityIataCode.Text
            };

            // Insert the new object to the DB
            int result = oPopularDestination.Action(DB_Actions.Insert);

            // Get the complete table with the new object from the DB
            oSessionPopularDestinationContainer = GetDestinatiosFromDB();
            BindDataToRepeater();
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            // The command argument of this button is set when the user clicks on the 'edit' btton of a row in the repeater. (in method: "btn_Edit_Click" ) 
            Button saveButton = (Button)sender;
            int destinationDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(destinationDocId))
            {
                PopularDestination PopularDestinationToEdit = oSessionPopularDestinationContainer.SelectByID(destinationDocId).Single;
                if (PopularDestinationToEdit != null)
                {
                    #region Fill admin properties
                    PopularDestinationToEdit.Name = txt_Name.Text;
                    PopularDestinationToEdit.IataCode = txt_IataCode.Text;
                    PopularDestinationToEdit.CityIataCode = txt_CityIataCode.Text;
                    #endregion

                    // Update the object in the DB
                    PopularDestinationToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the text on the button that shows the panel 
                    DestinationPanel.Style.Add("display", "none");
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewDestination.Value = "Add new Destination";

                    //Refresh the repeater to include the new data.
                    oSessionPopularDestinationContainer = GetDestinatiosFromDB();
                    BindDataToRepeater();

                    AddScript("DestinationUpdateSucesss();");
                }
            }
        }


        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the user from the command argument of the button
            int destinationDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(destinationDocId))
            {
                PopularDestination oPopularDestination = oSessionPopularDestinationContainer.SelectByID(destinationDocId).Single;
                if (oPopularDestination != null)
                {
                    oPopularDestination.Action(DB_Actions.Delete);
                    // It's imoprtant to hide the admin panel using display:none (and not visible=false) in to be able to display it back in javascript.
                    DestinationPanel.Style.Add("display", "none");
                    // IMPORTANT: the script for this button is based on this text!
                    btn_AddNewDestination.Value = "Add new Destination";
                    // Refresh the repeater
                    oSessionPopularDestinationContainer = GetDestinatiosFromDB();
                    BindDataToRepeater();
                }
            }
        }


        #region Helping methods

        /// <summary>
        /// Binding the drop down list of roles to the roles enum.
        /// </summary>
        private void SetDropDownLists()
        {
            //ddl_Roles.DataSource = EnumUtil.GetEnumToListItems<EnumHandler.UserRoles>();
            //ddl_Roles.DataValueField = "Value";
            //ddl_Roles.DataTextField = "Text";
            //ddl_Roles.DataBind();

            //// Remove the cief admin option from the list
            //ddl_Roles.Items.Remove(ddl_Roles.Items.FindByValue("0"));

            ///* if the logged user is an admin, remove the admin option from the list 
            //(users of role "Chief Admin" will see the admin in the DDL, and other users can't enter this page) */
            //Users _AdminObject = SessionManager.GetAdminFromSession();
            //if (_AdminObject.Role_Value == (int)EnumHandler.UserRoles.Admin)
            //{
            //    ddl_Roles.Items.Remove(ddl_Roles.Items.FindByValue("1"));
            //}
        }

        /// <summary>
        /// Binding the repeater of admins to the admins table from the database
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_Destinations.DataSource = oSessionPopularDestinationContainer.DataSource;
            drp_Destinations.DataBind();
        }


        #endregion


    }
}