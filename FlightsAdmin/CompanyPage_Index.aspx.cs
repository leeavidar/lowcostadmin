﻿using BL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DL_LowCost;
using DL_Generic;
using Generic;

namespace FlightsAdmin
{
    public partial class CompanyPage_Index : BasePage_UI
    {
        #region Session Private fields

        private CompanyPageContainer _oSessionCompanyPages;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<CompanyPageContainer>(out _oSessionCompanyPages) == false)
            {
                //take the data from the table in the database
                _oSessionCompanyPages = GetCompanyPagesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<CompanyPageContainer>(oSessionCompanyPages);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<CompanyPageContainer>(oSessionCompanyPages);

        }


        /// <summary>
        /// A method that gets all the destination pages of a white label from the database.
        /// </summary>
        /// <returns></returns>
        private CompanyPageContainer GetCompanyPagesFromDB()
        {
            //selecting all the destination pages with a given white label
            CompanyPageContainer allCompanies = BL_LowCost.CompanyPageContainer.SelectAllCompanyPages(1, null);  //TODO:  replace with "WhiteLabelId"
            return allCompanies;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class CompanyPage_Index : BasePage_UI
    {
        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public CompanyPageContainer oSessionCompanyPages { get { return _oSessionCompanyPages; } set { _oSessionCompanyPages = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            // Handle the permission for this page (if no permissions, redirect to the Login page).
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);

            // Bind the data to the repeater
            BindDataToRepeater();
        }

   
        #region Event handlers

        protected void btn_AddNewCompany_Click(object sender, EventArgs e)
        {
            Response.Redirect(StaticStrings.path_CompanyPageEdit);
        }

        protected void btn_edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id))
            {
                // Redirecting to the edit page, with the id of the page to edit.
                Response.Redirect(StaticStrings.path_CompanyPageEdit+ "?id=" + id.ToString());
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id))
            {
                CompanyPageContainer oCompanyPageContainer = oSessionCompanyPages.SelectByID(id, 1);   // TODO: change the 1 to a real white label
                if (oCompanyPageContainer != null && oCompanyPageContainer.Count > 0)
                {
                    CompanyPage oCompanyPage = oCompanyPageContainer.Single;
                 
                    if (oCompanyPage != null)
                    {
                        // Delete the SEO object (to release the friendly url for use).
                        if (oCompanyPage.SeoDocId != null)
                        {
                           oCompanyPage.SeoSingle.Action(DB_Actions.Delete);
                        }
                        
                        oCompanyPage.Action(DB_Actions.Delete);

                        //Refresh the page to remove the page from the repeater, and to refresh the session
                        oSessionCompanyPages = GetCompanyPagesFromDB();
                        BindDataToRepeater();

                        AddScript("DeletedAlert();");
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }


        #endregion


        #region Extra Methods

        /// <summary>
        /// A method that bind the repeater of destinatio pages to the table in the database.
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_Companies.DataSource = oSessionCompanyPages.DataSource;
            drp_Companies.DataBind();
        }
        #endregion

    }
}