﻿using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightsAdmin
{
    public class StaticData
    {
        #region Properties
        //A propert holding a collection of all the airports (used by the autocomplete)
        public static AirportContainer oAirportsList { get; set; }
        //A propert holding a collection of all the cities (used by the autocomplete)
        public static List<City> oCityList { get; set; }
        //A propert holding a collection of all the airlines
        public static List<Airline> oAirlineList { get; set; }
        #endregion

        /// <summary>
        /// A static constructor that filles the properties (containers of Airports and Cities), to be used by the autocomplete in the search engine.
        /// This method is being called in the Global.asax file.
        /// </summary>
        static StaticData()
        {
            //Load the data from the service
            StaticData.oAirportsList = GetAirports();
            StaticData.oCityList = GetCities();
            StaticData.oAirlineList = GetAirlines();
        }

        /// <summary>
        /// This method gets a list of airports IATA codes.
        /// </summary>
        /// <returns>A list of all airports IATA codes</returns>
        public static AirportContainer GetAirports()
        {
            AirportContainer oAirportContainer = AirportContainer.SelectAllAirports(true);
            oAirportContainer = oAirportContainer.FindAllContainer(airport => airport._is_active == true);
            return oAirportContainer;
        }

        /// <summary>
        /// This method gets a list of all the cities IATA codes.
        /// </summary>
        /// <returns>A list of all the cities IATA codes</returns>
        public static List<City> GetCities()
        {
            return CityContainer.SelectAllCitys(true).DataSource;
        }

        /// <summary>
        /// This method gets a list of all the Airlines
        /// </summary>
        /// <returns>A list of all the cities IATA codes</returns>
        public static List<Airline> GetAirlines()
        {
            return AirlineContainer.SelectAllAirlines(null).DataSource;
        }



    }
}