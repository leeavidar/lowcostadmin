﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class AirportsIndex : BasePage_UI
    {
        #region Session Private fields

        private AirportContainer _oSessionAirportContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<AirportContainer>(out _oSessionAirportContainer) == false)
            {
                //take the data from the table in the database
                _oSessionAirportContainer = GetAirportsFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<AirportContainer>(oSessionAirportContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<AirportContainer>(oSessionAirportContainer);

        }


        /// <summary>
        /// A method that gets all the Airports from the database.
        /// </summary>
        /// <returns></returns>
        private AirportContainer GetAirportsFromDB()
        {
            //selecting all the airports
            AirportContainer allAirports = BL_LowCost.AirportContainer.SelectAllAirports(null);
            return allAirports;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }


    public partial class AirportsIndex : BasePage_UI
    {
        #region Properties
        public AirportContainer oSessionAirportContainer { get { return _oSessionAirportContainer; } set { _oSessionAirportContainer = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                AirportPanel.Style.Add("display", "none");
            }
            SetPage();
        }

        /// <summary>
        /// Sets all the data on the page
        /// </summary>
        private void SetPage()
        {
            //  BindDataToRepeater();
        }

        #region Event Handlers

        protected void btn_Save_New_Click(object sender, EventArgs e)
        {
            // Check validations
            if (!IsValid)
            {
                return;
            }
            #region Getting the values from the fields
            string iataCode = txt_IataCode.Text;
            string hebrewName = txt_HebrewName.Text;
            string englishName = txt_EnglishName.Text;
            string cityCode = txt_CityCode.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            Airport oAirport = new Airport()
            {
                IataCode = iataCode,
                EnglishName = englishName,
                NameByLang = hebrewName,
                CityIataCode = cityCode,
                IsActive = isActive
            };
            // Save the new city to the DB, and update the session
            oAirport.Action(DB_Actions.Insert);
            oSessionAirportContainer = GetAirportsFromDB();
           
            BindDataToRepeater(null, oAirport.EnglishName_UI);
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string iataCode = txt_IataCode.Text;
            string hebrewName = txt_HebrewName.Text;
            string englishName = txt_EnglishName.Text;
            string cityCode = txt_CityCode.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            Button saveButton = (Button)sender;
            int cityDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(cityDocId))
            {
                var oAirportContainer = AirportContainer.SelectByID(cityDocId, null);

                if (oAirportContainer != null && oAirportContainer.Count > 0)
                {
                    Airport cityToEdit = oAirportContainer.Single;
                    #region Fill city properties
                    cityToEdit.IataCode = iataCode;
                    cityToEdit.EnglishName = englishName;
                    cityToEdit.NameByLang = hebrewName;
                    cityToEdit.CityIataCode = cityCode;
                    cityToEdit.IsActive = isActive;
                    #endregion

                    cityToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the button that shows the panel.
                    AirportPanel.Style.Add("display", "none");
                    btn_AddNewAirport.Value = "Add a new Airport";

                    oSessionAirportContainer = GetAirportsFromDB();
                    BindDataToRepeater(null, txt_SearchTerm.Value);
                }
            }
        }

        /// <summary>
        /// Fill the edit panel  with data from the city in the session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the Tip to edit from the command argument of the button
            int cityDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(cityDocId))
            {
                var oAirportContainer = oSessionAirportContainer.SelectByID(cityDocId);
                if (oAirportContainer != null)
                {
                    Airport oAirport = oAirportContainer.Single;
                    if (oAirport != null)
                    {
                        #region Fill the tip panel with data.
                        txt_IataCode.Text = oAirport.IataCode_UI;
                        txt_HebrewName.Text = oAirport.NameByLang_UI;
                        txt_EnglishName.Text = oAirport.EnglishName_UI;
                        txt_CityCode.Text = oAirport.CityIataCode_UI;
                        cb_IsActive.Checked = oAirport.IsActive_Value;
                        #endregion

                        // Displaying the SaveEdit button (and hiding the other one)
                        btn_SaveNew.Style.Add("display", "none");
                        btn_SaveEdit.Style.Add("display", "normal");
                        // Unhide the city panel for editing
                        AirportPanel.Style.Add("display", "normal");
                        btn_SaveEdit.CommandArgument = cityDocId.ToString();
                        btn_AddNewAirport.Value = "Cancel";
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int cityDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(cityDocId))
            {
                AirportContainer oAirportContainer = oSessionAirportContainer.SelectByID(cityDocId);
                if (oAirportContainer != null)
                {
                    Airport oAirport = oAirportContainer.Single;
                    if (oAirport != null)
                    {
                        oAirport.Action(DB_Actions.Delete);
                        oSessionAirportContainer = GetAirportsFromDB();
                        BindDataToRepeater(null,null,true);
                    }
                }
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            // When clicking the search button

            // get the search term:
            var term = txt_SearchTerm.Value.ToLower();

            if (term.Length >= 2)
            {
                // search for the term in the session object 
                var searchResults = GetSearchResults(term);

                // fill the repeater with the cintainer of matching results
                BindDataToRepeater(searchResults);

                // fire the javascript function that inits the table.
                AddScript("AfterUpdate()");
            }
            else
            {
                // there must be at least 2 characters for he search to not return to many results.
                return;
            }

        }

       
        #endregion

        #region Helping methods
        private void BindDataToRepeater(AirportContainer oAirportContainer = null, string searchTerm = null, bool IsEmpty = false)
        {
            if (IsEmpty == true)
            {
                drp_Airports.DataSource = new AirportContainer().DataSource;
            }
            else if (oAirportContainer != null)
            {
                drp_Airports.DataSource = oAirportContainer.DataSource;
            }
            else if (searchTerm != null && searchTerm.Length >= 2)
            {
              var searchResults = GetSearchResults(searchTerm);
              drp_Airports.DataSource = searchResults.DataSource;
            }
            else
            {
                drp_Airports.DataSource = oSessionAirportContainer.DataSource;
            }
            drp_Airports.DataBind();
        }

        /// <summary>
        /// A function that gets all matching airports to the search term provided.
        /// </summary>
        /// <param name="term">A string as a search term</param>
        /// <returns>All the results that containe the search term (in the code, name, city name etc.)</returns>
        private AirportContainer GetSearchResults(string term)
        {
            // search for the term in the session object 
            AirportContainer searchResults = AirportContainer.SelectAllAirports(null).FindAllContainer(
                a => a.IataCode_UI.ToLower().Contains(term) ||
                a.NameByLang_UI.ToLower().Contains(term) ||
                a.EnglishName_UI.ToLower().Contains(term) ||
                a.CityIataCode_UI.ToLower().Contains(term) ||
                (a.CitySingle != null && a.CitySingle.Name_UI.ToLower().Contains(term))
                );

            // return the results
            return searchResults;
        }

        /// <summary>
        /// Set the panel for a new entry.
        /// </summary>
        private void ClearAirportPanel()
        {
            txt_IataCode.Text = "";
            txt_EnglishName.Text = "";
            txt_HebrewName.Text = "";
            txt_CityCode.Text = "";
            cb_IsActive.Checked = true;
        }

        #endregion
    }
}