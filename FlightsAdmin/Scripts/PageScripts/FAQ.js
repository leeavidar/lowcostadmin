﻿// ------------------------------START:  FAQ PAGE --------------------------------------------------------


// Binding methods to events (on document ready):
$(function () {
    // Binding the button that show the hidden div for new faqs to it's function
    //$(".btn_AddNewFaq").click(ShowNewFaqDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new faq panel
function ShowNewFaqDiv() {

    // btn_AddNewFaq button optional strings:
    var newFaq = "Add Question and Answer";
    var cancel = "Cancel";

    // First clear all the fields
    ClearFaqPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewFaq").val() == newFaq) {
        //Change the action of the save button to add a new faq
        $(".FaqPanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewFaq").val(cancel);
    }
    else if ($(".btn_AddNewFaq").val() == cancel) {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add Question and Answer"
        $(".FaqPanel").slideUp(300);
        $(".btn_AddNewFaq").val(newFaq);
    }
}



// A method that clears all the fields of the tip panel.
function ClearFaqPanel() {
    $(".txt_Question").val('');
    $(".txt_Answer").val('');
}




// ------------------------------END:  FAQ PAGE ----------------------------------------------------------