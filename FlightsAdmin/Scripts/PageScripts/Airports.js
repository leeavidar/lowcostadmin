﻿// ------------------------------START:  AIRPORTS PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {
    // Binding the button that show the hidden div for new airport to it's function
    $(".btn_AddNewAirport").click(ShowNewAirportDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new airport panel
function ShowNewAirportDiv() {
    // First clear all the fields
    ClearAirportPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewAirport").val() == "Add a new Airport") {
        //Change the action of the save button to add a new airport
        $(".AirportPanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewAirport").val('Cancel');
    }
    else if ($(".btn_AddNewAirport").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new airport"
        $(".AirportPanel").slideUp(300);
        $(".btn_AddNewAirport").val('Add a new Airport');
    }
}



// A method that clears all the fields of the airport panel.
function ClearAirportPanel() {
    $(".txt_IataCode").val('');
    $(".txt_EnglishName").val('');
    $(".txt_HebrewName").val('');
    $(".txt_CityCode").val('');
    $(".cb_IsActive").prop('checked','checked');
}


// ------------------------------END:  AIRPORTS PAGE ---------------------------------------------------------------------


