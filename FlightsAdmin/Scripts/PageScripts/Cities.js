﻿// ------------------------------START:  CITIES PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {
    // Binding the button that show the hidden div for new city to it's function
    $(".btn_AddNewCity").click(ShowNewCityDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new city panel
function ShowNewCityDiv() {
    // First clear all the fields
    ClearCityPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewCity").val() == "Add a new City") {
        //Change the action of the save button to add a new city
        $(".CityPanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewCity").val('Cancel');
    }
    else if ($(".btn_AddNewCity").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new city"
        $(".CityPanel").slideUp(300);
        $(".btn_AddNewCity").val('Add a new City');
    }
}



// A method that clears all the fields of the city panel.
function ClearCityPanel() {
    $(".txt_Code").val('');
    $(".txt_Name").val('');
    $(".txt_NameEng").val('');
    $(".txt_CountryCode").val('');
    $(".cb_IsActive").prop('checked','checked');
}

// ------------------------------END:  CITIES PAGE ---------------------------------------------------------------------


