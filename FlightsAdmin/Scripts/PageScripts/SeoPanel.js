﻿// ------------------------------START:  SEO ---------------------------------------------------------------------
// Binding methods to events (on document ready):
$(function () {

    $(".txt_FriendlyUrl").change(FriendlyUrlCheck);
    $(".txt_FriendlyUrl").load(FriendlyUrlCheck);

    ///// BIND MORE EVENTS HERE !

});


function CheckDelete() {

    var isDelete = confirm("Are You Sure You Want To Delete?");
    return isDelete;
}

/// A method that shows an alert of success
function SavedAlert() {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Notice!',
        // (string | mandatory) the text inside the notification
        text: 'Saved successfully',
        time: 5000,
        // (string | optional) the class name you want to apply directly to the notification for custom styling
        class_name: 'SuccesssAlert'
    });
}

/// A method that shows an alert of success
function DeletedAlert() {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Notice!',
        // (string | mandatory) the text inside the notification
        text: 'Deleted successfully',
        time: 5000,
        // (string | optional) the class name you want to apply directly to the notification for custom styling
        class_name: 'SuccesssAlert'
    });
}



// A method that checks in the database if a friendly url that the user types is in use, or if it's free to use.
// The mehtod sends the typed url in the queryString (as 'Url'), and changes the status label to the appropriate message.
function FriendlyUrlCheck() {
    $.ajax({
        type: "GET",
        url: "SeoHandler.ashx",
        data: "Url=" + $(".txt_FriendlyUrl").val(),
        cache: false,
        success: function (result) {

            switch (result) {
                case "taken":
                    $(".lbl_FriendlyUrlStatus").text("Status: This URL is already in use. Try another one.").css('color', 'red');
                    $(".txt_FriendlyUrl").attr("data-valid", "false");
                    break;
                case "free":
                    $(".lbl_FriendlyUrlStatus").text("Status: This URL is available!").css('color', 'green');
                    $(".txt_FriendlyUrl").attr("data-valid", "true");
                    break;
                case "none":
                    $(".lbl_FriendlyUrlStatus").text("Status: ...").css('color', '');
                    $(".txt_FriendlyUrl").attr("data-valid", "true");
                    break;
                default:
                    break;
            }
        }
    });
}

function CheckValidation() {
    if ($(".txt_FriendlyUrl").data('valid') == false) {
        return false;
    }
    return true;
}

// ------------------------------END:  SEO -----------------------------------------------------------------------
