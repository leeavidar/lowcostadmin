﻿// ------------------------------START:  TIPS PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {
    // Binding the button that show the hidden div for new tips to it's function
    $(".btn_AddNewTip").click(ShowNewTipDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new tip panel
function ShowNewTipDiv() {
    // First clear all the fields
    ClearTipPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewTip").val() == "Add a new Tip") {
        //Change the action of the save button to add a new tip
        $(".TipPanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewTip").val('Cancel');
    }
    else if ($(".btn_AddNewTip").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new Tip"
        $(".TipPanel").slideUp(300);
        $(".btn_AddNewTip").val('Add a new Tip');
    }
}



// A method that clears all the fields of the tip panel.
function ClearTipPanel() {
    $(".txt_Title").val('');
    $(".txt_ShortText").val('');
    $(".txt_LongText").val('');
    $(".txt_Order").val('1');
    $(".txt_Date").val("");

}

// ------------------------------END:  TIPS PAGE ---------------------------------------------------------------------


