﻿// ------------------------------START:  DestinationS PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {

    // Binding the button that show the hidden div for new tips to it's function
    $(".btn_AddNewDestination").click(ShowNewDEstinationDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new destination panel
function ShowNewDEstinationDiv() {
    // First clear all the fields
    ClearDestinationPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewDestination").val() == "Add new Destination") {
        //Change the action of the save button to add a new tip
        $(".DestinationPanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewDestination").val('Cancel');
    }
    else if ($(".btn_AddNewDestination").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new Tip"
        $(".DestinationPanel").slideUp(300);
        $(".btn_AddNewDestination").val('Add new Destination');
    }
}

// A method that clears all the fields of the Destination panel.
function ClearDestinationPanel() {
   //  $('.txt_UserName').removeProp('disabled');
    $(".txt_Name").val('');
    $(".txt_IataCode").val('');
    $(".txt_CityIataCode").val('');
}

/// A method that shows an alert of success
function DestinationUpdateSucesss() {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Notice!',
        // (string | mandatory) the text inside the notification
        text: 'The user was updates successfully',
        time: 5000,
        // (string | optional) the class name you want to apply directly to the notification for custom styling
        class_name: 'SuccesssAlert'
    });
}

// A function that toggles the edit panel
function CloseEditPanel(e) {
    e.preventDefault();
    $(".DestinationPanel").slideToggle();
}


// ------------------------------END:  Destination PAGE ---------------------------------------------------------------------