﻿// ------------------------------START:  ADMINS PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {

    // Binding the button that show the hidden div for new tips to it's function
    $(".btn_AddNewAdmin").click(ShowNewAdminDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new tip panel
function ShowNewAdminDiv() {
    // First clear all the fields
    ClearAdminPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewAdmin").val() == "Add new Admin") {
        //Change the action of the save button to add a new tip
        $(".AdminPanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewAdmin").val('Cancel');
    }
    else if ($(".btn_AddNewAdmin").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new Tip"
        $(".AdminPanel").slideUp(300);
        $(".btn_AddNewAdmin").val('Add new Admin');
    }
}

// A method that clears all the fields of the admin panel.
function ClearAdminPanel() {
    $('.txt_UserName').removeProp('disabled');
    $(".txt_UserName").val('');
    $(".txt_FirstName").val('');
    $(".txt_LastName").val('');
    $(".txt_Password").val('');
    $(".txt_Email").val("");

}

/// A method that shows an alert of success
function UserUpdateSucesss() {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Notice!',
        // (string | mandatory) the text inside the notification
        text: 'The user was updates successfully',
        time: 5000,
        // (string | optional) the class name you want to apply directly to the notification for custom styling
        class_name: 'SuccesssAlert'
    });
}

// A function that toggles the edit panel
function CloseEditPanel(e) {
    e.preventDefault();
    $(".AdminPanel").slideToggle();
}


// ------------------------------END:  ADMINS PAGE ---------------------------------------------------------------------