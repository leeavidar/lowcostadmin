﻿// ------------------------------START:  AIRPORTS PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {
    // Binding the button that show the hidden div for new airline to it's function
    $(".btn_AddNewAirline").click(ShowNewAirlineDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new airline panel
function ShowNewAirlineDiv() {
    debugger;
    // First clear all the fields
    ClearAirlinePanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewAirline").val() == "Add a new Airline") {
        //Change the action of the save button to add a new airline
        $(".AirlinePanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewAirline").val('Cancel');
    }
    else if ($(".btn_AddNewAirline").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new airline"
        $(".AirlinePanel").slideUp(300);
        $(".btn_AddNewAirline").val('Add a new Airline');
    }
}



// A method that clears all the fields of the airline panel.
function ClearAirlinePanel() {
    $(".txt_IataCode").val('');
    $(".txt_EnglishName").val('');
    $(".txt_HebrewName").val('');
    $(".txt_CityCode").val('');
    $(".cb_IsActive").prop('checked','checked');
}


// ------------------------------END:  AIRPORTS PAGE ---------------------------------------------------------------------


