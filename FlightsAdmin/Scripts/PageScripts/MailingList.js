﻿// ------------------------------START:  MAILING LIST PAGE --------------------------------------------------------


// Binding methods to events (on document ready):
$(function () {
    // Binding the button that show the hidden div for new faqs to it's function
    $('#btn_ExportCSV').click(Export());

    ///// BIND MORE EVENTS HERE !
});



// A method that checks if the browser supports saving the file to CSV.
function chkBrowser() {
    var ua = navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    return M.join(' ');
};


// A method that checks the browser and saves the data only if the browser supports it.
function Generatexport(btnSave, data) {
    var uri = 'data:application/octet-stream;charset=UTF-8,' + data;
    if (chkBrowser().substring(0, 2) == 'IE') {
        SaveContents(data);
    }
    else {
        btnSave.attr('href', uri);
    }
}


// A method that saves the addresses to CSV file.
function SaveContents(data) {

    if (document.execCommand) {
        var oWin = window.open("about:blank", "_blank");
        oWin.document.write(data);
        oWin.document.close();
        var success = oWin.document.execCommand('SaveAs', true, null)
        oWin.close();
        if (!success)
            alert("Sorry, your browser does not support this feature");

    }
}

// A method that generates the string of mailing addresses, seperated by commas (for the csv file), and saves it to a file.
function Export() {
    var EmailsString = '';
    $(".storeEmail").each(function () {
        EmailsString += $(this).text() + ',';
    });

    Generatexport($('#btn_ExportCSV'), EmailsString);

    //var table = $("#sample_2");
    //table.toCSV($('#btn_ExportCSV'));


}

// ------------------------------END:  MAILING LIST PAGE --------------------------------------------------------