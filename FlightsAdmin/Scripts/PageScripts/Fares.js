﻿// ------------------------------START:  ADMINS PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {

    // Binding the button that show the hidden div for new fares to it's function
    $(".btn_AddNewFare").click(ShowNewFareDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new fare panel
function ShowNewFareDiv() {
    // First clear all the fields
    ClearFarePanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewFare").val() == "Add new Fare") {
        //Change the action of the save button to add a new tip
        $(".AirlineFarePanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewFare").val('Cancel');
    }
    else if ($(".btn_AddNewFare").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new Tip"
        $(".AirlineFarePanel").slideUp(300);
        $(".btn_AddNewFare").val('Add new Fare');
    }
}

// A method that clears all the fields of the fares panel.
function ClearFarePanel() {
    $('.ddl_Airline').val('');
    $(".txt_FareName").val('');
    $(".txt_Standard").val('');
    $(".txt_Flexible").val('');
}



/// A method that shows an alert of success
function FareUpdateSucesss() {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Notice!',
        // (string | mandatory) the text inside the notification
        text: 'The user was updates successfully',
        time: 5000,
        // (string | optional) the class name you want to apply directly to the notification for custom styling
        class_name: 'SuccesssAlert'
    });
}

// A function that toggles the edit panel
function CloseEditPanel(e) {
    e.preventDefault();
    $(".AirlineFarePanel").slideToggle();
}


// ------------------------------END:  ADMINS PAGE ---------------------------------------------------------------------