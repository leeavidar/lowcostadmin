﻿// ------------------------------START:  COUNTRIES PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {
    // Binding the button that show the hidden div for new country to it's function
    $(".btn_AddNewCountry").click(ShowNewCountryDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new country panel
function ShowNewCountryDiv() {
    // First clear all the fields
    ClearCountryPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddNewCountry").val() == "Add a new Country") {
        //Change the action of the save button to add a new country
        $(".CountryPanel").slideDown(300);
        // Change the button value to cancel
        $(".btn_AddNewCountry").val('Cancel');
    }
    else if ($(".btn_AddNewCountry").val() == "Cancel") {
        // when the value of the button is "cancel" - close the panel, and change the value back to "Add a new country"
        $(".CountryPanel").slideUp(300);
        $(".btn_AddNewCountry").val('Add a new Country');
    }
}



// A method that clears all the fields of the country panel.
function ClearCountryPanel() {
    $(".txt_Code").val('');
    $(".txt_Name").val('');
    $(".txt_NameEng").val('');
    $(".cb_IsActive").prop('checked','checked');
}

// ------------------------------END:  COUNTRIES PAGE ---------------------------------------------------------------------


