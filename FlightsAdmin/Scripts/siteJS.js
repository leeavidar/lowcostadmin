﻿// ------------------------------Start: General Function ---------------------------------------------------------------------
function CheckDelete() {

    var isDelete = confirm("Are You Sure You Want To Delete?");
    return isDelete;
}

// A method that clears the form, and resets the validators
//function CleanForm() {
//    document.forms[0].reset();
//    for (i = 0; i < Page_Validators.length; i++) {
//        Page_Validators[i].style.visibility = 'hidden';
//    }
//    return false;
//}

/// A method that shows an alert of success
function SavedAlert() {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Notice!',
        // (string | mandatory) the text inside the notification
        text: 'Saved successfully',
        time: 5000,
        // (string | optional) the class name you want to apply directly to the notification for custom styling
        class_name: 'SuccesssAlert'
    });
}

/// A method that shows an alert of success
function DeletedAlert() {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Notice!',
        // (string | mandatory) the text inside the notification
        text: 'Deleted successfully',
        time: 5000,
        // (string | optional) the class name you want to apply directly to the notification for custom styling
        class_name: 'SuccesssAlert'
    });
}



// ------------------------------End: General Function ---------------------------------------------------------------------



// ------------------------------START:  SEO ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {
   
    $(".txt_FriendlyUrl").change(FriendlyUrlCheck);
    $(".txt_FriendlyUrl").load(FriendlyUrlCheck);

    ///// BIND MORE EVENTS HERE !

});

// A method that checks in the database if a friendly url that the user types is in use, or if it's free to use.
// The mehtod sends the typed url in the queryString (as 'Url'), and changes the status label to the appropriate message.
function FriendlyUrlCheck() {
    $.ajax({
        type: "GET",
        url: "SeoHandler.ashx",
        data: "Url=" + $(".txt_FriendlyUrl").val(),
        cache: false,
        success: function (result) {

            switch (result) {
                case "taken":
                    $(".lbl_FriendlyUrlStatus").text("Status: This URL is already in use. Try another one.");
                    break;
                case "free":
                    $(".lbl_FriendlyUrlStatus").text("Status: This URL is available!");
                    break;
                case "none":
                    $(".lbl_FriendlyUrlStatus").text("Status: ...");
                    break;
                default:
                    break;
            }
        }
    });
}



// ------------------------------END:  SEO -----------------------------------------------------------------------


// ------------------------------START:  TEMPLATES PAGE ---------------------------------------------------------------------

// Binding methods to events (on document ready):
$(function () {
   
    // Binding the button that show the hidden div for new sub-boxes to it's function
    $(".btn_AddSubBox").click(ShowNewSubBoxDiv);

    ///// BIND MORE EVENTS HERE !
});

//A method that shows or hides the new-sub-box panel
function ShowNewSubBoxDiv() {
    // First clear all the fields
    ClearSubBoxPanel();
    $(".btn_SaveNew").show();
    $(".btn_SaveEdit").hide();

    if ($(".btn_AddSubBox").val() == "Add Sub Box") {
        //Change the action of the save button to add a new SubBox   
        $(".NewSubBoxDiv").slideDown(300);
        $(".btn_AddSubBox").val('Cancel');
    }
    else {
        $(".NewSubBoxDiv").slideUp(300);
        $(".btn_AddSubBox").val('Add Sub Box');
    }
}

// A method that hides (display:none) all the templates
function HideTemplates() {
    $(".template_SideWithImage").hide();
    $(".template_SideInformation").hide();
    $(".template_CenterWithImage").hide();
    $(".template_MainInformation").hide();
}

// A method that gets the selected template (the class of it's div) and show only this template.
function SelectTemplate(templateClass) {
    HideTemplates();
    $("." + templateClass).show();
}

// A method that clears all the fields of the SubBox panel.
function ClearSubBoxPanel() {
    $(".txt_SubTitle").val('');
    $(".txt_ShortText").val('');
    $(".txt_LongText").val('');
    $(".txt_Order").val('1');
}


// ------------------------------END:  TEMPLATES PAGE ---------------------------------------------------------------------



// ------------------------------START:  OrdersManagement PAGE --------------------------------------------------------


function ShowDetails(obj) {
    $('#moreDetails_' + obj.id).toggle();
    if ($('#' + obj.id).val() == '+')
        $('#' + obj.id).val('--');
    else {
        $('#' + obj.id).val('+');
    }
}

// ------------------------------END:  OrdersManagement PAGE ----------------------------------------------------------

// ------------------------------START:  DESTINATION PAGE --------------------------------------------------------
// ------------------------------END:  DESTINATION PAGE --------------------------------------------------------