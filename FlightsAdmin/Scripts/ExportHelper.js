﻿jQuery.fn.toCSV = function (btnSave) {
    var data = $(this).first(); //Only one table
    var csvData = [];
    var tmpArr = [];
    var tmpStr = '';
    data.find("tr").each(function () {
        if ($(this).find("th").length) {
            $(this).find("th").each(function () {
                tmpStr = $(this).text().replace(/"/g, '""');
                tmpArr.push('"' + tmpStr + '"');
            });
            csvData.push(tmpArr);
        } else {
            tmpArr = [];
            $(this).find("td").each(function () {
                if ($(this).text() == "Delete") {
                    // do nothing
                }
                else {
                    if ($(this).text().match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                        tmpArr.push(parseFloat($(this).text()));
                    } else {
                        tmpStr = $(this).text().replace(/"/g, '""');
                        tmpArr.push('"' + tmpStr + '"');
                    }
                }
                
            });
            csvData.push(tmpArr.join(','));
        }
    });

    var output = csvData.join('\n');
    var uri = 'data:application/csv;charset=UTF-8,' + encodeURIComponent(output);
    if (chkBrowser().substring(0, 2) == 'IE') {
        SaveContents(output);
    }
    else {
        btnSave.attr('href', uri);
    }
}



jQuery.fn.toHTML = function (btnSave) {
    var styletxt = '<style>table {width: 50%;        margin: 20px auto;        background-color: transparent;        border-spacing: 0;        border-collapse: collapse;        border-spacing: 2px;        border-color: gray;        display: table;        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;    }    th {        vertical-align: bottom;        border-bottom: 2px solid #ddd;        padding: 8px;        line-height: 1.428571429;        text-align: left;        font-weight: bold;        box-sizing: border-box;        font-size: 15px;        text-align: center;    }    thead {        display: table-header-group;        vertical-align: middle;        border-color: inherit;    }    td {        background-color: #f9f9f9;        padding: 8px;        line-height: 1.428571429;        border-top: 1px solid #ddd;        box-sizing: border-box;        font-size: 13px;        text-align: center;    }    h3 {        border-bottom: 1px solid #eee;        padding-bottom: 5px;        margin: 30px 0px 25px 0px;    }    .title_wrap {        width: 55%;        margin: 20px auto;        text-align: center;        font-size: 30px;    }    </style>    <div class="title_wrap">        <h3>title</h3>    </div>';
    var datatxt = (this).html().replace(/^\s+|\s+$/gm, '');
    var uri = 'data:application/octet-stream;charset=UTF-8,<html><body>'+escape(styletxt)+'<table>' + escape(datatxt) + '</table></body></html>';
    if (chkBrowser().substring(0, 2) == 'IE') {
        SaveContents('<html><body>'+styletxt+'<table>' + datatxt + '</table></body></html>');
    }
    else {
        btnSave.attr('href', uri);
    }
}


function chkBrowser() {
    var ua = navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    return M.join(' ');
};


function Generatexport(btnSave, data) {
    var uri = 'data:application/octet-stream;charset=UTF-8,'+data;
    if (chkBrowser().substring(0, 2) == 'IE') {
        SaveContents(data);
    }
    else {
        btnSave.attr('href', uri);
    }
}

function SaveContents(data) {

    if (document.execCommand) {
        var oWin = window.open("about:blank", "_blank");
        oWin.document.write(data);
        oWin.document.close();
        var success = oWin.document.execCommand('SaveAs', true, null)
        oWin.close();
        if (!success)
            alert("Sorry, your browser does not support this feature");

    }
}