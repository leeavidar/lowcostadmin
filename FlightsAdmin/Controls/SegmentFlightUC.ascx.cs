﻿using FlightsAdmin.FlightsSearchService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class SegmentFlightUC : UC_BasePage
    {
        #region Prop
        #region Private
        private string _departureAirportCode;
        #endregion
        #region Public
        public FlightSegment Segment { set { SetFlightSegment(value); } }

        public string ArrivalTime { get { return lbl_arrivalTime.Text; } set { lbl_arrivalTime.Text = value; } }
        public string ArrivalAirport { get { return lbl_arrivalAirport.Text; } set { lbl_arrivalAirport.Text = value; } }
        public string DepartureTime { get { return lbl_departureTime.Text; } set { lbl_departureTime.Text = value; } }
        public string DepartureAirportName { get { return lbl_departureAirport.Text; } set { lbl_departureAirport.Text = value; } }
        public string FlightCode { get { return lbl_flightCode.Text; } set { lbl_flightCode.Text = value; } }
        public string AirlineName { get { return lbl_airlineName.Text; } set { lbl_airlineName.Text = value; } }
        public string StopDetails { get { return lbl_stopDetails.Text; } set { lbl_stopDetails.Text = value; } }
        public string AirlineLogo
        {
            get { return "http://www.travelfusion.com/images/logos/" + lbl_airlineName.Text.ToLower() + ".gif"; }
        }
        public string DepartureAirportCode
        {
            get { return _departureAirportCode; }
            set { _departureAirportCode = value; }
        }
        #endregion
        #endregion
    
        protected void Page_Load(object sender, EventArgs e)
        {

        }
   
        /// <summary>
        /// Displays a description of the stop (connection) between two flight segments.
        /// </summary>
        /// <param name="stopTime">How much time of stop until the next flight leg</param>
        /// <param name="airportName">The name of te airport where the passenger will wait</param>
        /// <param name="airportCode">The code of the airport where the passenger will wait</param>
        public void displayStops(string stopTime, string airportName, string airportCode)
        {
            StopDetails = String.Format("{0} hours {1} airport ({2})", stopTime, airportName, airportCode);
            div_flightStop.Attributes["style"] = "display:normal";
        }

        /// <summary>
        /// Setting the properties (labels, images) for a Flight Segment user control.
        /// </summary>
        /// <param name="segment"> A flight segment object to take the data from</param>
        public void SetFlightSegment(FlightSegment segment)
        {
            DepartureTime = segment.DepartDateTime.ToShortTimeString();
            DepartureAirportName = string.Format("{0} ({1})", Extensions.GetAirportName(segment.DepartureAirport), segment.DepartureAirport.LocationCode);
            DepartureAirportCode = segment.DepartureAirport.LocationCode;
            ArrivalTime = segment.ArrivalDateTime.ToShortTimeString();
            ArrivalAirport = string.Format("{0} ({1})", Extensions.GetAirportName(segment.ArrivalAirport), segment.ArrivalAirport.LocationCode);
            FlightCode = segment.FlightCode;
            AirlineName = segment.OperatingAirline.Name;
            img_airlineLogo.ImageUrl = AirlineLogo;
        }
    }
}