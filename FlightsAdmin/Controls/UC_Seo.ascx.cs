﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Generic;
using DL_Generic;
using DL_LowCost;
using BL_LowCost;
using System.Web.Script.Services;
using System.Web.Services;

namespace FlightsAdmin.Controls
{
    public partial class UC_Seo : UC_BasePage
    {
        public Seo oSeo { get; set; }
        public object RelatedObject { get; set; }


        //public Type RelatedObjectType { get; set; }
    }

    public partial class UC_Seo : UC_BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (oSeo != null)
            {
                SessionManager.SetSession<Seo>(oSeo);
            }


            if (!IsPostBack)
            {
                FillSeoPanel();
            }
        }



        #region SEO Panel Events

        protected void btn_SeoSave_Click(object sender, EventArgs e)
        {
            //if (!IsValid)
            //{
            //    return;
            //}
            // Seo oSeo = oSessionDestinationPage.SeoSingle;

            if (oSeo != null)
            {
                // if there is already a SEO object for this destination page

                // the current Friendly url: 
                bool friendlyUrlTaken = IsFriendlyUrlTaken(oSeo);
                if (friendlyUrlTaken)
                {
                    return;
                }
                oSeo.FriendlyUrl = txt_FriendlyUrl.Text;
                oSeo.SeoTitle = txt_SeoTitle.Text;
                oSeo.SeoDescription = txt_SeoDescription.Text;
                oSeo.SeoKeyWords = txt_SeoMetaData.Text;
                oSeo.WhiteLabelDocId = 1; // TODO: Change to real white label

                // Update the SEO for this page in the DB
                oSeo.Action(DB_Actions.Update);

                // show a success alert
                
          // ScriptManager.RegisterStartupScript(up_SeoPanel, up_SeoPanel.GetType(), "MyKey", "SavedAlert();", true);

                (Page as BasePage_UI).AddScript("SavedAlert();");
            }
            //else
            //{
            //    // Create a new SEO for this page

            //    // first check the friendly url and if it's already in use, return (gives a message inside the function)
            //    bool friendlyUrlTaken = IsFriendlyUrlTaken();
            //    if (friendlyUrlTaken)
            //    {
            //        return;
            //    }
            //    Seo oSeoNew = new Seo()
            //    {
            //        SeoTitle = txt_SeoTitle.Text,
            //        FriendlyUrl = txt_FriendlyUrl.Text,
            //        SeoDescription = txt_SeoDescription.Text,
            //        SeoKeyWords = txt_SeoMetaData.Text,
            //        WhiteLabelDocId = 1
            //    };
            //    // Insert the new SEO to the DB
            //    oSeoNew.Action(DB_Actions.Insert);


            //    // Add the new SEO to the Destination
            //    if (RelatedObject is DestinationPage)
            //    {
            //        (RelatedObject as DestinationPage).SeoSingle = oSeoNew;
            //        (RelatedObject as DestinationPage).SeoDocId = oSeoNew.DocId_Value;
            //        (RelatedObject as DestinationPage).Action(DB_Actions.Update);
            //    }
            //    else if (RelatedObject is CompanyPage)
            //    {
            //        (RelatedObject as CompanyPage).SeoSingle = oSeoNew;
            //        (RelatedObject as CompanyPage).SeoDocId = oSeoNew.DocId_Value;
            //        (RelatedObject as CompanyPage).Action(DB_Actions.Update);
            //    }
            //    else if (RelatedObject is StaticPages)
            //    {
            //        (RelatedObject as StaticPages).SeoSingle = oSeoNew;
            //        (RelatedObject as StaticPages).SeoDocId = oSeoNew.DocId_Value;
            //        (RelatedObject as StaticPages).Action(DB_Actions.Update);
            //    }

            //    //oSessionDestinationPage.SeoSingle = oSeoNew;
            //    //oSessionDestinationPage.SeoDocId = oSeoNew.DocId_Value;
            //    //oSessionDestinationPage.Action(DB_Actions.Update);
            //}
        }

        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            
            if (RelatedObject is DestinationPage)
            {
                Response.Redirect(StaticStrings.path_DestinationIndex);
            }
            if (RelatedObject is CompanyPage)
            {
                Response.Redirect(StaticStrings.path_CompanyPageIndex);
            }
            if (RelatedObject is StaticPages)
            {
                StaticPages oPage = RelatedObject as StaticPages;

                if (oPage.Type_Value == (int)EnumHandler.PageType.Static)
                {
                    Response.Redirect(QueryStringManager.RedirectWithQuery(StaticStrings.path_StaticPageIndex, EnumHandler.QueryStrings.Type, "1"));
                }
                else if ((oPage.Type_Value == (int)EnumHandler.PageType.Dynamic))
                {
                    Response.Redirect(QueryStringManager.RedirectWithQuery(StaticStrings.path_StaticPageIndex, EnumHandler.QueryStrings.Type, "2"));
                }

            }

        }

        #region Helping methods

        /// <summary>
        /// A method that verifies if it the entered friendly url is different then the current one, and taken
        /// </summary>
        /// <param name="oSeo">The seo to test on.</param>
        /// <returns>True - the url is taekn (not the same) and can't be used again, or false otherwise.</returns>
        private bool IsFriendlyUrlTaken(Seo oSeo = null)
        {
            if (oSeo == null)
            {
                if (SeoContainer.IsFriendlyUrlTaken(txt_FriendlyUrl.Text, 1)) // TODO: change the 1 to real whilte label
                {
                    // if the firendly url is taken
                    lbl_FriendlyUrlStatus.ForeColor = System.Drawing.Color.Red;
                    lbl_FriendlyUrlStatus.Text = "Status: Please select another friendly url";
                    return true;
                }
                else
                {
                    return false;
                }
            }
            string currentFriendlyUrl = oSeo.FriendlyUrl;
            if (txt_FriendlyUrl.Text == currentFriendlyUrl)
            {
                // if the new url is the same as the current one, it is allowed.
                oSeo.FriendlyUrl = txt_FriendlyUrl.Text;
                return false;
            }
            else
            {
                if (SeoContainer.IsFriendlyUrlTaken(txt_FriendlyUrl.Text, 1)) // TODO: change the 1 to real whilte label
                {
                    // if the firendly url is taken
                    lbl_FriendlyUrlStatus.ForeColor = System.Drawing.Color.Red;
                    lbl_FriendlyUrlStatus.Text = "Status: Please select another friendly url";
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        #endregion



        #endregion


        /// <summary>
        /// A method that searches for a city name in the database, based on a given string.
        /// </summary>
        /// <param name="prefixText">The string (can be part of the city name)</param>
        /// <param name="count">How many letter to start the search from (because the method is called in a change event of a textbox)</param>
        /// <returns>A list of all matching resukts</returns>
        [ScriptMethod()]
        [WebMethod(EnableSession = true)]
        public List<string> SearchCities(string prefixText, int count)
        {
            CityContainer oCityContainer = (HttpContext.Current.Session["CityContainer"] != null) ? (CityContainer)HttpContext.Current.Session["CityContainer"] : CityContainer.SelectAllCitys(null);

            List<string> cities = new List<string>();
            foreach (City item in oCityContainer.DataSource)
            {
                if (item.Name_UI.ToLower().Contains(prefixText.ToLower()))
                {
                    string city = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(item.Name_UI, item.Name_UI);
                    cities.Add(city);
                }
            }
            return cities;
        }


        /// <summary>
        /// A method that checks if there is a SEO object for the current destinatio page, and fills the seo panel on the page.
        /// </summary>
        private void FillSeoPanel()
        {
            if (oSeo != null)
            {
                // if there is a SEO object for this page
                txt_SeoTitle.Text = oSeo.SeoTitle;
                txt_FriendlyUrl.Text = oSeo.FriendlyUrl;
                txt_SeoDescription.Text = oSeo.SeoDescription;
                txt_SeoMetaData.Text = oSeo.SeoKeyWords;
            }
            else
            {
                // There is no SEO for this page.
            }
        }

    }
}