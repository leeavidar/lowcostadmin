﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class uc_FlightLegRepeater : UC_BasePage
    {

        #region Prop
        public AirportContainer oAirportsContainer { get; set; }
        public AirlineContainer oAirlineContainer { get; set; }
        public Orders oOrder { get; set; }
        public EnumHandler.FlightTypes Type { get; set; }
        public int FlightLegDocId { get; set; }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (oOrder != null)
            {
                // Choose the outward flightLegs or the return flightLegs as the datasource for the repeater (depending on the 'Type' property)
                OrderFlightLeg oOrderFlightLeg = oOrder.OrderFlightLegs.FindFirstOrDefault(ofl => ofl.FlightType == Type.ToString());
                if (oOrderFlightLeg != null)
                {
                    drp_FlightLegs.DataSource = oOrder.OrderFlightLegs.FindFirstOrDefault(ofl => ofl.FlightType == Type.ToString()).FlightLegs.DataSource;
                    //drp_FlightLegs.DataSource = oOrder.OrderFlightLegs.First.FlightLegs.DataSource;

                    drp_FlightLegs.DataBind();
                }
            }



        }


        /// <summary>
        /// A method that gets the name of an airport.
        /// </summary>
        /// <param name="_airportIataCode">The IATA code of the airport</param>
        /// <returns>The name of the airport.</returns>
        protected string GetFlightLegAirportName(object _airportIataCode)
        {
            string airportIataCode = (string)_airportIataCode;
            return Extensions.GetAirportName(airportIataCode);
        }

        /// <summary>
        /// A method that gets the fuul url of an airline logo.
        /// </summary>
        /// <param name="_airlineIataCode">The IATA code of the airline</param>
        /// <returns>The full url to the logo.</returns>
        protected string GetAirlineLogo(object _airlineIataCode)
        {
            string baseURL = StaticStrings.path_AirlineImages;
            string airlineIataCode = ConvertToValue.ConvertToString(_airlineIataCode);
            string airlineName = "";

            if (airlineIataCode != ConvertToValue.StringEmptyValue)
            {
                DL_LowCost.Airline oAirline = oAirlineContainer.FindFirstOrDefault(airline => airline.IataCode_UI == airlineIataCode);
                if (oAirline != null)
                {
                    airlineName = oAirline.Name_UI;
                }
                else
                {
                    // There is no Airline found with the specified IATA code.
                }
            }
            // return the name of the airline with the url to get the logo url from travelfusion website
            return baseURL + airlineName.ToLower().Replace(" ", string.Empty) + ".gif";
        }

        /// <summary>
        /// A method that gets the airline name.
        /// </summary>
        /// <param name="_airlineIataCode">The IATA code of the airline</param>
        /// <returns>The name of the airline</returns>
        protected string GetFlightLegAirlineName(object _airlineIataCode)
        {
            string airlineIataCode = ConvertToValue.ConvertToString(_airlineIataCode);
            string airlineName = "";

            if (airlineIataCode != ConvertToValue.StringEmptyValue)
            {
                DL_LowCost.Airline oAirline = oAirlineContainer.FindFirstOrDefault(airline => airline.IataCode_UI == airlineIataCode);
                if (oAirline != null)
                {
                    airlineName = oAirline.Name_UI;
                }
                else
                {
                    // There is no Airline found with the specified IATA code.
                }
            }
            // return the name of the airline with the url to get the logo url from travelfusion website
            return airlineName;
        }

        /// <summary>
        /// A method that gets a string of information about a stop between 2 flight legs. 
        /// </summary>
        /// <param name="_flightLegDocId">The docID of the flight leg</param>
        /// <returns>The stop information in the format : "Connection in {airportName} ({airportCode}), {stopTime} hours" </returns>
        protected string GetStopDetails(object _flightLegDocId)
        {

            FlightLeg oFlightLeg = null;
            FlightLeg oFlightLegNext = null;
            bool GotFlightLeg = false;
            int flightLegDocId = ConvertToValue.ConvertToInt32(_flightLegDocId);
            if (flightLegDocId == ConvertToValue.Int32EmptyValue)
            {
                return "";
            }

            OrderFlightLeg oOrderFlightLegOutward;
            OrderFlightLeg oOrderFlightLegReturn;
            if (oOrder != null)
            {
                OrderFlightLegContainer oOrderFlightLegContainerOutward = GetOrderFlightLegsByFlightType(oOrder, EnumHandler.FlightTypes.Outward);
                if (oOrderFlightLegContainerOutward != null)
                {
                    oOrderFlightLegOutward = oOrderFlightLegContainerOutward.First;

                    OrderFlightLegContainer oOrderFlightLegContainerReturn = GetOrderFlightLegsByFlightType(oOrder, EnumHandler.FlightTypes.Return);
                    if (oOrderFlightLegContainerOutward != null)
                    {
                        oOrderFlightLegReturn = oOrderFlightLegContainerReturn.First;

                        foreach (FlightLeg leg in oOrderFlightLegOutward.FlightLegs.DataSource)
                        {
                            if (GotFlightLeg)
                            {
                                oFlightLegNext = leg;
                                break;
                            }
                            if (leg.DocId_Value == flightLegDocId)
                            {
                                oFlightLeg = leg;
                                GotFlightLeg = true;
                            }
                        }
                        if (oFlightLeg == null && GotFlightLeg == false)
                        {
                            if (oOrderFlightLegReturn != null)
                            {
                                foreach (FlightLeg leg in oOrderFlightLegReturn.FlightLegs.DataSource)
                                {
                                    if (GotFlightLeg)
                                    {
                                        oFlightLegNext = leg;
                                        break;
                                    }
                                    if (leg.DocId_Value == flightLegDocId)
                                    {
                                        oFlightLeg = leg;
                                        GotFlightLeg = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (oFlightLeg != null)
            {
                TimeSpan stopTime = FlightLegContainer.CalculateStopTime(oFlightLeg, oFlightLegNext);
                if (stopTime == new TimeSpan(0, 0, -1))
                {
                    return "";
                }
                string airportName = Extensions.GetAirportName(oFlightLeg.DestinationIataCode_Value); // GetFlightLegAirportName(oFlightLeg.DestinationIataCode_Value);
                string airportCode = oFlightLeg.DestinationIataCode_Value;

                string time = stopTime.ToString("hh\\:mm");
                if (!time.StartsWith("00")) 
                {
                    time = time.TrimStart(new char[] { '0' }); 
                }
               
                return String.Format("Connection in {0} ({1}), {2} hours", airportName, airportCode, time);   // timepsan format is: 0:dd\\.hh\\:mm\\:ss   for 01.12:24:02 days
            }

            return "";

        }

        protected string GetSeat(DL_LowCost.Passenger oPassenger)
        {
         
            string seat = "";
            SeatPerFlightLeg oSeatPerFlightLeg = oPassenger.SeatPerFlightLegs.FindFirstOrDefault(s => s.PassengerDocId_Value == oPassenger.DocId_Value && s.FlightLegDocId_Value == FlightLegDocId);
            if (oSeatPerFlightLeg!=null)
            {
                seat = oSeatPerFlightLeg.Seat_UI;
            }

            if (seat.Length == 0)
            {
                seat = "Not Specified";
            }
            return seat;
        }

        /// <summary>
        /// A function that returns the check in method. (returns "not specified" if it's empty)
        /// </summary>
        /// <param name="CheckInType">the check in type</param>
        /// <returns>The check-in type (or "not specified")</returns>
        protected string GetCheckIn(object CheckInType)
        {

            string checkIn = (string)CheckInType;
            if (checkIn.Length == 0)
            {
                checkIn = "Not Specified";
            }
            return checkIn;
        }

        /// <summary>
        /// A method that gets a container with all the OrderFlightLeg objects with a specified type (Outward / Return)
        /// </summary>
        /// <param name="oOrder">The order to search in.</param>
        /// <param name="flightType">The type of the flights you wish to get (Outward / Return) </param>
        /// <returns>A container of all the flight with the specified type</returns>
        protected OrderFlightLegContainer GetOrderFlightLegsByFlightType(Orders oOrder, EnumHandler.FlightTypes flightType)
        {
            OrderFlightLegContainer oOrderFlightLegContainer = oOrder.OrderFlightLegs.FindAllContainer(f => f.FlightType_UI.ToLower() == flightType.ToString().ToLower());
            return oOrderFlightLegContainer;
        }


        #region Events

        protected void drp_FlightLegs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                FlightLeg oFlightLeg = (FlightLeg)e.Item.DataItem;
                FlightLegDocId = oFlightLeg.DocId_Value;

                // Getting the data of the outer, Orders details, repeater
                Repeater currentRepeater = (Repeater)sender;

                // Old version: var data = (((RepeaterItem)e.Item.Parent.Parent)).DataItem as Orders;
                // now i have the order as a property and no need to find it.


                //Finding the Passengers repeater
                Repeater drp_PassengersDetails = (Repeater)e.Item.FindControl("drp_PassengersDetails");

                // Finding the passengers container of the current order
                PassengerContainer oPassengerContainer = oOrder.Passengers;
                drp_PassengersDetails.DataSource = oPassengerContainer.DataSource;

                // Binding data to the  passengers details repeater
                drp_PassengersDetails.DataBind();

             
            }
        }

        protected void drp_PassengersDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DL_LowCost.Passenger oPassenger = (DL_LowCost.Passenger)e.Item.DataItem;
            }
        }

        #endregion



    }
}