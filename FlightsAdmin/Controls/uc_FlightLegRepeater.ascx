﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uc_FlightLegRepeater.ascx.cs" Inherits="FlightsAdmin.uc_FlightLegRepeater" %>
<asp:Repeater ID="drp_FlightLegs" runat="server" ItemType="DL_LowCost.FlightLeg" OnItemDataBound="drp_FlightLegs_ItemDataBound">
    <ItemTemplate>
        <div class="flightleg_box">
            <table>
                <tr>
                    <td>
                        <b>Departure: 
                            <span id="lbl_departureTime">
                                <%# Eval("DepartureDateTimeWithTime_UI") %>
                            </span>
                        </b>
                        <br />
                        <span id="lbl_departureAirport">
                            <%#GetFlightLegAirportName(Eval("OriginIataCode_UI"))%> (<%#Eval("OriginIataCode_UI")%>)
                        </span>
                    </td>
                    <td>
                        <b>Arrival: 
                            <span id="lbl_arrivalTime">
                                <%#Eval("ArrivalDateTimeWithTime_UI") %>
                            </span>
                        </b>
                        <br />
                        <span id="lbl_arrivalAirport">
                            <%#GetFlightLegAirportName(Eval("DestinationIataCode_UI")) %> (<%#Eval("DestinationIataCode_UI")%>)
                        </span>
                    </td>
                    <td>
                        <img id="img_airlineLogo" src='<%#GetAirlineLogo(Eval("AirlineIataCode")) %>' />
                    </td>
                    <td>
                        <span id="lbl_flightCode">
                            <%# Eval("FlightNumber_UI") %>
                        </span>
                        <br />
                        <span id="lbl_airlineName">
                            <%#GetFlightLegAirlineName(Eval("AirlineIataCode")) %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <!-- Second nested repeater -->
                    <asp:Repeater ID="drp_PassengersDetails" runat="server" ItemType="DL_LowCost.Passenger" OnItemDataBound="drp_PassengersDetails_ItemDataBound">
                        <HeaderTemplate>
                            <th>Passengers Deatails:</th>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <span><strong>Name: </strong><%#Eval("FullNameNoTitle_UI")%> </span>
                                </td>
                                <td>
                                    <span><strong>Seat: </strong><%#GetSeat(Item)%></span>
                                </td>
                                <%--<td>
                                    <span><strong>Meal: </strong> </span>
                                </td>--%>
                                <td>
                                    <span><strong>Check-In: </strong><%#GetCheckIn(Eval("CheckInTypeOutward_UI"))%> </span>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
            </table>
        </div>
        <div id="div_flightStop" class="text_center" runat="server">
            <b>
                <span id="lbl_stopDetails"><%#GetStopDetails(Eval("DocId_UI")) %></span>
            </b>
        </div>
    </ItemTemplate>
</asp:Repeater>
