﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Seo.ascx.cs" Inherits="FlightsAdmin.Controls.UC_Seo" %>
<script type="text/javascript">
   
</script>
<!-- START SEO PANEL (this panel is only disaplaed after saving the page in the database) -->
<asp:UpdatePanel runat="server" ID="up_SeoPanel">
    <ContentTemplate>
        <div class="row" runat="server" id="SeoPanel">
            <div class="col-md-12">

                <div class="portlet  box grey">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa  fa-edit"></i>Seo
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div class="form-horizontal">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">URl Friendly</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_FriendlyUrl" CssClass="txt_FriendlyUrl form-control form-control-inline input-medium" runat="server"></asp:TextBox>
                                                <span class="help-block">
                                                    <%-- <asp:Button ID="Button1" runat="server" Text="Check" class="btn purple" />--%>
                                                    <asp:Label ID="lbl_FriendlyUrlStatus" class="lbl_FriendlyUrlStatus" runat="server" Text="Status: "></asp:Label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">SEO Title</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_SeoTitle" CssClass="form-control form-control-inline input-medium" runat="server"></asp:TextBox>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">SEO Description</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_SeoDescription" CssClass="form-control form-control-inline input-medium" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Meta data</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_SeoMetaData" CssClass="form-control form-control-inline input-medium" runat="server"></asp:TextBox>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="col-md-9">
                                            <asp:Button ID="btn_SeoSave" runat="server" CssClass="btn green" Text="Save" OnClientClick="return CheckValidation()" OnClick="btn_SeoSave_Click" />
                                            <asp:Button ID="bt_Cancel" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" formnovalidate />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btn_SeoSave" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>



