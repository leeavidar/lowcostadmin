﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SegmentFlightUC.ascx.cs" Inherits="FlightsAdmin.SegmentFlightUC" %>
<<div class="box_left">
<table>
    <tr>
        <td class="ltr">
            <b><%= GetText("Arrival") %>: 
                <asp:Label ID="lbl_arrivalTime" runat="server" Text="" />
            </b>
            <br />
            <span>
                <asp:Label ID="lbl_arrivalAirport" runat="server" Text="" />
            </span>
        </td>
        <td class="ltr">
            <b><%= GetText("Departure") %>: 
            <asp:Label ID="lbl_departureTime" runat="server" Text="" />
            </b>
            <br />
            <span >
                <asp:Label ID="lbl_departureAirport" runat="server" Text="" />
            </span>
        </td>

        <td>
            <asp:Image ID="img_airlineLogo" runat="server" />
        </td>
        <td>
            <span>
                <asp:Label ID="lbl_flightCode" runat="server" Text="" />
            </span>
            <br />
            <span>
                <asp:Label ID="lbl_airlineName" runat="server" Text="" />
            </span>
        </td>
    </tr>
    <tr>

    <tr>
        <td colspan="4" class="text_right ltr">
            <div id="div_flightStop" runat="server" style="display: none; direction:rtl;">
                <b>
                    <asp:Label ID="lbl_stopDetails" runat="server" Text="" />
                    <asp:Label ID="lbl_stopAirport" runat="server" Text="" />
                    <asp:Label ID="lbl_stopHours" runat="server" Text="" />
                </b>
            </div>
        </td>
    </tr>
</table>
    </div>







