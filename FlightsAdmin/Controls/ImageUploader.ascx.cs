﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin.Controls
{
    public partial class ImageUploader : UC_BasePage
    {
        #region Properties
        public string Title { get; set; }
        public string Alt { get; set; }
        public int RelatedObjectDocId { get; set; }
        public EnumHandler.RelatedObjects RelatedObjectType { get; set; }
        public EnumHandler.ImageTypes ImageType { get; set; }
        private bool _hasImage = false;
        public bool HasImage
        {
            get
            {
                return _hasImage;
            }
            set
            {
                if (value == true)
                {
                    _hasImage = value;
                    btn_RemoveImage.Style.Add("display", "normal");
                }
                else
                {
                    _hasImage = value;
                    btn_RemoveImage.Style.Add("display", "none");
                }
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            // try to get the image from the database
            ImagesContainer oImagesContainer = ImagesContainer.SelectByKeysView_RelatedObjectDocId_RelatedObjectType(RelatedObjectDocId, RelatedObjectType.ToString(), null);
            oImagesContainer = oImagesContainer.FindAllContainer(img => img.ImageType_Value == ImageType.ToString());
            if (oImagesContainer != null && oImagesContainer.Count > 0)
            {
                // if the related image was found, place it in the thumbnail 
                Images oImages = oImagesContainer.Single;
                img_ThumbnailImage.ImageUrl = String.Format("{0}\\{1}", StaticStrings.path_Uploads, oImages.ImageFileName_UI);
                // Set the user control to "has image" state.
                HasImage = true;
            }
        }

        /// <summary>
        /// A method that removes the image from the DB.
        /// The method also: removes the thumbnail image, and refreshes the page.
        /// </summary>
        protected void btn_RemoveImage_Click(object sender, EventArgs e)
        {
            // Get the image from the DB
            ImagesContainer oImagesContainer = ImagesContainer.SelectByKeysView_RelatedObjectDocId_RelatedObjectType(RelatedObjectDocId, RelatedObjectType.ToString(), null);
            oImagesContainer = oImagesContainer.FindAllContainer(img => img.ImageType_Value == ImageType.ToString());
            // If the image was found in the DB
            if (oImagesContainer != null && oImagesContainer.Count == 1)
            {
                Images oImages = oImagesContainer.Single;
                // Delete the image
                oImages.Action(DB_Actions.Delete);
                // remove the image from the thumbnail, and hide the "Remove" button
                SetImage(false);
            }
        }

        /// <summary>
        /// A method that save initiates the saving of the image to the database, and changes the user control to show the thumbnail of the image.
        /// </summary>
        public bool SaveImage()
        {
            // if the image in the file uploader is not the no-image image (or the resolution image):
            if (ctrl_Upload.PostedFile != null && ctrl_Upload.PostedFile.FileName != "") 
            {
                string extension = System.IO.Path.GetExtension(ctrl_Upload.PostedFile.FileName).ToLower();
                if (extension != ".gif" && extension != ".jpeg" && extension != ".jpg" && extension != ".bmp" && extension != ".tiff" && extension != ".png")
                {
                    // if the file is not an image file
                  //  throw new Exception(GetText("TheFileYouAreTryingToUploadIsNotSupported"));
                    return false ;
                   
                }
                else
                {
                    // this method also updates the alt and title, so we will call it even when there is no new image selected.
                    int result = UploadImage(ctrl_Upload.PostedFile, StaticStrings.UploadsDirectory, RelatedObjectType, RelatedObjectDocId, Title, Alt, ImageType);
                    // Change the control to "HasImage" and display the image.
                    if (result == 1)
                    {
                        HasImage = true;
                        return true;
                    }
                }
             
            }
            else if (img_ThumbnailImage.ImageUrl != @"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                && img_ThumbnailImage.ImageUrl != @"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image")
            {
                // only update the alt and title
                //Get the image from the DB, and update it's alt and title (The image will stay the same)
                ImagesContainer oImagesContainer = ImagesContainer.SelectByKeysView_RelatedObjectDocId_RelatedObjectType(RelatedObjectDocId, RelatedObjectType.ToString(), null);
                // from the images of the given object, select the image with the right type
                oImagesContainer = oImagesContainer.FindAllContainer(img => img.ImageType_Value == ImageType.ToString());

                if (oImagesContainer.Count != 0)
                {
                    Images oImage = oImagesContainer.Single;
                    oImage.Alt = Alt;
                    oImage.Title = Title;
                    oImage.Action(DB_Actions.Update);

                    HasImage = true;
                    return true;
                }    
            }
            return false;
        }

        /// <summary>
        /// A method that sets all the neccesssary information to use the image uploader.
        /// </summary>
        /// <param name="title">The title of the image. </param>
        /// <param name="alt">Altenative text for the image.</param>
        /// <param name="relatedObjectType">The type of the related object.</param>
        /// <param name="relatedObjectDocId">The DocId of the object in it's table in the DB.</param>
        /// <param name="ImageType">The type of the image (for example: main, cover etc.).</param>
        public void SetImageUploader(string title, string alt, int relatedObjectDocId, EnumHandler.RelatedObjects relatedObjectType, EnumHandler.ImageTypes imageType)
        {
            Title = title;
            Alt = alt;
            RelatedObjectDocId = relatedObjectDocId;
            RelatedObjectType = relatedObjectType;
            ImageType = imageType;
        }

        /// <summary>
        /// Uploading the image.
        /// </summary>
        /// <param name="postedFile">The posted file from the file uploader</param>
        /// <param name="uploadPath">The path to the directory where you wish to save the file.</param>
        /// <param name="relatedObjectType">The type of the related object.</param>
        /// <param name="relatedObjectDocId">The DocId of the object in it's table in the DB.</param>
        /// <param name="title">The title of the image.</param>
        /// <param name="alt">Altenative text for the image.</param>
        /// <param name="ImageType">The type of the image (for example: main, cover etc.).</param>
        /// <returns>The method returns 1 if the images was saved successfully, or otherwise returns -1.</returns>
        public int UploadImage(HttpPostedFile postedFile, string UploadDirectoryPath, EnumHandler.RelatedObjects relatedObjectType, int relatedObjectDocId, string title, string alt, EnumHandler.ImageTypes ImageType)
        {
            if (postedFile != null)
            {
                string FileExtension = System.IO.Path.GetExtension(postedFile.FileName).ToLower(); //get it's extension
                //gererate a new  GUID filename 
                string GUIDfileName = Guid.NewGuid().ToString() + FileExtension;
                string PathAndName = string.Format("{0}\\{1}", UploadDirectoryPath, GUIDfileName);
                try
                {
                    // Upload the new image
                    postedFile.SaveAs(PathAndName);

                    // save in images table in the database: 

                    ImagesContainer oImagesContainer = ImagesContainer.SelectByKeysView_RelatedObjectDocId_RelatedObjectType(relatedObjectDocId, relatedObjectType.ToString(), null);
                    // from the images of the given object, select the image with the right type
                    oImagesContainer = oImagesContainer.FindAllContainer(img => img.ImageType_Value == ImageType.ToString());
                    if (oImagesContainer.Count == 0)
                    {
                        // There is still no inmage for this object:
                        Images oImage = new Images()
                        {
                            ImageFileName = GUIDfileName,
                            RelatedObjectType = relatedObjectType.ToString(),
                            RelatedObjectDocId = relatedObjectDocId,
                            Title = title,
                            Alt = alt,
                            ImageType = ImageType.ToString()
                        };
                        oImage.Action(DB_Actions.Insert);

                        //Set the image in the thumbnail after saving the new image
                        SetImage(true, oImage);
                    }
                    else
                    {
                        // There is already an image for this object (with this type) - only update to the new image name
                        oImagesContainer.Single.ImageFileName = GUIDfileName;
                        oImagesContainer.Single.Action(DB_Actions.Update);
                        //Set the image in the thumbnail after updating the image
                        SetImage(true, oImagesContainer.Single);
                    }
                    return 1;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
           
            return -1;
        }

        /// <summary>
        /// A method that sets the control to show a thumbnail of an image, or a "No Image" image.
        /// It also displays or hides the "Remove" button (depending on the presence of an image in the thumbnail)
        /// </summary>
        /// <param name="hasImage">If there is no image in the control, set it to false (without the second param)</param>
        /// <param name="image">The image to set in the control.</param>
        internal void SetImage(bool hasImage, Images image = null)
        {
            btn_RemoveImage.Visible = hasImage;
            if (hasImage)
            {
                img_ThumbnailImage.ImageUrl = String.Format("{0}\\{1}", StaticStrings.path_Uploads, image.ImageFileName_UI);
            }
            else
            {
                img_ThumbnailImage.ImageUrl = @"http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
            }
        }
    }
}