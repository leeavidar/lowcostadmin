﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SubBox.ascx.cs" Inherits="FlightsAdmin.UC_SubBox" %>


   <!--SUB BOX-->
                            <div>
                                <h3 class="form-section">Sub box  
                                </h3>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Sub Title</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_SubTitle" class="form-control input-medium " runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Short Text</label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_ShortText" class="form-control input-medium " TextMode="MultiLine" runat="server"></asp:TextBox>
                                                <span class="help-block">This text will appear before "Read More"
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Long Text</label>
                                            <div class="col-md-9">
                                                <textarea id="txt_LongText" name="content" data-provide="markdown" rows="10" data-width="400" class="form-control" runat="server"></textarea>
                                                <span class="help-block">This text will replace the short text
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                     <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Order" class="form-control input-medium" TextMode="Number" runat="server"></asp:TextBox>
                                             <asp:RangeValidator ID="val_OrderRange4" runat="server" ValidationGroup="check_MainInformation" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order" MinimumValue="1" MaximumValue="2147483647" ></asp:RangeValidator>
                                            <span class="help-block">The number must be positive, and larger than 0.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <asp:Button ID="btn_Cancel" runat="server" CssClass="btn" Text="Cancel" OnClick="btn_Cancel_Click" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                          <asp:Button ID="btn_Save" runat="server" Text="Save" CssClass="btn btn-block green" ValidationGroup="check_MainInformation" OnClick="btn_Save_Click"/>
                                    </div>
                                </div>
                            </div>
                            <!-- END SUB BOX -->
