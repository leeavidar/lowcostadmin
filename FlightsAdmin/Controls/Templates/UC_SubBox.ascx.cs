﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BL_LowCost;
using DL_LowCost;
using DL_Generic;

namespace FlightsAdmin
{
    public partial class UC_SubBox : UC_BasePage
    {
        public int TemplateId { get; set; }
        public string TXT_SubTitle { get; set; }
        public string TXT_ShortText { get; set; }
        public string TXT_LongText { get; set; }
        public int TXT_Order { get; set; }
        public SubBox oSubBox { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPage();
        }

        public void SetPage()
        {
            txt_SubTitle.Text = TXT_SubTitle ;
            txt_ShortText.Text = TXT_ShortText;
            txt_LongText.InnerHtml = TXT_LongText;
            txt_Order.Text = ConvertToValue.ConvertToString(TXT_Order);
        }


        #region Events
        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(StaticStrings.path_TemplateIndex);
        }
        #endregion

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            int order = ConvertToValue.ConvertToInt(txt_Order.Text);
            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }
            if (oSubBox == null)
            {
                // In case the page is in 'New Page' mode:
                SubBox newSubBox = new SubBox();
                newSubBox.SubTitle = txt_SubTitle.Text;
                newSubBox.ShortText = txt_ShortText.Text;
                newSubBox.LongText = txt_LongText.InnerHtml;
                newSubBox.Order = order;
                newSubBox.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.

                newSubBox.Action(DL_Generic.DB_Actions.Insert);
            }
            else
            {
                //in case the page is in 'edit' mode:
                oSubBox.SubTitle = txt_SubTitle.Text;
                oSubBox.ShortText = txt_ShortText.Text;
                oSubBox.LongText = txt_LongText.InnerHtml;
                oSubBox.Order = order;
                oSubBox.Action(DL_Generic.DB_Actions.Update);
            }
            Response.Redirect(StaticStrings.path_TemplateIndex);
        }
    }
}