﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageUploader.ascx.cs" Inherits="FlightsAdmin.Controls.ImageUploader" %>

<!-- This user control requires bootstrap fileuploader CSS and JS files !! -->

<div id="fileUploaderDiv" class="fileupload fileupload-new" data-provides="fileupload" runat="server">
    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
        <asp:Image ID="img_ThumbnailImage" CssClass="img_ThumbnailImage" runat="server" ImageUrl="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" />
    </div>
    <div runat="server" id="myImageDiv" class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;">
    </div>
    <div>
        <span class="btn default btn-file">
            <asp:LinkButton ID="btn_SelectImage" CssClass="fileupload-new" runat="server"><i class="fa fa-paper-clip"></i>Select image</asp:LinkButton>
            <%--<span class="fileupload-new">
                        <i class="fa fa-paper-clip"></i>Select image
                    </span>--%>

            <asp:LinkButton ID="btn_Change" CssClass="fileupload-exists" runat="server"><i class="fa fa-undo"></i>Change</asp:LinkButton>
            <%-- <span class="fileupload-exists">
                        <i class="fa fa-undo"></i>Change
                    </span>--%>
            <input type="file" class="default" id="ctrl_Upload" runat="server" accept="image/*" />
            <!-- here is the actual image -->

        </span>
        <%--   <a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i>Remove</a>--%>
        <asp:LinkButton ID="btn_RemoveImage" runat="server" class="btn red btn_RemoveImage" Style="display: none;" OnClick="btn_RemoveImage_Click"><i class="fa fa-trash-o"></i>Remove</asp:LinkButton>

    </div>
</div>

