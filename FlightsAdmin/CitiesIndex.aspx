﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="CitiesIndex.aspx.cs" Inherits="FlightsAdmin.CitiesIndex" %>
<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">

     <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/pages/portfolio.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
 
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">

</asp:Content>
<asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Low Cost Flights Citys"></asp:Label>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>Cities table
                    </div>

                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                        <asp:Repeater ID="drp_Cities" runat="server" ItemType="DL_LowCost.City">
                            <HeaderTemplate>
                                <thead>
                                    <th>IATA Code</th>
                                    <th>Name (Hebrew)</th>
                                    <th>Name (English)</th>
                                    <th>Country Code</th>
                                    <th>Active Status</th>
                                    <%--<th class="hidden-xs">Date</th>--%>
                                    <th></th>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <!-- The order is a priority-->
                                    <td>
                                        <span><%# Eval("IataCode_UI") %></span>
                                    </td>
                                    <td>
                                        <span><%# Eval("Name_UI") %></span>
                                    </td>
                                    <td>
                                        <span><%# Eval("EnglishName_UI") %></span>
                                    </td>
                                     <td>
                                        <span><%# Eval("CountryCode_UI") %></span>
                                    </td>
                                    <td>
                                        <span><%# Eval("IsActive_UI") %></span>
                                    </td>
                                   <%-- <td class="hidden-xs">
                                        <span id="lbl_Type"><%# Eval("Date_UI") %></span>
                                    </td>--%>
                                    <td>
                                        <!--Edit button  -->
                                        <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                        <!-- Delete button -->
                                        <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-md-12 ">
            <!-- Add a new City button - The value of this button will change from "Add a new City" to "Cancel" and open or close the City panel -->
            <input type="button" id="btn_AddNewCity" class="btn green btn_AddNewCity" value="Add a new City" runat="server" />
        </div>
    </div>

    <!-- START - ADD/EDIT PANEL-->
    <div id="CityPanel" class="row  mt10 CityPanel" runat="server" style="display: none;">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-search"></i>Add/Edit City
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">IATA Code</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_IataCode" class="form-control input-medium txt_IataCode" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">City Name (Hebrew)</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Name" class="form-control input-medium txt_Name" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                 <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">City Name (English)</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_NameEng" class="form-control input-medium txt_NameEng" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Country Code</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_CountryCode" class="form-control input-medium txt_CountryCode" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Active Status</label>
                                        <div class="col-md-9">
                                            <asp:CheckBox ID="cb_IsActive" CssClass="cb_IsActive" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- Second row -->

                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <!-- This button's command arguments is set in a siteJS.js file  (SaveNew or SaveEdit) -->
                                        <asp:Button class="btn green btn_SaveNew" ID="btn_SaveNew" runat="server" Text="Save" ValidationGroup="check_city" OnClick="btn_Save_New_Click" />
                                        <asp:Button class="btn green btn_SaveEdit" ID="btn_SaveEdit" runat="server" Text="Save Changes" ValidationGroup="check_city" OnClick="btn_SaveEdit_Click" Style="display: none" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- END - ADD/EDIT PANEL-->
</asp:Content>

<asp:Content ID="scriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">

      <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/table-advanced.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="assets/scripts/form-components.js"></script>

    <script src="Scripts/siteJS.js" type="text/javascript"></script>
    <script src="Scripts/PageScripts/Cities.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();
            FormComponents.init();
        });
    </script>
</asp:Content>
