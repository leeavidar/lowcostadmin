﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Generic;

namespace FlightsAdmin
{
    public class UC_BasePage : UserControl
    {
        #region GetText

        /// </summary>
        /// Retrieves the subdomain from the specified URL.
        /// </summary>
        /// <param name="url">The URL from which to retrieve the subdomain.</param>
        /// <returns>The subdomain if it exist, otherwise null.</returns>
        private static string GetLanguagePrefix(Uri url)
        {
            if (url.HostNameType == UriHostNameType.Dns)
            {
                string host = url.Host;
                return host.Split(new char[] { '.' })[0];
            }
            return null;
        }

        public static int LangId
        {
            get
            {
                string langPrefix = GetLanguagePrefix(HttpContext.Current.Request.Url);

                //Languages oLanguages = LanguagesContainer.SelectByKeysView_Code(langPrefix, null).Single;
                //if (oLanguages != null)
                //{
                //    return oLanguages.DocId_Value;
                //}
                return 1;
            }
        }

        public static string Lang
        {
            get
            {
                string langPrefix = GetLanguagePrefix(HttpContext.Current.Request.Url);

                //Languages oLanguages = LanguagesContainer.SelectByKeysView_Code(langPrefix, null).Single;
                //if (oLanguages != null)
                //{
                //    return oLanguages.Name_UI;
                //}
                //
                //oLanguages = LanguagesContainer.SelectByKeysView_DocId(1, null).Single;
                //if (oLanguages != null)
                //{
                //    return oLanguages.Name_UI;
                //}

                return "English";
            }
        }

        public string GetText(string text)
        {
            try
            {
                return StringHandler.ConvertToFriendlyString(GetGlobalResourceObject(Lang, text).ToString());
            }
            catch (Exception)
            {
                return "";
            }
        }

        #endregion
    }
}