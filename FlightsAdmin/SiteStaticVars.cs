﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightsAdmin
{
    public static class SiteStaticVars
    {
        // use to deermine how many days back should we takes order from the database (if the user doesn't choose to get old orders)
        public static readonly double DaysToGoBack = -14;
    }
}