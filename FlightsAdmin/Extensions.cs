﻿using DL_Generic;
using Generic;
using FlightsAdmin.FlightsSearchService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DL_LowCost;

namespace FlightsAdmin
{
    public static class Extensions
    {
        #region For flights results

        #endregion

        /// <summary>
        /// A method that converts from  system.DayOfWeek to EnumHandler.DaysOfWeek
        /// </summary>
        public static EnumHandler.DaysOfWeek GetDayOfWeek(DayOfWeek oDayOfWeek)
        {
            switch (oDayOfWeek)
            {
                case DayOfWeek.Friday:
                    return EnumHandler.DaysOfWeek.Friday;

                case DayOfWeek.Monday:
                    return EnumHandler.DaysOfWeek.Monday;
                case DayOfWeek.Saturday:
                    return EnumHandler.DaysOfWeek.Saturday;

                case DayOfWeek.Sunday:
                    return EnumHandler.DaysOfWeek.Sunday;

                case DayOfWeek.Thursday:
                    return EnumHandler.DaysOfWeek.Thursday;

                case DayOfWeek.Tuesday:
                    return EnumHandler.DaysOfWeek.Tuesday;

                case DayOfWeek.Wednesday:
                    return EnumHandler.DaysOfWeek.Wednesday;
                default:
                    return EnumHandler.DaysOfWeek.Sunday;
            }
        }


        /// <summary>
        /// A method that gets a datetime objext and returns a stirng with the date folowwd by the day name (using gettext)
        /// </summary>
        /// <param name="DepartDateTime"></param>
        /// <returns></returns>
        public static string GetDateWithDayName(DateTime DepartDateTime)
        {
            //EnumHandler.DaysOfWeek dayOfWeek = Extensions.GetDayOfWeek(ConvertToValue.ConvertToDateTime(DepartDateTime).DayOfWeek);
            string dayName = (new UC_BasePage()).GetText(DepartDateTime.DayOfWeek.ToString()); // (dayOfWeek.ToString());
            return string.Format("{0} {1}", DepartDateTime.ToShortDateString(), dayName);
        }


        #region for searchservice LocationWithOrigin object

        public static string GetLocationName(LocationWithOrigin location)
        {
            if (location.Type == ELocationType.city)
            {
                return GetCityName(location);
            }
            else if (location.Type == ELocationType.airport)
            {
                return GetAirportName(location);
            }
            return "";
        }
        public static string GetLocationName(string iataCode)
        {
            return "";


            string name = GetCityName(iataCode);
            if (ConvertToValue.IsEmpty(name))
            {
                name = GetAirportName(iataCode);
            }
            return name;
        }
        public static string GetAirportName(LocationWithOrigin location)
        {
            return GetAirportName(location.LocationCode);
        }
        public static string GetAirportName(string iataCode)
        {
            DL_LowCost.Airport oAirport = StaticData.oAirportsList.DataSource.FirstOrDefault(airport => airport.IataCode_Value == iataCode);
            if (oAirport != null)
            {
                if (oAirport.NameByLang_UI != "")
                {
                    return oAirport.NameByLang_UI;
                }
                else
                {
                    return oAirport.EnglishName_UI;
                }

            }
            return "";
        }
        public static string GetCityName(LocationWithOrigin location)
        {
            return GetCityName(location.LocationCode);
        }
        public static string GetCityName(string iataCode)
        {
            DL_LowCost.City tempCity = StaticData.oCityList.FirstOrDefault(c => c.IataCode_UI == iataCode);
            if (tempCity != null)
            {
                return tempCity.Name_UI;
            }
            return "";
        }
        public static string GetCityOfAirport(LocationWithOrigin location)
        {
            return GetCityOfAirport(location.LocationCode);
        }
        public static string GetCityOfAirport(string iataCode)
        {
            // get the airport from the airport list
            DL_LowCost.Airport oAirport = StaticData.oAirportsList.DataSource.FirstOrDefault(airport => airport.IataCode_Value == iataCode);
            if (oAirport != null)
            {
                // if there is a city for the airport
                if (oAirport.CitySingle.Name_UI != "")
                {
                    // return the city name
                    return oAirport.CitySingle.Name_UI;
                }
                else
                {
                    // no city with the matching iata code was found in the database.
                }

            }
            return "";
        }
        public static string GetCoutryOfAirport(LocationWithOrigin location)
        {
            return GetCoutryOfAirport(location.LocationCode);
        }
        public static string GetCoutryOfAirport(string iataCode)
        {
            try
            {
                DL_LowCost.Airport oAirport = StaticData.oAirportsList.DataSource.FirstOrDefault(airport => airport.IataCode_UI == iataCode);
                if (oAirport != null)
                {
                    if (oAirport.CitySingle != null)
                    {
                        DL_LowCost.Country oCountry = oAirport.CitySingle.CountrySingle;
                        if (oCountry != null && oCountry.Name_UI != "")
                        {
                            return oAirport.CitySingle.CountrySingle.Name_UI;
                        }

                    }
                    else
                    {
                        // no country with the matching iata code was found in the database.
                    }

                }
                return ""; // when we will get the countries list to fill in the DB, this will return the real country name.
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("{0} ===  Airport Iata Code: {1}", e.Message, iataCode));

                ;
            }
        }

        internal static object GetCoutryOfCity(string iataCode)
        {
            DL_LowCost.City oCity = StaticData.oCityList.FirstOrDefault(city => city.IataCode_UI == iataCode);
            if (oCity != null)
            {
                DL_LowCost.Country oCountry = oCity.CountrySingle;
                if (oCountry != null && oCountry.Name_UI != "")
                {
                    return oCity.CountrySingle.Name_UI;
                }
            }
            return ""; // when we will get the countries list to fill in the DB, this will return the real country name.
        }



        // extension for city object:
        public static string GetCityNameByIataCode(string IataCode)
        {
            DL_LowCost.City tempCity = StaticData.oCityList.FirstOrDefault(c => c.IataCode_UI == IataCode);
            if (tempCity != null)
            {
                return tempCity.Name_UI;
            }

            return "";
        }

        #endregion

        /// <summary>
        /// A function that fires the function in the control's data-OnBack attribute (when returning from the server)
        /// </summary>
        /// <param name="theControl">The control (button, link button etc.) </param>
        /// <param name="thePage">The page to add the script to </param>
        public static void OnBack(System.Web.UI.WebControls.WebControl theControl, BasePage_UI thePage)
        {
            string backScript = theControl.Attributes["data-OnBack"];
            // if there is a back script, add it
            backScript = ConvertToValue.ConvertToString(backScript);
            if (!ConvertToValue.IsEmpty(backScript))
            {
                (thePage as BasePage_UI).AddScript(backScript);
            }
        }
    }

    public static  class ContactExtesnions
    {
        /// <summary>
        /// A method that returns the full name, including title, and uses gettext.
        /// </summary>
        public static string GetFullNameWithTitle(this ContactDetails contact)
        {
            string title = "";
            switch (contact.Title_UI)
            {
                default:
                    break;
                case "mr":
                    title = LangManager.GetLangText("mr");
                    break;
                case "ms":
                case "mrs":
                    title = LangManager.GetLangText("ms");
                    break;
            }

            return string.Format("{0} {1}", title, contact.FullName_UI);
        }
    }
}