﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="DestinationPage_Index.aspx.cs" Inherits="FlightsAdmin.DestinationPage_Index" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <!-- END PAGE LEVEL STYLES -->
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Destinations Pages" />
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>Index of all Destinations
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <asp:Repeater ID="drp_DestinationPagesData" runat="server" ItemType="DL_LowCost.DestinationPage">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Destination Name</th>
                                        <th>Time Difference</th>
                                        <th>Currency</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><span id="lbl_DocId"><%# Eval("DocId_UI") %></span></td>
                                    <td><span id="lbl_Name"><%# Eval("Name_UI") %></span></td>
                                    <td><span id="lbl_TimeDiff"><%# (DL_Generic.ConvertToValue.ConvertToInt(Eval("Time_UI"))/3600) %> Hours</span></td>
                                    <td><span id="lbl_Curency"><%#Eval("LocalCurrency_UI") %></span></td>
                                    <td>
                                        <!--Edit button-->
                                        <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                        <!-- Delete button-->
                                        <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" OnClientClick="return CheckDelete()" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>

            </div>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                 <asp:Button ID="btn_AddNewDestinationPage" runat="server" Text="Add New Destination" CssClass="btn green" OnClick="btn_AddNewDestinationPage_Click" />
            </h3>
        </div>
    </div>
</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" ></script>
    <script type="text/javascript" src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" ></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-markdown/lib/markdown.js" ></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" ></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap2-typeahead.min.js"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="/assets/scripts/form-components.js"></script>
    <script src="/assets/scripts/table-advanced.js"></script>
    <script src="/Scripts/siteJS.js"></script>



    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();
            FormComponents.init();
        });
    </script>

</asp:Content>
