﻿
namespace FlightsAdmin
{
    public class ConfigurationHandler
    {
        // Key for google api
        public static string GoogleApiKey { get { return System.Configuration.ConfigurationManager.AppSettings["GoogleApiKey"]; } }

        public static string InitTranslatorUrl { get { return System.Configuration.ConfigurationManager.AppSettings["InitTranslatorUrl"]; } }
    }
}