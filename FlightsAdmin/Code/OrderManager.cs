﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightsAdmin
{
    public class OrderManager
    {
        /// <summary>
        /// A method that gets an 'Orders' object (which represents a single order) by an OrderDocId 
        /// </summary>
        /// <param name="_orderDocId">The OrderDocId </param>
        /// <returns>The order that was found, or null if there is no order with this id.</returns>
        public static Orders GetOrderByOrderDocId(object _orderDocId,OrdersContainer oSessionOrdersContainer )
        {
            int orderDocId = ConvertToValue.ConvertToInt32(_orderDocId);
            if (!ConvertToValue.IsEmpty(orderDocId))
            {
                OrdersContainer oOrdersContainer = oSessionOrdersContainer.FindByDocIdContainer(orderDocId);
                if (oOrdersContainer.Count == 1)
                {
                    return oOrdersContainer.Single;
                }
                else
                {
                    // in case the Count is 0 it means that there was no order with the specified DocId, and the method will return NULL.
                }
            }
            else
            {
                // If the docId is not of type int.
            }
            return null;
        }


        /// <summary>
        /// A method that gets a container with all the OrderFlightLeg objects with a specified type (Outward / Return)
        /// * For now, an order can contain only one outward flight and one return flight, so the container will contain only 1 OrderFlightLeg.
        /// </summary>
        /// <param name="flightType">The type of the flights you wish to get (Outward / Return) </param>
        /// <returns>A container of all the orderFlightLeg objects with the specified type</returns>
        public static  OrderFlightLegContainer GetOrderFlightLegsByFlightType(Orders oOrder, EnumHandler.FlightTypes flightType)
        {
            // Selecting the OrderFlightLegs that has a type of the parameter 'flightType'
            OrderFlightLegContainer oOrderFlightLegContainer = oOrder.OrderFlightLegs.FindAllContainer(f => f.FlightType_UI.ToLower() == flightType.ToString().ToLower());
            return oOrderFlightLegContainer;
        }

        /// <summary>
        /// A method that gets the date of the outward flight by a specified order DocId.
        /// </summary>
        /// <returns>The date of the outward flight. The method will return "Error" if the date is not set in the datasource.</returns>
        public static string GetOutwardDate(Orders oOrder)
        {
            string outwardDate = "";
            if (oOrder != null)
            {
                OrderFlightLegContainer oOrderFlightLegContainer = GetOrderFlightLegsByFlightType(oOrder, EnumHandler.FlightTypes.Outward);
                if (oOrderFlightLegContainer != null)
                {
                    OrderFlightLeg oOrderFlightLeg = oOrderFlightLegContainer.First;
                    if (oOrderFlightLeg != null)
                    {
                        FlightLegContainer oFlightLegContainer = oOrderFlightLeg.FlightLegs;
                        if (oFlightLegContainer != null)
                        {
                            FlightLeg oFlightLeg = oFlightLegContainer.First;
                            if (oFlightLeg != null)
                            {
                                if (oFlightLeg.DepartureDateTime_Value != ConvertToValue.DateTimeEmptyValue)
                                    outwardDate = oFlightLeg.DepartureDateTime_UI;
                                else
                                {
                                    outwardDate = "Not Set";
                                }
                            }
                            else
                            {
                                //There are no flightLegs.
                            }
                        }
                        else
                        {
                            // There is no oFlightLegContainer.
                        }
                    }
                    else
                    {
                        // There are no OrderFlightLegs found.
                    }
                }
                else
                {
                    // There are no outward flight in this order.
                }
            }
            else
            {
                //There is no order with the specified docId.
            }
            return outwardDate;
        }

        /// <summary>
        /// A method that gets the date of the return flight by a specified order DocId.
        /// </summary>
        /// <returns>The date of the return flight. The method will return "Error" if the date is not set in the datasource.</returns>
        public static string GetReturnDate(Orders oOrder)
        {
            string date = "";
            if (oOrder != null)
            {
                // Get the outward or return OrderFlightLeg object of the current order
                OrderFlightLegContainer oOrderFlightLegContainer = GetOrderFlightLegsByFlightType(oOrder, EnumHandler.FlightTypes.Return);
                if (oOrderFlightLegContainer != null)
                {
                    // Get the first orderFlightLeg (should be only 1)
                    OrderFlightLeg oOrderFlightLeg = oOrderFlightLegContainer.First;
                    if (oOrderFlightLeg != null)
                    {
                        FlightLegContainer oFlightLegContainer = oOrderFlightLeg.FlightLegs;
                        if (oFlightLegContainer != null)
                        {
                            // The return date is the outward date of the first flightLeg:
                            FlightLeg oFlightLeg = oOrderFlightLeg.FlightLegs.First;
                            if (oFlightLeg != null)
                            {
                                if (oFlightLeg.DepartureDateTime_Value != ConvertToValue.DateTimeEmptyValue)
                                {
                                    date = oFlightLeg.DepartureDateTime_UI;
                                }
                                else
                                {
                                    date = "Error";
                                }

                            }
                            else
                            {
                                //There are no flightLegs.
                            }
                        }
                        else
                        {
                            // There is no FlightLegContainer
                        }
                    }
                    else
                    {
                        date = "No return";
                        // There are no OrderFlightLegs found.
                    }
                }
                else
                {
                    // There are no outward flight in this order.
                }
            }
            else
            {
                //There is no order with the specified docId.
            }
            return date;
        }

        /// <summary>
        /// A method that gets the full name of the first passenger in the passengers for an order.
        /// </summary>
        /// <returns>The full name (LastName FirstName)</returns>
        public static string GetLeadPassengerName(Orders oOrder)
        {
            string strPassengerFullName = "";
            if (oOrder != null)
            {
                // oOrder.Passenger could be 0 (not null), and have no passengers in it's datasource
                if (oOrder.Passengers != null && oOrder.Passengers.Count > 0)
                {
                    // Get the first passenger for the order
                    DL_LowCost.Passenger oPassengers = oOrder.Passengers.First;
                    // Get the FullName_UI (extension) Property for this passenger
                    strPassengerFullName = oPassengers.FullName_UI;
                }
                else
                {
                    //There are no passengers for this order
                }
            }
            else
            {
                //There is no order.
            }
            return strPassengerFullName;
        }

        /// <summary>
        /// A method that gets the airline name by an order_Doc_Id
        /// </summary>
        /// <returns>The name of the airline for this order.</returns>
        public static string GetAirlineName(Orders oOrder, AirlineContainer oSessionAirlinesContainer)
        {
            string strAirlineName = "";
            if (oOrder != null && oSessionAirlinesContainer != null)
            {
                //Getting the airline iata code:
                string airlineIataCode = oOrder.AirlineIataCode_Value;
                //Find the airline name using the iata code
                Airline oAirline = oSessionAirlinesContainer.FindFirstOrDefault(airline => airline.IataCode_UI == airlineIataCode);
                if (oAirline != null)
                {
                    strAirlineName = oAirline.Name_UI;
                }
            }
            else
            {
                // There is no order. 
            }
            if (strAirlineName.Length == 0)
            {
                strAirlineName = "Airline wasn't found in the DB.";
            }
            return strAirlineName;
        }

        /// <summary>
        /// A method that gets a Flight number by an OrderDocId 
        /// </summary>
        /// <returns>The flight number found</returns>
        public static string GetFlightNumber(Orders oOrder)
        {
            string flightNumber = "";
            if (oOrder != null)
            {
                OrderFlightLegContainer oOrderFlightLegContainer = oOrder.OrderFlightLegs;
                if (oOrderFlightLegContainer != null)
                {
                    // First get the first flight leg in this order (with type 'outward' and not 'return')
                    OrderFlightLeg oOrderFlightLeg = oOrderFlightLegContainer.FindFirstOrDefault(f => f.FlightType_UI == EnumHandler.FlightTypes.Outward.ToString()); // oOrder.OrderFlightLegs.First;
                    if (oOrderFlightLeg != null)
                    {
                        FlightLegContainer oFlightLegContainer = oOrderFlightLeg.FlightLegs;

                        if (oFlightLegContainer != null)
                        {
                            FlightLeg oFlightLeg = oOrderFlightLeg.FlightLegs.First;
                            if (oFlightLeg != null)
                            {
                                flightNumber = oFlightLeg.FlightNumber_UI;
                                return flightNumber;
                            }
                            else
                            {
                                // There are no flight legs for the OrderFlightLeg found.
                            }
                        }
                        else
                        {
                            // There is no FlightLegContainer for this OrderFlightLeg
                        }
                    }
                    else
                    {
                        // There are no 'OrderFlightLeg' combinations in the order
                    }
                }
                else
                {
                    // There is no OrderFlightLegContainer in this order
                }
            }
            else
            {
                // There is no order with the given orderDocId.
            }
            return flightNumber;
        }

        /// <summary>
        /// A method that gets the flight origin.
        /// </summary>
        /// <param name="oOrder">The Order (Orders object)</param>
        /// <returns>The origin of the flight</returns>
        public static string GetFlightOrigin(Orders oOrder)
        {
            string airportIata = "";
            if (oOrder != null)
            {
                // Get all the the OrderFlightLeg container of type 'Outward'
                OrderFlightLegContainer oOrderFlightLegContainer = GetOrderFlightLegsByFlightType(oOrder, EnumHandler.FlightTypes.Outward);
                if (oOrderFlightLegContainer != null)
                {
                    OrderFlightLeg oOrderFlightLeg = oOrderFlightLegContainer.First;
                    if (oOrderFlightLeg != null)
                    {
                        FlightLegContainer oFlightLegContainer = oOrderFlightLeg.FlightLegs;
                        if (oFlightLegContainer != null)
                        {
                            FlightLeg oFlightLeg = oFlightLegContainer.First;
                            if (oFlightLeg != null)
                            {
                                airportIata = oFlightLeg.OriginIataCode_UI;
                            }
                            else
                            {
                                // There are no flight legs.
                            }
                        }
                        else
                        {
                            // There is no oFlightLegContainer
                        }
                    }
                    else
                    {
                        //There are no orderFlightLegs.
                    }
                }
                else
                {
                    // There is no OrderFlightLegContainer with type Outward found
                }
            }
            else
            {
                //Order with the given docId was not found.
            }

            //  origin contains the iata code of the origin
            return string.Format("{0} ({1}), {2}, {3}", Extensions.GetAirportName(airportIata), airportIata, Extensions.GetCityOfAirport(airportIata), Extensions.GetCoutryOfAirport(airportIata));
        }

        /// <summary>
        /// A method that gets the flight destination.
        /// </summary>
        /// <param name="oOrder">The Order (Orders object)</param>
        /// <returns>The destination of the flight</returns>
        public static string GetFlightDestination(Orders oOrder)
        {
            string destination = "";
            if (oOrder != null)
            {
                // Get all the the OrderFlightLeg container of type 'Outward'.
                OrderFlightLegContainer oOrderFlightLegContainer = GetOrderFlightLegsByFlightType(oOrder, EnumHandler.FlightTypes.Outward);
                if (oOrderFlightLegContainer != null)
                {
                    // Get the first OrderFlightLeg from the container (in the future, if more than 1 flight in an order will be possible, there will be more than 1 OrderFlightLeg).
                    OrderFlightLeg oOrderFlightLeg = oOrderFlightLegContainer.First;
                    if (oOrderFlightLeg != null)
                    {
                        FlightLegContainer oFlightLegsContainer = oOrderFlightLeg.FlightLegs;
                        if (oFlightLegsContainer != null)
                        {
                            int count = oFlightLegsContainer.Count;
                            if (count > 0)
                            {
                                // Get the destination of the entire flight from the destination of the last flight leg.
                                destination = oFlightLegsContainer[count - 1].DestinationIataCode_UI;
                            }
                            else
                            {
                                // 
                            }

                        }
                    }
                    else
                    {
                        // There are no orderFlightLegs.
                    }
                }
                else
                {
                    // There is no OrderFlightLegContainer.
                }
            }
            else
            {
                // An order with the given docId was not found.
            }
            return string.Format("{0} ({1}), {2}, {3}", Extensions.GetAirportName(destination), destination, Extensions.GetCityOfAirport(destination), Extensions.GetCoutryOfAirport(destination));
        }

        /// <summary>
        /// A method that gets the date of the return flight by a specified order DocId.
        /// </summary>
        /// <returns>The date of the return flight. The method will return "Error" if the date is not set in the datasource.</returns>
        public static string GetReturnArrivalDate(Orders oOrder)
        {
            string date = "";
            if (oOrder != null)
            {
                // Get the outward or return OrderFlightLeg object of the current order
                OrderFlightLegContainer oOrderFlightLegContainer = GetOrderFlightLegsByFlightType(oOrder, EnumHandler.FlightTypes.Return);
                if (oOrderFlightLegContainer != null)
                {
                    // Get the first orderFlightLeg (should be only 1)
                    OrderFlightLeg oOrderFlightLeg = oOrderFlightLegContainer.First;
                    if (oOrderFlightLeg != null)
                    {
                        FlightLegContainer oFlightLegContainer = oOrderFlightLeg.FlightLegs;
                        if (oFlightLegContainer != null)
                        {
                            int lastIndex = oOrderFlightLeg.FlightLegs.Count - 1;
                            FlightLeg oFlightLeg = oOrderFlightLeg.FlightLegs[lastIndex];
                            if (oFlightLeg != null)
                            {
                                if (oFlightLeg.DepartureDateTime_Value != ConvertToValue.DateTimeEmptyValue)
                                {
                                    date = oFlightLeg.DepartureDateTime_UI;
                                }
                                else
                                {
                                    date = "Error";
                                }

                            }
                            else
                            {
                                //There are no flightLegs.
                            }
                        }
                        else
                        {
                            // There is no FlightLegContainer
                        }
                    }
                    else
                    {
                        date = "No return";
                        // There are no OrderFlightLegs found.
                    }
                }
                else
                {
                    // There are no outward flight in this order.
                }
            }
            else
            {
                //There is no order with the specified docId.
            }
            return date;
        }

        /// <summary>
        /// A method that gets a contact's phone number (main or mobile)
        /// </summary>
        /// <param name="SecondarPhoneNumberInstead">Leave this parameter empty if you need the main phone number, or send "true" for the secondary number.</param>
        /// <returns>The contact's phone number</returns>
        public static string GetContactPhoneNumber(Orders oOrder, bool SecondarPhoneNumberInstead = false)
        {
            string PhoneNumber = "";
            if (oOrder != null)
            {
                var oContactDetails = oOrder.ContactDetails;
                if (SecondarPhoneNumberInstead)
                    PhoneNumber = oContactDetails.MobliePhone_UI;
                else
                    PhoneNumber = oContactDetails.MainPhone_UI;
            }
            else
            {
                // There was no order found with the specified order docId.
            }
            return PhoneNumber;
        }

        /// <summary>
        /// A method that gets a contact's eMail
        /// </summary>
        /// <returns>The contact's eMail</returns>
        public static string GetContactEmail(Orders oOrder)
        {
            string eMail = "";
            if (oOrder != null)
            {
                var oContactDetails = oOrder.ContactDetails;
                eMail = oContactDetails.Email_UI;
            }
            else
            {
                // There was no order found with the specified order docId.
            }

            if (ConvertToValue.IsEmpty(eMail))
            {
                eMail = "Not Specified";
            }
            return eMail;
        }

        /// <summary>
        /// A method that gets a contact's address
        /// </summary>
        /// <returns>The contact's address</returns>
        public static string GetContactAddress(Orders oOrder)
        {
            string address = "";
            if (oOrder != null)
            {
                var oContactDetails = oOrder.ContactDetails;
                // Add a building name if it's not empty.
                string building = "";
                if (oContactDetails.BuildingName_UI != null && oContactDetails.BuildingName_UI != "")
                {
                    building = string.Format("({0})", oContactDetails.BuildingName_UI);
                }

                // if the zip code is not specified, it will insert empty string (and not int empty value)
                string zipCode = ConvertToValue.IsEmpty(oContactDetails.ZipCode_Value) ? "" : ", " + oContactDetails.ZipCode_UI;
                address = string.Format("{0} {1} {2} {3}{4}", oContactDetails.Street_UI, oContactDetails.BuildingNumber_UI, building, oContactDetails.City_UI, zipCode);

                if (address.Replace(" ", "").Length == 0)
                {
                    address = "Not Specified";
                }
            }
            else
            {
                // There was no order found with the specified order docId.
            }
            return address;
        }

        /// <summary>
        /// A method that returns HTML tags and data representing the passengers data.
        /// </summary>
        /// <returns>HTNL code for all the passengers data.</returns>
        public static string GetPassengersDetails(Orders oOrder)
        {
            string passportNum = "", passportExpiry = "", dateOfBirth = "";
            string details = "";
            int passengerNumber = 1;
            if (oOrder != null)
            {
                PassengerContainer oPassengerContainer = oOrder.Passengers;
                if (oPassengerContainer != null)
                {
                    foreach (DL_LowCost.Passenger p in oOrder.Passengers.DataSource)
                    {
                        passportNum = ConvertToValue.IsEmpty(p.PassportNumber_Value) ? "Not Specified" : p.PassportNumber_UI;
                        passportExpiry = ConvertToValue.IsEmpty(p.PassportExpiryDate_Value) ? "Not Specified" : p.PassportExpiryDate_UI;
                        dateOfBirth = ConvertToValue.IsEmpty(p.DateOfBirth_Value) ? "Not Specified" : p.DateOfBirth_UI;

                        details += string.Format(" <tr><td colspan=\"2\"><h5>Passenger {0}:</h5></td></tr><tr><td>name:</td><td> {1} </td></tr><tr><td>Date Of Birth:</td><td> {2} </td></tr><tr><td>Passport Num:</td><td> {3} </td></tr><tr><td>Passport Expiry Date:</td><td> {4} </td></tr>"
                                                    , passengerNumber++, p.FullName_UI, dateOfBirth, passportNum, passportExpiry);
                    }
                }
                else
                {
                    // There is no PassengerContainer.
                }
            }
            else
            {
                // There was no order found with the specified order docId.
            }
            return details;
        }

        /// <summary>
        /// A method that gets the price with markup for one of the fplight types in an order (Outward / Return).
        /// </summary>
        /// <param name="flightTypeEnum">The type of the flight (Outward / Return).</param>
        /// <returns></returns>
        public static double GetOneWayOrderPrice(Orders oOrder , EnumHandler.FlightTypes flightTypeEnum)
        {

            double totalPrice = 0;
            if (oOrder != null)
            {
                OrderFlightLegContainer oOrderFlightLegContainer = GetOrderFlightLegsByFlightType(oOrder, flightTypeEnum);
                if (oOrderFlightLegContainer != null)
                {
                    // Calculationg the sum of the prices (with markup) for all the flight legs in the given direction (outward / return)
                    foreach (OrderFlightLeg oOrderFlightLeg in oOrderFlightLegContainer.DataSource)
                    {
                        double priceNet = oOrderFlightLeg.PriceNet_Value;
                        double markup = oOrderFlightLeg.PriceMarkUp_Value;
                        if (priceNet == ConvertToValue.DoubleEmptyValue)
                        {
                            // In case one of the net prices is not filled in the datasource, return Empty value (there is no point to continue the calculation). Markup can be empty.
                            return ConvertToValue.DoubleEmptyValue;
                        }
                        if (markup == ConvertToValue.DoubleEmptyValue)
                        {
                            // Don't add the markup if it's an empty value.
                            totalPrice += priceNet;
                        }
                        else
                        {
                            totalPrice += priceNet + markup;
                        }
                    }
                }
                else
                {
                    // There is no OrderFlightLegContainer.
                }
            }
            else
            {
                // There is no order with the specified order DocId.
            }
            return totalPrice;
        }

        /// <summary>
        /// A method that gets the total price of an order, by a specified order docId.
        /// </summary>
        /// <returns>The total price of the order. The method will return "Error" if one of the prices is not set in the datasource.</returns>
        public static string GetOrderPrice(Orders oOrder)
        {
            double outwardPrice = GetOneWayOrderPrice(oOrder, EnumHandler.FlightTypes.Outward);
            double returnPrice = GetOneWayOrderPrice(oOrder, EnumHandler.FlightTypes.Outward);
            if (outwardPrice == ConvertToValue.DoubleEmptyValue || returnPrice == ConvertToValue.DoubleEmptyValue)
            {
                return "Error";
            }
            return (outwardPrice + returnPrice).ToString();
        }

        public static string ConvertOrderStatusToString(int orderStatus)
        {
            string stringStatus = EnumUtil.GetEnumItemAsLangString((EnumHandler.OrderStatus)orderStatus).Value;
            return stringStatus;
        }

        // For the filtering:
        /// <summary>
        /// A method that returns a list of all the passport number in an order.
        /// </summary>
        /// <param name="oOrder">The order to get the passport number from.</param>
        /// <returns>A list of all the passport numbers.</returns>
        public static List<string> GetPassportNumbers(Orders oOrder)
        {
            List<string> PassportNumbers = new List<string>();
            foreach (var oPassenger in oOrder.Passengers.DataSource)
            {
                PassportNumbers.Add(oPassenger.PassportNumber_Value);
            }
            return PassportNumbers;
        }

        /// <summary>
        /// A method that returns a list of all the passengers names (without the title) in an order.
        /// </summary>
        /// <param name="oOrder">The order to get the names from.</param>
        /// <returns>>A list of all the passengers names.</returns>
        public static List<string> GetPassengersNames(Orders oOrder)
        {
            List<string> names = new List<string>();
            foreach (var oPassenger in oOrder.Passengers.DataSource)
            {
                names.Add(oPassenger.FullNameNoTitle_UI);
            }
            return names;
        }

        /// <summary>
        /// A method that searches a given order for a given passport number.
        /// </summary>
        /// <param name="oOrder">The order to search in.</param>
        /// <param name="passportNumber">The passport number to search for in the order.</param>
        /// <returns></returns>
        public static bool IsPassportNumberIncluded(Orders oOrder, string passportNumber)
        {
            List<string> passportNumberList = GetPassportNumbers(oOrder);
            foreach (var number in passportNumberList)
            {
                if (passportNumber == number)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// A method that searches for a passenger name (or some letter of the name) in an order.
        /// </summary>
        /// <param name="oOrder">The order to get the names from.</param>
        /// <param name="name">The string representing the name (or part of the name) to search in the order.</param>
        /// <returns>True if he name exist in the order, or false otherwise.</returns>
        public static bool SearchPassengerNameInOrder(Orders oOrder, string passengerName)
        {
            List<string> passengersNamesList = GetPassengersNames(oOrder);
            foreach (var name in passengersNamesList)
            {
                if (name.ToLower().Contains(passengerName.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }



    }
}