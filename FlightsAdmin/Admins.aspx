﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="Admins.aspx.cs" Inherits="FlightsAdmin.Admins" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/pages/portfolio.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">

</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Low Cost Flights Admins"/>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>Admin Users
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <div id="sample_2_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                <label>
                                    <input type="checkbox" checked data-column="0">User Name</label>
                                <label>
                                    <input type="checkbox" checked data-column="1">First Name</label>
                                <label>
                                    <input type="checkbox" checked data-column="2">Last Name</label>
                                <label>
                                    <input type="checkbox" checked data-column="3">Role</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                        <asp:Repeater ID="drp_Admins" runat="server" ItemType="DL_LowCost.Users">
                            <HeaderTemplate>
                                <thead>
                                    <th>UserName</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th class="hidden-xs">Role</th>
                                    <th></th>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <span id="lbl_Order"><%# Eval("UserName_UI") %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_Title"><%# Eval("FirstName_UI") %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_ShortText"><%# Eval("LastName_UI") %></span>
                                    </td>
                                    <td class="hidden-xs">
                                        <span id="lbl_Type"><%#GetRole(Item)%></span>
                                    </td>
                                    <td>
                                        <!--Edit button  -->
                                        <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                        <!-- Delete button -->
                                        <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12 ">
            <!-- Add new Admin button - The value of this button will change from "Add new Admin" to "Cancel" and open or close the admin panel -->
            <input type="button" id="btn_AddNewAdmin" class="btn green btn_AddNewAdmin" value="Add new Admin" runat="server" />
        </div>
    </div>

    <!-- START - ADD/EDIT PANEL-->
    <div id="AdminPanel" class="row  mt10 AdminPanel" runat="server" style="display: none;">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-edit"></i>Add/Edit Admin
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">User Name:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_UserName" class="form-control  input-medium txt_UserName" runat="server" />
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="val_req_UserName" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_UserName" ForeColor="red" Display="Dynamic"/>
                                                <asp:CustomValidator ID="val_UserNameExist" runat="server" ErrorMessage="The user name already exist. Please choose another one." ForeColor="Red"
                                                    OnServerValidate="val_UserNameExist_ServerValidate" ControlToValidate="txt_UserName" ValidationGroup="usersControl"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">First Name:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_FirstName" class="form-control input-medium txt_FirstName" runat="server" />
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_FirstName" ForeColor="red"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Last name:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_LastName" class="form-control input-medium txt_LastName" runat="server"/>
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_LastName" ForeColor="red"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Password:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Password" class="form-control input-medium txt_Password" runat="server"/>
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="val_req_Password" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_Password" ForeColor="red"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Email" class="form-control input-medium txt_Email" runat="server"/>
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="val_req_Email" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_Email" ForeColor="red"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Role:</label>
                                        <div class="col-md-9">
                                            <asp:DropDownList ID="ddl_Roles" runat="server" class="form-control input-medium ">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- Second row -->
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button class="btn green btn_SaveNew" ID="btn_SaveNew" runat="server" Text="Save" ValidationGroup="usersControl" OnClientClick="CloseEditPanel()" OnClick="btn_Save_New_Click" />
                                        <asp:Button class="btn green btn_SaveEdit" ID="btn_SaveEdit" runat="server" Text="Save Changes" ValidationGroup="usersControl" OnClick="btn_SaveEdit_Click" Style="display: none" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- START - ADD/EDIT PANEL-->

</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="/assets/scripts/app.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/table-advanced.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script src="assets/scripts/app.js"></script>
    <script src="assets/scripts/form-components.js"></script>
    <script src="Scripts/siteJS.js" type="text/javascript"></script>
    <script src="Scripts/PageScripts/Admins.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();
            FormComponents.init();
        });
      
    </script>
</asp:Content>
