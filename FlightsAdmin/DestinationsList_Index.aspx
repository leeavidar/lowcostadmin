﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="DestinationsList_Index.aspx.cs" Inherits="FlightsAdmin.DestinationsList_Index" %>
<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
      <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <!-- END PAGE LEVEL STYLES -->
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Popular Destinations" />
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>Popular Destinations
                    </div>
                </div>

                <div class="portlet-body">


                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <asp:Repeater ID="drp_Destinations" runat="server" ItemType="DL_LowCost.PopularDestination">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Iata Code</th>
                                        <th>City Iata Code</th>
                                         <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                      <td><span id="lbl_DestinationName"><%# Eval("Name_UI") %></span></td>
                                    <td><span id="lbl_DocId"><%# Eval("IataCode_UI") %></span></td>
                                   <td><span id="lbl_CityIataCode"><%# Eval("CityIataCode_UI") %></span></td>
                                    <td>
                                        <!--Edit button-->
                                        <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>

                                        <!-- Delete button-->
                                        <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" OnClientClick="return CheckDelete()" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>

                </div>
            </div>
        </div>
    </div>
      <div class="row">

        <div class="col-md-12 ">
            <!-- Add new Destination button - The value of this button will change from "Add new Admin" to "Cancel" and open or close the admin panel -->
            <input type="button" id="btn_AddNewDestination" class="btn green btn_AddNewDestination" value="Add new Destination" runat="server" />
        </div>
    </div>

    <!-- START - ADD/EDIT PANEL-->
    <div id="DestinationPanel" class="row  mt10 DestinationPanel" runat="server" style="display: none;">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-edit"></i>Add/Edit Destination
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Destination Name:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Name" class="form-control  input-medium txt_Name" runat="server" />
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="val_req_Name" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_Name" ForeColor="red" Display="Dynamic"/>
                                                <%--<asp:CustomValidator ID="val_UserNameExist" runat="server" ErrorMessage="The user name already exist. Please choose another one." ForeColor="Red"
                                                    OnServerValidate="val_UserNameExist_ServerValidate" ControlToValidate="txt_UserName" ValidationGroup="usersControl"/>--%>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Iata Code:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_IataCode" class="form-control input-medium txt_IataCode" runat="server" />
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_IataCode" ForeColor="red"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">City Iata Code:</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_CityIataCode" class="form-control input-medium txt_CityIataCode" runat="server"/>
                                            <span class="help-block">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This fields is required." ValidationGroup="usersControl" ControlToValidate="txt_CityIataCode" ForeColor="red"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                               
                            </div>
                            <!-- Second row -->
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button class="btn green btn_SaveNew" ID="btn_SaveNew" runat="server" Text="Save" ValidationGroup="usersControl" OnClientClick="CloseEditPanel()" OnClick="btn_SaveNew_Click" />
                                        <asp:Button class="btn green btn_SaveEdit" ID="btn_SaveEdit" runat="server" Text="Save Changes" ValidationGroup="usersControl" OnClick="btn_SaveEdit_Click" Style="display: none" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- START - ADD/EDIT PANEL-->
</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
       <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-markdown/lib/markdown.js" ></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js"></script>
    <script type="text/javascript" src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap2-typeahead.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/form-components.js"></script>
    <script src="/assets/scripts/table-advanced.js"></script>
    <script src="/Scripts/siteJS.js"></script>
    <script src="Scripts/PageScripts/PopularDestinaions.js"></script>
</asp:Content>
