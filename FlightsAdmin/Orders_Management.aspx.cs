﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DL_LowCost;
using BL_LowCost;
using DL_Generic;
using Generic;
//using FlightsAdmin.FlightsSearchService;
using System.Linq.Expressions;

namespace FlightsAdmin
{
    public partial class Orders_Management : BasePage_UI
    {
        #region Session Private fields

        private OrdersContainer _oSessionOrders;
        private AirlineContainer _oSessionAirlines;
        // private BL_LowCost.AirportContainer _oSessionAirports; // AirportContainer is also in the services
        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions (or from the database if its the first load of the page or the session is empty)
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || SessionManager.GetSession<OrdersContainer>(out _oSessionOrders) == false)
            {
                //take the data from the tables in the database
                _oSessionOrders = GetOrdersFromDB();

            }
            else { /*Return from session*/}

            _oSessionAirlines = GetAirlinesFromDB();

        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<OrdersContainer>(oSessionOrdersContainer);
            Generic.SessionManager.SetSession<AirlineContainer>(oSessionAirlinesContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<OrdersContainer>(oSessionOrdersContainer);
            //  Generic.SessionManager.ClearSession<AirlineContainer>(oSessionAirlinesContainer);
        }

        /// <summary>
        /// A method that gets orders from the database. The default is to get results only from a specific date and newer.
        /// </summary>
        /// <param name="includeOld">If 'true' - the results will include all the orders from the DB (default is 'false').</param>
        /// <returns>The orders that were found.</returns>
        private OrdersContainer GetOrdersFromDB(bool includeOld = false)
        {
            //First select all the orders from the database
            OrdersContainer orders = BL_LowCost.OrdersContainer.SelectAllOrderss(1, null);
            // if the search should not include results olders than a given date (14 days old)
            if (!includeOld)
            {
                // Calculate the oldest date to get results from
                DateTime oldestDate = DateTime.Now.AddDays(SiteStaticVars.DaysToGoBack).Date;  // SiteStaticVars.DaysToGoBack is '-14'
                // Select only the results that are newer than the oldest date 
                orders = orders.FindAllContainer(order => order.DateOfBook_Value >= oldestDate);
            }

            return orders;
        }

        private BL_LowCost.AirportContainer GetAirportsFromDB()
        {
            //selecting all the orders with a given white label
            BL_LowCost.AirportContainer airports = BL_LowCost.AirportContainer.SelectAllAirports(null);
            return airports;
        }

        private AirlineContainer GetAirlinesFromDB()
        {
            //selecting all the passengers with a given white label
            AirlineContainer airlines = BL_LowCost.AirlineContainer.SelectAllAirlines(null);
            return airlines;
        }



        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }

    public partial class Orders_Management : BasePage_UI
    {
        #region Properties
        #region Private

        #endregion
        #region Public
        public OrdersContainer oSessionOrdersContainer { get { return _oSessionOrders; } set { _oSessionOrders = value; } }
        public OrdersContainer oFilteredOrdersContainer { get; set; }
        public AirlineContainer oSessionAirlinesContainer { get { return _oSessionAirlines; } }
        public BL_LowCost.AirportContainer oAirportsContainer { get { return StaticData.oAirportsList; } }

        public int OrderDocId { get; set; }
        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SalesOperator);
            if (!IsPostBack)
            {
                SetPage();
            }
        }

        /// <summary>
        /// Setting the repater's data source and binding it to the data.
        /// </summary>
        private void SetPage()
        {
            hdn_OldDataIncluded.Value = "false";
            BindDataToRepeater();
            // Fill the ddl with orders statuses
            List<ListItem> statuses = EnumUtil.GetEnumToListItems<EnumHandler.OrderStatus>(false);
            foreach (ListItem item in statuses)
            {
                ddl_Status.Items.Add(item);
            }
        }

        #region Events Handlers

        //protected void drp_OrdersData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        Orders oOrder = (Orders)e.Item.DataItem;
        //        OrderDocId = oOrder.DocId_Value;

        //        // set the details linkbutton's command argument to be the DocId of the order
        //    //    (e.Item.FindControl("lnk_DetailsPage") as LinkButton).CommandArgument = OrderDocId.ToString();
        //        (e.Item.FindControl("lnk_DetailsPage") as LinkButton).Attributes["DocId"] = OrderDocId.ToString();

        //    }
        //}

        protected void btn_Filter_Click(object sender, EventArgs e)
        {
            // // //  // binding data to the repeater (takes care of the change in the "Include-older-orders" check-box  from the last search)
            //BindDataToRepeater();
            //  // if the user clicks on 'Find' when all the fields are empty, it will show the un-filtered results.
            //if (IsFilterFormEmpty())
            //{
            //    return;
            //}

            #region getting all the filter values

            string lcPNR = txt_filter_LcPnr.Text;
            string supplierPNR = txt_filter_AirlinePnr.Text;
            DateTime fromDate = ConvertToValue.ConvertToDateTime(txt_filter_FromDate.Text);
            DateTime toDate = ConvertToValue.ConvertToDateTime(txt_filter_ToDate.Text);
            string clientName = txt_filter_ClientName.Text;
            string passportNumber = txt_filter_PassNum.Text;
            string airline = txt_filter_Airline.Text;
            string flightNumber = txt_filter_FlightNumber.Text;

            #endregion

            OrdersContainer oOrdersContainer = oSessionOrdersContainer;

            // This variable is a temp in order to bind the filtered results to the repeater, without changing the orders in the session
            oFilteredOrdersContainer = oOrdersContainer;
            if (ddl_Status.SelectedValue != "0")
            {
                int status = ConvertToValue.ConvertToInt(ddl_Status.SelectedValue);
                oFilteredOrdersContainer = oOrdersContainer.FindAllContainer(o => o.OrderStatus_Value == status);
            }

            if (lcPNR != "")
            {
                oFilteredOrdersContainer = oOrdersContainer.FindAllContainer(o => o.LowCostPNR_Value == lcPNR);
            }
            else if (supplierPNR != "")
            {
                oFilteredOrdersContainer = oOrdersContainer.FindAllContainer(o => o.SupplierPNR_Value == supplierPNR);

            }
            else if (lcPNR == "" && supplierPNR == "")
            {
                // If there is a "FROM DATE" only :
                if (fromDate != ConvertToValue.DateTimeEmptyValue && toDate == ConvertToValue.DateTimeEmptyValue)
                {
                    oFilteredOrdersContainer = oOrdersContainer.FindAllContainer(o => ConvertToValue.ConvertToDateTime(OrderManager.GetOutwardDate(o)) == fromDate);
                }
                // If there are both "FROM DATE" and "TO DATE"
                else if (fromDate != ConvertToValue.DateTimeEmptyValue && toDate != ConvertToValue.DateTimeEmptyValue)
                {
                    oFilteredOrdersContainer = oOrdersContainer.FindAllContainer(o => ConvertToValue.ConvertToDateTime(OrderManager.GetOutwardDate(o)) == fromDate && ConvertToValue.ConvertToDateTime(OrderManager.GetReturnDate(o)) == toDate);
                }
                //If there is a "TO DATE" only
                else if (fromDate == ConvertToValue.DateTimeEmptyValue && toDate != ConvertToValue.DateTimeEmptyValue)
                {
                    oFilteredOrdersContainer = oOrdersContainer.FindAllContainer(o => ConvertToValue.ConvertToDateTime(OrderManager.GetReturnDate(o)) == toDate);
                }

                if (clientName != "")
                {
                    oFilteredOrdersContainer = oFilteredOrdersContainer.FindAllContainer(o => OrderManager.SearchPassengerNameInOrder(o, clientName.ToLower()));
                }
                if (passportNumber != "")
                {
                    oFilteredOrdersContainer = oFilteredOrdersContainer.FindAllContainer(o => OrderManager.IsPassportNumberIncluded(o, passportNumber));
                }
                if (airline != "")
                {
                    oFilteredOrdersContainer = oFilteredOrdersContainer.FindAllContainer(o => OrderManager.GetAirlineName(o, oSessionAirlinesContainer).ToLower().Contains(airline.ToLower()));
                }
                if (flightNumber != "")
                {
                    oFilteredOrdersContainer = oFilteredOrdersContainer.FindAllContainer(o => OrderManager.GetFlightNumber(o) == flightNumber);
                }
            }
            //Binding the FILTERED data to the repeater (can't use the BindDataToRepeater method because it takes from the session)
            drp_OrdersData.DataSource = oFilteredOrdersContainer.DataSource;

            drp_OrdersData.DataBind();
        }

        protected void btn_Clear_Click(object sender, EventArgs e)
        {
            ClearFilterForm();

            btn_Filter_Click(new object(), new EventArgs());
        }

        protected void btn_ShowStatusMenu_Click(object sender, EventArgs e)
        {

        }

        protected void ddl_ChooseStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dropDownList = (DropDownList)sender;
            string SelectedStatus = dropDownList.SelectedValue;
            string orderDocId = dropDownList.DataValueField;

            Orders oOrder =OrderManager.GetOrderByOrderDocId(orderDocId, oSessionOrdersContainer);
            if (oOrder != null)
            {
                int newStatus = ConvertToValue.ConvertToInt(SelectedStatus);
                if (oOrder.OrderStatus != newStatus)
                {
                    oOrder.OrderStatus = newStatus;
                    oOrder.Action(DB_Actions.Update);
                    BindDataToRepeater();
                }
            }
        }

        protected void lnk_DetailsPage_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            string orderDocId = btn.CommandArgument;
            Response.Redirect(QueryStringManager.RedirectWithQuery(StaticStrings.path_OrderDetails, EnumHandler.QueryStrings.ID, orderDocId));
        }

        #endregion

        #region Helping methods
        // For the filtering:
        /// <summary>
        /// A method that check if all the fields of the filtering form is empty, and the status ddl is set to all.
        /// </summary>
        /// <returns>True if all fields are empty, or false oherwise.</returns>
        protected bool IsFilterFormEmpty()
        {
            if (ddl_Status.SelectedIndex == 0 &&
                txt_filter_LcPnr.Text == "" &&
                txt_filter_AirlinePnr.Text == "" &&
                txt_filter_FromDate.Text == "" &&
                txt_filter_ToDate.Text == "" &&
                txt_filter_ClientName.Text == "" &&
                txt_filter_PassNum.Text == "" &&
                txt_filter_Airline.Text == "" &&
                txt_filter_FlightNumber.Text == "")
            {
                return true;
            }
            return false;

        }

        /// <summary>
        /// A method that clears all the fields of the filter form.
        /// </summary>
        protected void ClearFilterForm()
        {
            ddl_Status.SelectedIndex = 0;
            txt_filter_LcPnr.Text = "";
            txt_filter_AirlinePnr.Text = "";
            txt_filter_FromDate.Text = "";
            txt_filter_ToDate.Text = "";
            txt_filter_ClientName.Text = "";
            txt_filter_PassNum.Text = "";
            txt_filter_Airline.Text = "";
            txt_filter_FlightNumber.Text = "";
        }

        /// <summary>
        /// A method that binds the data from the oSessionOrdersContainer to the orders repeater.
        /// This method takes the data from the DB if there are only new orders in the session, and the user asked for older orders also.
        /// If the data in the session is the same as the data needed for a new search, there will be no call to the DB.
        /// </summary>
        private void BindDataToRepeater()
        {
            // If the checkbox is checked and the previous session didn't have the older orders, get the older orders from the DB
            if (cb_SearchDatabase.Checked && hdn_OldDataIncluded.Value == "false")
            {
                oSessionOrdersContainer = GetOrdersFromDB(true);
                // Switch the hidden field to true for the next search.
                hdn_OldDataIncluded.Value = "true";
            }
            // if the older session had the olders orders, and the checkbox is not checked, filter out the older orders
            else if (!cb_SearchDatabase.Checked && hdn_OldDataIncluded.Value == "true")
            {
                // Calculate the oldest date to get results from
                DateTime oldestDate = DateTime.Now.AddDays(SiteStaticVars.DaysToGoBack).Date;  // SiteStaticVars.DaysToGoBack is '-14'
                // Select only the results that are newer than the oldest date
                oSessionOrdersContainer = oSessionOrdersContainer.FindAllContainer(order => order.DateOfBook_Value >= oldestDate);
                // Switch the hidden field to false for the next search.
                hdn_OldDataIncluded.Value = "false";
            }
            else if (cb_SearchDatabase.Checked)
            {
                hdn_OldDataIncluded.Value = "ture";
            }
            else
            {
                hdn_OldDataIncluded.Value = "false";
            }

            drp_OrdersData.DataSource = oSessionOrdersContainer.DataSource;
            drp_OrdersData.DataBind();
        }

        #endregion
    }
}