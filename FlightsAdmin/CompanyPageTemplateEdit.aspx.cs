﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{

    public partial class CompanyPageTemplateEdit : BasePage_UI
    {
        #region Session Private fields

        private Template _oSessionTemplate;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<Template>(out _oSessionTemplate) == false)
            {
                //take the data from the table in the database
                _oSessionTemplate = GetTemplateFromDB();
            }
            else { /*Return from session*/}
        }


        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<Template>(oSessionTemplate);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<Template>(oSessionTemplate);
        }

        private Template GetTemplateFromDB()
        {
            //selecting the template with the TemplateDocId from the query string (and the white label)
            TemplateContainer oTemplateContainer = TemplateContainer.SelectByID(QueryStringManager.QueryStringID(EnumHandler.QueryStrings.ID), 1, null); // TODO: replace with "WhiteLabelId"
            if (oTemplateContainer.Count > 0)
            {
                Template oTemplate = oTemplateContainer.Single;
                IsNewTemplate = false;
                return oTemplate;
            }
            else
            {
                IsNewTemplate = true;
                return null;
            }
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }
   
    public partial class CompanyPageTemplateEdit : BasePage_UI
    {
        #region Properties

          #region Private
          
          #endregion
          #region Public

        public Template oSessionTemplate { get { return _oSessionTemplate; } set { _oSessionTemplate = value; } }

        public int PageDocId { get; set; }

        public bool IsNewTemplate { get; set; }

     
          
          #endregion

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            //  Check if we are starting to edit a new template
            if (!IsNewTemplate)
            {
                //  << EDIT MODE >>
                // Save the CompanyPage DocId for later use.
                PageDocId = oSessionTemplate.RelatedObjectDocId_Value;

                if (!IsPostBack)
                {
                    //Change the page title to Edit...
                    lbl_PageTitle.Text = string.Format("Edit \"{0}\" Page", oSessionTemplate.Title_UI);
                    int TemplateId = ConvertToValue.ConvertToInt(QueryStringManager.QueryString(EnumHandler.QueryStrings.ID));
                    FillTemplateInformation(TemplateId);
                }

                // If there are subboxes for a template - load them into the sub boxes repeater
                if (oSessionTemplate != null && oSessionTemplate.SubBoxs != null && oSessionTemplate.SubBoxs.Count > 0)
                {
                    drp_SubBoxTable.DataSource = oSessionTemplate.SubBoxs.DataSource;
                    drp_SubBoxTable.DataBind();
                }
            }
            else // It is a new template
            {
                // Save the CompanyPage DocId for later use.
                PageDocId = ConvertToValue.ConvertToInt(QueryStringManager.QueryString(EnumHandler.QueryStrings.CompanyPageId));
                
            }

        }
            
        /// <summary>
        /// A method that fills the fields with data from the template in the session.
        /// </summary>
        private void FillTemplateInformation(int TemplateDocId)
        {
                // Initialize the fields with data from the session
                txt_Title_tmp_MainInformation.Text = oSessionTemplate.Title_UI;
                txt_Order_tmp_MainInformation.Text = oSessionTemplate.Order_UI;

                // We are in edit mode, so we need to unhide the "Add Sub Box" button
                btn_AddSubBox.Style.Add("display", "normal");
                SubBoxesTableDiv.Style.Add("display", "normal");
                //Focus on the first field
                txt_Title_tmp_MainInformation.Focus();

        }



        #region Event Handlers

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            // if anything enterd is invalid
                if (!IsValid)
                return;

            string title = txt_Title_tmp_MainInformation.Text;
            int order = ConvertToValue.ConvertToInt(txt_Order_tmp_MainInformation.Text);

            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }

            if (oSessionTemplate == null)
            {
                // In case the page is in 'New Page' mode:

                //Create a new empty static page
                oSessionTemplate = new Template();
                //Fill its title and html content
                oSessionTemplate.Title = title;
                oSessionTemplate.Order = order;
                oSessionTemplate.Type = EnumHandler.TemplateType.MainInformation.ToString();
                oSessionTemplate.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.
                oSessionTemplate.RelatedObjectDocId = PageDocId;
                oSessionTemplate.RelatedObjectType = (int)EnumHandler.RelatedObjects.CompanyPage;
                oSessionTemplate.Action(DL_Generic.DB_Actions.Insert);


                btn_AddSubBox.Style.Add("display", "normal");

                Response.Redirect(StaticStrings.path_CompanyPageTemplateEdit + "?" + EnumHandler.QueryStrings.ID + "=" + oSessionTemplate.DocId_Value);
            }
            else
            {
                //in case the page is in 'edit' mode:
                oSessionTemplate.Title = title;
                oSessionTemplate.Order = order;
                oSessionTemplate.Action(DL_Generic.DB_Actions.Update);
            }
        }


        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            if (ConvertToValue.IsEmpty(PageDocId))
            {
                  Response.Redirect(StaticStrings.path_CompanyPageEdit + "?" + EnumHandler.QueryStrings.ID + "=" + QueryStringManager.QueryStringID(EnumHandler.QueryStrings.ID));
            }
            Response.Redirect(StaticStrings.path_CompanyPageEdit + "?" + EnumHandler.QueryStrings.ID + "=" + PageDocId);
        }


        #region SubBox events

        /// <summary>
        /// This method only fills the sub box panel with data from the sub box in the session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int SubBoxDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(SubBoxDocId))
            {
                SubBoxContainer oSubBoxContainer = oSessionTemplate.SubBoxs.SelectByID(SubBoxDocId, 1); // TODO: replace 1 with whitelable
                if (oSubBoxContainer != null)
                {
                    SubBox oSubBox = oSubBoxContainer.Single;
                    if (oSubBox != null)
                    {
                        #region Fill SubBox panel fields

                        txt_SubTitle.Text = oSubBox.SubTitle_UI;
                        txt_ShortText.Text = oSubBox.ShortText_UI;
                        txt_LongText.InnerText = oSubBox.LongText_UI;
                        txt_Order.Text = oSubBox.Order_UI;
                        
                        #endregion

                        // Unhide the SubBoxPanel and the SaveEdit button  
                        NewSubBoxDiv.Style.Add("display", "normal");
                        // Switch buttons
                        btn_SaveEdit.Style.Add("display", "normal");
                        btn_SaveNew.Style.Add("display", "none");
                        // Set the command argument of the button to hold the DocId of the SubBox to edit
                        btn_SaveEdit.CommandArgument = SubBoxDocId.ToString();
                        btn_AddSubBox.Value = "Cancel";
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);

            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id)) // Same as comparing to int32EmptyValue
            {
                SubBoxContainer oSubBoxContainer = oSessionTemplate.SubBoxs.SelectByID(id, 1); // TODO: replace 1 with whitelable
                if (oSubBoxContainer != null)
                {
                    SubBox oSubBox = oSubBoxContainer.Single; // TODO: change the 1 to a real white label
                    if (oSubBox != null)
                    {
                        oSubBox.Action(DB_Actions.Delete);
                        Response.Redirect(StaticStrings.path_CompanyPageTemplateEdit + "?id=" + oSessionTemplate.DocId_Value);
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_SaveNew_Click(object sender, EventArgs e)
        {

            int order = ConvertToValue.ConvertToInt(txt_Order.Text);
            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }
            SubBox newSubBox = new SubBox();
            newSubBox.TemplateDocId = oSessionTemplate.DocId_Value;
            newSubBox.SubTitle = txt_SubTitle.Text;
            newSubBox.ShortText = txt_ShortText.Text;
            newSubBox.LongText = txt_LongText.InnerHtml;
            newSubBox.Order = order;
            newSubBox.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.

            // Save the new SubBox to the DB, and update the session
            newSubBox.Action(DL_Generic.DB_Actions.Insert);
            oSessionTemplate = GetTemplateFromDB();

            BindDataToRepeater();

            // Response.Redirect(StaticStrings.path_TemplateEdit+"?id="+oSessionTemplate.DocId_Value);
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            int order = ConvertToValue.ConvertToInt(txt_Order.Text);
            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }

            LoadFromSession(false);
            int SubBoxDocId = ConvertToValue.ConvertToInt(((Button)sender).CommandArgument);
            if (SubBoxDocId != ConvertToValue.Int32EmptyValue)
            {
                SubBoxContainer oSubBoxContainer = oSessionTemplate.SubBoxs.SelectByID(SubBoxDocId, 1); // TODO: replace 1 with whitelable
                if (oSubBoxContainer != null && oSubBoxContainer.Count > 0)
                {
                    SubBox oSubBox = oSubBoxContainer.Single;
                    oSubBox.SubTitle = txt_SubTitle.Text;
                    oSubBox.ShortText = txt_ShortText.Text;
                    oSubBox.LongText = txt_LongText.InnerHtml;
                    // Save the new SubBox to the DB, and update the session
                    oSubBox.Action(DB_Actions.Update);
                    oSessionTemplate = GetTemplateFromDB();

                    BindDataToRepeater();

                    // Switch buttons
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");

                    // Response.Redirect(StaticStrings.path_TemplateEdit + "?id=" + oSessionTemplate.DocId_Value);
                }
            }
        }


        #endregion

        #endregion


        #region Helping methods

        /// <summary>
        /// A method that clears the fields of the SubBox Panel.
        /// </summary>
        private void ClearSubBoxPanel()
        {
            txt_ShortText.Text = "";
            txt_LongText.InnerText = "";
            txt_SubTitle.Text = "";
            txt_Order.Text = "1";
        }

        /// <summary>
        /// A method that binds the data from the session to the repaeater.
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_SubBoxTable.DataSource = oSessionTemplate.SubBoxs.DataSource;
            drp_SubBoxTable.DataBind();
        }
        #endregion

    }
}