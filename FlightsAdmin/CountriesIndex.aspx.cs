﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class CountriesIndex : BasePage_UI
    {
        #region Session Private fields

        private CountryContainer _oSessionCountryContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<CountryContainer>(out _oSessionCountryContainer) == false)
            {
                //take the data from the table in the database
                _oSessionCountryContainer = GetCountriesFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<CountryContainer>(oSessionCountryContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<CountryContainer>(oSessionCountryContainer);

        }


        /// <summary>
        /// A method that gets all the Countries from the database.
        /// </summary>
        /// <returns></returns>
        private CountryContainer GetCountriesFromDB()
        {
            //selecting all the countries
            CountryContainer allCountries = BL_LowCost.CountryContainer.SelectAllCountrys(null);  
            return allCountries;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion
    }


    public partial class CountriesIndex : BasePage_UI
    {
        #region Properties
        public CountryContainer oSessionCountryContainer { get { return _oSessionCountryContainer; } set { _oSessionCountryContainer = value; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                CountryPanel.Style.Add("display", "none");
            }
            SetPage();
        }

        /// <summary>
        /// Sets all the data on the page
        /// </summary>
        private void SetPage()
        {
            BindDataToRepeater();
        }

        #region Event Handlers

        protected void btn_Save_New_Click(object sender, EventArgs e)
        {
            // Check validations
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string code = txt_Code.Text;
            string name = txt_Name.Text;
            string englishName = txt_NameEng.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            Country oCountry = new Country()
            {
                CountryCode = code,
                Name = name,
                EnglishName = englishName,
                IsActive = isActive 
            };
            // Save the new country to the DB, and update the session
            oCountry.Action(DB_Actions.Insert);
            oSessionCountryContainer= GetCountriesFromDB();
            BindDataToRepeater();
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string code = txt_Code.Text;
            string name = txt_Name.Text;
            string englishName = txt_NameEng.Text;
            bool isActive = cb_IsActive.Checked;
            #endregion

            Button saveButton = (Button)sender;
            int countryDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(countryDocId))
            {
                var oCountryContainer = CountryContainer.SelectByID(countryDocId, null);

                if (oCountryContainer != null && oCountryContainer.Count > 0)
                {
                    Country countryToEdit = oCountryContainer.Single;
                    #region Fill tip properties
                    countryToEdit.CountryCode = code;
                    countryToEdit.Name= name;
                    countryToEdit.EnglishName = englishName;
                    countryToEdit.IsActive = isActive;
                    #endregion

                    countryToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the button that shows the panel.
                    CountryPanel.Style.Add("display", "none");
                    btn_AddNewCountry.Value = "Add a new Country";
                    oSessionCountryContainer = GetCountriesFromDB();
                    BindDataToRepeater();
                }
            }
        }

        /// <summary>
        /// Fill the edit panel  with data from the country in the session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the Tip to edit from the command argument of the button
            int countryDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(countryDocId))
            {
                var oCountryContainer  = oSessionCountryContainer.SelectByID(countryDocId); 
                if (oCountryContainer!= null)
                {
                    Country oCountry = oCountryContainer.Single;
                    if (oCountry != null)
                    {
                        #region Fill the tip panel with data.
                        txt_Code.Text = oCountry.CountryCode_UI;
                        txt_Name.Text = oCountry.Name_UI;
                        txt_NameEng.Text = oCountry.EnglishName_UI;
                        cb_IsActive.Checked = oCountry.IsActive_Value;
                        #endregion

                        // Displaying the SaveEdit button (and hiding the other one)
                        btn_SaveNew.Style.Add("display", "none");
                        btn_SaveEdit.Style.Add("display", "normal");
                        // Unhide the country panel for editing
                        CountryPanel.Style.Add("display", "normal");
                        btn_SaveEdit.CommandArgument = countryDocId.ToString();
                        btn_AddNewCountry.Value = "Cancel";
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int countryDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(countryDocId))
            {
                CountryContainer oCountryContainer = oSessionCountryContainer.SelectByID(countryDocId);
                if (oCountryContainer != null)
                {
                    Country oCountry  = oCountryContainer.Single;
                    if (oCountry != null)
                    {
                        oCountry.Action(DB_Actions.Delete);
                        oSessionCountryContainer= GetCountriesFromDB();
                        BindDataToRepeater();
                    }
                }
            }
        }

        #endregion

        #region Helping methods
        private void BindDataToRepeater()
        {
            drp_Countries.DataSource = oSessionCountryContainer.DataSource;
            drp_Countries.DataBind();
        }

        /// <summary>
        /// Set the panel for a new entry.
        /// </summary>
        private void ClearCountryPanel()
        {
            txt_Code.Text = "";
            txt_Name.Text = "";
            txt_NameEng.Text = "";
            cb_IsActive.Checked = true;
        }

        #endregion
    }
}