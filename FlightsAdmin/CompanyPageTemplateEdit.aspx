﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="CompanyPageTemplateEdit.aspx.cs" Inherits="FlightsAdmin.CompanyPageTemplateEdit" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<%@ Register Src="Controls/Templates/UC_SubBox.ascx" TagName="UC_SubBox" TagPrefix="uc1" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="assets/plugins/fancybox/source/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style-metronic.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/style-responsive.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/plugins.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/pages/portfolio.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/themes/default.css" id="style_color" />
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--Page title -->
    <div class="row">
        <div class="col-md-12">
            <h3>
                <asp:Label ID="lbl_PageTitle" runat="server" Text="New Paragraph"></asp:Label>
            </h3>
        </div>
    </div>
    <!-- Template Editor: Main information -->
    <div id="template_MainInformation" class="row template_MainInformation" runat="server">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet  box grey">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa  fa-search"></i>Main information
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <div class="form-horizontal">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Main Title</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_Title_tmp_MainInformation" class="form-control input-medium " runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Order</label>
                                                <div class="col-md-9">
                                                    <asp:TextBox ID="txt_Order_tmp_MainInformation" class="form-control input-medium" TextMode="Number" runat="server"></asp:TextBox>
                                                    <asp:RangeValidator ID="val_OrderRange4" runat="server" ValidationGroup="check_MainInformation" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order_tmp_MainInformation" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                                    <span class="help-block">The number must be positive, and larger than 0.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions fluid">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="col-md-9">
                                                    <asp:Button ID="btn_Save" runat="server" CssClass="btn green" Text="Save" OnClick="btn_Save_Click" />
                                                    <asp:Button ID="btn_Cancel1" runat="server" CssClass="btn" Text="Cancel (back)" OnClick="btn_Cancel_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                            <!-- END FORM-->
                        </div>


                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!--START: SUB BOXES TABLE-->
                    <div id="SubBoxesTableDiv" runat="server" style="display: none;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet box blue">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-book"></i>Sub Box
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="SubBoxesTable table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                            <asp:Repeater ID="drp_SubBoxTable" runat="server" ItemType="DL_LowCost.SubBox">
                                                <HeaderTemplate>
                                                    <thead>
                                                        <th>ID</th>
                                                        <th>Sub title</th>
                                                        <th>Order</th>
                                                        <th>Options</th>
                                                    </thead>
                                                    <tbody>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%#Eval("DocId_UI") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("SubTitle_UI") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Order_UI") %>
                                                        </td>
                                                        <td>
                                                            <!-- Buttons for Editing or Deleting a SubBox   -->
                                                            <asp:LinkButton ID="btn_Edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                                            <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </tbody>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: SUB BOXES TABLE-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <input type="button" id="btn_AddSubBox" class="btn green btn_AddSubBox" value="Add Sub Box" style="display: none;" runat="server" />

                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <!-- START: NEW SUB-BOX DIV -->
            <div id="NewSubBoxDiv" style="display: none" runat="server" class="NewSubBoxDiv">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet  box grey">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa  fa-search"></i>Add / Edit Sub Box 
                                </div>

                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-horizontal">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Sub Title</label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txt_SubTitle" class="txt_SubTitle form-control input-medium " runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Short Text</label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txt_ShortText" class="txt_ShortText form-control input-medium " TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        <span class="help-block">This text will appear before "Read More"
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Long Text</label>
                                                    <div class="col-md-9">
                                                        <textarea id="txt_LongText" name="content" data-provide="markdown" rows="10" data-width="400" class="txt_LongText form-control" runat="server"></textarea>
                                                        <span class="help-block">This text will replace the short text
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--/span-->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3">Order</label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txt_Order" class="txt_Order form-control input-medium" TextMode="Number" runat="server"></asp:TextBox>
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ValidationGroup="check_MainInformation" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                                        <span class="help-block">The number must be positive, and larger than 0.
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                    <div class="form-actions fluid">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="col-md-9">
                                                    <asp:Button ID="btn_SaveNew" runat="server" Text="Save" CssClass="btn  green btn_SaveNew" ValidationGroup="check_SubBox" OnClick="btn_SaveNew_Click" CommandArgument="" />
                                                    <asp:Button ID="btn_SaveEdit" runat="server" Text="Save Changes" CssClass="btn btn-block green btn_SaveEdit" ValidationGroup="check_SubBox" OnClick="btn_SaveEdit_Click" Style="display: none" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Save button -->

            </div>
            <!-- END NEW SUB-BOX DIV -->
        </div>
    </div>
</asp:Content>



<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="/assets/scripts/app.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/app.js"></script>
    <script src="/assets/scripts/table-advanced.js"></script>
    <script type="text/javascript" src="assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" ></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" ></script>
    <script type="text/javascript" src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" ></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js" ></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js"></script>
    <script type="text/javascript" src="assets/scripts/app.js"></script>
    <script type="text/javascript" src="assets/scripts/form-components.js"></script>
    <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>

    <script src="Scripts/siteJS.js" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();

            FormComponents.init();
        });
    </script>
</asp:Content>
