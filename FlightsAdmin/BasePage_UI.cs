﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Generic;
using DL_LowCost;
using BL_LowCost;

namespace FlightsAdmin
{
    public abstract class BasePage_UI : BasePage
    {
        #region Scripter
        public List<string> ScriptListToCall { get; set; }
        public void AddScript(string script)
        {
            if (ScriptListToCall == null)
            {
                ScriptListToCall = new List<string>();
            }
            ScriptListToCall.Add(script);
        }
        /// <summary>
        /// Set scripts on the page
        /// </summary>
        public void CallPageScript()
        {
            if (ScriptListToCall != null)
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "MyScript_PAGE", string.Format("{0};", string.Join(";", ScriptListToCall)), true);
            }
        }

        /// <summary>
        /// update the sessions when page load complite 
        /// </summary>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            SaveToSession();
            CallPageScript();
        }
        #endregion

        #region Session Methods

        /// <summary>
        /// Load the all containers that the page uses (preferably only 1) from the session.
        /// </summary>
        /// <param name="isPostBack"></param>
        internal abstract void LoadFromSession(bool isPostBack);

        /// <summary>
        /// Save the containers to the sessions
        /// </summary>
        internal abstract void SaveToSession();


        /// <summary>
        /// A method that resets all session properties. 
        /// </summary>
        internal abstract void resetAllSessions();


        #endregion

        #region GetText

        public override string GetText(string text)
        {
            try
            {
                return StringHandler.ConvertToFriendlyString(GetGlobalResourceObject(Lang, text).ToString());
            }
            catch (Exception)
            {
                return "";
            }
        }

        #endregion

        #region Main Object Session Handler
         public Users AdminObject
        {
            get
            {
                Users _AdminObject;
                if (SessionManager.IsAdminExist())
                {
                    _AdminObject = SessionManager.GetAdminFromSession();
                }
                else
                {
                    _AdminObject = null;
                    Response.Redirect("Login.aspx");
                }
                return _AdminObject;
            }
        }
        #endregion

        //     public override string GetText(string text)
        //     {
        //         try
        //         {
        //             System.Resources.ResourceManager temp =
        //                 new System.Resources.ResourceManager("Resources." + Lang,
        //System.Reflection.Assembly.GetExecutingAssembly());
        //             return temp.GetString(text);

        //         }
        //         catch (Exception)
        //         {
        //             return "";
        //         }
        //     }
    }
}