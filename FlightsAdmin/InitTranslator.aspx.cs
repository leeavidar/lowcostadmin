﻿using DL_Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class InitTranslator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TranslatorApp.InitTranslatedList();
            Response.Redirect(StaticStrings.path_HomePage);
        }
    }
}