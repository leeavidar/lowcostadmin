﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{
    public partial class Tips : BasePage_UI
    {
        #region Session Private fields

        private TipContainer _oSessionTipContainer;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<TipContainer>(out _oSessionTipContainer) == false)
            {
                //take the data from the table in the database
                _oSessionTipContainer = GetTipsFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            // The function 'SetSession' works on a given type (ex. YTable), and gets the session object (ex. oSessionYTable)
            Generic.SessionManager.SetSession<TipContainer>(oSessionTipContainer);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<TipContainer>(oSessionTipContainer);

        }


        /// <summary>
        /// A method that gets all the Tips of a white label from the database.
        /// </summary>
        /// <returns></returns>
        private TipContainer GetTipsFromDB()
        {
            //selecting all the Tips with a given white label
            TipContainer allTips = BL_LowCost.TipContainer.SelectAllTips(1, null);  //TODO:  replace with "WhiteLabelId"
            return allTips;
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }

        #endregion

        protected void btn_first_Click(object sender, EventArgs e)
        {
            LinkButton setFirstButton = (LinkButton)sender;
            int tipDocId = ConvertToValue.ConvertToInt(setFirstButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(tipDocId))
            {
                TipContainer oTipContainer = TipContainer.SelectByID(tipDocId, BasePage.WhiteLabelObj.DocId_Value, null); // TODO: replace the 1 with a real WhiteLabel //21/9/14 -yarin .Done

                if (oTipContainer != null && oTipContainer.Count > 0)
                {
                    if (oTipContainer.Single != null)
                    {
                        var oTipToUpdate = oTipContainer.Single;
                        if (oTipToUpdate.Order_Value != 1)
                        {
                            //change order number for another tips
                            var oAllTipsContainer = TipContainer.SelectAllTips(BasePage.WhiteLabelObj.DocId_Value, null);
                            oAllTipsContainer.Remove(oTipToUpdate.DocId_Value);
                            oAllTipsContainer.SortByKey(KeyValuesType.KeyTipOrder, false);
                            int lastOrder = 0;
                            foreach (var item in oAllTipsContainer.DataSource)
                            {
                                var itemOrder = item.Order_Value;
                                if (item.Order_Value >= 1)
                                {
                                    if (lastOrder == 0 || lastOrder + 1 == itemOrder)
                                    {
                                        item.Order = itemOrder + 1;
                                        item.Action(DB_Actions.Update);
                                    }
                                }
                                lastOrder = itemOrder;
                            }
                            oTipToUpdate.Order = 1;
                            oTipToUpdate.Action(DB_Actions.Update);
                        }
                        else
                        { 
                        //הטיפ כבר במקום 1
                        }
                    }
                }
            }
            oSessionTipContainer = GetTipsFromDB();
            BindDataToRepeater();
        }


    }

    public partial class Tips : BasePage_UI
    {
        #region Properties

        #region Private
        private int _whiteLabelId;
        #endregion
        #region Public
        public TipContainer oSessionTipContainer { get { return _oSessionTipContainer; } set { _oSessionTipContainer = value; } }
        public int WhiteLabelId
        {
            get { return _whiteLabelId; }
            set { _whiteLabelId = value; }
        }
        #endregion

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            if (!IsPostBack)
            {
                BindDataToRepeater();

                TipPanel.Style.Add("display", "none");
            }
        }


        #region Event Handlers
        
        protected void btn_Save_New_Click(object sender, EventArgs e)
        {
            // Check validations
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string title = txt_Title.Text;
            string shortText = txt_ShortText.Text;
            string longText = Server.HtmlDecode(txt_LongText.Value); 
            DateTime datetime = ConvertToValue.ConvertToDateTime(txt_Date.Text);
            int order = ConvertToValue.ConvertToInt(txt_Order.Text);

            #endregion

            Tip oTip = new Tip()
            {
                Title = title,
                ShortText = shortText,
                LongText = longText,
                Date = datetime,
                Order = order,
                WhiteLabelDocId = 1 // TODO: replace with Whitelabel
            };
            //change order number for another tips
            var oTipContainer = TipContainer.SelectAllTips(BasePage.WhiteLabelObj.DocId_Value, null);
            oTipContainer.SortByKey(KeyValuesType.KeyTipOrder,false);
            int lastOrder = 0;
            foreach (var item in oTipContainer.DataSource)
            {
                var itemOrder = item.Order_Value;
                if (item.Order_Value >= order)
                {
                    if (lastOrder == 0 || lastOrder + 1 == itemOrder)
                    {
                        item.Order = itemOrder + 1;
                        item.Action(DB_Actions.Update);
                    }
                }
                lastOrder = itemOrder;
            }
            // Save the new tip to the DB, and update the session
            oTip.Action(DB_Actions.Insert);
            
            oSessionTipContainer = GetTipsFromDB();
            BindDataToRepeater();
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                return;
            }

            #region Getting the values from the fields
            string title = txt_Title.Text;
            string shortText = txt_ShortText.Text;
            string longText = Server.HtmlDecode(txt_LongText.Value);
            DateTime datetime =txt_Date.Text !=""? ConvertToValue.ConvertToDateTime(txt_Date.Text) : DateTime.Today ;
            int order = ConvertToValue.ConvertToInt(txt_Order.Text);

            #endregion

            Button saveButton = (Button)sender;
            int tipDocId = ConvertToValue.ConvertToInt(saveButton.CommandArgument);
            if (!ConvertToValue.IsEmpty(tipDocId))
            {
                TipContainer oTipContainer = TipContainer.SelectByID(tipDocId,BasePage.WhiteLabelObj.DocId_Value, null); // TODO: replace the 1 with a real WhiteLabel //21/9/14 -yarin .Done

                if (oTipContainer != null && oTipContainer.Count > 0)
                {
                    Tip TipToEdit = oTipContainer.Single;
                    var originalOrder = TipToEdit.Order;
                    #region Fill tip properties
                    TipToEdit.Title = title;
                    TipToEdit.ShortText = shortText;
                    TipToEdit.LongText = longText;
                    TipToEdit.Date = datetime;
                    TipToEdit.Order = order;
                    #endregion
                    //change order number for another tips
                    var oAllTipsContainer = TipContainer.SelectAllTips(BasePage.WhiteLabelObj.DocId_Value, null);
                    oAllTipsContainer.Remove(TipToEdit.DocId_Value);
                    oAllTipsContainer.SortByKey(KeyValuesType.KeyTipOrder, false);
                    int lastOrder = 0;
                    foreach (var item in oAllTipsContainer.DataSource)
                    {
                        var itemOrder = item.Order_Value;
                        if (originalOrder < order) //אם הוריד אותו למטה ברשימה
                        {
                            if (itemOrder > originalOrder)
                            {
                               
                                    item.Order = itemOrder - 1;
                                    item.Action(DB_Actions.Update);
                           
                            }
                            lastOrder = itemOrder;
                        }
                        else //אם קודם ברשימה למעלה
                        {
                            if (itemOrder >= order && itemOrder <= originalOrder)
                            {
                                if (lastOrder == 0 || lastOrder + 1 == itemOrder)
                                {
                                item.Order = itemOrder + 1;
                                item.Action(DB_Actions.Update);
                                }
                            }
                            lastOrder = itemOrder;
                        }
                        
                       
                    }
                    TipToEdit.Action(DB_Actions.Update);

                    // Switch buttons:
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");
                    // Hide the edit panel and change the button that shows the panel.
                    TipPanel.Style.Add("display", "none");
                    btn_AddNewTip.Value = "Add a new Tip";

                    oSessionTipContainer = GetTipsFromDB();
                    BindDataToRepeater();
                }
            }
        }

        /// <summary>
        /// Fill the sub box panel with data from the sub box in the session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            // LoadFromSession(false);

            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the Tip to edit from the command argument of the button
            int TipDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(TipDocId))
            {
                TipContainer oTipContainer = oSessionTipContainer.SelectByID(TipDocId,BasePage.WhiteLabelObj.DocId_Value); // TODO: replace 1 with whitelable.Done
                if (oTipContainer != null)
                {
                    Tip oTip = oTipContainer.Single;
                    if (oTip != null)
                    {
                        #region Fill the tip panel with data.
                        txt_Title.Text = oTip.Title_UI;
                        txt_ShortText.Text = oTip.ShortText_UI;
                        txt_LongText.InnerHtml = oTip.LongText_UI;
                        txt_Order.Text = oTip.Order_UI;
                        txt_Date.Text = oTip.Date_UI;
                        #endregion

                        // Displaying the SaveEdit button (and hiding the other one)
                        btn_SaveNew.Style.Add("display", "none");
                        btn_SaveEdit.Style.Add("display", "normal");
                        // Unhide the Tip panel for editing
                        TipPanel.Style.Add("display", "normal");
                        btn_SaveEdit.CommandArgument = TipDocId.ToString();
                        btn_AddNewTip.Value = "Cancel";
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int TipDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(TipDocId))
            {
                TipContainer oTipContainer = oSessionTipContainer.SelectByID(TipDocId, 1); // TODO: replace 1 with whitelable
                if (oTipContainer != null)
                {
                    Tip oTip = oTipContainer.Single;
                    if (oTip != null)
                    {
                        oTip.Action(DB_Actions.Delete);
                        oSessionTipContainer = GetTipsFromDB();
                        BindDataToRepeater();
                    }
                }
            }
        }

        #endregion

        #region Helping methods
        private void BindDataToRepeater()
        {
            drp_Tips.DataSource = oSessionTipContainer.DataSource;
            drp_Tips.DataBind();
        }

        private void ClearTipPanel()
        {
            txt_Title.Text = "";
            txt_ShortText.Text = "";
            txt_LongText.InnerHtml = "";
            txt_Order.Text = "1";
            txt_Date.Text = DateTime.Today.ToShortDateString();

        }
        #endregion
    }
}