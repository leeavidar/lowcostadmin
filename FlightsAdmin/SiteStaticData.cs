﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightsAdmin.FlightsCommonService;

namespace FlightsAdmin
{
    public static class SiteStaticData
    {
        //A propert holding a collection of all the airports (userd by the autocomplete)
        public static AirportsCollection AirportsCollection { get; set; }
        //A propert holding a collection of all the cities (userd by the autocomplete)
        public static CitiesCollection CitiesCollection { get; set; }


        /// <summary>
        /// A static constructor that filles the properties (collections of Airports and Cities), to be used by the autocomplete in the search engine.
        /// This method is being called in the Global.asax file.
        /// </summary>
        //static SiteStaticData()
        //{
        //    //Load the data from the service
        //    using (var flightsService = new FlightsCommonService.FlightsCommonService())
        //    {
        //        SiteStaticData.AirportsCollection = flightsService.GetAirports();
        //        SiteStaticData.CitiesCollection = flightsService.GetCities();
        //    }
        //}

        /// <summary>
        /// This method gets a list of airports IATA codes.
        /// </summary>
        /// <returns>A list of all airports IATA codes</returns>
        public static List<String> GetAirports()
        {
            if (AirportsCollection == null)
            {
                return new List<string>();
            }
            return AirportsCollection.Airports.ToList().ConvertAll(input => input.IATACode).ToList();
        }

        /// <summary>
        /// This method gets a list of all the cities IATA codes.
        /// </summary>
        /// <returns>A list of all the cities IATA codes</returns>
        public static List<String> GetCities()
        {
            if (CitiesCollection == null)
            {
                return new List<string>();
            }
            return CitiesCollection.Cities.ToList().ConvertAll(input => input.IATACode).ToList();
        }

        /// <summary>
        /// Gets a double represinting time in minutes, and convert it to a string in the form of HH:MM
        /// </summary>
        /// <param name="timeMinutes">Time in minutes</param>
        /// <returns>Returns the time in a HH:MM format</returns>
    //    public static string TimeMinutesToHours(double timeMinutes)
    //    {
    //        int hours = 0;
    //        //A variable to hold the time in a string format
    //        string timeinHoursMinutesFormat;
    //        //Every 60 minutes adding 1 hour and substracting 60 from the minutes.
    //        while (timeMinutes >= 60)
    //        {
    //            hours++;
    //            timeMinutes -= 60;
    //        }
    //        //Buiding the time in a HH:MM format
    //        timeinHoursMinutesFormat = hours + ":" + timeMinutes;
    //        return timeinHoursMinutesFormat;
    //    }
    }
}