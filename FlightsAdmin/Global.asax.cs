﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.Web.Optimization;

using Generic;
namespace FlightsAdmin
{
    public class Global : HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterOpenAuth();
            RouteConfig.RegisterRoutes(RouteTable.Routes);


            StaticData oStaticData = new StaticData();

            //Load data from service (this may take a long time)
            //using (var service = new FlightsCommonService.FlightsCommonService())
            //{
            //    SiteStaticData.AirportsCollection = service.GetAirports();
            //    SiteStaticData.CitiesCollection = service.GetCities();
            //}
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (SessionManager.GetAdminFromSession() == null)
            {
                Response.Redirect(StaticStrings.path_Login);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}