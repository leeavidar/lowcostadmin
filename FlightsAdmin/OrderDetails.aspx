﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="FlightsAdmin.OrderDetails" %>

<%@ Register Src="~/Controls/uc_FlightLegRepeater.ascx" TagPrefix="flightsusercontrols" TagName="uc_FlightLegRepeater" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />

    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />

    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">

    <link rel="stylesheet" type="text/css" href="Content/Site.css">
    <!-- END PAGE LEVEL STYLES -->
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div>
        <div class="row">
            <a href="<%=FlightsAdmin.StaticStrings.path_OrderManagement %>" class="btn btn-info">Back to order list</a>
          
        </div>
        <div class="row">
            <div class="col-md-3 OrderDetails">
                <table>
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <h3>Order Details:</h3>
                            </td>
                        </tr>
                        <tr>
                            <td>Time of booking:</td>
                            <td>
                                <asp:Label ID="lbl_TimeOfBook" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>LowCost PNR:</td>
                            <td>
                                <asp:Label ID="lbl_LowCostPnr" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>Airline PNR:</td>
                            <td>
                                <asp:Label ID="lbl_SupplierPnr" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>Price:</td>
                            <td>
                                <asp:Label ID="lbl_TotalPrice" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Payment:</td>
                            <td>
                                <asp:Label ID="lbl_Payment" runat="server" /></td>
                        </tr>
                        <tr>
                            <asp:UpdatePanel ID="up_OrderStatus" runat="server">
                                <ContentTemplate>
                                    <td>Status:</td>
                                    <td>
                                        <asp:Label ID="lbl_Status" runat="server" />
                                    </td>
                                    <td>
                                        <input id='btn_ChangeStatus' type="button" value="Change Status" onclick='ShowChangeStatusMenu(this)' />
                                        <div id='changeStatusMenu' style="display: none; position: relative; top: 20px; left: 20px; width: 200px; height: 70px; background-color: #c4e6ff; border: 1px solid black; margin: 10px;">
                                            <span>Choose status: </span>
                                            <asp:HiddenField ID="hf_ChangedStatus" runat="server" ClientIDMode="Static" />
                                            <asp:DropDownList AutoPostBack="true" ID="ddl_ChooseStatus" CssClass="ddl_ChooseStatus" runat="server" DataValueField='<%# Eval("DocId_UI")%>' onchange="KeepNewStatus()" OnSelectedIndexChanged="ddl_ChooseStatus_SelectedIndexChanged">
                                                <asp:ListItem Value="1" Text="Succeeded" />
                                                <asp:ListItem Value="2" Text="Booking In Progress" />
                                                <asp:ListItem Value="3" Text="Failed" />
                                                <asp:ListItem Value="4" Text="Unconfirmed" />
                                                <asp:ListItem Value="5" Text="Unconfirmed By Supplier" />
                                                <asp:ListItem Value="6" Text="Duplicate Booking" />
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </tr>
                        <tr>
                            <td>Origin:</td>
                            <td>
                                <asp:Label ID="lbl_Origin" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Destination:</td>
                            <td>
                                <asp:Label ID="lbl_Destination" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Outward Date:</td>
                            <td>
                                <asp:Label ID="lbl_OutwardDate" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Return Date:</td>
                            <td>
                                <asp:Label ID="lbl_ReturnDate" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Flight Number:</td>
                            <td>
                                <asp:Label ID="lbl_FlightNumber" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>Ariline Name:</td>
                            <td>
                                <asp:Label ID="lbl_AirlineName" runat="server" /></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="col-md-3 ContactDetails">
                <table>
                    <tr>
                        <td colspan="2">
                            <h3>Contact Details:</h3>
                        </td>
                    </tr>
                    
                     <tr>
                        <td>Full Name: </td>
                        <td>
                            <asp:Label ID="lbl_FullName" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Phone Number: </td>
                        <td>
                            <asp:Label ID="lbl_PhoneNumber" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Second Phone Number: </td>
                        <td>
                            <asp:Label ID="lbl_SecondPhoneNumber" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Email address: </td>
                        <td>
                            <asp:Label ID="lbl_EmailAddress" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Home address: </td>
                        <td>
                            <asp:Label ID="lbl_HomeAddress" runat="server" /></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-3 PassengerDetails">
                <table>
                    <tr>
                        <td colspan="2">
                            <h3>Passenger Details:</h3>
                            <%=FlightsAdmin.OrderManager.GetPassengersDetails(oSessionOrder) %>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </div>
            <div class="col-md-3 ContactDetails">
                <table>
                    <tr>
                        <td colspan="2">
                            <h3>Payment Details:</h3>
                        </td>
                    </tr>
                    
                     <tr>
                        <td>Payment Status: </td>
                        <td>
                            <asp:Label ID="lbl_paymentStatus" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Transaction Number: </td>
                        <td>
                            <asp:Label ID="lbl_TransactionNum" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Payment Token: </td>
                        <td>
                            <asp:Label ID="lbl_PaymentToken" runat="server" /></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="wrap_table_inside col-md-12 FlightDetails">
                <h3>Flight Details:</h3>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Outward:
                        </h4>
                        <!-- Nested repeater showing all OUTWARD flight legs -->
                        <div>
                            <flightsusercontrols:uc_FlightLegRepeater runat="server" ID="uc_FlightLegRepeaterOutward" ClientIDMode="Static" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Return:
                        </h4>
                        <!-- Nested repeater showing all RETURN flight legs -->
                        <flightsusercontrols:uc_FlightLegRepeater runat="server" ID="uc_FlightLegRepeaterReturn" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <script>

        $(function () {
            $(this).css({ 'left': $(this).position().left + 20 + "px", 'top': '-20px' })
        });
        // A function that toggles the change status div
        function ShowChangeStatusMenu(obj) {
            $('#changeStatusMenu').slideToggle(100);
            if ($('#' + obj.id).val() == 'Change Status')
                $('#' + obj.id).val('Cancel');
            else {
                $('#' + obj.id).val('Change Status');
            }
        }

        function KeepNewStatus() {
            debugger;
            var selectedValue = $('.ddl_ChooseStatus').val();
            $("#hf_ChangedStatus").val(selectedValue);
        }
    </script>
</asp:Content>
