﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="Tips.aspx.cs" Inherits="FlightsAdmin.Tips" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/pages/portfolio.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />

    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />

    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Low Cost Flights Tips"></asp:Label>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>Tips table
                    </div>

                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                        <asp:Repeater ID="drp_Tips" runat="server" ItemType="DL_LowCost.Tip">
                            <HeaderTemplate>
                                <thead>
                                    <th>Order</th>
                                    <th>Tip title</th>
                                    <th>Tip short text</th>
                                    <th class="hidden-xs">Date</th>
                                    <th></th>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <!-- The order is a priority-->
                                    <td>
                                        <span id="lbl_Order"><%# Eval("Order_UI") %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_Title"><%# Eval("Title_UI") %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_ShortText"><%# Eval("ShortText_UI") %></span>
                                    </td>
                                    <td class="hidden-xs">
                                        <span id="lbl_Type"><%# Eval("Date_UI") %></span>
                                    </td>
                                    <td>
                                        <!--Edit button  -->
                                        <asp:LinkButton ID="btn_edit" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClick="btn_Edit_Click"><i class="fa fa-edit"></i>  Edit</asp:LinkButton>
                                        <!-- Delete button -->
                                        <asp:LinkButton ID="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                         <!-- MoveToFirst button -->
                                        <asp:LinkButton ID="btn_first" runat="server" CssClass="btn default btn-sm blue" OnClick="btn_first_Click" CommandArgument='<%# Eval("DocId_UI") %>'><i class="fa fa-level-up"></i> First</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-md-12 ">
            <!-- Add a new TIP button - The value of this button will change from "Add a new Tip" to "Cancel" and open or close the tip panel -->
            <input type="button" id="btn_AddNewTip" class="btn green btn_AddNewTip" value="Add a new Tip" runat="server" />
        </div>
    </div>

    <!-- START - ADD/EDIT PANEL-->
    <div id="TipPanel" class="row  mt10 TipPanel" runat="server" style="display: none;">
        <div class="col-md-12">
            <div class="portlet  box grey">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-search"></i>Add/Edit tip
                    </div>
                   
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <!-- First row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Tip Title</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Title" class="form-control input-medium txt_Title" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Short Text</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_ShortText" class="form-control input-medium txt_ShortText" TextMode="MultiLine" runat="server"></asp:TextBox>
                                            <span class="help-block">This tip appear in the Home Page
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Long Text</label>
                                        <div class="col-md-9">
                                            <textarea id="txt_LongText" name="content" data-provide="markdown" runat="server" rows="10" data-width="400" class="form-control txt_LongText"></textarea>
                                            <span class="help-block">This tip appear in Tips Page
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Order</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Order" class="form-control input-medium txt_Order" TextMode="Number" runat="server"></asp:TextBox>
                                            <asp:RangeValidator ID="val_OrderRange1" ValidationGroup="check_tip" runat="server" ErrorMessage="RangeValidator" Type="Integer" ControlToValidate="txt_Order" MinimumValue="1" MaximumValue="2147483647"></asp:RangeValidator>
                                            <span class="help-block">The number must be positive, and larger than 0.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_Date" CssClass="txt_Date form-control form-control-inline input-medium date-picker" runat="server" />
                                            <!--התאריך יופיע דיפולטיבי לאותו יום עם אופציה לשינוי-->
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- Second row -->

                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <!-- This button's command arguments is set in a siteJS.js file  (SaveNew or SaveEdit) -->
                                        <asp:Button class="btn green btn_SaveNew" ID="btn_SaveNew" runat="server" Text="Save" ValidationGroup="check_tip" OnClick="btn_Save_New_Click" />
                                        <asp:Button class="btn green btn_SaveEdit" ID="btn_SaveEdit" runat="server" Text="Save Changes" ValidationGroup="check_tip" OnClick="btn_SaveEdit_Click" Style="display: none" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- END - ADD/EDIT PANEL-->

</asp:Content>
<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/table-advanced.js"></script>
    <script type="text/javascript" src="assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="assets/scripts/form-components.js"></script>

    <script src="Scripts/siteJS.js" type="text/javascript"></script>
    <script src="Scripts/PageScripts/Tips.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();
            FormComponents.init();
        });
    </script>
</asp:Content>
