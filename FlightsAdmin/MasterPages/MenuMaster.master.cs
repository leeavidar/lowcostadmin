﻿using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin.MasterPage
{
    public partial class MenuMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Users _AdminObject = new Users();
            if (SessionManager.IsAdminExist())
            {
                _AdminObject = SessionManager.GetAdminFromSession();

                lbl_UserName.Text = _AdminObject.UserName_UI;
            }
            else
            {
                _AdminObject = null;
                Response.Redirect(StaticStrings.path_Login);
            }

            // Show only the  menu items relevant to the user's permissions:

            // if the role is Admin:

            switch (_AdminObject.Role_Value)
            {
                case (int)EnumHandler.UserRoles.Admin:
                    li_OrderManagemenu.Visible = true;
                    li_UpdateFlightDetails.Visible = true;
                    li_Admins.Visible = true;
                    li_StaticPages.Visible = true;
                    li_DynamicPages.Visible = true;
                    li_DestinationPages.Visible = true;
                    li_CompanyPages.Visible = true;
                    li_AirlineFares.Visible = true;

                    li_CountriesIndex.Visible = true;
                    li_CitiesIndex.Visible = true;
                    li_AirportsIndex.Visible = true;
                    li_AirlinesIndex.Visible = true;

                    li_HomePages.Visible = true;
                    li_Tips.Visible = true;
                    li_MailingList.Visible = true;
                    li_Faq.Visible = true;
                    
                    break;
                case (int)EnumHandler.UserRoles.SalesOperator:
                    li_OrderManagemenu.Visible = true;
                    li_UpdateFlightDetails.Visible = true;
                    li_Admins.Visible = false;
                    li_StaticPages.Visible = false;
                    li_DynamicPages.Visible = false;
                    li_DestinationPages.Visible = false;
                    li_CompanyPages.Visible = false;
                    li_AirlineFares.Visible = false;

                    
                    li_CountriesIndex.Visible = false;
                    li_CitiesIndex.Visible = false;
                    li_AirportsIndex.Visible = false;
                    li_AirlinesIndex.Visible = false;


                    li_HomePages.Visible = false;
                    li_Tips.Visible = false;
                    li_MailingList.Visible = true;
                    li_Faq.Visible = false;
                    break;
                case (int)EnumHandler.UserRoles.SiteOperator:
                    li_OrderManagemenu.Visible = false;
                    li_UpdateFlightDetails.Visible = false;
                    li_Admins.Visible = false;
                    li_StaticPages.Visible = true;
                    li_DynamicPages.Visible = true;
                    li_DestinationPages.Visible = true;
                    li_CompanyPages.Visible = true;
                    li_AirlineFares.Visible = true;

                    li_CountriesIndex.Visible = true;
                    li_CitiesIndex.Visible = true;
                    li_AirportsIndex.Visible = true;
                    li_AirlinesIndex.Visible = true;

                    li_HomePages.Visible = true;
                    li_Tips.Visible = true;
                    li_MailingList.Visible = false;
                    li_Faq.Visible = true;
                    break;

                default:
                    break;
            }




            // TODO: ENABLE THIS PART WHEN NOT DEBUGGING



        }

        protected void btn_Logout_Click(object sender, EventArgs e)
        {
            SessionManager.ResetAdminSession();
            Response.Redirect(StaticStrings.path_Login);
        }

        /// <summary>
        /// A method that gives permission to view each page, by the role of the connected user.
        /// </summary>
        public void PagePermissions(EnumHandler.UserRoles permittedUsers)
        {
            Users _AdminObject = new Users();
            if (SessionManager.IsAdminExist())
            {
                _AdminObject = SessionManager.GetAdminFromSession();


                // IF no admin is logged in, redirect to login page
                if (_AdminObject == null)
                {
                    Response.Redirect(StaticStrings.path_Login);
                }

                if (_AdminObject.Role_Value == (int)EnumHandler.UserRoles.Admin || _AdminObject.Role_Value == (int)EnumHandler.UserRoles.ChiefAdmin)
                {
                    // Admin has full aceess to all the pages.
                    return;
                }
                if (_AdminObject.Role_Value != (int)permittedUsers)
                {
                    // if the role is of the user doesn't match the page permissions, he will be redirected to the login page. 
                    Response.Redirect(StaticStrings.path_Login);
                }
                // if the role matches the page permissions , the user will be permitted to enter the page.
            }
        }
    }
}