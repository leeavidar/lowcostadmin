﻿using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace FlightsAdmin
{
    /// <summary>
    /// Summary description for Autocomplete
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class Autocomplete : System.Web.Services.WebService
    {
        [ScriptMethod()]
        [WebMethod(EnableSession = true)]
        public List<string> SearchCities(string prefixText, int count )
        {
            CityContainer oCityContainer = (HttpContext.Current.Session["CityContainer"] != null) ? (CityContainer)HttpContext.Current.Session["CityContainer"] : CityContainer.SelectAllCitys(null);            

            List<string> cities = new List<string>();
            foreach (City item in oCityContainer.DataSource)
            {
                if (item.Name_UI.ToLower().Contains(prefixText.ToLower()))
                {
                    string city = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(item.Name_UI, item.DocId_UI);
                    cities.Add(city);
                }
            }
            return cities;
        }
    }
}
