﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DL_Generic;

namespace FlightsAdmin
{
    public static class SetLinkHandler
    {
        private static string link = "";

        public static string SetLink(Generic.EnumHandler.LinkPages Page, Generic.EnumHandler.QueryStrings ParamType, string URLValue, string IdValue, string SubDomain = "", bool IsGetURLByID = false)
        {
            bool LocalMode = ConvertToValue.ConvertToBool(System.Configuration.ConfigurationManager.AppSettings["LocalMode"]);

            #region Test URI
            //Uri uri = new Uri("http://en.cruiseit.algomasystems.com/");
            //string[] arrayHost = uri.Host.ToString().Split('.');
            //SubDomain = "he";
            #endregion
            string[] arrayHost = HttpContext.Current.Request.Url.Host.ToString().Split('.');
            if (SubDomain == "")
            {
                if (LocalMode)
                {
                    link = "http://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
                }
                else
                {
                    link = "http://" + HttpContext.Current.Request.Url.Host;
                }
            }
            else
            {
                if ((arrayHost.Contains("he") || arrayHost.Contains("en") || arrayHost.Contains("ru") || arrayHost.Contains("tr")))
                {
                    arrayHost[0] = SubDomain;
                    if (LocalMode)
                    {
                        link = "http://" + string.Join(".", arrayHost) + ":" + HttpContext.Current.Request.Url.Port;
                    }
                    else
                    {
                        link = "http://" + string.Join(".", arrayHost);
                    }
                }
                else
                {
                    if (LocalMode)
                    {
                        link = "http://" + SubDomain + "." + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
                    }
                    else
                    {
                        link = "http://" + SubDomain + "." + HttpContext.Current.Request.Url.Host;
                    }
                }
            }
            switch (Page)
            {
                //Use Break to continue building the link with the parameters
                #region Pages With Parameters

                //case Generic.EnumHandler.LinkPages.Users:
                //    link += "/user";
                //    break;

                #endregion

                //use Return to return the link made - no use of extra parameters
                #region Pages Without Parameters

                case Generic.EnumHandler.LinkPages.Home:
                    return link + "/";

                #endregion

                default:
                    break;
            }
            if (IsGetURLByID || ParamType == Generic.EnumHandler.QueryStrings.ID)
            {
                switch (Page)
                {
                    case Generic.EnumHandler.LinkPages.Home:
                        break;

                    ///Example For Retrieveing URL Friendly from ID
                    ///this example suld be used for all pages that support Friendly URL Functionality from DB
                    //case Generic.EnumHandler.LinkPages.Company:
                    //CompanyContainer oCompanyContainer = CompanyContainer.SelectCompanyByDocId(ConvertToValue.ConvertToInt(IdValue));
                    //if (oCompanyContainer.Single != null)
                    //{
                    //    if (oCompanyContainer.Single.UrlFriendly_UI.Length > 0)
                    //    {
                    //        link += "/" + oCompanyContainer.Single.UrlFriendly_UI;
                    //    }
                    //    else
                    //    {
                    //        CheckID(IdValue);
                    //    }
                    //    return link;
                    //}
                    //else
                    //{
                    //    CheckID(IdValue);
                    //    return link;
                    //}
                }
            }

            if (URLValue.Length == 0)
            {
                CheckID(IdValue);
            }
            else
            {
                link += "/" + URLValue;
            }

            return link;
        }

        private static void CheckID(string IdValue)
        {
            if (ConvertToValue.ConvertToInt(IdValue) > 0)
            {
                link += "/" + IdValue;
            }
            else
            {
                link += "/";
            }
        }
    }
}