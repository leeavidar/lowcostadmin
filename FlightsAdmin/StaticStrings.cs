﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightsAdmin
{
    public static class StaticStrings
    {
        #region Folder paths
        public static readonly string path_Root = @"http://localhost:8239/";

        public static  string path_Uploads { get { return System.Configuration.ConfigurationManager.AppSettings["ImageUploads"].ToString(); } }
        public static string UploadsDirectory { get { return System.Configuration.ConfigurationManager.AppSettings["UploadsDirectory"].ToString(); } }

        #endregion

        #region User Controls path
        public static readonly string path_FlightSegmentUC = @"\Controls\SegmentFlightUC.ascx";
        public static readonly string path_UC_SubBox = @"\Controls\Templates\UC_SubBox.ascx"; 
        #endregion


        #region Page addresses
        public static readonly string path_AirlineImages = @"http://www.travelfusion.com/images/logos/";
        public static readonly string path_HomePage = @"Home_Page.aspx";
        public static readonly string path_Login = @"Login.aspx";


        public static readonly string path_Index = @"Index.aspx";
        public static readonly string path_StaticPageEditor = @"Static_Page_Editor";
        public static readonly string path_StaticPageIndex = @"Static_Page_Index";
        public static readonly string path_TemplateEdit = @"TemplateEdit.aspx";
        public static readonly string path_TemplateIndex = @"Templates";
        public static readonly string path_DestinationIndex = @"DestinationPage_Index.aspx";
        public static readonly string path_DestinationEdit = @"DestinationPageEdit.aspx";
        public static readonly string path_CompanyPageIndex = @"CompanyPage_Index.aspx";
        public static readonly string path_CompanyPageEdit = @"CompanyPageEdit.aspx";
        public static readonly string path_Tips = @"Tips.aspx";
        public static readonly string path_Admins = @"Admins.aspx";
        public static readonly string path_CompanyPageTemplateEdit = @"CompanyPageTemplateEdit.aspx";
        public static readonly string path_MailingList = @"mailinglist.aspx";
        public static readonly string path_OrderDetails = @"OrderDetails.aspx";
        public static readonly string path_OrderManagement = @"Orders_Management.aspx"; 
        
        #endregion


        #region Session Names

        public static readonly string ses_OutwardFlightId = @"outwardFlightId";
        public static readonly string ses_ReturnFlightId = @"returnFlightId";
        public static readonly string ses_RoutingId = @"routingId";

        public static readonly string ses_oFlightsAvailResults = @"oFlightsAvailResults";
        public static readonly string ses_oBookingDetails = @"oBookingDetails";


        #endregion
        #region flight Types
        public static readonly string flightTypeOutward = "outward";
        public static readonly string flightTypeReturn = "return";
       
        #endregion



      
    }


}