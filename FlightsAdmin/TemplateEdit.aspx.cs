﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightsAdmin
{

    public partial class TemplateEdit : BasePage_UI
    {
        #region Session Private fields

        private Template _oSessionTemplate;

        #endregion

        #region Sessions Methodes
        /// <summary>
        /// load the containers fron the sessions
        /// </summary>
        /// <param name="isPostBack"></param>
        internal override void LoadFromSession(bool isPostBack)
        {
            //if it's the first time the pages is loaded, or there is no container in the session
            if (!isPostBack || Generic.SessionManager.GetSession<Template>(out _oSessionTemplate) == false)
            {
                //take the data from the table in the database
                _oSessionTemplate = GetTemplateFromDB();
            }
            else { /*Return from session*/}
        }

        /// <summary>
        /// save the containers to the sessions
        /// </summary>
        internal override void SaveToSession()
        {
            Generic.SessionManager.SetSession<Template>(oSessionTemplate);
        }

        /// <summary>
        /// function to reset all sessions in the page
        /// </summary>
        internal override void resetAllSessions()
        {
            Generic.SessionManager.ClearSession<Template>(oSessionTemplate);
        }

        private Template GetTemplateFromDB()
        {
            //selecting the template with the TemplateDocId from the query string (and the white label)
            TemplateContainer oTemplateContainer = TemplateContainer.SelectByID(QueryStringManager.QueryStringID(EnumHandler.QueryStrings.ID), 1, null); // TODO: replace with "WhiteLabelId"
            if (oTemplateContainer.Count > 0)
            {
                Template oTemplate = oTemplateContainer.Single;
                IsNewTemplate = false;
                return oTemplate;
            }
            else
            {
                IsNewTemplate = true;
                return null;
            }
        }

        #endregion

        #region Events

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                resetAllSessions();
            }
            LoadFromSession(IsPostBack);
        }
     
        #endregion
    }

    public partial class TemplateEdit : BasePage_UI
    {
        #region Properties
        #region Private

        #endregion
        #region Public
        public Template oSessionTemplate { get { return _oSessionTemplate; } set { _oSessionTemplate = value; } }

        public int PageDocId { get; set; }

        public bool IsNewTemplate { get; set; }

        #endregion
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PagePermissions(EnumHandler.UserRoles.SiteOperator);
            //  Check if we are starting to edit a new template
            if (!IsNewTemplate)
            {
                //  << EDIT MODE >>
                //Set the properties of the image uploaders
                uc_ImageUploader_CenterWithImage.SetImageUploader(txt_ImageTitle_CenterWithImage.Text, txt_ImageAlt_CenterWithImage.Text, oSessionTemplate.DocId_Value, EnumHandler.RelatedObjects.Template, EnumHandler.ImageTypes.Main);
                uc_ImageUploader_SideWithImage.SetImageUploader(txt_ImageTitle_SideWithImage.Text, txt_ImageAlt_SideWithImage.Text, oSessionTemplate.DocId_Value, EnumHandler.RelatedObjects.Template, EnumHandler.ImageTypes.Main);

                // Save the DestinationPage DocId for later use.
                PageDocId = oSessionTemplate.RelatedObjectDocId_Value;

                if (!IsPostBack)
                {
                    PageInit();
                }

                // If there are subboxes for a template - load them into the sub boxes repeater
                if (oSessionTemplate != null && oSessionTemplate.SubBoxs != null && oSessionTemplate.SubBoxs.Count > 0)
                {
                    drp_SubBoxTable.DataSource = oSessionTemplate.SubBoxs.DataSource;
                    drp_SubBoxTable.DataBind();
                }
            }
            else // It is a new template
            {
                // Save the DestinationPage DocId for later use.
                PageDocId = ConvertToValue.ConvertToInt(QueryStringManager.QueryString(EnumHandler.QueryStrings.DestinationPageId));
                AddNewTemplate.Style.Add("display", "normal");
            }
        }

        /// <summary>
        /// Initializes the page. 
        /// If the page was navigated to using the edit button in the previous page, and there is a destinationPage id in the query string, change to 'edit' mode.
        /// In other case, stay in create a new page mode (to create a new static page)
        /// </summary>
        private void PageInit()
        {
            //When selecting an existing temlate to edit, Remove the menu for new templates
            AddNewTemplate.Style.Add("display", "none");
            //Change the page title to Edit...
            lbl_PageTitle.Text = string.Format("Edit \"{0}\" Page", oSessionTemplate.Title_UI);

            ShowOnlyRelevantTemplate();

            int TemplateId = ConvertToValue.ConvertToInt(QueryStringManager.QueryString(EnumHandler.QueryStrings.ID));
            FillTemplateInformation(TemplateId);


        }

        #region Event Handlers


        protected void btn_Save_SideWithImage_Click(object sender, EventArgs e)
        {
            // if anything enterd is invalid
            if (!IsValid)
            {
                AddScript(" SelectTemplate('template_SideWithImage');");
                return;
            }

            string shortText = txt_ShortText_SideWithImage.Text;
            string longText = Server.HtmlDecode(txt_LongText_tmp_SideWithImage.Value);
            string title = txt_Title_tmp_SideWithImage.Text;
            int order = ConvertToValue.ConvertToInt(txt_Order_tmp_SideWithImage.Text);

            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }

            if (oSessionTemplate == null)
            {
                // << NEW PAGE MODE >>

                //Create a new empty static page
                oSessionTemplate = new Template();
                //Fill its title and html content
                oSessionTemplate.Title = title;
                oSessionTemplate.TextShort = shortText;
                oSessionTemplate.TextLong = Server.HtmlDecode(txt_LongText_tmp_SideWithImage.Value);
                oSessionTemplate.Order = order;
                oSessionTemplate.Type = EnumHandler.TemplateType.SideWithImage.ToString();
                oSessionTemplate.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.
                oSessionTemplate.RelatedObjectDocId = PageDocId;
                oSessionTemplate.RelatedObjectType = (int)EnumHandler.RelatedObjects.DestinationPage;
                // Insert the new template to the DB.
                int result = oSessionTemplate.Action(DL_Generic.DB_Actions.Insert);
                if (result != -1)
                {
                    // upload the image and add it to the DB:
                    uc_ImageUploader_SideWithImage.SetImageUploader(txt_ImageTitle_SideWithImage.Text, txt_ImageAlt_SideWithImage.Text, oSessionTemplate.DocId_Value, EnumHandler.RelatedObjects.Template, EnumHandler.ImageTypes.Main);
                    uc_ImageUploader_SideWithImage.SaveImage();

                }

            }
            else
            {
                //  << EDIT MODE >>

                oSessionTemplate.Title = title;
                oSessionTemplate.TextShort = shortText;
                oSessionTemplate.TextLong = longText;
                oSessionTemplate.Order = order;
                // Update the template in the DB.
                oSessionTemplate.Action(DL_Generic.DB_Actions.Update);

                // Upload the image and add it to the DB:
                uc_ImageUploader_SideWithImage.SetImageUploader(txt_ImageTitle_SideWithImage.Text, txt_ImageAlt_SideWithImage.Text, oSessionTemplate.DocId_Value, EnumHandler.RelatedObjects.Template, EnumHandler.ImageTypes.Main);
                uc_ImageUploader_SideWithImage.SaveImage();
            }
            Response.Redirect(StaticStrings.path_TemplateEdit + "?" + EnumHandler.QueryStrings.ID + "=" + oSessionTemplate.DocId_Value);
        }

        protected void btn_Save_SideInformation_Click(object sender, EventArgs e)
        {
            // if anything enterd is invalid
            if (!IsValid)
            {
                AddScript(" SelectTemplate('template_SideInformation');");
                return;
            }

            string title = txt_Title_tmp_SideInformation.Text;
            string longText = Server.HtmlDecode(txt_LongText_tmp_SideInformation.Value);
            string shortText = txt_ShortText_tmp_SideInformation.Text;
            int order = ConvertToValue.ConvertToInt(txt_Order_tmp_SideInformation.Text);

            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }
            if (oSessionTemplate == null)
            {
                // In case the page is in 'New Page' mode:

                //Create a new empty static page
                oSessionTemplate = new Template();
                //Fill its title and html content
                oSessionTemplate.Title = title;
                oSessionTemplate.TextShort = shortText;
                oSessionTemplate.TextLong =  longText;
                oSessionTemplate.Order = order;
                oSessionTemplate.Type = EnumHandler.TemplateType.SideWithImage.ToString();
                oSessionTemplate.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.
                oSessionTemplate.RelatedObjectDocId = PageDocId;
                oSessionTemplate.RelatedObjectType = (int)EnumHandler.RelatedObjects.DestinationPage;
                oSessionTemplate.Action(DL_Generic.DB_Actions.Insert);
            }
            else
            {
                //in case the page is in 'edit' mode:
                oSessionTemplate.Title = title;
                oSessionTemplate.TextShort = shortText;
                oSessionTemplate.TextLong = longText;
                oSessionTemplate.Order = order;
                oSessionTemplate.Action(DL_Generic.DB_Actions.Update);
            }
            Response.Redirect(StaticStrings.path_DestinationEdit + "?" + EnumHandler.QueryStrings.ID + "=" + PageDocId);
        }

        protected void btn_Save_CenterWithImage_Click(object sender, EventArgs e)
        {
            // if anything enterd is invalid
            if (!IsValid)
            {
                //AddScript("OpenTemplate(3);");
                AddScript(" SelectTemplate('template_CenterWithImage');");
                return;
            }

            string shortText = txt_ShortText_tmp_CenterWithImage.Text;
            string longText = Server.HtmlDecode(txt_LongText_tmp_CenterWithImage.Value); 
            string title = txt_Title_tmp_CenterWithImage.Text;
            int order = ConvertToValue.ConvertToInt(txt_Order_tmp_CenterWithImage.Text);

            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }

            if (oSessionTemplate == null)
            {
                // In case the page is in 'New Page' mode:

                //Create a new empty static page
                oSessionTemplate = new Template();
                //Fill its title and html content
                oSessionTemplate.Title = title;
                oSessionTemplate.TextShort = shortText;
                oSessionTemplate.TextLong = longText;
                oSessionTemplate.Order = order;
                oSessionTemplate.Type = EnumHandler.TemplateType.CenterWithImage.ToString();
                oSessionTemplate.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.
                oSessionTemplate.RelatedObjectDocId = PageDocId;
                oSessionTemplate.RelatedObjectType = (int)EnumHandler.RelatedObjects.DestinationPage;

                int result = oSessionTemplate.Action(DL_Generic.DB_Actions.Insert);

                if (result != -1)
                {
                    // upload the image and add it to the DB:
                    uc_ImageUploader_CenterWithImage.SetImageUploader(txt_ImageTitle_CenterWithImage.Text, txt_ImageAlt_CenterWithImage.Text, oSessionTemplate.DocId_Value, EnumHandler.RelatedObjects.Template, EnumHandler.ImageTypes.Main);
                    uc_ImageUploader_CenterWithImage.SaveImage();
                }
            }
            else
            {
                //in case the page is in 'edit' mode:
                oSessionTemplate.Title = title;
                oSessionTemplate.TextShort = shortText;
                oSessionTemplate.TextLong = longText;
                oSessionTemplate.Order = order;
                oSessionTemplate.Action(DL_Generic.DB_Actions.Update);

                // Upload the image and add it to the DB:
                uc_ImageUploader_CenterWithImage.SetImageUploader(txt_ImageTitle_CenterWithImage.Text, txt_ImageAlt_CenterWithImage.Text, oSessionTemplate.DocId_Value, EnumHandler.RelatedObjects.Template, EnumHandler.ImageTypes.Main);
                uc_ImageUploader_CenterWithImage.SaveImage();
            }
            Response.Redirect(StaticStrings.path_TemplateEdit + "?" + EnumHandler.QueryStrings.ID + "=" + oSessionTemplate.DocId_Value);
        }

        protected void btn_Save_MainInformation_Click(object sender, EventArgs e)
        {
            // if anything enterd is invalid
            if (!IsValid)
            {
                AddScript(" SelectTemplate('template_MainInformation');");
                return;
            }

            string title = txt_Title_tmp_MainInformation.Text;
            int order = ConvertToValue.ConvertToInt(txt_Order_tmp_MainInformation.Text);

            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }

            if (oSessionTemplate == null)
            {
                // In case the page is in 'New Page' mode:

                //Create a new empty static page
                oSessionTemplate = new Template();
                //Fill its title and html content
                oSessionTemplate.Title = title;
                oSessionTemplate.Order = order;
                oSessionTemplate.Type = EnumHandler.TemplateType.MainInformation.ToString();
                oSessionTemplate.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.
                oSessionTemplate.RelatedObjectDocId = PageDocId;
                oSessionTemplate.RelatedObjectType = (int)EnumHandler.RelatedObjects.DestinationPage;
                oSessionTemplate.Action(DL_Generic.DB_Actions.Insert);


                btn_AddSubBox.Style.Add("display", "normal");
                Response.Redirect(StaticStrings.path_TemplateEdit + "?" + EnumHandler.QueryStrings.ID + "=" + oSessionTemplate.DocId_Value);
            }
            else
            {
                //in case the page is in 'edit' mode:
                oSessionTemplate.Title = title;
                oSessionTemplate.Order = order;
                oSessionTemplate.Action(DL_Generic.DB_Actions.Update);
            }
        }

        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            // Redirect back to the destination edit page 
            Response.Redirect(String.Format("{0}?{1}={2}", StaticStrings.path_DestinationEdit, EnumHandler.QueryStrings.ID, PageDocId));
        }


        #region Sub box events

        protected void btn_SaveNew_Click(object sender, EventArgs e)
        {

            int order = ConvertToValue.ConvertToInt(txt_Order.Text);
            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }
            SubBox newSubBox = new SubBox();
            newSubBox.TemplateDocId = oSessionTemplate.DocId_Value;
            newSubBox.SubTitle = txt_SubTitle.Text;
            newSubBox.ShortText = txt_ShortText.Text;
            newSubBox.LongText = Server.HtmlDecode(txt_LongText.Value);
            newSubBox.Order = order;
            newSubBox.WhiteLabelDocId = 1; //TODO: replace the 1 with the real white label when there is white label support.

            // Save the new SubBox to the DB, and update the session
            newSubBox.Action(DL_Generic.DB_Actions.Insert);
            oSessionTemplate = GetTemplateFromDB();

            BindDataToRepeater();

            // Response.Redirect(StaticStrings.path_TemplateEdit+"?id="+oSessionTemplate.DocId_Value);
        }

        protected void btn_SaveEdit_Click(object sender, EventArgs e)
        {
            int order = ConvertToValue.ConvertToInt(txt_Order.Text);
            // If the number for order is not an integer, or if it's negative, reset it to 1.
            if (order == ConvertToValue.Int32EmptyValue || order <= 0)
            {
                order = 1;
            }

            LoadFromSession(false);

            int SubBoxDocId = ConvertToValue.ConvertToInt(((Button)sender).CommandArgument);
            if (SubBoxDocId != ConvertToValue.Int32EmptyValue)
            {
                SubBoxContainer oSubBoxContainer = oSessionTemplate.SubBoxs.SelectByID(SubBoxDocId, 1); // TODO: replace 1 with whitelable
                if (oSubBoxContainer != null && oSubBoxContainer.Count > 0)
                {
                    SubBox oSubBox = oSubBoxContainer.Single;
                    oSubBox.SubTitle = txt_SubTitle.Text;
                    oSubBox.ShortText = txt_ShortText.Text;
                    oSubBox.LongText = Server.HtmlDecode(txt_LongText.Value);
                    // Save the new SubBox to the DB, and update the session
                    oSubBox.Action(DB_Actions.Update);
                    oSessionTemplate = GetTemplateFromDB();

                    BindDataToRepeater();

                    // Switch buttons
                    btn_SaveEdit.Style.Add("display", "none");
                    btn_SaveNew.Style.Add("display", "normal");

                    // close the edit menu:
                    NewSubBoxDiv.Style.Add("display", "none");
                    btn_AddSubBox.Value = "Add Sub Box";
                }
            }
        }


        /// <summary>
        /// Fill the sub box panel with data from the sub box in the session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);
            LinkButton rowButton = (LinkButton)sender;
            //Getting the id of the sub box from the command argument of the button
            int SubBoxDocId = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(SubBoxDocId))
            {
                SubBoxContainer oSubBoxContainer = oSessionTemplate.SubBoxs.SelectByID(SubBoxDocId, 1); // TODO: replace 1 with whitelable
                if (oSubBoxContainer != null)
                {
                    SubBox oSubBox = oSubBoxContainer.Single;
                    if (oSubBox != null)
                    {
                        #region Fill SubBox panel fields

                        txt_SubTitle.Text = oSubBox.SubTitle_UI;
                        txt_ShortText.Text = oSubBox.ShortText_UI;
                        txt_LongText.InnerText = oSubBox.LongText_UI;
                        txt_Order.Text = oSubBox.Order_UI;

                        #endregion

                        // Unhide the SubBoxPanel and the SaveEdit button  
                        NewSubBoxDiv.Style.Add("display", "normal");
                        // Switch buttons
                        btn_SaveEdit.Style.Add("display", "normal");
                        btn_SaveNew.Style.Add("display", "none");
                        // Set the command argument of the button to hold the DocId of the SubBox to edit
                        btn_SaveEdit.CommandArgument = SubBoxDocId.ToString();
                        btn_AddSubBox.Value = "Cancel";
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            LoadFromSession(false);

            LinkButton rowButton = (LinkButton)sender;

            //Getting the id from the command argument of the button
            int id = ConvertToValue.ConvertToInt32(rowButton.CommandArgument);

            if (!ConvertToValue.IsEmpty(id)) // Same as comparing to int32EmptyValue
            {
                SubBoxContainer oSubBoxContainer = oSessionTemplate.SubBoxs.SelectByID(id, 1); // TODO: replace 1 with whitelable
                if (oSubBoxContainer != null)
                {
                    SubBox oSubBox = oSubBoxContainer.Single; // TODO: change the 1 to a real white label
                    if (oSubBox != null)
                    {
                        oSubBox.Action(DB_Actions.Delete);
                        Response.Redirect(StaticStrings.path_TemplateEdit + "?id=" + oSessionTemplate.DocId_Value);
                    }
                }
            }
            else
            {
                //the id is empty
                //no id was selected
            }
        }

        #endregion

        #endregion

        #region Helping methods



        /// <summary>
        /// A method that fills the relevant inputs with data from the template in the session.
        /// It also unhides the image uploaders, so they will work only after saving the template in the database.
        /// </summary>
        private void FillTemplateInformation(int TemplateDocId)
        {
            string type = oSessionTemplate.Type_Value;

            // For each template type, fill the relevant fields with data from the template object in the session

            if (type == EnumHandler.TemplateType.MainInformation.ToString())
            {
                // Initialize the fields with data from the session
                txt_Title_tmp_MainInformation.Text = oSessionTemplate.Title_UI;
                txt_Order_tmp_MainInformation.Text = oSessionTemplate.Order_UI;
                // When in edit mode, unhide the "Add Sub Box" button
                btn_AddSubBox.Style.Add("display", "normal");
                SubBoxesTableDiv.Style.Add("display", "normal");
                //Focus on the first field
                txt_Title_tmp_MainInformation.Focus();
            }
            else if (type == EnumHandler.TemplateType.CenterWithImage.ToString())
            {
                // Initialize the fields with data from the session
                txt_Title_tmp_CenterWithImage.Text = oSessionTemplate.Title_UI;
                txt_ShortText_tmp_CenterWithImage.Text = oSessionTemplate.TextShort_UI;
                txt_LongText_tmp_CenterWithImage.InnerHtml = oSessionTemplate.TextLong_UI;
                txt_Order_tmp_CenterWithImage.Text = oSessionTemplate.Order_UI;
                Images oImage = oSessionTemplate.Imagess.FindFirstOrDefault(img => img.ImageType == EnumHandler.ImageTypes.Main.ToString());
                if (oImage != null)
                {
                    txt_ImageTitle_CenterWithImage.Text = oImage.Title_UI;
                    txt_ImageAlt_CenterWithImage.Text = oImage.Alt_UI;
                }
                //Focus on the first field
                txt_Title_tmp_CenterWithImage.Focus();
            }
            else if (type == EnumHandler.TemplateType.SideInformation.ToString())
            {
                // Initialize the fields with data from the session
                txt_Title_tmp_SideInformation.Text = oSessionTemplate.Title_UI;
                txt_ShortText_tmp_SideInformation.Text = oSessionTemplate.TextShort_UI;
                txt_LongText_tmp_SideInformation.InnerHtml = oSessionTemplate.TextLong_UI;
                txt_Order_tmp_SideInformation.Text = oSessionTemplate.Order_UI;
                //Focus on the first field
                txt_Title_tmp_SideInformation.Focus();
            }
            else if (type == EnumHandler.TemplateType.SideWithImage.ToString())
            {
                // Initialize the fields with data from the session
                txt_Title_tmp_SideWithImage.Text = oSessionTemplate.Title_UI;
                txt_ShortText_SideWithImage.Text = oSessionTemplate.TextShort_UI;
                txt_LongText_tmp_SideWithImage.InnerHtml =  oSessionTemplate.TextLong_UI;
                txt_Order_tmp_SideWithImage.Text = oSessionTemplate.Order_UI;

                Images oImage = oSessionTemplate.Imagess.FindFirstOrDefault(img => img.ImageType == EnumHandler.ImageTypes.Main.ToString());
                if (oImage != null)
                {
                    txt_ImageTitle_SideWithImage.Text = oImage.Title_UI;
                    txt_ImageAlt_SideWithImage.Text = oImage.Alt_UI;
                }

                //Focus on the first field
                txt_Title_tmp_SideWithImage.Focus();
            }

        }

        /// <summary>
        /// A meethod that un-hides the matching template type, by the id that was specified in the query string.
        /// </summary>
        private void ShowOnlyRelevantTemplate()
        {
            string type = oSessionTemplate.Type_Value;
            if (type == EnumHandler.TemplateType.MainInformation.ToString())
            {
                template_MainInformation.Style.Add("display", "normal");
            }
            else if (type == EnumHandler.TemplateType.CenterWithImage.ToString())
            {
                template_CenterWithImage.Style.Add("display", "normal");
            }
            else if (type == EnumHandler.TemplateType.SideInformation.ToString())
            {
                template_SideInformation.Style.Add("display", "normal");
            }
            else if (type == EnumHandler.TemplateType.SideWithImage.ToString())
            {
                template_SideWithImage.Style.Add("display", "normal");
            }
        }

        /// <summary>
        /// A method that generates an id for a new SubBox User Control.
        /// </summary>
        /// <param name="templateId">The id of a template to use.</param>
        /// <returns>A new id string in the format: 'SubBox_template_[templateId]_num_[last id + 1]'</returns>
        private string GenerateNewSubBoxID(int templateId)
        {
            SubBox sb = new SubBox();
            Type type = sb.GetType();
            int count = 1;
            //After the loop, the counter will equal to 1 more than the amount of SubBox UCs on the page.
            foreach (var control1 in Page.Controls)
            {
                if (control1.GetType() == type)
                {
                    count++;
                }
            }
            return string.Format("SubBox_templateId_{0}_num_{1}", templateId, count);
        }


        /// <summary>
        /// A method that clears the fields of the SubBox Panel.
        /// </summary>
        private void ClearSubBoxPanel()
        {
            txt_ShortText.Text = "";
            txt_LongText.InnerText = "";
            txt_SubTitle.Text = "";
            txt_Order.Text = "1";
        }

        /// <summary>
        /// A method that binds the data from the session to the repaeater.
        /// </summary>
        private void BindDataToRepeater()
        {
            drp_SubBoxTable.DataSource = oSessionTemplate.SubBoxs.DataSource;
            drp_SubBoxTable.DataBind();
        }

        #endregion

    }
}