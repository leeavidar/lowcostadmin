﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="MailingList.aspx.cs" Inherits="FlightsAdmin.MailingList" %>
<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <link href="/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/pages/portfolio.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                <asp:Label ID="lbl_PageTitle" runat="server" Text="Mailing List"></asp:Label>
            </h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">

                <!-- START:  Title section of the panel -->
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>Mailing list
                    </div>
                  
                </div>
                <!-- END:  Title section of the panel -->

                <div class="portlet-body form">
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Repeater ID="drp_MailingList" runat="server" ItemType="DL_LowCost.MailAddress">
                                        <HeaderTemplate>
                                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                                <thead>
                                                    <th>ID</th>
                                                    <th>Mail Address</th>
                                                    <th></th>
                                                </thead>
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <!-- The order is a priority-->
                                                <td>
                                                    <span id="lbl_DocId"><%# Eval("DocId_UI") %></span>
                                                </td>
                                                <td>
                                                    <span id="lbl_MailAddress" class="storeEmail"><%# Eval("Address_UI") %></span>
                                                </td>
                                                <td>
                                                    <!-- Delete button -->
                                                    <asp:LinkButton ID="btn_Delete" name="btn_Delete" runat="server" CssClass="btn default btn-sm blue" CommandArgument='<%# Eval("DocId_UI") %>' OnClientClick="return CheckDelete()" OnClick="btn_Delete_Click"><i class="fa fa-fire"></i>  Delete</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <a href="#" download="LowCostMailingList.csv" id="btn_ExportCSV" class="btn green">Export to CSV (Excel)</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="ScriptsContent" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/table-advanced.js"></script>
    <script type="text/javascript" src="assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="assets/scripts/form-components.js"></script>
      <script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="Scripts/siteJS.js" type="text/javascript"></script>
     <script src="Scripts/PageScripts/MailingList.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            Portfolio.init();
            TableAdvanced.init();
            FormComponents.init();
        });
    </script>
</asp:Content>
