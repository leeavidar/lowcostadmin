﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MenuMaster.master" AutoEventWireup="true" CodeBehind="Orders_Management.aspx.cs" Inherits="FlightsAdmin.Orders_Management" %>

<%@ MasterType VirtualPath="~/MasterPages/MenuMaster.master" %>
<%@ Register Src="~/Controls/SegmentFlightUC.ascx" TagPrefix="FlightsUserControls" TagName="SegmentFlightUC" %>
<%@ Register Src="~/Controls/uc_FlightLegRepeater.ascx" TagPrefix="FlightsUserControls" TagName="uc_FlightLegRepeater" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="/assets/plugins/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />

    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />

    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-multi-select/css/multi-select.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="/assets/plugins/jquery-tags-input/jquery.tagsinput.css" />

    <link rel="stylesheet" type="text/css" href="Content/Site.css">
    <!-- END PAGE LEVEL STYLES -->
</asp:Content>


<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SEARCH AREA  -->
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa  fa-search"></i>Find Flight
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN SEARCH fields-->
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <!--  it's possible to add:  has-error to this class -->
                                        <label class="control-label col-md-3">LC PNR</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_filter_LcPnr" class="form-control input-medium txt_LCPNR" runat="server" />
                                            <span class="help-block">
                                                <!-- Here is the place for help or for validation -->
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Airline PNR</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_filter_AirlinePnr" class="form-control input-medium txt_AirlinePNR" runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">From Date</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_filter_FromDate" CssClass="form-control form-control-inline input-medium date-picker" runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">To Date</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_filter_ToDate" CssClass="form-control form-control-inline input-medium date-picker" runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Client Name</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_filter_ClientName" class="form-control input-medium " runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <!--  it's possible to add:  has-error to this class -->
                                        <label class="control-label col-md-3">Passport Num.</label>
                                        <div class="col-md-9">
                                            <asp:TextBox name="number" ID="txt_filter_PassNum" class="form-control input-medium " runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Airline</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_filter_Airline" CssClass="form-control input-medium" runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Flight Number</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txt_filter_FlightNumber" CssClass="form-control input-medium" runat="server" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Status</label>
                                        <div class="col-md-9">
                                            <asp:DropDownList ID="ddl_Status" class="form-control input-medium" runat="server"></asp:DropDownList>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-9">
                                        <asp:Button ID="btn_Clear" runat="server" CssClass="btn_Clear btn purple" Text="Clear" OnClick="btn_Clear_Click" />
                                        <asp:Button ID="btn_Filter" runat="server" CssClass="btn_Filter btn green" Text="Find" OnClick="btn_Filter_Click" />
                                        <asp:CheckBox ID="cb_SearchDatabase" runat="server" Text="Include old orders" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Search fields-->
                </div>
            </div>
            <!-- END OF SEARCH AREA -->


            <!-- BEGIN ORDERS TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>Orders table
                    </div>
                </div>
                <div class="portlet-body">
                    <!-- BEGIN REPEATER FOR THE ORDRES DETALS -->
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <!-- This hidden field is used to know if the prevoius session included all the orders from the database (the defaults should be false)-->
                        <asp:HiddenField ID="hdn_OldDataIncluded" runat="server" Value="false" />
                        <%-- <asp:Button runat="server" ID="btn_RefreshPage" Style="display: none;" OnClick="btn_RefreshPage_Click" />--%>
                        <asp:Repeater ID="drp_OrdersData" runat="server" ItemType="DL_LowCost.Orders" >  <%--OnItemDataBound="drp_OrdersData_ItemDataBound"--%>
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Details</th>
                                        <th>Date Created</th>
                                        <th>Low Cost PNR</th>
                                        <th>Airline PNR </th>
                                        <th>Status</th>
                                        <th>Lead Pass</th>
                                        <th>Airline</th>
                                        <th>Flight number</th>
                                        <th>Class</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>Price</th>
                                        <!--   <th>Send Message</th>   -->
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <!-- START: Row of basic details  -->
                                <tr>
                                     <td>
                                        <span id="Span1"><%# Eval("DocId_Value") %> </span>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnk_DetailsPage" CssClass="btn blue" runat="server" OnClick="lnk_DetailsPage_Click" CommandArgument="<%#Item.DocId_UI%>">Details</asp:LinkButton>
                                    </td>
                                    <td>
                                        <span id="Span2"><%# Eval("DateCreated_Value") %> </span>
                                    </td>
                                    <td>
                                        <span id="lbl_LowCostPNR"><%# Eval("LowCostPNR_UI") %> </span>
                                    </td>
                                    <td>
                                        <span id="lbl_SupplierPNR"><%# Eval("SupplierPNR_UI") %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_OrderStatus"><%#FlightsAdmin.OrderManager.ConvertOrderStatusToString(Item.OrderStatus_Value) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_LeadPassengerName"><%#FlightsAdmin.OrderManager.GetLeadPassengerName(Item) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_Airline"><%#FlightsAdmin.OrderManager.GetAirlineName(Item, oSessionAirlinesContainer) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_FlightNumber"><%#FlightsAdmin.OrderManager.GetFlightNumber(Item) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_Class"><%#Eval("MarketingCabin_UI") %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_FlightOrigin"><%#FlightsAdmin.OrderManager.GetFlightOrigin(Item) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_FlightDestinaiotn"><%#FlightsAdmin.OrderManager.GetFlightDestination(Item) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_FlightOutwardDate"><%#FlightsAdmin.OrderManager.GetOutwardDate(Item) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_FlightReturnDate"><%#FlightsAdmin.OrderManager.GetReturnDate(Item) %></span>
                                    </td>
                                    <td>
                                        <span id="lbl_Price"><%#Eval("Currency_UI")%> <%#Eval("TotalPrice_UI")%>  </span>
                                    </td>
                                    <%--  <td>
                                    <a href="#" class="btn btn-sm blue">Send Message <i class="fa fa-envelope-o"></i></a>
                                </td>--%>
                                </tr>
                                <!-- END: Row of basic details -->
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                    <!-- END REPEATER FOR THE ORDRES DETALS -->
                </div>
            </div>
            <!-- END OF ORDERS TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="CPHMain_ScriptsButton" runat="server">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="/assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/assets/plugins/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/assets/scripts/form-samples.js"></script>
    <script src="/assets/scripts/form-components.js"></script>
    <script src="/assets/scripts/table-advanced.js"></script>
    <script src="/Scripts/siteJS.js"></script>

    <script>
        jQuery(document).ready(function () {
            $('#sample_1').DataTable();
            FormSamples.init();
            FormComponents.init();
        });

    </script>
</asp:Content>
