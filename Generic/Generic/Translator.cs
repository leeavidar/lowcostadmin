﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DL_Generic
{
    public static class TranslatorApp
    {
        static TranslatorApp()
        {
            InitTranslatedList();
        }
        //Declare if to set translate from static list 
        public static bool IsStaticTranslated = Settings.IsStaticTranslated;
        private static Dictionary<int, Dictionary<long, string>> lTranslaed;
        public static Dictionary<long, string> ListTranslaed(int langId) { return lTranslaed[langId]; }
        /// <summary>
        /// Init all translated list
        /// </summary>
        public static void InitTranslatedList()
        {
            if (IsStaticTranslated)
            {

                DataSet ds = null;
                try
                {
                    DB db = new DB();
                    #region DB Params (IN/OUT)
                    //----add parameters-------------------------------------//
                    //-------------------------------------------------------//
                    #endregion
                    //get all langs from db
                    ds = db.ExecuteDataSet("translator_select_langs");
                    if (lTranslaed == null)
                    {
                        lTranslaed = new Dictionary<int, Dictionary<long, string>>();
                        
                    }
                    if (ds == null) return;
                    int keyMax = GetMaxKeyId();
                    if (keyMax >= 0)
                    {
                        //ok found the id
                        //init each lang list 
                        foreach (DataRow drTranslated in ds.Tables[0].Rows)
                        {
                            int langId = ConvertToValue.ConvertToInt(drTranslated["Lang_id"]);
                            InitTranslatedList(langId, keyMax);
                        }
                    }
                    else
                    {
                        //there was some error to get the max key id
                    }
                }
                catch (Exception ex)
                {
                    //throw ex;
                }
            }
            else
            {
                //no need to init translator to app
            }

        }
        //Set static list of translated values from DB
        public static void InitTranslatedList(int langId, int keyMax)
        {
            DataSet ds = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//
                db.AddParameter("@prm_lang_id", ConvertTo.ConvertEmptyToDBNull(langId));
                //-------------------------------------------------------//
                #endregion
                ds = db.ExecuteDataSet("translator_select_by_lang");
                if (lTranslaed == null)
                {
                    lTranslaed = new Dictionary<int, Dictionary<long, string>>();

                    //new Dictionary<int, List<KeyValuePair<int, string>>>();
                }

                //lTranslaed.Add(langId, SetTranlatedList(ds, keyMax));
                lTranslaed[langId] = SetTranlatedList(ds, keyMax);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }
        public static int GetMaxKeyId()
        {
            DataSet ds = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//
                //-------------------------------------------------------//
                #endregion
                ds = db.ExecuteDataSet("translator_max_key_id");
                if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    //retrun max id
                    return ConvertToValue.ConvertToInt32(ds.Tables[0].Rows[0]["MaxKeyId"]);
                }
                else
                {
                    // cannot get the max id
                    return -1;
                }
            }
            catch (Exception ex)
            {
                //there was some error
                return -1;
                //throw ex;
            }
        }

        /// <summary>
        /// Set list with data from DB
        /// </summary>
        /// <param name="dsTranslated"></param>
        private static Dictionary<long, string> SetTranlatedList(DataSet dsTranslated, int keyMax)
        {
            //set to new list
            Dictionary<long, string> currlTranslaed = null;
            if (dsTranslated == null) { /*no data*/ return null; }
            lock (lTranslaed)
            {
                //Find Max key
                //int keyMax = ConvertToValue.ConvertToInt(dsTranslated.Tables[0].Rows[dsTranslated.Tables[0].Rows.Count - 1]["Translate_id"]) + 1;
                currlTranslaed = new Dictionary<long, string>();
                //string[keyMax + 1];

                foreach (DataRow drTranslated in dsTranslated.Tables[0].Rows)
                {
                    int key = ConvertToValue.ConvertToInt(drTranslated["Translate_id"]);
                    string value = ConvertToValue.ConvertToString(drTranslated["Translate_value"]);
                    if (!ConvertToValue.IsEmpty(key))
                    {
                        currlTranslaed[key] = value;//.Add(new KeyValuePair<int, string>(key, value));
                    }
                }
            }
            return currlTranslaed;
        }

        public static string FindTranslate(int id, int langId)
        {
            Comparison<KeyValuePair<int, string>> c = (x, y) => x.Key.CompareTo(y.Key);
            IComparer<KeyValuePair<int, string>> icc = c.AsComparer();
            KeyValuePair<int, string> objToSearch = new KeyValuePair<int, string>(id, "");
            if (lTranslaed[langId] != null)
            {
                if (lTranslaed[langId].ContainsKey(id))
                {
                    return lTranslaed[langId][id];
                }
                else {
                    return null;
                }
                 //ElementAt(id);
            }
            else
            {
                //Cannot find language array
                return "";
            }
        }
    }

    public class Translator<T>
        where T : ContainerItem<T>, new()
    {
        public static int LangId
        {
            get
            {
                if (System.Web.HttpContext.Current == null)
                {
                    return 1;
                }


                DL_LowCost.Languages oLanguages = Generic.SessionManager.GetLanguageFromSession();

                if (oLanguages != null)
                {
                    return oLanguages.DocId_Value;
                }

                return 1;

                if (System.Web.HttpContext.Current.Request.Url.HostNameType == UriHostNameType.Dns)
                {
                    string host = System.Web.HttpContext.Current.Request.Url.Host;
                    foreach (var item in host.Split(new char[] { '.' }))
                    {
                        DL_LowCost.Languages oLanguages2 = BL_LowCost.LanguagesContainer.SelectByKeysView_Code(item, null).Single;
                        if (oLanguages != null)
                        {
                            return oLanguages.DocId_Value;
                        }
                    }
                }
                return 1;

                if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session["Lang"] == null)
                {
                    int DefaultLang = ConvertToValue.ConvertToInt(System.Configuration.ConfigurationManager.AppSettings["LangId"]);

                    if (DefaultLang > 0)
                    {
                        return DefaultLang;
                    }
                    else
                    {
                        return 1;
                    }
                }
                return ConvertToValue.ConvertToInt(System.Web.HttpContext.Current.Session["Lang"]);
            }
        }

        //get all mirror keys
        public static List<KeyValues<T>> Translate(List<KeyValues<T>> mirrorKeys, int langId)
        {
            if (mirrorKeys == null) return null;

            if (TranslatorApp.IsStaticTranslated)
            {
                //Translate from application layer
                return Marge(ref mirrorKeys, langId);
            }
            else
            {
                //Translate from DB
                List<object> lKeys = new List<object>();
                string strKeys = "";

                #region Set mirror key names
                foreach (var key in mirrorKeys)
                {
                    //all mirror keys contains # in the DB name
                    if (!ConvertToValue.IsEmpty(key.Key))
                    {
                        lKeys.Add(key.Key);
                    }
                    else
                    {
                        //lKeys.Add("-1");
                        //Empty key - no need to translate
                    }
                }
                if (lKeys.Count > 0)
                { strKeys = string.Join(",", lKeys.ToArray()); }
                #endregion

                if (!string.IsNullOrEmpty(strKeys))
                {
                    DataSet ds = null;
                    try
                    {
                        DB db = new DB();
                        #region DB Params (IN/OUT)
                        //----add parameters-------------------------------------//
                        db.AddParameter("@prm_where_in", ConvertTo.ConvertEmptyToDBNull(strKeys));
                        db.AddParameter("@prm_lang_id", ConvertTo.ConvertEmptyToDBNull(langId));
                        //-------------------------------------------------------//
                        #endregion
                        ds = db.ExecuteDataSet("translator_where_in");
                    }
                    catch (Exception ex)
                    {
                        return null;
                        //throw ex;
                    }
                    return Marge(ds, ref mirrorKeys);
                }
                else
                {
                    return null;
                }
            }
        }

        //marge with translated list
        public static List<KeyValues<T>> Marge(ref List<KeyValues<T>> mirrorKeys, int langId)
        {
            if (mirrorKeys == null) return null;
            mirrorKeys = mirrorKeys.FindAll(R => R.Key != null);

            List<object> lKeysFound = new List<object>();
            for (int i = 0; i < mirrorKeys.Count; i++)
            {
                KeyValues<T> key = mirrorKeys[i];
                string keyTranslatedValue = TranslatorApp.FindTranslate(ConvertToValue.ConvertToInt(key.Key), langId);
                key.SetKey(keyTranslatedValue);
            }
            return mirrorKeys;
        }

        //marge with db
        public static List<KeyValues<T>> Marge(DataSet ds, ref List<KeyValues<T>> mirrorKeys)
        {
            if (mirrorKeys == null) return null;
            mirrorKeys = mirrorKeys.FindAll(R => R.Key != null);

            List<object> lKeysFound = new List<object>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                List<KeyValues<T>> lKeys = mirrorKeys.FindAll(R => (int.Equals(R.Key.ToString(), dr["id"].ToString())));
                if (lKeys != null)
                {
                    //if (lKeys.Count == 1) throw new Exception("found more then one column that use the same key id to translate");
                    foreach (var key in lKeys)
                    {
                        key.Key = dr["value"];
                        lKeysFound.Add(key.Key);
                    }
                }
            }
            //Remove keys that have no translation
            foreach (var key in mirrorKeys)
            {
                List<object> lKeys = lKeysFound.FindAll(R => int.Equals(R.ToString(), key.Key.ToString()));
                if (lKeys == null || lKeys.Count == 0)
                {
                    key.Key = "";
                }
            }
            return mirrorKeys;
        }

    }
    /*
use BookingIsrael
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[translator_select_by_lang]
@prm_lang_id int
 AS BEGIN
 select id as Translate_id,value as Translate_value FROM dbo.Translator 
 where lang_id = @prm_lang_id and value is not null
  group by id ,value
  order by dbo.Translator.id
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[translator_select_langs]
 AS BEGIN
 select lang_id as Lang_id FROM dbo.Translator 
 where lang_id is not null
  group by lang_id
 order by dbo.Translator.lang_id
END
    GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[translator_max_key_id]
 AS BEGIN
 select top 1 id as MaxKeyId
  FROM dbo.Translator 
  
 order by dbo.Translator.id desc
END
     */
}
