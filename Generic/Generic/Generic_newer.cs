﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.IO;
using Code;
using System.Xml.Serialization;
using Generic;

namespace DL_Generic
{
    public static class ComparisonEx
    {
        public static IComparer<T> AsComparer<T>(this Comparison<T> @this)
        {
            if (@this == null)
                throw new System.ArgumentNullException("Comparison<T> @this");
            return new ComparisonComparer<T>(@this);
        }

        public static IComparer<T> AsComparer<T>(this Func<T, T, int> @this)
        {
            if (@this == null)
                throw new System.ArgumentNullException("Func<T, T, int> @this");
            return new ComparisonComparer<T>((x, y) => @this(x, y));
        }

        private class ComparisonComparer<T> : IComparer<T>
        {
            public ComparisonComparer(Comparison<T> comparison)
            {
                if (comparison == null)
                    throw new System.ArgumentNullException("comparison");
                this.Comparison = comparison;
            }

            public int Compare(T x, T y)
            {
                return this.Comparison(x, y);
            }

            public Comparison<T> Comparison { get; private set; }
        }
    }
    #region GlobalProps
    #region GlobalEnum
    public enum ESocialTitle
    {

        /// <remarks/>
        None,

        /// <remarks/>
        Mr,

        /// <remarks/>
        Ms,

        /// <remarks/>
        Mrs,

        /// <remarks/>
        Miss,
    }

    public enum FileType
    {
        None = 0,
        xls = 1,
        pdf = 2,
        xml = 3,
    }

    public enum CurrencyType
    {
        None = 0,
        ILS = 1,
        USD = 2,
        EUR = 3,
    }

    public enum ExceptionType
    {
        Print = 0,
        Exporte = 1,
        Importe = 2,
        Genral = 3,
        DataBase = 4,
    }
    public enum ExceptionFrom
    {
        Code = 0,
        UI = 1,
    }
    public enum PaymentType
    {
        NotSet = 0,
        Cash = 1,
        Check = 2,
        WireTransfer = 3,
        PaymentOrder = 4,

    }
    public enum RecommendType
    {
        NotSet = 0,
        Site = 1,
        Tour = 2,
        Restaurant = 3,
        Club = 4,

    }
    public enum FeedbackType
    {
        NotSet = 0,
        Hotel = 1,
        Guide = 2,
        TranSport = 3,
        Flight = 4,
        Agent = 5,

    }
    public enum Role
    {
        Guide = 0,
        Manger = 1,
        Administrator = 2,

    }
    public enum TripExpenseType
    {
        None = 0,
        Enteris = 1,
        Tips = 2,
        Other = 3,
    }
    #endregion
    #region regular expression
    public class RegexExp
    {
        private static readonly string ExpEmail = @"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$";
        private static readonly string ExpFileNamae = "^[a-zA-Z0-9]+$";
        public static bool CheckEmail(string Email)
        {
            Regex Reg = new Regex(ExpEmail);
            return Reg.IsMatch(Email);
        }
        public static bool IsFileNameCorrect(string FileName)
        {
            Regex Reg = new Regex(ExpFileNamae);
            return Reg.IsMatch(FileName);
        }

    }
    #endregion
    #region ExtensionMethod
    public static class ExtensionMethod
    {
        public static string TrimString(this string str)
        {
            return str.Trim(new char[] { ' ' });
        }

        public static string ToFormat_DMY(this DateTime dt, string separator)
        {
            return string.Format("{0:dd#MM#yyyy}", dt).Replace("#", separator);
        }
    }
    #endregion
    #region Convert
    public class ConvertTo
    {

        public static DateTime? ConvertToDateTime(object obj, string format = null, System.Globalization.CultureInfo cultureInfo = null)
        {
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("he-IL");
            if (IsEmpty(obj))
                return null;
            DateTime val;
            if (format == null && cultureInfo == null)
            {
                DateTime.TryParse(obj.ToString(), out val);
            }
            else
            {
                if (format == null || cultureInfo == null) throw new Exception("ConvertToDateTime - worng function using must send format and cultureInfo");
                //string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",  
                //   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
                //   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
                //   "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
                //   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

                //convert with date time format
                //DateTime.TryParseExact(dateString, formats, 
                //              new CultureInfo("en-US"), 
                //              DateTimeStyles.None, 
                //              out dateValue))

                DateTime.TryParseExact(obj.ToString(), format, cultureInfo, System.Globalization.DateTimeStyles.None, out val);
            }
            return val;
        }

        public static object ConvertEmptyToDBNull(object obj)
        {
            if (DBNull.Value == obj || null == obj || ConvertToValue.IsEmpty(obj))
                return DBNull.Value;

            return obj;
        }
        public static bool IsEmptyValue(object obj)
        {
            if (obj == null)
                return true;

            bool isEmpty = true;
            TypeCode typeCode = Type.GetTypeCode(obj.GetType());

            switch (typeCode)
            {
                case TypeCode.Boolean:
                    isEmpty = false;//Convert.ToBoolean(obj) == ConvertToValue.BoolEmptyValue;
                    break;
                case TypeCode.DateTime:
                    isEmpty = Convert.ToDateTime(obj) == ConvertToValue.DateTimeEmptyValue;
                    break;
                case TypeCode.Decimal:
                    isEmpty = Convert.ToDecimal(obj) == ConvertToValue.DecimalEmptyValue;
                    break;
                case TypeCode.Double:
                    isEmpty = Convert.ToDouble(obj) == ConvertToValue.DoubleEmptyValue;
                    break;
                case TypeCode.Int32:
                    isEmpty = Convert.ToInt32(obj) == ConvertToValue.Int32EmptyValue;
                    break;
                case TypeCode.String:
                    isEmpty = Convert.ToString(obj) == ConvertToValue.StringEmptyValue;
                    break;
                default:
                    isEmpty = false;
                    break;
            }

            return isEmpty;
        }
        public static bool IsEmpty(object obj)
        {
            return (DBNull.Value == obj || null == obj || IsEmptyValue(obj));
        }
        public static bool? ConvertToBool(object obj)
        {
            if (IsEmpty(obj))
                return null;
            bool val = false;
            Boolean.TryParse(obj.ToString(), out val);
            return val;
        }
        public static decimal? ConvertToDecimal(object obj)
        {
            if (IsEmpty(obj))
                return null;
            decimal val;
            Decimal.TryParse(obj.ToString(), out val);
            return val;
        }

        public static DateTime? ConvertToDateTime(object obj)
        {
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-us");
            if (IsEmpty(obj))
                return null;
            DateTime val;
            DateTime.TryParse(obj.ToString(), out val);
            return val;
        }
        public static long? ConvertToLong(object obj)
        {
            if (IsEmpty(obj))
                return null;
            long val;
            long.TryParse(obj.ToString(), out val);
            return val;
        }
        public static double? ConvertToDouble(object obj)
        {
            if (IsEmpty(obj))
                return null;
            double val = -1;
            Double.TryParse(obj.ToString(), out val);
            return val;
        }
        public static Int32? ConvertToInt32(object obj)
        {
            if (IsEmpty(obj))
                return null;
            int val;
            if (!Int32.TryParse(obj.ToString(), out val))
            {
                val = ConvertToValue.Int32EmptyValue;
            }

            return val;
        }
        public static string ConvertToString(object obj)
        {
            if (IsEmpty(obj))
                return null;
            return Convert.ToString(obj.ToString());
        }



        public static FeedbackType ConvertToFeedbackType(object obj)
        {
            if (IsEmpty(obj))
                return FeedbackType.NotSet;
            foreach (FeedbackType cType in Enum.GetValues(typeof(FeedbackType)))
            {
                int index = (int)cType;
                if (obj.ToString().TrimString() == cType.ToString() || obj.ToString().TrimString() == index.ToString())
                    return cType;
            }
            return FeedbackType.NotSet;
        }
        public static RecommendType ConvertToRecommendType(object obj)
        {
            if (IsEmpty(obj))
                return RecommendType.NotSet;
            foreach (RecommendType cType in Enum.GetValues(typeof(RecommendType)))
            {
                int index = (int)cType;
                if (obj.ToString().TrimString() == cType.ToString() || obj.ToString().TrimString() == index.ToString())
                    return cType;
            }
            return RecommendType.NotSet;
        }


        public static TripExpenseType ConvertToTripExpenseType(object obj)
        {
            if (IsEmpty(obj))
                return TripExpenseType.None;
            foreach (TripExpenseType cType in Enum.GetValues(typeof(TripExpenseType)))
            {
                int index = (int)cType;
                if (obj.ToString().TrimString() == cType.ToString() || obj.ToString().TrimString() == index.ToString())
                    return cType;
            }
            return TripExpenseType.None;
        }

        public static CurrencyType ConvertToCurrencyType(object obj)
        {
            if (IsEmpty(obj))
                return CurrencyType.None;
            foreach (CurrencyType cType in Enum.GetValues(typeof(CurrencyType)))
            {
                int index = (int)cType;
                if (obj.ToString().TrimString() == cType.ToString() || obj.ToString().TrimString() == index.ToString())
                    return cType;
            }
            return CurrencyType.None;
        }
        public static PaymentType ConvertToPaymentType(object obj)
        {
            if (IsEmpty(obj))
                return PaymentType.NotSet;
            foreach (PaymentType cType in Enum.GetValues(typeof(PaymentType)))
            {
                int index = (int)cType;
                if (obj.ToString().TrimString() == cType.ToString() || obj.ToString().TrimString() == index.ToString())
                    return cType;
            }
            return PaymentType.NotSet;
        }
        public static bool IsSingleRow(DataSet ds)
        {
            if (ds != null)
                if (ds.Tables[0].Rows.Count == 1)
                    return true;
            return false;
        }
        public static DataRow ConvertToSingleRow(DataSet ds)
        {
            if (IsSingleRow(ds))
                return ds.Tables[0].Rows[0];
            return null;
        }

        public static int? ConvertToInt(object obj)
        {
            return ConvertToInt32(obj);
        }
    }
    public class ConvertToValue
    {

        public static readonly long LongEmptyValue = long.MinValue;
        public static readonly bool BoolEmptyValue = false;
        public static readonly decimal DecimalEmptyValue = decimal.MinValue;
        public static readonly DateTime DateTimeEmptyValue = Convert.ToDateTime("1/1/1975");
        public static readonly double DoubleEmptyValue = double.MinValue;
        public static readonly Int32 Int32EmptyValue = int.MinValue;
        public static readonly string StringEmptyValue = string.Empty;

        public static DateTime ConvertToDateTimeWithFormats(object obj, string format = null, System.Globalization.CultureInfo cultureInfo = null)
        {
            DateTime? val = ConvertTo.ConvertToDateTime(obj, format, cultureInfo);
            return IsEmpty(val) ? ConvertToValue.DateTimeEmptyValue : val.Value;
        }

        public static object ConvertEmptyToDBNull(object obj)
        {
            return ConvertTo.ConvertEmptyToDBNull(obj);
        }
        public static bool IsEmpty(object obj)
        {
            return ConvertTo.IsEmpty(obj);
        }
        public static bool ConvertToBool(object obj)
        {
            bool? val = ConvertTo.ConvertToBool(obj);
            return IsEmpty(val) ? BoolEmptyValue : val.Value;
        }
        public static long ConvertToLong(object obj)
        {
            long? val = ConvertTo.ConvertToLong(obj);
            return IsEmpty(val) ? LongEmptyValue : val.Value;
        }
        public static decimal ConvertToDecimal(object obj)
        {
            decimal? val = ConvertTo.ConvertToDecimal(obj);
            return IsEmpty(val) ? DecimalEmptyValue : val.Value;
        }
        public static DateTime ConvertToDateTime(object obj)
        {
            DateTime? val = ConvertTo.ConvertToDateTime(obj);
            return IsEmpty(val) ? DateTimeEmptyValue : val.Value;
        }
        public static double ConvertToDouble(object obj)
        {
            double? val = ConvertTo.ConvertToDouble(obj);
            return IsEmpty(val) ? DoubleEmptyValue : val.Value;
        }
        public static Int32 ConvertToInt32(object obj)
        {
            Int32? val = ConvertTo.ConvertToInt32(obj);
            return IsEmpty(val) ? Int32EmptyValue : val.Value;
        }
        public static string ConvertToString(object obj)
        {
            string val = ConvertTo.ConvertToString(obj);
            return IsEmpty(val) ? StringEmptyValue : val;
        }
        public static CurrencyType ConvertToCurrencyType(object obj)
        {
            return ConvertTo.ConvertToCurrencyType(obj);
        }
        public static TripExpenseType ConvertToTripExpenseType(object obj)
        {
            return ConvertTo.ConvertToTripExpenseType(obj);
        }
        public static FeedbackType ConvertToFeedbackType(object obj)
        {
            return ConvertTo.ConvertToFeedbackType(obj);
        }

        public static RecommendType ConvertToRecommendType(object obj)
        {
            return ConvertTo.ConvertToRecommendType(obj);
        }
        public static PaymentType ConvertToPaymentType(object obj)
        {
            return ConvertTo.ConvertToPaymentType(obj);
        }


        public static int ConvertToInt(object obj)
        {
            return ConvertToInt32(obj);
        }


    }
    #endregion
    #region Filters
    public class FileFilters
    {
        public static readonly string PDF = "PDF files (*.pdf)|*.pdf";
        public static readonly string XML = "XML files (*.xml)|*.xml";
        public static readonly string EXCEL = "EXCEL files (*.xls) (*.xlsx)|*.xls;*.xlsx";
    }
    #endregion
    #region Exception&log
    public class ExceptionLog
    {
        public static void WriteToLog(string ExceptionMessage)
        {
            //string Folder = new FolderSetting().ExceptionLog;
            //string File = new FolderSetting().LogExeption;
            //FileInfo fi = new FileInfo(Folder + File);
            //DirectoryInfo di = new DirectoryInfo(Folder);
            //if (!di.Exists)
            //{
            //    di.Create();
            //}
            //if (!fi.Exists)
            //{
            //    fi.Create();
            //}
            //TextWriter tw = new StreamWriter(Folder + File, true);
            //tw.WriteLine("\n========================================================================================\n");
            //tw.WriteLine("\n " + ExceptionMessage + "\n");
            //tw.Close();

        }
        public static string SetErrorMsg(string Type, string MethodName, string Emassage)
        {
            string Errormsg = string.Format("{0} \r\nMethod : {1} \r\nE.Message : {2}", Type, MethodName, Emassage);
            ExceptionLog.WriteToLog(Errormsg);
            SendMail(MethodName, Errormsg, ExceptionFrom.Code);
            return Errormsg;
        }
        public static string UIExceptionMsg(string Type, string MethodName, string Emassage, string UIMessage)
        {
            string Errormsg = string.Format("{0} \r\nMethod : {1} \r\nE.Message : {2}", Type, MethodName, Emassage);
            ExceptionLog.WriteToLog(Errormsg);
            SendMail(MethodName, Errormsg, ExceptionFrom.UI);
            return UIMessage;
        }
        public static void SendMail(string MethodName, string Errormsg, ExceptionFrom ExceptionFrom)
        {
            //string Subject = string.Format("Exception From {0} . Type {1}", MethodName, ExceptionFrom.ToString());
            //FolderSetting fs = new FolderSetting();
            //SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            //client.Credentials = new NetworkCredential(fs.ExceptionMail, fs.ExceptionMailPassword);
            //client.EnableSsl = true;
            //MailMessage Message = new MailMessage(fs.ExceptionMail, fs.ExceptionMail, Subject, Errormsg);
            //Message.IsBodyHtml = true;
            //// client.Send(Message);
        }

    }
    #endregion
    #endregion
    #region Keys
    //Auto To Add public static TypeCode GetTypeCodeByKey(KeyValuesType SortKey)
    public class KeyValuesContainer<ITEM>
    where ITEM : ContainerItem<ITEM>, new()
    {
        #region KeyFunctions
        public static KeyValuesType SortKey = KeyValuesType.None;
        public static bool SortDesc = true;
        public static int Compare(object _item, object _itemToCompareWith)
        {
            ITEM item = _item as ITEM;
            ITEM itemToCompareWith = _itemToCompareWith as ITEM;
            object x = item.KeyValuesContainer.GetKeyValue(SortKey);
            object y = itemToCompareWith.KeyValuesContainer.GetKeyValue(SortKey);

            int iResult = 0;
            #region CHECK EMPTY - NOT IMPLAMENT
            //if (itemToCompareWith.Equals(item))
            //{
            //    iResult = 0;
            //}
            //else if (item == null || itemToCompareWith == null)
            //{
            //    if (item == null)
            //        iResult = -1;
            //    else
            //        iResult = 1;
            //}
            //else 
            #endregion
            switch (GetTypeCodeByKey(SortKey))
            {
                case TypeCode.Boolean:
                    bool xBoolean = ConvertToValue.ConvertToBool(x);
                    bool yBoolean = ConvertToValue.ConvertToBool(y);
                    iResult = xBoolean == yBoolean ? 0 : xBoolean == true ? -1 : 1;
                    break;
                case TypeCode.DateTime:
                    iResult = DateTime.Compare(ConvertToValue.ConvertToDateTime(x), ConvertToValue.ConvertToDateTime(y));
                    break;
                case TypeCode.Decimal:
                    iResult = Decimal.Compare(ConvertToValue.ConvertToDecimal(x), ConvertToValue.ConvertToDecimal(y));
                    break;
                case TypeCode.Double:
                    double xDouble = ConvertToValue.ConvertToDouble(x);
                    double yDouble = ConvertToValue.ConvertToDouble(y);
                    iResult = xDouble == yDouble ? 0 : xDouble > yDouble ? -1 : 1;
                    break;
                case TypeCode.Int32:
                    int xInt32 = ConvertToValue.ConvertToInt32(x);
                    int yInt32 = ConvertToValue.ConvertToInt32(y);
                    iResult = xInt32 == yInt32 ? 0 : xInt32 > yInt32 ? -1 : 1;
                    break;
                case TypeCode.String:
                    iResult = String.Compare(ConvertToValue.ConvertToString(x), ConvertToValue.ConvertToString(y));
                    break;
                default:
                    iResult = 0;
                    break;
            }
            return SortDesc ? iResult : iResult * -1;

        }
        //Auto public static TypeCode GetTypeCodeByKey(KeyValuesType SortKey)
        // Replace Or Add Function GetTypeCodeByKey in KeyValuesContainer<ITEM> generic class
        // Replace Or Add Function GetTypeCodeByKey in KeyValuesContainer<ITEM> generic class
        #region GetTypeCodeByKey
        public static TypeCode GetTypeCodeByKey(KeyValuesType SortKey)
        {
            return DBKeyValurPair.GetTypeCodeByKey(SortKey);
        }
        #endregion
        public int Count
        {
            get
            {
                if (KeyValuesList == null)
                    return 0;
                return KeyValuesList.Count;
            }
        }
        private List<KeyValues<ITEM>> KeyValuesList = new List<KeyValues<ITEM>>();
        public List<KeyValues<ITEM>> DataSource
        {
            get
            {
                if (this.KeyValuesList == null)
                    this.KeyValuesList = new List<KeyValues<ITEM>>();
                return this.KeyValuesList;
            }
        }
        public void Add(KeyValuesContainer<ITEM> items)
        {
            if (this.Count == 0)
            {
                this.KeyValuesList = new List<KeyValues<ITEM>>();
            }
            this.DataSource.AddRange(items.DataSource);
        }
        public void Add(KeyValuesType keyValuesType, bool isPrimery = false, bool Isnullable = true, bool IsSickID = false, bool isMirrorable = false, bool isShareedKey = false)
        {
            KeyValues<ITEM> key = new KeyValues<ITEM>();
            key.IsCheckable = isPrimery;
            key.KeyValuesType = keyValuesType;
            key.IsNullable = Isnullable;
            key.IsSickID = IsSickID;
            key.IsMirrorable = isMirrorable;
            key.IsSharedKey = isShareedKey;
            Add(key, true);
        }
        public int Add(KeyValues<ITEM> oKeyValues, bool withMarge = false)
        {
            if (oKeyValues == null) return -1;
            if (withMarge)
            {
                KeyValues<ITEM> okv = Find(oKeyValues);
                if (okv != null)
                { //found match
                    okv.Marge(okv);
                    return 0;
                }
            }

            oKeyValues.Index = Count;
            if (KeyValuesList == null)
                KeyValuesList = new List<KeyValues<ITEM>>();

            KeyValuesList.Add(oKeyValues);
            return 1;
        }

        public KeyValues<ITEM> Find(KeyValues<ITEM> oKeyValues)
        {
            if (KeyValuesList == null) return null;
            foreach (KeyValues<ITEM> oKv in KeyValuesList)
            {
                if (oKv.Equals(oKeyValues))
                {
                    return oKv;
                }
            }
            return null;
        }
        public KeyValues<ITEM> Find(KeyValuesType keyValuesType)
        {
            if (KeyValuesList == null) return null;
            foreach (KeyValues<ITEM> oKv in KeyValuesList)
            {
                if (oKv.KeyValuesType == keyValuesType)
                {
                    return oKv;
                }
            }
            return null;
        }
        public int Remove(KeyValues<ITEM> oKeyValues)
        {
            int iResult = -1;
            if (oKeyValues != null)
            {
                KeyValues<ITEM> okv = Find(oKeyValues);
                if (okv != null)
                { //found match
                    if (okv.Index > -1)
                    {
                        KeyValuesList.RemoveAt(okv.Index);
                        iResult = 1;
                    }
                }
                iResult = 0;
            }
            return iResult;
        }
        public bool IsEquals(KeyValuesContainer<ITEM> oKeyValuesContainer, bool isStrong)
        {
            if (this.KeyValuesList == null || oKeyValuesContainer == null)
                return (oKeyValuesContainer != null && oKeyValuesContainer.KeyValuesList == KeyValuesList) || (oKeyValuesContainer == null && this.KeyValuesList == null);

            foreach (KeyValues<ITEM> oKeyValues in oKeyValuesContainer.KeyValuesList)
            {
                KeyValues<ITEM> oKv = Find(oKeyValues);
                if (oKv != null)
                {
                    if (oKv.IsStrongKey == isStrong)
                    {
                        if (!oKv.Equals(oKeyValues))
                            return false;
                    }
                }
                //else skip 
            }
            return true;
        }
        public object GetKeyValue(KeyValuesType oKeyValuesType)
        {
            if (this.KeyValuesList == null)
                return null;
            KeyValues<ITEM> oKv = Find(oKeyValuesType);
            if (oKv != null)
            {
                if (oKv.Key == null)
                    return null;
                else return oKv.Key;
            }
            return null;
        }
        public KeyValues<ITEM> GetKey(KeyValuesType oKeyValuesType)
        {
            if (this.KeyValuesList == null)
                return null;
            KeyValues<ITEM> oKv = Find(oKeyValuesType);
            if (oKv != null)
            {
                if (oKv.Key == null)
                    return null;
                else return oKv;
            }
            return null;
        }
        public KeyValuesContainer<ITEM> GetKey(KeyValuesStrong oKeyValuesStrong)
        {
            if (this.KeyValuesList == null)
                return null;
            KeyValuesContainer<ITEM> oKvContainer = null;
            foreach (KeyValues<ITEM> oKeyValues in this.KeyValuesList)
            {
                if (oKvContainer == null)
                    oKvContainer = new KeyValuesContainer<ITEM>();
                if (oKeyValues.IsAlow(oKeyValuesStrong))
                {
                    oKvContainer.Add(oKeyValues);
                }
            }
            return oKvContainer;
        }
        public void SetKey(KeyValuesType oKeyValuesType, object value)
        {
            if (this.KeyValuesList == null)
                return;
            KeyValues<ITEM> oKv = Find(oKeyValuesType);
            if (oKv != null)
            {
                oKv.SetKey(value);
            }
        }
        public void SetKey(KeyValuesType oKeyValuesType, bool isCheckable)
        {
            if (this.KeyValuesList == null)
                return;
            KeyValues<ITEM> oKv = Find(oKeyValuesType);
            if (oKv != null)
            {
                oKv.IsCheckable = isCheckable;
            }
        }
        public void SetCheckableAll(bool isCheckable)
        {
            if (KeyValuesList == null) return;
            foreach (KeyValues<ITEM> oKv in KeyValuesList)
            {
                oKv.IsCheckable = isCheckable;
            }
        }
        internal KeyValuesContainer<ITEM> Marge(KeyValuesContainer<ITEM> keyValuesContainer, bool byStrong = true)
        {
            if (keyValuesContainer == null)
                //no keys to marge 
                return null;
            KeyValuesContainer<ITEM> oKvContainer = null;
            foreach (KeyValues<ITEM> oKeyValues in keyValuesContainer.KeyValuesList)
            {
                KeyValues<ITEM> oKv = Find(oKeyValues);
                if (oKv != null)
                {

                    if (oKeyValues.IsAlow(oKv.KeyValuesStrong) && byStrong || !byStrong)
                    {
                        if (oKvContainer == null)
                            oKvContainer = new KeyValuesContainer<ITEM>();
                        //save object before change
                        oKvContainer.Add(oKeyValues);
                        //change keys
                        oKeyValues.Key = oKv.Key;
                    }
                }
            }
            return oKvContainer;
        }

        #endregion
        #region Mirror
        internal List<KeyValues<ITEM>> GetMirrorKeys()
        {
            if (this.DataSource != null)
            {
                return this.DataSource.FindAll(R => R.IsMirrorable);
            }
            return null;
        }
        public List<KeyValues<ITEM>> MirrorKeys { get { return GetMirrorKeys(); } }
        #endregion
    }
    #region KeysSetup
    public enum KeyValuesStrong
    {
        None = 0,
        Strong = 1,
        Medium = 2,
        Week = 3,
        VeryWeek = 4
    }

    public class DBType
    {
        public static readonly string BigInt = "bigint";
        public static readonly string Binary_50 = "binary(50)";
        public static readonly string Bit = "bit";
        public static readonly string Char_10 = "char(10)";
        public static readonly string Date = "datetime";//"date";
        public static readonly string DateTime = "datetime";
        public static readonly string datetime2_7 = "datetime2(7)";
        public static readonly string Datetimeoffset_7 = "datetimeoffset(7)";
        public static readonly string Decimal_18_0 = "decimal(18, 0)";
        public static readonly string Float = "float";
        public static readonly string Geography = "geography";
        public static readonly string Geometry = "geometry";
        public static readonly string Hierarchyid = "hierarchyid";
        public static readonly string Image = "image";
        public static readonly string Int = "int";
        public static readonly string Money = "money";
        public static readonly string Nchar_10 = "nchar(10)";
        public static readonly string Ntext = "ntext";
        public static readonly string Numeric_18_0 = "numeric(18, 0)";
        public static readonly string Numeric_18_2 = "numeric(18, 2)";

        public static readonly string Nvarchar_50 = "nvarchar(50)";
        public static readonly string Nvarchar_20 = "nvarchar(20)";

        public static readonly string Nvarchar_100 = "nvarchar(100)";
        public static readonly string Nvarchar_150 = "nvarchar(150)";
        public static readonly string Nvarchar_200 = "nvarchar(200)";
        public static readonly string Nvarchar_250 = "nvarchar(250)";
        public static readonly string Nvarchar_1024 = "nvarchar(1024)";
        public static readonly string Nvarchar_2048 = "nvarchar(2048)";
        public static readonly string Nvarchar_8000 = "nvarchar(8000)";

        public static readonly string Nvarchar_MAX = "nvarchar(MAX)";
        public static readonly string Real = "real";
        public static readonly string Smalldatetime = "smalldatetime";
        public static readonly string Smallint = "smallint";
        public static readonly string Smallmoney = "smallmoney";
        public static readonly string Sql_variant = "sql_variant";
        public static readonly string Text = "text";
        public static readonly string Time_7 = "time(7)";
        public static readonly string TimeStamp = "timestamp";
        public static readonly string Tinyint = "tinyint";
        public static readonly string Uniqueidentifier = "uniqueidentifier";
        public static readonly string Varbinary_50 = "varbinary(50)";
        public static readonly string Varbinary_MAX = "varbinary(MAX)";
        public static readonly string Varchar_50 = "varchar(50)";
        public static readonly string Varchar_MAX = "varchar(MAX)";
        public static readonly string Xml = "xml";
        public string GetCustomChar(int CharLength)
        {
            return string.Format("nvarchar({0})", CharLength);
        }


    }

    public class KeyValues<ITEM>
        where ITEM : ContainerItem<ITEM>, new()
    {
        public KeyValues()
        {
            KeyValuesStrong = KeyValuesStrong.None;
            IsCheckable = false;
            IsMargeable = true;
            Key = null;
            IsMirrorable = true;
            Index = -1;
        }
        #region IKeyValuesable Members
        public object Key { get; set; }
        public KeyValuesType KeyValuesType { get; set; }
        public bool IsAlow(KeyValuesStrong oKeyValuesStrong)
        {
            return (KeyValuesStrong <= oKeyValuesStrong);
        }
        public System.Type keyType { get { return Key == null ? null : Key.GetType(); } }
        public System.TypeCode keyTypeCode { get { return Key == null ? TypeCode.Empty : Type.GetTypeCode(Key.GetType()); } }
        public bool IsMargeable { get; set; }
        public bool IsMirrorable { get; set; }
        public bool IsSickID { get; set; }
        public bool IsCheckable { get; set; }
        public bool IsNullable { get; set; }
        public bool IsStrongKey { get { return IsCheckable && IsMargeable; } }
        public bool IsWeekKey { get { return !IsStrongKey; } }
        public bool IsSharedKey { get; set; }
        public bool Equals(KeyValues<ITEM> obj)
        {
            if (obj == null)
                return false;
            if (this.KeyValuesType == obj.KeyValuesType)
            {
                return true;

                //if (!IsCheckable)
                //    return true;
                //else
                //    return Object.Equals(Key, obj.Key);// false;

            }
            return false;
        }
        #endregion
        public void Marge(KeyValues<ITEM> okv)
        {
            if (okv == null) return;
            if (IsAlow(okv.KeyValuesStrong))
            {
                IsCheckable = okv.IsCheckable;
                IsMargeable = okv.IsMargeable;
                SetKey(okv.Key);
            }
        }
        public int Index { get; set; }
        public KeyValuesStrong KeyValuesStrong { get; set; }
        public void SetKey(object obj)
        {
            this.Key = obj;
            //if (obj == null)
            //{
            //    //if (this.IsMargeable)
            //    this.Key = obj;
            //    return;
            //}
            //else if (this.Key == null)
            //    this.Key = obj;
            //else if (obj.GetType() == this.keyType)
            //    this.Key = obj;
        }
        internal static bool Copy(KeyValuesContainer<ITEM> keyValuesContainerSource, KeyValuesContainer<ITEM> keyValuesContainerDest)
        {
            if (keyValuesContainerSource == null || keyValuesContainerDest == null)
                return false;

            foreach (KeyValues<ITEM> kval in keyValuesContainerDest.DataSource)
            {
                KeyValues<ITEM> oKeyValues = keyValuesContainerSource.Find(kval);
                if (oKeyValues != null)
                {
                    kval.SetKey(oKeyValues.Key);
                }
            }
            return true;
        }
    }
    #endregion
    #region KeyFactory
    //Auto File 
    #endregion
    //public static class DBKeyValurPair
    //public enum KeyValuesType
    #endregion
    #region Generic
    #region Generic Containers And ContainerItems
    public delegate ITEM ConvertHandler<ITEM>(DataRow dr);

    #region Generic Class
    //Item
    public abstract class ContainerItem<I> : ContainerItemAble<I>
        where I : ContainerItem<I>, new()
    {
        #region ContainerItemAble<I> Members
        [XmlIgnore]
        public KeyValuesContainer<I> KeyValuesContainer { get; set; }
        /*
        #region Relations
        private Container<ContainerItem<I>,I> relations { get; set; }
        
        public void RelationAdd(ContainerItem<I> relation){
            relations.Add(relation);
        }

        public int CountRelation { 
            get 
            {
                if (relations == null) return 0;
                return relations.Count;            
            } 
        }

        public KeyValuesContainer<I> RelationsKeyValuesContainer
        {
            get
            {
                if (this.CountRelation == 0) return null;
                KeyValuesContainer<I> keys = new KeyValuesContainer<I>();
                keys.Add(this.KeyValuesContainer);
                for (int i=0;i<CountRelation;i++)
                {
                    keys.Add(relations[i].RelationsKeyValuesContainer);
                }
                return keys;
            }
        }

        public KeyValuesContainer<I> RelationsKeyValuesContainerAtIndex(int index)
        {
            if (this.CountRelation == 0) return null;
            if (index > CountRelation || index < 0) throw new Exception("Relation Index is NOT exist");
            KeyValuesContainer<I> keys = new KeyValuesContainer<I>();
            keys.Add(this.KeyValuesContainer);
            keys.Add(relations[index].KeyValuesContainer);
            return keys;
        }
        public int CountRelationKeyValuesContainerAtIndex(int index)
        {
                if (relations == null) return 0;
                if (index > CountRelation || index < 0) throw new Exception("Relation Index is NOT exist");

                return relations[index].KeyValuesContainer.Count + this.KeyValuesContainer.Count;
        }
        #endregion
        */
        #region Operators
        public bool EqualsWeek(I itemCompareWith)
        {
            return (this.KeyValuesContainer.IsEquals(itemCompareWith.KeyValuesContainer, false));
        }
        public bool EqualsStrong(I itemCompareWith)
        {
            return (this.KeyValuesContainer.IsEquals(itemCompareWith.KeyValuesContainer, true));
        }
        public bool Equals(I itemCompareWith)
        {
            if (itemCompareWith == null)
                return false;
            return EqualsStrong(itemCompareWith);
        }

        #region Calling DB Basic
        public int Insert(out int? ID)
        {
            Type ClassInfo = this.GetType();
            int iRefID = -1;
            ID = null;
            if (true)
            {
                try
                {
                    DB db = new DB();
                    #region DB Params (IN/OUT)
                    bool isTranslateAblee = false;
                    //Get All Keys
                    foreach (var Keys in KeyValuesContainer.DataSource)
                    {
                        //If It Is Sick ID Key (it insert automaticlley)    
                        if (Keys.IsSickID)
                        {
                            continue;
                        }
                        if (Keys.IsMirrorable)
                        {
                            isTranslateAblee = true;
                        }
                        KeyValuePair<string, string> KeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(Keys.KeyValuesType);
                        if (KeyValue.Key.Equals("white_label_doc_id"))
                        {
                            int whitelabel = BasePage.CurrWhiteLableDocId;
                            Keys.Key = whitelabel;
                            //db.AddParameter(string.Format("{0}_{1}", "@prm", KeyValue.Key), ConvertTo.ConvertEmptyToDBNull(whitelabel));
                        }
                        else
                        {
                        }

                        db.AddParameter(string.Format("{0}_{1}", "@prm", KeyValue.Key), ConvertTo.ConvertEmptyToDBNull(Keys.Key));

                    }
                    ////---set output(returned) parameter----------------------//
                    SqlParameter p = new SqlParameter("@prm_out_docid", 0);
                    p.Direction = ParameterDirection.Output;
                    db.AddParameter(p);
                    ////------------------------------------------------------//
                    if (isTranslateAblee)
                    {
                        db.AddParameter(string.Format("@prm_lang"), ConvertTo.ConvertEmptyToDBNull(Translator<I>.LangId));
                    }
                    #region 02/12/2013 white label id
                    //need  to check that whitelable property is exist 
                    //if (System.ComponentModel.TypeDescriptor.GetProperties(this)["WhiteLabelDocId"] != null)
                    //{
                    //    int whitelabel = BasePage.CurrWhiteLableDocId;
                    //    //int whitelabel_value = ConvertToValue.ConvertToInt((System.ComponentModel.TypeDescriptor.GetProperties(this)["WhiteLable"] as System.ComponentModel.PropertyDescriptor).GetValue(this)));
                    //    //if (ConvertToValue.IsEmpty(whitelabel_value)) { 

                    //    //}
                    //    (System.ComponentModel.TypeDescriptor.GetProperties(this)["WhiteLabelDocId"] as System.ComponentModel.PropertyDescriptor).SetValue(this, whitelabel);
                    //    db.AddParameter(string.Format("@prm_white_label_doc_id"), ConvertTo.ConvertEmptyToDBNull(whitelabel));
                    //}
                    //else
                    //{
                    //    //Object not support whitelabel
                    //}
                    #endregion

                    #endregion
                    iRefID = db.ExecuteNonQuery(string.Format("{0}{1}", "Insert_", ClassInfo.Name));
                    ID = ConvertTo.ConvertToInt32(p.Value);
                }
                catch (Exception ex)
                {
                    //   throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AttachedDock, "InsertAttachedDocs", ex.Message));
                }
                finally { }
            }
            return iRefID;
        }
        public int Expunge(int? id)
        {
            int iRefID = -1;
            Type ClassInfo = this.GetType();
            DataSet ds = null;
            var PrimeryKey = this.KeyValuesContainer.DataSource.FirstOrDefault(K => K.IsSickID);
            //If There is no Primery Key Can't Use Genric Procedure
            if (PrimeryKey == null)
            {
                return iRefID;
            }
            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);
            if (id != null)
            {
                try
                {
                    DB db = new DB();
                    #region DB Params (IN/OUT)
                    //----add parameters-------------------------------------//                    
                    db.AddParameter(string.Format("{0}_{1}", "@prm", PrimeryKeyValue.Key), ConvertTo.ConvertEmptyToDBNull(id));
                    //-------------------------------------------------------//                    
                    #endregion
                    iRefID = db.ExecuteNonQuery(string.Format("{0}_{1}", "Expunge", ClassInfo.Name));
                }
                catch (Exception ex)
                {
                    //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AttachedDock, "DeleteAttachedDocs", ex.Message));
                }
                finally { }
            }
            return iRefID;
        }
        public int Update()
        {
            int iRefID = -1;
            Type ClassInfo = this.GetType();
            var PrimeryKey = this.KeyValuesContainer.DataSource.FirstOrDefault(K => K.IsSickID);
            //If There is no Primery Key Can't Use Genric Procedure
            if (PrimeryKey == null)
            {
                return iRefID;
            }
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                bool isTranslateAblee = false; ;
                //Get All Keys
                foreach (var Keys in KeyValuesContainer.DataSource)
                {
                    KeyValuePair<string, string> KeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(Keys.KeyValuesType);
                    db.AddParameter(string.Format("{0}_{1}", "@prm", KeyValue.Key), ConvertTo.ConvertEmptyToDBNull(Keys.Key));
                    if (Keys.IsMirrorable) isTranslateAblee = true;
                }

                if (isTranslateAblee)
                {
                    db.AddParameter(string.Format("@prm_lang"), ConvertTo.ConvertEmptyToDBNull(Translator<I>.LangId));
                }
                #endregion
                iRefID = db.ExecuteNonQuery(string.Format("{0}{1}", "Update_", ClassInfo.Name));
            }
            catch (Exception ex)
            {
                //throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.InvoiceFaults, "UpdateInvoiceFaults", ex.Message));
            }
            finally { }

            return iRefID;
        }
        public DataSet SelectAll(int? whiteLabelDocId)
        {
            Type ClassInfo = this.GetType();
            DataSet ds = null;
            try
            {
                DB db = new DB();
                bool isWithWhiteLable = false;
                KeyValuePair<string, string> WhiteLabeKeyValue;
                foreach (KeyValues<I> key in this.KeyValuesContainer.DataSource)
                {
                    KeyValuePair<string, string> WhiteLabeKeyValueTemp = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                    if (WhiteLabeKeyValueTemp.Key.Equals(SQL_Tamplate<I>.WhiteLabelDocId) && !key.IsSickID)
                    {
                        WhiteLabeKeyValue = WhiteLabeKeyValueTemp;
                        isWithWhiteLable = true;
                        break;
                    }
                }

                if (isWithWhiteLable)
                {
                    db.AddParameter(string.Format("{0}_{1}", "@prm", "white_label_doc_id"), ConvertTo.ConvertEmptyToDBNull(whiteLabelDocId));
                }

                ds = db.ExecuteDataSet(string.Format("{0}{1}", "Select_All_", ClassInfo.Name));
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AttachedDock, "DeleteAttachedDocs", ex.Message));
            }
            finally { }
            return ds;
        }
        public DataSet SelectByID(int id, int? whiteLabelDocId)
        {
            Type ClassInfo = this.GetType();
            DataSet ds = null;
            var PrimeryKey = this.KeyValuesContainer.DataSource.FirstOrDefault(K => K.IsSickID);
            //If There is no Primery Key Can't Ganerte Procedure
            if (PrimeryKey == null)
            {
                return ds;
            }
            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//
                db.AddParameter(string.Format("{0}_{1}", "@prm", PrimeryKeyValue.Key), ConvertTo.ConvertEmptyToDBNull(id));
                //if (!ConvertToValue.IsEmpty(whiteLabelDocId))
                //{
                //var WhiteLabelKey = this.KeyValuesContainer.DataSource.FirstOrDefault(K => K.Key.Equals("WhiteLabelDocId"));
                //if (WhiteLabelKey != null)
                //{
                //    KeyValuePair<string, string> WhiteLabeKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(WhiteLabelKey.KeyValuesType);
                //    db.AddParameter(string.Format("{0}_{1}", "@prm", WhiteLabeKeyValue.Key), ConvertTo.ConvertEmptyToDBNull(whiteLabelDocId));
                //}
                //}
                //-
                
                //bool isWithWhiteLable = false;
                //KeyValuePair<string, string> WhiteLabeKeyValue;
                //foreach (KeyValues<I> key in this.KeyValuesContainer.DataSource)
                //{
                //    KeyValuePair<string, string> WhiteLabeKeyValueTemp = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                //    if (WhiteLabeKeyValueTemp.Key.Equals("white_label_doc_id"))
                //    {
                //        WhiteLabeKeyValue = WhiteLabeKeyValueTemp;
                //        isWithWhiteLable = true;
                //        break;
                //    }
                //}

                //if (isWithWhiteLable)
                //{
                //    db.AddParameter(string.Format("{0}_{1}", "@prm", "white_label_doc_id"), ConvertTo.ConvertEmptyToDBNull(whiteLabelDocId));
                //} 
                //------------------------------------------------------//
                #endregion
                ds = db.ExecuteDataSet(string.Format("{0}{1}{2}", "Select_", ClassInfo.Name, "_By_Id"));
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OpenedReservations, "GetopenReservationsByPnr", ex.Message));
            }
            return ds;
        }

        #endregion

        public object GetKey(KeyValuesType oKeyValuesType)
        {
            if (this.KeyValuesContainer == null) return null;
            return this.KeyValuesContainer.GetKeyValue(oKeyValuesType);
        }
        public void SetKey(KeyValuesType oKeyValuesType, object value)
        {
            //if (this.KeyValuesContainer == null) return;
            //if (ConvertToValue.IsEmpty(value))
            //{
            //    value = null;
            //}
            this.KeyValuesContainer.SetKey(oKeyValuesType, value);
        }
        public void SetKey(KeyValuesType oKeyValuesType, bool value)
        {
            if (this.KeyValuesContainer == null) return;
            this.KeyValuesContainer.SetKey(oKeyValuesType, value);
        }
        #endregion

        #region DB
        public abstract int Action(DB_Actions dB_Actions);
        public static ConvertHandler<I> ConvertEvent;
        public static I Convert(DataRow dr)
        {
            return ConvertEvent(dr);
        }
        public static List<I> Convert(DataSet ds)
        {
            List<I> list = null;
            if (ds != null)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list.Add(Convert(dr));
                }
            }
            return list;
        }
        #endregion
        public I Clone()
        {
            I item = new I();
            if (ContainerItem<I>.Copy(this, item))
                return item;
            return null;
        }
        public static bool Copy(ContainerItem<I> itemSource, I itemDest)
        {
            bool isCopy = false;
            if (itemSource != null)
            {
                isCopy = KeyValues<I>.Copy(itemSource.KeyValuesContainer, itemDest.KeyValuesContainer);
            }
            return isCopy;
        }
        #endregion


        #region GenerateDB reflection
        //return list of key pair foreach key
        public List<KeyValuePair<string, string>> GetKeys()
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            foreach (KeyValues<I> key in this.KeyValuesContainer.DataSource)
            {
                list.Add(DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType));
            }
            return list;
        }

        public string GenerateDBTabls(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetCreateTable(this.KeyValuesContainer.DataSource, ClassName);
        }
        public string GenerateInsertProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetInsertIntoTable(this.KeyValuesContainer.DataSource, ClassName);
        }
        public string GenerateUpdateProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetUpadteTable(this.KeyValuesContainer.DataSource, ClassName);
        }
        //Expunge -- Delete Db
        public string GenerateExpungeProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetExpungeTable(this.KeyValuesContainer.DataSource, ClassName);
        }
        public string GenerateSelectAllProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetSelectAll(this.KeyValuesContainer.DataSource, ClassName);
        }
        public string GenerateSelectByIDProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetSelectByID(this.KeyValuesContainer.DataSource, ClassName);
        }
        public int GetCount()
        {
            return this.KeyValuesContainer.Count;
        }

        public bool ValidateFilters(List<KeyValues<I>> listFilterBy)
        {
            foreach (KeyValues<I> keyFilter in listFilterBy)
            {
                if (this.KeyValuesContainer.Find(keyFilter) == null) return false;
            }
            return true;
        }

        public string GenerateSelectCombinations(string ClassName, List<int> comb)
        {
            List<KeyValues<I>> filters = new List<KeyValues<I>>();
            bool isWithWhiteLable = false;
            foreach (var itemKey in this.KeyValuesContainer.DataSource)
            {
                //get key name and type
                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(itemKey.KeyValuesType);
                string cellName = key.Key;
                string cellType = key.Value;

                //Check if existing whiteLabe 
                if (cellName == SQL_Tamplate<I>.WhiteLabelDocId && !ClassName.Equals(SQL_Tamplate<I>.WhiteLabelTableName))
                {
                    filters.Add(itemKey);
                    isWithWhiteLable = true;
                }
            }
            foreach (int combinationIndex in comb)
            {
                if (isWithWhiteLable)
                {
                    //get key name and type
                    KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(this.KeyValuesContainer.DataSource[combinationIndex].KeyValuesType);
                    string cellName = key.Key;
                    string cellType = key.Value;
                    //Check if existing whiteLabe 
                    if (cellName == SQL_Tamplate<I>.WhiteLabelDocId)
                    {
                        //continue;
                        return null;
                    }
                }
                filters.Add(this.KeyValuesContainer.DataSource[combinationIndex]);
            }

            return SQL_Tamplate<I>.SQL_GetSelectByCombination(new I(), filters, ClassName);
        }

        public string GenerateDeleteSelectCombinations(string ClassName, List<int> comb)
        {
            List<KeyValues<I>> filters = new List<KeyValues<I>>();
            bool isWithWhiteLable = false;
            foreach (var itemKey in this.KeyValuesContainer.DataSource)
            {
                //get key name and type
                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(itemKey.KeyValuesType);
                string cellName = key.Key;
                string cellType = key.Value;

                //Check if existing whiteLabe 
                if (cellName == SQL_Tamplate<I>.WhiteLabelDocId && !ClassName.Equals(SQL_Tamplate<I>.WhiteLabelTableName))
                {
                    filters.Add(itemKey);
                    isWithWhiteLable = true;
                }
            }

            foreach (int combinationIndex in comb)
            {
                if (isWithWhiteLable)
                {
                    //get key name and type
                    KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(this.KeyValuesContainer.DataSource[combinationIndex].KeyValuesType);
                    string cellName = key.Key;
                    string cellType = key.Value;
                    //Check if existing whiteLabe 
                    if (cellName == SQL_Tamplate<I>.WhiteLabelDocId)
                    {
                        //continue;
                        return null;
                    }
                }
                filters.Add(this.KeyValuesContainer.DataSource[combinationIndex]);
            }
            return SQL_Tamplate<I>.SQL_GetSelectByCombination_Delete(new I(), filters, ClassName);
        }

        //
        //
        //
        //
        //

        public string GenerateDeleteSelectAllProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetSelectAll_Delete(ClassName);
        }


        public string GenerateDeleteCreateTableProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetCreateTable_Delete(ClassName);
        }

        public string GenerateDeleteInsertIntoProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetInsertIntoTable_Delete(ClassName);
        }


        public string GenerateDeleteSelectByIDProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetSelectByID_Delete(ClassName);
        }

        public string GenerateDeleteUpdateProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetUpadteTable_Delete(ClassName);
        }

        public string GenerateDeleteExpungeProcedure(string ClassName)
        {
            return SQL_Tamplate<I>.SQL_GetExpunge_Delete(ClassName);
        }


        //public KeyValuePair<string, string> GenerateSelectByCombinationProcedure(string ClassName, List<KeyValues<I>> listFilterBy)
        //{
        //    if (!ValidateFilters(listFilterBy)) throw new Exception("Some of the filters are not included in the key list");
        //    string selectProc = SQL_Tamplate<I>.SQL_GetSelectByCombination(new I(), listFilterBy, ClassName);
        //    string deleteProc = SQL_Tamplate<I>.SQL_GetSelectByCombination(new I(), listFilterBy, ClassName);
        //    return new KeyValuePair<string, string>(selectProc, deleteProc);
        //}
        #endregion

        [XmlIgnore]
        public int DocId_Value { get { return ConvertToValue.ConvertToInt32((System.ComponentModel.TypeDescriptor.GetProperties(this)["DocId_Value"] as System.ComponentModel.PropertyDescriptor).GetValue(this)); } }

        //public bool? IsActive { get; set; }
        //public bool? IsDeleted { get; set; }
    }

    //Container
    public abstract class Container<C, I> : ContainerAble<C, I>
        where C : Container<C, I>, new()
        where I : ContainerItem<I>, new()
    {


        //Function for amount of results displaying(pager)
        public C GetTopResult(int amountOfResults)
        {

            C oContainerToReturn = new C();
            for (int i = 0; i < amountOfResults && i < Count; i++)
            {
                oContainerToReturn.Add(DataSource[i]);
            }
            return oContainerToReturn;
        }

        #region ContainerAble<C,I> Members
        virtual public I EmptyObj()
        {
            return null;
        }

        #region Add

        public void Add(IEnumerable<I> itemList)
        {
            if (itemList == null)
                return;
            foreach (I item in itemList.ToList())
            {
                Add(item);
            }
        }
        public void Add(List<I> itemList)
        {
            if (itemList == null)
                return;
            foreach (I item in itemList)
            {
                Add(item);
            }
        }
        public void Add(C itemContainer)
        {
            if (itemContainer == null)
                return;
            Add(itemContainer.DataSource);
        }
        public void Add(I oItem)
        {
            if (oItem == null)
                return;
            this.DataSource.Add(oItem);
        }
        #endregion
        #region Remove
        public void Remove(int doc_id)
        {
            this.DataSource.RemoveAll(R => ConvertTo.ConvertToInt32((System.ComponentModel.TypeDescriptor.GetProperties(R)["DocId_Value"] as System.ComponentModel.PropertyDescriptor).GetValue(R)).Value == doc_id);
        }
        #endregion
        #region GroupBy
        public C GroupByDocID()
        {
            C itemContainer = new C();
            itemContainer.Add(this.ItemList.GroupBy(R => ConvertTo.ConvertToInt32((System.ComponentModel.TypeDescriptor.GetProperties(R)["DocId_Value"] as System.ComponentModel.PropertyDescriptor).GetValue(R)).Value).Select(group => group.FirstOrDefault()).Where(item => item != null).ToList());
            return itemContainer;
        }

        public C GroupByDocID(int items)
        {
            C itemContainer = new C();
            itemContainer.Add(this.ItemList.GroupBy(R => ConvertTo.ConvertToInt32((System.ComponentModel.TypeDescriptor.GetProperties(R)["DocId_Value"] as System.ComponentModel.PropertyDescriptor).GetValue(R)).Value).Select(group => group.Count() == items ? group.FirstOrDefault() : null).Where(item => item != null).ToList());
            return itemContainer;
        }


        #endregion
        #region Find
        public I FindFirstOrDefault(Func<I, bool> match)
        {
            return DataSource.FirstOrDefault(match);
        }
        public List<I> FindAllList(Predicate<I> match)
        {
            return DataSource.FindAll(match);
        }
        public C FindAllContainer(Predicate<I> match)
        {
            C itemContainer = new C();
            itemContainer.Add(FindAllList(match));
            return itemContainer;
        }
        public I Find(int doc_id)
        {
            return this.FindFirstOrDefault(R => ConvertTo.ConvertToInt32((System.ComponentModel.TypeDescriptor.GetProperties(R)["DocId_Value"] as System.ComponentModel.PropertyDescriptor).GetValue(R)).Value == doc_id);
        }
        public I Find(I oItem)
        {

            if (oItem == null)
                return null;
            foreach (I item in DataSource)
            {
                if (item.Equals(oItem))
                {
                    return item;
                }
            }
            return new I();
        }
        public bool IsExist(int doc_id)
        {
            return this.Find(doc_id) != null;
        }
        public C FindByDocIdContainer(int doc_id)
        {
            return this.FindAllContainer(R => ConvertTo.ConvertToInt32((System.ComponentModel.TypeDescriptor.GetProperties(R)["DocId_Value"] as System.ComponentModel.PropertyDescriptor).GetValue(R)).Value == doc_id);
        }

        #endregion

        #region Prop
        [XmlIgnore]
        public List<I> ItemList { get; set; }
        [XmlIgnore]
        public int Count
        {
            get { return DataSource.Count; }
        }

        public List<I> DataSource
        {
            get
            {
                if (this.ItemList == null)
                    this.ItemList = new List<I>();
                return this.ItemList;
            }
        }
        [XmlIgnore]
        public List<I> DataSourceClone
        {
            get
            {
                List<I> itemListNew = new List<I>();
                foreach (I item in DataSource)
                {
                    itemListNew.Add(item.Clone());
                }
                return itemListNew;
            }
        }
        [XmlIgnore]
        public I First
        {
            get
            {
                if (Count == 0)
                    return null;
                return DataSource[0];
            }
        }
        [XmlIgnore]
        public I Single
        {
            get
            {
                if (Count == 0)
                    return null;
                if (Count > 1)
                {
                    throw new Exception("More Than One Object on DataSourse " + this.GetType());
                }
                return DataSource[0];
            }
        }
        #endregion

        #region Indexer
        public I this[int index]
        {
            get
            {
                if (index > (Count - 1))
                    //Exception
                    return null;
                return DataSource[index];
            }
            set
            {
                if (index > (Count - 1))
                    //Exception
                    return;
                DataSource[index] = value;
            }
        }
        public void RemoveAt(int index)
        {
            if (Count == 0)
                return;
            if ((index + 1) <= Count)
            {
                this.DataSource.RemoveAt(index);
            }
        }
        #endregion

        #region Clone And Convert
        public static C Convert(DataSet ds)
        {
            C itemContainer = new C();
            if (ds != null)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    itemContainer.Add(ContainerItem<I>.Convert(dr));
                }
            }
            return itemContainer;
        }
        public C Clone()
        {
            C containerclone = new C();
            containerclone.Add(this.DataSourceClone);
            return containerclone;
        }
        #endregion

        #region Actions Default
        public C Action(DB_Actions dB_Actions)
        {
            int iResult = -1;
            C itemErrors = null;
            foreach (I item in this.DataSource)
            {
                iResult = item.Action(dB_Actions);
                if (iResult == -1)
                {
                    if (itemErrors == null)
                        itemErrors = new C();
                    itemErrors.Add(item);
                }
            }
            return itemErrors;
        }
        public static C Convert(List<I> list)
        {
            C containerNew = null;
            if (list != null)
            {
                foreach (I item in list)
                {
                    if (containerNew == null)
                        containerNew = new C();
                    containerNew.Add(item);
                }
            }
            return containerNew;
        }
        #endregion
        #endregion

        #region Sort
        public List<I> SortByKey(KeyValuesType keyType, bool desc)
        {
            KeyValuesContainer<I>.SortKey = keyType;
            KeyValuesContainer<I>.SortDesc = desc;
            DataSource.Sort(KeyValuesContainer<I>.Compare);

            return DataSource;
            //return DataSourceClone;
        }
        public C SortByKeyContainer(KeyValuesType keyType, bool desc)
        {
            C oContainer = new C();
            oContainer.Add(SortByKey(keyType, desc));
            return oContainer;
        }
        #endregion

        public static C SelectData(IDataParameterCollection parameters, string procName)
        {
            if (ConvertToValue.IsEmpty(procName) || parameters == null)
            {
                throw new Exception("SelectData - Some of the parameters or the ProcName are wrong");
            }
            DataSet ds = null;
            try
            {

                DB db = new DB();
                #region DB Params (IN/OUT)
                //----set parameters-------------------------------------//
                //db.ClearParams();

                foreach (IDataParameter itemParam in parameters)
                {
                    db.AddParameter(itemParam.ParameterName, itemParam.Value, itemParam.DbType);
                }
                //-------------------------------------------------------//
                #endregion
                ds = db.ExecuteDataSet(procName);
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticCapacity_YaYa, "Select_StaticCapacity_YaYa_By_Keys_View_DateCreated", ex.Message));
            }
            return Convert(ds);

        }
        public object SelectAllObject()
        {
            return SelectAll();
        }
        public C SelectAll(int? whiteLabelDocId = null)
        {
            C objC = new C();
            I objI = new I();
            objC.Add(Convert(objI.SelectAll(whiteLabelDocId)));
            return objC;
        }
        public C SelectByID(int doc_id, int? whiteLabelDocId = null)
        {
            C objC = new C();
            I objI = new I();
            objC.Add(Convert(objI.SelectByID(doc_id, whiteLabelDocId)));
            return objC;
        }

        /// <summary>
        /// Get Delta container between two containers from same type - the "UnExist Items"
        /// </summary>
        /// <param name="oContainer">the full container</param>
        /// <param name="oExistContainer">the exist container</param>
        /// <returns>the delta container</returns>
        public static C GetDelta(C oContainer, C oExistContainer)
        {
            C oUnExistsContainer = new C();
            oUnExistsContainer.Add(oContainer);
            oUnExistsContainer.Add(oExistContainer);
            if (oUnExistsContainer.Count > 0)
            {
                oUnExistsContainer = oUnExistsContainer.GroupByDocID(1);
            }
            return oUnExistsContainer;
        }
    }

    //public class ContainerObject<C, I> : Container<C, I>
    //    where C : Container<C, I>, new()
    //    where I : ContainerItem<I>, new()
    //{ }

    #endregion
    #endregion

    #region DB
    public enum DB_Actions
    {
        Insert = 1,
        Delete = 2,
        Update = 4,
        Expunge = 5,
        Deactivate = 6,
        Activate = 7,
    }
    //Auto file DB TBNames
    #endregion

    #region DBBuilder
    public class DBBuilder
    {
        public static List<object> GetAllClasses()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            List<object> ObList = new List<object>();
            foreach (Type type in asm.GetTypes())
            {
                Type oType = type;
                if (!oType.ContainsGenericParameters && !oType.IsAbstract)
                {
                    object ob = null;
                    try
                    {
                        //Get Instance oF the Class
                        ob = Activator.CreateInstance(type);
                        //if class is part of Db Classes
                        if (ob.GetType().GetMethod("GenerateDBTabls") != null)
                        {
                            ObList.Add(ob);
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Cannont be generated - ");//+ ob.GetType());
                        continue;
                    }
                }

            }

            //return the list
            return ObList;
        }
        public static List<object> GetAllKeys(List<object> classList, string className)
        {
            List<object> keys = null;
            foreach (var item in classList)
            {
                if (string.Equals(className.ToLower(), item.GetType().Name.ToLower()))
                {
                    keys = (item.GetType().GetMethod("GetKeys").Invoke(item, new object[] { item.GetType().Name }) as List<object>);
                }
            }
            return keys;
        }
        public static void GenDB(int maxLevel = 2, string projectFolder = "", string classToDo = null)
        {
            List<object> ObList = DBBuilder.GetAllClasses();
            #region Basic procs
            List<string> lCreateTable = new List<string>();
            List<string> lInsertInto = new List<string>();
            List<string> lUpdate = new List<string>();
            List<string> lExpunge = new List<string>();
            List<string> lSelectByID = new List<string>();
            List<string> lSelectAll = new List<string>();
            List<string> lSelectCombinations = new List<string>();

            List<string> lDeleteCombinations = new List<string>();
            List<string> lDeleteCreateTable = new List<string>();
            List<string> lDeleteInsertInto = new List<string>();
            List<string> lDeleteUpdate = new List<string>();
            List<string> lDeleteSelectByID = new List<string>();
            List<string> lDeleteSelectAll = new List<string>();
            List<string> lDeleteExpunge = new List<string>();


            #region Set combinetions lists
            foreach (var item in ObList)
            {
                try
                {

                    if (classToDo != null)
                    {
                        if (classToDo != item.GetType().Name) continue;
                    }
                    lCreateTable.Add(item.GetType().GetMethod("GenerateDBTabls").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lInsertInto.Add(item.GetType().GetMethod("GenerateInsertProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lUpdate.Add(item.GetType().GetMethod("GenerateUpdateProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lExpunge.Add(item.GetType().GetMethod("GenerateExpungeProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lSelectByID.Add(item.GetType().GetMethod("GenerateSelectByIDProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lSelectAll.Add(item.GetType().GetMethod("GenerateSelectAllProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);

                    lDeleteCreateTable.Add(item.GetType().GetMethod("GenerateDeleteCreateTableProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lDeleteInsertInto.Add(item.GetType().GetMethod("GenerateDeleteInsertIntoProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lDeleteUpdate.Add(item.GetType().GetMethod("GenerateDeleteUpdateProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lDeleteSelectByID.Add(item.GetType().GetMethod("GenerateDeleteSelectByIDProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lDeleteSelectAll.Add(item.GetType().GetMethod("GenerateDeleteSelectAllProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);
                    lDeleteExpunge.Add(item.GetType().GetMethod("GenerateDeleteExpungeProcedure").Invoke(item, new object[] { item.GetType().Name }) as string);

                    //get list of columns
                    int? list = item.GetType().GetMethod("GetCount").Invoke(item, new object[] { }) as int?;
                    int n = (list == null ? 0 : int.Parse(list.ToString()));
                    //number of options
                    int baseNumber = n;
                    int totalCounter;
                    List<List<int>> list_combinations_in;
                    if (maxLevel > n)
                    {
                        maxLevel = n;
                    }
                    for (int i = 1; i <= maxLevel; i++)
                    {
                        int counter;
                        Combinations.GetCombinationsByLevel(out list_combinations_in, i, i, out totalCounter, out counter, n - 1, 0);
                        foreach (List<int> combination in list_combinations_in)
                        {
                            lSelectCombinations.Add(item.GetType().GetMethod("GenerateSelectCombinations").Invoke(item, new object[] { item.GetType().Name, combination }) as string);
                            lDeleteCombinations.Add(item.GetType().GetMethod("GenerateDeleteSelectCombinations").Invoke(item, new object[] { item.GetType().Name, combination }) as string);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Table {0} Cannot be generate", item.GetType());
                    continue;

                }

            }
            #endregion

            #region FullDB string
            string full_db =
                       String.Format(
                       @"
                    --CreateTable
                    {0}
                    SET ANSI_NULLS ON
                    GO
                    SET QUOTED_IDENTIFIER ON
                    GO
                    --InsertInto
                    {1}
                    --Update
                    {2}
                    --Expunge
                    {3}
                    --SelectByID
                    {4}
                    --SelectAll
                    {5}
                    --combinations
                    {6}
                    ",
                        string.Join(Environment.NewLine, lCreateTable),
                        string.Join(Environment.NewLine, lInsertInto),
                        string.Join(Environment.NewLine, lUpdate),
                        string.Join(Environment.NewLine, lExpunge),
                        string.Join(Environment.NewLine, lSelectByID),
                        string.Join(Environment.NewLine, lSelectAll),
                         string.Join(Environment.NewLine, lSelectCombinations)
                       );
            #endregion

            #region Create Tables
            string CreateTables_db =
                       String.Format(
                       @"
                    --CreateTable
                    {0}
                    
                    ",
                        string.Join(Environment.NewLine, lCreateTable)
                       );
            #endregion

            #region Create Proc
            string CreateProc_db =
                       String.Format(
                       @"
                    SET ANSI_NULLS ON
                    GO
                    SET QUOTED_IDENTIFIER ON
                    GO
                    --InsertInto
                    {0}
                    --Update
                    {1}
                    --Expunge
                    {2}
                    --SelectByID
                    {3}
                    --SelectAll
                    {4}
                    --combinations
                    {5}
                    ",
                        string.Join(Environment.NewLine, lInsertInto),
                        string.Join(Environment.NewLine, lUpdate),
                        string.Join(Environment.NewLine, lExpunge),
                        string.Join(Environment.NewLine, lSelectByID),
                        string.Join(Environment.NewLine, lSelectAll),
                         string.Join(Environment.NewLine, lSelectCombinations)
                       );
            #endregion

            #region FullDelete string
            string full_delete_db =
              String.Format(
              @"
            --Delete Combination
            {0}
GO

            {1}
GO
            {2}
GO
            {3}
GO
            {4}
GO
            {5}
GO
            {6}
            ",
                string.Join(Environment.NewLine, lDeleteCreateTable),
                string.Join(Environment.NewLine, lDeleteCombinations),
              string.Join(Environment.NewLine, lDeleteInsertInto),
              string.Join(Environment.NewLine, lDeleteUpdate),
              string.Join(Environment.NewLine, lDeleteSelectByID),
              string.Join(Environment.NewLine, lDeleteSelectAll),
              string.Join(Environment.NewLine, lDeleteExpunge)
              );
            #endregion

            #region FullDelete string
            string Tables_delete_db =
              String.Format(
              @"
            --Delete Combination
            {0}
GO

            ",
                string.Join(Environment.NewLine, lDeleteCreateTable)
              );
            #endregion
            #region FullDelete string
            string Proc_delete_db =
              String.Format(
              @"
            --Delete Combination
            {0}
GO

            {1}
GO
            {2}
GO
            {3}
GO
            {4}
GO
            {5}
            ",
                string.Join(Environment.NewLine, lDeleteCombinations),
              string.Join(Environment.NewLine, lDeleteInsertInto),
              string.Join(Environment.NewLine, lDeleteUpdate),
              string.Join(Environment.NewLine, lDeleteSelectByID),
              string.Join(Environment.NewLine, lDeleteSelectAll),
              string.Join(Environment.NewLine, lDeleteExpunge)
              );
            #endregion
            Files.CreateDir("", projectFolder);
            File.WriteAllText(String.Format("{0}\\fullDB.sql", projectFolder), full_db);
            File.WriteAllText(String.Format("{0}\\CreateTablesDB.sql", projectFolder), CreateTables_db);
            File.WriteAllText(String.Format("{0}\\CreateProcDB.sql", projectFolder), CreateProc_db);


            File.WriteAllText(String.Format("{0}\\fullDB_Delete.sql", projectFolder), full_delete_db);
            File.WriteAllText(String.Format("{0}\\Tables_delete.sql", projectFolder), Tables_delete_db);
            File.WriteAllText(String.Format("{0}\\Proc_delete.sql", projectFolder), Proc_delete_db);
            //Console.WriteLine(full_db);
            #endregion
        }
    }
    #endregion
    #endregion

}
