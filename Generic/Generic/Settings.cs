﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace DL_Generic
{
    class Settings
    {
        public static readonly bool IsStaticTranslated = ConvertToValue.ConvertToBool(ConfigurationSettings.AppSettings["IsStaticTranslated"]);

        public string ConnectionString
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectionString"]; 
            }
        }
    }
}
