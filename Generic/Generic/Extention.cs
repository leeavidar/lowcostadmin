﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DB_Class
{

    /// <summary>
    /// check lisst empty or null
    /// </summary>
    public static class IsNullorEmpty
    {
        public static bool List<T>(List<T> list)
        {
            return list == null || list.Count == 0;
        }
    }
    public static class MyExtensions
    {
        public static string ToCapitalFirst(this string str)
        {
            if (str == null) return null;
            return str.ToUpper()[0] + str.Substring(1);
        }
        public static string ToNoCapital(this string str)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);
            return Regex.Replace(r.Replace(str, "_").ToLower(), "_{2,}", "_");
            //return r.Replace(str.Replace("_", string.Empty), "_").ToLower();
        }
        public static string[] SplitWithCapital(this string str)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return r.Replace(str, " ").Split(' ');
        }
    }
}
