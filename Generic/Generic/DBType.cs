﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace DB_Class
{

    /// <summary>
    /// 
    /// </summary>
    public static class DBTypes
    {
        public static Dictionary<string, string> dicDBTypes { get; set; }
        
        //Set static list of all types
        static DBTypes()
        {
            dicDBTypes = new Dictionary<string, string>();
            FieldInfo[] FieldInfos = typeof(DBTypes).GetFields();
            // sort properties by name
            Array.Sort(FieldInfos,
                    delegate(FieldInfo fieldInfo1, FieldInfo fieldInfo2)
                    { return fieldInfo1.Name.CompareTo(fieldInfo2.Name); });

            // write property names
            foreach (FieldInfo oFieldInfo in FieldInfos)
            {
                //Console.WriteLine(propertyInfo.Name);
                dicDBTypes.Add(oFieldInfo.Name, oFieldInfo.GetValue(null).ToString());
            }
        }
        
        //return all types names and values
        public static string Display()
        {
            string str = "";
            foreach (var dbType in dicDBTypes)
            {
                str += String.Format("Key: {0}, Value: {1}", dbType.Key, dbType.Value);
                str += Environment.NewLine;
            }
            return str;
        }
        
        public static readonly string Int = "int";
        public static readonly string BigInt = "bigint";
        public static readonly string Smallint = "smallint";
        public static readonly string Tinyint = "tinyint";

        public static readonly string Bit = "bit";

        public static readonly string Date = "date";
        public static readonly string DateTime = "datetime";
        public static readonly string Time_7 = "time(7)";
        public static readonly string TimeStamp = "timestamp";
        public static readonly string SmallDateTime = "smalldatetime";

        public static readonly string Datetime2_7 = "datetime2(7)";
        public static readonly string Datetimeoffset_7 = "datetimeoffset(7)";        
        public static readonly string Decimal_18_0 = "decimal(18, 0)";
        public static readonly string Numeric_18_0 = "numeric(18, 0)"; 
        public static readonly string Float = "float";
        public static readonly string Real = "real";
        public static readonly string Money = "money";
        public static readonly string Smallmoney = "smallmoney";

        public static readonly string Nchar_10 = "nchar(10)";
        public static readonly string Nvarchar_50 = "nvarchar(50)";
        public static readonly string Nvarchar_100 = "nvarchar(100)";
        public static readonly string Nvarchar_150 = "nvarchar(150)";
        public static readonly string Nvarchar_200 = "nvarchar(200)";
        public static readonly string Nvarchar_250 = "nvarchar(250)";
        public static readonly string Nvarchar_1024 = "nvarchar(1024)";
        public static readonly string Nvarchar_2048 = "nvarchar(2048)";
        public static readonly string Nvarchar_8000 = "nvarchar(8000)";

        public static readonly string Nvarchar_MAX = "nvarchar(MAX)";
        public static readonly string Varchar_50 = "varchar(50)";
        public static readonly string Varchar_MAX = "varchar(MAX)";
        public static readonly string Char_10 = "char(10)";        
        public static readonly string Xml = "xml";
        public static readonly string Ntext = "ntext";
        public static readonly string Text = "text";

        #region Not Supported
        public static readonly string Sql_variant = "sql_variant";
        public static readonly string Uniqueidentifier = "uniqueidentifier";
        public static readonly string Varbinary_50 = "varbinary(50)";
        public static readonly string Varbinary_MAX = "varbinary(MAX)";
        public static readonly string Binary_50 = "binary(50)";
        public static readonly string Image = "image";
        public static readonly string Geography = "geography";
        public static readonly string Geometry = "geometry";
        public static readonly string Hierarchyid = "hierarchyid";
        #endregion

        public static string GetCustomChar(int CharLength)
        {
            return string.Format("nvarchar({0})", CharLength);
        }

        //return the db declartion (key not found will return the default key value)
        public static string GetTypeValue(string key)
        {
            string value;
            if (dicDBTypes.TryGetValue(key, out value))
            {
                return value;
            }
            //default type
            return DefaultType.Value;
        }

        //return the Field name (key not found will return the default key value)
        public static string GetTypeKey(string value)
        {
            if (dicDBTypes.Values.Contains(value))
            {
                return dicDBTypes.Values.First(R => R == value);
            }
            //default type
            return DefaultType.Key;
        }
        
        //return the string Pair of key and value by key name
        public static KeyValuePair<string,string> GetType(string key)
        {
            string value;
            if (dicDBTypes.TryGetValue(key, out value))
            {
                return dicDBTypes.First(R=>R.Key== key);
            }
            //default type
            return DefaultType;
        }

        public static KeyValuePair<string, string> DefaultType
        {
            get
            {
                return GetType("Nvarchar_50");
            }
        }
        
        public static string DefaultTypeKey
        {
            get
            {
                return GetType("Nvarchar_50").Key;
            }
        }
        
        //function will return the c# type
        public static string GetCodeType(string type) {
            if (type.ToLower().Contains("int")) {
                return "int";
            }
            else if (type.ToLower().Contains("date") || type.ToLower().Contains("time"))
            {
                return "DateTime";
            }
            else if (type.ToLower().Contains("money") || type.ToLower().Contains("decimal") || type.ToLower().Contains("numeric") || type.ToLower().Contains("float") || type.ToLower().Contains("real"))
            {
                return "Double";
            }
            else if (type.ToLower().Contains("char") || type.ToLower().Contains("text") || type.ToLower().Contains("xml"))
            {
                return "string";
            }
            else if (type.ToLower().Contains("bit"))
            {
                return "bool";
            }

            //Geography Geometry Hierarchyid Image
            throw new Exception("Data base type is Not supported ");
        }

        public static string GetTypeCode(string type)
        {
            if (type.ToLower().Contains("int"))
            {
                return "Int32";
            }
            else if (type.ToLower().Contains("date") || type.ToLower().Contains("time"))
            {
                return "DateTime";
            }
            else if (type.ToLower().Contains("money") || type.ToLower().Contains("decimal") || type.ToLower().Contains("numeric") || type.ToLower().Contains("float") || type.ToLower().Contains("real") || type.ToLower().Contains("double"))
            {
                return "Double";
            }
            else if (type.ToLower().Contains("char") || type.ToLower().Contains("text") || type.ToLower().Contains("xml") || type.ToLower().Contains("string"))
            {
                return "String";
            }
            else if (type.ToLower().Contains("bit") || type.ToLower().Contains("bool"))
            {
                return "Boolean";
            }

            //Geography Geometry Hierarchyid Image
            throw new Exception("Data base type is Not supported ");
        }
    }
}
