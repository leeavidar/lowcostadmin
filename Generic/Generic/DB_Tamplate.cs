﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using DL_Generic;
//using DB_Class;
using DB_Class;

namespace DL_Generic
{
    public static class SQL_Tamplate<I> where I : ContainerItem<I>, new()
    {
        public static readonly string IS_DELETED = "is_deleted";
        public static readonly string IS_ACTIVE = "is_active";
        public static readonly string WhiteLabelTableName = "WhiteLabel";
        public static readonly string WhiteLabelDocId = "white_label_doc_id";
        public static readonly string WhiteLabelDocIdPrm = "@prm_white_label_doc_id";
        public static readonly string WhiteLabelDocIdPrmDecleration = "@prm_white_label_doc_id int";

        //return update table procedure
        public static string SQL_GetUpadteTable(IEnumerable<KeyValues<I>> keys, string ClassName)
        {
            var PrimeryKey = keys.FirstOrDefault(K => K.IsSickID);
            //If There is no Primery Key Can't Ganerte Procedure
            if (PrimeryKey == null)
            {
                return "";
            }
            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);

            string res = string.Format("Create PROCEDURE [dbo].[update_{0}]\r\n", ClassName);
            int count = 1;

            bool isTranslateAble = false;
            bool isWithWhiteLabel = false;

            //Insert Prm's (includ ID)
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                string DBLine = string.Empty;

                if (count == keys.Count())
                {
                    DBLine = string.Format(" {0}_{1} {2} \r\n", "@prm", Values.Key, Values.Value);
                }
                else
                {
                    DBLine = string.Format(" {0}_{1} {2} ,\r\n", "@prm", Values.Key, Values.Value);
                }

                count++;
                res += DBLine;

            }

            //Insert Prm's (includ ID)
            #region Translator
            string translate = "";
            List<string> declare = new List<string>();
            List<string> select = new List<string>();
            List<string> update = new List<string>();
            List<string> declareAndSelect = new List<string>();

            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                if (key.IsMirrorable)
                {
                    isTranslateAble = true;
                    declare.Add(string.Format(@"@prm_mir_{0} int ", Values.Key));//, Values.Value));
                    declare.Add(string.Format(@"@prm_found_{0} int ", Values.Key));//, Values.Value));

                    declareAndSelect.Add(string.Format(
@"
select @prm_found_{0} = [id] from [dbo].[Translator] where [id] = @prm_mir_{0}  and [lang_id] = @prm_lang

if @prm_found_{0} IS NULL
BEGIN
	insert into dbo.Translator ([id],[lang_id],[value]) values (@prm_mir_{0},@prm_lang,@prm_{0})
END
else
BEGIN 
	update dbo.Translator set [value] = @prm_{0} where [id]=@prm_mir_{0} and [lang_id] = @prm_lang
END

", Values.Key));//, Values.Value));

                    select.Add(String.Format("@prm_mir_{0} = [{0}]", Values.Key));//, Values.Value));
                    //update.Add(String.Format("update dbo.Translator set [value] = @prm_{0} where [id]=@prm_mir_{0} and [lang_id] = @prm_lang", Values.Key, Values.Value));
                }
            }
            if (isTranslateAble)
                res += string.Format(" ,@prm_lang int \r\n");

            if (declare.Count > 0)
            {
                //select @prm_mir_name = [name],@prm_mir_description = [description] from [dbo].[Company] where [company_doc_id]=@prm_company_doc_id

                translate = String.Format(
@" 
declare {3} 
select {2} from [dbo].[{0}] where [{1}]=@prm_{1}
{5}
{4}
", ClassName
 , PrimeryKeyValue.Key
 , String.Join(("," + Environment.NewLine), select.ToArray())
 , String.Join(("," + Environment.NewLine), declare.ToArray())
 , String.Join(Environment.NewLine, update.ToArray())
 , String.Join(Environment.NewLine, declareAndSelect.ToArray())

 );

            }
            #endregion
            string BeginUpdate = string.Format("\r\n AS BEGIN\r\n {3} \r\nUPDATE  \r\n [{1}].[{2}]\r\n SET\r\n", "", "dbo", ClassName, translate);
            res += BeginUpdate;

            count = 1;
            foreach (var key in keys)
            {

                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                string DBLine = string.Empty;
                if (!key.IsSickID)
                {
                    string mirror = key.IsMirrorable ? "mir_" : "";
                    if (count == keys.Count())
                    {
                        DBLine = string.Format(" [{0}]={1}{2} \r\n", Values.Key, "@prm_" + mirror, Values.Key);
                    }
                    else
                    {
                        DBLine = string.Format(" [{0}]={1}{2}, \r\n", Values.Key, "@prm_" + mirror, Values.Key);
                    }
                }
                if (Values.Key.Equals(WhiteLabelDocId) && !ClassName.Equals(WhiteLabelTableName))
                {
                    isWithWhiteLabel = true;
                }
                count++;
                res += DBLine;

            }
            if (isWithWhiteLabel)
            {
                //res = string.Format("{0}\r\n{1} \r\n", WhiteLabelDocIdPrmDecleration, res);
            }

            //Insert Update Statment
            string whereStatmant = string.Format("\r\nWHERE [{0}]={1}_{2}", PrimeryKeyValue.Key, "@prm", PrimeryKeyValue.Key);
            res += whereStatmant;
            res += "\r\n\r\nEND \r\n\r\nGO";
            return res + "\r\n";
        }
        //return Insert into table procedure
        public static string SQL_GetInsertIntoTable(IEnumerable<KeyValues<I>> keys, string ClassName)
        {
            //Insert
            string res = string.Format("Create PROCEDURE [dbo].[insert_{0}]\r\n", ClassName);
            //Insert Prm's
            int count = 1;
            bool isWithWhiteLabel = false;
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                string DBLine = string.Empty;
                if (!key.IsSickID)
                {
                    DBLine = string.Format(" {0}_{1} {2} ,\r\n", "@prm", Values.Key, Values.Value);
                    res += DBLine;
                }
                if (Values.Key.Equals(WhiteLabelDocId) && !ClassName.Equals(WhiteLabelTableName))
                {
                    isWithWhiteLabel = true;
                }
            }
            if (isWithWhiteLabel)
            {
                //res = string.Format("{0}\r\n{1} \r\n", WhiteLabelDocIdPrmDecleration, res);
            }
            //Insert Statment
            #region Translator
            string translate = "";
            List<string> declare = new List<string>();
            List<string> selectAndInsert = new List<string>();
            bool isTranslateAble = false;
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                if (key.IsMirrorable)
                {
                    isTranslateAble = true;
                    declare.Add(string.Format("@prm_mir_{0} {1}", Values.Key, Values.Value));
                    selectAndInsert.Add(String.Format(
@"
      select @prm_mir_{0} = (max(id)+1) from dbo.Translator
      insert into dbo.Translator ([id],[lang_id],[value]) values (@prm_mir_{0},@prm_lang,@prm_{0})
", Values.Key, Values.Value));
                }
            }
            if (declare.Count > 0)
            {
                translate = String.Format(@"{0} declare {1} {0} {2}", Environment.NewLine, String.Join(("," + Environment.NewLine), declare.ToArray()), String.Join(Environment.NewLine, selectAndInsert.ToArray()));

            }
            #endregion

            //Set Prm Out To Get DockID
            res += " @prm_out_docid bigint OUTPUT";
            if (isTranslateAble)
                res += string.Format(" ,@prm_lang int \r\n");

            string BeginInsert = string.Format("\r\n AS BEGIN\r\n {3} \r\nINSERT INTO [{1}].[{2}](\r\n", "", "dbo", ClassName, translate);
            res += BeginInsert;

            count = 1;
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                string DBLine = string.Empty;
                if (!key.IsSickID)
                {
                    if (count == keys.Count())
                    {
                        DBLine = string.Format(" [{0}]\r\n", Values.Key);
                    }
                    else
                    {
                        DBLine = string.Format(" [{0}],\r\n", Values.Key);
                    }
                }
                res += DBLine;
                count++;
            }
            res += ")\r\n  VALUES\r\n(";
            count = 1;
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                string DBLine = string.Empty;
                if (!key.IsSickID)
                {
                    string mirror = key.IsMirrorable ? "mir_" : "";
                    if (count == keys.Count())
                    {
                        DBLine = string.Format("{0}{1}\r\n", "@prm_" + mirror, Values.Key);
                    }
                    else
                    {
                        DBLine = string.Format("{0}{1},\r\n", "@prm_" + mirror, Values.Key);
                    }

                }
                res += DBLine;
                count++;
            }
            res += ")\r\nset @prm_out_docid = @@identity\r\nEND\r\nGO";
            return res + "\r\n";
        }

        //return create table procedure
        public static string SQL_GetCreateTable(IEnumerable<KeyValues<I>> keys, string ClassName)
        {
            return
                String.Format(
@"/****** 
Object:  Table [dbo].[{0}]    
Script Date: {1} 
******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[{0}](
{2}
) ON [PRIMARY]
"
                , ClassName, DateTime.Now, SQL_CreateTableParms(keys, ClassName));
        }

        //return Expunge table procedure
        public static string SQL_GetExpungeTable(IEnumerable<KeyValues<I>> keys, string ClassName)
        {
            var PrimeryKey = keys.FirstOrDefault(K => K.IsSickID);
            //If There is no Primery Key Can't Ganerte Procedure
            if (PrimeryKey == null)
            {
                return "";
            }
            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);
            string res = string.Format("Create PROCEDURE [dbo].[Expunge_{0}]\r\n", ClassName);
            string DBLine;
            DBLine = string.Format(" {0}_{1} {2} \r\n", "@prm", PrimeryKeyValue.Key, PrimeryKeyValue.Value);
            res += DBLine;
            string BeginInsert = string.Format("\r\n AS BEGIN\r\n Delete FROM [{1}].[{2}]\r\n ", "", "dbo", ClassName);
            res += BeginInsert;
            string whereStatmant = string.Format("WHERE [{0}]={1}_{2}", PrimeryKeyValue.Key, "@prm", PrimeryKeyValue.Key);
            res += whereStatmant;
            res += "\r\n\r\nEND \r\n\r\nGO";
            return res + "\r\n";
        }
        //return select all from table procedure
        public static string SQL_GetSelectAll(IEnumerable<KeyValues<I>> keys, string ClassName)
        {
            string resDeclaretion = string.Format("Create PROCEDURE [dbo].[Select_All_{0}]\r\n ", ClassName);
            string resParams = string.Format(" AS\r\n BEGIN \r\n\r\n");
            string resLines = "";
            string res = "";

            //string DBLineStart = "SELECT \r\n\r\n";
            string DBLine = "";
            //Insert Prm's
            int count = 1;
            bool isWithWhiteLabel = false;
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                if (count == keys.Count())
                {
                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] \r\n", ClassName, Values.Key, ClassName, Values.Key);
                }
                else
                {
                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] ,\r\n", ClassName, Values.Key, ClassName, Values.Key);
                }
                if (Values.Key.Equals(WhiteLabelDocId) && !ClassName.Equals(WhiteLabelTableName))
                {
                    isWithWhiteLabel = true;
                }
                count++;
                resLines += DBLine;
            }
            DBLine = string.Format(" FROM {0}.[{1}] {2} \r\n where {1}.[{3}] = 0 ", "dbo", ClassName, ClassName, "is_deleted");
            if (isWithWhiteLabel)
            {
                DBLine = string.Format("{0} AND ({1}.[{2}] = {3} OR {3} IS NULL)", DBLine, ClassName, WhiteLabelDocId, WhiteLabelDocIdPrm);
                resParams = string.Format("{0}\r\n{1} \r\n", WhiteLabelDocIdPrmDecleration, resParams);
            }
            resLines += DBLine;
            res = string.Format("{0} {1} SELECT {2} \r\n\r\nEND \r\n\r\nGO", resDeclaretion, resParams, resLines);

            return res + "\r\n";
        }
        //return select by id from table procedure
        public static string SQL_GetSelectByID(IEnumerable<KeyValues<I>> keys, string ClassName)
        {
            var PrimeryKey = keys.FirstOrDefault(K => K.IsSickID);
            //If There is no Primery Key Can't Ganerte Procedure
            if (PrimeryKey == null)
            {
                return "";
            }
            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);
            string res = string.Format("Create PROCEDURE [dbo].[Select_{0}_By_Id]\r\n", ClassName);
            string DBLine = string.Empty;
            DBLine = string.Format(" {0}_{1} {2} \r\n", "@prm", PrimeryKeyValue.Key, PrimeryKeyValue.Value);
            res += DBLine;
            res += " AS\r\n BEGIN \r\n\r\n \r\n SELECT \r\n\r\n";
            //Insert Prm's
            int count = 1;
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                if (count == keys.Count())
                {
                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] \r\n", ClassName, Values.Key, ClassName, Values.Key);
                }
                else
                {
                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] ,\r\n", ClassName, Values.Key, ClassName, Values.Key);
                }
                count++;
                res += DBLine;
            }
            DBLine = string.Format(" FROM {0}.[{1}] {2} \r\n", "dbo", ClassName, ClassName);
            res += DBLine;
            DBLine = string.Format(" WHERE  {0}.[{1}]={2}_{3}  AND {0}.[{4}] = 0\r\n", ClassName, PrimeryKeyValue.Key, "@prm", PrimeryKeyValue.Key, "is_deleted");
            res += DBLine;
            res += "\r\n\r\nEND \r\n\r\nGO";
            return res + "\r\n";
        }
        //return select procedure
        public static string SQL_GetSelectByCombination(I obj, IEnumerable<KeyValues<I>> keysToFilterBy, string ClassName, bool withIsDeleteFilter = true, bool withIsActiveFilter = false)
        {
            IEnumerable<KeyValues<I>> keys = obj.KeyValuesContainer.DataSource;
            return String.Format(
@"
GO
SET QUOTED_IDENTIFIER ON
GO
--Indexs :{4}
--Names  :{2}
Create PROCEDURE [dbo].[select_{0}{1}{4}]
{3}
AS
BEGIN 
SELECT 
{5}
FROM dbo.[{0}] {0} 
WHERE 
{6}
END
"
    , ClassName
    , "_by_keys_view_"
    , PROC_SelectNames(keysToFilterBy, ClassName)
    , PROC_SelectDecleration(keysToFilterBy, ClassName)
    , PROC_SelectIndexs(keys, keysToFilterBy, ClassName)
    , SQL_SelectNames(keys, ClassName)
    , SQL_SelectWhere(keysToFilterBy, ClassName, withIsDeleteFilter, withIsActiveFilter)
);
        }

        //return delete procedure
        public static string SQL_GetSelectByCombination_Delete(I obj, List<KeyValues<I>> keysToFilterBy, string ClassName)
        {
            IEnumerable<KeyValues<I>> keys = obj.KeyValuesContainer.DataSource;
            return String.Format(
    @"
GO
--Indexs :{4}
--Names  :{2}
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[select_{0}{1}{4}]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[select_{0}{1}{4}]"
    , ClassName
    , "_by_keys_view_"
    , PROC_SelectNames(keysToFilterBy, ClassName)
    , PROC_SelectDecleration(keysToFilterBy, ClassName)
    , PROC_SelectIndexs(keys, keysToFilterBy, ClassName)
    , SQL_SelectNames(keys, ClassName)
    , SQL_SelectWhere(keysToFilterBy, ClassName)
    );
        }

        //return update table procedure
        public static string SQL_GetSelectAll_Delete(string ClassName)
        {

            string res = string.Format(@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Select_All_{0}]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Select_All_{0}]", ClassName);

            return res + "\r\n";
        }
        //return update table procedure
        public static string SQL_GetUpadteTable_Delete(string ClassName)
        {
            string res = string.Format(@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[update_{0}]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[update_{0}]", ClassName);

            return res + "\r\n";
        }
        //return Insert into table procedure
        public static string SQL_GetInsertIntoTable_Delete(string ClassName)
        {
            string res = string.Format(@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[insert_{0}]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[insert_{0}]", ClassName);

            return res + "\r\n";
        }
        //return Insert into table procedure
        public static string SQL_GetSelectByID_Delete(string ClassName)
        {
            //Insert
            string res = string.Format(@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Select_{0}_By_Id]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Select_{0}_By_Id]", ClassName);
            return res + "\r\n";
        }
        //return create table procedure
        public static string SQL_GetCreateTable_Delete(string ClassName)
        {

            string res = string.Format(
 @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'U'))
DROP Table [dbo].[{0}]", ClassName);
            return res + "\r\n";
        }
        //return create table procedure
        public static string SQL_GetExpunge_Delete(string ClassName)
        {

            string res = string.Format(@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Expunge_{0}]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Expunge_{0}]", ClassName);
            return res + "\r\n";
        }


        #region Private methodes
        private static string StatmentKeyName(string cellName, string celltype, string tableName)
        {
            return String.Format(" {0}.[{1}] as [{0}.{1}]", tableName, cellName);
        }
        private static string StatmentDecleration(string cellName, string celltype, string tableName)
        {
            return String.Format("@prm_{0} {1}", cellName, celltype);
        }
        private static string StatmentWhere(string cellName, string celltype, string tableName)
        {
            return String.Format("{1}.[{0}] = @prm_{0} ", cellName, tableName);
        }
        private static string SQL_CreateTableParms(IEnumerable<KeyValues<I>> keys, string tableName)
        {
            string res = "";
            foreach (var key in keys)
            {
                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
                string Nullable = key.IsNullable == false ? "NOT NULL" : "";
                string SickID = key.IsSickID == true ? "IDENTITY(1,1)" : "";
                string DBLine;
                //Incase of Spicel Types liske nvrachar(50) 
                if (Values.Value.IndexOf('(') > 0)
                {
                    string Type = Values.Value.Substring(0, Values.Value.IndexOf('('));
                    string SpicelType = Values.Value.Substring(Values.Value.IndexOf('('));
                    if (key.IsMirrorable)
                    {
                        Type = " int ";
                        SpicelType = "";
                    }
                    DBLine = string.Format("       [{0}] {1} {2} {3} {4},\r\n", Values.Key, Type, SpicelType, SickID, Nullable);
                }
                else
                {
                    DBLine = string.Format("       [{0}] {1} {2} {3},\r\n", Values.Key, Values.Value, SickID, Nullable);
                }

                res += DBLine;

            }
            //Fixing the last ',' after finshing the loop
            res = res.Substring(0, res.Length - 5);
            return res;
        }
        private static string PROC_SelectDecleration(IEnumerable<KeyValues<I>> keys, string tableName)
        {
            List<string> cellsToSelectby = new List<string>();
            foreach (KeyValues<I> keyValue in keys)
            {
                //get key name and type
                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(keyValue.KeyValuesType);
                string cellName = key.Key;
                string cellType = key.Value;
                cellsToSelectby.Add(StatmentDecleration(cellName, cellType, tableName));
            }
            return string.Join("," + Environment.NewLine, cellsToSelectby.ToArray());
        }
        private static string SQL_SelectNames(IEnumerable<KeyValues<I>> keys, string tableName)
        {
            List<string> cellsToSelectby = new List<string>();
            foreach (KeyValues<I> keyValue in keys)
            {
                //get key name and type
                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(keyValue.KeyValuesType);
                string cellName = key.Key;
                string cellType = key.Value;
                cellsToSelectby.Add(StatmentKeyName(cellName, cellType, tableName));
            }
            return string.Join("," + Environment.NewLine, cellsToSelectby.ToArray());
        }
        private static string SQL_SelectWhere(IEnumerable<KeyValues<I>> keysToFilterBy, string tableName, bool withIsDeleteFilter = true, bool withIsActiveFilter = false)
        {
            bool withIsDeleted = withIsDeleteFilter;
            bool withIsActive = withIsActiveFilter;
            bool withIsWhiteLable = true;

            List<string> cellsToSelectby = new List<string>();
            foreach (KeyValues<I> keyValue in keysToFilterBy)
            {
                //get key name and type
                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(keyValue.KeyValuesType);
                string cellName = key.Key;
                string cellType = key.Value;
                string strWhere = StatmentWhere(cellName, cellType, tableName);
                
                withIsDeleted = withIsDeleted && !string.Equals(cellName.ToLower(), IS_DELETED.ToLower());
                withIsActive = withIsActive && !string.Equals(cellName.ToLower(), IS_ACTIVE.ToLower());
                //withIsWhiteLable = withIsWhiteLable && !string.Equals(cellName.ToLower(), WhiteLabelDocId.ToLower());
                if (withIsWhiteLable && string.Equals(cellName.ToLower(), WhiteLabelDocId.ToLower()))
                {
                    strWhere = string.Format("({0} OR @prm_white_label_doc_id IS NULL)", strWhere);
                }
                cellsToSelectby.Add(strWhere);
            }
            if (withIsDeleted)
            {
                cellsToSelectby.Add(String.Format("{0}.[{1}] = 0", tableName, IS_DELETED));
            }
            if (withIsActive)
            {
                cellsToSelectby.Add(String.Format("{0}.[{1}] = 1", tableName, IS_ACTIVE));
            }

            return string.Join(" AND " + Environment.NewLine, cellsToSelectby.ToArray());
        }
        private static string PROC_SelectIndexs(IEnumerable<KeyValues<I>> keys, IEnumerable<KeyValues<I>> keysToFilter, string tableName)
        {
            List<string> cellsToSelectby = new List<string>();
            for (int i = 0; i < keysToFilter.Count(); i++)
            {
                KeyValues<I> keyValue = keysToFilter.ElementAt(i);
                //get key name and type
                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(keyValue.KeyValuesType);
                string cellName = key.Key;
                string cellType = key.Value;
                cellsToSelectby.Add(GetIndex(keyValue, keys).ToString());
            }

            return string.Join("_", cellsToSelectby.ToArray());
        }
        private static string PROC_SelectNames(IEnumerable<KeyValues<I>> keys, string tableName)
        {
            List<string> cellsToSelectby = new List<string>();
            for (int i = 0; i < keys.Count(); i++)
            {
                KeyValues<I> keyValue = keys.ElementAt(i);
                //get key name and type
                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(keyValue.KeyValuesType);
                string cellName = key.Key;
                string cellType = key.Value;
                cellsToSelectby.Add(cellName);
            }

            return string.Join("_", cellsToSelectby.ToArray());
        }
        #endregion
        //need to separate this function from this object - duplicate at CODE_Tamplate
        public static int GetIndex(KeyValues<I> keyToSearch, IEnumerable<KeyValues<I>> keys)
        {
            for (int i = 0; i < keys.Count(); i++)
            {
                if (keyToSearch.KeyValuesType == keys.ElementAt(i).KeyValuesType)
                {
                    return i;
                }
            }

            throw new Exception("cant find the filter key (" + keyToSearch.KeyValuesType.ToString() + ") in the table keys");
        }
    }

    public static class CODE_Tamplate<I> where I : ContainerItem<I>, new()
    {
        private static string getCellName(KeyValues<I> oKeyValues)
        {
            string str = "";
            foreach (string item in DBKeyValurPair.GetDBKeyValueByDBKey(oKeyValues.KeyValuesType).Key.Split('_'))
            {
                str += item.ToCapitalFirst();
            }
            return str;
        }
        private static string getDBTypeName(KeyValues<I> oKeyValues)
        {
            return DBKeyValurPair.GetDBKeyValueByDBKey(oKeyValues.KeyValuesType).Value;
        }
        public static string GetLayersCodeForSelectByCombination(I obj, IEnumerable<KeyValues<I>> keysToFilterBy, string className, out string str_dl, out string str_bl, out string str_tbNames, out string str_sql)
        {
            IEnumerable<KeyValues<I>> keys = obj.KeyValuesContainer.DataSource;

            List<string> cellsToSelectby_title_comment = new List<string>();

            List<string> cellsToSelectby_title_index = new List<string>();
            List<string> cellsToSelectby_title_keys = new List<string>();

            List<string> cellsToSelectby_decleration_DL = new List<string>();
            List<string> cellsToSelectbyParams_DL = new List<string>();

            List<string> cellsToSelectby_decleration_BL = new List<string>();
            List<string> cellsToSelectbyParams_BL = new List<string>();

            List<string> cellsToSelectbyParams_PROC = new List<string>();
            //string procTitle = string.Join("", Combinations.ConvertToBaseString(combination));
            foreach (KeyValues<I> key in keysToFilterBy)
            {

                int columnIndex = SQL_Tamplate<I>.GetIndex(key, keys);
                string cellName = getCellName(keys.ElementAt(columnIndex));
                string DBType = getDBTypeName(keys.ElementAt(columnIndex));
                cellsToSelectby_title_comment.Add(columnIndex.ToString());
                cellsToSelectby_title_index.Add(columnIndex.ToString());
                cellsToSelectby_title_keys.Add(cellName.ToCapitalFirst());

                cellsToSelectby_decleration_DL.Add(get_Object_Set(className, LAYER.DL, cellName));
                cellsToSelectby_decleration_BL.Add(get_Object_Set(className, LAYER.BL, cellName));

                cellsToSelectbyParams_DL.Add(get_Object_Params(className, LAYER.DL, cellName, DBType));
                cellsToSelectbyParams_BL.Add(get_Object_Params(className, LAYER.BL, cellName, DBType));
            }

            str_dl = (get_Object_Function(className, LAYER.DL, cellsToSelectby_title_comment, cellsToSelectby_title_index, cellsToSelectby_title_keys, cellsToSelectby_decleration_DL, cellsToSelectbyParams_DL).ToString());
            str_bl = (get_Object_Function(className, LAYER.BL, cellsToSelectby_title_comment, cellsToSelectby_title_index, cellsToSelectby_title_keys, cellsToSelectby_decleration_BL, cellsToSelectbyParams_BL).ToString());
            str_tbNames = (get_Object_ProcName(className, cellsToSelectby_title_index, cellsToSelectby_title_keys, cellsToSelectby_title_comment).ToString());
            str_sql = SQL_Tamplate<I>.SQL_GetSelectByCombination(obj, keysToFilterBy, className);
            return str_dl + str_bl + str_tbNames;
        }


        private enum LAYER
        {
            DB,
            DL,
            BL,
            PL
        }

        private static string get_Object_Set(string tableName, LAYER layer, string cellName)
        {
            switch (layer)
            {
                case LAYER.DL:
                    return String.Format(@"{0}db.AddParameter(TBNames_{1}.PRM_{2}, ConvertTo.ConvertEmptyToDBNull(o{1}.{2}));",
                        Environment.NewLine,
                        tableName,
                        cellName.ToCapitalFirst());
                case LAYER.BL:
                    return String.Format(@"{0} o{2}.{1} = _{1};",
                        Environment.NewLine,
                        cellName.ToCapitalFirst(),
                        tableName);
                case LAYER.PL:
                    break;
                default:
                    break;
            }
            return "";
        }

        private static string get_Object_Params(string tableName, LAYER layer, string cellName, string cellDBType)
        {
            switch (layer)
            {
                case LAYER.DL:
                    return "";
                case LAYER.BL:
                    return String.Format(@"{0}{2} _{1}",
                        Environment.NewLine,
                        cellName.ToCapitalFirst(),
                        DBTypes.GetCodeType(cellDBType));
                case LAYER.PL:
                    break;
                default:
                    break;
            }
            return "";
        }

        private static StringBuilder get_Object_Function(string tableName, LAYER layer, List<string> cellsToSelectby_title_comment, List<string> cellsToSelectby_title_index, List<string> cellsToSelectby_title_keys, List<string> cellsToSelectby_decleration, List<string> cellsToSelectbyParams)
        {
            switch (layer)
            {
                case LAYER.DL:
                    return new StringBuilder(String.Format(
     @"
        //{6}
        internal static DataSet SelectByKeysView_{4}({3} o{3})
        {1}
            DataSet ds = null;
            try
            {1}
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//
{5}
                //-------------------------------------------------------//
                #endregion
                ds = db.ExecuteDataSet(TBNames_{3}.PROC_Select_{3}_By_Keys_View_{4});   
            {2}
            catch (Exception ex)
            {1}
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.{3}, {0}Select_{3}_By_Keys_View_{4}{0}, ex.Message));

            {2}
            return ds;
        {2}

",
                  '"',
                  '{',
                  '}',
                 tableName,
                 string.Join("_", cellsToSelectby_title_keys.ToArray()),
                 string.Join(" ", cellsToSelectby_decleration.ToArray()),
                 string.Join("_", cellsToSelectby_title_comment.ToArray())
                 ));
                case LAYER.BL:
                    return new StringBuilder(String.Format(
@"
        //{8}
        //{7}
        public static {3}Container SelectByKeysView_{4}({6})
        {1}
            {3}Container o{3}Container = new {3}Container();
            {3} o{3} = new {3}();
            #region Params
            {5}
            #endregion 
            o{3}Container.Add(Convert({3}.SelectByKeysView_{4}(o{3})));
            return o{3}Container;
        {2}

",
         '"',
         '{',
         '}',
            tableName,
            string.Join("_", cellsToSelectby_title_keys.ToArray()),
            string.Join(" ", cellsToSelectby_decleration.ToArray()),
            string.Join(",", cellsToSelectbyParams.ToArray()),
            string.Join("_", cellsToSelectby_title_index.ToArray()),
            string.Join("_", cellsToSelectby_title_comment.ToArray())
            ));
                case LAYER.PL:
                    break;
                default:
                    break;
            }
            return null;
        }

        private static StringBuilder get_Object_ProcName(string tableName, List<string> cellsToSelectby_title_index, List<string> cellsToSelectby_title_keys, List<string> cellsToSelectby_title_comment)
        {
            return new StringBuilder(String.Format(@"
#region PROC_Select_{2}_By_Keys_View_{3}
//{5}
//{4}
//{3}
public static readonly string  PROC_Select_{2}_By_Keys_View_{3} = {1}select_{2}_by_keys_view_{5}{1};
#endregion
",
                        Environment.NewLine,
                        '"',
                        tableName,
                        string.Join("_", cellsToSelectby_title_keys.ToArray()),
                        string.Join("_", cellsToSelectby_title_index.ToArray()),
                        string.Join("_", cellsToSelectby_title_comment.ToArray())
                        ));
        }

        private static StringBuilder get_Object_ProcName_forSearch(string tableName, List<string> cellsToSelectby_title_index, List<string> cellsToSelectby_title_keys, List<string> cellsToSelectby_title_comment)
        {
            return new StringBuilder(String.Format(@"{4}",
                        Environment.NewLine,
                        '"',
                        tableName,
                        string.Join("_", cellsToSelectby_title_keys.ToArray()),
                        string.Join("_", cellsToSelectby_title_index.ToArray()),
                        string.Join("_", cellsToSelectby_title_comment.ToArray())
                        ));
        }

    }
}


//        #region GenerateDB
//        public string GenerateDBTabls(string ClassName)
//        {
//            string res = "/****** Object:  Table [dbo].[" + ClassName + "]    Script Date: " + DateTime.Now + "******/\r\nSET ANSI_NULLS ON\r\nGO\r\nSET QUOTED_IDENTIFIER ON\r\nGO\r\nCREATE TABLE [dbo].[" + ClassName + "](\r\n";
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                string Nullable = GuideValues.IsNullable == false ? "NOT NULL" : "";
//                string SickID = GuideValues.IsSickID == true ? "IDENTITY(1,1)" : "";
//                string DBLine;
//                //Incase of Spicel Types liske nvrachar(50) 
//                if (Values.Value.IndexOf('(') > 0)
//                {
//                    string Type = Values.Value.Substring(0, Values.Value.IndexOf('('));
//                    string SpicelType = Values.Value.Substring(Values.Value.IndexOf('('));
//                    if (GuideValues.IsMirrorable)
//                    {
//                        Type = " int ";
//                        SpicelType = "";
//                    }
//                    DBLine = string.Format("       [{0}] {1} {2} {3} {4},\r\n", Values.Key, Type, SpicelType, SickID, Nullable);
//                }
//                else
//                {
//                    DBLine = string.Format("       [{0}] {1} {2} {3},\r\n", Values.Key, Values.Value, SickID, Nullable);
//                }

//                res += DBLine;

//            }
//            //Fixing the last ',' after finshing the loop
//            res = res.Substring(0, res.Length - 5);
//            res += "\r\n) ON [PRIMARY]";
//            return res += "\r\n";
//        }

//        public List<string> GenerateBasicProcs(string ClassName) {
//            List<string> procs = new List<string>();
//            procs.Add( GenerateDBTabls(ClassName));
//            procs.Add(GenerateInsertProcedure(ClassName));
//            procs.Add(GenerateUpdateProcedure(ClassName));
//            procs.Add(GenerateExpungeProcedure(ClassName));
//            procs.Add(GenerateSelectAllProcedure(ClassName));
//            procs.Add(GenerateSelectByIDProcedure(ClassName));
//            return procs;

//        }
//        #region Basic Procs
//        public string GenerateInsertProcedure(string ClassName)
//        {
//            //Insert
//            string res = string.Format("Create PROCEDURE [dbo].[insert_{0}]\r\n", ClassName);
//            //Insert Prm's
//            int count = 1;
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                string DBLine = string.Empty;
//                if (!GuideValues.IsSickID)
//                {
//                    DBLine = string.Format(" {0}_{1} {2} ,\r\n", "@prm", Values.Key, Values.Value);
//                    res += DBLine;
//                }
//            }
//            //Set Prm Out To Get DockID
//            res += " @prm_out_docid bigint OUTPUT";
//            //Insert Statment
//            #region Translator
//            string translate = "";
//            List<string> declare = new List<string>();
//            List<string> selectAndInsert = new List<string>();
//            foreach (var key in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
//                if (key.IsMirrorable)
//                {
//                    declare.Add(string.Format("@prm_mir_{0} {1}", Values.Key, Values.Value));
//                    selectAndInsert.Add(String.Format(
//@"
//      select @prm_mir_{0} = (max(id)+1) from dbo.Translator
//      insert into dbo.Translator ([id],[lang_id],[value]) values (@prm_mir_{0},1,@prm_{0})
//", Values.Key, Values.Value));
//                }
//            }
//            if (declare.Count > 0)
//            {
//                translate = String.Format(@"{0} declare {1} {0} {2}", Environment.NewLine, String.Join(("," + Environment.NewLine), declare.ToArray()), String.Join(Environment.NewLine, selectAndInsert.ToArray()));

//            }
//            #endregion
//            string BeginInsert = string.Format("\r\n AS BEGIN\r\n {3} \r\nINSERT INTO [{1}].[{2}](\r\n", "", "dbo", ClassName, translate);
//            res += BeginInsert;

//            count = 1;
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                string DBLine = string.Empty;
//                if (!GuideValues.IsSickID)
//                {
//                    if (count == this.KeyValuesContainer.DataSource.Count)
//                    {
//                        DBLine = string.Format(" [{0}]\r\n", Values.Key);
//                    }
//                    else
//                    {
//                        DBLine = string.Format(" [{0}],\r\n", Values.Key);
//                    }
//                }
//                res += DBLine;
//                count++;
//            }
//            res += ")\r\n  VALUES\r\n(";
//            count = 1;
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                string DBLine = string.Empty;
//                if (!GuideValues.IsSickID)
//                {
//                    string mirror = GuideValues.IsMirrorable ? "mir_" : "";
//                    if (count == this.KeyValuesContainer.DataSource.Count)
//                    {
//                        DBLine = string.Format("{0}{1}\r\n", "@prm_" + mirror, Values.Key);
//                    }
//                    else
//                    {
//                        DBLine = string.Format("{0}{1},\r\n", "@prm_" + mirror, Values.Key);
//                    }

//                }
//                res += DBLine;
//                count++;
//            }
//            res += ")\r\nset @prm_out_docid = @@identity\r\nEND\r\nGO";
//            return res + "\r\n";
//        }
//        public string GenerateUpdateProcedure(string ClassName)
//        {
//            var PrimeryKey = this.KeyValuesContainer.DataSource.FirstOrDefault(K => K.IsSickID);
//            //If There is no Primery Key Can't Ganerte Procedure
//            if (PrimeryKey == null)
//            {
//                return "";
//            }
//            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);

//            string res = string.Format("Create PROCEDURE [dbo].[update_{0}]\r\n", ClassName);
//            int count = 1;
//            //Insert Prm's (includ ID)
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                string DBLine = string.Empty;

//                if (count == this.KeyValuesContainer.DataSource.Count)
//                {
//                    DBLine = string.Format(" {0}_{1} {2} \r\n", "@prm", Values.Key, Values.Value);
//                }
//                else
//                {
//                    DBLine = string.Format(" {0}_{1} {2} ,\r\n", "@prm", Values.Key, Values.Value);
//                }

//                count++;
//                res += DBLine;

//            }

//            //Insert Prm's (includ ID)
//            #region Translator
//            string translate = "";
//            List<string> declare = new List<string>();
//            List<string> select = new List<string>();
//            List<string> update = new List<string>();
//            foreach (var key in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(key.KeyValuesType);
//                if (key.IsMirrorable)
//                {
//                    declare.Add(string.Format("@prm_mir_{0} {1}", Values.Key, Values.Value));
//                    select.Add(String.Format("@prm_mir_{0} = [{0}]", Values.Key, Values.Value));
//                    update.Add(String.Format("update dbo.Translator set [value] = @prm_{0} where [id]=@prm_mir_{0} and [lang_id] = 1", Values.Key, Values.Value));
//                }
//            }
//            if (declare.Count > 0)
//            {
//                //select @prm_mir_name = [name],@prm_mir_description = [description] from [dbo].[Company] where [company_doc_id]=@prm_company_doc_id

//                translate = String.Format(
//@" 
//declare {3} 
//select {2} from [dbo].[{0}] where [{1}]=@prm_{1}
//{4}
//", ClassName, PrimeryKeyValue.Key, String.Join(("," + Environment.NewLine), select.ToArray()), String.Join(("," + Environment.NewLine), declare.ToArray()), String.Join(Environment.NewLine, update.ToArray()));

//            }
//            #endregion
//            string BeginUpdate = string.Format("\r\n AS BEGIN\r\n {3} \r\nUPDATE  \r\n [{1}].[{2}]\r\n SET\r\n", "", "dbo", ClassName, translate);
//            res += BeginUpdate;

//            count = 1;
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {

//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                string DBLine = string.Empty;
//                if (!GuideValues.IsSickID)
//                {
//                    string mirror = GuideValues.IsMirrorable ? "mir_" : "";
//                    if (count == this.KeyValuesContainer.DataSource.Count)
//                    {
//                        DBLine = string.Format(" [{0}]={1}{2} \r\n", Values.Key, "@prm_" + mirror, Values.Key);
//                    }
//                    else
//                    {
//                        DBLine = string.Format(" [{0}]={1}{2}, \r\n", Values.Key, "@prm_" + mirror, Values.Key);
//                    }
//                }
//                count++;
//                res += DBLine;

//            }
//            //Insert Update Statment
//            string whereStatmant = string.Format("\r\nWHERE [{0}]={1}_{2}", PrimeryKeyValue.Key, "@prm", PrimeryKeyValue.Key);
//            res += whereStatmant;
//            res += "\r\n\r\nEND \r\n\r\nGO";
//            return res + "\r\n";
//        }
//        //Expunge -- Delete Db
//        public string GenerateExpungeProcedure(string ClassName)
//        {
//            var PrimeryKey = this.KeyValuesContainer.DataSource.FirstOrDefault(K => K.IsSickID);
//            //If There is no Primery Key Can't Ganerte Procedure
//            if (PrimeryKey == null)
//            {
//                return "";
//            }
//            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);
//            string res = string.Format("Create PROCEDURE [dbo].[Expunge_{0}]\r\n", ClassName);
//            string DBLine;
//            DBLine = string.Format(" {0}_{1} {2} \r\n", "@prm", PrimeryKeyValue.Key, PrimeryKeyValue.Value);
//            res += DBLine;
//            string BeginInsert = string.Format("\r\n AS BEGIN\r\n Delete FROM [{1}].[{2}]\r\n ", "", "dbo", ClassName);
//            res += BeginInsert;
//            string whereStatmant = string.Format("WHERE [{0}]={1}_{2}", PrimeryKeyValue.Key, "@prm", PrimeryKeyValue.Key);
//            res += whereStatmant;
//            res += "\r\n\r\nEND \r\n\r\nGO";
//            return res + "\r\n";
//        }

//        public string GenerateSelectAllProcedure(string ClassName)
//        {
//            string res = string.Format("Create PROCEDURE [dbo].[Select_All_{0}]\r\n AS\r\n BEGIN \r\n\r\n \r\n SELECT \r\n\r\n", ClassName);
//            string DBLine = string.Empty;
//            //Insert Prm's
//            int count = 1;
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {

//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                if (count == this.KeyValuesContainer.DataSource.Count)
//                {
//                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] \r\n", ClassName, Values.Key, ClassName, Values.Key);
//                }
//                else
//                {
//                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] ,\r\n", ClassName, Values.Key, ClassName, Values.Key);
//                }

//                count++;
//                res += DBLine;

//            }
//            DBLine = string.Format(" FROM {0}.[{1}] {2} \r\n where {1}.[{3}] = 0 ", "dbo", ClassName, ClassName, "is_deleted");
//            res += DBLine;
//            res += "\r\n\r\nEND \r\n\r\nGO";
//            return res + "\r\n";
//        }

//        public string GenerateSelectByIDProcedure(string ClassName)
//        {
//            var PrimeryKey = this.KeyValuesContainer.DataSource.FirstOrDefault(K => K.IsSickID);
//            //If There is no Primery Key Can't Ganerte Procedure
//            if (PrimeryKey == null)
//            {
//                return "";
//            }
//            KeyValuePair<string, string> PrimeryKeyValue = DBKeyValurPair.GetDBKeyValueByDBKey(PrimeryKey.KeyValuesType);
//            string res = string.Format("Create PROCEDURE [dbo].[Select_{0}_By_Id]\r\n", ClassName);
//            string DBLine = string.Empty;
//            DBLine = string.Format(" {0}_{1} {2} \r\n", "@prm", PrimeryKeyValue.Key, PrimeryKeyValue.Value);
//            res += DBLine;
//            res += " AS\r\n BEGIN \r\n\r\n \r\n SELECT \r\n\r\n";
//            //Insert Prm's
//            int count = 1;
//            foreach (var GuideValues in this.KeyValuesContainer.DataSource)
//            {
//                KeyValuePair<string, string> Values = DBKeyValurPair.GetDBKeyValueByDBKey(GuideValues.KeyValuesType);
//                if (count == this.KeyValuesContainer.DataSource.Count)
//                {
//                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] \r\n", ClassName, Values.Key, ClassName, Values.Key);
//                }
//                else
//                {
//                    DBLine = string.Format(" {0}.[{1}] as [{2}.{3}] ,\r\n", ClassName, Values.Key, ClassName, Values.Key);
//                }
//                count++;
//                res += DBLine;
//            }
//            DBLine = string.Format(" FROM {0}.[{1}] {2} \r\n", "dbo", ClassName, ClassName);
//            res += DBLine;
//            DBLine = string.Format(" WHERE  {0}.[{1}]={2}_{3}  AND {0}.[{4}] = 0\r\n", ClassName, PrimeryKeyValue.Key, "@prm", PrimeryKeyValue.Key, "is_deleted");
//            res += DBLine;
//            res += "\r\n\r\nEND \r\n\r\nGO";
//            return res + "\r\n";
//        }
//        #endregion

//        string IS_DELETED = "is_deleted";

//        //Select By Pairs
//        public string GenerateSelectByViewParamsProcedure_Pairs(string ClassName)
//        {
//            int count = 0;
//            bool withIsDeleted = true;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable);
//            List<string> selectBy = new List<string>();
//            #region Select Names
//            List<string> list_selectNames = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//                string DBLine = String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, key.Key);
//                list_selectNames.Add(DBLine);
//            }
//            #endregion

//            #region Pairs
//            selectBy = new List<string>();
//            string where_isDeleted = (String.Format("{0}.[{1}] = 0", ClassName, IS_DELETED));

//            for (int i = 0; i < list.Count(); i++)
//            {
//                withIsDeleted = true;
//                List<string> cellsToSelectby_pairs = new List<string>();
//                List<string> cellsToSelectby_pairs_prm = new List<string>();
//                List<string> cellsToSelectby_pairs_where = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Pair

//                cellsToSelectby_pairs.Add(key.Key);
//                cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    //remove last pair
//                    if (cellsToSelectby_pairs.Count == 2)
//                    {
//                        cellsToSelectby_pairs.RemoveAt(1);
//                        cellsToSelectby_pairs_prm.RemoveAt(1);
//                        cellsToSelectby_pairs_where.RemoveAt(1);
//                    }
//                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                    //add new 
//                    cellsToSelectby_pairs.Add(key.Key);
//                    cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                    cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));

//                    #region Proc
//                    //flag to check if the deleted key is include in this query
//                    withIsDeleted = cellsToSelectby_pairs.Find(R => string.Equals(R.ToLower(), IS_DELETED.ToLower())) == null;
//                    count++;

//                    string proc = String.Format("");
//                    //string.Join("_", cellsToSelectby_pairs.ToArray())
//                    //procs.Add(proc);
//                    selectBy.Add(String.Format(
//@"
//GO
//SET QUOTED_IDENTIFIER ON
//GO
//
//Create PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//{2}
//AS
//BEGIN 
//SELECT 
//{3}
//FROM dbo.[{0}] {0} 
//WHERE 
//{4}
//{5}
//END
//"
//, ClassName,
//    string.Join("_", cellsToSelectby_pairs.ToArray()),
//    string.Join("," + Environment.NewLine, cellsToSelectby_pairs_prm.ToArray()),
//    string.Join("," + Environment.NewLine, list_selectNames.ToArray()),
//    string.Join(" AND " + Environment.NewLine, cellsToSelectby_pairs_where.ToArray()),
//    withIsDeleted ? (" AND " + where_isDeleted) : ""
//));

//                    #endregion
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        //Select By Single
//        public string GenerateSelectByViewParamsProcedure_Single(string ClassName)
//        {
//            int count = 0;
//            bool withIsDeleted = true;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();
//            string where_isDeleted = (String.Format("{0}.[{1}] = 0", ClassName, IS_DELETED));
//            #region Select Names
//            List<string> list_selectNames = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//                string DBLine = String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, key.Key);
//                list_selectNames.Add(DBLine);
//            }
//            #endregion

//            #region Single
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                List<string> cellsToSelectby_pairs_prm = new List<string>();
//                List<string> cellsToSelectby_pairs_where = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Single

//                cellsToSelectby_pairs.Add(key.Key);
//                cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                #region Proc
//                //flag to check if the deleted key is include in this query
//                withIsDeleted = cellsToSelectby_pairs.Find(R => string.Equals(R.ToLower(), IS_DELETED.ToLower())) == null;
//                count++;
//                string proc = String.Format("");
//                //string.Join("_", cellsToSelectby_pairs.ToArray())
//                //procs.Add(proc);
//                selectBy.Add(String.Format(
//@"
//GO
//SET QUOTED_IDENTIFIER ON
//GO
//
//Create PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//{2}
//AS
//BEGIN 
//SELECT 
//{3}
//FROM dbo.[{0}] {0} 
//WHERE 
//{4}
//{5}
//END
//"
//, ClassName,
//string.Join("_", cellsToSelectby_pairs.ToArray()),
//string.Join("," + Environment.NewLine, cellsToSelectby_pairs_prm.ToArray()),
//string.Join("," + Environment.NewLine, list_selectNames.ToArray()),
//string.Join(" AND " + Environment.NewLine, cellsToSelectby_pairs_where.ToArray()),
//    withIsDeleted ? (" AND " + where_isDeleted) : ""

//));

//                #endregion

//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        #region GenerateSelectByViewParamsProcedure_Combination - old
//        //        //Select By Combination
//        //        public string GenerateSelectByViewParamsProcedure_Combination(string ClassName)
//        //        {
//        //            int count = 0;

//        //            bool withIsDeleted = true;
//        //            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable); ;
//        //            List<string> selectBy = new List<string>();
//        //            string where_isDeleted = (String.Format("{0}.[{1}] = 0", ClassName, IS_DELETED));

//        //            #region Select Names
//        //            List<string> list_selectNames = new List<string>();
//        //            for (int i = 0; i < list.Count(); i++)
//        //            {
//        //                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//        //                string DBLine = String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, key.Key);
//        //                list_selectNames.Add(DBLine);
//        //            }
//        //            #endregion

//        //            #region Pairs
//        //            selectBy = new List<string>();
//        //            for (int i = 0; i < list.Count(); i++)
//        //            {
//        //                List<string> cellsToSelectby_pairs = new List<string>();
//        //                List<string> cellsToSelectby_indexs = new List<string>();
//        //                List<string> cellsToSelectby_pairs_prm = new List<string>();
//        //                List<string> cellsToSelectby_pairs_where = new List<string>();
//        //                //get key name and type
//        //                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//        //                #region Select by Pair
//        //                cellsToSelectby_indexs.Add(i.ToString());
//        //                cellsToSelectby_pairs.Add(key.Key);
//        //                cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//        //                cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//        //                for (int j = (i + 1); j < list.Count(); j++)
//        //                {
//        //                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//        //                    //add new 
//        //                    cellsToSelectby_pairs.Add(key.Key);
//        //                    cellsToSelectby_indexs.Add(j.ToString());
//        //                    if (cellsToSelectby_pairs.Count > 2)
//        //                    {
//        //                        cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//        //                        cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//        //                        //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//        //                        #region Proc
//        //                        //flag to check if the deleted key is include in this query
//        //                        withIsDeleted = cellsToSelectby_pairs.Find(R => string.Equals(R.ToLower(), IS_DELETED.ToLower())) == null;
//        //                        count++;
//        //                        string proc = String.Format("");
//        //                        //string.Join("_", cellsToSelectby_pairs.ToArray())
//        //                        //procs.Add(proc);
//        //                        selectBy.Add(String.Format(
//        //    @"
//        //GO
//        //SET QUOTED_IDENTIFIER ON
//        //GO
//        //
//        //Create PROCEDURE [dbo].[select_{0}_by_keys_view_{6}]
//        //{2}
//        //AS
//        //BEGIN 
//        //-- {1}
//        //SELECT 
//        //{3}
//        //FROM dbo.[{0}] {0} 
//        //WHERE 
//        //{4}
//        //{5}
//        //END
//        //"
//        //    , ClassName,
//        //        string.Join("_", cellsToSelectby_pairs.ToArray()),
//        //        string.Join("," + Environment.NewLine, cellsToSelectby_pairs_prm.ToArray()),
//        //        string.Join("," + Environment.NewLine, list_selectNames.ToArray()),
//        //        string.Join(" AND " + Environment.NewLine, cellsToSelectby_pairs_where.ToArray()),
//        //        withIsDeleted ? (" AND " + where_isDeleted) : "",
//        //        string.Join("_", cellsToSelectby_indexs.ToArray())

//        //    ));



//        //                        #endregion
//        //                    }
//        //                }
//        //                #endregion
//        //            }

//        //            #endregion

//        //            return string.Join("" + Environment.NewLine, selectBy);
//        //        } \
//        #endregion

//        private string SQL_GetSelectNames(string ClassName, string Key)
//        {
//            return String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, Key);
//        }

//        //Select By Combination
//        public string GenerateSelectByViewParamsProcedure_Combination(string ClassName)
//        {
//            int count = 0;

//            bool withIsDeleted = true;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();
//            string where_isDeleted = (String.Format("{0}.[{1}] = 0", ClassName, IS_DELETED));



//            List<List<int>> list_combinations = null;

//            #region Select Names
//            //יוצרים את כותרת השדות של השאילתא
//            List<string> list_selectNames = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//                string DBLine = SQL_GetSelectNames(ClassName, key.Key);//String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, key.Key);
//                list_selectNames.Add(DBLine);
//            }
//            //--------------------------------------
//            #endregion

//            #region Pairs
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                List<string> cellsToSelectby_indexs = new List<string>();
//                List<string> cellsToSelectby_pairs_prm = new List<string>();
//                List<string> cellsToSelectby_pairs_where = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Pair
//                cellsToSelectby_indexs.Add(i.ToString());
//                cellsToSelectby_pairs.Add(key.Key);
//                cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                    //add new 
//                    cellsToSelectby_pairs.Add(key.Key);
//                    cellsToSelectby_indexs.Add(j.ToString());
//                    if (cellsToSelectby_pairs.Count > 2)
//                    {
//                        cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                        cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                        //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                        #region Proc
//                        //flag to check if the deleted key is include in this query
//                        withIsDeleted = cellsToSelectby_pairs.Find(R => string.Equals(R.ToLower(), IS_DELETED.ToLower())) == null;
//                        count++;
//                        string proc = String.Format("");
//                        //string.Join("_", cellsToSelectby_pairs.ToArray())
//                        //procs.Add(proc);
//                        selectBy.Add(String.Format(
//    @"
//GO
//SET QUOTED_IDENTIFIER ON
//GO
//
//Create PROCEDURE [dbo].[select_{0}_by_keys_view_{6}]
//{2}
//AS
//BEGIN 
//-- {1}
//SELECT 
//{3}
//FROM dbo.[{0}] {0} 
//WHERE 
//{4}
//{5}
//END
//"
//    , ClassName,
//        string.Join("_", cellsToSelectby_pairs.ToArray()),
//        string.Join("," + Environment.NewLine, cellsToSelectby_pairs_prm.ToArray()),
//        string.Join("," + Environment.NewLine, list_selectNames.ToArray()),
//        string.Join(" AND " + Environment.NewLine, cellsToSelectby_pairs_where.ToArray()),
//        withIsDeleted ? (" AND " + where_isDeleted) : "",
//        string.Join("_", cellsToSelectby_indexs.ToArray())

//    ));



//                        #endregion
//                    }
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }


//        #region DELETE basic procs

//        public string GenerateDeleteBasicProcedure(string ClassName, string action)
//        {
//            //Insert
//            string res = string.Format(@"
//            GO
//            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{1}_{0}]') AND type in (N'P', N'PC'))
//            DROP PROCEDURE [dbo].[{1}_{0}]", ClassName, action);
//            return res;
//        }
//        public string GenerateDeleteSelectByIDProcedure(string ClassName)
//        {
//            //Insert
//            string res = string.Format(@"
//            GO
//            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Select_{0}_By_Id]') AND type in (N'P', N'PC'))
//            DROP PROCEDURE [dbo].[Select_{0}_By_Id]", ClassName);
//            return res;
//        }
//        public string GenerateDeleteDBTabls(string ClassName)
//        {
//            //Insert
//            string res = string.Format(@"
//            GO
//            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'U'))
//            DROP TABLE [dbo].[{0}]", ClassName);
//            return res;
//        }

//        #endregion

//        #region DELETE PROCS
//        //Select By Pairs
//        public string GenerateDeleteSelectByViewParamsProcedure_Pairs(string ClassName)
//        {
//            int count = 0;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable);
//            List<string> selectBy = new List<string>();

//            #region Pairs
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Pair

//                cellsToSelectby_pairs.Add(key.Key);
//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    //remove last pair
//                    if (cellsToSelectby_pairs.Count == 2)
//                    {
//                        cellsToSelectby_pairs.RemoveAt(1);
//                    }
//                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                    //add new 
//                    cellsToSelectby_pairs.Add(key.Key);

//                    #region Proc
//                    count++;
//                    string proc = String.Format("");
//                    //string.Join("_", cellsToSelectby_pairs.ToArray())
//                    //procs.Add(proc);
//                    selectBy.Add(String.Format(
//@"
//GO
//IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[select_{0}_by_keys_view_{1}]') AND type in (N'P', N'PC'))
//DROP PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]"
//, ClassName,
//    string.Join("_", cellsToSelectby_pairs.ToArray())
//));

//                    #endregion
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        //Select By Single
//        public string GenerateDeleteSelectByViewParamsProcedure_Single(string ClassName)
//        {
//            int count = 0;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();

//            #region Single
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Single

//                cellsToSelectby_pairs.Add(key.Key);
//                //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                #region Proc
//                count++;
//                string proc = String.Format("");
//                //string.Join("_", cellsToSelectby_pairs.ToArray())
//                //procs.Add(proc);
//                selectBy.Add(String.Format(
//@"
//GO
//IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[select_{0}_by_keys_view_{1}]') AND type in (N'P', N'PC'))
//DROP PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//"
//, ClassName,
//string.Join("_", cellsToSelectby_pairs.ToArray())
//));

//                #endregion

//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        //Select By Combination
//        public string GenerateDeleteSelectByViewParamsProcedure_Combination(string ClassName)
//        {
//            int count = 0;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();

//            #region Pairs
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_indexs = new List<string>();
//                List<string> cellsToSelectby_pairs = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//                #region Select by Pair

//                cellsToSelectby_pairs.Add(key.Key);
//                cellsToSelectby_indexs.Add(i.ToString());

//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                    //add new 
//                    cellsToSelectby_pairs.Add(key.Key);
//                    cellsToSelectby_indexs.Add(j.ToString());

//                    if (cellsToSelectby_pairs.Count > 2)
//                    {
//                        //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                        #region Proc
//                        count++;
//                        string proc = String.Format("");
//                        //string.Join("_", cellsToSelectby_pairs.ToArray())
//                        //procs.Add(proc);
//                        selectBy.Add(String.Format(
//    @"
//--{1}
//GO
//IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[select_{0}_by_keys_view_{2}]') AND type in (N'P', N'PC'))
//DROP PROCEDURE [dbo].[select_{0}_by_keys_view_{2}]
//"
//    , ClassName,
//        string.Join("_", cellsToSelectby_pairs.ToArray()),
//        string.Join("_", cellsToSelectby_indexs.ToArray())

//    ));



//                        #endregion
//                    }
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }

//        #endregion

//        string IS_DELETED = "is_deleted";
//        //Select By Pairs
//        public string GenerateSelectByViewParamsProcedure_Pairs(string ClassName)
//        {
//            int count = 0;
//            bool withIsDeleted = true;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource;//.FindAll(R => !R.IsMirrorable);
//            List<string> selectBy = new List<string>();
//            #region Select Names
//            List<string> list_selectNames = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//                string DBLine = String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, key.Key);
//                list_selectNames.Add(DBLine);
//            }
//            #endregion

//            #region Pairs
//            selectBy = new List<string>();
//            string where_isDeleted = (String.Format("{0}.[{1}] = 0", ClassName, IS_DELETED));

//            for (int i = 0; i < list.Count(); i++)
//            {
//                withIsDeleted = true;
//                List<string> cellsToSelectby_pairs = new List<string>();
//                List<string> cellsToSelectby_pairs_prm = new List<string>();
//                List<string> cellsToSelectby_pairs_where = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Pair

//                cellsToSelectby_pairs.Add(key.Key);
//                cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    //remove last pair
//                    if (cellsToSelectby_pairs.Count == 2)
//                    {
//                        cellsToSelectby_pairs.RemoveAt(1);
//                        cellsToSelectby_pairs_prm.RemoveAt(1);
//                        cellsToSelectby_pairs_where.RemoveAt(1);
//                    }
//                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                    //add new 
//                    cellsToSelectby_pairs.Add(key.Key);
//                    cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                    cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));

//                    #region Proc
//                    //flag to check if the deleted key is include in this query
//                    withIsDeleted = cellsToSelectby_pairs.Find(R => string.Equals(R.ToLower(), IS_DELETED.ToLower())) == null;
//                    count++;

//                    string proc = String.Format("");
//                    //string.Join("_", cellsToSelectby_pairs.ToArray())
//                    //procs.Add(proc);
//                    selectBy.Add(String.Format(
//@"
//GO
//SET QUOTED_IDENTIFIER ON
//GO
//
//Create PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//{2}
//AS
//BEGIN 
//SELECT 
//{3}
//FROM dbo.[{0}] {0} 
//WHERE 
//{4}
//{5}
//END
//"
//    , ClassName,
//    string.Join("_", cellsToSelectby_pairs.ToArray()),
//    string.Join("," + Environment.NewLine, cellsToSelectby_pairs_prm.ToArray()),
//    string.Join("," + Environment.NewLine, list_selectNames.ToArray()),
//    string.Join(" AND " + Environment.NewLine, cellsToSelectby_pairs_where.ToArray()),
//    withIsDeleted ? (" AND " + where_isDeleted) : ""
//));

//                    #endregion
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        //Select By Single
//        public string GenerateSelectByViewParamsProcedure_Single(string ClassName)
//        {
//            int count = 0;
//            bool withIsDeleted = true;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource;//.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();
//            string where_isDeleted = (String.Format("{0}.[{1}] = 0", ClassName, IS_DELETED));
//            #region Select Names
//            List<string> list_selectNames = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//                string DBLine = String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, key.Key);
//                list_selectNames.Add(DBLine);
//            }
//            #endregion

//            #region Single
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                List<string> cellsToSelectby_pairs_prm = new List<string>();
//                List<string> cellsToSelectby_pairs_where = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Single

//                cellsToSelectby_pairs.Add(key.Key);
//                cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                #region Proc
//                //flag to check if the deleted key is include in this query
//                withIsDeleted = cellsToSelectby_pairs.Find(R => string.Equals(R.ToLower(), IS_DELETED.ToLower())) == null;
//                count++;
//                string proc = String.Format("");
//                //string.Join("_", cellsToSelectby_pairs.ToArray())
//                //procs.Add(proc);
//                selectBy.Add(String.Format(
//@"
//GO
//SET QUOTED_IDENTIFIER ON
//GO
//
//Create PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//{2}
//AS
//BEGIN 
//SELECT 
//{3}
//FROM dbo.[{0}] {0} 
//WHERE 
//{4}
//{5}
//END
//"
//, ClassName,
//string.Join("_", cellsToSelectby_pairs.ToArray()),
//string.Join("," + Environment.NewLine, cellsToSelectby_pairs_prm.ToArray()),
//string.Join("," + Environment.NewLine, list_selectNames.ToArray()),
//string.Join(" AND " + Environment.NewLine, cellsToSelectby_pairs_where.ToArray()),
//    withIsDeleted ? (" AND " + where_isDeleted) : ""

//));

//                #endregion

//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        //Select By Combination
//        public string GenerateSelectByViewParamsProcedure_Combination(string ClassName)
//        {
//            int count = 0;

//            bool withIsDeleted = true;
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource;//.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();
//            string where_isDeleted = (String.Format("{0}.[{1}] = 0", ClassName, IS_DELETED));

//            #region Select Names
//            List<string> list_selectNames = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);
//                string DBLine = String.Format(" {0}.[{1}] as [{0}.{1}]", ClassName, key.Key);
//                list_selectNames.Add(DBLine);
//            }
//            #endregion

//            #region Pairs
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                List<string> cellsToSelectby_indexs = new List<string>();
//                List<string> cellsToSelectby_pairs_prm = new List<string>();
//                List<string> cellsToSelectby_pairs_where = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Pair
//                cellsToSelectby_indexs.Add(i.ToString());
//                cellsToSelectby_pairs.Add(key.Key);
//                cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                    //add new 
//                    cellsToSelectby_pairs.Add(key.Key);
//                    cellsToSelectby_indexs.Add(j.ToString());
//                    if (cellsToSelectby_pairs.Count > 2)
//                    {
//                        cellsToSelectby_pairs_prm.Add(String.Format("@prm_{0} {1}", key.Key, key.Value));
//                        cellsToSelectby_pairs_where.Add(String.Format("{1}.[{0}] = @prm_{0}", key.Key, ClassName));
//                        //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                        #region Proc
//                        //flag to check if the deleted key is include in this query
//                        withIsDeleted = cellsToSelectby_pairs.Find(R => string.Equals(R.ToLower(), IS_DELETED.ToLower())) == null;
//                        count++;
//                        string proc = String.Format("");
//                        //string.Join("_", cellsToSelectby_pairs.ToArray())
//                        //procs.Add(proc);
//                        selectBy.Add(String.Format(
//    @"
//        GO
//        SET QUOTED_IDENTIFIER ON
//        GO
//        
//        Create PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//        {2}
//        AS
//        BEGIN 
//        -- {6}
//        SELECT 
//        {3}
//        FROM dbo.[{0}] {0} 
//        WHERE 
//        {4}
//        {5}
//        END
//        "
//    , ClassName,
//        string.Join("_", cellsToSelectby_pairs.ToArray()),
//        string.Join("," + Environment.NewLine, cellsToSelectby_pairs_prm.ToArray()),
//        string.Join("," + Environment.NewLine, list_selectNames.ToArray()),
//        string.Join(" AND " + Environment.NewLine, cellsToSelectby_pairs_where.ToArray()),
//        withIsDeleted ? (" AND " + where_isDeleted) : "",
//        string.Join("_", cellsToSelectby_indexs.ToArray())

//    ));



//                        #endregion
//                    }
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }

//        #region DELETE PROCS
//        #region DELETE basic procs

//        public string GenerateDeleteBasicProcedure(string ClassName, string action)
//        {
//            //Insert
//            string res = string.Format(@"
//            GO
//            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{1}_{0}]') AND type in (N'P', N'PC'))
//            DROP PROCEDURE [dbo].[{1}_{0}]", ClassName, action);
//            return res;
//        }
//        public string GenerateDeleteSelectByIDProcedure(string ClassName)
//        {
//            //Insert
//            string res = string.Format(@"
//            GO
//            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Select_{0}_By_Id]') AND type in (N'P', N'PC'))
//            DROP PROCEDURE [dbo].[Select_{0}_By_Id]", ClassName);
//            return res;
//        }
//        public string GenerateDeleteDBTabls(string ClassName)
//        {
//            //Insert
//            string res = string.Format(@"
//            GO
//            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'U'))
//            DROP TABLE [dbo].[{0}]", ClassName);
//            return res;
//        }

//        #endregion

//        //Select By Pairs
//        public string GenerateDeleteSelectByViewParamsProcedure_Pairs(string ClassName)
//        {
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable);
//            List<string> selectBy = new List<string>();

//            #region Pairs
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Pair

//                cellsToSelectby_pairs.Add(key.Key);
//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    //remove last pair
//                    if (cellsToSelectby_pairs.Count == 2)
//                    {
//                        cellsToSelectby_pairs.RemoveAt(1);
//                    }
//                    key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                    //add new 
//                    cellsToSelectby_pairs.Add(key.Key);

//                    #region Proc
//                    string proc = String.Format("");
//                    //string.Join("_", cellsToSelectby_pairs.ToArray())
//                    //procs.Add(proc);
//                    selectBy.Add(String.Format(
//@"
//GO
//IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[select_{0}_by_keys_view_{1}]') AND type in (N'P', N'PC'))
//DROP PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]"
//, ClassName,
//    string.Join("_", cellsToSelectby_pairs.ToArray())
//));

//                    #endregion
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        //Select By Single
//        public string GenerateDeleteSelectByViewParamsProcedure_Single(string ClassName)
//        {
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();

//            #region Single
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Single

//                cellsToSelectby_pairs.Add(key.Key);
//                //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                #region Proc
//                string proc = String.Format("");
//                //string.Join("_", cellsToSelectby_pairs.ToArray())
//                //procs.Add(proc);
//                selectBy.Add(String.Format(
//@"
//GO
//IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[select_{0}_by_keys_view_{1}]') AND type in (N'P', N'PC'))
//DROP PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//"
//, ClassName,
//string.Join("_", cellsToSelectby_pairs.ToArray())
//));

//                #endregion

//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }
//        //Select By Combination
//        public string GenerateDeleteSelectByViewParamsProcedure_Combination(string ClassName)
//        {
//            IEnumerable<KeyValues<I>> list = this.KeyValuesContainer.DataSource.FindAll(R => !R.IsMirrorable); ;
//            List<string> selectBy = new List<string>();

//            #region Pairs
//            selectBy = new List<string>();
//            for (int i = 0; i < list.Count(); i++)
//            {
//                List<string> cellsToSelectby_pairs = new List<string>();
//                //get key name and type
//                KeyValuePair<string, string> key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(i).KeyValuesType);

//                #region Select by Pair

//                cellsToSelectby_pairs.Add(key.Key);
//                for (int j = (i + 1); j < list.Count(); j++)
//                {
//                    //remove last pair
//                    if (cellsToSelectby_pairs.Count > 2)
//                    {
//                        key = DBKeyValurPair.GetDBKeyValueByDBKey(list.ElementAt(j).KeyValuesType);
//                        //add new 
//                        cellsToSelectby_pairs.Add(key.Key);
//                        //cellsToSelectby_pairs_where.Add(String.Format("{0}.[is_deleted] = 0", ClassName));
//                        #region Proc
//                        string proc = String.Format("");
//                        //string.Join("_", cellsToSelectby_pairs.ToArray())
//                        //procs.Add(proc);
//                        selectBy.Add(String.Format(
//    @"
//GO
//IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[select_{0}_by_keys_view_{1}]') AND type in (N'P', N'PC'))
//DROP PROCEDURE [dbo].[select_{0}_by_keys_view_{1}]
//"
//    , ClassName,
//        string.Join("_", cellsToSelectby_pairs.ToArray())
//    ));



//                        #endregion
//                    }
//                }
//                #endregion
//            }

//            #endregion

//            return string.Join("" + Environment.NewLine, selectBy);
//        }

//        #endregion