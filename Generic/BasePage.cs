﻿using BL_LowCost;
using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    public class BasePage : LangManager
    {
        //public static readonly string VirtualDirectoryPath = @"C:\Users\User17\Desktop\LowCost\Flights Admin\FlightsAdmin";

        //public static readonly string UploadDirectoryName = @"\Images\Uploads";

        public static WhiteLabel WhiteLabelObj
        {
            get
            {
                //string domain = HttpContext.Current.Request.Url.Host;
                WhiteLabel oWhiteLabel = null;
                //Get the white label id from web config
                int whiteLabel = WebConfigHandler.WhiteLabelID;
                // Get the white label object from the database
                oWhiteLabel = WhiteLabelContainer.SelectByKeysView_DocId(whiteLabel, true).Single;
                if (oWhiteLabel == null)
                {
                    return null;
                }
                //WhiteLabel oWhiteLabel = WhiteLabelContainer.SelectByKeysView_DefaultDomain(domain, true).Single;
                //Domains oDomains = ApplicationManager.GetDomainsContainerFromApplication().FindAllContainer(R => R.Domain_UI.ToLower() == domain.ToLower()).Single;
                //if (oDomains != null)
                //{
                //    oWhiteLabel = WhiteLabelContainer.SelectByKeysView_DocId(oDomains.WhiteLabelDocId_Value, true).Single;
                //    if (oWhiteLabel == null)
                //    {
                //        HttpContext.Current.Response.Redirect(SiteDomain);
                //    }
                //}
                //else
                //{
                //    HttpContext.Current.Response.Redirect(SiteDomain);
                //}
                return oWhiteLabel;
            }
        }
        
    }
}
