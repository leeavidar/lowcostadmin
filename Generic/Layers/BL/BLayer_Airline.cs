

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class AirlineContainer  : Container<AirlineContainer, Airline>{
#region Extra functions

#endregion

        #region Static Method
        
        public static AirlineContainer SelectByID(int doc_id,bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            oAirlineContainer.Add(oAirlineContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oAirlineContainer;
        }

        
        public static AirlineContainer SelectAllAirlines(bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            oAirlineContainer.Add(oAirlineContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oAirlineContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static AirlineContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DateCreated = _DateCreated;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DateCreated(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //B
        //1
        public static AirlineContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DocId = _DocId;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DocId(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //C
        //2
        public static AirlineContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.IsDeleted = _IsDeleted;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_IsDeleted(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //E
        //4
        public static AirlineContainer SelectByKeysView_IataCode(
string _IataCode , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.IataCode = _IataCode;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_IataCode(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //F
        //5
        public static AirlineContainer SelectByKeysView_Name(
string _Name , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.Name = _Name;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_Name(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //A_B
        //0_1
        public static AirlineContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DateCreated = _DateCreated; 
 oAirline.DocId = _DocId;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DateCreated_DocId(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //A_C
        //0_2
        public static AirlineContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DateCreated = _DateCreated; 
 oAirline.IsDeleted = _IsDeleted;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //A_E
        //0_4
        public static AirlineContainer SelectByKeysView_DateCreated_IataCode(
DateTime _DateCreated,
string _IataCode , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DateCreated = _DateCreated; 
 oAirline.IataCode = _IataCode;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DateCreated_IataCode(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DateCreated_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //A_F
        //0_5
        public static AirlineContainer SelectByKeysView_DateCreated_Name(
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DateCreated = _DateCreated; 
 oAirline.Name = _Name;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DateCreated_Name(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //B_C
        //1_2
        public static AirlineContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DocId = _DocId; 
 oAirline.IsDeleted = _IsDeleted;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DocId_IsDeleted(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //B_E
        //1_4
        public static AirlineContainer SelectByKeysView_DocId_IataCode(
int _DocId,
string _IataCode , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DocId = _DocId; 
 oAirline.IataCode = _IataCode;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DocId_IataCode(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DocId_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //B_F
        //1_5
        public static AirlineContainer SelectByKeysView_DocId_Name(
int _DocId,
string _Name , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.DocId = _DocId; 
 oAirline.Name = _Name;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_DocId_Name(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //C_E
        //2_4
        public static AirlineContainer SelectByKeysView_IsDeleted_IataCode(
bool _IsDeleted,
string _IataCode , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.IsDeleted = _IsDeleted; 
 oAirline.IataCode = _IataCode;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_IsDeleted_IataCode(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_IsDeleted_IataCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //C_F
        //2_5
        public static AirlineContainer SelectByKeysView_IsDeleted_Name(
bool _IsDeleted,
string _Name , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.IsDeleted = _IsDeleted; 
 oAirline.Name = _Name;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_IsDeleted_Name(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }



        //E_F
        //4_5
        public static AirlineContainer SelectByKeysView_IataCode_Name(
string _IataCode,
string _Name , bool? isActive)
        {
            AirlineContainer oAirlineContainer = new AirlineContainer();
            Airline oAirline = new Airline();
            #region Params
            
 oAirline.IataCode = _IataCode; 
 oAirline.Name = _Name;
            #endregion 
            oAirlineContainer.Add(SelectData(Airline.GetParamsForSelectByKeysView_IataCode_Name(oAirline), TBNames_Airline.PROC_Select_Airline_By_Keys_View_IataCode_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineContainer = oAirlineContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineContainer;
        }


#endregion
}

    
}
