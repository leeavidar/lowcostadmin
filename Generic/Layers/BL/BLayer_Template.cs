

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class TemplateContainer  : Container<TemplateContainer, Template>{
#region Extra functions

#endregion

        #region Static Method
        
        public static TemplateContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            oTemplateContainer.Add(oTemplateContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oTemplateContainer;
        }

        
        public static TemplateContainer SelectAllTemplates(int? _WhiteLabelDocId,bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            oTemplateContainer.Add(oTemplateContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oTemplateContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //I_A
        //8_0
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_B
        //8_1
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_C
        //8_2
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_E
        //8_4
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title(
int _WhiteLabelDocId,
string _Title , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_F
        //8_5
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_SubTitle(
int _WhiteLabelDocId,
string _SubTitle , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.SubTitle = _SubTitle;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_G
        //8_6
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Type(
int _WhiteLabelDocId,
string _Type , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Type = _Type;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Type(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_H
        //8_7
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Order(
int _WhiteLabelDocId,
int _Order , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Order = _Order;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Order(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_J
        //8_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_RelatedObjectType(
int _WhiteLabelDocId,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_K
        //8_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_RelatedObjectDocId(
int _WhiteLabelDocId,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_L
        //8_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_TextLong(
int _WhiteLabelDocId,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_M
        //8_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_TextShort(
int _WhiteLabelDocId,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_A_B
        //8_0_1
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.DocId = _DocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_A_C
        //8_0_2
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.IsDeleted = _IsDeleted;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_A_E
        //8_0_4
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Title(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.Title = _Title;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_A_F
        //8_0_5
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SubTitle(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _SubTitle , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.SubTitle = _SubTitle;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SubTitle(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_A_G
        //8_0_6
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Type(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Type , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.Type = _Type;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Type(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Type));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_A_H
        //8_0_7
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Order(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Order , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.Order = _Order;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_A_J
        //8_0_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObjectType(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_A_K
        //8_0_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObjectDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_A_L
        //8_0_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TextLong(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_A_M
        //8_0_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TextShort(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DateCreated = _DateCreated; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DateCreated_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_B_C
        //8_1_2
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.IsDeleted = _IsDeleted;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_B_E
        //8_1_4
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_Title(
int _WhiteLabelDocId,
int _DocId,
string _Title , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.Title = _Title;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_B_F
        //8_1_5
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_SubTitle(
int _WhiteLabelDocId,
int _DocId,
string _SubTitle , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.SubTitle = _SubTitle;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SubTitle(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_B_G
        //8_1_6
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_Type(
int _WhiteLabelDocId,
int _DocId,
string _Type , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.Type = _Type;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Type(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Type));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_B_H
        //8_1_7
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_Order(
int _WhiteLabelDocId,
int _DocId,
int _Order , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.Order = _Order;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_B_J
        //8_1_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_RelatedObjectType(
int _WhiteLabelDocId,
int _DocId,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_B_K
        //8_1_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_RelatedObjectDocId(
int _WhiteLabelDocId,
int _DocId,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_B_L
        //8_1_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_TextLong(
int _WhiteLabelDocId,
int _DocId,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_B_M
        //8_1_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_DocId_TextShort(
int _WhiteLabelDocId,
int _DocId,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.DocId = _DocId; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_DocId_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_C_E
        //8_2_4
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Title(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Title , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.Title = _Title;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_C_F
        //8_2_5
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SubTitle(
int _WhiteLabelDocId,
bool _IsDeleted,
string _SubTitle , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.SubTitle = _SubTitle;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SubTitle(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_C_G
        //8_2_6
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Type(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Type , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.Type = _Type;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Type(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Type));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_C_H
        //8_2_7
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Order(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Order , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.Order = _Order;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_C_J
        //8_2_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObjectType(
int _WhiteLabelDocId,
bool _IsDeleted,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_C_K
        //8_2_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObjectDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_C_L
        //8_2_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TextLong(
int _WhiteLabelDocId,
bool _IsDeleted,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_C_M
        //8_2_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TextShort(
int _WhiteLabelDocId,
bool _IsDeleted,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.IsDeleted = _IsDeleted; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_IsDeleted_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_E_F
        //8_4_5
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title_SubTitle(
int _WhiteLabelDocId,
string _Title,
string _SubTitle , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title; 
 oTemplate.SubTitle = _SubTitle;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_SubTitle(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title) && string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_E_G
        //8_4_6
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title_Type(
int _WhiteLabelDocId,
string _Title,
string _Type , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title; 
 oTemplate.Type = _Type;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Type(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_Type));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_E_H
        //8_4_7
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title_Order(
int _WhiteLabelDocId,
string _Title,
int _Order , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title; 
 oTemplate.Order = _Order;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Order(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_E_J
        //8_4_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title_RelatedObjectType(
int _WhiteLabelDocId,
string _Title,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_E_K
        //8_4_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title_RelatedObjectDocId(
int _WhiteLabelDocId,
string _Title,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title));
            #endregion
            return oTemplateContainer;
        }



        //I_E_L
        //8_4_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title_TextLong(
int _WhiteLabelDocId,
string _Title,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title) && string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_E_M
        //8_4_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Title_TextShort(
int _WhiteLabelDocId,
string _Title,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Title = _Title; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Title_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.Title, _Title) && string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_F_G
        //8_5_6
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_SubTitle_Type(
int _WhiteLabelDocId,
string _SubTitle,
string _Type , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.SubTitle = _SubTitle; 
 oTemplate.Type = _Type;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_Type(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_Type));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_F_H
        //8_5_7
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_SubTitle_Order(
int _WhiteLabelDocId,
string _SubTitle,
int _Order , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.SubTitle = _SubTitle; 
 oTemplate.Order = _Order;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_Order(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_F_J
        //8_5_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_SubTitle_RelatedObjectType(
int _WhiteLabelDocId,
string _SubTitle,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.SubTitle = _SubTitle; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_F_K
        //8_5_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_SubTitle_RelatedObjectDocId(
int _WhiteLabelDocId,
string _SubTitle,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.SubTitle = _SubTitle; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oTemplateContainer;
        }



        //I_F_L
        //8_5_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_SubTitle_TextLong(
int _WhiteLabelDocId,
string _SubTitle,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.SubTitle = _SubTitle; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle) && string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_F_M
        //8_5_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_SubTitle_TextShort(
int _WhiteLabelDocId,
string _SubTitle,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.SubTitle = _SubTitle; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_SubTitle_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle) && string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_G_H
        //8_6_7
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Type_Order(
int _WhiteLabelDocId,
string _Type,
int _Order , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Type = _Type; 
 oTemplate.Order = _Order;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_Order(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_Order));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_G_J
        //8_6_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Type_RelatedObjectType(
int _WhiteLabelDocId,
string _Type,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Type = _Type; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_G_K
        //8_6_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Type_RelatedObjectDocId(
int _WhiteLabelDocId,
string _Type,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Type = _Type; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_G_L
        //8_6_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Type_TextLong(
int _WhiteLabelDocId,
string _Type,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Type = _Type; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_G_M
        //8_6_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Type_TextShort(
int _WhiteLabelDocId,
string _Type,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Type = _Type; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Type_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_H_J
        //8_7_9
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Order_RelatedObjectType(
int _WhiteLabelDocId,
int _Order,
int _RelatedObjectType , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Order = _Order; 
 oTemplate.RelatedObjectType = _RelatedObjectType;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_RelatedObjectType(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_RelatedObjectType));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_H_K
        //8_7_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Order_RelatedObjectDocId(
int _WhiteLabelDocId,
int _Order,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Order = _Order; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_H_L
        //8_7_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Order_TextLong(
int _WhiteLabelDocId,
int _Order,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Order = _Order; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_H_M
        //8_7_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_Order_TextShort(
int _WhiteLabelDocId,
int _Order,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.Order = _Order; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_Order_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_J_K
        //8_9_10
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId(
int _WhiteLabelDocId,
int _RelatedObjectType,
int _RelatedObjectDocId , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.RelatedObjectType = _RelatedObjectType; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oTemplateContainer;
        }



        //I_J_L
        //8_9_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_RelatedObjectType_TextLong(
int _WhiteLabelDocId,
int _RelatedObjectType,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.RelatedObjectType = _RelatedObjectType; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_J_M
        //8_9_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_RelatedObjectType_TextShort(
int _WhiteLabelDocId,
int _RelatedObjectType,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.RelatedObjectType = _RelatedObjectType; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectType_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectType_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_K_L
        //8_10_11
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_RelatedObjectDocId_TextLong(
int _WhiteLabelDocId,
int _RelatedObjectDocId,
string _TextLong , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId; 
 oTemplate.TextLong = _TextLong;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectDocId_TextLong(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId_TextLong));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong));
            #endregion
            return oTemplateContainer;
        }



        //I_K_M
        //8_10_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_RelatedObjectDocId_TextShort(
int _WhiteLabelDocId,
int _RelatedObjectDocId,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.RelatedObjectDocId = _RelatedObjectDocId; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_RelatedObjectDocId_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_RelatedObjectDocId_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }



        //I_L_M
        //8_11_12
        public static TemplateContainer SelectByKeysView_WhiteLabelDocId_TextLong_TextShort(
int _WhiteLabelDocId,
string _TextLong,
string _TextShort , bool? isActive)
        {
            TemplateContainer oTemplateContainer = new TemplateContainer();
            Template oTemplate = new Template();
            #region Params
            
 oTemplate.WhiteLabelDocId = _WhiteLabelDocId; 
 oTemplate.TextLong = _TextLong; 
 oTemplate.TextShort = _TextShort;
            #endregion 
            oTemplateContainer.Add(SelectData(Template.GetParamsForSelectByKeysView_WhiteLabelDocId_TextLong_TextShort(oTemplate), TBNames_Template.PROC_Select_Template_By_Keys_View_WhiteLabelDocId_TextLong_TextShort));
            #region ExtraFilters
            
if(isActive != null){
                oTemplateContainer = oTemplateContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oTemplateContainer = oTemplateContainer.FindAllContainer(R => string.Equals(R.TextLong, _TextLong) && string.Equals(R.TextShort, _TextShort));
            #endregion
            return oTemplateContainer;
        }


#endregion
}

    
}
