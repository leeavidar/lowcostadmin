

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class UsersContainer  : Container<UsersContainer, Users>{
#region Extra functions

#endregion

        #region Static Method
        
        public static UsersContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            oUsersContainer.Add(oUsersContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oUsersContainer;
        }

        
        public static UsersContainer SelectAllUserss(int? _WhiteLabelDocId,bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            oUsersContainer.Add(oUsersContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oUsersContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //K_A
        //10_0
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B
        //10_1
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_C
        //10_2
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.IsDeleted = _IsDeleted;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_E
        //10_4
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_UserName(
int _WhiteLabelDocId,
string _UserName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.UserName = _UserName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_UserName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_F
        //10_5
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Password(
int _WhiteLabelDocId,
string _Password , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Password = _Password;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Password(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_G
        //10_6
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Email(
int _WhiteLabelDocId,
string _Email , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Email = _Email;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Email(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_H
        //10_7
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_FirstName(
int _WhiteLabelDocId,
string _FirstName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.FirstName = _FirstName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_I
        //10_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_LastName(
int _WhiteLabelDocId,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_J
        //10_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Role(
int _WhiteLabelDocId,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_B
        //10_0_1
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.DocId = _DocId;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_C
        //10_0_2
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.IsDeleted = _IsDeleted;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_E
        //10_0_4
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_UserName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _UserName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.UserName = _UserName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_UserName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_UserName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_F
        //10_0_5
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Password(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Password , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.Password = _Password;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Password(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Password));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_G
        //10_0_6
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Email(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Email , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.Email = _Email;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Email(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Email));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_H
        //10_0_7
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FirstName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.FirstName = _FirstName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_I
        //10_0_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LastName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_A_J
        //10_0_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Role(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DateCreated = _DateCreated; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B_C
        //10_1_2
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId; 
 oUsers.IsDeleted = _IsDeleted;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B_E
        //10_1_4
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId_UserName(
int _WhiteLabelDocId,
int _DocId,
string _UserName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId; 
 oUsers.UserName = _UserName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_UserName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_UserName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B_F
        //10_1_5
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId_Password(
int _WhiteLabelDocId,
int _DocId,
string _Password , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId; 
 oUsers.Password = _Password;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Password(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Password));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B_G
        //10_1_6
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId_Email(
int _WhiteLabelDocId,
int _DocId,
string _Email , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId; 
 oUsers.Email = _Email;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Email(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Email));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B_H
        //10_1_7
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId_FirstName(
int _WhiteLabelDocId,
int _DocId,
string _FirstName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId; 
 oUsers.FirstName = _FirstName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FirstName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B_I
        //10_1_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId_LastName(
int _WhiteLabelDocId,
int _DocId,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_B_J
        //10_1_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_DocId_Role(
int _WhiteLabelDocId,
int _DocId,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.DocId = _DocId; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_C_E
        //10_2_4
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_UserName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _UserName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.IsDeleted = _IsDeleted; 
 oUsers.UserName = _UserName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_UserName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_UserName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_C_F
        //10_2_5
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Password(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Password , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.IsDeleted = _IsDeleted; 
 oUsers.Password = _Password;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Password(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Password));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_C_G
        //10_2_6
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Email(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Email , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.IsDeleted = _IsDeleted; 
 oUsers.Email = _Email;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Email(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Email));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_C_H
        //10_2_7
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FirstName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.IsDeleted = _IsDeleted; 
 oUsers.FirstName = _FirstName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_C_I
        //10_2_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.IsDeleted = _IsDeleted; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_C_J
        //10_2_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Role(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.IsDeleted = _IsDeleted; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_E_F
        //10_4_5
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_UserName_Password(
int _WhiteLabelDocId,
string _UserName,
string _Password , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.UserName = _UserName; 
 oUsers.Password = _Password;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_Password(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Password));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_E_G
        //10_4_6
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_UserName_Email(
int _WhiteLabelDocId,
string _UserName,
string _Email , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.UserName = _UserName; 
 oUsers.Email = _Email;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_Email(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Email));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_E_H
        //10_4_7
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_UserName_FirstName(
int _WhiteLabelDocId,
string _UserName,
string _FirstName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.UserName = _UserName; 
 oUsers.FirstName = _FirstName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_FirstName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_E_I
        //10_4_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_UserName_LastName(
int _WhiteLabelDocId,
string _UserName,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.UserName = _UserName; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_E_J
        //10_4_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_UserName_Role(
int _WhiteLabelDocId,
string _UserName,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.UserName = _UserName; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_UserName_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_F_G
        //10_5_6
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Password_Email(
int _WhiteLabelDocId,
string _Password,
string _Email , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Password = _Password; 
 oUsers.Email = _Email;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Password_Email(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_Email));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_F_H
        //10_5_7
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Password_FirstName(
int _WhiteLabelDocId,
string _Password,
string _FirstName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Password = _Password; 
 oUsers.FirstName = _FirstName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Password_FirstName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_F_I
        //10_5_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Password_LastName(
int _WhiteLabelDocId,
string _Password,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Password = _Password; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Password_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_F_J
        //10_5_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Password_Role(
int _WhiteLabelDocId,
string _Password,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Password = _Password; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Password_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_G_H
        //10_6_7
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Email_FirstName(
int _WhiteLabelDocId,
string _Email,
string _FirstName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Email = _Email; 
 oUsers.FirstName = _FirstName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Email_FirstName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_G_I
        //10_6_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Email_LastName(
int _WhiteLabelDocId,
string _Email,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Email = _Email; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Email_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_G_J
        //10_6_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_Email_Role(
int _WhiteLabelDocId,
string _Email,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.Email = _Email; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_Email_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_H_I
        //10_7_8
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_FirstName_LastName(
int _WhiteLabelDocId,
string _FirstName,
string _LastName , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.FirstName = _FirstName; 
 oUsers.LastName = _LastName;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_LastName(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_FirstName_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_H_J
        //10_7_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_FirstName_Role(
int _WhiteLabelDocId,
string _FirstName,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.FirstName = _FirstName; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_FirstName_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }



        //K_I_J
        //10_8_9
        public static UsersContainer SelectByKeysView_WhiteLabelDocId_LastName_Role(
int _WhiteLabelDocId,
string _LastName,
int _Role , bool? isActive)
        {
            UsersContainer oUsersContainer = new UsersContainer();
            Users oUsers = new Users();
            #region Params
            
 oUsers.WhiteLabelDocId = _WhiteLabelDocId; 
 oUsers.LastName = _LastName; 
 oUsers.Role = _Role;
            #endregion 
            oUsersContainer.Add(SelectData(Users.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Role(oUsers), TBNames_Users.PROC_Select_Users_By_Keys_View_WhiteLabelDocId_LastName_Role));
            #region ExtraFilters
            
if(isActive != null){
                oUsersContainer = oUsersContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oUsersContainer;
        }


#endregion
}

    
}
