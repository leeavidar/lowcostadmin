

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class AirlineFareContainer  : Container<AirlineFareContainer, AirlineFare>{
#region Extra functions

#endregion

        #region Static Method
        
        public static AirlineFareContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            oAirlineFareContainer.Add(oAirlineFareContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oAirlineFareContainer;
        }

        
        public static AirlineFareContainer SelectAllAirlineFares(int? _WhiteLabelDocId,bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            oAirlineFareContainer.Add(oAirlineFareContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oAirlineFareContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //I_A
        //8_0
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DateCreated = _DateCreated;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_B
        //8_1
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DocId = _DocId;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_C
        //8_2
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.IsDeleted = _IsDeleted;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_E
        //8_4
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_AirlineCode(
int _WhiteLabelDocId,
string _AirlineCode , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.AirlineCode = _AirlineCode;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_F
        //8_5
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_Name(
int _WhiteLabelDocId,
string _Name , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.Name = _Name;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_Name(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oAirlineFareContainer;
        }



        //I_G
        //8_6
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_Standard(
int _WhiteLabelDocId,
string _Standard , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.Standard = _Standard;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_Standard(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Standard));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Standard, _Standard));
            #endregion
            return oAirlineFareContainer;
        }



        //I_H
        //8_7
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_Flexible(
int _WhiteLabelDocId,
string _Flexible , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.Flexible = _Flexible;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_Flexible(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Flexible));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Flexible, _Flexible));
            #endregion
            return oAirlineFareContainer;
        }



        //I_A_B
        //8_0_1
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DateCreated = _DateCreated; 
 oAirlineFare.DocId = _DocId;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_A_C
        //8_0_2
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DateCreated = _DateCreated; 
 oAirlineFare.IsDeleted = _IsDeleted;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_A_E
        //8_0_4
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DateCreated_AirlineCode(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _AirlineCode , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DateCreated = _DateCreated; 
 oAirlineFare.AirlineCode = _AirlineCode;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineCode(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_A_F
        //8_0_5
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Name(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DateCreated = _DateCreated; 
 oAirlineFare.Name = _Name;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oAirlineFareContainer;
        }



        //I_A_G
        //8_0_6
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Standard(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Standard , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DateCreated = _DateCreated; 
 oAirlineFare.Standard = _Standard;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Standard(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Standard));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Standard, _Standard));
            #endregion
            return oAirlineFareContainer;
        }



        //I_A_H
        //8_0_7
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Flexible(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Flexible , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DateCreated = _DateCreated; 
 oAirlineFare.Flexible = _Flexible;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Flexible(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Flexible));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Flexible, _Flexible));
            #endregion
            return oAirlineFareContainer;
        }



        //I_B_C
        //8_1_2
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DocId = _DocId; 
 oAirlineFare.IsDeleted = _IsDeleted;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_B_E
        //8_1_4
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DocId_AirlineCode(
int _WhiteLabelDocId,
int _DocId,
string _AirlineCode , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DocId = _DocId; 
 oAirlineFare.AirlineCode = _AirlineCode;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineCode(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_AirlineCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_B_F
        //8_1_5
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DocId_Name(
int _WhiteLabelDocId,
int _DocId,
string _Name , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DocId = _DocId; 
 oAirlineFare.Name = _Name;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oAirlineFareContainer;
        }



        //I_B_G
        //8_1_6
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DocId_Standard(
int _WhiteLabelDocId,
int _DocId,
string _Standard , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DocId = _DocId; 
 oAirlineFare.Standard = _Standard;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Standard(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Standard));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Standard, _Standard));
            #endregion
            return oAirlineFareContainer;
        }



        //I_B_H
        //8_1_7
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_DocId_Flexible(
int _WhiteLabelDocId,
int _DocId,
string _Flexible , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.DocId = _DocId; 
 oAirlineFare.Flexible = _Flexible;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Flexible(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Flexible));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Flexible, _Flexible));
            #endregion
            return oAirlineFareContainer;
        }



        //I_C_E
        //8_2_4
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineCode(
int _WhiteLabelDocId,
bool _IsDeleted,
string _AirlineCode , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.IsDeleted = _IsDeleted; 
 oAirlineFare.AirlineCode = _AirlineCode;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineCode(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineCode));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oAirlineFareContainer;
        }



        //I_C_F
        //8_2_5
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Name(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Name , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.IsDeleted = _IsDeleted; 
 oAirlineFare.Name = _Name;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oAirlineFareContainer;
        }



        //I_C_G
        //8_2_6
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Standard(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Standard , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.IsDeleted = _IsDeleted; 
 oAirlineFare.Standard = _Standard;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Standard(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Standard));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Standard, _Standard));
            #endregion
            return oAirlineFareContainer;
        }



        //I_C_H
        //8_2_7
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Flexible(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Flexible , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.IsDeleted = _IsDeleted; 
 oAirlineFare.Flexible = _Flexible;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Flexible(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Flexible));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Flexible, _Flexible));
            #endregion
            return oAirlineFareContainer;
        }



        //I_E_F
        //8_4_5
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_AirlineCode_Name(
int _WhiteLabelDocId,
string _AirlineCode,
string _Name , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.AirlineCode = _AirlineCode; 
 oAirlineFare.Name = _Name;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode_Name(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Name));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oAirlineFareContainer;
        }



        //I_E_G
        //8_4_6
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_AirlineCode_Standard(
int _WhiteLabelDocId,
string _AirlineCode,
string _Standard , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.AirlineCode = _AirlineCode; 
 oAirlineFare.Standard = _Standard;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode_Standard(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Standard));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Standard, _Standard));
            #endregion
            return oAirlineFareContainer;
        }



        //I_E_H
        //8_4_7
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_AirlineCode_Flexible(
int _WhiteLabelDocId,
string _AirlineCode,
string _Flexible , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.AirlineCode = _AirlineCode; 
 oAirlineFare.Flexible = _Flexible;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode_Flexible(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Flexible));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Flexible, _Flexible));
            #endregion
            return oAirlineFareContainer;
        }



        //I_F_G
        //8_5_6
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_Name_Standard(
int _WhiteLabelDocId,
string _Name,
string _Standard , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.Name = _Name; 
 oAirlineFare.Standard = _Standard;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Standard(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name_Standard));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Name, _Name) && string.Equals(R.Standard, _Standard));
            #endregion
            return oAirlineFareContainer;
        }



        //I_F_H
        //8_5_7
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_Name_Flexible(
int _WhiteLabelDocId,
string _Name,
string _Flexible , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.Name = _Name; 
 oAirlineFare.Flexible = _Flexible;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Flexible(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name_Flexible));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Name, _Name) && string.Equals(R.Flexible, _Flexible));
            #endregion
            return oAirlineFareContainer;
        }



        //I_G_H
        //8_6_7
        public static AirlineFareContainer SelectByKeysView_WhiteLabelDocId_Standard_Flexible(
int _WhiteLabelDocId,
string _Standard,
string _Flexible , bool? isActive)
        {
            AirlineFareContainer oAirlineFareContainer = new AirlineFareContainer();
            AirlineFare oAirlineFare = new AirlineFare();
            #region Params
            
 oAirlineFare.WhiteLabelDocId = _WhiteLabelDocId; 
 oAirlineFare.Standard = _Standard; 
 oAirlineFare.Flexible = _Flexible;
            #endregion 
            oAirlineFareContainer.Add(SelectData(AirlineFare.GetParamsForSelectByKeysView_WhiteLabelDocId_Standard_Flexible(oAirlineFare), TBNames_AirlineFare.PROC_Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Standard_Flexible));
            #region ExtraFilters
            
if(isActive != null){
                oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oAirlineFareContainer = oAirlineFareContainer.FindAllContainer(R => string.Equals(R.Standard, _Standard) && string.Equals(R.Flexible, _Flexible));
            #endregion
            return oAirlineFareContainer;
        }


#endregion
}

    
}
