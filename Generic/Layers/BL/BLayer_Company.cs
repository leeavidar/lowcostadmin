

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class CompanyContainer  : Container<CompanyContainer, Company>{
#region Extra functions

#endregion

        #region Static Method
        
        public static CompanyContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            oCompanyContainer.Add(oCompanyContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oCompanyContainer;
        }

        
        public static CompanyContainer SelectAllCompanys(int? _WhiteLabelDocId,bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            oCompanyContainer.Add(oCompanyContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oCompanyContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //F_A
        //5_0
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.DateCreated = _DateCreated;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_B
        //5_1
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.DocId = _DocId;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_C
        //5_2
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.IsDeleted = _IsDeleted;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_E
        //5_4
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_Name(
int _WhiteLabelDocId,
string _Name , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.Name = _Name;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_Name(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_A_B
        //5_0_1
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.DateCreated = _DateCreated; 
 oCompany.DocId = _DocId;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_A_C
        //5_0_2
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.DateCreated = _DateCreated; 
 oCompany.IsDeleted = _IsDeleted;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_A_E
        //5_0_4
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Name(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.DateCreated = _DateCreated; 
 oCompany.Name = _Name;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_B_C
        //5_1_2
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.DocId = _DocId; 
 oCompany.IsDeleted = _IsDeleted;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_B_E
        //5_1_4
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_DocId_Name(
int _WhiteLabelDocId,
int _DocId,
string _Name , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.DocId = _DocId; 
 oCompany.Name = _Name;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }



        //F_C_E
        //5_2_4
        public static CompanyContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Name(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Name , bool? isActive)
        {
            CompanyContainer oCompanyContainer = new CompanyContainer();
            Company oCompany = new Company();
            #region Params
            
 oCompany.WhiteLabelDocId = _WhiteLabelDocId; 
 oCompany.IsDeleted = _IsDeleted; 
 oCompany.Name = _Name;
            #endregion 
            oCompanyContainer.Add(SelectData(Company.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(oCompany), TBNames_Company.PROC_Select_Company_By_Keys_View_WhiteLabelDocId_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCompanyContainer = oCompanyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCompanyContainer;
        }


#endregion
}

    
}
