

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class SubBoxContainer  : Container<SubBoxContainer, SubBox>{
#region Extra functions

#endregion

        #region Static Method
        
        public static SubBoxContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            oSubBoxContainer.Add(oSubBoxContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oSubBoxContainer;
        }

        
        public static SubBoxContainer SelectAllSubBoxs(int? _WhiteLabelDocId,bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            oSubBoxContainer.Add(oSubBoxContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oSubBoxContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //J_A
        //9_0
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_B
        //9_1
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DocId = _DocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_C
        //9_2
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.IsDeleted = _IsDeleted;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_E
        //9_4
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_SubTitle(
int _WhiteLabelDocId,
string _SubTitle , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.SubTitle = _SubTitle;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oSubBoxContainer;
        }



        //J_F
        //9_5
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_ShortText(
int _WhiteLabelDocId,
string _ShortText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.ShortText = _ShortText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oSubBoxContainer;
        }



        //J_G
        //9_6
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_LongText(
int _WhiteLabelDocId,
string _LongText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.LongText = _LongText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_LongText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_H
        //9_7
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_Order(
int _WhiteLabelDocId,
int _Order , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.Order = _Order;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_Order(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_I
        //9_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_TemplateDocId(
int _WhiteLabelDocId,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_A_B
        //9_0_1
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated; 
 oSubBox.DocId = _DocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_A_C
        //9_0_2
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated; 
 oSubBox.IsDeleted = _IsDeleted;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_A_E
        //9_0_4
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SubTitle(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _SubTitle , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated; 
 oSubBox.SubTitle = _SubTitle;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SubTitle(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oSubBoxContainer;
        }



        //J_A_F
        //9_0_5
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated_ShortText(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _ShortText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated; 
 oSubBox.ShortText = _ShortText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ShortText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oSubBoxContainer;
        }



        //J_A_G
        //9_0_6
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LongText(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LongText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated; 
 oSubBox.LongText = _LongText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LongText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_A_H
        //9_0_7
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Order(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Order , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated; 
 oSubBox.Order = _Order;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_Order));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_A_I
        //9_0_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DateCreated_TemplateDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DateCreated = _DateCreated; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_B_C
        //9_1_2
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DocId = _DocId; 
 oSubBox.IsDeleted = _IsDeleted;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_B_E
        //9_1_4
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DocId_SubTitle(
int _WhiteLabelDocId,
int _DocId,
string _SubTitle , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DocId = _DocId; 
 oSubBox.SubTitle = _SubTitle;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SubTitle(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oSubBoxContainer;
        }



        //J_B_F
        //9_1_5
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DocId_ShortText(
int _WhiteLabelDocId,
int _DocId,
string _ShortText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DocId = _DocId; 
 oSubBox.ShortText = _ShortText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ShortText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oSubBoxContainer;
        }



        //J_B_G
        //9_1_6
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DocId_LongText(
int _WhiteLabelDocId,
int _DocId,
string _LongText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DocId = _DocId; 
 oSubBox.LongText = _LongText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LongText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_B_H
        //9_1_7
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DocId_Order(
int _WhiteLabelDocId,
int _DocId,
int _Order , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DocId = _DocId; 
 oSubBox.Order = _Order;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_Order));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_B_I
        //9_1_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_DocId_TemplateDocId(
int _WhiteLabelDocId,
int _DocId,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.DocId = _DocId; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_C_E
        //9_2_4
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SubTitle(
int _WhiteLabelDocId,
bool _IsDeleted,
string _SubTitle , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.IsDeleted = _IsDeleted; 
 oSubBox.SubTitle = _SubTitle;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SubTitle(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_SubTitle));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oSubBoxContainer;
        }



        //J_C_F
        //9_2_5
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_ShortText(
int _WhiteLabelDocId,
bool _IsDeleted,
string _ShortText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.IsDeleted = _IsDeleted; 
 oSubBox.ShortText = _ShortText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ShortText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oSubBoxContainer;
        }



        //J_C_G
        //9_2_6
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LongText(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LongText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.IsDeleted = _IsDeleted; 
 oSubBox.LongText = _LongText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LongText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_C_H
        //9_2_7
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Order(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Order , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.IsDeleted = _IsDeleted; 
 oSubBox.Order = _Order;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_Order));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_C_I
        //9_2_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_TemplateDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.IsDeleted = _IsDeleted; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }



        //J_E_F
        //9_4_5
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_SubTitle_ShortText(
int _WhiteLabelDocId,
string _SubTitle,
string _ShortText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.SubTitle = _SubTitle; 
 oSubBox.ShortText = _ShortText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_ShortText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_ShortText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle) && string.Equals(R.ShortText, _ShortText));
            #endregion
            return oSubBoxContainer;
        }



        //J_E_G
        //9_4_6
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_SubTitle_LongText(
int _WhiteLabelDocId,
string _SubTitle,
string _LongText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.SubTitle = _SubTitle; 
 oSubBox.LongText = _LongText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_LongText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle) && string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_E_H
        //9_4_7
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_SubTitle_Order(
int _WhiteLabelDocId,
string _SubTitle,
int _Order , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.SubTitle = _SubTitle; 
 oSubBox.Order = _Order;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_Order(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_Order));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oSubBoxContainer;
        }



        //J_E_I
        //9_4_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_SubTitle_TemplateDocId(
int _WhiteLabelDocId,
string _SubTitle,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.SubTitle = _SubTitle; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_SubTitle_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.SubTitle, _SubTitle));
            #endregion
            return oSubBoxContainer;
        }



        //J_F_G
        //9_5_6
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_ShortText_LongText(
int _WhiteLabelDocId,
string _ShortText,
string _LongText , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.ShortText = _ShortText; 
 oSubBox.LongText = _LongText;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_LongText(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_LongText));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText) && string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_F_H
        //9_5_7
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_ShortText_Order(
int _WhiteLabelDocId,
string _ShortText,
int _Order , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.ShortText = _ShortText; 
 oSubBox.Order = _Order;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_Order(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_Order));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oSubBoxContainer;
        }



        //J_F_I
        //9_5_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_ShortText_TemplateDocId(
int _WhiteLabelDocId,
string _ShortText,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.ShortText = _ShortText; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.ShortText, _ShortText));
            #endregion
            return oSubBoxContainer;
        }



        //J_G_H
        //9_6_7
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_LongText_Order(
int _WhiteLabelDocId,
string _LongText,
int _Order , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.LongText = _LongText; 
 oSubBox.Order = _Order;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_Order(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText_Order));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_G_I
        //9_6_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_LongText_TemplateDocId(
int _WhiteLabelDocId,
string _LongText,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.LongText = _LongText; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => string.Equals(R.LongText, _LongText));
            #endregion
            return oSubBoxContainer;
        }



        //J_H_I
        //9_7_8
        public static SubBoxContainer SelectByKeysView_WhiteLabelDocId_Order_TemplateDocId(
int _WhiteLabelDocId,
int _Order,
int _TemplateDocId , bool? isActive)
        {
            SubBoxContainer oSubBoxContainer = new SubBoxContainer();
            SubBox oSubBox = new SubBox();
            #region Params
            
 oSubBox.WhiteLabelDocId = _WhiteLabelDocId; 
 oSubBox.Order = _Order; 
 oSubBox.TemplateDocId = _TemplateDocId;
            #endregion 
            oSubBoxContainer.Add(SelectData(SubBox.GetParamsForSelectByKeysView_WhiteLabelDocId_Order_TemplateDocId(oSubBox), TBNames_SubBox.PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_Order_TemplateDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSubBoxContainer = oSubBoxContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSubBoxContainer;
        }


#endregion
}

    
}
