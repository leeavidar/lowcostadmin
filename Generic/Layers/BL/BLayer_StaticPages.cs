

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class StaticPagesContainer  : Container<StaticPagesContainer, StaticPages>{
#region Extra functions

#endregion

        #region Static Method
        
        public static StaticPagesContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            oStaticPagesContainer.Add(oStaticPagesContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oStaticPagesContainer;
        }

        
        public static StaticPagesContainer SelectAllStaticPagess(int? _WhiteLabelDocId,bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            oStaticPagesContainer.Add(oStaticPagesContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oStaticPagesContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //J_A
        //9_0
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_B
        //9_1
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DocId = _DocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_C
        //9_2
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.IsDeleted = _IsDeleted;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_E
        //9_4
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl(
int _WhiteLabelDocId,
string _FriendlyUrl , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl));
            #endregion
            return oStaticPagesContainer;
        }



        //J_F
        //9_5
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Text(
int _WhiteLabelDocId,
string _Text , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Text = _Text;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Text(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStaticPagesContainer;
        }



        //J_G
        //9_6
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Name(
int _WhiteLabelDocId,
string _Name , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Name = _Name;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Name(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_H
        //9_7
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Type(
int _WhiteLabelDocId,
int _Type , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Type = _Type;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Type(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_I
        //9_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_SeoDocId(
int _WhiteLabelDocId,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_A_B
        //9_0_1
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated; 
 oStaticPages.DocId = _DocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_A_C
        //9_0_2
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated; 
 oStaticPages.IsDeleted = _IsDeleted;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_A_E
        //9_0_4
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FriendlyUrl(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FriendlyUrl , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated; 
 oStaticPages.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FriendlyUrl(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl));
            #endregion
            return oStaticPagesContainer;
        }



        //J_A_F
        //9_0_5
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Text(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Text , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated; 
 oStaticPages.Text = _Text;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Text(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStaticPagesContainer;
        }



        //J_A_G
        //9_0_6
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Name(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated; 
 oStaticPages.Name = _Name;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_A_H
        //9_0_7
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Type(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _Type , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated; 
 oStaticPages.Type = _Type;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Type(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_A_I
        //9_0_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DateCreated = _DateCreated; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_B_C
        //9_1_2
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DocId = _DocId; 
 oStaticPages.IsDeleted = _IsDeleted;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_B_E
        //9_1_4
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DocId_FriendlyUrl(
int _WhiteLabelDocId,
int _DocId,
string _FriendlyUrl , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DocId = _DocId; 
 oStaticPages.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FriendlyUrl(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl));
            #endregion
            return oStaticPagesContainer;
        }



        //J_B_F
        //9_1_5
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DocId_Text(
int _WhiteLabelDocId,
int _DocId,
string _Text , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DocId = _DocId; 
 oStaticPages.Text = _Text;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Text(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStaticPagesContainer;
        }



        //J_B_G
        //9_1_6
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DocId_Name(
int _WhiteLabelDocId,
int _DocId,
string _Name , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DocId = _DocId; 
 oStaticPages.Name = _Name;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_B_H
        //9_1_7
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DocId_Type(
int _WhiteLabelDocId,
int _DocId,
int _Type , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DocId = _DocId; 
 oStaticPages.Type = _Type;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Type(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_B_I
        //9_1_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(
int _WhiteLabelDocId,
int _DocId,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.DocId = _DocId; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_C_E
        //9_2_4
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FriendlyUrl(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FriendlyUrl , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.IsDeleted = _IsDeleted; 
 oStaticPages.FriendlyUrl = _FriendlyUrl;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FriendlyUrl(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_FriendlyUrl));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl));
            #endregion
            return oStaticPagesContainer;
        }



        //J_C_F
        //9_2_5
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Text(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Text , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.IsDeleted = _IsDeleted; 
 oStaticPages.Text = _Text;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Text(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStaticPagesContainer;
        }



        //J_C_G
        //9_2_6
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Name(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Name , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.IsDeleted = _IsDeleted; 
 oStaticPages.Name = _Name;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_C_H
        //9_2_7
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Type(
int _WhiteLabelDocId,
bool _IsDeleted,
int _Type , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.IsDeleted = _IsDeleted; 
 oStaticPages.Type = _Type;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Type(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_C_I
        //9_2_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.IsDeleted = _IsDeleted; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }



        //J_E_F
        //9_4_5
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_Text(
int _WhiteLabelDocId,
string _FriendlyUrl,
string _Text , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.FriendlyUrl = _FriendlyUrl; 
 oStaticPages.Text = _Text;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_Text(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl) && string.Equals(R.Text, _Text));
            #endregion
            return oStaticPagesContainer;
        }



        //J_E_G
        //9_4_6
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_Name(
int _WhiteLabelDocId,
string _FriendlyUrl,
string _Name , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.FriendlyUrl = _FriendlyUrl; 
 oStaticPages.Name = _Name;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_Name(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Name));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl) && string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_E_H
        //9_4_7
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_Type(
int _WhiteLabelDocId,
string _FriendlyUrl,
int _Type , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.FriendlyUrl = _FriendlyUrl; 
 oStaticPages.Type = _Type;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_Type(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl));
            #endregion
            return oStaticPagesContainer;
        }



        //J_E_I
        //9_4_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoDocId(
int _WhiteLabelDocId,
string _FriendlyUrl,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.FriendlyUrl = _FriendlyUrl; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.FriendlyUrl, _FriendlyUrl));
            #endregion
            return oStaticPagesContainer;
        }



        //J_F_G
        //9_5_6
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Text_Name(
int _WhiteLabelDocId,
string _Text,
string _Name , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Text = _Text; 
 oStaticPages.Name = _Name;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Name(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_Name));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Text, _Text) && string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_F_H
        //9_5_7
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Text_Type(
int _WhiteLabelDocId,
string _Text,
int _Type , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Text = _Text; 
 oStaticPages.Type = _Type;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Type(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStaticPagesContainer;
        }



        //J_F_I
        //9_5_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Text_SeoDocId(
int _WhiteLabelDocId,
string _Text,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Text = _Text; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Text_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStaticPagesContainer;
        }



        //J_G_H
        //9_6_7
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Name_Type(
int _WhiteLabelDocId,
string _Name,
int _Type , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Name = _Name; 
 oStaticPages.Type = _Type;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Type(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_G_I
        //9_6_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Name_SeoDocId(
int _WhiteLabelDocId,
string _Name,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Name = _Name; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Name_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => string.Equals(R.Name, _Name));
            #endregion
            return oStaticPagesContainer;
        }



        //J_H_I
        //9_7_8
        public static StaticPagesContainer SelectByKeysView_WhiteLabelDocId_Type_SeoDocId(
int _WhiteLabelDocId,
int _Type,
int _SeoDocId , bool? isActive)
        {
            StaticPagesContainer oStaticPagesContainer = new StaticPagesContainer();
            StaticPages oStaticPages = new StaticPages();
            #region Params
            
 oStaticPages.WhiteLabelDocId = _WhiteLabelDocId; 
 oStaticPages.Type = _Type; 
 oStaticPages.SeoDocId = _SeoDocId;
            #endregion 
            oStaticPagesContainer.Add(SelectData(StaticPages.GetParamsForSelectByKeysView_WhiteLabelDocId_Type_SeoDocId(oStaticPages), TBNames_StaticPages.PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Type_SeoDocId));
            #region ExtraFilters
            
if(isActive != null){
                oStaticPagesContainer = oStaticPagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStaticPagesContainer;
        }


#endregion
}

    
}
