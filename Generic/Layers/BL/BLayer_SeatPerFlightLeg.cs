

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class SeatPerFlightLegContainer  : Container<SeatPerFlightLegContainer, SeatPerFlightLeg>{
#region Extra functions

#endregion

        #region Static Method
        
        public static SeatPerFlightLegContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            oSeatPerFlightLegContainer.Add(oSeatPerFlightLegContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oSeatPerFlightLegContainer;
        }

        
        public static SeatPerFlightLegContainer SelectAllSeatPerFlightLegs(int? _WhiteLabelDocId,bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            oSeatPerFlightLegContainer.Add(oSeatPerFlightLegContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oSeatPerFlightLegContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //I_A
        //8_0
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_B
        //8_1
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DocId = _DocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_C
        //8_2
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_E
        //8_4
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_PassengerId(
int _WhiteLabelDocId,
string _PassengerId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.PassengerId = _PassengerId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_F
        //8_5
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_Seat(
int _WhiteLabelDocId,
string _Seat , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.Seat = _Seat;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Seat(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_G
        //8_6
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber(
int _WhiteLabelDocId,
string _FlightNumber , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_H
        //8_7
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_PassengerDocId(
int _WhiteLabelDocId,
int _PassengerDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_J
        //8_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightLegDocId(
int _WhiteLabelDocId,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_A_B
        //8_0_1
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated; 
 oSeatPerFlightLeg.DocId = _DocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_A_C
        //8_0_2
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_A_E
        //8_0_4
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PassengerId(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _PassengerId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated; 
 oSeatPerFlightLeg.PassengerId = _PassengerId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassengerId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_A_F
        //8_0_5
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Seat(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Seat , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated; 
 oSeatPerFlightLeg.Seat = _Seat;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Seat(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Seat));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_A_G
        //8_0_6
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FlightNumber(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FlightNumber , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightNumber(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_A_H
        //8_0_7
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PassengerDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _PassengerDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassengerDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_A_J
        //8_0_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FlightLegDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DateCreated = _DateCreated; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_B_C
        //8_1_2
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DocId = _DocId; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_B_E
        //8_1_4
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_PassengerId(
int _WhiteLabelDocId,
int _DocId,
string _PassengerId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DocId = _DocId; 
 oSeatPerFlightLeg.PassengerId = _PassengerId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassengerId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PassengerId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_B_F
        //8_1_5
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_Seat(
int _WhiteLabelDocId,
int _DocId,
string _Seat , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DocId = _DocId; 
 oSeatPerFlightLeg.Seat = _Seat;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Seat(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Seat));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_B_G
        //8_1_6
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_FlightNumber(
int _WhiteLabelDocId,
int _DocId,
string _FlightNumber , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DocId = _DocId; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightNumber(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_B_H
        //8_1_7
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_PassengerDocId(
int _WhiteLabelDocId,
int _DocId,
int _PassengerDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DocId = _DocId; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassengerDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_B_J
        //8_1_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_DocId_FlightLegDocId(
int _WhiteLabelDocId,
int _DocId,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.DocId = _DocId; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_C_E
        //8_2_4
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerId(
int _WhiteLabelDocId,
bool _IsDeleted,
string _PassengerId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted; 
 oSeatPerFlightLeg.PassengerId = _PassengerId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_C_F
        //8_2_5
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Seat(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Seat , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted; 
 oSeatPerFlightLeg.Seat = _Seat;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Seat(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Seat));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_C_G
        //8_2_6
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FlightNumber(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FlightNumber , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightNumber(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_C_H
        //8_2_7
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _PassengerDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassengerDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_C_J
        //8_2_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FlightLegDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.IsDeleted = _IsDeleted; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_E_F
        //8_4_5
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_PassengerId_Seat(
int _WhiteLabelDocId,
string _PassengerId,
string _Seat , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.PassengerId = _PassengerId; 
 oSeatPerFlightLeg.Seat = _Seat;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_Seat(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_Seat));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_E_G
        //8_4_6
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_PassengerId_FlightNumber(
int _WhiteLabelDocId,
string _PassengerId,
string _FlightNumber , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.PassengerId = _PassengerId; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_FlightNumber(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_E_H
        //8_4_7
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_PassengerId_PassengerDocId(
int _WhiteLabelDocId,
string _PassengerId,
int _PassengerDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.PassengerId = _PassengerId; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_PassengerDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_E_J
        //8_4_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_PassengerId_FlightLegDocId(
int _WhiteLabelDocId,
string _PassengerId,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.PassengerId = _PassengerId; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerId_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_F_G
        //8_5_6
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_Seat_FlightNumber(
int _WhiteLabelDocId,
string _Seat,
string _FlightNumber , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.Seat = _Seat; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Seat_FlightNumber(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_FlightNumber));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_F_H
        //8_5_7
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_Seat_PassengerDocId(
int _WhiteLabelDocId,
string _Seat,
int _PassengerDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.Seat = _Seat; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Seat_PassengerDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_F_J
        //8_5_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_Seat_FlightLegDocId(
int _WhiteLabelDocId,
string _Seat,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.Seat = _Seat; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_Seat_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_G_H
        //8_6_7
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_PassengerDocId(
int _WhiteLabelDocId,
string _FlightNumber,
int _PassengerDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_PassengerDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_PassengerDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_G_J
        //8_6_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_FlightNumber_FlightLegDocId(
int _WhiteLabelDocId,
string _FlightNumber,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.FlightNumber = _FlightNumber; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }



        //I_H_J
        //8_7_9
        public static SeatPerFlightLegContainer SelectByKeysView_WhiteLabelDocId_PassengerDocId_FlightLegDocId(
int _WhiteLabelDocId,
int _PassengerDocId,
int _FlightLegDocId , bool? isActive)
        {
            SeatPerFlightLegContainer oSeatPerFlightLegContainer = new SeatPerFlightLegContainer();
            SeatPerFlightLeg oSeatPerFlightLeg = new SeatPerFlightLeg();
            #region Params
            
 oSeatPerFlightLeg.WhiteLabelDocId = _WhiteLabelDocId; 
 oSeatPerFlightLeg.PassengerDocId = _PassengerDocId; 
 oSeatPerFlightLeg.FlightLegDocId = _FlightLegDocId;
            #endregion 
            oSeatPerFlightLegContainer.Add(SelectData(SeatPerFlightLeg.GetParamsForSelectByKeysView_WhiteLabelDocId_PassengerDocId_FlightLegDocId(oSeatPerFlightLeg), TBNames_SeatPerFlightLeg.PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerDocId_FlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oSeatPerFlightLegContainer = oSeatPerFlightLegContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oSeatPerFlightLegContainer;
        }


#endregion
}

    
}
