

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class PassengerContainer  : Container<PassengerContainer, Passenger>{
#region Extra functions

#endregion

        #region Static Method
        
        public static PassengerContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            oPassengerContainer.Add(oPassengerContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oPassengerContainer;
        }

        
        public static PassengerContainer SelectAllPassengers(int? _WhiteLabelDocId,bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            oPassengerContainer.Add(oPassengerContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oPassengerContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //U_A
        //20_0
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B
        //20_1
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C
        //20_2
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E
        //20_4
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice(
int _WhiteLabelDocId,
Double _NetPrice , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F
        //20_5
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName(
int _WhiteLabelDocId,
string _FirstName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G
        //20_6
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName(
int _WhiteLabelDocId,
string _LastName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H
        //20_7
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth(
int _WhiteLabelDocId,
DateTime _DateOfBirth , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I
        //20_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender(
int _WhiteLabelDocId,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J
        //20_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title(
int _WhiteLabelDocId,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K
        //20_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber(
int _WhiteLabelDocId,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L
        //20_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M
        //20_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry(
int _WhiteLabelDocId,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_N
        //20_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeOutward(
int _WhiteLabelDocId,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_O
        //20_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeReturn(
int _WhiteLabelDocId,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_P
        //20_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceOutward(
int _WhiteLabelDocId,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_Q
        //20_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceReturn(
int _WhiteLabelDocId,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_R
        //20_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PriceMarkUp(
int _WhiteLabelDocId,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_S
        //20_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_T
        //20_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId(
int _WhiteLabelDocId,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_B
        //20_0_1
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.DocId = _DocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_C
        //20_0_2
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.IsDeleted = _IsDeleted;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_E
        //20_0_4
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_NetPrice(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _NetPrice , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.NetPrice = _NetPrice;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_NetPrice(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_NetPrice));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_F
        //20_0_5
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _FirstName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.FirstName = _FirstName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FirstName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_G
        //20_0_6
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LastName(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LastName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.LastName = _LastName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LastName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_H
        //20_0_7
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DateOfBirth(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _DateOfBirth , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.DateOfBirth = _DateOfBirth;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DateOfBirth(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_DateOfBirth));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_I
        //20_0_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Gender(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_J
        //20_0_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Title(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_K
        //20_0_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PassportNumber(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_L
        //20_0_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PassportExpiryDate(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_M
        //20_0_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PassportIssueCountry(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_N
        //20_0_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_CheckInTypeOutward(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_O
        //20_0_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_CheckInTypeReturn(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_P
        //20_0_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_CheckInPriceOutward(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_Q
        //20_0_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_CheckInPriceReturn(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_R
        //20_0_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_PriceMarkUp(
int _WhiteLabelDocId,
DateTime _DateCreated,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_S
        //20_0_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_A_T
        //20_0_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateCreated = _DateCreated; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_C
        //20_1_2
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.IsDeleted = _IsDeleted;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_E
        //20_1_4
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_NetPrice(
int _WhiteLabelDocId,
int _DocId,
Double _NetPrice , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.NetPrice = _NetPrice;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_NetPrice(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_NetPrice));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_F
        //20_1_5
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_FirstName(
int _WhiteLabelDocId,
int _DocId,
string _FirstName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.FirstName = _FirstName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FirstName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_G
        //20_1_6
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_LastName(
int _WhiteLabelDocId,
int _DocId,
string _LastName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.LastName = _LastName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LastName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_H
        //20_1_7
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_DateOfBirth(
int _WhiteLabelDocId,
int _DocId,
DateTime _DateOfBirth , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.DateOfBirth = _DateOfBirth;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DateOfBirth(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_DateOfBirth));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_I
        //20_1_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_Gender(
int _WhiteLabelDocId,
int _DocId,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_J
        //20_1_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_Title(
int _WhiteLabelDocId,
int _DocId,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_K
        //20_1_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_PassportNumber(
int _WhiteLabelDocId,
int _DocId,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_L
        //20_1_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_PassportExpiryDate(
int _WhiteLabelDocId,
int _DocId,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_M
        //20_1_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_PassportIssueCountry(
int _WhiteLabelDocId,
int _DocId,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_N
        //20_1_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_CheckInTypeOutward(
int _WhiteLabelDocId,
int _DocId,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_O
        //20_1_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_CheckInTypeReturn(
int _WhiteLabelDocId,
int _DocId,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_P
        //20_1_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_CheckInPriceOutward(
int _WhiteLabelDocId,
int _DocId,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_Q
        //20_1_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_CheckInPriceReturn(
int _WhiteLabelDocId,
int _DocId,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_R
        //20_1_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_PriceMarkUp(
int _WhiteLabelDocId,
int _DocId,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_S
        //20_1_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
int _DocId,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_B_T
        //20_1_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(
int _WhiteLabelDocId,
int _DocId,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DocId = _DocId; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_E
        //20_2_4
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_NetPrice(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _NetPrice , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.NetPrice = _NetPrice;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_NetPrice(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_NetPrice));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_F
        //20_2_5
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _FirstName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.FirstName = _FirstName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FirstName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_G
        //20_2_6
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LastName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.LastName = _LastName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LastName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_H
        //20_2_7
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_DateOfBirth(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _DateOfBirth , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.DateOfBirth = _DateOfBirth;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DateOfBirth(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_DateOfBirth));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_I
        //20_2_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Gender(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_J
        //20_2_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Title(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_K
        //20_2_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PassportNumber(
int _WhiteLabelDocId,
bool _IsDeleted,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_L
        //20_2_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PassportExpiryDate(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_M
        //20_2_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PassportIssueCountry(
int _WhiteLabelDocId,
bool _IsDeleted,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_N
        //20_2_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInTypeOutward(
int _WhiteLabelDocId,
bool _IsDeleted,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_O
        //20_2_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInTypeReturn(
int _WhiteLabelDocId,
bool _IsDeleted,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_P
        //20_2_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInPriceOutward(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_Q
        //20_2_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInPriceReturn(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_R
        //20_2_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_PriceMarkUp(
int _WhiteLabelDocId,
bool _IsDeleted,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_S
        //20_2_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_C_T
        //20_2_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.IsDeleted = _IsDeleted; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_F
        //20_4_5
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_FirstName(
int _WhiteLabelDocId,
Double _NetPrice,
string _FirstName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.FirstName = _FirstName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_FirstName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_FirstName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_G
        //20_4_6
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_LastName(
int _WhiteLabelDocId,
Double _NetPrice,
string _LastName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.LastName = _LastName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_LastName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_H
        //20_4_7
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_DateOfBirth(
int _WhiteLabelDocId,
Double _NetPrice,
DateTime _DateOfBirth , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.DateOfBirth = _DateOfBirth;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_DateOfBirth(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_DateOfBirth));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_I
        //20_4_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_Gender(
int _WhiteLabelDocId,
Double _NetPrice,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_J
        //20_4_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_Title(
int _WhiteLabelDocId,
Double _NetPrice,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_K
        //20_4_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_PassportNumber(
int _WhiteLabelDocId,
Double _NetPrice,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_L
        //20_4_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_PassportExpiryDate(
int _WhiteLabelDocId,
Double _NetPrice,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_M
        //20_4_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_PassportIssueCountry(
int _WhiteLabelDocId,
Double _NetPrice,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_N
        //20_4_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_CheckInTypeOutward(
int _WhiteLabelDocId,
Double _NetPrice,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_O
        //20_4_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_CheckInTypeReturn(
int _WhiteLabelDocId,
Double _NetPrice,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_P
        //20_4_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_CheckInPriceOutward(
int _WhiteLabelDocId,
Double _NetPrice,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_Q
        //20_4_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_CheckInPriceReturn(
int _WhiteLabelDocId,
Double _NetPrice,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_R
        //20_4_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_PriceMarkUp(
int _WhiteLabelDocId,
Double _NetPrice,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_S
        //20_4_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
Double _NetPrice,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_E_T
        //20_4_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_NetPrice_OrdersDocId(
int _WhiteLabelDocId,
Double _NetPrice,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.NetPrice = _NetPrice; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_NetPrice_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_NetPrice_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_G
        //20_5_6
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_LastName(
int _WhiteLabelDocId,
string _FirstName,
string _LastName , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.LastName = _LastName;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_LastName(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_LastName));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_H
        //20_5_7
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_DateOfBirth(
int _WhiteLabelDocId,
string _FirstName,
DateTime _DateOfBirth , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.DateOfBirth = _DateOfBirth;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_DateOfBirth(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_DateOfBirth));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_I
        //20_5_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_Gender(
int _WhiteLabelDocId,
string _FirstName,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_J
        //20_5_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_Title(
int _WhiteLabelDocId,
string _FirstName,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_K
        //20_5_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_PassportNumber(
int _WhiteLabelDocId,
string _FirstName,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_L
        //20_5_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_PassportExpiryDate(
int _WhiteLabelDocId,
string _FirstName,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_M
        //20_5_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_PassportIssueCountry(
int _WhiteLabelDocId,
string _FirstName,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_N
        //20_5_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_CheckInTypeOutward(
int _WhiteLabelDocId,
string _FirstName,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_O
        //20_5_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_CheckInTypeReturn(
int _WhiteLabelDocId,
string _FirstName,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_P
        //20_5_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_CheckInPriceOutward(
int _WhiteLabelDocId,
string _FirstName,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_Q
        //20_5_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_CheckInPriceReturn(
int _WhiteLabelDocId,
string _FirstName,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_R
        //20_5_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_PriceMarkUp(
int _WhiteLabelDocId,
string _FirstName,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_S
        //20_5_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _FirstName,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_F_T
        //20_5_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_FirstName_OrdersDocId(
int _WhiteLabelDocId,
string _FirstName,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.FirstName = _FirstName; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_FirstName_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_FirstName_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_H
        //20_6_7
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_DateOfBirth(
int _WhiteLabelDocId,
string _LastName,
DateTime _DateOfBirth , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.DateOfBirth = _DateOfBirth;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_DateOfBirth(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_DateOfBirth));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_I
        //20_6_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_Gender(
int _WhiteLabelDocId,
string _LastName,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_J
        //20_6_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_Title(
int _WhiteLabelDocId,
string _LastName,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_K
        //20_6_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_PassportNumber(
int _WhiteLabelDocId,
string _LastName,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_L
        //20_6_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_PassportExpiryDate(
int _WhiteLabelDocId,
string _LastName,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_M
        //20_6_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_PassportIssueCountry(
int _WhiteLabelDocId,
string _LastName,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_N
        //20_6_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_CheckInTypeOutward(
int _WhiteLabelDocId,
string _LastName,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_O
        //20_6_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_CheckInTypeReturn(
int _WhiteLabelDocId,
string _LastName,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_P
        //20_6_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_CheckInPriceOutward(
int _WhiteLabelDocId,
string _LastName,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_Q
        //20_6_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_CheckInPriceReturn(
int _WhiteLabelDocId,
string _LastName,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_R
        //20_6_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_PriceMarkUp(
int _WhiteLabelDocId,
string _LastName,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_S
        //20_6_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _LastName,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_G_T
        //20_6_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_LastName_OrdersDocId(
int _WhiteLabelDocId,
string _LastName,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.LastName = _LastName; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_LastName_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_LastName_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_I
        //20_7_8
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_Gender(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
string _Gender , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.Gender = _Gender;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_Gender(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_Gender));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_J
        //20_7_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_Title(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_K
        //20_7_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportNumber(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_L
        //20_7_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportExpiryDate(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_M
        //20_7_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportIssueCountry(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_N
        //20_7_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInTypeOutward(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_O
        //20_7_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInTypeReturn(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_P
        //20_7_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInPriceOutward(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_Q
        //20_7_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInPriceReturn(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_R
        //20_7_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_PriceMarkUp(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_S
        //20_7_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_H_T
        //20_7_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_DateOfBirth_OrdersDocId(
int _WhiteLabelDocId,
DateTime _DateOfBirth,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.DateOfBirth = _DateOfBirth; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBirth_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_DateOfBirth_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_J
        //20_8_9
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_Title(
int _WhiteLabelDocId,
string _Gender,
string _Title , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.Title = _Title;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_Title(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_Title));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_K
        //20_8_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_PassportNumber(
int _WhiteLabelDocId,
string _Gender,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_L
        //20_8_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_PassportExpiryDate(
int _WhiteLabelDocId,
string _Gender,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_M
        //20_8_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_PassportIssueCountry(
int _WhiteLabelDocId,
string _Gender,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_N
        //20_8_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_CheckInTypeOutward(
int _WhiteLabelDocId,
string _Gender,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_O
        //20_8_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_CheckInTypeReturn(
int _WhiteLabelDocId,
string _Gender,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_P
        //20_8_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_CheckInPriceOutward(
int _WhiteLabelDocId,
string _Gender,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_Q
        //20_8_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_CheckInPriceReturn(
int _WhiteLabelDocId,
string _Gender,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_R
        //20_8_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_PriceMarkUp(
int _WhiteLabelDocId,
string _Gender,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_S
        //20_8_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _Gender,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_I_T
        //20_8_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Gender_OrdersDocId(
int _WhiteLabelDocId,
string _Gender,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Gender = _Gender; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Gender_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Gender_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_K
        //20_9_10
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_PassportNumber(
int _WhiteLabelDocId,
string _Title,
string _PassportNumber , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.PassportNumber = _PassportNumber;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PassportNumber(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportNumber));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_L
        //20_9_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_PassportExpiryDate(
int _WhiteLabelDocId,
string _Title,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_M
        //20_9_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_PassportIssueCountry(
int _WhiteLabelDocId,
string _Title,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_N
        //20_9_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_CheckInTypeOutward(
int _WhiteLabelDocId,
string _Title,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_O
        //20_9_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_CheckInTypeReturn(
int _WhiteLabelDocId,
string _Title,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_P
        //20_9_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_CheckInPriceOutward(
int _WhiteLabelDocId,
string _Title,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_Q
        //20_9_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_CheckInPriceReturn(
int _WhiteLabelDocId,
string _Title,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_R
        //20_9_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_PriceMarkUp(
int _WhiteLabelDocId,
string _Title,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_S
        //20_9_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _Title,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_J_T
        //20_9_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_Title_OrdersDocId(
int _WhiteLabelDocId,
string _Title,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.Title = _Title; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_Title_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_Title_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_L
        //20_10_11
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_PassportExpiryDate(
int _WhiteLabelDocId,
string _PassportNumber,
DateTime _PassportExpiryDate , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_PassportExpiryDate(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PassportExpiryDate));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_M
        //20_10_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_PassportIssueCountry(
int _WhiteLabelDocId,
string _PassportNumber,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_N
        //20_10_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInTypeOutward(
int _WhiteLabelDocId,
string _PassportNumber,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_O
        //20_10_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInTypeReturn(
int _WhiteLabelDocId,
string _PassportNumber,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_P
        //20_10_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInPriceOutward(
int _WhiteLabelDocId,
string _PassportNumber,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_Q
        //20_10_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInPriceReturn(
int _WhiteLabelDocId,
string _PassportNumber,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_R
        //20_10_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_PriceMarkUp(
int _WhiteLabelDocId,
string _PassportNumber,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_S
        //20_10_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _PassportNumber,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_K_T
        //20_10_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportNumber_OrdersDocId(
int _WhiteLabelDocId,
string _PassportNumber,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportNumber = _PassportNumber; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportNumber_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportNumber_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_M
        //20_11_12
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_PassportIssueCountry(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
string _PassportIssueCountry , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_PassportIssueCountry(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_PassportIssueCountry));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_N
        //20_11_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInTypeOutward(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_O
        //20_11_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInTypeReturn(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_P
        //20_11_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInPriceOutward(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_Q
        //20_11_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInPriceReturn(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_R
        //20_11_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_PriceMarkUp(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_S
        //20_11_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_L_T
        //20_11_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportExpiryDate_OrdersDocId(
int _WhiteLabelDocId,
DateTime _PassportExpiryDate,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportExpiryDate = _PassportExpiryDate; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportExpiryDate_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportExpiryDate_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M_N
        //20_12_13
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInTypeOutward(
int _WhiteLabelDocId,
string _PassportIssueCountry,
string _CheckInTypeOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInTypeOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInTypeOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M_O
        //20_12_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInTypeReturn(
int _WhiteLabelDocId,
string _PassportIssueCountry,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M_P
        //20_12_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInPriceOutward(
int _WhiteLabelDocId,
string _PassportIssueCountry,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M_Q
        //20_12_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInPriceReturn(
int _WhiteLabelDocId,
string _PassportIssueCountry,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M_R
        //20_12_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry_PriceMarkUp(
int _WhiteLabelDocId,
string _PassportIssueCountry,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M_S
        //20_12_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _PassportIssueCountry,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_M_T
        //20_12_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PassportIssueCountry_OrdersDocId(
int _WhiteLabelDocId,
string _PassportIssueCountry,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PassportIssueCountry = _PassportIssueCountry; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PassportIssueCountry_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PassportIssueCountry_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_N_O
        //20_13_14
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInTypeReturn(
int _WhiteLabelDocId,
string _CheckInTypeOutward,
string _CheckInTypeReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInTypeReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInTypeReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_N_P
        //20_13_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceOutward(
int _WhiteLabelDocId,
string _CheckInTypeOutward,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_N_Q
        //20_13_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceReturn(
int _WhiteLabelDocId,
string _CheckInTypeOutward,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_N_R
        //20_13_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_PriceMarkUp(
int _WhiteLabelDocId,
string _CheckInTypeOutward,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_N_S
        //20_13_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _CheckInTypeOutward,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_N_T
        //20_13_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_OrdersDocId(
int _WhiteLabelDocId,
string _CheckInTypeOutward,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeOutward = _CheckInTypeOutward; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeOutward_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeOutward_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_O_P
        //20_14_15
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceOutward(
int _WhiteLabelDocId,
string _CheckInTypeReturn,
Double _CheckInPriceOutward , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceOutward(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceOutward));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_O_Q
        //20_14_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceReturn(
int _WhiteLabelDocId,
string _CheckInTypeReturn,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_O_R
        //20_14_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_PriceMarkUp(
int _WhiteLabelDocId,
string _CheckInTypeReturn,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_O_S
        //20_14_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
string _CheckInTypeReturn,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_O_T
        //20_14_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_OrdersDocId(
int _WhiteLabelDocId,
string _CheckInTypeReturn,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInTypeReturn = _CheckInTypeReturn; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInTypeReturn_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInTypeReturn_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_P_Q
        //20_15_16
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_CheckInPriceReturn(
int _WhiteLabelDocId,
Double _CheckInPriceOutward,
Double _CheckInPriceReturn , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_CheckInPriceReturn(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_CheckInPriceReturn));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_P_R
        //20_15_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_PriceMarkUp(
int _WhiteLabelDocId,
Double _CheckInPriceOutward,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_P_S
        //20_15_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
Double _CheckInPriceOutward,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_P_T
        //20_15_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_OrdersDocId(
int _WhiteLabelDocId,
Double _CheckInPriceOutward,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceOutward = _CheckInPriceOutward; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceOutward_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceOutward_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_Q_R
        //20_16_17
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_PriceMarkUp(
int _WhiteLabelDocId,
Double _CheckInPriceReturn,
Double _PriceMarkUp , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn; 
 oPassenger.PriceMarkUp = _PriceMarkUp;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_PriceMarkUp(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_PriceMarkUp));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_Q_S
        //20_16_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
Double _CheckInPriceReturn,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_Q_T
        //20_16_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_OrdersDocId(
int _WhiteLabelDocId,
Double _CheckInPriceReturn,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.CheckInPriceReturn = _CheckInPriceReturn; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_CheckInPriceReturn_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_CheckInPriceReturn_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_R_S
        //20_17_18
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PriceMarkUp_SeatPerFlightLegDocId(
int _WhiteLabelDocId,
Double _PriceMarkUp,
int _SeatPerFlightLegDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PriceMarkUp = _PriceMarkUp; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_SeatPerFlightLegDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp_SeatPerFlightLegDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_R_T
        //20_17_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_PriceMarkUp_OrdersDocId(
int _WhiteLabelDocId,
Double _PriceMarkUp,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.PriceMarkUp = _PriceMarkUp; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_PriceMarkUp_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }



        //U_S_T
        //20_18_19
        public static PassengerContainer SelectByKeysView_WhiteLabelDocId_SeatPerFlightLegDocId_OrdersDocId(
int _WhiteLabelDocId,
int _SeatPerFlightLegDocId,
int _OrdersDocId , bool? isActive)
        {
            PassengerContainer oPassengerContainer = new PassengerContainer();
            Passenger oPassenger = new Passenger();
            #region Params
            
 oPassenger.WhiteLabelDocId = _WhiteLabelDocId; 
 oPassenger.SeatPerFlightLegDocId = _SeatPerFlightLegDocId; 
 oPassenger.OrdersDocId = _OrdersDocId;
            #endregion 
            oPassengerContainer.Add(SelectData(Passenger.GetParamsForSelectByKeysView_WhiteLabelDocId_SeatPerFlightLegDocId_OrdersDocId(oPassenger), TBNames_Passenger.PROC_Select_Passenger_By_Keys_View_WhiteLabelDocId_SeatPerFlightLegDocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oPassengerContainer = oPassengerContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oPassengerContainer;
        }


#endregion
}

    
}
