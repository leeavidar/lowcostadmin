

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class CurrencyContainer  : Container<CurrencyContainer, Currency>{
#region Extra functions

#endregion

        #region Static Method
        
        public static CurrencyContainer SelectByID(int doc_id,bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            oCurrencyContainer.Add(oCurrencyContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oCurrencyContainer;
        }

        
        public static CurrencyContainer SelectAllCurrencys(bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            oCurrencyContainer.Add(oCurrencyContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oCurrencyContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static CurrencyContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DateCreated = _DateCreated;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DateCreated(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //B
        //1
        public static CurrencyContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DocId = _DocId;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DocId(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //C
        //2
        public static CurrencyContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.IsDeleted = _IsDeleted;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_IsDeleted(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //E
        //4
        public static CurrencyContainer SelectByKeysView_Name(
string _Name , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.Name = _Name;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_Name(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //F
        //5
        public static CurrencyContainer SelectByKeysView_Code(
string _Code , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.Code = _Code;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_Code(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_Code));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //G
        //6
        public static CurrencyContainer SelectByKeysView_Sign(
string _Sign , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.Sign = _Sign;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_Sign(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_Sign));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //A_B
        //0_1
        public static CurrencyContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DateCreated = _DateCreated; 
 oCurrency.DocId = _DocId;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DateCreated_DocId(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //A_C
        //0_2
        public static CurrencyContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DateCreated = _DateCreated; 
 oCurrency.IsDeleted = _IsDeleted;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //A_E
        //0_4
        public static CurrencyContainer SelectByKeysView_DateCreated_Name(
DateTime _DateCreated,
string _Name , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DateCreated = _DateCreated; 
 oCurrency.Name = _Name;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DateCreated_Name(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DateCreated_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //A_F
        //0_5
        public static CurrencyContainer SelectByKeysView_DateCreated_Code(
DateTime _DateCreated,
string _Code , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DateCreated = _DateCreated; 
 oCurrency.Code = _Code;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DateCreated_Code(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DateCreated_Code));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //A_G
        //0_6
        public static CurrencyContainer SelectByKeysView_DateCreated_Sign(
DateTime _DateCreated,
string _Sign , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DateCreated = _DateCreated; 
 oCurrency.Sign = _Sign;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DateCreated_Sign(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DateCreated_Sign));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //B_C
        //1_2
        public static CurrencyContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DocId = _DocId; 
 oCurrency.IsDeleted = _IsDeleted;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DocId_IsDeleted(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //B_E
        //1_4
        public static CurrencyContainer SelectByKeysView_DocId_Name(
int _DocId,
string _Name , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DocId = _DocId; 
 oCurrency.Name = _Name;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DocId_Name(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DocId_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //B_F
        //1_5
        public static CurrencyContainer SelectByKeysView_DocId_Code(
int _DocId,
string _Code , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DocId = _DocId; 
 oCurrency.Code = _Code;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DocId_Code(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DocId_Code));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //B_G
        //1_6
        public static CurrencyContainer SelectByKeysView_DocId_Sign(
int _DocId,
string _Sign , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.DocId = _DocId; 
 oCurrency.Sign = _Sign;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_DocId_Sign(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_DocId_Sign));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //C_E
        //2_4
        public static CurrencyContainer SelectByKeysView_IsDeleted_Name(
bool _IsDeleted,
string _Name , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.IsDeleted = _IsDeleted; 
 oCurrency.Name = _Name;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_IsDeleted_Name(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_IsDeleted_Name));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //C_F
        //2_5
        public static CurrencyContainer SelectByKeysView_IsDeleted_Code(
bool _IsDeleted,
string _Code , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.IsDeleted = _IsDeleted; 
 oCurrency.Code = _Code;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_IsDeleted_Code(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_IsDeleted_Code));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //C_G
        //2_6
        public static CurrencyContainer SelectByKeysView_IsDeleted_Sign(
bool _IsDeleted,
string _Sign , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.IsDeleted = _IsDeleted; 
 oCurrency.Sign = _Sign;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_IsDeleted_Sign(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_IsDeleted_Sign));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //E_F
        //4_5
        public static CurrencyContainer SelectByKeysView_Name_Code(
string _Name,
string _Code , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.Name = _Name; 
 oCurrency.Code = _Code;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_Name_Code(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_Name_Code));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //E_G
        //4_6
        public static CurrencyContainer SelectByKeysView_Name_Sign(
string _Name,
string _Sign , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.Name = _Name; 
 oCurrency.Sign = _Sign;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_Name_Sign(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_Name_Sign));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }



        //F_G
        //5_6
        public static CurrencyContainer SelectByKeysView_Code_Sign(
string _Code,
string _Sign , bool? isActive)
        {
            CurrencyContainer oCurrencyContainer = new CurrencyContainer();
            Currency oCurrency = new Currency();
            #region Params
            
 oCurrency.Code = _Code; 
 oCurrency.Sign = _Sign;
            #endregion 
            oCurrencyContainer.Add(SelectData(Currency.GetParamsForSelectByKeysView_Code_Sign(oCurrency), TBNames_Currency.PROC_Select_Currency_By_Keys_View_Code_Sign));
            #region ExtraFilters
            
if(isActive != null){
                oCurrencyContainer = oCurrencyContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oCurrencyContainer;
        }


#endregion
}

    
}
