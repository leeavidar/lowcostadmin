

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class StampsContainer  : Container<StampsContainer, Stamps>{
#region Extra functions

#endregion

        #region Static Method
        
        public static StampsContainer SelectByID(int doc_id,bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            oStampsContainer.Add(oStampsContainer.SelectByID(doc_id));
            #region ExtraFilters
            if(isActive != null){
                                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oStampsContainer;
        }

        
        public static StampsContainer SelectAllStampss(bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            oStampsContainer.Add(oStampsContainer.SelectAll());
            #region ExtraFilters
            if(isActive != null){
                                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oStampsContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //A
        //0
        public static StampsContainer SelectByKeysView_DateCreated(
DateTime _DateCreated , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DateCreated = _DateCreated;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DateCreated(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //B
        //1
        public static StampsContainer SelectByKeysView_DocId(
int _DocId , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DocId = _DocId;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DocId(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //C
        //2
        public static StampsContainer SelectByKeysView_IsDeleted(
bool _IsDeleted , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.IsDeleted = _IsDeleted;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_IsDeleted(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //E
        //4
        public static StampsContainer SelectByKeysView_Text(
string _Text , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.Text = _Text;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_Text(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStampsContainer = oStampsContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStampsContainer;
        }



        //F
        //5
        public static StampsContainer SelectByKeysView_Type(
int _Type , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.Type = _Type;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_Type(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //A_B
        //0_1
        public static StampsContainer SelectByKeysView_DateCreated_DocId(
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DateCreated = _DateCreated; 
 oStamps.DocId = _DocId;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DateCreated_DocId(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //A_C
        //0_2
        public static StampsContainer SelectByKeysView_DateCreated_IsDeleted(
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DateCreated = _DateCreated; 
 oStamps.IsDeleted = _IsDeleted;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DateCreated_IsDeleted(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //A_E
        //0_4
        public static StampsContainer SelectByKeysView_DateCreated_Text(
DateTime _DateCreated,
string _Text , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DateCreated = _DateCreated; 
 oStamps.Text = _Text;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DateCreated_Text(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DateCreated_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStampsContainer = oStampsContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStampsContainer;
        }



        //A_F
        //0_5
        public static StampsContainer SelectByKeysView_DateCreated_Type(
DateTime _DateCreated,
int _Type , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DateCreated = _DateCreated; 
 oStamps.Type = _Type;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DateCreated_Type(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DateCreated_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //B_C
        //1_2
        public static StampsContainer SelectByKeysView_DocId_IsDeleted(
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DocId = _DocId; 
 oStamps.IsDeleted = _IsDeleted;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DocId_IsDeleted(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //B_E
        //1_4
        public static StampsContainer SelectByKeysView_DocId_Text(
int _DocId,
string _Text , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DocId = _DocId; 
 oStamps.Text = _Text;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DocId_Text(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DocId_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStampsContainer = oStampsContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStampsContainer;
        }



        //B_F
        //1_5
        public static StampsContainer SelectByKeysView_DocId_Type(
int _DocId,
int _Type , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.DocId = _DocId; 
 oStamps.Type = _Type;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_DocId_Type(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_DocId_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //C_E
        //2_4
        public static StampsContainer SelectByKeysView_IsDeleted_Text(
bool _IsDeleted,
string _Text , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.IsDeleted = _IsDeleted; 
 oStamps.Text = _Text;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_IsDeleted_Text(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_IsDeleted_Text));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStampsContainer = oStampsContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStampsContainer;
        }



        //C_F
        //2_5
        public static StampsContainer SelectByKeysView_IsDeleted_Type(
bool _IsDeleted,
int _Type , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.IsDeleted = _IsDeleted; 
 oStamps.Type = _Type;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_IsDeleted_Type(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_IsDeleted_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oStampsContainer;
        }



        //E_F
        //4_5
        public static StampsContainer SelectByKeysView_Text_Type(
string _Text,
int _Type , bool? isActive)
        {
            StampsContainer oStampsContainer = new StampsContainer();
            Stamps oStamps = new Stamps();
            #region Params
            
 oStamps.Text = _Text; 
 oStamps.Type = _Type;
            #endregion 
            oStampsContainer.Add(SelectData(Stamps.GetParamsForSelectByKeysView_Text_Type(oStamps), TBNames_Stamps.PROC_Select_Stamps_By_Keys_View_Text_Type));
            #region ExtraFilters
            
if(isActive != null){
                oStampsContainer = oStampsContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
oStampsContainer = oStampsContainer.FindAllContainer(R => string.Equals(R.Text, _Text));
            #endregion
            return oStampsContainer;
        }


#endregion
}

    
}
