

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace BL_LowCost{
    using DL_LowCost;

public partial class MessagesContainer  : Container<MessagesContainer, Messages>{
#region Extra functions

#endregion

        #region Static Method
        
        public static MessagesContainer SelectByID(int doc_id,int? _WhiteLabelDocId,bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            oMessagesContainer.Add(oMessagesContainer.SelectByID(doc_id, _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }
            #endregion
            return oMessagesContainer;
        }

        
        public static MessagesContainer SelectAllMessagess(int? _WhiteLabelDocId,bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            oMessagesContainer.Add(oMessagesContainer.SelectAll( _WhiteLabelDocId ));
            #region ExtraFilters
            if(isActive != null){
                                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive.Equals(isActive));
                    }            
            #endregion
            return oMessagesContainer;
        }

        #endregion


//#REP_HERE
#region Combinations

        //J_A
        //9_0
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated(
int _WhiteLabelDocId,
DateTime _DateCreated , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_B
        //9_1
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DocId(
int _WhiteLabelDocId,
int _DocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DocId = _DocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_C
        //9_2
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted(
int _WhiteLabelDocId,
bool _IsDeleted , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.IsDeleted = _IsDeleted;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_E
        //9_4
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_SentDateTime(
int _WhiteLabelDocId,
DateTime _SentDateTime , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.SentDateTime = _SentDateTime;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_F
        //9_5
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_MessageType(
int _WhiteLabelDocId,
DateTime _MessageType , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.MessageType = _MessageType;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_G
        //9_6
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR(
int _WhiteLabelDocId,
string _LowCostPNR , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.LowCostPNR = _LowCostPNR;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_H
        //9_7
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId(
int _WhiteLabelDocId,
int _OrdersDocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.OrdersDocId = _OrdersDocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_I
        //9_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_Content(
int _WhiteLabelDocId,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_A_B
        //9_0_1
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_DocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _DocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated; 
 oMessages.DocId = _DocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_DocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_A_C
        //9_0_2
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(
int _WhiteLabelDocId,
DateTime _DateCreated,
bool _IsDeleted , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated; 
 oMessages.IsDeleted = _IsDeleted;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_A_E
        //9_0_4
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_SentDateTime(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _SentDateTime , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated; 
 oMessages.SentDateTime = _SentDateTime;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SentDateTime(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_SentDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_A_F
        //9_0_5
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_MessageType(
int _WhiteLabelDocId,
DateTime _DateCreated,
DateTime _MessageType , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated; 
 oMessages.MessageType = _MessageType;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MessageType(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_MessageType));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_A_G
        //9_0_6
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _LowCostPNR , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated; 
 oMessages.LowCostPNR = _LowCostPNR;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_A_H
        //9_0_7
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(
int _WhiteLabelDocId,
DateTime _DateCreated,
int _OrdersDocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated; 
 oMessages.OrdersDocId = _OrdersDocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_A_I
        //9_0_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DateCreated_Content(
int _WhiteLabelDocId,
DateTime _DateCreated,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DateCreated = _DateCreated; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_B_C
        //9_1_2
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(
int _WhiteLabelDocId,
int _DocId,
bool _IsDeleted , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DocId = _DocId; 
 oMessages.IsDeleted = _IsDeleted;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_B_E
        //9_1_4
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DocId_SentDateTime(
int _WhiteLabelDocId,
int _DocId,
DateTime _SentDateTime , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DocId = _DocId; 
 oMessages.SentDateTime = _SentDateTime;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SentDateTime(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_SentDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_B_F
        //9_1_5
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DocId_MessageType(
int _WhiteLabelDocId,
int _DocId,
DateTime _MessageType , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DocId = _DocId; 
 oMessages.MessageType = _MessageType;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MessageType(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_MessageType));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_B_G
        //9_1_6
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(
int _WhiteLabelDocId,
int _DocId,
string _LowCostPNR , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DocId = _DocId; 
 oMessages.LowCostPNR = _LowCostPNR;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_B_H
        //9_1_7
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(
int _WhiteLabelDocId,
int _DocId,
int _OrdersDocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DocId = _DocId; 
 oMessages.OrdersDocId = _OrdersDocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_B_I
        //9_1_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_DocId_Content(
int _WhiteLabelDocId,
int _DocId,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.DocId = _DocId; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_C_E
        //9_2_4
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_SentDateTime(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _SentDateTime , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.IsDeleted = _IsDeleted; 
 oMessages.SentDateTime = _SentDateTime;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SentDateTime(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_SentDateTime));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_C_F
        //9_2_5
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_MessageType(
int _WhiteLabelDocId,
bool _IsDeleted,
DateTime _MessageType , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.IsDeleted = _IsDeleted; 
 oMessages.MessageType = _MessageType;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MessageType(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_MessageType));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_C_G
        //9_2_6
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(
int _WhiteLabelDocId,
bool _IsDeleted,
string _LowCostPNR , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.IsDeleted = _IsDeleted; 
 oMessages.LowCostPNR = _LowCostPNR;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_C_H
        //9_2_7
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(
int _WhiteLabelDocId,
bool _IsDeleted,
int _OrdersDocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.IsDeleted = _IsDeleted; 
 oMessages.OrdersDocId = _OrdersDocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_C_I
        //9_2_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_IsDeleted_Content(
int _WhiteLabelDocId,
bool _IsDeleted,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.IsDeleted = _IsDeleted; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_E_F
        //9_4_5
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_SentDateTime_MessageType(
int _WhiteLabelDocId,
DateTime _SentDateTime,
DateTime _MessageType , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.SentDateTime = _SentDateTime; 
 oMessages.MessageType = _MessageType;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_MessageType(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_MessageType));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_E_G
        //9_4_6
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_SentDateTime_LowCostPNR(
int _WhiteLabelDocId,
DateTime _SentDateTime,
string _LowCostPNR , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.SentDateTime = _SentDateTime; 
 oMessages.LowCostPNR = _LowCostPNR;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_LowCostPNR(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_E_H
        //9_4_7
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_SentDateTime_OrdersDocId(
int _WhiteLabelDocId,
DateTime _SentDateTime,
int _OrdersDocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.SentDateTime = _SentDateTime; 
 oMessages.OrdersDocId = _OrdersDocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_OrdersDocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_E_I
        //9_4_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_SentDateTime_Content(
int _WhiteLabelDocId,
DateTime _SentDateTime,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.SentDateTime = _SentDateTime; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_F_G
        //9_5_6
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_MessageType_LowCostPNR(
int _WhiteLabelDocId,
DateTime _MessageType,
string _LowCostPNR , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.MessageType = _MessageType; 
 oMessages.LowCostPNR = _LowCostPNR;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType_LowCostPNR(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_LowCostPNR));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_F_H
        //9_5_7
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_MessageType_OrdersDocId(
int _WhiteLabelDocId,
DateTime _MessageType,
int _OrdersDocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.MessageType = _MessageType; 
 oMessages.OrdersDocId = _OrdersDocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType_OrdersDocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_F_I
        //9_5_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_MessageType_Content(
int _WhiteLabelDocId,
DateTime _MessageType,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.MessageType = _MessageType; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_G_H
        //9_6_7
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_OrdersDocId(
int _WhiteLabelDocId,
string _LowCostPNR,
int _OrdersDocId , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.LowCostPNR = _LowCostPNR; 
 oMessages.OrdersDocId = _OrdersDocId;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrdersDocId(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrdersDocId));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_G_I
        //9_6_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_LowCostPNR_Content(
int _WhiteLabelDocId,
string _LowCostPNR,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.LowCostPNR = _LowCostPNR; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }



        //J_H_I
        //9_7_8
        public static MessagesContainer SelectByKeysView_WhiteLabelDocId_OrdersDocId_Content(
int _WhiteLabelDocId,
int _OrdersDocId,
string _Content , bool? isActive)
        {
            MessagesContainer oMessagesContainer = new MessagesContainer();
            Messages oMessages = new Messages();
            #region Params
            
 oMessages.WhiteLabelDocId = _WhiteLabelDocId; 
 oMessages.OrdersDocId = _OrdersDocId; 
 oMessages.Content = _Content;
            #endregion 
            oMessagesContainer.Add(SelectData(Messages.GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId_Content(oMessages), TBNames_Messages.PROC_Select_Messages_By_Keys_View_WhiteLabelDocId_OrdersDocId_Content));
            #region ExtraFilters
            
if(isActive != null){
                oMessagesContainer = oMessagesContainer.FindAllContainer(R => R.IsActive_Value.Equals(isActive));
            }

            
            #endregion
            return oMessagesContainer;
        }


#endregion
}

    
}
