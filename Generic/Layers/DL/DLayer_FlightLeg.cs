

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class FlightLeg  : ContainerItem<FlightLeg>{

#region CTOR

    #region Constractor
    static FlightLeg()
    {
        ConvertEvent = FlightLeg.OnConvert;
    }  
    //public KeyValuesContainer<FlightLeg> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public FlightLeg()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.FlightLegKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static FlightLeg OnConvert(DataRow dr)
    {
        int LangId = Translator<FlightLeg>.LangId;            
        FlightLeg oFlightLeg = null;
        if (dr != null)
        {
            oFlightLeg = new FlightLeg();
            #region Create Object
            oFlightLeg.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_FlightLeg.Field_DateCreated]);
             oFlightLeg.DocId = ConvertTo.ConvertToInt(dr[TBNames_FlightLeg.Field_DocId]);
             oFlightLeg.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_FlightLeg.Field_IsDeleted]);
             oFlightLeg.IsActive = ConvertTo.ConvertToBool(dr[TBNames_FlightLeg.Field_IsActive]);
             oFlightLeg.FlightNumber = ConvertTo.ConvertToString(dr[TBNames_FlightLeg.Field_FlightNumber]);
             oFlightLeg.DepartureDateTime = ConvertTo.ConvertToDateTime(dr[TBNames_FlightLeg.Field_DepartureDateTime]);
             oFlightLeg.ArrivalDateTime = ConvertTo.ConvertToDateTime(dr[TBNames_FlightLeg.Field_ArrivalDateTime]);
             oFlightLeg.Status = ConvertTo.ConvertToString(dr[TBNames_FlightLeg.Field_Status]);
             oFlightLeg.Duration = ConvertTo.ConvertToInt(dr[TBNames_FlightLeg.Field_Duration]);
             oFlightLeg.Hops = ConvertTo.ConvertToInt(dr[TBNames_FlightLeg.Field_Hops]);
             oFlightLeg.AirlineIataCode = ConvertTo.ConvertToString(dr[TBNames_FlightLeg.Field_AirlineIataCode]);
             oFlightLeg.OriginIataCode = ConvertTo.ConvertToString(dr[TBNames_FlightLeg.Field_OriginIataCode]);
             oFlightLeg.DestinationIataCode = ConvertTo.ConvertToString(dr[TBNames_FlightLeg.Field_DestinationIataCode]);
 
//FK     KeyOrderFlightLegDocId
            oFlightLeg.OrderFlightLegDocId = ConvertTo.ConvertToInt(dr[TBNames_FlightLeg.Field_OrderFlightLegDocId]);
 
//FK     KeyWhiteLabelDocId
            oFlightLeg.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_FlightLeg.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<FlightLeg>.Translate(oFlightLeg.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oFlightLeg;
    }

    
#endregion

//#REP_HERE 
#region FlightLeg Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyFlightLegDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFlightLegDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyFlightLegIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyFlightLegIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_FlightNumber;
private string _flight_number;
public String FriendlyFlightNumber
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegFlightNumber);   
    }
}
public  string FlightNumber
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyFlightLegFlightNumber));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegFlightNumber, value);
         _flight_number = ConvertToValue.ConvertToString(value);
        isSetOnce_FlightNumber = true;
    }
}


public string FlightNumber_Value
{
    get
    {
        //return _flight_number; //ConvertToValue.ConvertToString(FlightNumber);
        if(isSetOnce_FlightNumber) {return _flight_number;}
        else {return ConvertToValue.ConvertToString(FlightNumber);}
    }
}

public string FlightNumber_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_flight_number).ToString();
               //if(isSetOnce_FlightNumber) {return ConvertToValue.ConvertToString(_flight_number).ToString();}
               //else {return ConvertToValue.ConvertToString(FlightNumber).ToString();}
            ConvertToValue.ConvertToString(FlightNumber).ToString();
    }
}

private bool isSetOnce_DepartureDateTime;
private DateTime _departure_date_time;
public String FriendlyDepartureDateTime
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegDepartureDateTime);   
    }
}
public  DateTime? DepartureDateTime
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyFlightLegDepartureDateTime));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegDepartureDateTime, value);
         _departure_date_time = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DepartureDateTime = true;
    }
}


public DateTime DepartureDateTime_Value
{
    get
    {
        //return _departure_date_time; //ConvertToValue.ConvertToDateTime(DepartureDateTime);
        if(isSetOnce_DepartureDateTime) {return _departure_date_time;}
        else {return ConvertToValue.ConvertToDateTime(DepartureDateTime);}
    }
}

public string DepartureDateTime_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_departure_date_time).ToShortDateString();
               //if(isSetOnce_DepartureDateTime) {return ConvertToValue.ConvertToDateTime(_departure_date_time).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DepartureDateTime).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DepartureDateTime).ToShortDateString();
    }
}

private bool isSetOnce_ArrivalDateTime;
private DateTime _arrival_date_time;
public String FriendlyArrivalDateTime
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegArrivalDateTime);   
    }
}
public  DateTime? ArrivalDateTime
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyFlightLegArrivalDateTime));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegArrivalDateTime, value);
         _arrival_date_time = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_ArrivalDateTime = true;
    }
}


public DateTime ArrivalDateTime_Value
{
    get
    {
        //return _arrival_date_time; //ConvertToValue.ConvertToDateTime(ArrivalDateTime);
        if(isSetOnce_ArrivalDateTime) {return _arrival_date_time;}
        else {return ConvertToValue.ConvertToDateTime(ArrivalDateTime);}
    }
}

public string ArrivalDateTime_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_arrival_date_time).ToShortDateString();
               //if(isSetOnce_ArrivalDateTime) {return ConvertToValue.ConvertToDateTime(_arrival_date_time).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(ArrivalDateTime).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(ArrivalDateTime).ToShortDateString();
    }
}

private bool isSetOnce_Status;
private string _status;
public String FriendlyStatus
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegStatus);   
    }
}
public  string Status
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyFlightLegStatus));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegStatus, value);
         _status = ConvertToValue.ConvertToString(value);
        isSetOnce_Status = true;
    }
}


public string Status_Value
{
    get
    {
        //return _status; //ConvertToValue.ConvertToString(Status);
        if(isSetOnce_Status) {return _status;}
        else {return ConvertToValue.ConvertToString(Status);}
    }
}

public string Status_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_status).ToString();
               //if(isSetOnce_Status) {return ConvertToValue.ConvertToString(_status).ToString();}
               //else {return ConvertToValue.ConvertToString(Status).ToString();}
            ConvertToValue.ConvertToString(Status).ToString();
    }
}

private bool isSetOnce_Duration;
private int _duration;
public String FriendlyDuration
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegDuration);   
    }
}
public  int? Duration
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFlightLegDuration));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegDuration, value);
         _duration = ConvertToValue.ConvertToInt(value);
        isSetOnce_Duration = true;
    }
}


public int Duration_Value
{
    get
    {
        //return _duration; //ConvertToValue.ConvertToInt(Duration);
        if(isSetOnce_Duration) {return _duration;}
        else {return ConvertToValue.ConvertToInt(Duration);}
    }
}

public string Duration_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_duration).ToString();
               //if(isSetOnce_Duration) {return ConvertToValue.ConvertToInt(_duration).ToString();}
               //else {return ConvertToValue.ConvertToInt(Duration).ToString();}
            ConvertToValue.ConvertToInt(Duration).ToString();
    }
}

private bool isSetOnce_Hops;
private int _hops;
public String FriendlyHops
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegHops);   
    }
}
public  int? Hops
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFlightLegHops));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegHops, value);
         _hops = ConvertToValue.ConvertToInt(value);
        isSetOnce_Hops = true;
    }
}


public int Hops_Value
{
    get
    {
        //return _hops; //ConvertToValue.ConvertToInt(Hops);
        if(isSetOnce_Hops) {return _hops;}
        else {return ConvertToValue.ConvertToInt(Hops);}
    }
}

public string Hops_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_hops).ToString();
               //if(isSetOnce_Hops) {return ConvertToValue.ConvertToInt(_hops).ToString();}
               //else {return ConvertToValue.ConvertToInt(Hops).ToString();}
            ConvertToValue.ConvertToInt(Hops).ToString();
    }
}

private bool isSetOnce_AirlineIataCode;
private string _airline_iata_code;
public String FriendlyAirlineIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegAirlineIataCode);   
    }
}
public  string AirlineIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyFlightLegAirlineIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegAirlineIataCode, value);
         _airline_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_AirlineIataCode = true;
    }
}


public string AirlineIataCode_Value
{
    get
    {
        //return _airline_iata_code; //ConvertToValue.ConvertToString(AirlineIataCode);
        if(isSetOnce_AirlineIataCode) {return _airline_iata_code;}
        else {return ConvertToValue.ConvertToString(AirlineIataCode);}
    }
}

public string AirlineIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_airline_iata_code).ToString();
               //if(isSetOnce_AirlineIataCode) {return ConvertToValue.ConvertToString(_airline_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(AirlineIataCode).ToString();}
            ConvertToValue.ConvertToString(AirlineIataCode).ToString();
    }
}

private bool isSetOnce_OriginIataCode;
private string _origin_iata_code;
public String FriendlyOriginIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegOriginIataCode);   
    }
}
public  string OriginIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyFlightLegOriginIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegOriginIataCode, value);
         _origin_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_OriginIataCode = true;
    }
}


public string OriginIataCode_Value
{
    get
    {
        //return _origin_iata_code; //ConvertToValue.ConvertToString(OriginIataCode);
        if(isSetOnce_OriginIataCode) {return _origin_iata_code;}
        else {return ConvertToValue.ConvertToString(OriginIataCode);}
    }
}

public string OriginIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_origin_iata_code).ToString();
               //if(isSetOnce_OriginIataCode) {return ConvertToValue.ConvertToString(_origin_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(OriginIataCode).ToString();}
            ConvertToValue.ConvertToString(OriginIataCode).ToString();
    }
}

private bool isSetOnce_DestinationIataCode;
private string _destination_iata_code;
public String FriendlyDestinationIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegDestinationIataCode);   
    }
}
public  string DestinationIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyFlightLegDestinationIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegDestinationIataCode, value);
         _destination_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_DestinationIataCode = true;
    }
}


public string DestinationIataCode_Value
{
    get
    {
        //return _destination_iata_code; //ConvertToValue.ConvertToString(DestinationIataCode);
        if(isSetOnce_DestinationIataCode) {return _destination_iata_code;}
        else {return ConvertToValue.ConvertToString(DestinationIataCode);}
    }
}

public string DestinationIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_destination_iata_code).ToString();
               //if(isSetOnce_DestinationIataCode) {return ConvertToValue.ConvertToString(_destination_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(DestinationIataCode).ToString();}
            ConvertToValue.ConvertToString(DestinationIataCode).ToString();
    }
}

private bool isSetOnce_OrderFlightLegDocId;
private int _order_flight_leg_doc_id;
public String FriendlyOrderFlightLegDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegDocId);   
    }
}
public  int? OrderFlightLegDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrderFlightLegDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegDocId, value);
         _order_flight_leg_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrderFlightLegDocId = true;
    }
}


public int OrderFlightLegDocId_Value
{
    get
    {
        //return _order_flight_leg_doc_id; //ConvertToValue.ConvertToInt(OrderFlightLegDocId);
        if(isSetOnce_OrderFlightLegDocId) {return _order_flight_leg_doc_id;}
        else {return ConvertToValue.ConvertToInt(OrderFlightLegDocId);}
    }
}

public string OrderFlightLegDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order_flight_leg_doc_id).ToString();
               //if(isSetOnce_OrderFlightLegDocId) {return ConvertToValue.ConvertToInt(_order_flight_leg_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrderFlightLegDocId).ToString();}
            ConvertToValue.ConvertToInt(OrderFlightLegDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //O_A
        //14_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //O_B
        //14_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_C
        //14_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //O_E
        //14_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //O_F
        //14_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_G
        //14_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_H
        //14_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status", ex.Message));

            }
            return paramsSelect;
        }



        //O_I
        //14_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_J
        //14_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_K
        //14_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_L
        //14_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_M
        //14_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_N
        //14_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_B
        //14_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_C
        //14_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_E
        //14_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightNumber(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_F
        //14_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DepartureDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DepartureDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_G
        //14_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ArrivalDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_ArrivalDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_H
        //14_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Status(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Status", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_I
        //14_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_J
        //14_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_K
        //14_0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_L
        //14_0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_M
        //14_0_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_A_N
        //14_0_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DateCreated)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_C
        //14_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_E
        //14_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightNumber(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_F
        //14_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DepartureDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_DepartureDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_G
        //14_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ArrivalDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_ArrivalDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_H
        //14_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Status(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Status", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_I
        //14_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_J
        //14_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_K
        //14_1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_L
        //14_1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_M
        //14_1_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_B_N
        //14_1_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_E
        //14_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightNumber(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightNumber", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_F
        //14_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DepartureDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_DepartureDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_G
        //14_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ArrivalDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_ArrivalDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_H
        //14_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Status(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Status", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_I
        //14_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_J
        //14_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_K
        //14_2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_L
        //14_2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_M
        //14_2_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_C_N
        //14_2_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_F
        //14_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_DepartureDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_DepartureDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_G
        //14_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_ArrivalDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_ArrivalDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_H
        //14_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_Status(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Status", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_I
        //14_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_J
        //14_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_K
        //14_4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_L
        //14_4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_M
        //14_4_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_E_N
        //14_4_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightNumber_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_FlightNumber, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.FlightNumber)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_G
        //14_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_ArrivalDateTime(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_ArrivalDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_H
        //14_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_Status(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Status", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_I
        //14_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_J
        //14_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_K
        //14_5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_L
        //14_5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_M
        //14_5_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_F_N
        //14_5_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DepartureDateTime_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DepartureDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DepartureDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_G_H
        //14_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Status(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Status", ex.Message));

            }
            return paramsSelect;
        }



        //O_G_I
        //14_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_G_J
        //14_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_G_K
        //14_6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_G_L
        //14_6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_G_M
        //14_6_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_G_N
        //14_6_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ArrivalDateTime_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_ArrivalDateTime, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.ArrivalDateTime)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_H_I
        //14_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_Duration(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_Duration", ex.Message));

            }
            return paramsSelect;
        }



        //O_H_J
        //14_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_H_K
        //14_7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_H_L
        //14_7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_H_M
        //14_7_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_H_N
        //14_7_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Status)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_I_J
        //14_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_Hops(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_Hops", ex.Message));

            }
            return paramsSelect;
        }



        //O_I_K
        //14_8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_I_L
        //14_8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_I_M
        //14_8_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_I_N
        //14_8_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Duration_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Duration, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Duration)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_J_K
        //14_9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_AirlineIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_J_L
        //14_9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_J_M
        //14_9_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_J_N
        //14_9_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Hops_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_Hops, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.Hops)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_K_L
        //14_10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OriginIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OriginIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_K_M
        //14_10_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_K_N
        //14_10_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.AirlineIataCode)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_L_M
        //14_11_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OriginIataCode_DestinationIataCode(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //O_L_N
        //14_11_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OriginIataCode_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_OriginIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OriginIataCode)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //O_M_N
        //14_12_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_OrderFlightLegDocId(FlightLeg oFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_FlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_FlightLeg.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.DestinationIataCode)); 
db.AddParameter(TBNames_FlightLeg.PRM_OrderFlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oFlightLeg.OrderFlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.FlightLeg, "Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DestinationIataCode_OrderFlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
