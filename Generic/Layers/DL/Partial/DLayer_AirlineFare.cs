

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;

    public partial class AirlineFare : ContainerItem<AirlineFare>
    {

        #region AirlineFare Properties



        public string AirlineName_UI
        {
            get
            {
                return this.AirlineSingle.Name_UI;
            }
        }

     

        #endregion



    }
}
