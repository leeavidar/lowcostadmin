

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Tip  : ContainerItem<Tip>{

                #region Relations Code
                
                            //Relation From:[Tip] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[HomePage] To:[Tip] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region HomePage Container Object
        public bool IsHomePageNullAble = true;
        private bool _isHomePageContainerInit = false;
        
        private HomePageContainer _home_pageContainer;
        public HomePageContainer HomePages
        {
            get
            {
                if (_home_pageContainer != null)
                {
                    return _home_pageContainer;
                }
                else 
                {
                    if (_isHomePageContainerInit)
                    {
                        if(IsHomePageNullAble) {return null;}
                        else { return  new HomePageContainer();}
                    }
                    else
                    {
                        Init_HomePageContainer(false);
                    }
                }
                return _home_pageContainer;
            }
            set
            {
                 _home_pageContainer = value;
                //if (value == null)
                //{
                //    _home_pageContainer = new HomePageContainer();
                //}
                //else
                //{
                //    if (_home_pageContainer == null)
                //    {
                //        _home_pageContainer = value;
                //    }
                //    else
                //    {
                //        lock (_home_pageContainer)
                //        {
                //            _home_pageContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with HomePageContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_HomePageContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsHomePageNullAble;
            IsHomePageNullAble = true;
            if (isInitAnyway || !_isHomePageContainerInit) //this.HomePages == null)
            {
                this.HomePages = HomePageContainer.SelectByKeysView_WhiteLabelDocId_TipDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isHomePageContainerInit = true;
            }
            IsHomePageNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
