

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Stops  : ContainerItem<Stops>{

                #region Relations Code
                
                            //Relation From:[Stops] To:[FlightLeg] -Type M:1
                            
		//-----M Side----------------------//
		#region FlightLeg object
        private FlightLeg _flight_leg;
        public bool IsFlightLegNullAble = true;
        private bool _isFlightLegSingleInit = false;

        public FlightLeg FlightLegSingle
        {
            get
            {
                if (_flight_leg != null)
                {
                    return _flight_leg;
                }
                else 
                {
                    if (_isFlightLegSingleInit)
                    {
                        if(IsFlightLegNullAble) {return null;}
                        else  {return new FlightLeg();}
                    }
                    else
                    {
                        Init_FlightLeg(false);
                    }
                }
                return _flight_leg;
            }
            set
            {
                _flight_leg = value;
                //if (value == null)
                //{
                //    _flight_leg = new FlightLeg();
                //}
                //else
                //{
                //    if (_flight_leg == null)
                //    {
                //        _flight_leg = value;
                //    }
                //    else
                //    {
                //        lock (_flight_leg)
                //        {
                //            _flight_leg = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with FlightLeg 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_FlightLeg(bool isInitAnyway)
        {
            bool _isNullAble = IsFlightLegNullAble;
            IsFlightLegNullAble = true;
            if (isInitAnyway || !_isFlightLegSingleInit)//FlightLegSingle == null)
            {
                //Select by shared key
                this.FlightLegSingle = FlightLegContainer.SelectByID(this.FlightLegDocId_Value,null,true).Single;
                _isFlightLegSingleInit = true;
            }
            IsFlightLegNullAble = _isNullAble;
        }
        #endregion
        
                        
                #endregion
                
}
}
