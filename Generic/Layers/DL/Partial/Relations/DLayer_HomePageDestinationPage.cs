

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class HomePageDestinationPage  : ContainerItem<HomePageDestinationPage>{

                #region Relations Code
                
                            //Relation From:[HomePageDestinationPage] To:[HomePage] -Type M:1
                            
		//-----M Side----------------------//
		#region HomePage object
        private HomePage _home_page;
        public bool IsHomePageNullAble = true;
        private bool _isHomePageSingleInit = false;

        public HomePage HomePageSingle
        {
            get
            {
                if (_home_page != null)
                {
                    return _home_page;
                }
                else 
                {
                    if (_isHomePageSingleInit)
                    {
                        if(IsHomePageNullAble) {return null;}
                        else  {return new HomePage();}
                    }
                    else
                    {
                        Init_HomePage(false);
                    }
                }
                return _home_page;
            }
            set
            {
                _home_page = value;
                //if (value == null)
                //{
                //    _home_page = new HomePage();
                //}
                //else
                //{
                //    if (_home_page == null)
                //    {
                //        _home_page = value;
                //    }
                //    else
                //    {
                //        lock (_home_page)
                //        {
                //            _home_page = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with HomePage 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_HomePage(bool isInitAnyway)
        {
            bool _isNullAble = IsHomePageNullAble;
            IsHomePageNullAble = true;
            if (isInitAnyway || !_isHomePageSingleInit)//HomePageSingle == null)
            {
                //Select by shared key
                this.HomePageSingle = HomePageContainer.SelectByID(this.HomePageDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isHomePageSingleInit = true;
            }
            IsHomePageNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[HomePageDestinationPage] To:[DestinationPage] -Type M:1
                            
		//-----M Side----------------------//
		#region DestinationPage object
        private DestinationPage _destination_page;
        public bool IsDestinationPageNullAble = true;
        private bool _isDestinationPageSingleInit = false;

        public DestinationPage DestinationPageSingle
        {
            get
            {
                if (_destination_page != null)
                {
                    return _destination_page;
                }
                else 
                {
                    if (_isDestinationPageSingleInit)
                    {
                        if(IsDestinationPageNullAble) {return null;}
                        else  {return new DestinationPage();}
                    }
                    else
                    {
                        Init_DestinationPage(false);
                    }
                }
                return _destination_page;
            }
            set
            {
                _destination_page = value;
                //if (value == null)
                //{
                //    _destination_page = new DestinationPage();
                //}
                //else
                //{
                //    if (_destination_page == null)
                //    {
                //        _destination_page = value;
                //    }
                //    else
                //    {
                //        lock (_destination_page)
                //        {
                //            _destination_page = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with DestinationPage 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_DestinationPage(bool isInitAnyway)
        {
            bool _isNullAble = IsDestinationPageNullAble;
            IsDestinationPageNullAble = true;
            if (isInitAnyway || !_isDestinationPageSingleInit)//DestinationPageSingle == null)
            {
                //Select by shared key
                this.DestinationPageSingle = DestinationPageContainer.SelectByID(this.DestinationPageDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isDestinationPageSingleInit = true;
            }
            IsDestinationPageNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[HomePageDestinationPage] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        
                #endregion
                
}
}
