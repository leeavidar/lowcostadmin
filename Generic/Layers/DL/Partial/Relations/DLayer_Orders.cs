

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;

    public partial class Orders : ContainerItem<Orders>
    {

        #region Relations Code

        //Relation From:[Orders] To:[ContactDetails] -Type 1:1

        //-------1 Side--------------------------------------//
        #region ContactDetails Container Object

        public bool IsContactDetailsNullAble = true;
        private bool _isContactDetailsInit = false;

        private ContactDetails _contact_details;
        public ContactDetails ContactDetails
        {
            get
            {
                if (_contact_details != null)
                {
                    return _contact_details;
                }
                else
                {
                    if (_isContactDetailsInit)
                    {
                        if (IsContactDetailsNullAble) { return null; }
                        else { return new ContactDetails(); }
                    }
                    else
                    {
                        Init_ContactDetails(false);
                    }
                }
                return _contact_details;
            }
            set
            {
                _contact_details = value;
                //if (value == null)
                //{
                //    _contact_detailsContainer = new ContactDetailsContainer();
                //}
                //else
                //{
                //    if (_contact_detailsContainer == null)
                //    {
                //        _contact_detailsContainer = value;
                //    }
                //    else
                //    {
                //        lock (_contact_detailsContainer)
                //        {
                //            _contact_detailsContainer = value;
                //        }
                //
                //    }
                //}
            }
        }

        #endregion




        //Relation From:[Orders] To:[WhiteLabel] -Type M:1

        //-----M Side----------------------//
        #region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if (IsWhiteLabelNullAble) { return null; }
                        else { return new WhiteLabel(); }
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value, true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion



        //Relation From:[Messages] To:[Orders] -Type M:1

        //-------1 Side--------------------------------------//
        #region Messages Container Object
        public bool IsMessagesNullAble = true;
        private bool _isMessagesContainerInit = false;

        private MessagesContainer _messagesContainer;
        public MessagesContainer Messagess
        {
            get
            {
                if (_messagesContainer != null)
                {
                    return _messagesContainer;
                }
                else
                {
                    if (_isMessagesContainerInit)
                    {
                        if (IsMessagesNullAble) { return null; }
                        else { return new MessagesContainer(); }
                    }
                    else
                    {
                        Init_MessagesContainer(false);
                    }
                }
                return _messagesContainer;
            }
            set
            {
                _messagesContainer = value;
                //if (value == null)
                //{
                //    _messagesContainer = new MessagesContainer();
                //}
                //else
                //{
                //    if (_messagesContainer == null)
                //    {
                //        _messagesContainer = value;
                //    }
                //    else
                //    {
                //        lock (_messagesContainer)
                //        {
                //            _messagesContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with MessagesContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_MessagesContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsMessagesNullAble;
            IsMessagesNullAble = true;
            if (isInitAnyway || !_isMessagesContainerInit) //this.Messagess == null)
            {
                this.Messagess = MessagesContainer.SelectByKeysView_WhiteLabelDocId_OrdersDocId(this.WhiteLabelDocId_Value, this.DocId_Value, true);
                _isMessagesContainerInit = true;
            }
            IsMessagesNullAble = _isNullAble;
        }
        #endregion




        //Relation From:[Passenger] To:[Orders] -Type M:1

        //-------1 Side--------------------------------------//
        #region Passenger Container Object
        public bool IsPassengerNullAble = true;
        private bool _isPassengerContainerInit = false;

        private PassengerContainer _passengerContainer;
        public PassengerContainer Passengers
        {
            get
            {
                if (_passengerContainer != null)
                {
                    return _passengerContainer;
                }
                else
                {
                    if (_isPassengerContainerInit)
                    {
                        if (IsPassengerNullAble) { return null; }
                        else { return new PassengerContainer(); }
                    }
                    else
                    {
                        Init_PassengerContainer(false);
                    }
                }
                return _passengerContainer;
            }
            set
            {
                _passengerContainer = value;
                //if (value == null)
                //{
                //    _passengerContainer = new PassengerContainer();
                //}
                //else
                //{
                //    if (_passengerContainer == null)
                //    {
                //        _passengerContainer = value;
                //    }
                //    else
                //    {
                //        lock (_passengerContainer)
                //        {
                //            _passengerContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with PassengerContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_PassengerContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsPassengerNullAble;
            IsPassengerNullAble = true;
            if (isInitAnyway || !_isPassengerContainerInit) //this.Passengers == null)
            {
                this.Passengers = PassengerContainer.SelectByKeysView_WhiteLabelDocId_OrdersDocId(this.WhiteLabelDocId_Value, this.DocId_Value, true);
                _isPassengerContainerInit = true;
            }
            IsPassengerNullAble = _isNullAble;
        }
        #endregion




        //Relation From:[OrderFlightLeg] To:[Orders] -Type M:1

        //-------1 Side--------------------------------------//
        #region OrderFlightLeg Container Object
        public bool IsOrderFlightLegNullAble = true;
        private bool _isOrderFlightLegContainerInit = false;

        private OrderFlightLegContainer _order_flight_legContainer;
        public OrderFlightLegContainer OrderFlightLegs
        {
            get
            {
                if (_order_flight_legContainer != null)
                {
                    return _order_flight_legContainer;
                }
                else
                {
                    if (_isOrderFlightLegContainerInit)
                    {
                        if (IsOrderFlightLegNullAble) { return null; }
                        else { return new OrderFlightLegContainer(); }
                    }
                    else
                    {
                        Init_OrderFlightLegContainer(false);
                    }
                }
                return _order_flight_legContainer;
            }
            set
            {
                _order_flight_legContainer = value;
                //if (value == null)
                //{
                //    _order_flight_legContainer = new OrderFlightLegContainer();
                //}
                //else
                //{
                //    if (_order_flight_legContainer == null)
                //    {
                //        _order_flight_legContainer = value;
                //    }
                //    else
                //    {
                //        lock (_order_flight_legContainer)
                //        {
                //            _order_flight_legContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with OrderFlightLegContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_OrderFlightLegContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsOrderFlightLegNullAble;
            IsOrderFlightLegNullAble = true;
            if (isInitAnyway || !_isOrderFlightLegContainerInit) //this.OrderFlightLegs == null)
            {
                this.OrderFlightLegs = OrderFlightLegContainer.SelectByKeysView_WhiteLabelDocId_OrdersDocId(this.WhiteLabelDocId_Value, this.DocId_Value, true);
                _isOrderFlightLegContainerInit = true;
            }
            IsOrderFlightLegNullAble = _isNullAble;
        }
        #endregion




        //Relation From:[OrderLog] To:[Orders] -Type M:1

        //-------1 Side--------------------------------------//
        #region OrderLog Container Object
        public bool IsOrderLogNullAble = true;
        private bool _isOrderLogContainerInit = false;

        private OrderLogContainer _order_logContainer;
        public OrderLogContainer OrderLogs
        {
            get
            {
                if (_order_logContainer != null)
                {
                    return _order_logContainer;
                }
                else
                {
                    if (_isOrderLogContainerInit)
                    {
                        if (IsOrderLogNullAble) { return null; }
                        else { return new OrderLogContainer(); }
                    }
                    else
                    {
                        Init_OrderLogContainer(false);
                    }
                }
                return _order_logContainer;
            }
            set
            {
                _order_logContainer = value;
                //if (value == null)
                //{
                //    _order_logContainer = new OrderLogContainer();
                //}
                //else
                //{
                //    if (_order_logContainer == null)
                //    {
                //        _order_logContainer = value;
                //    }
                //    else
                //    {
                //        lock (_order_logContainer)
                //        {
                //            _order_logContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with OrderLogContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_OrderLogContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsOrderLogNullAble;
            IsOrderLogNullAble = true;
            if (isInitAnyway || !_isOrderLogContainerInit) //this.OrderLogs == null)
            {
                this.OrderLogs = OrderLogContainer.SelectByKeysView_WhiteLabelDocId_OrdersDocId(this.WhiteLabelDocId_Value, this.DocId_Value, true);
                _isOrderLogContainerInit = true;
            }
            IsOrderLogNullAble = _isNullAble;
        }
        #endregion



        #endregion

    }
}
