

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class FlightLeg  : ContainerItem<FlightLeg>{

                #region Relations Code
                
                     // Relation From:[FlightLeg] To:[Airline] -Type M:1          
                            
		//-----M Side----------------------//
		#region Airline object
    private Airline _airline;
    public bool IsAirlineNullAble = true;
    private bool _isAirlineSingleInit = false;

    public Airline AirlineSingle
        {
            get
            {
                if (_airline != null)
                {
                    return _airline;
                }
                else 
                {
                    if (_isAirlineSingleInit)
                    {
                        if (IsAirlineNullAble) { return null; }
                        else { return new Airline(); }
                    }
                    else
                    {
                        Init_Airline(false);
                    }
                }
                return _airline;
            }
            set
            {
                if (value == null)
                {
                    _airline = new Airline();
                }
                else
                {
                    if (_airline == null)
                    {
                        _airline = value;
                    }
                    else
                    {
                        lock (_airline)
                        {
                            _airline = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Init object with OrderFlightLeg 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
    public void Init_Airline(bool isInitAnyway)
        {
            bool _isNullAble = IsAirlineNullAble;
            IsAirlineNullAble = true;
            if (isInitAnyway || !_isAirlineSingleInit)//AirlineSingle == null)
            {
                //Select by shared key
                this.AirlineSingle = AirlineContainer.SelectByKeysView_IataCode(this.AirlineIataCode_Value, true).Single;
                _isOrderFlightLegSingleInit = true;
            }
            IsOrderFlightLegNullAble = _isNullAble;
        }
        #endregion



    // Relation From:[FlightLeg] To:[Airport] -Type M:1           

    //-----M Side----------------------//
    #region Airport object
    private Airport _airport;
    public bool IsAirportNullAble = true;
    private bool _isAirportSingleInit = false;

    public Airport AirportSingle
    {
        get
        {
            if (_airport != null)
            {
                return _airport;
            }
            else
            {
                if (_isAirportSingleInit)
                {
                    if (IsAirportNullAble) { return null; }
                    else { return new Airport(); }
                }
                else
                {
                    Init_Airport(false);
                }
            }
            return _airport;
        }
        set
        {
            if (value == null)
            {
                _airport = new Airport();
            }
            else
            {
                if (_airport == null)
                {
                    _airport = value;
                }
                else
                {
                    lock (_airport)
                    {
                        _airport = value;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Init object with OrderFlightLeg 
    /// </summary>
    /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
    public void Init_Airport(bool isInitAnyway)
    {
        bool _isNullAble = IsAirportNullAble;
        IsAirportNullAble = true;
        if (isInitAnyway || !_isAirportSingleInit)//AirportSingle == null)
        {
            //Select by shared key
            this.AirportSingle = AirportContainer.SelectByKeysView_IataCode(this.OriginIataCode_Value, true).Single;
            this.AirportSingle = AirportContainer.SelectByKeysView_IataCode(this.DestinationIataCode_Value, true).Single;
            _isOrderFlightLegSingleInit = true;
        }
        IsOrderFlightLegNullAble = _isNullAble;
    }
    #endregion
        
                        
                #endregion
                
}
}
