using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;
    using Generic;

    public partial class DestinationPage : ContainerItem<DestinationPage>
    {
        //  THIS IS MY PARTIAL FOR DestinationPage RELATIONS

        #region Relations Code

        //  Relation From:[Template]  To:[DestinationPage] -Type M:1


        ////-------1 Side--------------------------------------//
        #region Template Container Object
        public bool IsTemplateNullAble = true;
        private bool _isTemplateContainerInit = false;

        private TemplateContainer _templateContainer;
        public TemplateContainer Templates
        {
            get
            {
                if (_templateContainer != null)
                {
                    return _templateContainer;
                }
                else
                {
                    if (_isTemplateContainerInit)
                    {
                        if (IsTemplateNullAble) { return null; }
                        else { return new TemplateContainer(); }
                    }
                    else
                    {
                        Init_TemplateContainer(false);
                    }
                }
                return _templateContainer;
            }
            set
            {
                _templateContainer = value;
            }
        }
        /// <summary>
        /// Init object with TemplateContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_TemplateContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsTemplateNullAble;
            IsTemplateNullAble = true;
            if (isInitAnyway || !_isTemplateContainerInit) //this.OrderFlightLegs == null)
            {
                this.Templates = TemplateContainer.SelectByKeysView_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId(this.WhiteLabelDocId_Value, (int)EnumHandler.ImageRelatedObject.DestinationPage, this.DocId_Value, null);
                _isTemplateContainerInit = true;
            }
            IsTemplateNullAble = _isNullAble;
        }
        #endregion

        




        //Relation From:[Images] To:[DestinationPage] -Type M:1     

        //-----M Side----------------------//

        private ImagesContainer _imagesContainer;
        public bool IsImagesContainerNullAble = true;
        private bool _isImagesContainerInit = false;


        public ImagesContainer ImagesContainer
        {
            get
            {
                if (_imagesContainer != null)
                {
                    return _imagesContainer;
                }
                else
                {
                    if (_isImagesContainerInit)
                    {
                        if (IsImagesContainerNullAble) { return null; }
                        else { return new ImagesContainer(); }
                    }
                    else
                    {
                        Init_ImagesContainer(false);
                    }
                }
                return _imagesContainer;
            }
            set
            {
                if (value == null)
                {
                    _imagesContainer = new ImagesContainer();
                }
                else
                {
                    if (_imagesContainer == null)
                    {
                        _imagesContainer = value;
                    }
                    else
                    {
                        lock (_imagesContainer)
                        {
                            _imagesContainer = value;
                        }
                    }
                }
            }
        }


        public void Init_ImagesContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsImagesContainerNullAble;
            IsImagesContainerNullAble = true;
            if (isInitAnyway || !_isImagesContainerInit)
            {
                //Select by shared key
                this.ImagesContainer = ImagesContainer.SelectByKeysView_RelatedObjectDocId_RelatedObjectType(this.DocId_Value,EnumHandler.ImageRelatedObject.DestinationPage.ToString(), null);
                _isImagesContainerInit = true;
            }
            IsImagesContainerNullAble = _isNullAble;
        }




        // relation with PriceCurrency
        ////-------1 Side--------------------------------------//
        #region Price Currency Object
        public bool IsPriceCurrencyNullAble = true;
        private bool _isPriceCurrencyInit = false;

        private Currency _priceCurrency;
        public Currency PriceCurrencyObject
        {
            get
            {
                if (_priceCurrency != null)
                {
                    return _priceCurrency;
                }
                else
                {
                    if (_isPriceCurrencyInit)
                    {
                        if (IsPriceCurrencyNullAble) { return null; }
                        else { return new Currency(); }
                    }
                    else
                    {
                        Init_PriceCurrency(false);
                    }
                }
                return _priceCurrency;
            }
            set
            {
                _priceCurrency = value;
            }
        }


        /// <summary>
        /// Init object with TemplateContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_PriceCurrency(bool isInitAnyway)
        {
            bool _isNullAble = IsPriceCurrencyNullAble;
            IsTemplateNullAble = true;
            if (isInitAnyway || !_isPriceCurrencyInit) //this.OrderFlightLegs == null)
            {
                CurrencyContainer currencies = CurrencyContainer.SelectByKeysView_Code(this.PriceCurrency_UI, true);
                if (currencies != null)
                {
                    this.PriceCurrencyObject = currencies.Single;
                }
                else
                {
                    this.PriceCurrencyObject = new Currency();
                }
                _isPriceCurrencyInit = true;
            }
            IsPriceCurrencyNullAble = _isNullAble;
        }
        #endregion


        // relation with LocalCurrencyObject 
        ////-------1 Side--------------------------------------//
        #region Local Currency Object
        public bool IsLocalCurrencyNullAble = true;
        private bool _isLocalCurrencyInit = false;

        private Currency _localCurrency;
        public Currency LocalCurrencyObject
        {
            get
            {
                if (_localCurrency != null)
                {
                    return _localCurrency;
                }
                else
                {
                    if (_isLocalCurrencyInit)
                    {
                        if (IsLocalCurrencyNullAble) { return null; }
                        else { return new Currency(); }
                    }
                    else
                    {
                        Init_LocalCurrency(false);
                    }
                }
                return _localCurrency;
            }
            set
            {
                _localCurrency = value;
            }
        }


        /// <summary>
        /// Init object with TemplateContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_LocalCurrency(bool isInitAnyway)
        {
            bool _isNullAble = IsLocalCurrencyNullAble;
            IsTemplateNullAble = true;
            if (isInitAnyway || !_isLocalCurrencyInit) //this.OrderFlightLegs == null)
            {
                CurrencyContainer currencies = CurrencyContainer.SelectByKeysView_Code(this.LocalCurrency_UI, true);
                if (currencies != null)
                {
                    this.LocalCurrencyObject = currencies.Single;
                }
                else
                {
                    this.LocalCurrencyObject = new Currency();
                }
                _isLocalCurrencyInit = true;
            }
            IsLocalCurrencyNullAble = _isNullAble;
        }
        #endregion



        #endregion
    }
}
