

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class AirlineFare  : ContainerItem<AirlineFare>{

                #region Relations Code


    //Relation From:[Airline] To:[AirlineFare] -Type 1:1     
    private Airline _airline;
    public bool IsAirlineNullAble = true;
    private bool _isAirlineInit = false;


    public Airline AirlineSingle
    {
        get
        {
            if (_airline != null)
            {
                return _airline;
            }
            else
            {
                if (_isAirlineInit)
                {
                    if (IsAirlineNullAble) { return null; }
                    else { return new Airline(); }
                }
                else
                {
                    Init_Airline(false);
                }
            }
            return _airline;
        }
        set
        {
            if (value == null)
            {
                _airline = new Airline();
            }
            else
            {
                if (_airline == null)
                {
                    _airline = value;
                }
                else
                {
                    lock (_airline)
                    {
                        _airline = value;
                    }
                }
            }
        }
    }
    public void Init_Airline(bool isInitAnyway)
    {
        bool _isNullAble = IsAirlineNullAble;
        IsAirlineNullAble = true;
        if (isInitAnyway || !_isAirlineInit)
        {
            //Select by shared key
            var oAirlineContainer = AirlineContainer.SelectByKeysView_IataCode(this.AirlineCode_UI, null);
            if (oAirlineContainer != null)
            {
                // because there could be a case where there are 2 countries with the same iata code (it is not a key), take the first and not single.
                this.AirlineSingle = oAirlineContainer.First; //.Single;
            }
            _isAirlineInit = true;
        }
        IsAirlineNullAble = _isNullAble;
    }
                        
                #endregion
                
}
}
