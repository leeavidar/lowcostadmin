

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;

    public partial class Orders : ContainerItem<Orders>
    {

       

        /// <summary>
        /// Init object with ContactDetailsContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_ContactDetails(bool isInitAnyway)
        {
            bool _isNullAble = IsContactDetailsNullAble;
            IsContactDetailsNullAble = true;
            if (isInitAnyway || !_isContactDetailsInit) //this.ContactDetailss == null)
            {
                //this.ContactDetailss = ContactDetailsContainer.SelectByKeysView_WhiteLabelDocId_OrdersDocId(this.WhiteLabelDocId_Value, this.DocId_Value, true);
                //To fix for dudu 
                this.ContactDetails = ContactDetailsContainer.SelectByKeysView_WhiteLabelDocId_DocId(this.WhiteLabelDocId_Value, this.ContactDetailsDocId_Value, true).Single;

                _isContactDetailsInit = true;
            }
            IsContactDetailsNullAble = _isNullAble;
        }

    }
}
