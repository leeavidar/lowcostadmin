

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;
    using Generic;

    public partial class CompanyPage : ContainerItem<CompanyPage>
    {
        #region Relations Code

        //  Relation From:[Template]  To:[CompanyPage] -Type M:1


        //-------1 Side--------------------------------------//

        #region Templates Container
     
        public bool IsTemplateNullAble = true;
        private bool _isTemplateContainerInit = false;

        private TemplateContainer _templateContainer;
        public TemplateContainer Templates
        {
            get
            {
                if (_templateContainer != null)
                {
                    return _templateContainer;
                }
                else
                {
                    if (_isTemplateContainerInit)
                    {
                        if (IsTemplateNullAble) { return null; }
                        else { return new TemplateContainer(); }
                    }
                    else
                    {
                        Init_TemplateContainer(false);
                    }
                }
                return _templateContainer;
            }
            set
            {
                _templateContainer = value;
            }
        }
        /// <summary>
        /// Init object with TemplateContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_TemplateContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsTemplateNullAble;
            IsTemplateNullAble = true;
            if (isInitAnyway || !_isTemplateContainerInit)
            {
                this.Templates = TemplateContainer.SelectByKeysView_WhiteLabelDocId_RelatedObjectType_RelatedObjectDocId(this.WhiteLabelDocId_Value, (int)EnumHandler.ImageRelatedObject.CompanyPage, this.DocId_Value, null);
                _isTemplateContainerInit = true;
            }
            IsTemplateNullAble = _isNullAble;
        }

        #endregion




        //  Relation From:[Images]  To:[CompanyPage] -Type M:1


        //-------1 Side--------------------------------------//

        #region Images container

        public bool IsImagesNullAble = true;
        private bool _isImagesContainerInit = false;

        private ImagesContainer _imagesContainer;
        public ImagesContainer Imagess
        {
            get
            {
                if (_imagesContainer != null)
                {
                    return _imagesContainer;
                }
                else
                {
                    if (_isImagesContainerInit)
                    {
                        if (IsImagesNullAble) { return null; }
                        else { return new ImagesContainer(); }
                    }
                    else
                    {
                        Init_ImagesContainer(false);
                    }
                }
                return _imagesContainer;
            }
            set
            {
                _imagesContainer = value;
            }
        }
        /// <summary>
        /// Init object with ImagesContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_ImagesContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsImagesNullAble;
            IsImagesNullAble = true;
            if (isInitAnyway || !_isImagesContainerInit)
            {
                this.Imagess = ImagesContainer.SelectByKeysView_RelatedObjectDocId_RelatedObjectType(this.DocId_Value, EnumHandler.RelatedObjects.CompanyPage.ToString(), null);

                _isImagesContainerInit = true;
            }
            IsImagesNullAble = _isNullAble;
        }

        #endregion

        #endregion
    }
}

