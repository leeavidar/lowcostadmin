

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Airport  : ContainerItem<Airport>{

                #region Relations Code

    //Relation From:[FlightLeg] To:[Airport] -Type M:1                     

    //-------1 Side--------------------------------------//
    #region FlightLeg Container Object
    public bool IsFlightLegNullAble = true;
    private bool _isFlightLegContainerInit = true;

    private FlightLegContainer _flightLegContainer;
    public FlightLegContainer FlightLegs
    {
        get
        {
            if (_flightLegContainer != null)
            {
                return _flightLegContainer;
            }
            else
            {
                if (_isFlightLegContainerInit)
                {
                    if (IsFlightLegNullAble) { return null; }
                    else { return new FlightLegContainer(); }
                }
                else
                {
                    Init_FlightLegContainer(false);
                }
            }
            return _flightLegContainer;
        }
        set
        {
            if (value == null)
            {
                _flightLegContainer = new FlightLegContainer();
            }
            else
            {
                if (_flightLegContainer == null)
                {
                    _flightLegContainer = value;
                }
                else
                {
                    lock (_flightLegContainer)
                    {
                        _flightLegContainer = value;
                    }

                }
            }
        }
    }
    /// <summary>
    /// Init object with FlightLegContainer
    /// </summary>
    /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
    public void Init_FlightLegContainer(bool isInitAnyway)
    {
        bool _isNullAble = IsFlightLegNullAble;
        IsFlightLegNullAble = true;
        if (isInitAnyway || !_isFlightLegContainerInit) //this.FlightLegs == null)
        {
            // this.FlightLegs = FlightLegContainer.SelectByKeysView_WhiteLabelDocId_AirportIataCode(this.WhiteLabelDocId_Value, this.DocId_Value, true);

            //this.FlightLegs = FlightLegContainer.SelectByKeysView_WhiteLabelDocId_AirportIataCode(this.WhiteLabelDocId_Value, this.DocId_Value, true);

            _isFlightLegContainerInit = true;
        }
        IsFlightLegNullAble = _isNullAble;
    }
    #endregion



    //Relation From:[City] To:[Airport] -Type 1:1     
    private City _city;
    public bool IsCityNullAble = true;
    private bool _isCityInit = false;


    public City CitySingle
    {
        get
        {
            if (_city != null)
            {
                return _city;
            }
            else
            {
                if (_isCityInit)
                {
                    if (IsCityNullAble) { return null; }
                    else { return new City(); }
                }
                else
                {
                    Init_City(false);
                }
            }
            return _city;
        }
        set
        {
            if (value == null)
            {
                _city = new City();
            }
            else
            {
                if (_city == null)
                {
                    _city = value;
                }
                else
                {
                    lock (_city)
                    {
                        _city = value;
                    }
                }
            }
        }
    }

    public void Init_City(bool isInitAnyway)
    {
        bool _isNullAble = IsCityNullAble;
        IsCityNullAble = true;
        if (isInitAnyway || !_isCityInit)
        {
            //Select by shared key
            CityContainer oCityContainer = CityContainer.SelectByKeysView_IataCode(this.CityIataCode_Value, null);
            if (oCityContainer != null)
            {
                // because there could be a situation where there are more than 1 cities with the same IATA code in the database, take the first and not single.
                this.CitySingle = oCityContainer.First; // .Single;
            }

            _isCityInit = true;
        }
        IsCityNullAble = _isNullAble;
    }
                #endregion
                
}
}
