

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;

    public partial class City : ContainerItem<City>
    {

        //Relation From:[Country] To:[Country] -Type 1:1     
        private Country _country;
        public bool IsCountryNullAble = true;
        private bool _isCountryInit = false;


        public Country CountrySingle
        {
            get
            {
                if (_country != null)
                {
                    return _country;
                }
                else
                {
                    if (_isCountryInit)
                    {
                        if (IsCountryNullAble) { return null; }
                        else { return new Country(); }
                    }
                    else
                    {
                        Init_Country(false);
                    }
                }
                return _country;
            }
            set
            {
                if (value == null)
                {
                    _country = new Country();
                }
                else
                {
                    if (_country == null)
                    {
                        _country = value;
                    }
                    else
                    {
                        lock (_country)
                        {
                            _country = value;
                        }
                    }
                }
            }
        }
        public void Init_Country(bool isInitAnyway)
        {
            bool _isNullAble = IsCountryNullAble;
            IsCountryNullAble = true;
            if (isInitAnyway || !_isCountryInit)
            {
                //Select by shared key
                CountryContainer oCountryContainer = CountryContainer.SelectByKeysView_CountryCode(this.CountryCode_UI, null);
                if (oCountryContainer != null)
                {
                    // because there could be a case where there are 2 countries with the same iata code (it is not a key), take the first and not single.
                    this.CountrySingle = oCountryContainer.First; //.Single;
                }
                _isCountryInit = true;
            }
            IsCountryNullAble = _isNullAble;
        }
    }
}
