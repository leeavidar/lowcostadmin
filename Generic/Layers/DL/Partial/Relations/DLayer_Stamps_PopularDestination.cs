

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Data;
//using DL_Generic;

//namespace DL_LowCost{
//    using BL_LowCost;

//public partial class Stamps_PopularDestination  : ContainerItem<Stamps_PopularDestination>{

//                #region Relations Code
                
//                            //Relation From:[Stamps_PopularDestination] To:[PopularDestination] -Type M:1
                            
//        //-----M Side----------------------//
//        #region PopularDestination object
//        private PopularDestination _popular_destination;
//        public bool IsPopularDestinationNullAble = true;
//        private bool _isPopularDestinationSingleInit = false;

//        public PopularDestination PopularDestinationSingle
//        {
//            get
//            {
//                if (_popular_destination != null)
//                {
//                    return _popular_destination;
//                }
//                else 
//                {
//                    if (_isPopularDestinationSingleInit)
//                    {
//                        if(IsPopularDestinationNullAble) {return null;}
//                        else  {return new PopularDestination();}
//                    }
//                    else
//                    {
//                        Init_PopularDestination(false);
//                    }
//                }
//                return _popular_destination;
//            }
//            set
//            {
//                _popular_destination = value;
//                //if (value == null)
//                //{
//                //    _popular_destination = new PopularDestination();
//                //}
//                //else
//                //{
//                //    if (_popular_destination == null)
//                //    {
//                //        _popular_destination = value;
//                //    }
//                //    else
//                //    {
//                //        lock (_popular_destination)
//                //        {
//                //            _popular_destination = value;
//                //        }
//                //    }
//                //}
//            }
//        }

//        /// <summary>
//        /// Init object with PopularDestination 
//        /// </summary>
//        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
//        public void Init_PopularDestination(bool isInitAnyway)
//        {
//            bool _isNullAble = IsPopularDestinationNullAble;
//            IsPopularDestinationNullAble = true;
//            if (isInitAnyway || !_isPopularDestinationSingleInit)//PopularDestinationSingle == null)
//            {
//                //Select by shared key
//                this.PopularDestinationSingle = PopularDestinationContainer.SelectByID(this.PopularDestinationDocId_Value,true).Single;
//                _isPopularDestinationSingleInit = true;
//            }
//            IsPopularDestinationNullAble = _isNullAble;
//        }
//        #endregion
        
                        

//                            //Relation From:[Stamps_PopularDestination] To:[Stamps] -Type M:1
                            
//        //-----M Side----------------------//
//        #region Stamps object
//        private Stamps _stamps;
//        public bool IsStampsNullAble = true;
//        private bool _isStampsSingleInit = false;

//        public Stamps StampsSingle
//        {
//            get
//            {
//                if (_stamps != null)
//                {
//                    return _stamps;
//                }
//                else 
//                {
//                    if (_isStampsSingleInit)
//                    {
//                        if(IsStampsNullAble) {return null;}
//                        else  {return new Stamps();}
//                    }
//                    else
//                    {
//                        Init_Stamps(false);
//                    }
//                }
//                return _stamps;
//            }
//            set
//            {
//                _stamps = value;
//                //if (value == null)
//                //{
//                //    _stamps = new Stamps();
//                //}
//                //else
//                //{
//                //    if (_stamps == null)
//                //    {
//                //        _stamps = value;
//                //    }
//                //    else
//                //    {
//                //        lock (_stamps)
//                //        {
//                //            _stamps = value;
//                //        }
//                //    }
//                //}
//            }
//        }

//        /// <summary>
//        /// Init object with Stamps 
//        /// </summary>
//        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
//        public void Init_Stamps(bool isInitAnyway)
//        {
//            bool _isNullAble = IsStampsNullAble;
//            IsStampsNullAble = true;
//            if (isInitAnyway || !_isStampsSingleInit)//StampsSingle == null)
//            {
//                //Select by shared key
//                this.StampsSingle = StampsContainer.SelectByID(this.StampsDocId_Value,true).Single;
//                _isStampsSingleInit = true;
//            }
//            IsStampsNullAble = _isNullAble;
//        }
//        #endregion
        
                        
//                #endregion
                
//}
//}
