

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Seo  : ContainerItem<Seo>{

                #region Relations Code
                
                            //Relation From:[Seo] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[StaticPages] To:[Seo] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region StaticPages Container Object
        public bool IsStaticPagesNullAble = true;
        private bool _isStaticPagesContainerInit = false;
        
        private StaticPagesContainer _static_pagesContainer;
        public StaticPagesContainer StaticPagess
        {
            get
            {
                if (_static_pagesContainer != null)
                {
                    return _static_pagesContainer;
                }
                else 
                {
                    if (_isStaticPagesContainerInit)
                    {
                        if(IsStaticPagesNullAble) {return null;}
                        else { return  new StaticPagesContainer();}
                    }
                    else
                    {
                        Init_StaticPagesContainer(false);
                    }
                }
                return _static_pagesContainer;
            }
            set
            {
                 _static_pagesContainer = value;
                //if (value == null)
                //{
                //    _static_pagesContainer = new StaticPagesContainer();
                //}
                //else
                //{
                //    if (_static_pagesContainer == null)
                //    {
                //        _static_pagesContainer = value;
                //    }
                //    else
                //    {
                //        lock (_static_pagesContainer)
                //        {
                //            _static_pagesContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with StaticPagesContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_StaticPagesContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsStaticPagesNullAble;
            IsStaticPagesNullAble = true;
            if (isInitAnyway || !_isStaticPagesContainerInit) //this.StaticPagess == null)
            {
                this.StaticPagess = StaticPagesContainer.SelectByKeysView_WhiteLabelDocId_SeoDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isStaticPagesContainerInit = true;
            }
            IsStaticPagesNullAble = _isNullAble;
        }
        #endregion

        
                        

                            //Relation From:[DestinationPage] To:[Seo] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region DestinationPage Container Object
        public bool IsDestinationPageNullAble = true;
        private bool _isDestinationPageContainerInit = false;
        
        private DestinationPageContainer _destination_pageContainer;
        public DestinationPageContainer DestinationPages
        {
            get
            {
                if (_destination_pageContainer != null)
                {
                    return _destination_pageContainer;
                }
                else 
                {
                    if (_isDestinationPageContainerInit)
                    {
                        if(IsDestinationPageNullAble) {return null;}
                        else { return  new DestinationPageContainer();}
                    }
                    else
                    {
                        Init_DestinationPageContainer(false);
                    }
                }
                return _destination_pageContainer;
            }
            set
            {
                 _destination_pageContainer = value;
                //if (value == null)
                //{
                //    _destination_pageContainer = new DestinationPageContainer();
                //}
                //else
                //{
                //    if (_destination_pageContainer == null)
                //    {
                //        _destination_pageContainer = value;
                //    }
                //    else
                //    {
                //        lock (_destination_pageContainer)
                //        {
                //            _destination_pageContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with DestinationPageContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_DestinationPageContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsDestinationPageNullAble;
            IsDestinationPageNullAble = true;
            if (isInitAnyway || !_isDestinationPageContainerInit) //this.DestinationPages == null)
            {
                this.DestinationPages = DestinationPageContainer.SelectByKeysView_WhiteLabelDocId_SeoDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isDestinationPageContainerInit = true;
            }
            IsDestinationPageNullAble = _isNullAble;
        }
        #endregion

        
                        

                            //Relation From:[CompanyPage] To:[Seo] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region CompanyPage Container Object
        public bool IsCompanyPageNullAble = true;
        private bool _isCompanyPageContainerInit = false;
        
        private CompanyPageContainer _company_pageContainer;
        public CompanyPageContainer CompanyPages
        {
            get
            {
                if (_company_pageContainer != null)
                {
                    return _company_pageContainer;
                }
                else 
                {
                    if (_isCompanyPageContainerInit)
                    {
                        if(IsCompanyPageNullAble) {return null;}
                        else { return  new CompanyPageContainer();}
                    }
                    else
                    {
                        Init_CompanyPageContainer(false);
                    }
                }
                return _company_pageContainer;
            }
            set
            {
                 _company_pageContainer = value;
                //if (value == null)
                //{
                //    _company_pageContainer = new CompanyPageContainer();
                //}
                //else
                //{
                //    if (_company_pageContainer == null)
                //    {
                //        _company_pageContainer = value;
                //    }
                //    else
                //    {
                //        lock (_company_pageContainer)
                //        {
                //            _company_pageContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with CompanyPageContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_CompanyPageContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsCompanyPageNullAble;
            IsCompanyPageNullAble = true;
            if (isInitAnyway || !_isCompanyPageContainerInit) //this.CompanyPages == null)
            {
                this.CompanyPages = CompanyPageContainer.SelectByKeysView_WhiteLabelDocId_SeoDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isCompanyPageContainerInit = true;
            }
            IsCompanyPageNullAble = _isNullAble;
        }
        #endregion

        
                        

                            //Relation From:[HomePage] To:[Seo] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region HomePage Container Object
        public bool IsHomePageNullAble = true;
        private bool _isHomePageContainerInit = false;
        
        private HomePageContainer _home_pageContainer;
        public HomePageContainer HomePages
        {
            get
            {
                if (_home_pageContainer != null)
                {
                    return _home_pageContainer;
                }
                else 
                {
                    if (_isHomePageContainerInit)
                    {
                        if(IsHomePageNullAble) {return null;}
                        else { return  new HomePageContainer();}
                    }
                    else
                    {
                        Init_HomePageContainer(false);
                    }
                }
                return _home_pageContainer;
            }
            set
            {
                 _home_pageContainer = value;
                //if (value == null)
                //{
                //    _home_pageContainer = new HomePageContainer();
                //}
                //else
                //{
                //    if (_home_pageContainer == null)
                //    {
                //        _home_pageContainer = value;
                //    }
                //    else
                //    {
                //        lock (_home_pageContainer)
                //        {
                //            _home_pageContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with HomePageContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_HomePageContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsHomePageNullAble;
            IsHomePageNullAble = true;
            if (isInitAnyway || !_isHomePageContainerInit) //this.HomePages == null)
            {
                this.HomePages = HomePageContainer.SelectByKeysView_WhiteLabelDocId_SeoDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isHomePageContainerInit = true;
            }
            IsHomePageNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
