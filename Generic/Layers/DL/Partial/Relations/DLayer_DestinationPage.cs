

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class DestinationPage  : ContainerItem<DestinationPage>{

                #region Relations Code
                
                            //Relation From:[DestinationPage] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[DestinationPage] To:[Seo] -Type M:1
                            
		//-----M Side----------------------//
		#region Seo object
        private Seo _seo;
        public bool IsSeoNullAble = true;
        private bool _isSeoSingleInit = false;

        public Seo SeoSingle
        {
            get
            {
                if (_seo != null)
                {
                    return _seo;
                }
                else 
                {
                    if (_isSeoSingleInit)
                    {
                        if(IsSeoNullAble) {return null;}
                        else  {return new Seo();}
                    }
                    else
                    {
                        Init_Seo(false);
                    }
                }
                return _seo;
            }
            set
            {
                _seo = value;
                //if (value == null)
                //{
                //    _seo = new Seo();
                //}
                //else
                //{
                //    if (_seo == null)
                //    {
                //        _seo = value;
                //    }
                //    else
                //    {
                //        lock (_seo)
                //        {
                //            _seo = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with Seo 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Seo(bool isInitAnyway)
        {
            bool _isNullAble = IsSeoNullAble;
            IsSeoNullAble = true;
            if (isInitAnyway || !_isSeoSingleInit)//SeoSingle == null)
            {
                //Select by shared key
                this.SeoSingle = SeoContainer.SelectByID(this.SeoDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isSeoSingleInit = true;
            }
            IsSeoNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[HomePageDestinationPage] To:[DestinationPage] -Type M:1
                            
        //-------1 Side--------------------------------------//
        #region HomePageDestinationPage Container Object
        public bool IsHomePageDestinationPageNullAble = true;
        private bool _isHomePageDestinationPageContainerInit = false;
        
        private HomePageDestinationPageContainer _home_page_destination_pageContainer;
        public HomePageDestinationPageContainer HomePageDestinationPages
        {
            get
            {
                if (_home_page_destination_pageContainer != null)
                {
                    return _home_page_destination_pageContainer;
                }
                else 
                {
                    if (_isHomePageDestinationPageContainerInit)
                    {
                        if(IsHomePageDestinationPageNullAble) {return null;}
                        else { return  new HomePageDestinationPageContainer();}
                    }
                    else
                    {
                        Init_HomePageDestinationPageContainer(false);
                    }
                }
                return _home_page_destination_pageContainer;
            }
            set
            {
                 _home_page_destination_pageContainer = value;
                //if (value == null)
                //{
                //    _home_page_destination_pageContainer = new HomePageDestinationPageContainer();
                //}
                //else
                //{
                //    if (_home_page_destination_pageContainer == null)
                //    {
                //        _home_page_destination_pageContainer = value;
                //    }
                //    else
                //    {
                //        lock (_home_page_destination_pageContainer)
                //        {
                //            _home_page_destination_pageContainer = value;
                //        }
                //
                //    }
                //}
            }
        }
        /// <summary>
        /// Init object with HomePageDestinationPageContainer
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_HomePageDestinationPageContainer(bool isInitAnyway)
        {
            bool _isNullAble = IsHomePageDestinationPageNullAble;
            IsHomePageDestinationPageNullAble = true;
            if (isInitAnyway || !_isHomePageDestinationPageContainerInit) //this.HomePageDestinationPages == null)
            {
                this.HomePageDestinationPages = HomePageDestinationPageContainer.SelectByKeysView_WhiteLabelDocId_DestinationPageDocId(this.WhiteLabelDocId_Value,this.DocId_Value,true);
                _isHomePageDestinationPageContainerInit = true;
            }
            IsHomePageDestinationPageNullAble = _isNullAble;
        }
        #endregion

        
                        
                #endregion
                
}
}
