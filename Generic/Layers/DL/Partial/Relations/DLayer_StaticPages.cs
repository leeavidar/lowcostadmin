

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class StaticPages  : ContainerItem<StaticPages>{

                #region Relations Code
                
                            //Relation From:[StaticPages] To:[Seo] -Type M:1
                            
		//-----M Side----------------------//
		#region Seo object
        private Seo _seo;
        public bool IsSeoNullAble = true;
        private bool _isSeoSingleInit = false;

        public Seo SeoSingle
        {
            get
            {
                if (_seo != null)
                {
                    return _seo;
                }
                else 
                {
                    if (_isSeoSingleInit)
                    {
                        if(IsSeoNullAble) {return null;}
                        else  {return new Seo();}
                    }
                    else
                    {
                        Init_Seo(false);
                    }
                }
                return _seo;
            }
            set
            {
                _seo = value;
                //if (value == null)
                //{
                //    _seo = new Seo();
                //}
                //else
                //{
                //    if (_seo == null)
                //    {
                //        _seo = value;
                //    }
                //    else
                //    {
                //        lock (_seo)
                //        {
                //            _seo = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with Seo 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_Seo(bool isInitAnyway)
        {
            bool _isNullAble = IsSeoNullAble;
            IsSeoNullAble = true;
            if (isInitAnyway || !_isSeoSingleInit)//SeoSingle == null)
            {
                //Select by shared key
                this.SeoSingle = SeoContainer.SelectByID(this.SeoDocId_Value,this.WhiteLabelDocId_Value,true).Single;
                _isSeoSingleInit = true;
            }
            IsSeoNullAble = _isNullAble;
        }
        #endregion
        
                        

                            //Relation From:[StaticPages] To:[WhiteLabel] -Type M:1
                            
		//-----M Side----------------------//
		#region WhiteLabel object
        private WhiteLabel _white_label;
        public bool IsWhiteLabelNullAble = true;
        private bool _isWhiteLabelSingleInit = false;

        public WhiteLabel WhiteLabelSingle
        {
            get
            {
                if (_white_label != null)
                {
                    return _white_label;
                }
                else 
                {
                    if (_isWhiteLabelSingleInit)
                    {
                        if(IsWhiteLabelNullAble) {return null;}
                        else  {return new WhiteLabel();}
                    }
                    else
                    {
                        Init_WhiteLabel(false);
                    }
                }
                return _white_label;
            }
            set
            {
                _white_label = value;
                //if (value == null)
                //{
                //    _white_label = new WhiteLabel();
                //}
                //else
                //{
                //    if (_white_label == null)
                //    {
                //        _white_label = value;
                //    }
                //    else
                //    {
                //        lock (_white_label)
                //        {
                //            _white_label = value;
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Init object with WhiteLabel 
        /// </summary>
        /// <param name='isInitAnyway'>Define if to setup even if the object is setup</param>
        public void Init_WhiteLabel(bool isInitAnyway)
        {
            bool _isNullAble = IsWhiteLabelNullAble;
            IsWhiteLabelNullAble = true;
            if (isInitAnyway || !_isWhiteLabelSingleInit)//WhiteLabelSingle == null)
            {
                //Select by shared key
                this.WhiteLabelSingle = WhiteLabelContainer.SelectByID(this.WhiteLabelDocId_Value,true).Single;
                _isWhiteLabelSingleInit = true;
            }
            IsWhiteLabelNullAble = _isNullAble;
        }
        #endregion
        
                        
                #endregion
                
}
}
