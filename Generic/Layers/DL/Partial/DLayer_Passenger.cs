

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost
{
    using BL_LowCost;

    public partial class Passenger : ContainerItem<Passenger>
    {

        public string FullNameNoTitle_UI
        {
            get
            {
                return string.Format("{0} {1}", this.LastName_UI, this.FirstName_UI);
            }
        }

        public string FullName_UI
        {
            get
            {
                return string.Format("{0}. {1} {2}", this.Title_UI, this.LastName_UI, this.FirstName_UI);
            }
        }
    }
}
