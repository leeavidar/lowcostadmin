

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class StaticPages  : ContainerItem<StaticPages>{

#region CTOR

    #region Constractor
    static StaticPages()
    {
        ConvertEvent = StaticPages.OnConvert;
    }  
    //public KeyValuesContainer<StaticPages> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public StaticPages()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.StaticPagesKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static StaticPages OnConvert(DataRow dr)
    {
        int LangId = Translator<StaticPages>.LangId;            
        StaticPages oStaticPages = null;
        if (dr != null)
        {
            oStaticPages = new StaticPages();
            #region Create Object
            oStaticPages.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_StaticPages.Field_DateCreated]);
             oStaticPages.DocId = ConvertTo.ConvertToInt(dr[TBNames_StaticPages.Field_DocId]);
             oStaticPages.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_StaticPages.Field_IsDeleted]);
             oStaticPages.IsActive = ConvertTo.ConvertToBool(dr[TBNames_StaticPages.Field_IsActive]);
             oStaticPages.FriendlyUrl = ConvertTo.ConvertToString(dr[TBNames_StaticPages.Field_FriendlyUrl]);
             oStaticPages.Text = ConvertTo.ConvertToString(dr[TBNames_StaticPages.Field_Text]);
             oStaticPages.Name = ConvertTo.ConvertToString(dr[TBNames_StaticPages.Field_Name]);
             oStaticPages.Type = ConvertTo.ConvertToInt(dr[TBNames_StaticPages.Field_Type]);
 
//FK     KeySeoDocId
            oStaticPages.SeoDocId = ConvertTo.ConvertToInt(dr[TBNames_StaticPages.Field_SeoDocId]);
 
//FK     KeyWhiteLabelDocId
            oStaticPages.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_StaticPages.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<StaticPages>.Translate(oStaticPages.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oStaticPages;
    }

    
#endregion

//#REP_HERE 
#region StaticPages Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyStaticPagesDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyStaticPagesDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStaticPagesIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStaticPagesIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_FriendlyUrl;
private string _friendly_url;
public String FriendlyFriendlyUrl
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesFriendlyUrl);   
    }
}
public  string FriendlyUrl
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyStaticPagesFriendlyUrl));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesFriendlyUrl, value);
         _friendly_url = ConvertToValue.ConvertToString(value);
        isSetOnce_FriendlyUrl = true;
    }
}


public string FriendlyUrl_Value
{
    get
    {
        //return _friendly_url; //ConvertToValue.ConvertToString(FriendlyUrl);
        if(isSetOnce_FriendlyUrl) {return _friendly_url;}
        else {return ConvertToValue.ConvertToString(FriendlyUrl);}
    }
}

public string FriendlyUrl_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_friendly_url).ToString();
               //if(isSetOnce_FriendlyUrl) {return ConvertToValue.ConvertToString(_friendly_url).ToString();}
               //else {return ConvertToValue.ConvertToString(FriendlyUrl).ToString();}
            ConvertToValue.ConvertToString(FriendlyUrl).ToString();
    }
}

private bool isSetOnce_Text;
private string _text;
public String FriendlyText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesText);   
    }
}
public  string Text
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyStaticPagesText));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesText, value);
         _text = ConvertToValue.ConvertToString(value);
        isSetOnce_Text = true;
    }
}


public string Text_Value
{
    get
    {
        //return _text; //ConvertToValue.ConvertToString(Text);
        if(isSetOnce_Text) {return _text;}
        else {return ConvertToValue.ConvertToString(Text);}
    }
}

public string Text_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_text).ToString();
               //if(isSetOnce_Text) {return ConvertToValue.ConvertToString(_text).ToString();}
               //else {return ConvertToValue.ConvertToString(Text).ToString();}
            ConvertToValue.ConvertToString(Text).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyStaticPagesName));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_Type;
private int _type;
public String FriendlyType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStaticPagesType);   
    }
}
public  int? Type
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyStaticPagesType));
    }
    set
    {
        SetKey(KeyValuesType.KeyStaticPagesType, value);
         _type = ConvertToValue.ConvertToInt(value);
        isSetOnce_Type = true;
    }
}


public int Type_Value
{
    get
    {
        //return _type; //ConvertToValue.ConvertToInt(Type);
        if(isSetOnce_Type) {return _type;}
        else {return ConvertToValue.ConvertToInt(Type);}
    }
}

public string Type_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_type).ToString();
               //if(isSetOnce_Type) {return ConvertToValue.ConvertToInt(_type).ToString();}
               //else {return ConvertToValue.ConvertToInt(Type).ToString();}
            ConvertToValue.ConvertToInt(Type).ToString();
    }
}

private bool isSetOnce_SeoDocId;
private int _seo_doc_id;
public String FriendlySeoDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoDocId);   
    }
}
public  int? SeoDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeoDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoDocId, value);
         _seo_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_SeoDocId = true;
    }
}


public int SeoDocId_Value
{
    get
    {
        //return _seo_doc_id; //ConvertToValue.ConvertToInt(SeoDocId);
        if(isSetOnce_SeoDocId) {return _seo_doc_id;}
        else {return ConvertToValue.ConvertToInt(SeoDocId);}
    }
}

public string SeoDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_seo_doc_id).ToString();
               //if(isSetOnce_SeoDocId) {return ConvertToValue.ConvertToInt(_seo_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(SeoDocId).ToString();}
            ConvertToValue.ConvertToInt(SeoDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //J_A
        //9_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //J_B
        //9_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C
        //9_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_E
        //9_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_F
        //9_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //J_G
        //9_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //J_H
        //9_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Type", ex.Message));

            }
            return paramsSelect;
        }



        //J_I
        //9_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_B
        //9_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_C
        //9_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_E
        //9_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FriendlyUrl(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_F
        //9_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Text(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Text", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_G
        //9_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_H
        //9_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Type(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Type", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_I
        //9_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DateCreated)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_C
        //9_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_E
        //9_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FriendlyUrl(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_F
        //9_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Text(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Text", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_G
        //9_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_H
        //9_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Type(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Type", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_I
        //9_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.DocId)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_E
        //9_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FriendlyUrl(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_FriendlyUrl", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_F
        //9_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Text(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Text", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_G
        //9_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_H
        //9_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Type(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Type", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_I
        //9_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStaticPages.IsDeleted)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_F
        //9_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_Text(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Text", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_G
        //9_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_Name(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Name", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_H
        //9_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_Type(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Type", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_I
        //9_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FriendlyUrl_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_FriendlyUrl, ConvertTo.ConvertEmptyToDBNull(oStaticPages.FriendlyUrl)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_G
        //9_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Name(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_Name", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_H
        //9_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_Type(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_Type", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_I
        //9_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Text_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Text, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Text)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_H
        //9_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Type(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name_Type", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_I
        //9_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Name)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_H_I
        //9_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_SeoDocId(StaticPages oStaticPages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_StaticPages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.WhiteLabelDocId)); 
db.AddParameter(TBNames_StaticPages.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oStaticPages.Type)); 
db.AddParameter(TBNames_StaticPages.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oStaticPages.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.StaticPages, "Select_StaticPages_By_Keys_View_WhiteLabelDocId_Type_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
