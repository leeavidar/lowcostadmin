

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Tip  : ContainerItem<Tip>{

#region CTOR

    #region Constractor
    static Tip()
    {
        ConvertEvent = Tip.OnConvert;
    }  
    //public KeyValuesContainer<Tip> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Tip()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.TipKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Tip OnConvert(DataRow dr)
    {
        int LangId = Translator<Tip>.LangId;            
        Tip oTip = null;
        if (dr != null)
        {
            oTip = new Tip();
            #region Create Object
            oTip.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Tip.Field_DateCreated]);
             oTip.DocId = ConvertTo.ConvertToInt(dr[TBNames_Tip.Field_DocId]);
             oTip.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Tip.Field_IsDeleted]);
             oTip.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Tip.Field_IsActive]);
 
//FK     KeyWhiteLabelDocId
            oTip.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Tip.Field_WhiteLabelDocId]);
             oTip.Title = ConvertTo.ConvertToString(dr[TBNames_Tip.Field_Title]);
             oTip.ShortText = ConvertTo.ConvertToString(dr[TBNames_Tip.Field_ShortText]);
             oTip.LongText = ConvertTo.ConvertToString(dr[TBNames_Tip.Field_LongText]);
             oTip.Order = ConvertTo.ConvertToInt(dr[TBNames_Tip.Field_Order]);
             oTip.Date = ConvertTo.ConvertToDateTime(dr[TBNames_Tip.Field_Date]);
 
            #endregion
            Translator<Tip>.Translate(oTip.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oTip;
    }

    
#endregion

//#REP_HERE 
#region Tip Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyTipDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTipDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyTipIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyTipIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTipTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

private bool isSetOnce_ShortText;
private string _short_text;
public String FriendlyShortText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipShortText);   
    }
}
public  string ShortText
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTipShortText));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipShortText, value);
         _short_text = ConvertToValue.ConvertToString(value);
        isSetOnce_ShortText = true;
    }
}


public string ShortText_Value
{
    get
    {
        //return _short_text; //ConvertToValue.ConvertToString(ShortText);
        if(isSetOnce_ShortText) {return _short_text;}
        else {return ConvertToValue.ConvertToString(ShortText);}
    }
}

public string ShortText_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_short_text).ToString();
               //if(isSetOnce_ShortText) {return ConvertToValue.ConvertToString(_short_text).ToString();}
               //else {return ConvertToValue.ConvertToString(ShortText).ToString();}
            ConvertToValue.ConvertToString(ShortText).ToString();
    }
}

private bool isSetOnce_LongText;
private string _long_text;
public String FriendlyLongText
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipLongText);   
    }
}
public  string LongText
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyTipLongText));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipLongText, value);
         _long_text = ConvertToValue.ConvertToString(value);
        isSetOnce_LongText = true;
    }
}


public string LongText_Value
{
    get
    {
        //return _long_text; //ConvertToValue.ConvertToString(LongText);
        if(isSetOnce_LongText) {return _long_text;}
        else {return ConvertToValue.ConvertToString(LongText);}
    }
}

public string LongText_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_long_text).ToString();
               //if(isSetOnce_LongText) {return ConvertToValue.ConvertToString(_long_text).ToString();}
               //else {return ConvertToValue.ConvertToString(LongText).ToString();}
            ConvertToValue.ConvertToString(LongText).ToString();
    }
}

private bool isSetOnce_Order;
private int _order;
public String FriendlyOrder
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipOrder);   
    }
}
public  int? Order
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyTipOrder));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipOrder, value);
         _order = ConvertToValue.ConvertToInt(value);
        isSetOnce_Order = true;
    }
}


public int Order_Value
{
    get
    {
        //return _order; //ConvertToValue.ConvertToInt(Order);
        if(isSetOnce_Order) {return _order;}
        else {return ConvertToValue.ConvertToInt(Order);}
    }
}

public string Order_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order).ToString();
               //if(isSetOnce_Order) {return ConvertToValue.ConvertToInt(_order).ToString();}
               //else {return ConvertToValue.ConvertToInt(Order).ToString();}
            ConvertToValue.ConvertToInt(Order).ToString();
    }
}

private bool isSetOnce_Date;
private DateTime _date;
public String FriendlyDate
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyTipDate);   
    }
}
public  DateTime? Date
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyTipDate));
    }
    set
    {
        SetKey(KeyValuesType.KeyTipDate, value);
         _date = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_Date = true;
    }
}


public DateTime Date_Value
{
    get
    {
        //return _date; //ConvertToValue.ConvertToDateTime(Date);
        if(isSetOnce_Date) {return _date;}
        else {return ConvertToValue.ConvertToDateTime(Date);}
    }
}

public string Date_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date).ToShortDateString();
               //if(isSetOnce_Date) {return ConvertToValue.ConvertToDateTime(_date).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(Date).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(Date).ToShortDateString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //E_A
        //4_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //E_B
        //4_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //E_C
        //4_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //E_H
        //4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LongText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //E_I
        //4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //E_J
        //4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Date", ex.Message));

            }
            return paramsSelect;
        }



        //E_A_B
        //4_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //E_A_C
        //4_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E_A_F
        //4_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //E_A_G
        //4_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ShortText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //E_A_H
        //4_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LongText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //E_A_I
        //4_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Order", ex.Message));

            }
            return paramsSelect;
        }



        //E_A_J
        //4_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oTip.DateCreated)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DateCreated_Date", ex.Message));

            }
            return paramsSelect;
        }



        //E_B_C
        //4_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E_B_F
        //4_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //E_B_G
        //4_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ShortText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //E_B_H
        //4_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LongText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //E_B_I
        //4_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //E_B_J
        //4_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oTip.DocId)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_DocId_Date", ex.Message));

            }
            return paramsSelect;
        }



        //E_C_F
        //4_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //E_C_G
        //4_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ShortText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //E_C_H
        //4_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LongText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //E_C_I
        //4_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Order", ex.Message));

            }
            return paramsSelect;
        }



        //E_C_J
        //4_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oTip.IsDeleted)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_IsDeleted_Date", ex.Message));

            }
            return paramsSelect;
        }



        //E_F_G
        //4_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_ShortText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Title_ShortText", ex.Message));

            }
            return paramsSelect;
        }



        //E_F_H
        //4_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_LongText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Title_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //E_F_I
        //4_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Order(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Title_Order", ex.Message));

            }
            return paramsSelect;
        }



        //E_F_J
        //4_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oTip.Title)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Title_Date", ex.Message));

            }
            return paramsSelect;
        }



        //E_G_H
        //4_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_LongText(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_LongText", ex.Message));

            }
            return paramsSelect;
        }



        //E_G_I
        //4_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_Order(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_Order", ex.Message));

            }
            return paramsSelect;
        }



        //E_G_J
        //4_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ShortText_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_ShortText, ConvertTo.ConvertEmptyToDBNull(oTip.ShortText)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_ShortText_Date", ex.Message));

            }
            return paramsSelect;
        }



        //E_H_I
        //4_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_Order(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_LongText_Order", ex.Message));

            }
            return paramsSelect;
        }



        //E_H_J
        //4_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LongText_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_LongText, ConvertTo.ConvertEmptyToDBNull(oTip.LongText)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_LongText_Date", ex.Message));

            }
            return paramsSelect;
        }



        //E_I_J
        //4_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Date(Tip oTip)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Tip.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oTip.WhiteLabelDocId)); 
db.AddParameter(TBNames_Tip.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oTip.Order)); 
db.AddParameter(TBNames_Tip.PRM_Date, ConvertTo.ConvertEmptyToDBNull(oTip.Date));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Tip, "Select_Tip_By_Keys_View_WhiteLabelDocId_Order_Date", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
