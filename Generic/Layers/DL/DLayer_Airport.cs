

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Airport  : ContainerItem<Airport>{

#region CTOR

    #region Constractor
    static Airport()
    {
        ConvertEvent = Airport.OnConvert;
    }  
    //public KeyValuesContainer<Airport> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Airport()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.AirportKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Airport OnConvert(DataRow dr)
    {
        int LangId = Translator<Airport>.LangId;            
        Airport oAirport = null;
        if (dr != null)
        {
            oAirport = new Airport();
            #region Create Object
            oAirport.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Airport.Field_DateCreated]);
             oAirport.DocId = ConvertTo.ConvertToInt(dr[TBNames_Airport.Field_DocId]);
             oAirport.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Airport.Field_IsDeleted]);
             oAirport.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Airport.Field_IsActive]);
             oAirport.IataCode = ConvertTo.ConvertToString(dr[TBNames_Airport.Field_IataCode]);
             oAirport.CityIataCode = ConvertTo.ConvertToString(dr[TBNames_Airport.Field_CityIataCode]);
             oAirport.EnglishName = ConvertTo.ConvertToString(dr[TBNames_Airport.Field_EnglishName]);
             oAirport.NameByLang = ConvertTo.ConvertToString(dr[TBNames_Airport.Field_NameByLang]);
 
            #endregion
            Translator<Airport>.Translate(oAirport.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oAirport;
    }

    
#endregion

//#REP_HERE 
#region Airport Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyAirportDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyAirportDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyAirportIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyAirportIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_IataCode;
private string _iata_code;
public String FriendlyIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportIataCode);   
    }
}
public  string IataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirportIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportIataCode, value);
         _iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_IataCode = true;
    }
}


public string IataCode_Value
{
    get
    {
        //return _iata_code; //ConvertToValue.ConvertToString(IataCode);
        if(isSetOnce_IataCode) {return _iata_code;}
        else {return ConvertToValue.ConvertToString(IataCode);}
    }
}

public string IataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_iata_code).ToString();
               //if(isSetOnce_IataCode) {return ConvertToValue.ConvertToString(_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(IataCode).ToString();}
            ConvertToValue.ConvertToString(IataCode).ToString();
    }
}

private bool isSetOnce_CityIataCode;
private string _city_iata_code;
public String FriendlyCityIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportCityIataCode);   
    }
}
public  string CityIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirportCityIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportCityIataCode, value);
         _city_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_CityIataCode = true;
    }
}


public string CityIataCode_Value
{
    get
    {
        //return _city_iata_code; //ConvertToValue.ConvertToString(CityIataCode);
        if(isSetOnce_CityIataCode) {return _city_iata_code;}
        else {return ConvertToValue.ConvertToString(CityIataCode);}
    }
}

public string CityIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_city_iata_code).ToString();
               //if(isSetOnce_CityIataCode) {return ConvertToValue.ConvertToString(_city_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(CityIataCode).ToString();}
            ConvertToValue.ConvertToString(CityIataCode).ToString();
    }
}

private bool isSetOnce_EnglishName;
private string _english_name;
public String FriendlyEnglishName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportEnglishName);   
    }
}
public  string EnglishName
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirportEnglishName));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportEnglishName, value);
         _english_name = ConvertToValue.ConvertToString(value);
        isSetOnce_EnglishName = true;
    }
}


public string EnglishName_Value
{
    get
    {
        //return _english_name; //ConvertToValue.ConvertToString(EnglishName);
        if(isSetOnce_EnglishName) {return _english_name;}
        else {return ConvertToValue.ConvertToString(EnglishName);}
    }
}

public string EnglishName_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_english_name).ToString();
               //if(isSetOnce_EnglishName) {return ConvertToValue.ConvertToString(_english_name).ToString();}
               //else {return ConvertToValue.ConvertToString(EnglishName).ToString();}
            ConvertToValue.ConvertToString(EnglishName).ToString();
    }
}

private bool isSetOnce_NameByLang;
private string _name_by_lang;
public String FriendlyNameByLang
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportNameByLang);   
    }
}
public  string NameByLang
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirportNameByLang));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportNameByLang, value);
         _name_by_lang = ConvertToValue.ConvertToString(value);
        isSetOnce_NameByLang = true;
    }
}


public string NameByLang_Value
{
    get
    {
        //return _name_by_lang; //ConvertToValue.ConvertToString(NameByLang);
        if(isSetOnce_NameByLang) {return _name_by_lang;}
        else {return ConvertToValue.ConvertToString(NameByLang);}
    }
}

public string NameByLang_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name_by_lang).ToString();
               //if(isSetOnce_NameByLang) {return ConvertToValue.ConvertToString(_name_by_lang).ToString();}
               //else {return ConvertToValue.ConvertToString(NameByLang).ToString();}
            ConvertToValue.ConvertToString(NameByLang).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirport.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirport.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirport.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CityIataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //G
        //6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_EnglishName(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oAirport.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //H
        //7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_NameByLang(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_NameByLang, ConvertTo.ConvertEmptyToDBNull(oAirport.NameByLang));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_NameByLang", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirport.DateCreated)); 
db.AddParameter(TBNames_Airport.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirport.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirport.DateCreated)); 
db.AddParameter(TBNames_Airport.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirport.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirport.DateCreated)); 
db.AddParameter(TBNames_Airport.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DateCreated_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_CityIataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirport.DateCreated)); 
db.AddParameter(TBNames_Airport.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DateCreated_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //A_G
        //0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_EnglishName(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirport.DateCreated)); 
db.AddParameter(TBNames_Airport.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oAirport.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DateCreated_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //A_H
        //0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_NameByLang(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirport.DateCreated)); 
db.AddParameter(TBNames_Airport.PRM_NameByLang, ConvertTo.ConvertEmptyToDBNull(oAirport.NameByLang));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DateCreated_NameByLang", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirport.DocId)); 
db.AddParameter(TBNames_Airport.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirport.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirport.DocId)); 
db.AddParameter(TBNames_Airport.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DocId_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_CityIataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirport.DocId)); 
db.AddParameter(TBNames_Airport.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DocId_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //B_G
        //1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_EnglishName(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirport.DocId)); 
db.AddParameter(TBNames_Airport.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oAirport.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DocId_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //B_H
        //1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_NameByLang(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirport.DocId)); 
db.AddParameter(TBNames_Airport.PRM_NameByLang, ConvertTo.ConvertEmptyToDBNull(oAirport.NameByLang));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_DocId_NameByLang", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_IataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirport.IsDeleted)); 
db.AddParameter(TBNames_Airport.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IsDeleted_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_CityIataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirport.IsDeleted)); 
db.AddParameter(TBNames_Airport.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IsDeleted_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //C_G
        //2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_EnglishName(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirport.IsDeleted)); 
db.AddParameter(TBNames_Airport.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oAirport.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IsDeleted_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //C_H
        //2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_NameByLang(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirport.IsDeleted)); 
db.AddParameter(TBNames_Airport.PRM_NameByLang, ConvertTo.ConvertEmptyToDBNull(oAirport.NameByLang));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IsDeleted_NameByLang", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode_CityIataCode(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.IataCode)); 
db.AddParameter(TBNames_Airport.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IataCode_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode_EnglishName(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.IataCode)); 
db.AddParameter(TBNames_Airport.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oAirport.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IataCode_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //E_H
        //4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IataCode_NameByLang(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.IataCode)); 
db.AddParameter(TBNames_Airport.PRM_NameByLang, ConvertTo.ConvertEmptyToDBNull(oAirport.NameByLang));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_IataCode_NameByLang", ex.Message));

            }
            return paramsSelect;
        }



        //F_G
        //5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CityIataCode_EnglishName(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.CityIataCode)); 
db.AddParameter(TBNames_Airport.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oAirport.EnglishName));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_CityIataCode_EnglishName", ex.Message));

            }
            return paramsSelect;
        }



        //F_H
        //5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_CityIataCode_NameByLang(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oAirport.CityIataCode)); 
db.AddParameter(TBNames_Airport.PRM_NameByLang, ConvertTo.ConvertEmptyToDBNull(oAirport.NameByLang));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_CityIataCode_NameByLang", ex.Message));

            }
            return paramsSelect;
        }



        //G_H
        //6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_EnglishName_NameByLang(Airport oAirport)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Airport.PRM_EnglishName, ConvertTo.ConvertEmptyToDBNull(oAirport.EnglishName)); 
db.AddParameter(TBNames_Airport.PRM_NameByLang, ConvertTo.ConvertEmptyToDBNull(oAirport.NameByLang));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Airport, "Select_Airport_By_Keys_View_EnglishName_NameByLang", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
