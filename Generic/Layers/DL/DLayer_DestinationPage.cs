

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class DestinationPage  : ContainerItem<DestinationPage>{

#region CTOR

    #region Constractor
    static DestinationPage()
    {
        ConvertEvent = DestinationPage.OnConvert;
    }  
    //public KeyValuesContainer<DestinationPage> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public DestinationPage()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.DestinationPageKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static DestinationPage OnConvert(DataRow dr)
    {
        int LangId = Translator<DestinationPage>.LangId;            
        DestinationPage oDestinationPage = null;
        if (dr != null)
        {
            oDestinationPage = new DestinationPage();
            #region Create Object
            oDestinationPage.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_DestinationPage.Field_DateCreated]);
             oDestinationPage.DocId = ConvertTo.ConvertToInt(dr[TBNames_DestinationPage.Field_DocId]);
             oDestinationPage.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_DestinationPage.Field_IsDeleted]);
             oDestinationPage.IsActive = ConvertTo.ConvertToBool(dr[TBNames_DestinationPage.Field_IsActive]);
             oDestinationPage.Order = ConvertTo.ConvertToInt(dr[TBNames_DestinationPage.Field_Order]);
             oDestinationPage.DestinationIataCode = ConvertTo.ConvertToString(dr[TBNames_DestinationPage.Field_DestinationIataCode]);
 
//FK     KeyWhiteLabelDocId
            oDestinationPage.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_DestinationPage.Field_WhiteLabelDocId]);
 
//FK     KeySeoDocId
            oDestinationPage.SeoDocId = ConvertTo.ConvertToInt(dr[TBNames_DestinationPage.Field_SeoDocId]);
             oDestinationPage.Price = ConvertTo.ConvertToDouble(dr[TBNames_DestinationPage.Field_Price]);
             oDestinationPage.LocalCurrency = ConvertTo.ConvertToString(dr[TBNames_DestinationPage.Field_LocalCurrency]);
             oDestinationPage.Name = ConvertTo.ConvertToString(dr[TBNames_DestinationPage.Field_Name]);
             oDestinationPage.Time = ConvertTo.ConvertToString(dr[TBNames_DestinationPage.Field_Time]);
             oDestinationPage.Title = ConvertTo.ConvertToString(dr[TBNames_DestinationPage.Field_Title]);
             oDestinationPage.PriceCurrency = ConvertTo.ConvertToString(dr[TBNames_DestinationPage.Field_PriceCurrency]);
 
            #endregion
            Translator<DestinationPage>.Translate(oDestinationPage.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oDestinationPage;
    }

    
#endregion

//#REP_HERE 
#region DestinationPage Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyDestinationPageDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyDestinationPageDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyDestinationPageIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyDestinationPageIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Order;
private int _order;
public String FriendlyOrder
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageOrder);   
    }
}
public  int? Order
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyDestinationPageOrder));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageOrder, value);
         _order = ConvertToValue.ConvertToInt(value);
        isSetOnce_Order = true;
    }
}


public int Order_Value
{
    get
    {
        //return _order; //ConvertToValue.ConvertToInt(Order);
        if(isSetOnce_Order) {return _order;}
        else {return ConvertToValue.ConvertToInt(Order);}
    }
}

public string Order_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order).ToString();
               //if(isSetOnce_Order) {return ConvertToValue.ConvertToInt(_order).ToString();}
               //else {return ConvertToValue.ConvertToInt(Order).ToString();}
            ConvertToValue.ConvertToInt(Order).ToString();
    }
}

private bool isSetOnce_DestinationIataCode;
private string _destination_iata_code;
public String FriendlyDestinationIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageDestinationIataCode);   
    }
}
public  string DestinationIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyDestinationPageDestinationIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageDestinationIataCode, value);
         _destination_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_DestinationIataCode = true;
    }
}


public string DestinationIataCode_Value
{
    get
    {
        //return _destination_iata_code; //ConvertToValue.ConvertToString(DestinationIataCode);
        if(isSetOnce_DestinationIataCode) {return _destination_iata_code;}
        else {return ConvertToValue.ConvertToString(DestinationIataCode);}
    }
}

public string DestinationIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_destination_iata_code).ToString();
               //if(isSetOnce_DestinationIataCode) {return ConvertToValue.ConvertToString(_destination_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(DestinationIataCode).ToString();}
            ConvertToValue.ConvertToString(DestinationIataCode).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_SeoDocId;
private int _seo_doc_id;
public String FriendlySeoDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeySeoDocId);   
    }
}
public  int? SeoDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeySeoDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeySeoDocId, value);
         _seo_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_SeoDocId = true;
    }
}


public int SeoDocId_Value
{
    get
    {
        //return _seo_doc_id; //ConvertToValue.ConvertToInt(SeoDocId);
        if(isSetOnce_SeoDocId) {return _seo_doc_id;}
        else {return ConvertToValue.ConvertToInt(SeoDocId);}
    }
}

public string SeoDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_seo_doc_id).ToString();
               //if(isSetOnce_SeoDocId) {return ConvertToValue.ConvertToInt(_seo_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(SeoDocId).ToString();}
            ConvertToValue.ConvertToInt(SeoDocId).ToString();
    }
}

private bool isSetOnce_Price;
private Double _price;
public String FriendlyPrice
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPagePrice);   
    }
}
public  Double? Price
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyDestinationPagePrice));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPagePrice, value);
         _price = ConvertToValue.ConvertToDouble(value);
        isSetOnce_Price = true;
    }
}


public Double Price_Value
{
    get
    {
        //return _price; //ConvertToValue.ConvertToDouble(Price);
        if(isSetOnce_Price) {return _price;}
        else {return ConvertToValue.ConvertToDouble(Price);}
    }
}

public string Price_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_price).ToString();
               //if(isSetOnce_Price) {return ConvertToValue.ConvertToDouble(_price).ToString();}
               //else {return ConvertToValue.ConvertToDouble(Price).ToString();}
            ConvertToValue.ConvertToDouble(Price).ToString();
    }
}

private bool isSetOnce_LocalCurrency;
private string _local_currency;
public String FriendlyLocalCurrency
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageLocalCurrency);   
    }
}
public  string LocalCurrency
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyDestinationPageLocalCurrency));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageLocalCurrency, value);
         _local_currency = ConvertToValue.ConvertToString(value);
        isSetOnce_LocalCurrency = true;
    }
}


public string LocalCurrency_Value
{
    get
    {
        //return _local_currency; //ConvertToValue.ConvertToString(LocalCurrency);
        if(isSetOnce_LocalCurrency) {return _local_currency;}
        else {return ConvertToValue.ConvertToString(LocalCurrency);}
    }
}

public string LocalCurrency_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_local_currency).ToString();
               //if(isSetOnce_LocalCurrency) {return ConvertToValue.ConvertToString(_local_currency).ToString();}
               //else {return ConvertToValue.ConvertToString(LocalCurrency).ToString();}
            ConvertToValue.ConvertToString(LocalCurrency).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyDestinationPageName));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_Time;
private string _time;
public String FriendlyTime
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageTime);   
    }
}
public  string Time
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyDestinationPageTime));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageTime, value);
         _time = ConvertToValue.ConvertToString(value);
        isSetOnce_Time = true;
    }
}


public string Time_Value
{
    get
    {
        //return _time; //ConvertToValue.ConvertToString(Time);
        if(isSetOnce_Time) {return _time;}
        else {return ConvertToValue.ConvertToString(Time);}
    }
}

public string Time_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_time).ToString();
               //if(isSetOnce_Time) {return ConvertToValue.ConvertToString(_time).ToString();}
               //else {return ConvertToValue.ConvertToString(Time).ToString();}
            ConvertToValue.ConvertToString(Time).ToString();
    }
}

private bool isSetOnce_Title;
private string _title;
public String FriendlyTitle
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPageTitle);   
    }
}
public  string Title
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyDestinationPageTitle));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPageTitle, value);
         _title = ConvertToValue.ConvertToString(value);
        isSetOnce_Title = true;
    }
}


public string Title_Value
{
    get
    {
        //return _title; //ConvertToValue.ConvertToString(Title);
        if(isSetOnce_Title) {return _title;}
        else {return ConvertToValue.ConvertToString(Title);}
    }
}

public string Title_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_title).ToString();
               //if(isSetOnce_Title) {return ConvertToValue.ConvertToString(_title).ToString();}
               //else {return ConvertToValue.ConvertToString(Title).ToString();}
            ConvertToValue.ConvertToString(Title).ToString();
    }
}

private bool isSetOnce_PriceCurrency;
private string _price_currency;
public String FriendlyPriceCurrency
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyDestinationPagePriceCurrency);   
    }
}
public  string PriceCurrency
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyDestinationPagePriceCurrency));
    }
    set
    {
        SetKey(KeyValuesType.KeyDestinationPagePriceCurrency, value);
         _price_currency = ConvertToValue.ConvertToString(value);
        isSetOnce_PriceCurrency = true;
    }
}


public string PriceCurrency_Value
{
    get
    {
        //return _price_currency; //ConvertToValue.ConvertToString(PriceCurrency);
        if(isSetOnce_PriceCurrency) {return _price_currency;}
        else {return ConvertToValue.ConvertToString(PriceCurrency);}
    }
}

public string PriceCurrency_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_price_currency).ToString();
               //if(isSetOnce_PriceCurrency) {return ConvertToValue.ConvertToString(_price_currency).ToString();}
               //else {return ConvertToValue.ConvertToString(PriceCurrency).ToString();}
            ConvertToValue.ConvertToString(PriceCurrency).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //G_A
        //6_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //G_B
        //6_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_C
        //6_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //G_E
        //6_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //G_F
        //6_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //G_H
        //6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_I
        //6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price", ex.Message));

            }
            return paramsSelect;
        }



        //G_J
        //6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_K
        //6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_L
        //6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_M
        //6_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_N
        //6_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_B
        //6_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_C
        //6_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_E
        //6_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Order(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Order", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_F
        //6_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DestinationIataCode(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_H
        //6_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SeoDocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_I
        //6_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Price(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Price", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_J
        //6_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_K
        //6_0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_L
        //6_0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_M
        //6_0_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_A_N
        //6_0_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DateCreated)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DateCreated_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_C
        //6_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_E
        //6_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Order(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Order", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_F
        //6_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DestinationIataCode(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_H
        //6_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SeoDocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_I
        //6_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Price(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Price", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_J
        //6_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_K
        //6_1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_L
        //6_1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_M
        //6_1_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_B_N
        //6_1_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DocId_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_E
        //6_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Order(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Order", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_F
        //6_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DestinationIataCode(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_H
        //6_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SeoDocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_I
        //6_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Price(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Price", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_J
        //6_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_K
        //6_2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_L
        //6_2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_M
        //6_2_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_C_N
        //6_2_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.IsDeleted)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_F
        //6_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_DestinationIataCode(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_DestinationIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_H
        //6_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_SeoDocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_I
        //6_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Price(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Price", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_J
        //6_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_K
        //6_4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_L
        //6_4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_M
        //6_4_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_E_N
        //6_4_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Order_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Order, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Order)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Order_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_H
        //6_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_SeoDocId(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_SeoDocId", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_I
        //6_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Price(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Price", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_J
        //6_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_K
        //6_5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_L
        //6_5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_M
        //6_5_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_F_N
        //6_5_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DestinationIataCode_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_DestinationIataCode, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.DestinationIataCode)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_DestinationIataCode_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_I
        //6_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Price(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Price", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_J
        //6_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_K
        //6_7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_L
        //6_7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_M
        //6_7_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_H_N
        //6_7_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SeoDocId_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_SeoDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.SeoDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_SeoDocId_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_I_J
        //6_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_LocalCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_LocalCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_I_K
        //6_8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_I_L
        //6_8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_I_M
        //6_8_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_I_N
        //6_8_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Price_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Price, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Price)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Price_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_J_K
        //6_9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_Name(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Name", ex.Message));

            }
            return paramsSelect;
        }



        //G_J_L
        //6_9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_J_M
        //6_9_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_J_N
        //6_9_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LocalCurrency_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_LocalCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.LocalCurrency)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_LocalCurrency_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_K_L
        //6_10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Time(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_Time", ex.Message));

            }
            return paramsSelect;
        }



        //G_K_M
        //6_10_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_K_N
        //6_10_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Name)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Name_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_L_M
        //6_11_12
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Time_Title(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time_Title", ex.Message));

            }
            return paramsSelect;
        }



        //G_L_N
        //6_11_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Time_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Time, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Time)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Time_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }



        //G_M_N
        //6_12_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Title_PriceCurrency(DestinationPage oDestinationPage)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_DestinationPage.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.WhiteLabelDocId)); 
db.AddParameter(TBNames_DestinationPage.PRM_Title, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.Title)); 
db.AddParameter(TBNames_DestinationPage.PRM_PriceCurrency, ConvertTo.ConvertEmptyToDBNull(oDestinationPage.PriceCurrency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.DestinationPage, "Select_DestinationPage_By_Keys_View_WhiteLabelDocId_Title_PriceCurrency", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
