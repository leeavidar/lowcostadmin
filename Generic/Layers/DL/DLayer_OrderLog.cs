

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class OrderLog  : ContainerItem<OrderLog>{

#region CTOR

    #region Constractor
    static OrderLog()
    {
        ConvertEvent = OrderLog.OnConvert;
    }  
    //public KeyValuesContainer<OrderLog> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public OrderLog()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.OrderLogKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static OrderLog OnConvert(DataRow dr)
    {
        int LangId = Translator<OrderLog>.LangId;            
        OrderLog oOrderLog = null;
        if (dr != null)
        {
            oOrderLog = new OrderLog();
            #region Create Object
            oOrderLog.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_OrderLog.Field_DateCreated]);
             oOrderLog.DocId = ConvertTo.ConvertToInt(dr[TBNames_OrderLog.Field_DocId]);
             oOrderLog.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_OrderLog.Field_IsDeleted]);
             oOrderLog.IsActive = ConvertTo.ConvertToBool(dr[TBNames_OrderLog.Field_IsActive]);
             oOrderLog.Type = ConvertTo.ConvertToInt(dr[TBNames_OrderLog.Field_Type]);
             oOrderLog.Status = ConvertTo.ConvertToInt(dr[TBNames_OrderLog.Field_Status]);
             oOrderLog.XML = ConvertTo.ConvertToString(dr[TBNames_OrderLog.Field_XML]);
 
//FK     KeyWhiteLabelDocId
            oOrderLog.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_OrderLog.Field_WhiteLabelDocId]);
 
//FK     KeyOrdersDocId
            oOrderLog.OrdersDocId = ConvertTo.ConvertToInt(dr[TBNames_OrderLog.Field_OrdersDocId]);
             oOrderLog.OrderStage = ConvertTo.ConvertToInt(dr[TBNames_OrderLog.Field_OrderStage]);
 
            #endregion
            Translator<OrderLog>.Translate(oOrderLog.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oOrderLog;
    }

    
#endregion

//#REP_HERE 
#region OrderLog Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyOrderLogDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrderLogDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrderLogIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrderLogIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Type;
private int _type;
public String FriendlyType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogType);   
    }
}
public  int? Type
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrderLogType));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogType, value);
         _type = ConvertToValue.ConvertToInt(value);
        isSetOnce_Type = true;
    }
}


public int Type_Value
{
    get
    {
        //return _type; //ConvertToValue.ConvertToInt(Type);
        if(isSetOnce_Type) {return _type;}
        else {return ConvertToValue.ConvertToInt(Type);}
    }
}

public string Type_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_type).ToString();
               //if(isSetOnce_Type) {return ConvertToValue.ConvertToInt(_type).ToString();}
               //else {return ConvertToValue.ConvertToInt(Type).ToString();}
            ConvertToValue.ConvertToInt(Type).ToString();
    }
}

private bool isSetOnce_Status;
private int _status;
public String FriendlyStatus
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogStatus);   
    }
}
public  int? Status
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrderLogStatus));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogStatus, value);
         _status = ConvertToValue.ConvertToInt(value);
        isSetOnce_Status = true;
    }
}


public int Status_Value
{
    get
    {
        //return _status; //ConvertToValue.ConvertToInt(Status);
        if(isSetOnce_Status) {return _status;}
        else {return ConvertToValue.ConvertToInt(Status);}
    }
}

public string Status_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_status).ToString();
               //if(isSetOnce_Status) {return ConvertToValue.ConvertToInt(_status).ToString();}
               //else {return ConvertToValue.ConvertToInt(Status).ToString();}
            ConvertToValue.ConvertToInt(Status).ToString();
    }
}

private bool isSetOnce_XML;
private string _xml;
public String FriendlyXML
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogXML);   
    }
}
public  string XML
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrderLogXML));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogXML, value);
         _xml = ConvertToValue.ConvertToString(value);
        isSetOnce_XML = true;
    }
}


public string XML_Value
{
    get
    {
        //return _xml; //ConvertToValue.ConvertToString(XML);
        if(isSetOnce_XML) {return _xml;}
        else {return ConvertToValue.ConvertToString(XML);}
    }
}

public string XML_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_xml).ToString();
               //if(isSetOnce_XML) {return ConvertToValue.ConvertToString(_xml).ToString();}
               //else {return ConvertToValue.ConvertToString(XML).ToString();}
            ConvertToValue.ConvertToString(XML).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_OrdersDocId;
private int _orders_doc_id;
public String FriendlyOrdersDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDocId);   
    }
}
public  int? OrdersDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDocId, value);
         _orders_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrdersDocId = true;
    }
}


public int OrdersDocId_Value
{
    get
    {
        //return _orders_doc_id; //ConvertToValue.ConvertToInt(OrdersDocId);
        if(isSetOnce_OrdersDocId) {return _orders_doc_id;}
        else {return ConvertToValue.ConvertToInt(OrdersDocId);}
    }
}

public string OrdersDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_orders_doc_id).ToString();
               //if(isSetOnce_OrdersDocId) {return ConvertToValue.ConvertToInt(_orders_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrdersDocId).ToString();}
            ConvertToValue.ConvertToInt(OrdersDocId).ToString();
    }
}

private bool isSetOnce_OrderStage;
private int _order_stage;
public String FriendlyOrderStage
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderLogOrderStage);   
    }
}
public  int? OrderStage
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrderLogOrderStage));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderLogOrderStage, value);
         _order_stage = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrderStage = true;
    }
}


public int OrderStage_Value
{
    get
    {
        //return _order_stage; //ConvertToValue.ConvertToInt(OrderStage);
        if(isSetOnce_OrderStage) {return _order_stage;}
        else {return ConvertToValue.ConvertToInt(OrderStage);}
    }
}

public string OrderStage_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order_stage).ToString();
               //if(isSetOnce_OrderStage) {return ConvertToValue.ConvertToInt(_order_stage).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrderStage).ToString();}
            ConvertToValue.ConvertToInt(OrderStage).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //H_A
        //7_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //H_B
        //7_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_C
        //7_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_E
        //7_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type", ex.Message));

            }
            return paramsSelect;
        }



        //H_F
        //7_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status", ex.Message));

            }
            return paramsSelect;
        }



        //H_G
        //7_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XML(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML", ex.Message));

            }
            return paramsSelect;
        }



        //H_I
        //7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_J
        //7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrderStage", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_B
        //7_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_C
        //7_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_E
        //7_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Type(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_Type", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_F
        //7_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Status(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_Status", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_G
        //7_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_XML(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_XML", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_I
        //7_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_J
        //7_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DateCreated)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DateCreated_OrderStage", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_C
        //7_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_E
        //7_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Type(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_Type", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_F
        //7_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Status(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_Status", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_G
        //7_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_XML(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_XML", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_I
        //7_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_J
        //7_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.DocId)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_DocId_OrderStage", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_E
        //7_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Type(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_Type", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_F
        //7_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Status(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_Status", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_G
        //7_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_XML(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_XML", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_I
        //7_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_J
        //7_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderLog.IsDeleted)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderStage", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_F
        //7_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_Status(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_Status", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_G
        //7_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_XML(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_XML", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_I
        //7_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_OrdersDocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_J
        //7_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Type_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Type, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Type)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Type_OrderStage", ex.Message));

            }
            return paramsSelect;
        }



        //H_F_G
        //7_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_XML(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_XML", ex.Message));

            }
            return paramsSelect;
        }



        //H_F_I
        //7_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OrdersDocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_F_J
        //7_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Status_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_Status, ConvertTo.ConvertEmptyToDBNull(oOrderLog.Status)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_Status_OrderStage", ex.Message));

            }
            return paramsSelect;
        }



        //H_G_I
        //7_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XML_OrdersDocId(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_G_J
        //7_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XML_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_XML, ConvertTo.ConvertEmptyToDBNull(oOrderLog.XML)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_XML_OrderStage", ex.Message));

            }
            return paramsSelect;
        }



        //H_I_J
        //7_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId_OrderStage(OrderLog oOrderLog)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderLog.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrdersDocId)); 
db.AddParameter(TBNames_OrderLog.PRM_OrderStage, ConvertTo.ConvertEmptyToDBNull(oOrderLog.OrderStage));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderLog, "Select_OrderLog_By_Keys_View_WhiteLabelDocId_OrdersDocId_OrderStage", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
