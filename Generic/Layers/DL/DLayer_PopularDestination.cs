

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class PopularDestination  : ContainerItem<PopularDestination>{

#region CTOR

    #region Constractor
    static PopularDestination()
    {
        ConvertEvent = PopularDestination.OnConvert;
    }  
    //public KeyValuesContainer<PopularDestination> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public PopularDestination()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.PopularDestinationKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static PopularDestination OnConvert(DataRow dr)
    {
        int LangId = Translator<PopularDestination>.LangId;            
        PopularDestination oPopularDestination = null;
        if (dr != null)
        {
            oPopularDestination = new PopularDestination();
            #region Create Object
            oPopularDestination.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_PopularDestination.Field_DateCreated]);
             oPopularDestination.DocId = ConvertTo.ConvertToInt(dr[TBNames_PopularDestination.Field_DocId]);
             oPopularDestination.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_PopularDestination.Field_IsDeleted]);
             oPopularDestination.IsActive = ConvertTo.ConvertToBool(dr[TBNames_PopularDestination.Field_IsActive]);
             oPopularDestination.IataCode = ConvertTo.ConvertToString(dr[TBNames_PopularDestination.Field_IataCode]);
             oPopularDestination.CityIataCode = ConvertTo.ConvertToString(dr[TBNames_PopularDestination.Field_CityIataCode]);
             oPopularDestination.Name = ConvertTo.ConvertToString(dr[TBNames_PopularDestination.Field_Name]);
 
//FK     KeyWhiteLabelDocId
            oPopularDestination.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_PopularDestination.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<PopularDestination>.Translate(oPopularDestination.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oPopularDestination;
    }

    
#endregion

//#REP_HERE 
#region PopularDestination Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyPopularDestinationDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyPopularDestinationDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyPopularDestinationIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyPopularDestinationIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_IataCode;
private string _iata_code;
public String FriendlyIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationIataCode);   
    }
}
public  string IataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPopularDestinationIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationIataCode, value);
         _iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_IataCode = true;
    }
}


public string IataCode_Value
{
    get
    {
        //return _iata_code; //ConvertToValue.ConvertToString(IataCode);
        if(isSetOnce_IataCode) {return _iata_code;}
        else {return ConvertToValue.ConvertToString(IataCode);}
    }
}

public string IataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_iata_code).ToString();
               //if(isSetOnce_IataCode) {return ConvertToValue.ConvertToString(_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(IataCode).ToString();}
            ConvertToValue.ConvertToString(IataCode).ToString();
    }
}

private bool isSetOnce_CityIataCode;
private string _city_iata_code;
public String FriendlyCityIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationCityIataCode);   
    }
}
public  string CityIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPopularDestinationCityIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationCityIataCode, value);
         _city_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_CityIataCode = true;
    }
}


public string CityIataCode_Value
{
    get
    {
        //return _city_iata_code; //ConvertToValue.ConvertToString(CityIataCode);
        if(isSetOnce_CityIataCode) {return _city_iata_code;}
        else {return ConvertToValue.ConvertToString(CityIataCode);}
    }
}

public string CityIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_city_iata_code).ToString();
               //if(isSetOnce_CityIataCode) {return ConvertToValue.ConvertToString(_city_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(CityIataCode).ToString();}
            ConvertToValue.ConvertToString(CityIataCode).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyPopularDestinationName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyPopularDestinationName));
    }
    set
    {
        SetKey(KeyValuesType.KeyPopularDestinationName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //H_A
        //7_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //H_B
        //7_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_C
        //7_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_E
        //7_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_F
        //7_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CityIataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_G
        //7_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_B
        //7_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DateCreated)); 
db.AddParameter(TBNames_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_C
        //7_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DateCreated)); 
db.AddParameter(TBNames_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_E
        //7_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DateCreated)); 
db.AddParameter(TBNames_PopularDestination.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_F
        //7_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_CityIataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DateCreated)); 
db.AddParameter(TBNames_PopularDestination.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_A_G
        //7_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DateCreated)); 
db.AddParameter(TBNames_PopularDestination.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_C
        //7_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_E
        //7_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_F
        //7_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_CityIataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_B_G
        //7_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.DocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_E
        //7_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_IataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IsDeleted)); 
db.AddParameter(TBNames_PopularDestination.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_IataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_F
        //7_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_CityIataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IsDeleted)); 
db.AddParameter(TBNames_PopularDestination.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_C_G
        //7_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IsDeleted)); 
db.AddParameter(TBNames_PopularDestination.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_F
        //7_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IataCode_CityIataCode(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IataCode)); 
db.AddParameter(TBNames_PopularDestination.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.CityIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode_CityIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //H_E_G
        //7_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IataCode_Name(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_IataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.IataCode)); 
db.AddParameter(TBNames_PopularDestination.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_IataCode_Name", ex.Message));

            }
            return paramsSelect;
        }



        //H_F_G
        //7_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_CityIataCode_Name(PopularDestination oPopularDestination)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_PopularDestination.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.WhiteLabelDocId)); 
db.AddParameter(TBNames_PopularDestination.PRM_CityIataCode, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.CityIataCode)); 
db.AddParameter(TBNames_PopularDestination.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oPopularDestination.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.PopularDestination, "Select_PopularDestination_By_Keys_View_WhiteLabelDocId_CityIataCode_Name", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
