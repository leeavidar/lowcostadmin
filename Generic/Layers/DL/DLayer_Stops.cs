

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Stops  : ContainerItem<Stops>{

#region CTOR

    #region Constractor
    static Stops()
    {
        ConvertEvent = Stops.OnConvert;
    }  
    //public KeyValuesContainer<Stops> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Stops()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.StopsKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Stops OnConvert(DataRow dr)
    {
        int LangId = Translator<Stops>.LangId;            
        Stops oStops = null;
        if (dr != null)
        {
            oStops = new Stops();
            #region Create Object
            oStops.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Stops.Field_DateCreated]);
             oStops.DocId = ConvertTo.ConvertToInt(dr[TBNames_Stops.Field_DocId]);
             oStops.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Stops.Field_IsDeleted]);
             oStops.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Stops.Field_IsActive]);
             oStops.Arrival = ConvertTo.ConvertToDateTime(dr[TBNames_Stops.Field_Arrival]);
             oStops.Departure = ConvertTo.ConvertToDateTime(dr[TBNames_Stops.Field_Departure]);
 
//FK     KeyFlightLegDocId
            oStops.FlightLegDocId = ConvertTo.ConvertToInt(dr[TBNames_Stops.Field_FlightLegDocId]);
 
//FK     KeyAirportIataCode
            oStops.AirportIataCode = ConvertTo.ConvertToString(dr[TBNames_Stops.Field_AirportIataCode]);
 
            #endregion
            Translator<Stops>.Translate(oStops.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oStops;
    }

    
#endregion

//#REP_HERE 
#region Stops Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStopsDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyStopsDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyStopsDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStopsDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyStopsDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyStopsDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStopsIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStopsIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyStopsIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStopsIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyStopsIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyStopsIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_Arrival;
private DateTime _arrival;
public String FriendlyArrival
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStopsArrival);   
    }
}
public  DateTime? Arrival
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyStopsArrival));
    }
    set
    {
        SetKey(KeyValuesType.KeyStopsArrival, value);
         _arrival = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_Arrival = true;
    }
}


public DateTime Arrival_Value
{
    get
    {
        //return _arrival; //ConvertToValue.ConvertToDateTime(Arrival);
        if(isSetOnce_Arrival) {return _arrival;}
        else {return ConvertToValue.ConvertToDateTime(Arrival);}
    }
}

public string Arrival_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_arrival).ToShortDateString();
               //if(isSetOnce_Arrival) {return ConvertToValue.ConvertToDateTime(_arrival).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(Arrival).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(Arrival).ToShortDateString();
    }
}

private bool isSetOnce_Departure;
private DateTime _departure;
public String FriendlyDeparture
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyStopsDeparture);   
    }
}
public  DateTime? Departure
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyStopsDeparture));
    }
    set
    {
        SetKey(KeyValuesType.KeyStopsDeparture, value);
         _departure = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_Departure = true;
    }
}


public DateTime Departure_Value
{
    get
    {
        //return _departure; //ConvertToValue.ConvertToDateTime(Departure);
        if(isSetOnce_Departure) {return _departure;}
        else {return ConvertToValue.ConvertToDateTime(Departure);}
    }
}

public string Departure_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_departure).ToShortDateString();
               //if(isSetOnce_Departure) {return ConvertToValue.ConvertToDateTime(_departure).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(Departure).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(Departure).ToShortDateString();
    }
}

private bool isSetOnce_FlightLegDocId;
private int _flight_leg_doc_id;
public String FriendlyFlightLegDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyFlightLegDocId);   
    }
}
public  int? FlightLegDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyFlightLegDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyFlightLegDocId, value);
         _flight_leg_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_FlightLegDocId = true;
    }
}


public int FlightLegDocId_Value
{
    get
    {
        //return _flight_leg_doc_id; //ConvertToValue.ConvertToInt(FlightLegDocId);
        if(isSetOnce_FlightLegDocId) {return _flight_leg_doc_id;}
        else {return ConvertToValue.ConvertToInt(FlightLegDocId);}
    }
}

public string FlightLegDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_flight_leg_doc_id).ToString();
               //if(isSetOnce_FlightLegDocId) {return ConvertToValue.ConvertToInt(_flight_leg_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(FlightLegDocId).ToString();}
            ConvertToValue.ConvertToInt(FlightLegDocId).ToString();
    }
}

private bool isSetOnce_AirportIataCode;
private string _airport_iata_code;
public String FriendlyAirportIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirportIataCode);   
    }
}
public  string AirportIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirportIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirportIataCode, value);
         _airport_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_AirportIataCode = true;
    }
}


public string AirportIataCode_Value
{
    get
    {
        //return _airport_iata_code; //ConvertToValue.ConvertToString(AirportIataCode);
        if(isSetOnce_AirportIataCode) {return _airport_iata_code;}
        else {return ConvertToValue.ConvertToString(AirportIataCode);}
    }
}

public string AirportIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_airport_iata_code).ToString();
               //if(isSetOnce_AirportIataCode) {return ConvertToValue.ConvertToString(_airport_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(AirportIataCode).ToString();}
            ConvertToValue.ConvertToString(AirportIataCode).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //A
        //0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStops.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //B
        //1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStops.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //C
        //2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStops.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //E
        //4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Arrival(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_Arrival, ConvertTo.ConvertEmptyToDBNull(oStops.Arrival));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_Arrival", ex.Message));

            }
            return paramsSelect;
        }



        //F
        //5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Departure(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_Departure, ConvertTo.ConvertEmptyToDBNull(oStops.Departure));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_Departure", ex.Message));

            }
            return paramsSelect;
        }



        //G
        //6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_FlightLegDocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oStops.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //H
        //7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_AirportIataCode(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_AirportIataCode, ConvertTo.ConvertEmptyToDBNull(oStops.AirportIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_AirportIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //A_B
        //0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_DocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStops.DateCreated)); 
db.AddParameter(TBNames_Stops.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStops.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_C
        //0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_IsDeleted(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStops.DateCreated)); 
db.AddParameter(TBNames_Stops.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStops.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //A_E
        //0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Arrival(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStops.DateCreated)); 
db.AddParameter(TBNames_Stops.PRM_Arrival, ConvertTo.ConvertEmptyToDBNull(oStops.Arrival));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DateCreated_Arrival", ex.Message));

            }
            return paramsSelect;
        }



        //A_F
        //0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_Departure(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStops.DateCreated)); 
db.AddParameter(TBNames_Stops.PRM_Departure, ConvertTo.ConvertEmptyToDBNull(oStops.Departure));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DateCreated_Departure", ex.Message));

            }
            return paramsSelect;
        }



        //A_G
        //0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_FlightLegDocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStops.DateCreated)); 
db.AddParameter(TBNames_Stops.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oStops.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DateCreated_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //A_H
        //0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DateCreated_AirportIataCode(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oStops.DateCreated)); 
db.AddParameter(TBNames_Stops.PRM_AirportIataCode, ConvertTo.ConvertEmptyToDBNull(oStops.AirportIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DateCreated_AirportIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //B_C
        //1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_IsDeleted(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStops.DocId)); 
db.AddParameter(TBNames_Stops.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStops.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //B_E
        //1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Arrival(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStops.DocId)); 
db.AddParameter(TBNames_Stops.PRM_Arrival, ConvertTo.ConvertEmptyToDBNull(oStops.Arrival));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DocId_Arrival", ex.Message));

            }
            return paramsSelect;
        }



        //B_F
        //1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_Departure(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStops.DocId)); 
db.AddParameter(TBNames_Stops.PRM_Departure, ConvertTo.ConvertEmptyToDBNull(oStops.Departure));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DocId_Departure", ex.Message));

            }
            return paramsSelect;
        }



        //B_G
        //1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_FlightLegDocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStops.DocId)); 
db.AddParameter(TBNames_Stops.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oStops.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DocId_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //B_H
        //1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_DocId_AirportIataCode(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oStops.DocId)); 
db.AddParameter(TBNames_Stops.PRM_AirportIataCode, ConvertTo.ConvertEmptyToDBNull(oStops.AirportIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_DocId_AirportIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //C_E
        //2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Arrival(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStops.IsDeleted)); 
db.AddParameter(TBNames_Stops.PRM_Arrival, ConvertTo.ConvertEmptyToDBNull(oStops.Arrival));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_IsDeleted_Arrival", ex.Message));

            }
            return paramsSelect;
        }



        //C_F
        //2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_Departure(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStops.IsDeleted)); 
db.AddParameter(TBNames_Stops.PRM_Departure, ConvertTo.ConvertEmptyToDBNull(oStops.Departure));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_IsDeleted_Departure", ex.Message));

            }
            return paramsSelect;
        }



        //C_G
        //2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_FlightLegDocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStops.IsDeleted)); 
db.AddParameter(TBNames_Stops.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oStops.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_IsDeleted_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //C_H
        //2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_IsDeleted_AirportIataCode(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oStops.IsDeleted)); 
db.AddParameter(TBNames_Stops.PRM_AirportIataCode, ConvertTo.ConvertEmptyToDBNull(oStops.AirportIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_IsDeleted_AirportIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //E_F
        //4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Arrival_Departure(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_Arrival, ConvertTo.ConvertEmptyToDBNull(oStops.Arrival)); 
db.AddParameter(TBNames_Stops.PRM_Departure, ConvertTo.ConvertEmptyToDBNull(oStops.Departure));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_Arrival_Departure", ex.Message));

            }
            return paramsSelect;
        }



        //E_G
        //4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Arrival_FlightLegDocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_Arrival, ConvertTo.ConvertEmptyToDBNull(oStops.Arrival)); 
db.AddParameter(TBNames_Stops.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oStops.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_Arrival_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //E_H
        //4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Arrival_AirportIataCode(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_Arrival, ConvertTo.ConvertEmptyToDBNull(oStops.Arrival)); 
db.AddParameter(TBNames_Stops.PRM_AirportIataCode, ConvertTo.ConvertEmptyToDBNull(oStops.AirportIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_Arrival_AirportIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //F_G
        //5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Departure_FlightLegDocId(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_Departure, ConvertTo.ConvertEmptyToDBNull(oStops.Departure)); 
db.AddParameter(TBNames_Stops.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oStops.FlightLegDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_Departure_FlightLegDocId", ex.Message));

            }
            return paramsSelect;
        }



        //F_H
        //5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_Departure_AirportIataCode(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_Departure, ConvertTo.ConvertEmptyToDBNull(oStops.Departure)); 
db.AddParameter(TBNames_Stops.PRM_AirportIataCode, ConvertTo.ConvertEmptyToDBNull(oStops.AirportIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_Departure_AirportIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //G_H
        //6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_FlightLegDocId_AirportIataCode(Stops oStops)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Stops.PRM_FlightLegDocId, ConvertTo.ConvertEmptyToDBNull(oStops.FlightLegDocId)); 
db.AddParameter(TBNames_Stops.PRM_AirportIataCode, ConvertTo.ConvertEmptyToDBNull(oStops.AirportIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Stops, "Select_Stops_By_Keys_View_FlightLegDocId_AirportIataCode", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
