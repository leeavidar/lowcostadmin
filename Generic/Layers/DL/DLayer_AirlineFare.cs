

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class AirlineFare  : ContainerItem<AirlineFare>{

#region CTOR

    #region Constractor
    static AirlineFare()
    {
        ConvertEvent = AirlineFare.OnConvert;
    }  
    //public KeyValuesContainer<AirlineFare> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public AirlineFare()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.AirlineFareKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static AirlineFare OnConvert(DataRow dr)
    {
        int LangId = Translator<AirlineFare>.LangId;            
        AirlineFare oAirlineFare = null;
        if (dr != null)
        {
            oAirlineFare = new AirlineFare();
            #region Create Object
            oAirlineFare.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_AirlineFare.Field_DateCreated]);
             oAirlineFare.DocId = ConvertTo.ConvertToInt(dr[TBNames_AirlineFare.Field_DocId]);
             oAirlineFare.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_AirlineFare.Field_IsDeleted]);
             oAirlineFare.IsActive = ConvertTo.ConvertToBool(dr[TBNames_AirlineFare.Field_IsActive]);
             oAirlineFare.AirlineCode = ConvertTo.ConvertToString(dr[TBNames_AirlineFare.Field_AirlineCode]);
             oAirlineFare.Name = ConvertTo.ConvertToString(dr[TBNames_AirlineFare.Field_Name]);
             oAirlineFare.Standard = ConvertTo.ConvertToString(dr[TBNames_AirlineFare.Field_Standard]);
             oAirlineFare.Flexible = ConvertTo.ConvertToString(dr[TBNames_AirlineFare.Field_Flexible]);
 
//FK     KeyWhiteLabelDocId
            oAirlineFare.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_AirlineFare.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<AirlineFare>.Translate(oAirlineFare.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oAirlineFare;
    }

    
#endregion

//#REP_HERE 
#region AirlineFare Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyAirlineFareDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyAirlineFareDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyAirlineFareIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyAirlineFareIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_AirlineCode;
private string _airline_code;
public String FriendlyAirlineCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareAirlineCode);   
    }
}
public  string AirlineCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirlineFareAirlineCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareAirlineCode, value);
         _airline_code = ConvertToValue.ConvertToString(value);
        isSetOnce_AirlineCode = true;
    }
}


public string AirlineCode_Value
{
    get
    {
        //return _airline_code; //ConvertToValue.ConvertToString(AirlineCode);
        if(isSetOnce_AirlineCode) {return _airline_code;}
        else {return ConvertToValue.ConvertToString(AirlineCode);}
    }
}

public string AirlineCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_airline_code).ToString();
               //if(isSetOnce_AirlineCode) {return ConvertToValue.ConvertToString(_airline_code).ToString();}
               //else {return ConvertToValue.ConvertToString(AirlineCode).ToString();}
            ConvertToValue.ConvertToString(AirlineCode).ToString();
    }
}

private bool isSetOnce_Name;
private string _name;
public String FriendlyName
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareName);   
    }
}
public  string Name
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirlineFareName));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareName, value);
         _name = ConvertToValue.ConvertToString(value);
        isSetOnce_Name = true;
    }
}


public string Name_Value
{
    get
    {
        //return _name; //ConvertToValue.ConvertToString(Name);
        if(isSetOnce_Name) {return _name;}
        else {return ConvertToValue.ConvertToString(Name);}
    }
}

public string Name_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_name).ToString();
               //if(isSetOnce_Name) {return ConvertToValue.ConvertToString(_name).ToString();}
               //else {return ConvertToValue.ConvertToString(Name).ToString();}
            ConvertToValue.ConvertToString(Name).ToString();
    }
}

private bool isSetOnce_Standard;
private string _standard;
public String FriendlyStandard
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareStandard);   
    }
}
public  string Standard
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirlineFareStandard));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareStandard, value);
         _standard = ConvertToValue.ConvertToString(value);
        isSetOnce_Standard = true;
    }
}


public string Standard_Value
{
    get
    {
        //return _standard; //ConvertToValue.ConvertToString(Standard);
        if(isSetOnce_Standard) {return _standard;}
        else {return ConvertToValue.ConvertToString(Standard);}
    }
}

public string Standard_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_standard).ToString();
               //if(isSetOnce_Standard) {return ConvertToValue.ConvertToString(_standard).ToString();}
               //else {return ConvertToValue.ConvertToString(Standard).ToString();}
            ConvertToValue.ConvertToString(Standard).ToString();
    }
}

private bool isSetOnce_Flexible;
private string _flexible;
public String FriendlyFlexible
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyAirlineFareFlexible);   
    }
}
public  string Flexible
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyAirlineFareFlexible));
    }
    set
    {
        SetKey(KeyValuesType.KeyAirlineFareFlexible, value);
         _flexible = ConvertToValue.ConvertToString(value);
        isSetOnce_Flexible = true;
    }
}


public string Flexible_Value
{
    get
    {
        //return _flexible; //ConvertToValue.ConvertToString(Flexible);
        if(isSetOnce_Flexible) {return _flexible;}
        else {return ConvertToValue.ConvertToString(Flexible);}
    }
}

public string Flexible_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_flexible).ToString();
               //if(isSetOnce_Flexible) {return ConvertToValue.ConvertToString(_flexible).ToString();}
               //else {return ConvertToValue.ConvertToString(Flexible).ToString();}
            ConvertToValue.ConvertToString(Flexible).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //I_A
        //8_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //I_B
        //8_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C
        //8_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_E
        //8_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_AirlineCode, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.AirlineCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode", ex.Message));

            }
            return paramsSelect;
        }



        //I_F
        //8_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //I_G
        //8_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Standard(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Standard, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Standard));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Standard", ex.Message));

            }
            return paramsSelect;
        }



        //I_H
        //8_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Flexible(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Flexible, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Flexible));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Flexible", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_B
        //8_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DateCreated)); 
db.AddParameter(TBNames_AirlineFare.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_C
        //8_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DateCreated)); 
db.AddParameter(TBNames_AirlineFare.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_E
        //8_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineCode(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DateCreated)); 
db.AddParameter(TBNames_AirlineFare.PRM_AirlineCode, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.AirlineCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineCode", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_F
        //8_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Name(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DateCreated)); 
db.AddParameter(TBNames_AirlineFare.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Name", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_G
        //8_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Standard(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DateCreated)); 
db.AddParameter(TBNames_AirlineFare.PRM_Standard, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Standard));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Standard", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_H
        //8_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Flexible(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DateCreated)); 
db.AddParameter(TBNames_AirlineFare.PRM_Flexible, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Flexible));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DateCreated_Flexible", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_C
        //8_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_E
        //8_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineCode(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_AirlineCode, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.AirlineCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_AirlineCode", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_F
        //8_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Name(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Name", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_G
        //8_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Standard(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Standard, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Standard));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Standard", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_H
        //8_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Flexible(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.DocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Flexible, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Flexible));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_DocId_Flexible", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_E
        //8_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineCode(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.IsDeleted)); 
db.AddParameter(TBNames_AirlineFare.PRM_AirlineCode, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.AirlineCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineCode", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_F
        //8_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Name(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.IsDeleted)); 
db.AddParameter(TBNames_AirlineFare.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Name", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_G
        //8_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Standard(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.IsDeleted)); 
db.AddParameter(TBNames_AirlineFare.PRM_Standard, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Standard));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Standard", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_H
        //8_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Flexible(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.IsDeleted)); 
db.AddParameter(TBNames_AirlineFare.PRM_Flexible, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Flexible));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_IsDeleted_Flexible", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_F
        //8_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode_Name(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_AirlineCode, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.AirlineCode)); 
db.AddParameter(TBNames_AirlineFare.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Name));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Name", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_G
        //8_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode_Standard(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_AirlineCode, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.AirlineCode)); 
db.AddParameter(TBNames_AirlineFare.PRM_Standard, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Standard));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Standard", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_H
        //8_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineCode_Flexible(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_AirlineCode, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.AirlineCode)); 
db.AddParameter(TBNames_AirlineFare.PRM_Flexible, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Flexible));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_AirlineCode_Flexible", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_G
        //8_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Standard(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Name)); 
db.AddParameter(TBNames_AirlineFare.PRM_Standard, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Standard));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name_Standard", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_H
        //8_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Name_Flexible(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Name, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Name)); 
db.AddParameter(TBNames_AirlineFare.PRM_Flexible, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Flexible));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Name_Flexible", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_H
        //8_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Standard_Flexible(AirlineFare oAirlineFare)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_AirlineFare.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.WhiteLabelDocId)); 
db.AddParameter(TBNames_AirlineFare.PRM_Standard, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Standard)); 
db.AddParameter(TBNames_AirlineFare.PRM_Flexible, ConvertTo.ConvertEmptyToDBNull(oAirlineFare.Flexible));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.AirlineFare, "Select_AirlineFare_By_Keys_View_WhiteLabelDocId_Standard_Flexible", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
