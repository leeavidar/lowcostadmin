

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class OrderFlightLeg  : ContainerItem<OrderFlightLeg>{

#region CTOR

    #region Constractor
    static OrderFlightLeg()
    {
        ConvertEvent = OrderFlightLeg.OnConvert;
    }  
    //public KeyValuesContainer<OrderFlightLeg> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public OrderFlightLeg()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.OrderFlightLegKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static OrderFlightLeg OnConvert(DataRow dr)
    {
        int LangId = Translator<OrderFlightLeg>.LangId;            
        OrderFlightLeg oOrderFlightLeg = null;
        if (dr != null)
        {
            oOrderFlightLeg = new OrderFlightLeg();
            #region Create Object
            oOrderFlightLeg.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_OrderFlightLeg.Field_DateCreated]);
             oOrderFlightLeg.DocId = ConvertTo.ConvertToInt(dr[TBNames_OrderFlightLeg.Field_DocId]);
             oOrderFlightLeg.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_OrderFlightLeg.Field_IsDeleted]);
             oOrderFlightLeg.IsActive = ConvertTo.ConvertToBool(dr[TBNames_OrderFlightLeg.Field_IsActive]);
             oOrderFlightLeg.LowCostPNR = ConvertTo.ConvertToString(dr[TBNames_OrderFlightLeg.Field_LowCostPNR]);
             oOrderFlightLeg.PriceNet = ConvertTo.ConvertToDouble(dr[TBNames_OrderFlightLeg.Field_PriceNet]);
             oOrderFlightLeg.PriceMarkUp = ConvertTo.ConvertToDouble(dr[TBNames_OrderFlightLeg.Field_PriceMarkUp]);
             oOrderFlightLeg.FlightType = ConvertTo.ConvertToString(dr[TBNames_OrderFlightLeg.Field_FlightType]);
 
//FK     KeyWhiteLabelDocId
            oOrderFlightLeg.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_OrderFlightLeg.Field_WhiteLabelDocId]);
 
//FK     KeyOrdersDocId
            oOrderFlightLeg.OrdersDocId = ConvertTo.ConvertToInt(dr[TBNames_OrderFlightLeg.Field_OrdersDocId]);
 
            #endregion
            Translator<OrderFlightLeg>.Translate(oOrderFlightLeg.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oOrderFlightLeg;
    }

    
#endregion

//#REP_HERE 
#region OrderFlightLeg Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyOrderFlightLegDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrderFlightLegDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrderFlightLegIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrderFlightLegIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_LowCostPNR;
private string _low_cost_pnr;
public String FriendlyLowCostPNR
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegLowCostPNR);   
    }
}
public  string LowCostPNR
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrderFlightLegLowCostPNR));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegLowCostPNR, value);
         _low_cost_pnr = ConvertToValue.ConvertToString(value);
        isSetOnce_LowCostPNR = true;
    }
}


public string LowCostPNR_Value
{
    get
    {
        //return _low_cost_pnr; //ConvertToValue.ConvertToString(LowCostPNR);
        if(isSetOnce_LowCostPNR) {return _low_cost_pnr;}
        else {return ConvertToValue.ConvertToString(LowCostPNR);}
    }
}

public string LowCostPNR_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_low_cost_pnr).ToString();
               //if(isSetOnce_LowCostPNR) {return ConvertToValue.ConvertToString(_low_cost_pnr).ToString();}
               //else {return ConvertToValue.ConvertToString(LowCostPNR).ToString();}
            ConvertToValue.ConvertToString(LowCostPNR).ToString();
    }
}

private bool isSetOnce_PriceNet;
private Double _price_net;
public String FriendlyPriceNet
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegPriceNet);   
    }
}
public  Double? PriceNet
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyOrderFlightLegPriceNet));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegPriceNet, value);
         _price_net = ConvertToValue.ConvertToDouble(value);
        isSetOnce_PriceNet = true;
    }
}


public Double PriceNet_Value
{
    get
    {
        //return _price_net; //ConvertToValue.ConvertToDouble(PriceNet);
        if(isSetOnce_PriceNet) {return _price_net;}
        else {return ConvertToValue.ConvertToDouble(PriceNet);}
    }
}

public string PriceNet_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_price_net).ToString();
               //if(isSetOnce_PriceNet) {return ConvertToValue.ConvertToDouble(_price_net).ToString();}
               //else {return ConvertToValue.ConvertToDouble(PriceNet).ToString();}
            ConvertToValue.ConvertToDouble(PriceNet).ToString();
    }
}

private bool isSetOnce_PriceMarkUp;
private Double _price_mark_up;
public String FriendlyPriceMarkUp
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegPriceMarkUp);   
    }
}
public  Double? PriceMarkUp
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyOrderFlightLegPriceMarkUp));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegPriceMarkUp, value);
         _price_mark_up = ConvertToValue.ConvertToDouble(value);
        isSetOnce_PriceMarkUp = true;
    }
}


public Double PriceMarkUp_Value
{
    get
    {
        //return _price_mark_up; //ConvertToValue.ConvertToDouble(PriceMarkUp);
        if(isSetOnce_PriceMarkUp) {return _price_mark_up;}
        else {return ConvertToValue.ConvertToDouble(PriceMarkUp);}
    }
}

public string PriceMarkUp_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_price_mark_up).ToString();
               //if(isSetOnce_PriceMarkUp) {return ConvertToValue.ConvertToDouble(_price_mark_up).ToString();}
               //else {return ConvertToValue.ConvertToDouble(PriceMarkUp).ToString();}
            ConvertToValue.ConvertToDouble(PriceMarkUp).ToString();
    }
}

private bool isSetOnce_FlightType;
private string _flight_type;
public String FriendlyFlightType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrderFlightLegFlightType);   
    }
}
public  string FlightType
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrderFlightLegFlightType));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrderFlightLegFlightType, value);
         _flight_type = ConvertToValue.ConvertToString(value);
        isSetOnce_FlightType = true;
    }
}


public string FlightType_Value
{
    get
    {
        //return _flight_type; //ConvertToValue.ConvertToString(FlightType);
        if(isSetOnce_FlightType) {return _flight_type;}
        else {return ConvertToValue.ConvertToString(FlightType);}
    }
}

public string FlightType_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_flight_type).ToString();
               //if(isSetOnce_FlightType) {return ConvertToValue.ConvertToString(_flight_type).ToString();}
               //else {return ConvertToValue.ConvertToString(FlightType).ToString();}
            ConvertToValue.ConvertToString(FlightType).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_OrdersDocId;
private int _orders_doc_id;
public String FriendlyOrdersDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDocId);   
    }
}
public  int? OrdersDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDocId, value);
         _orders_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrdersDocId = true;
    }
}


public int OrdersDocId_Value
{
    get
    {
        //return _orders_doc_id; //ConvertToValue.ConvertToInt(OrdersDocId);
        if(isSetOnce_OrdersDocId) {return _orders_doc_id;}
        else {return ConvertToValue.ConvertToInt(OrdersDocId);}
    }
}

public string OrdersDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_orders_doc_id).ToString();
               //if(isSetOnce_OrdersDocId) {return ConvertToValue.ConvertToInt(_orders_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrdersDocId).ToString();}
            ConvertToValue.ConvertToInt(OrdersDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //I_A
        //8_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //I_B
        //8_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C
        //8_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_E
        //8_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //I_F
        //8_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet", ex.Message));

            }
            return paramsSelect;
        }



        //I_G
        //8_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //I_H
        //8_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //I_J
        //8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_B
        //8_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_C
        //8_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_E
        //8_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_F
        //8_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceNet(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PriceNet", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_G
        //8_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PriceMarkUp(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_H
        //8_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_FlightType(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //I_A_J
        //8_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DateCreated)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_C
        //8_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_E
        //8_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_F
        //8_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceNet(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PriceNet", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_G
        //8_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PriceMarkUp(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_H
        //8_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_FlightType(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //I_B_J
        //8_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.DocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_E
        //8_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_F
        //8_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceNet(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceNet", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_G
        //8_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PriceMarkUp(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_H
        //8_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_FlightType(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //I_C_J
        //8_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.IsDeleted)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_F
        //8_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PriceNet(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_PriceNet", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_G
        //8_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PriceMarkUp(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_H
        //8_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_FlightType(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //I_E_J
        //8_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.LowCostPNR)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_G
        //8_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet_PriceMarkUp(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_PriceMarkUp", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_H
        //8_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet_FlightType(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //I_F_J
        //8_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceNet_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceNet, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceNet)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceNet_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_H
        //8_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_FlightType(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp_FlightType", ex.Message));

            }
            return paramsSelect;
        }



        //I_G_J
        //8_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PriceMarkUp_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_PriceMarkUp, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.PriceMarkUp)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_PriceMarkUp_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //I_H_J
        //8_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_FlightType_OrdersDocId(OrderFlightLeg oOrderFlightLeg)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_OrderFlightLeg.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.WhiteLabelDocId)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_FlightType, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.FlightType)); 
db.AddParameter(TBNames_OrderFlightLeg.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oOrderFlightLeg.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.OrderFlightLeg, "Select_OrderFlightLeg_By_Keys_View_WhiteLabelDocId_FlightType_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
