

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Messages  : ContainerItem<Messages>{

#region CTOR

    #region Constractor
    static Messages()
    {
        ConvertEvent = Messages.OnConvert;
    }  
    //public KeyValuesContainer<Messages> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Messages()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.MessagesKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Messages OnConvert(DataRow dr)
    {
        int LangId = Translator<Messages>.LangId;            
        Messages oMessages = null;
        if (dr != null)
        {
            oMessages = new Messages();
            #region Create Object
            oMessages.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Messages.Field_DateCreated]);
             oMessages.DocId = ConvertTo.ConvertToInt(dr[TBNames_Messages.Field_DocId]);
             oMessages.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Messages.Field_IsDeleted]);
             oMessages.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Messages.Field_IsActive]);
             oMessages.SentDateTime = ConvertTo.ConvertToDateTime(dr[TBNames_Messages.Field_SentDateTime]);
             oMessages.MessageType = ConvertTo.ConvertToDateTime(dr[TBNames_Messages.Field_MessageType]);
             oMessages.LowCostPNR = ConvertTo.ConvertToString(dr[TBNames_Messages.Field_LowCostPNR]);
 
//FK     KeyOrdersDocId
            oMessages.OrdersDocId = ConvertTo.ConvertToInt(dr[TBNames_Messages.Field_OrdersDocId]);
             oMessages.Content = ConvertTo.ConvertToString(dr[TBNames_Messages.Field_Content]);
 
//FK     KeyWhiteLabelDocId
            oMessages.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Messages.Field_WhiteLabelDocId]);
 
            #endregion
            Translator<Messages>.Translate(oMessages.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oMessages;
    }

    
#endregion

//#REP_HERE 
#region Messages Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyMessagesDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyMessagesDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyMessagesIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyMessagesIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_SentDateTime;
private DateTime _sent_date_time;
public String FriendlySentDateTime
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesSentDateTime);   
    }
}
public  DateTime? SentDateTime
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyMessagesSentDateTime));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesSentDateTime, value);
         _sent_date_time = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_SentDateTime = true;
    }
}


public DateTime SentDateTime_Value
{
    get
    {
        //return _sent_date_time; //ConvertToValue.ConvertToDateTime(SentDateTime);
        if(isSetOnce_SentDateTime) {return _sent_date_time;}
        else {return ConvertToValue.ConvertToDateTime(SentDateTime);}
    }
}

public string SentDateTime_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_sent_date_time).ToShortDateString();
               //if(isSetOnce_SentDateTime) {return ConvertToValue.ConvertToDateTime(_sent_date_time).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(SentDateTime).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(SentDateTime).ToShortDateString();
    }
}

private bool isSetOnce_MessageType;
private DateTime _message_type;
public String FriendlyMessageType
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesMessageType);   
    }
}
public  DateTime? MessageType
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyMessagesMessageType));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesMessageType, value);
         _message_type = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_MessageType = true;
    }
}


public DateTime MessageType_Value
{
    get
    {
        //return _message_type; //ConvertToValue.ConvertToDateTime(MessageType);
        if(isSetOnce_MessageType) {return _message_type;}
        else {return ConvertToValue.ConvertToDateTime(MessageType);}
    }
}

public string MessageType_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_message_type).ToShortDateString();
               //if(isSetOnce_MessageType) {return ConvertToValue.ConvertToDateTime(_message_type).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(MessageType).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(MessageType).ToShortDateString();
    }
}

private bool isSetOnce_LowCostPNR;
private string _low_cost_pnr;
public String FriendlyLowCostPNR
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesLowCostPNR);   
    }
}
public  string LowCostPNR
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyMessagesLowCostPNR));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesLowCostPNR, value);
         _low_cost_pnr = ConvertToValue.ConvertToString(value);
        isSetOnce_LowCostPNR = true;
    }
}


public string LowCostPNR_Value
{
    get
    {
        //return _low_cost_pnr; //ConvertToValue.ConvertToString(LowCostPNR);
        if(isSetOnce_LowCostPNR) {return _low_cost_pnr;}
        else {return ConvertToValue.ConvertToString(LowCostPNR);}
    }
}

public string LowCostPNR_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_low_cost_pnr).ToString();
               //if(isSetOnce_LowCostPNR) {return ConvertToValue.ConvertToString(_low_cost_pnr).ToString();}
               //else {return ConvertToValue.ConvertToString(LowCostPNR).ToString();}
            ConvertToValue.ConvertToString(LowCostPNR).ToString();
    }
}

private bool isSetOnce_OrdersDocId;
private int _orders_doc_id;
public String FriendlyOrdersDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDocId);   
    }
}
public  int? OrdersDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDocId, value);
         _orders_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrdersDocId = true;
    }
}


public int OrdersDocId_Value
{
    get
    {
        //return _orders_doc_id; //ConvertToValue.ConvertToInt(OrdersDocId);
        if(isSetOnce_OrdersDocId) {return _orders_doc_id;}
        else {return ConvertToValue.ConvertToInt(OrdersDocId);}
    }
}

public string OrdersDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_orders_doc_id).ToString();
               //if(isSetOnce_OrdersDocId) {return ConvertToValue.ConvertToInt(_orders_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrdersDocId).ToString();}
            ConvertToValue.ConvertToInt(OrdersDocId).ToString();
    }
}

private bool isSetOnce_Content;
private string _content;
public String FriendlyContent
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyMessagesContent);   
    }
}
public  string Content
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyMessagesContent));
    }
    set
    {
        SetKey(KeyValuesType.KeyMessagesContent, value);
         _content = ConvertToValue.ConvertToString(value);
        isSetOnce_Content = true;
    }
}


public string Content_Value
{
    get
    {
        //return _content; //ConvertToValue.ConvertToString(Content);
        if(isSetOnce_Content) {return _content;}
        else {return ConvertToValue.ConvertToString(Content);}
    }
}

public string Content_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_content).ToString();
               //if(isSetOnce_Content) {return ConvertToValue.ConvertToString(_content).ToString();}
               //else {return ConvertToValue.ConvertToString(Content).ToString();}
            ConvertToValue.ConvertToString(Content).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //J_A
        //9_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //J_B
        //9_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C
        //9_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_E
        //9_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //J_F
        //9_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType", ex.Message));

            }
            return paramsSelect;
        }



        //J_G
        //9_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //J_H
        //9_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_I
        //9_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_Content", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_B
        //9_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_C
        //9_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_E
        //9_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SentDateTime(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_SentDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_F
        //9_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MessageType(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_MessageType", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_G
        //9_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_H
        //9_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrdersDocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_A_I
        //9_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oMessages.DateCreated)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DateCreated_Content", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_C
        //9_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_E
        //9_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SentDateTime(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_SentDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_F
        //9_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MessageType(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_MessageType", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_G
        //9_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_H
        //9_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrdersDocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_B_I
        //9_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oMessages.DocId)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_DocId_Content", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_E
        //9_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SentDateTime(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_SentDateTime", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_F
        //9_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MessageType(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_MessageType", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_G
        //9_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_H
        //9_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrdersDocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_C_I
        //9_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oMessages.IsDeleted)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_IsDeleted_Content", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_F
        //9_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_MessageType(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_MessageType", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_G
        //9_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_LowCostPNR(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_H
        //9_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_OrdersDocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_E_I
        //9_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SentDateTime_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_SentDateTime, ConvertTo.ConvertEmptyToDBNull(oMessages.SentDateTime)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_SentDateTime_Content", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_G
        //9_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType_LowCostPNR(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_H
        //9_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType_OrdersDocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_F_I
        //9_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MessageType_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_MessageType, ConvertTo.ConvertEmptyToDBNull(oMessages.MessageType)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_MessageType_Content", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_H
        //9_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrdersDocId(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrdersDocId", ex.Message));

            }
            return paramsSelect;
        }



        //J_G_I
        //9_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oMessages.LowCostPNR)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_LowCostPNR_Content", ex.Message));

            }
            return paramsSelect;
        }



        //J_H_I
        //9_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrdersDocId_Content(Messages oMessages)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Messages.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.WhiteLabelDocId)); 
db.AddParameter(TBNames_Messages.PRM_OrdersDocId, ConvertTo.ConvertEmptyToDBNull(oMessages.OrdersDocId)); 
db.AddParameter(TBNames_Messages.PRM_Content, ConvertTo.ConvertEmptyToDBNull(oMessages.Content));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Messages, "Select_Messages_By_Keys_View_WhiteLabelDocId_OrdersDocId_Content", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
