

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_FlightLeg
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertFlightLeg = PROC_Prefix + "save_flight_leg";
        #endregion
        #region Select
        public static readonly string PROC_Select_FlightLeg_By_DocId = PROC_Prefix + "Select_flight_leg_By_doc_id";        
        public static readonly string PROC_Select_FlightLeg_By_Keys_View = PROC_Prefix + "Select_flight_leg_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteFlightLeg = PROC_Prefix + "delete_flight_leg";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegIsActive);

        public static readonly string PRM_FlightNumber = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegFlightNumber);

        public static readonly string PRM_DepartureDateTime = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDepartureDateTime);

        public static readonly string PRM_ArrivalDateTime = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegArrivalDateTime);

        public static readonly string PRM_Status = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegStatus);

        public static readonly string PRM_Duration = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDuration);

        public static readonly string PRM_Hops = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegHops);

        public static readonly string PRM_AirlineIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegAirlineIataCode);

        public static readonly string PRM_OriginIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegOriginIataCode);

        public static readonly string PRM_DestinationIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDestinationIataCode);

        public static readonly string PRM_OrderFlightLegDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "FlightLeg.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegIsActive);

        public static readonly string Field_FlightNumber = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegFlightNumber);

        public static readonly string Field_DepartureDateTime = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDepartureDateTime);

        public static readonly string Field_ArrivalDateTime = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegArrivalDateTime);

        public static readonly string Field_Status = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegStatus);

        public static readonly string Field_Duration = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDuration);

        public static readonly string Field_Hops = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegHops);

        public static readonly string Field_AirlineIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegAirlineIataCode);

        public static readonly string Field_OriginIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegOriginIataCode);

        public static readonly string Field_DestinationIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDestinationIataCode);

        public static readonly string Field_OrderFlightLegDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrderFlightLegDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//O_A
//14_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated = "select_FlightLeg_by_keys_view_14_0";

//O_B
//14_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId = "select_FlightLeg_by_keys_view_14_1";

//O_C
//14_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_FlightLeg_by_keys_view_14_2";

//O_E
//14_4
//WhiteLabelDocId_FlightNumber
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber = "select_FlightLeg_by_keys_view_14_4";

//O_F
//14_5
//WhiteLabelDocId_DepartureDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime = "select_FlightLeg_by_keys_view_14_5";

//O_G
//14_6
//WhiteLabelDocId_ArrivalDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime = "select_FlightLeg_by_keys_view_14_6";

//O_H
//14_7
//WhiteLabelDocId_Status
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status = "select_FlightLeg_by_keys_view_14_7";

//O_I
//14_8
//WhiteLabelDocId_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration = "select_FlightLeg_by_keys_view_14_8";

//O_J
//14_9
//WhiteLabelDocId_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops = "select_FlightLeg_by_keys_view_14_9";

//O_K
//14_10
//WhiteLabelDocId_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode = "select_FlightLeg_by_keys_view_14_10";

//O_L
//14_11
//WhiteLabelDocId_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode = "select_FlightLeg_by_keys_view_14_11";

//O_M
//14_12
//WhiteLabelDocId_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DestinationIataCode = "select_FlightLeg_by_keys_view_14_12";

//O_N
//14_13
//WhiteLabelDocId_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_13";

//O_A_B
//14_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_FlightLeg_by_keys_view_14_0_1";

//O_A_C
//14_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_FlightLeg_by_keys_view_14_0_2";

//O_A_E
//14_0_4
//WhiteLabelDocId_DateCreated_FlightNumber
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightNumber = "select_FlightLeg_by_keys_view_14_0_4";

//O_A_F
//14_0_5
//WhiteLabelDocId_DateCreated_DepartureDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DepartureDateTime = "select_FlightLeg_by_keys_view_14_0_5";

//O_A_G
//14_0_6
//WhiteLabelDocId_DateCreated_ArrivalDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_ArrivalDateTime = "select_FlightLeg_by_keys_view_14_0_6";

//O_A_H
//14_0_7
//WhiteLabelDocId_DateCreated_Status
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Status = "select_FlightLeg_by_keys_view_14_0_7";

//O_A_I
//14_0_8
//WhiteLabelDocId_DateCreated_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Duration = "select_FlightLeg_by_keys_view_14_0_8";

//O_A_J
//14_0_9
//WhiteLabelDocId_DateCreated_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Hops = "select_FlightLeg_by_keys_view_14_0_9";

//O_A_K
//14_0_10
//WhiteLabelDocId_DateCreated_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineIataCode = "select_FlightLeg_by_keys_view_14_0_10";

//O_A_L
//14_0_11
//WhiteLabelDocId_DateCreated_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OriginIataCode = "select_FlightLeg_by_keys_view_14_0_11";

//O_A_M
//14_0_12
//WhiteLabelDocId_DateCreated_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DestinationIataCode = "select_FlightLeg_by_keys_view_14_0_12";

//O_A_N
//14_0_13
//WhiteLabelDocId_DateCreated_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_0_13";

//O_B_C
//14_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_FlightLeg_by_keys_view_14_1_2";

//O_B_E
//14_1_4
//WhiteLabelDocId_DocId_FlightNumber
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightNumber = "select_FlightLeg_by_keys_view_14_1_4";

//O_B_F
//14_1_5
//WhiteLabelDocId_DocId_DepartureDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_DepartureDateTime = "select_FlightLeg_by_keys_view_14_1_5";

//O_B_G
//14_1_6
//WhiteLabelDocId_DocId_ArrivalDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_ArrivalDateTime = "select_FlightLeg_by_keys_view_14_1_6";

//O_B_H
//14_1_7
//WhiteLabelDocId_DocId_Status
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Status = "select_FlightLeg_by_keys_view_14_1_7";

//O_B_I
//14_1_8
//WhiteLabelDocId_DocId_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Duration = "select_FlightLeg_by_keys_view_14_1_8";

//O_B_J
//14_1_9
//WhiteLabelDocId_DocId_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Hops = "select_FlightLeg_by_keys_view_14_1_9";

//O_B_K
//14_1_10
//WhiteLabelDocId_DocId_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_AirlineIataCode = "select_FlightLeg_by_keys_view_14_1_10";

//O_B_L
//14_1_11
//WhiteLabelDocId_DocId_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OriginIataCode = "select_FlightLeg_by_keys_view_14_1_11";

//O_B_M
//14_1_12
//WhiteLabelDocId_DocId_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_DestinationIataCode = "select_FlightLeg_by_keys_view_14_1_12";

//O_B_N
//14_1_13
//WhiteLabelDocId_DocId_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DocId_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_1_13";

//O_C_E
//14_2_4
//WhiteLabelDocId_IsDeleted_FlightNumber
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightNumber = "select_FlightLeg_by_keys_view_14_2_4";

//O_C_F
//14_2_5
//WhiteLabelDocId_IsDeleted_DepartureDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_DepartureDateTime = "select_FlightLeg_by_keys_view_14_2_5";

//O_C_G
//14_2_6
//WhiteLabelDocId_IsDeleted_ArrivalDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_ArrivalDateTime = "select_FlightLeg_by_keys_view_14_2_6";

//O_C_H
//14_2_7
//WhiteLabelDocId_IsDeleted_Status
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Status = "select_FlightLeg_by_keys_view_14_2_7";

//O_C_I
//14_2_8
//WhiteLabelDocId_IsDeleted_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Duration = "select_FlightLeg_by_keys_view_14_2_8";

//O_C_J
//14_2_9
//WhiteLabelDocId_IsDeleted_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Hops = "select_FlightLeg_by_keys_view_14_2_9";

//O_C_K
//14_2_10
//WhiteLabelDocId_IsDeleted_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineIataCode = "select_FlightLeg_by_keys_view_14_2_10";

//O_C_L
//14_2_11
//WhiteLabelDocId_IsDeleted_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OriginIataCode = "select_FlightLeg_by_keys_view_14_2_11";

//O_C_M
//14_2_12
//WhiteLabelDocId_IsDeleted_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_DestinationIataCode = "select_FlightLeg_by_keys_view_14_2_12";

//O_C_N
//14_2_13
//WhiteLabelDocId_IsDeleted_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_2_13";

//O_E_F
//14_4_5
//WhiteLabelDocId_FlightNumber_DepartureDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_DepartureDateTime = "select_FlightLeg_by_keys_view_14_4_5";

//O_E_G
//14_4_6
//WhiteLabelDocId_FlightNumber_ArrivalDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_ArrivalDateTime = "select_FlightLeg_by_keys_view_14_4_6";

//O_E_H
//14_4_7
//WhiteLabelDocId_FlightNumber_Status
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Status = "select_FlightLeg_by_keys_view_14_4_7";

//O_E_I
//14_4_8
//WhiteLabelDocId_FlightNumber_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Duration = "select_FlightLeg_by_keys_view_14_4_8";

//O_E_J
//14_4_9
//WhiteLabelDocId_FlightNumber_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_Hops = "select_FlightLeg_by_keys_view_14_4_9";

//O_E_K
//14_4_10
//WhiteLabelDocId_FlightNumber_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_AirlineIataCode = "select_FlightLeg_by_keys_view_14_4_10";

//O_E_L
//14_4_11
//WhiteLabelDocId_FlightNumber_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_OriginIataCode = "select_FlightLeg_by_keys_view_14_4_11";

//O_E_M
//14_4_12
//WhiteLabelDocId_FlightNumber_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_DestinationIataCode = "select_FlightLeg_by_keys_view_14_4_12";

//O_E_N
//14_4_13
//WhiteLabelDocId_FlightNumber_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_4_13";

//O_F_G
//14_5_6
//WhiteLabelDocId_DepartureDateTime_ArrivalDateTime
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_ArrivalDateTime = "select_FlightLeg_by_keys_view_14_5_6";

//O_F_H
//14_5_7
//WhiteLabelDocId_DepartureDateTime_Status
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Status = "select_FlightLeg_by_keys_view_14_5_7";

//O_F_I
//14_5_8
//WhiteLabelDocId_DepartureDateTime_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Duration = "select_FlightLeg_by_keys_view_14_5_8";

//O_F_J
//14_5_9
//WhiteLabelDocId_DepartureDateTime_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_Hops = "select_FlightLeg_by_keys_view_14_5_9";

//O_F_K
//14_5_10
//WhiteLabelDocId_DepartureDateTime_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_AirlineIataCode = "select_FlightLeg_by_keys_view_14_5_10";

//O_F_L
//14_5_11
//WhiteLabelDocId_DepartureDateTime_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_OriginIataCode = "select_FlightLeg_by_keys_view_14_5_11";

//O_F_M
//14_5_12
//WhiteLabelDocId_DepartureDateTime_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_DestinationIataCode = "select_FlightLeg_by_keys_view_14_5_12";

//O_F_N
//14_5_13
//WhiteLabelDocId_DepartureDateTime_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DepartureDateTime_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_5_13";

//O_G_H
//14_6_7
//WhiteLabelDocId_ArrivalDateTime_Status
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Status = "select_FlightLeg_by_keys_view_14_6_7";

//O_G_I
//14_6_8
//WhiteLabelDocId_ArrivalDateTime_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Duration = "select_FlightLeg_by_keys_view_14_6_8";

//O_G_J
//14_6_9
//WhiteLabelDocId_ArrivalDateTime_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_Hops = "select_FlightLeg_by_keys_view_14_6_9";

//O_G_K
//14_6_10
//WhiteLabelDocId_ArrivalDateTime_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_AirlineIataCode = "select_FlightLeg_by_keys_view_14_6_10";

//O_G_L
//14_6_11
//WhiteLabelDocId_ArrivalDateTime_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_OriginIataCode = "select_FlightLeg_by_keys_view_14_6_11";

//O_G_M
//14_6_12
//WhiteLabelDocId_ArrivalDateTime_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_DestinationIataCode = "select_FlightLeg_by_keys_view_14_6_12";

//O_G_N
//14_6_13
//WhiteLabelDocId_ArrivalDateTime_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_ArrivalDateTime_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_6_13";

//O_H_I
//14_7_8
//WhiteLabelDocId_Status_Duration
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_Duration = "select_FlightLeg_by_keys_view_14_7_8";

//O_H_J
//14_7_9
//WhiteLabelDocId_Status_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_Hops = "select_FlightLeg_by_keys_view_14_7_9";

//O_H_K
//14_7_10
//WhiteLabelDocId_Status_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_AirlineIataCode = "select_FlightLeg_by_keys_view_14_7_10";

//O_H_L
//14_7_11
//WhiteLabelDocId_Status_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_OriginIataCode = "select_FlightLeg_by_keys_view_14_7_11";

//O_H_M
//14_7_12
//WhiteLabelDocId_Status_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_DestinationIataCode = "select_FlightLeg_by_keys_view_14_7_12";

//O_H_N
//14_7_13
//WhiteLabelDocId_Status_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Status_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_7_13";

//O_I_J
//14_8_9
//WhiteLabelDocId_Duration_Hops
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_Hops = "select_FlightLeg_by_keys_view_14_8_9";

//O_I_K
//14_8_10
//WhiteLabelDocId_Duration_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_AirlineIataCode = "select_FlightLeg_by_keys_view_14_8_10";

//O_I_L
//14_8_11
//WhiteLabelDocId_Duration_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_OriginIataCode = "select_FlightLeg_by_keys_view_14_8_11";

//O_I_M
//14_8_12
//WhiteLabelDocId_Duration_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_DestinationIataCode = "select_FlightLeg_by_keys_view_14_8_12";

//O_I_N
//14_8_13
//WhiteLabelDocId_Duration_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Duration_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_8_13";

//O_J_K
//14_9_10
//WhiteLabelDocId_Hops_AirlineIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_AirlineIataCode = "select_FlightLeg_by_keys_view_14_9_10";

//O_J_L
//14_9_11
//WhiteLabelDocId_Hops_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_OriginIataCode = "select_FlightLeg_by_keys_view_14_9_11";

//O_J_M
//14_9_12
//WhiteLabelDocId_Hops_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_DestinationIataCode = "select_FlightLeg_by_keys_view_14_9_12";

//O_J_N
//14_9_13
//WhiteLabelDocId_Hops_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_Hops_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_9_13";

//O_K_L
//14_10_11
//WhiteLabelDocId_AirlineIataCode_OriginIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OriginIataCode = "select_FlightLeg_by_keys_view_14_10_11";

//O_K_M
//14_10_12
//WhiteLabelDocId_AirlineIataCode_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_DestinationIataCode = "select_FlightLeg_by_keys_view_14_10_12";

//O_K_N
//14_10_13
//WhiteLabelDocId_AirlineIataCode_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_10_13";

//O_L_M
//14_11_12
//WhiteLabelDocId_OriginIataCode_DestinationIataCode
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode_DestinationIataCode = "select_FlightLeg_by_keys_view_14_11_12";

//O_L_N
//14_11_13
//WhiteLabelDocId_OriginIataCode_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_OriginIataCode_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_11_13";

//O_M_N
//14_12_13
//WhiteLabelDocId_DestinationIataCode_OrderFlightLegDocId
public static readonly string  PROC_Select_FlightLeg_By_Keys_View_WhiteLabelDocId_DestinationIataCode_OrderFlightLegDocId = "select_FlightLeg_by_keys_view_14_12_13";
         #endregion

    }

}
