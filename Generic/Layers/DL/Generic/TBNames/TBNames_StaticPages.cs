

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_StaticPages
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertStaticPages = PROC_Prefix + "save_static_pages";
        #endregion
        #region Select
        public static readonly string PROC_Select_StaticPages_By_DocId = PROC_Prefix + "Select_static_pages_By_doc_id";        
        public static readonly string PROC_Select_StaticPages_By_Keys_View = PROC_Prefix + "Select_static_pages_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteStaticPages = PROC_Prefix + "delete_static_pages";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesIsActive);

        public static readonly string PRM_FriendlyUrl = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesFriendlyUrl);

        public static readonly string PRM_Text = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesText);

        public static readonly string PRM_Name = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesName);

        public static readonly string PRM_Type = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesType);

        public static readonly string PRM_SeoDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "StaticPages.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesIsActive);

        public static readonly string Field_FriendlyUrl = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesFriendlyUrl);

        public static readonly string Field_Text = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesText);

        public static readonly string Field_Name = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesName);

        public static readonly string Field_Type = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStaticPagesType);

        public static readonly string Field_SeoDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//J_A
//9_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated = "select_StaticPages_by_keys_view_9_0";

//J_B
//9_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId = "select_StaticPages_by_keys_view_9_1";

//J_C
//9_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_StaticPages_by_keys_view_9_2";

//J_E
//9_4
//WhiteLabelDocId_FriendlyUrl
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl = "select_StaticPages_by_keys_view_9_4";

//J_F
//9_5
//WhiteLabelDocId_Text
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text = "select_StaticPages_by_keys_view_9_5";

//J_G
//9_6
//WhiteLabelDocId_Name
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name = "select_StaticPages_by_keys_view_9_6";

//J_H
//9_7
//WhiteLabelDocId_Type
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Type = "select_StaticPages_by_keys_view_9_7";

//J_I
//9_8
//WhiteLabelDocId_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_SeoDocId = "select_StaticPages_by_keys_view_9_8";

//J_A_B
//9_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_StaticPages_by_keys_view_9_0_1";

//J_A_C
//9_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_StaticPages_by_keys_view_9_0_2";

//J_A_E
//9_0_4
//WhiteLabelDocId_DateCreated_FriendlyUrl
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_FriendlyUrl = "select_StaticPages_by_keys_view_9_0_4";

//J_A_F
//9_0_5
//WhiteLabelDocId_DateCreated_Text
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Text = "select_StaticPages_by_keys_view_9_0_5";

//J_A_G
//9_0_6
//WhiteLabelDocId_DateCreated_Name
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Name = "select_StaticPages_by_keys_view_9_0_6";

//J_A_H
//9_0_7
//WhiteLabelDocId_DateCreated_Type
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_Type = "select_StaticPages_by_keys_view_9_0_7";

//J_A_I
//9_0_8
//WhiteLabelDocId_DateCreated_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId = "select_StaticPages_by_keys_view_9_0_8";

//J_B_C
//9_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_StaticPages_by_keys_view_9_1_2";

//J_B_E
//9_1_4
//WhiteLabelDocId_DocId_FriendlyUrl
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_FriendlyUrl = "select_StaticPages_by_keys_view_9_1_4";

//J_B_F
//9_1_5
//WhiteLabelDocId_DocId_Text
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Text = "select_StaticPages_by_keys_view_9_1_5";

//J_B_G
//9_1_6
//WhiteLabelDocId_DocId_Name
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Name = "select_StaticPages_by_keys_view_9_1_6";

//J_B_H
//9_1_7
//WhiteLabelDocId_DocId_Type
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_Type = "select_StaticPages_by_keys_view_9_1_7";

//J_B_I
//9_1_8
//WhiteLabelDocId_DocId_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId = "select_StaticPages_by_keys_view_9_1_8";

//J_C_E
//9_2_4
//WhiteLabelDocId_IsDeleted_FriendlyUrl
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_FriendlyUrl = "select_StaticPages_by_keys_view_9_2_4";

//J_C_F
//9_2_5
//WhiteLabelDocId_IsDeleted_Text
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Text = "select_StaticPages_by_keys_view_9_2_5";

//J_C_G
//9_2_6
//WhiteLabelDocId_IsDeleted_Name
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Name = "select_StaticPages_by_keys_view_9_2_6";

//J_C_H
//9_2_7
//WhiteLabelDocId_IsDeleted_Type
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_Type = "select_StaticPages_by_keys_view_9_2_7";

//J_C_I
//9_2_8
//WhiteLabelDocId_IsDeleted_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId = "select_StaticPages_by_keys_view_9_2_8";

//J_E_F
//9_4_5
//WhiteLabelDocId_FriendlyUrl_Text
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Text = "select_StaticPages_by_keys_view_9_4_5";

//J_E_G
//9_4_6
//WhiteLabelDocId_FriendlyUrl_Name
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Name = "select_StaticPages_by_keys_view_9_4_6";

//J_E_H
//9_4_7
//WhiteLabelDocId_FriendlyUrl_Type
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_Type = "select_StaticPages_by_keys_view_9_4_7";

//J_E_I
//9_4_8
//WhiteLabelDocId_FriendlyUrl_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_FriendlyUrl_SeoDocId = "select_StaticPages_by_keys_view_9_4_8";

//J_F_G
//9_5_6
//WhiteLabelDocId_Text_Name
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_Name = "select_StaticPages_by_keys_view_9_5_6";

//J_F_H
//9_5_7
//WhiteLabelDocId_Text_Type
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_Type = "select_StaticPages_by_keys_view_9_5_7";

//J_F_I
//9_5_8
//WhiteLabelDocId_Text_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Text_SeoDocId = "select_StaticPages_by_keys_view_9_5_8";

//J_G_H
//9_6_7
//WhiteLabelDocId_Name_Type
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name_Type = "select_StaticPages_by_keys_view_9_6_7";

//J_G_I
//9_6_8
//WhiteLabelDocId_Name_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Name_SeoDocId = "select_StaticPages_by_keys_view_9_6_8";

//J_H_I
//9_7_8
//WhiteLabelDocId_Type_SeoDocId
public static readonly string  PROC_Select_StaticPages_By_Keys_View_WhiteLabelDocId_Type_SeoDocId = "select_StaticPages_by_keys_view_9_7_8";
         #endregion

    }

}
