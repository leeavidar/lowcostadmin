

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_IndexPagePromo
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertIndexPagePromo = PROC_Prefix + "save_index_page_promo";
        #endregion
        #region Select
        public static readonly string PROC_Select_IndexPagePromo_By_DocId = PROC_Prefix + "Select_index_page_promo_By_doc_id";        
        public static readonly string PROC_Select_IndexPagePromo_By_Keys_View = PROC_Prefix + "Select_index_page_promo_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteIndexPagePromo = PROC_Prefix + "delete_index_page_promo";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoIsActive);

        public static readonly string PRM_ImagesDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoImagesDocId);

        public static readonly string PRM_Title = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoTitle);

        public static readonly string PRM_Text = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoText);

        public static readonly string PRM_Link = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoLink);

        public static readonly string PRM_PromoType = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoPromoType);

        public static readonly string PRM_Order = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoOrder);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_PriceAfter = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoPriceAfter);

        public static readonly string PRM_PriceBefore = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoPriceBefore);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "IndexPagePromo.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoIsActive);

        public static readonly string Field_ImagesDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoImagesDocId);

        public static readonly string Field_Title = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoTitle);

        public static readonly string Field_Text = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoText);

        public static readonly string Field_Link = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoLink);

        public static readonly string Field_PromoType = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoPromoType);

        public static readonly string Field_Order = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoOrder);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_PriceAfter = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoPriceAfter);

        public static readonly string Field_PriceBefore = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyIndexPagePromoPriceBefore);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//K_A
//10_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated = "select_IndexPagePromo_by_keys_view_10_0";

//K_B
//10_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId = "select_IndexPagePromo_by_keys_view_10_1";

//K_C
//10_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_IndexPagePromo_by_keys_view_10_2";

//K_E
//10_4
//WhiteLabelDocId_ImagesDocId
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId = "select_IndexPagePromo_by_keys_view_10_4";

//K_F
//10_5
//WhiteLabelDocId_Title
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title = "select_IndexPagePromo_by_keys_view_10_5";

//K_G
//10_6
//WhiteLabelDocId_Text
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text = "select_IndexPagePromo_by_keys_view_10_6";

//K_H
//10_7
//WhiteLabelDocId_Link
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link = "select_IndexPagePromo_by_keys_view_10_7";

//K_I
//10_8
//WhiteLabelDocId_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType = "select_IndexPagePromo_by_keys_view_10_8";

//K_J
//10_9
//WhiteLabelDocId_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order = "select_IndexPagePromo_by_keys_view_10_9";

//K_L
//10_11
//WhiteLabelDocId_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceAfter = "select_IndexPagePromo_by_keys_view_10_11";

//K_M
//10_12
//WhiteLabelDocId_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceBefore = "select_IndexPagePromo_by_keys_view_10_12";

//K_A_B
//10_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_IndexPagePromo_by_keys_view_10_0_1";

//K_A_C
//10_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_IndexPagePromo_by_keys_view_10_0_2";

//K_A_E
//10_0_4
//WhiteLabelDocId_DateCreated_ImagesDocId
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_ImagesDocId = "select_IndexPagePromo_by_keys_view_10_0_4";

//K_A_F
//10_0_5
//WhiteLabelDocId_DateCreated_Title
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Title = "select_IndexPagePromo_by_keys_view_10_0_5";

//K_A_G
//10_0_6
//WhiteLabelDocId_DateCreated_Text
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Text = "select_IndexPagePromo_by_keys_view_10_0_6";

//K_A_H
//10_0_7
//WhiteLabelDocId_DateCreated_Link
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Link = "select_IndexPagePromo_by_keys_view_10_0_7";

//K_A_I
//10_0_8
//WhiteLabelDocId_DateCreated_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PromoType = "select_IndexPagePromo_by_keys_view_10_0_8";

//K_A_J
//10_0_9
//WhiteLabelDocId_DateCreated_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_Order = "select_IndexPagePromo_by_keys_view_10_0_9";

//K_A_L
//10_0_11
//WhiteLabelDocId_DateCreated_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PriceAfter = "select_IndexPagePromo_by_keys_view_10_0_11";

//K_A_M
//10_0_12
//WhiteLabelDocId_DateCreated_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DateCreated_PriceBefore = "select_IndexPagePromo_by_keys_view_10_0_12";

//K_B_C
//10_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_IndexPagePromo_by_keys_view_10_1_2";

//K_B_E
//10_1_4
//WhiteLabelDocId_DocId_ImagesDocId
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_ImagesDocId = "select_IndexPagePromo_by_keys_view_10_1_4";

//K_B_F
//10_1_5
//WhiteLabelDocId_DocId_Title
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Title = "select_IndexPagePromo_by_keys_view_10_1_5";

//K_B_G
//10_1_6
//WhiteLabelDocId_DocId_Text
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Text = "select_IndexPagePromo_by_keys_view_10_1_6";

//K_B_H
//10_1_7
//WhiteLabelDocId_DocId_Link
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Link = "select_IndexPagePromo_by_keys_view_10_1_7";

//K_B_I
//10_1_8
//WhiteLabelDocId_DocId_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PromoType = "select_IndexPagePromo_by_keys_view_10_1_8";

//K_B_J
//10_1_9
//WhiteLabelDocId_DocId_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_Order = "select_IndexPagePromo_by_keys_view_10_1_9";

//K_B_L
//10_1_11
//WhiteLabelDocId_DocId_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PriceAfter = "select_IndexPagePromo_by_keys_view_10_1_11";

//K_B_M
//10_1_12
//WhiteLabelDocId_DocId_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_DocId_PriceBefore = "select_IndexPagePromo_by_keys_view_10_1_12";

//K_C_E
//10_2_4
//WhiteLabelDocId_IsDeleted_ImagesDocId
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_ImagesDocId = "select_IndexPagePromo_by_keys_view_10_2_4";

//K_C_F
//10_2_5
//WhiteLabelDocId_IsDeleted_Title
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Title = "select_IndexPagePromo_by_keys_view_10_2_5";

//K_C_G
//10_2_6
//WhiteLabelDocId_IsDeleted_Text
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Text = "select_IndexPagePromo_by_keys_view_10_2_6";

//K_C_H
//10_2_7
//WhiteLabelDocId_IsDeleted_Link
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Link = "select_IndexPagePromo_by_keys_view_10_2_7";

//K_C_I
//10_2_8
//WhiteLabelDocId_IsDeleted_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PromoType = "select_IndexPagePromo_by_keys_view_10_2_8";

//K_C_J
//10_2_9
//WhiteLabelDocId_IsDeleted_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_Order = "select_IndexPagePromo_by_keys_view_10_2_9";

//K_C_L
//10_2_11
//WhiteLabelDocId_IsDeleted_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceAfter = "select_IndexPagePromo_by_keys_view_10_2_11";

//K_C_M
//10_2_12
//WhiteLabelDocId_IsDeleted_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_IsDeleted_PriceBefore = "select_IndexPagePromo_by_keys_view_10_2_12";

//K_E_F
//10_4_5
//WhiteLabelDocId_ImagesDocId_Title
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Title = "select_IndexPagePromo_by_keys_view_10_4_5";

//K_E_G
//10_4_6
//WhiteLabelDocId_ImagesDocId_Text
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Text = "select_IndexPagePromo_by_keys_view_10_4_6";

//K_E_H
//10_4_7
//WhiteLabelDocId_ImagesDocId_Link
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Link = "select_IndexPagePromo_by_keys_view_10_4_7";

//K_E_I
//10_4_8
//WhiteLabelDocId_ImagesDocId_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PromoType = "select_IndexPagePromo_by_keys_view_10_4_8";

//K_E_J
//10_4_9
//WhiteLabelDocId_ImagesDocId_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_Order = "select_IndexPagePromo_by_keys_view_10_4_9";

//K_E_L
//10_4_11
//WhiteLabelDocId_ImagesDocId_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PriceAfter = "select_IndexPagePromo_by_keys_view_10_4_11";

//K_E_M
//10_4_12
//WhiteLabelDocId_ImagesDocId_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_ImagesDocId_PriceBefore = "select_IndexPagePromo_by_keys_view_10_4_12";

//K_F_G
//10_5_6
//WhiteLabelDocId_Title_Text
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Text = "select_IndexPagePromo_by_keys_view_10_5_6";

//K_F_H
//10_5_7
//WhiteLabelDocId_Title_Link
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Link = "select_IndexPagePromo_by_keys_view_10_5_7";

//K_F_I
//10_5_8
//WhiteLabelDocId_Title_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PromoType = "select_IndexPagePromo_by_keys_view_10_5_8";

//K_F_J
//10_5_9
//WhiteLabelDocId_Title_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_Order = "select_IndexPagePromo_by_keys_view_10_5_9";

//K_F_L
//10_5_11
//WhiteLabelDocId_Title_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PriceAfter = "select_IndexPagePromo_by_keys_view_10_5_11";

//K_F_M
//10_5_12
//WhiteLabelDocId_Title_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Title_PriceBefore = "select_IndexPagePromo_by_keys_view_10_5_12";

//K_G_H
//10_6_7
//WhiteLabelDocId_Text_Link
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_Link = "select_IndexPagePromo_by_keys_view_10_6_7";

//K_G_I
//10_6_8
//WhiteLabelDocId_Text_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PromoType = "select_IndexPagePromo_by_keys_view_10_6_8";

//K_G_J
//10_6_9
//WhiteLabelDocId_Text_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_Order = "select_IndexPagePromo_by_keys_view_10_6_9";

//K_G_L
//10_6_11
//WhiteLabelDocId_Text_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PriceAfter = "select_IndexPagePromo_by_keys_view_10_6_11";

//K_G_M
//10_6_12
//WhiteLabelDocId_Text_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Text_PriceBefore = "select_IndexPagePromo_by_keys_view_10_6_12";

//K_H_I
//10_7_8
//WhiteLabelDocId_Link_PromoType
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PromoType = "select_IndexPagePromo_by_keys_view_10_7_8";

//K_H_J
//10_7_9
//WhiteLabelDocId_Link_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_Order = "select_IndexPagePromo_by_keys_view_10_7_9";

//K_H_L
//10_7_11
//WhiteLabelDocId_Link_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PriceAfter = "select_IndexPagePromo_by_keys_view_10_7_11";

//K_H_M
//10_7_12
//WhiteLabelDocId_Link_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Link_PriceBefore = "select_IndexPagePromo_by_keys_view_10_7_12";

//K_I_J
//10_8_9
//WhiteLabelDocId_PromoType_Order
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_Order = "select_IndexPagePromo_by_keys_view_10_8_9";

//K_I_L
//10_8_11
//WhiteLabelDocId_PromoType_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_PriceAfter = "select_IndexPagePromo_by_keys_view_10_8_11";

//K_I_M
//10_8_12
//WhiteLabelDocId_PromoType_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PromoType_PriceBefore = "select_IndexPagePromo_by_keys_view_10_8_12";

//K_J_L
//10_9_11
//WhiteLabelDocId_Order_PriceAfter
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order_PriceAfter = "select_IndexPagePromo_by_keys_view_10_9_11";

//K_J_M
//10_9_12
//WhiteLabelDocId_Order_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_Order_PriceBefore = "select_IndexPagePromo_by_keys_view_10_9_12";

//K_L_M
//10_11_12
//WhiteLabelDocId_PriceAfter_PriceBefore
public static readonly string  PROC_Select_IndexPagePromo_By_Keys_View_WhiteLabelDocId_PriceAfter_PriceBefore = "select_IndexPagePromo_by_keys_view_10_11_12";
         #endregion

    }

}
