

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Orders
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertOrders = PROC_Prefix + "save_orders";
        #endregion
        #region Select
        public static readonly string PROC_Select_Orders_By_DocId = PROC_Prefix + "Select_orders_By_doc_id";        
        public static readonly string PROC_Select_Orders_By_Keys_View = PROC_Prefix + "Select_orders_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteOrders = PROC_Prefix + "delete_orders";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersIsActive);

        public static readonly string PRM_LowCostPNR = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersLowCostPNR);

        public static readonly string PRM_SupplierPNR = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersSupplierPNR);

        public static readonly string PRM_TotalPrice = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersTotalPrice);

        public static readonly string PRM_DateOfBook = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDateOfBook);

        public static readonly string PRM_ReceiveEmail = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersReceiveEmail);

        public static readonly string PRM_ReceiveSms = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersReceiveSms);

        public static readonly string PRM_MarketingCabin = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersMarketingCabin);

        public static readonly string PRM_ContactDetailsDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_AirlineIataCode = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersAirlineIataCode);

        public static readonly string PRM_OrderStatus = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderStatus);

        public static readonly string PRM_AdminRemarks = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersAdminRemarks);

        public static readonly string PRM_StatusPayment = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersStatusPayment);

        public static readonly string PRM_OrderToken = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderToken);

        public static readonly string PRM_PaymentTransaction = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersPaymentTransaction);

        public static readonly string PRM_XmlBookRequest = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersXmlBookRequest);

        public static readonly string PRM_XmlBookResponse = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersXmlBookResponse);

        public static readonly string PRM_OrderRemarks = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderRemarks);

        public static readonly string PRM_OrderPayment = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderPayment);

        public static readonly string PRM_PaymentToken = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersPaymentToken);

        public static readonly string PRM_PaymentRemarks = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersPaymentRemarks);

        public static readonly string PRM_Currency = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersCurrency);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Orders.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersIsActive);

        public static readonly string Field_LowCostPNR = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersLowCostPNR);

        public static readonly string Field_SupplierPNR = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersSupplierPNR);

        public static readonly string Field_TotalPrice = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersTotalPrice);

        public static readonly string Field_DateOfBook = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersDateOfBook);

        public static readonly string Field_ReceiveEmail = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersReceiveEmail);

        public static readonly string Field_ReceiveSms = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersReceiveSms);

        public static readonly string Field_MarketingCabin = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersMarketingCabin);

        public static readonly string Field_ContactDetailsDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyContactDetailsDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_AirlineIataCode = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersAirlineIataCode);

        public static readonly string Field_OrderStatus = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderStatus);

        public static readonly string Field_AdminRemarks = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersAdminRemarks);

        public static readonly string Field_StatusPayment = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersStatusPayment);

        public static readonly string Field_OrderToken = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderToken);

        public static readonly string Field_PaymentTransaction = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersPaymentTransaction);

        public static readonly string Field_XmlBookRequest = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersXmlBookRequest);

        public static readonly string Field_XmlBookResponse = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersXmlBookResponse);

        public static readonly string Field_OrderRemarks = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderRemarks);

        public static readonly string Field_OrderPayment = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersOrderPayment);

        public static readonly string Field_PaymentToken = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersPaymentToken);

        public static readonly string Field_PaymentRemarks = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersPaymentRemarks);

        public static readonly string Field_Currency = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyOrdersCurrency);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//M_A
//12_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Orders_by_keys_view_12_0";

//M_B
//12_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId = "select_Orders_by_keys_view_12_1";

//M_C
//12_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Orders_by_keys_view_12_2";

//M_E
//12_4
//WhiteLabelDocId_LowCostPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR = "select_Orders_by_keys_view_12_4";

//M_F
//12_5
//WhiteLabelDocId_SupplierPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR = "select_Orders_by_keys_view_12_5";

//M_G
//12_6
//WhiteLabelDocId_TotalPrice
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice = "select_Orders_by_keys_view_12_6";

//M_H
//12_7
//WhiteLabelDocId_DateOfBook
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook = "select_Orders_by_keys_view_12_7";

//M_I
//12_8
//WhiteLabelDocId_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail = "select_Orders_by_keys_view_12_8";

//M_J
//12_9
//WhiteLabelDocId_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms = "select_Orders_by_keys_view_12_9";

//M_K
//12_10
//WhiteLabelDocId_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin = "select_Orders_by_keys_view_12_10";

//M_L
//12_11
//WhiteLabelDocId_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId = "select_Orders_by_keys_view_12_11";

//M_N
//12_13
//WhiteLabelDocId_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode = "select_Orders_by_keys_view_12_13";

//M_O
//12_14
//WhiteLabelDocId_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus = "select_Orders_by_keys_view_12_14";

//M_P
//12_15
//WhiteLabelDocId_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks = "select_Orders_by_keys_view_12_15";

//M_Q
//12_16
//WhiteLabelDocId_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment = "select_Orders_by_keys_view_12_16";

//M_R
//12_17
//WhiteLabelDocId_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken = "select_Orders_by_keys_view_12_17";

//M_S
//12_18
//WhiteLabelDocId_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction = "select_Orders_by_keys_view_12_18";

//M_T
//12_19
//WhiteLabelDocId_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest = "select_Orders_by_keys_view_12_19";

//M_U
//12_20
//WhiteLabelDocId_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse = "select_Orders_by_keys_view_12_20";

//M_V
//12_21
//WhiteLabelDocId_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks = "select_Orders_by_keys_view_12_21";

//M_W
//12_22
//WhiteLabelDocId_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment = "select_Orders_by_keys_view_12_22";

//M_X
//12_23
//WhiteLabelDocId_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken = "select_Orders_by_keys_view_12_23";

//M_Y
//12_24
//WhiteLabelDocId_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentRemarks = "select_Orders_by_keys_view_12_24";

//M_Z
//12_25
//WhiteLabelDocId_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_Currency = "select_Orders_by_keys_view_12_25";

//M_A_B
//12_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Orders_by_keys_view_12_0_1";

//M_A_C
//12_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Orders_by_keys_view_12_0_2";

//M_A_E
//12_0_4
//WhiteLabelDocId_DateCreated_LowCostPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR = "select_Orders_by_keys_view_12_0_4";

//M_A_F
//12_0_5
//WhiteLabelDocId_DateCreated_SupplierPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_SupplierPNR = "select_Orders_by_keys_view_12_0_5";

//M_A_G
//12_0_6
//WhiteLabelDocId_DateCreated_TotalPrice
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_TotalPrice = "select_Orders_by_keys_view_12_0_6";

//M_A_H
//12_0_7
//WhiteLabelDocId_DateCreated_DateOfBook
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_DateOfBook = "select_Orders_by_keys_view_12_0_7";

//M_A_I
//12_0_8
//WhiteLabelDocId_DateCreated_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ReceiveEmail = "select_Orders_by_keys_view_12_0_8";

//M_A_J
//12_0_9
//WhiteLabelDocId_DateCreated_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ReceiveSms = "select_Orders_by_keys_view_12_0_9";

//M_A_K
//12_0_10
//WhiteLabelDocId_DateCreated_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_MarketingCabin = "select_Orders_by_keys_view_12_0_10";

//M_A_L
//12_0_11
//WhiteLabelDocId_DateCreated_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ContactDetailsDocId = "select_Orders_by_keys_view_12_0_11";

//M_A_N
//12_0_13
//WhiteLabelDocId_DateCreated_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineIataCode = "select_Orders_by_keys_view_12_0_13";

//M_A_O
//12_0_14
//WhiteLabelDocId_DateCreated_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderStatus = "select_Orders_by_keys_view_12_0_14";

//M_A_P
//12_0_15
//WhiteLabelDocId_DateCreated_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_AdminRemarks = "select_Orders_by_keys_view_12_0_15";

//M_A_Q
//12_0_16
//WhiteLabelDocId_DateCreated_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_StatusPayment = "select_Orders_by_keys_view_12_0_16";

//M_A_R
//12_0_17
//WhiteLabelDocId_DateCreated_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderToken = "select_Orders_by_keys_view_12_0_17";

//M_A_S
//12_0_18
//WhiteLabelDocId_DateCreated_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentTransaction = "select_Orders_by_keys_view_12_0_18";

//M_A_T
//12_0_19
//WhiteLabelDocId_DateCreated_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_XmlBookRequest = "select_Orders_by_keys_view_12_0_19";

//M_A_U
//12_0_20
//WhiteLabelDocId_DateCreated_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_XmlBookResponse = "select_Orders_by_keys_view_12_0_20";

//M_A_V
//12_0_21
//WhiteLabelDocId_DateCreated_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderRemarks = "select_Orders_by_keys_view_12_0_21";

//M_A_W
//12_0_22
//WhiteLabelDocId_DateCreated_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderPayment = "select_Orders_by_keys_view_12_0_22";

//M_A_X
//12_0_23
//WhiteLabelDocId_DateCreated_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentToken = "select_Orders_by_keys_view_12_0_23";

//M_A_Y
//12_0_24
//WhiteLabelDocId_DateCreated_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentRemarks = "select_Orders_by_keys_view_12_0_24";

//M_A_Z
//12_0_25
//WhiteLabelDocId_DateCreated_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_Currency = "select_Orders_by_keys_view_12_0_25";

//M_B_C
//12_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Orders_by_keys_view_12_1_2";

//M_B_E
//12_1_4
//WhiteLabelDocId_DocId_LowCostPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR = "select_Orders_by_keys_view_12_1_4";

//M_B_F
//12_1_5
//WhiteLabelDocId_DocId_SupplierPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_SupplierPNR = "select_Orders_by_keys_view_12_1_5";

//M_B_G
//12_1_6
//WhiteLabelDocId_DocId_TotalPrice
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_TotalPrice = "select_Orders_by_keys_view_12_1_6";

//M_B_H
//12_1_7
//WhiteLabelDocId_DocId_DateOfBook
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_DateOfBook = "select_Orders_by_keys_view_12_1_7";

//M_B_I
//12_1_8
//WhiteLabelDocId_DocId_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ReceiveEmail = "select_Orders_by_keys_view_12_1_8";

//M_B_J
//12_1_9
//WhiteLabelDocId_DocId_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ReceiveSms = "select_Orders_by_keys_view_12_1_9";

//M_B_K
//12_1_10
//WhiteLabelDocId_DocId_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_MarketingCabin = "select_Orders_by_keys_view_12_1_10";

//M_B_L
//12_1_11
//WhiteLabelDocId_DocId_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ContactDetailsDocId = "select_Orders_by_keys_view_12_1_11";

//M_B_N
//12_1_13
//WhiteLabelDocId_DocId_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_AirlineIataCode = "select_Orders_by_keys_view_12_1_13";

//M_B_O
//12_1_14
//WhiteLabelDocId_DocId_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderStatus = "select_Orders_by_keys_view_12_1_14";

//M_B_P
//12_1_15
//WhiteLabelDocId_DocId_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_AdminRemarks = "select_Orders_by_keys_view_12_1_15";

//M_B_Q
//12_1_16
//WhiteLabelDocId_DocId_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_StatusPayment = "select_Orders_by_keys_view_12_1_16";

//M_B_R
//12_1_17
//WhiteLabelDocId_DocId_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderToken = "select_Orders_by_keys_view_12_1_17";

//M_B_S
//12_1_18
//WhiteLabelDocId_DocId_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentTransaction = "select_Orders_by_keys_view_12_1_18";

//M_B_T
//12_1_19
//WhiteLabelDocId_DocId_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_XmlBookRequest = "select_Orders_by_keys_view_12_1_19";

//M_B_U
//12_1_20
//WhiteLabelDocId_DocId_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_XmlBookResponse = "select_Orders_by_keys_view_12_1_20";

//M_B_V
//12_1_21
//WhiteLabelDocId_DocId_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderRemarks = "select_Orders_by_keys_view_12_1_21";

//M_B_W
//12_1_22
//WhiteLabelDocId_DocId_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderPayment = "select_Orders_by_keys_view_12_1_22";

//M_B_X
//12_1_23
//WhiteLabelDocId_DocId_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentToken = "select_Orders_by_keys_view_12_1_23";

//M_B_Y
//12_1_24
//WhiteLabelDocId_DocId_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentRemarks = "select_Orders_by_keys_view_12_1_24";

//M_B_Z
//12_1_25
//WhiteLabelDocId_DocId_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_Currency = "select_Orders_by_keys_view_12_1_25";

//M_C_E
//12_2_4
//WhiteLabelDocId_IsDeleted_LowCostPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR = "select_Orders_by_keys_view_12_2_4";

//M_C_F
//12_2_5
//WhiteLabelDocId_IsDeleted_SupplierPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_SupplierPNR = "select_Orders_by_keys_view_12_2_5";

//M_C_G
//12_2_6
//WhiteLabelDocId_IsDeleted_TotalPrice
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_TotalPrice = "select_Orders_by_keys_view_12_2_6";

//M_C_H
//12_2_7
//WhiteLabelDocId_IsDeleted_DateOfBook
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_DateOfBook = "select_Orders_by_keys_view_12_2_7";

//M_C_I
//12_2_8
//WhiteLabelDocId_IsDeleted_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ReceiveEmail = "select_Orders_by_keys_view_12_2_8";

//M_C_J
//12_2_9
//WhiteLabelDocId_IsDeleted_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ReceiveSms = "select_Orders_by_keys_view_12_2_9";

//M_C_K
//12_2_10
//WhiteLabelDocId_IsDeleted_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_MarketingCabin = "select_Orders_by_keys_view_12_2_10";

//M_C_L
//12_2_11
//WhiteLabelDocId_IsDeleted_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ContactDetailsDocId = "select_Orders_by_keys_view_12_2_11";

//M_C_N
//12_2_13
//WhiteLabelDocId_IsDeleted_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineIataCode = "select_Orders_by_keys_view_12_2_13";

//M_C_O
//12_2_14
//WhiteLabelDocId_IsDeleted_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderStatus = "select_Orders_by_keys_view_12_2_14";

//M_C_P
//12_2_15
//WhiteLabelDocId_IsDeleted_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_AdminRemarks = "select_Orders_by_keys_view_12_2_15";

//M_C_Q
//12_2_16
//WhiteLabelDocId_IsDeleted_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_StatusPayment = "select_Orders_by_keys_view_12_2_16";

//M_C_R
//12_2_17
//WhiteLabelDocId_IsDeleted_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderToken = "select_Orders_by_keys_view_12_2_17";

//M_C_S
//12_2_18
//WhiteLabelDocId_IsDeleted_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentTransaction = "select_Orders_by_keys_view_12_2_18";

//M_C_T
//12_2_19
//WhiteLabelDocId_IsDeleted_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_XmlBookRequest = "select_Orders_by_keys_view_12_2_19";

//M_C_U
//12_2_20
//WhiteLabelDocId_IsDeleted_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_XmlBookResponse = "select_Orders_by_keys_view_12_2_20";

//M_C_V
//12_2_21
//WhiteLabelDocId_IsDeleted_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderRemarks = "select_Orders_by_keys_view_12_2_21";

//M_C_W
//12_2_22
//WhiteLabelDocId_IsDeleted_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderPayment = "select_Orders_by_keys_view_12_2_22";

//M_C_X
//12_2_23
//WhiteLabelDocId_IsDeleted_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentToken = "select_Orders_by_keys_view_12_2_23";

//M_C_Y
//12_2_24
//WhiteLabelDocId_IsDeleted_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentRemarks = "select_Orders_by_keys_view_12_2_24";

//M_C_Z
//12_2_25
//WhiteLabelDocId_IsDeleted_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_Currency = "select_Orders_by_keys_view_12_2_25";

//M_E_F
//12_4_5
//WhiteLabelDocId_LowCostPNR_SupplierPNR
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_SupplierPNR = "select_Orders_by_keys_view_12_4_5";

//M_E_G
//12_4_6
//WhiteLabelDocId_LowCostPNR_TotalPrice
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_TotalPrice = "select_Orders_by_keys_view_12_4_6";

//M_E_H
//12_4_7
//WhiteLabelDocId_LowCostPNR_DateOfBook
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_DateOfBook = "select_Orders_by_keys_view_12_4_7";

//M_E_I
//12_4_8
//WhiteLabelDocId_LowCostPNR_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ReceiveEmail = "select_Orders_by_keys_view_12_4_8";

//M_E_J
//12_4_9
//WhiteLabelDocId_LowCostPNR_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ReceiveSms = "select_Orders_by_keys_view_12_4_9";

//M_E_K
//12_4_10
//WhiteLabelDocId_LowCostPNR_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_MarketingCabin = "select_Orders_by_keys_view_12_4_10";

//M_E_L
//12_4_11
//WhiteLabelDocId_LowCostPNR_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ContactDetailsDocId = "select_Orders_by_keys_view_12_4_11";

//M_E_N
//12_4_13
//WhiteLabelDocId_LowCostPNR_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_AirlineIataCode = "select_Orders_by_keys_view_12_4_13";

//M_E_O
//12_4_14
//WhiteLabelDocId_LowCostPNR_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderStatus = "select_Orders_by_keys_view_12_4_14";

//M_E_P
//12_4_15
//WhiteLabelDocId_LowCostPNR_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_AdminRemarks = "select_Orders_by_keys_view_12_4_15";

//M_E_Q
//12_4_16
//WhiteLabelDocId_LowCostPNR_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_StatusPayment = "select_Orders_by_keys_view_12_4_16";

//M_E_R
//12_4_17
//WhiteLabelDocId_LowCostPNR_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderToken = "select_Orders_by_keys_view_12_4_17";

//M_E_S
//12_4_18
//WhiteLabelDocId_LowCostPNR_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentTransaction = "select_Orders_by_keys_view_12_4_18";

//M_E_T
//12_4_19
//WhiteLabelDocId_LowCostPNR_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_XmlBookRequest = "select_Orders_by_keys_view_12_4_19";

//M_E_U
//12_4_20
//WhiteLabelDocId_LowCostPNR_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_XmlBookResponse = "select_Orders_by_keys_view_12_4_20";

//M_E_V
//12_4_21
//WhiteLabelDocId_LowCostPNR_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderRemarks = "select_Orders_by_keys_view_12_4_21";

//M_E_W
//12_4_22
//WhiteLabelDocId_LowCostPNR_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderPayment = "select_Orders_by_keys_view_12_4_22";

//M_E_X
//12_4_23
//WhiteLabelDocId_LowCostPNR_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentToken = "select_Orders_by_keys_view_12_4_23";

//M_E_Y
//12_4_24
//WhiteLabelDocId_LowCostPNR_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentRemarks = "select_Orders_by_keys_view_12_4_24";

//M_E_Z
//12_4_25
//WhiteLabelDocId_LowCostPNR_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_Currency = "select_Orders_by_keys_view_12_4_25";

//M_F_G
//12_5_6
//WhiteLabelDocId_SupplierPNR_TotalPrice
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_TotalPrice = "select_Orders_by_keys_view_12_5_6";

//M_F_H
//12_5_7
//WhiteLabelDocId_SupplierPNR_DateOfBook
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_DateOfBook = "select_Orders_by_keys_view_12_5_7";

//M_F_I
//12_5_8
//WhiteLabelDocId_SupplierPNR_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ReceiveEmail = "select_Orders_by_keys_view_12_5_8";

//M_F_J
//12_5_9
//WhiteLabelDocId_SupplierPNR_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ReceiveSms = "select_Orders_by_keys_view_12_5_9";

//M_F_K
//12_5_10
//WhiteLabelDocId_SupplierPNR_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_MarketingCabin = "select_Orders_by_keys_view_12_5_10";

//M_F_L
//12_5_11
//WhiteLabelDocId_SupplierPNR_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ContactDetailsDocId = "select_Orders_by_keys_view_12_5_11";

//M_F_N
//12_5_13
//WhiteLabelDocId_SupplierPNR_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_AirlineIataCode = "select_Orders_by_keys_view_12_5_13";

//M_F_O
//12_5_14
//WhiteLabelDocId_SupplierPNR_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderStatus = "select_Orders_by_keys_view_12_5_14";

//M_F_P
//12_5_15
//WhiteLabelDocId_SupplierPNR_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_AdminRemarks = "select_Orders_by_keys_view_12_5_15";

//M_F_Q
//12_5_16
//WhiteLabelDocId_SupplierPNR_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_StatusPayment = "select_Orders_by_keys_view_12_5_16";

//M_F_R
//12_5_17
//WhiteLabelDocId_SupplierPNR_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderToken = "select_Orders_by_keys_view_12_5_17";

//M_F_S
//12_5_18
//WhiteLabelDocId_SupplierPNR_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentTransaction = "select_Orders_by_keys_view_12_5_18";

//M_F_T
//12_5_19
//WhiteLabelDocId_SupplierPNR_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_XmlBookRequest = "select_Orders_by_keys_view_12_5_19";

//M_F_U
//12_5_20
//WhiteLabelDocId_SupplierPNR_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_XmlBookResponse = "select_Orders_by_keys_view_12_5_20";

//M_F_V
//12_5_21
//WhiteLabelDocId_SupplierPNR_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderRemarks = "select_Orders_by_keys_view_12_5_21";

//M_F_W
//12_5_22
//WhiteLabelDocId_SupplierPNR_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderPayment = "select_Orders_by_keys_view_12_5_22";

//M_F_X
//12_5_23
//WhiteLabelDocId_SupplierPNR_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentToken = "select_Orders_by_keys_view_12_5_23";

//M_F_Y
//12_5_24
//WhiteLabelDocId_SupplierPNR_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentRemarks = "select_Orders_by_keys_view_12_5_24";

//M_F_Z
//12_5_25
//WhiteLabelDocId_SupplierPNR_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_Currency = "select_Orders_by_keys_view_12_5_25";

//M_G_H
//12_6_7
//WhiteLabelDocId_TotalPrice_DateOfBook
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_DateOfBook = "select_Orders_by_keys_view_12_6_7";

//M_G_I
//12_6_8
//WhiteLabelDocId_TotalPrice_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ReceiveEmail = "select_Orders_by_keys_view_12_6_8";

//M_G_J
//12_6_9
//WhiteLabelDocId_TotalPrice_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ReceiveSms = "select_Orders_by_keys_view_12_6_9";

//M_G_K
//12_6_10
//WhiteLabelDocId_TotalPrice_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_MarketingCabin = "select_Orders_by_keys_view_12_6_10";

//M_G_L
//12_6_11
//WhiteLabelDocId_TotalPrice_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ContactDetailsDocId = "select_Orders_by_keys_view_12_6_11";

//M_G_N
//12_6_13
//WhiteLabelDocId_TotalPrice_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_AirlineIataCode = "select_Orders_by_keys_view_12_6_13";

//M_G_O
//12_6_14
//WhiteLabelDocId_TotalPrice_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderStatus = "select_Orders_by_keys_view_12_6_14";

//M_G_P
//12_6_15
//WhiteLabelDocId_TotalPrice_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_AdminRemarks = "select_Orders_by_keys_view_12_6_15";

//M_G_Q
//12_6_16
//WhiteLabelDocId_TotalPrice_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_StatusPayment = "select_Orders_by_keys_view_12_6_16";

//M_G_R
//12_6_17
//WhiteLabelDocId_TotalPrice_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderToken = "select_Orders_by_keys_view_12_6_17";

//M_G_S
//12_6_18
//WhiteLabelDocId_TotalPrice_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentTransaction = "select_Orders_by_keys_view_12_6_18";

//M_G_T
//12_6_19
//WhiteLabelDocId_TotalPrice_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_XmlBookRequest = "select_Orders_by_keys_view_12_6_19";

//M_G_U
//12_6_20
//WhiteLabelDocId_TotalPrice_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_XmlBookResponse = "select_Orders_by_keys_view_12_6_20";

//M_G_V
//12_6_21
//WhiteLabelDocId_TotalPrice_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderRemarks = "select_Orders_by_keys_view_12_6_21";

//M_G_W
//12_6_22
//WhiteLabelDocId_TotalPrice_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderPayment = "select_Orders_by_keys_view_12_6_22";

//M_G_X
//12_6_23
//WhiteLabelDocId_TotalPrice_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentToken = "select_Orders_by_keys_view_12_6_23";

//M_G_Y
//12_6_24
//WhiteLabelDocId_TotalPrice_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentRemarks = "select_Orders_by_keys_view_12_6_24";

//M_G_Z
//12_6_25
//WhiteLabelDocId_TotalPrice_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_Currency = "select_Orders_by_keys_view_12_6_25";

//M_H_I
//12_7_8
//WhiteLabelDocId_DateOfBook_ReceiveEmail
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ReceiveEmail = "select_Orders_by_keys_view_12_7_8";

//M_H_J
//12_7_9
//WhiteLabelDocId_DateOfBook_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ReceiveSms = "select_Orders_by_keys_view_12_7_9";

//M_H_K
//12_7_10
//WhiteLabelDocId_DateOfBook_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_MarketingCabin = "select_Orders_by_keys_view_12_7_10";

//M_H_L
//12_7_11
//WhiteLabelDocId_DateOfBook_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ContactDetailsDocId = "select_Orders_by_keys_view_12_7_11";

//M_H_N
//12_7_13
//WhiteLabelDocId_DateOfBook_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_AirlineIataCode = "select_Orders_by_keys_view_12_7_13";

//M_H_O
//12_7_14
//WhiteLabelDocId_DateOfBook_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderStatus = "select_Orders_by_keys_view_12_7_14";

//M_H_P
//12_7_15
//WhiteLabelDocId_DateOfBook_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_AdminRemarks = "select_Orders_by_keys_view_12_7_15";

//M_H_Q
//12_7_16
//WhiteLabelDocId_DateOfBook_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_StatusPayment = "select_Orders_by_keys_view_12_7_16";

//M_H_R
//12_7_17
//WhiteLabelDocId_DateOfBook_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderToken = "select_Orders_by_keys_view_12_7_17";

//M_H_S
//12_7_18
//WhiteLabelDocId_DateOfBook_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentTransaction = "select_Orders_by_keys_view_12_7_18";

//M_H_T
//12_7_19
//WhiteLabelDocId_DateOfBook_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_XmlBookRequest = "select_Orders_by_keys_view_12_7_19";

//M_H_U
//12_7_20
//WhiteLabelDocId_DateOfBook_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_XmlBookResponse = "select_Orders_by_keys_view_12_7_20";

//M_H_V
//12_7_21
//WhiteLabelDocId_DateOfBook_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderRemarks = "select_Orders_by_keys_view_12_7_21";

//M_H_W
//12_7_22
//WhiteLabelDocId_DateOfBook_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderPayment = "select_Orders_by_keys_view_12_7_22";

//M_H_X
//12_7_23
//WhiteLabelDocId_DateOfBook_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentToken = "select_Orders_by_keys_view_12_7_23";

//M_H_Y
//12_7_24
//WhiteLabelDocId_DateOfBook_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentRemarks = "select_Orders_by_keys_view_12_7_24";

//M_H_Z
//12_7_25
//WhiteLabelDocId_DateOfBook_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_Currency = "select_Orders_by_keys_view_12_7_25";

//M_I_J
//12_8_9
//WhiteLabelDocId_ReceiveEmail_ReceiveSms
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_ReceiveSms = "select_Orders_by_keys_view_12_8_9";

//M_I_K
//12_8_10
//WhiteLabelDocId_ReceiveEmail_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_MarketingCabin = "select_Orders_by_keys_view_12_8_10";

//M_I_L
//12_8_11
//WhiteLabelDocId_ReceiveEmail_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_ContactDetailsDocId = "select_Orders_by_keys_view_12_8_11";

//M_I_N
//12_8_13
//WhiteLabelDocId_ReceiveEmail_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_AirlineIataCode = "select_Orders_by_keys_view_12_8_13";

//M_I_O
//12_8_14
//WhiteLabelDocId_ReceiveEmail_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderStatus = "select_Orders_by_keys_view_12_8_14";

//M_I_P
//12_8_15
//WhiteLabelDocId_ReceiveEmail_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_AdminRemarks = "select_Orders_by_keys_view_12_8_15";

//M_I_Q
//12_8_16
//WhiteLabelDocId_ReceiveEmail_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_StatusPayment = "select_Orders_by_keys_view_12_8_16";

//M_I_R
//12_8_17
//WhiteLabelDocId_ReceiveEmail_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderToken = "select_Orders_by_keys_view_12_8_17";

//M_I_S
//12_8_18
//WhiteLabelDocId_ReceiveEmail_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentTransaction = "select_Orders_by_keys_view_12_8_18";

//M_I_T
//12_8_19
//WhiteLabelDocId_ReceiveEmail_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_XmlBookRequest = "select_Orders_by_keys_view_12_8_19";

//M_I_U
//12_8_20
//WhiteLabelDocId_ReceiveEmail_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_XmlBookResponse = "select_Orders_by_keys_view_12_8_20";

//M_I_V
//12_8_21
//WhiteLabelDocId_ReceiveEmail_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderRemarks = "select_Orders_by_keys_view_12_8_21";

//M_I_W
//12_8_22
//WhiteLabelDocId_ReceiveEmail_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderPayment = "select_Orders_by_keys_view_12_8_22";

//M_I_X
//12_8_23
//WhiteLabelDocId_ReceiveEmail_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentToken = "select_Orders_by_keys_view_12_8_23";

//M_I_Y
//12_8_24
//WhiteLabelDocId_ReceiveEmail_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentRemarks = "select_Orders_by_keys_view_12_8_24";

//M_I_Z
//12_8_25
//WhiteLabelDocId_ReceiveEmail_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_Currency = "select_Orders_by_keys_view_12_8_25";

//M_J_K
//12_9_10
//WhiteLabelDocId_ReceiveSms_MarketingCabin
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_MarketingCabin = "select_Orders_by_keys_view_12_9_10";

//M_J_L
//12_9_11
//WhiteLabelDocId_ReceiveSms_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_ContactDetailsDocId = "select_Orders_by_keys_view_12_9_11";

//M_J_N
//12_9_13
//WhiteLabelDocId_ReceiveSms_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_AirlineIataCode = "select_Orders_by_keys_view_12_9_13";

//M_J_O
//12_9_14
//WhiteLabelDocId_ReceiveSms_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderStatus = "select_Orders_by_keys_view_12_9_14";

//M_J_P
//12_9_15
//WhiteLabelDocId_ReceiveSms_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_AdminRemarks = "select_Orders_by_keys_view_12_9_15";

//M_J_Q
//12_9_16
//WhiteLabelDocId_ReceiveSms_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_StatusPayment = "select_Orders_by_keys_view_12_9_16";

//M_J_R
//12_9_17
//WhiteLabelDocId_ReceiveSms_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderToken = "select_Orders_by_keys_view_12_9_17";

//M_J_S
//12_9_18
//WhiteLabelDocId_ReceiveSms_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentTransaction = "select_Orders_by_keys_view_12_9_18";

//M_J_T
//12_9_19
//WhiteLabelDocId_ReceiveSms_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_XmlBookRequest = "select_Orders_by_keys_view_12_9_19";

//M_J_U
//12_9_20
//WhiteLabelDocId_ReceiveSms_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_XmlBookResponse = "select_Orders_by_keys_view_12_9_20";

//M_J_V
//12_9_21
//WhiteLabelDocId_ReceiveSms_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderRemarks = "select_Orders_by_keys_view_12_9_21";

//M_J_W
//12_9_22
//WhiteLabelDocId_ReceiveSms_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderPayment = "select_Orders_by_keys_view_12_9_22";

//M_J_X
//12_9_23
//WhiteLabelDocId_ReceiveSms_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentToken = "select_Orders_by_keys_view_12_9_23";

//M_J_Y
//12_9_24
//WhiteLabelDocId_ReceiveSms_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentRemarks = "select_Orders_by_keys_view_12_9_24";

//M_J_Z
//12_9_25
//WhiteLabelDocId_ReceiveSms_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_Currency = "select_Orders_by_keys_view_12_9_25";

//M_K_L
//12_10_11
//WhiteLabelDocId_MarketingCabin_ContactDetailsDocId
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_ContactDetailsDocId = "select_Orders_by_keys_view_12_10_11";

//M_K_N
//12_10_13
//WhiteLabelDocId_MarketingCabin_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_AirlineIataCode = "select_Orders_by_keys_view_12_10_13";

//M_K_O
//12_10_14
//WhiteLabelDocId_MarketingCabin_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderStatus = "select_Orders_by_keys_view_12_10_14";

//M_K_P
//12_10_15
//WhiteLabelDocId_MarketingCabin_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_AdminRemarks = "select_Orders_by_keys_view_12_10_15";

//M_K_Q
//12_10_16
//WhiteLabelDocId_MarketingCabin_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_StatusPayment = "select_Orders_by_keys_view_12_10_16";

//M_K_R
//12_10_17
//WhiteLabelDocId_MarketingCabin_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderToken = "select_Orders_by_keys_view_12_10_17";

//M_K_S
//12_10_18
//WhiteLabelDocId_MarketingCabin_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentTransaction = "select_Orders_by_keys_view_12_10_18";

//M_K_T
//12_10_19
//WhiteLabelDocId_MarketingCabin_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_XmlBookRequest = "select_Orders_by_keys_view_12_10_19";

//M_K_U
//12_10_20
//WhiteLabelDocId_MarketingCabin_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_XmlBookResponse = "select_Orders_by_keys_view_12_10_20";

//M_K_V
//12_10_21
//WhiteLabelDocId_MarketingCabin_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderRemarks = "select_Orders_by_keys_view_12_10_21";

//M_K_W
//12_10_22
//WhiteLabelDocId_MarketingCabin_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderPayment = "select_Orders_by_keys_view_12_10_22";

//M_K_X
//12_10_23
//WhiteLabelDocId_MarketingCabin_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentToken = "select_Orders_by_keys_view_12_10_23";

//M_K_Y
//12_10_24
//WhiteLabelDocId_MarketingCabin_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentRemarks = "select_Orders_by_keys_view_12_10_24";

//M_K_Z
//12_10_25
//WhiteLabelDocId_MarketingCabin_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_Currency = "select_Orders_by_keys_view_12_10_25";

//M_L_N
//12_11_13
//WhiteLabelDocId_ContactDetailsDocId_AirlineIataCode
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_AirlineIataCode = "select_Orders_by_keys_view_12_11_13";

//M_L_O
//12_11_14
//WhiteLabelDocId_ContactDetailsDocId_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderStatus = "select_Orders_by_keys_view_12_11_14";

//M_L_P
//12_11_15
//WhiteLabelDocId_ContactDetailsDocId_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_AdminRemarks = "select_Orders_by_keys_view_12_11_15";

//M_L_Q
//12_11_16
//WhiteLabelDocId_ContactDetailsDocId_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_StatusPayment = "select_Orders_by_keys_view_12_11_16";

//M_L_R
//12_11_17
//WhiteLabelDocId_ContactDetailsDocId_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderToken = "select_Orders_by_keys_view_12_11_17";

//M_L_S
//12_11_18
//WhiteLabelDocId_ContactDetailsDocId_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentTransaction = "select_Orders_by_keys_view_12_11_18";

//M_L_T
//12_11_19
//WhiteLabelDocId_ContactDetailsDocId_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_XmlBookRequest = "select_Orders_by_keys_view_12_11_19";

//M_L_U
//12_11_20
//WhiteLabelDocId_ContactDetailsDocId_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_XmlBookResponse = "select_Orders_by_keys_view_12_11_20";

//M_L_V
//12_11_21
//WhiteLabelDocId_ContactDetailsDocId_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderRemarks = "select_Orders_by_keys_view_12_11_21";

//M_L_W
//12_11_22
//WhiteLabelDocId_ContactDetailsDocId_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderPayment = "select_Orders_by_keys_view_12_11_22";

//M_L_X
//12_11_23
//WhiteLabelDocId_ContactDetailsDocId_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentToken = "select_Orders_by_keys_view_12_11_23";

//M_L_Y
//12_11_24
//WhiteLabelDocId_ContactDetailsDocId_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentRemarks = "select_Orders_by_keys_view_12_11_24";

//M_L_Z
//12_11_25
//WhiteLabelDocId_ContactDetailsDocId_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_Currency = "select_Orders_by_keys_view_12_11_25";

//M_N_O
//12_13_14
//WhiteLabelDocId_AirlineIataCode_OrderStatus
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderStatus = "select_Orders_by_keys_view_12_13_14";

//M_N_P
//12_13_15
//WhiteLabelDocId_AirlineIataCode_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_AdminRemarks = "select_Orders_by_keys_view_12_13_15";

//M_N_Q
//12_13_16
//WhiteLabelDocId_AirlineIataCode_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_StatusPayment = "select_Orders_by_keys_view_12_13_16";

//M_N_R
//12_13_17
//WhiteLabelDocId_AirlineIataCode_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderToken = "select_Orders_by_keys_view_12_13_17";

//M_N_S
//12_13_18
//WhiteLabelDocId_AirlineIataCode_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentTransaction = "select_Orders_by_keys_view_12_13_18";

//M_N_T
//12_13_19
//WhiteLabelDocId_AirlineIataCode_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_XmlBookRequest = "select_Orders_by_keys_view_12_13_19";

//M_N_U
//12_13_20
//WhiteLabelDocId_AirlineIataCode_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_XmlBookResponse = "select_Orders_by_keys_view_12_13_20";

//M_N_V
//12_13_21
//WhiteLabelDocId_AirlineIataCode_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderRemarks = "select_Orders_by_keys_view_12_13_21";

//M_N_W
//12_13_22
//WhiteLabelDocId_AirlineIataCode_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderPayment = "select_Orders_by_keys_view_12_13_22";

//M_N_X
//12_13_23
//WhiteLabelDocId_AirlineIataCode_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentToken = "select_Orders_by_keys_view_12_13_23";

//M_N_Y
//12_13_24
//WhiteLabelDocId_AirlineIataCode_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentRemarks = "select_Orders_by_keys_view_12_13_24";

//M_N_Z
//12_13_25
//WhiteLabelDocId_AirlineIataCode_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_Currency = "select_Orders_by_keys_view_12_13_25";

//M_O_P
//12_14_15
//WhiteLabelDocId_OrderStatus_AdminRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_AdminRemarks = "select_Orders_by_keys_view_12_14_15";

//M_O_Q
//12_14_16
//WhiteLabelDocId_OrderStatus_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_StatusPayment = "select_Orders_by_keys_view_12_14_16";

//M_O_R
//12_14_17
//WhiteLabelDocId_OrderStatus_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderToken = "select_Orders_by_keys_view_12_14_17";

//M_O_S
//12_14_18
//WhiteLabelDocId_OrderStatus_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentTransaction = "select_Orders_by_keys_view_12_14_18";

//M_O_T
//12_14_19
//WhiteLabelDocId_OrderStatus_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_XmlBookRequest = "select_Orders_by_keys_view_12_14_19";

//M_O_U
//12_14_20
//WhiteLabelDocId_OrderStatus_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_XmlBookResponse = "select_Orders_by_keys_view_12_14_20";

//M_O_V
//12_14_21
//WhiteLabelDocId_OrderStatus_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderRemarks = "select_Orders_by_keys_view_12_14_21";

//M_O_W
//12_14_22
//WhiteLabelDocId_OrderStatus_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderPayment = "select_Orders_by_keys_view_12_14_22";

//M_O_X
//12_14_23
//WhiteLabelDocId_OrderStatus_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentToken = "select_Orders_by_keys_view_12_14_23";

//M_O_Y
//12_14_24
//WhiteLabelDocId_OrderStatus_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentRemarks = "select_Orders_by_keys_view_12_14_24";

//M_O_Z
//12_14_25
//WhiteLabelDocId_OrderStatus_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_Currency = "select_Orders_by_keys_view_12_14_25";

//M_P_Q
//12_15_16
//WhiteLabelDocId_AdminRemarks_StatusPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_StatusPayment = "select_Orders_by_keys_view_12_15_16";

//M_P_R
//12_15_17
//WhiteLabelDocId_AdminRemarks_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderToken = "select_Orders_by_keys_view_12_15_17";

//M_P_S
//12_15_18
//WhiteLabelDocId_AdminRemarks_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentTransaction = "select_Orders_by_keys_view_12_15_18";

//M_P_T
//12_15_19
//WhiteLabelDocId_AdminRemarks_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_XmlBookRequest = "select_Orders_by_keys_view_12_15_19";

//M_P_U
//12_15_20
//WhiteLabelDocId_AdminRemarks_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_XmlBookResponse = "select_Orders_by_keys_view_12_15_20";

//M_P_V
//12_15_21
//WhiteLabelDocId_AdminRemarks_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderRemarks = "select_Orders_by_keys_view_12_15_21";

//M_P_W
//12_15_22
//WhiteLabelDocId_AdminRemarks_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderPayment = "select_Orders_by_keys_view_12_15_22";

//M_P_X
//12_15_23
//WhiteLabelDocId_AdminRemarks_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentToken = "select_Orders_by_keys_view_12_15_23";

//M_P_Y
//12_15_24
//WhiteLabelDocId_AdminRemarks_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentRemarks = "select_Orders_by_keys_view_12_15_24";

//M_P_Z
//12_15_25
//WhiteLabelDocId_AdminRemarks_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_Currency = "select_Orders_by_keys_view_12_15_25";

//M_Q_R
//12_16_17
//WhiteLabelDocId_StatusPayment_OrderToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderToken = "select_Orders_by_keys_view_12_16_17";

//M_Q_S
//12_16_18
//WhiteLabelDocId_StatusPayment_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentTransaction = "select_Orders_by_keys_view_12_16_18";

//M_Q_T
//12_16_19
//WhiteLabelDocId_StatusPayment_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_XmlBookRequest = "select_Orders_by_keys_view_12_16_19";

//M_Q_U
//12_16_20
//WhiteLabelDocId_StatusPayment_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_XmlBookResponse = "select_Orders_by_keys_view_12_16_20";

//M_Q_V
//12_16_21
//WhiteLabelDocId_StatusPayment_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderRemarks = "select_Orders_by_keys_view_12_16_21";

//M_Q_W
//12_16_22
//WhiteLabelDocId_StatusPayment_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderPayment = "select_Orders_by_keys_view_12_16_22";

//M_Q_X
//12_16_23
//WhiteLabelDocId_StatusPayment_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentToken = "select_Orders_by_keys_view_12_16_23";

//M_Q_Y
//12_16_24
//WhiteLabelDocId_StatusPayment_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentRemarks = "select_Orders_by_keys_view_12_16_24";

//M_Q_Z
//12_16_25
//WhiteLabelDocId_StatusPayment_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_Currency = "select_Orders_by_keys_view_12_16_25";

//M_R_S
//12_17_18
//WhiteLabelDocId_OrderToken_PaymentTransaction
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentTransaction = "select_Orders_by_keys_view_12_17_18";

//M_R_T
//12_17_19
//WhiteLabelDocId_OrderToken_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_XmlBookRequest = "select_Orders_by_keys_view_12_17_19";

//M_R_U
//12_17_20
//WhiteLabelDocId_OrderToken_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_XmlBookResponse = "select_Orders_by_keys_view_12_17_20";

//M_R_V
//12_17_21
//WhiteLabelDocId_OrderToken_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_OrderRemarks = "select_Orders_by_keys_view_12_17_21";

//M_R_W
//12_17_22
//WhiteLabelDocId_OrderToken_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_OrderPayment = "select_Orders_by_keys_view_12_17_22";

//M_R_X
//12_17_23
//WhiteLabelDocId_OrderToken_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentToken = "select_Orders_by_keys_view_12_17_23";

//M_R_Y
//12_17_24
//WhiteLabelDocId_OrderToken_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentRemarks = "select_Orders_by_keys_view_12_17_24";

//M_R_Z
//12_17_25
//WhiteLabelDocId_OrderToken_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_Currency = "select_Orders_by_keys_view_12_17_25";

//M_S_T
//12_18_19
//WhiteLabelDocId_PaymentTransaction_XmlBookRequest
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_XmlBookRequest = "select_Orders_by_keys_view_12_18_19";

//M_S_U
//12_18_20
//WhiteLabelDocId_PaymentTransaction_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_XmlBookResponse = "select_Orders_by_keys_view_12_18_20";

//M_S_V
//12_18_21
//WhiteLabelDocId_PaymentTransaction_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_OrderRemarks = "select_Orders_by_keys_view_12_18_21";

//M_S_W
//12_18_22
//WhiteLabelDocId_PaymentTransaction_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_OrderPayment = "select_Orders_by_keys_view_12_18_22";

//M_S_X
//12_18_23
//WhiteLabelDocId_PaymentTransaction_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_PaymentToken = "select_Orders_by_keys_view_12_18_23";

//M_S_Y
//12_18_24
//WhiteLabelDocId_PaymentTransaction_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_PaymentRemarks = "select_Orders_by_keys_view_12_18_24";

//M_S_Z
//12_18_25
//WhiteLabelDocId_PaymentTransaction_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_Currency = "select_Orders_by_keys_view_12_18_25";

//M_T_U
//12_19_20
//WhiteLabelDocId_XmlBookRequest_XmlBookResponse
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_XmlBookResponse = "select_Orders_by_keys_view_12_19_20";

//M_T_V
//12_19_21
//WhiteLabelDocId_XmlBookRequest_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_OrderRemarks = "select_Orders_by_keys_view_12_19_21";

//M_T_W
//12_19_22
//WhiteLabelDocId_XmlBookRequest_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_OrderPayment = "select_Orders_by_keys_view_12_19_22";

//M_T_X
//12_19_23
//WhiteLabelDocId_XmlBookRequest_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_PaymentToken = "select_Orders_by_keys_view_12_19_23";

//M_T_Y
//12_19_24
//WhiteLabelDocId_XmlBookRequest_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_PaymentRemarks = "select_Orders_by_keys_view_12_19_24";

//M_T_Z
//12_19_25
//WhiteLabelDocId_XmlBookRequest_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_Currency = "select_Orders_by_keys_view_12_19_25";

//M_U_V
//12_20_21
//WhiteLabelDocId_XmlBookResponse_OrderRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_OrderRemarks = "select_Orders_by_keys_view_12_20_21";

//M_U_W
//12_20_22
//WhiteLabelDocId_XmlBookResponse_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_OrderPayment = "select_Orders_by_keys_view_12_20_22";

//M_U_X
//12_20_23
//WhiteLabelDocId_XmlBookResponse_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_PaymentToken = "select_Orders_by_keys_view_12_20_23";

//M_U_Y
//12_20_24
//WhiteLabelDocId_XmlBookResponse_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_PaymentRemarks = "select_Orders_by_keys_view_12_20_24";

//M_U_Z
//12_20_25
//WhiteLabelDocId_XmlBookResponse_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_Currency = "select_Orders_by_keys_view_12_20_25";

//M_V_W
//12_21_22
//WhiteLabelDocId_OrderRemarks_OrderPayment
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_OrderPayment = "select_Orders_by_keys_view_12_21_22";

//M_V_X
//12_21_23
//WhiteLabelDocId_OrderRemarks_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_PaymentToken = "select_Orders_by_keys_view_12_21_23";

//M_V_Y
//12_21_24
//WhiteLabelDocId_OrderRemarks_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_PaymentRemarks = "select_Orders_by_keys_view_12_21_24";

//M_V_Z
//12_21_25
//WhiteLabelDocId_OrderRemarks_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_Currency = "select_Orders_by_keys_view_12_21_25";

//M_W_X
//12_22_23
//WhiteLabelDocId_OrderPayment_PaymentToken
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_PaymentToken = "select_Orders_by_keys_view_12_22_23";

//M_W_Y
//12_22_24
//WhiteLabelDocId_OrderPayment_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_PaymentRemarks = "select_Orders_by_keys_view_12_22_24";

//M_W_Z
//12_22_25
//WhiteLabelDocId_OrderPayment_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_Currency = "select_Orders_by_keys_view_12_22_25";

//M_X_Y
//12_23_24
//WhiteLabelDocId_PaymentToken_PaymentRemarks
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken_PaymentRemarks = "select_Orders_by_keys_view_12_23_24";

//M_X_Z
//12_23_25
//WhiteLabelDocId_PaymentToken_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken_Currency = "select_Orders_by_keys_view_12_23_25";

//M_Y_Z
//12_24_25
//WhiteLabelDocId_PaymentRemarks_Currency
public static readonly string  PROC_Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentRemarks_Currency = "select_Orders_by_keys_view_12_24_25";
         #endregion

    }

}
