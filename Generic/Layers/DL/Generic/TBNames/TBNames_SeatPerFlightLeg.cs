

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_SeatPerFlightLeg
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertSeatPerFlightLeg = PROC_Prefix + "save_seat_per_flight_leg";
        #endregion
        #region Select
        public static readonly string PROC_Select_SeatPerFlightLeg_By_DocId = PROC_Prefix + "Select_seat_per_flight_leg_By_doc_id";        
        public static readonly string PROC_Select_SeatPerFlightLeg_By_Keys_View = PROC_Prefix + "Select_seat_per_flight_leg_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteSeatPerFlightLeg = PROC_Prefix + "delete_seat_per_flight_leg";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegIsActive);

        public static readonly string PRM_PassengerId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegPassengerId);

        public static readonly string PRM_Seat = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegSeat);

        public static readonly string PRM_FlightNumber = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegFlightNumber);

        public static readonly string PRM_PassengerDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_FlightLegDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "SeatPerFlightLeg.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegIsActive);

        public static readonly string Field_PassengerId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegPassengerId);

        public static readonly string Field_Seat = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegSeat);

        public static readonly string Field_FlightNumber = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeatPerFlightLegFlightNumber);

        public static readonly string Field_PassengerDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPassengerDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_FlightLegDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyFlightLegDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//I_A
//8_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated = "select_SeatPerFlightLeg_by_keys_view_8_0";

//I_B
//8_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId = "select_SeatPerFlightLeg_by_keys_view_8_1";

//I_C
//8_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_SeatPerFlightLeg_by_keys_view_8_2";

//I_E
//8_4
//WhiteLabelDocId_PassengerId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId = "select_SeatPerFlightLeg_by_keys_view_8_4";

//I_F
//8_5
//WhiteLabelDocId_Seat
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat = "select_SeatPerFlightLeg_by_keys_view_8_5";

//I_G
//8_6
//WhiteLabelDocId_FlightNumber
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber = "select_SeatPerFlightLeg_by_keys_view_8_6";

//I_H
//8_7
//WhiteLabelDocId_PassengerDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerDocId = "select_SeatPerFlightLeg_by_keys_view_8_7";

//I_J
//8_9
//WhiteLabelDocId_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_9";

//I_A_B
//8_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_SeatPerFlightLeg_by_keys_view_8_0_1";

//I_A_C
//8_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_SeatPerFlightLeg_by_keys_view_8_0_2";

//I_A_E
//8_0_4
//WhiteLabelDocId_DateCreated_PassengerId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerId = "select_SeatPerFlightLeg_by_keys_view_8_0_4";

//I_A_F
//8_0_5
//WhiteLabelDocId_DateCreated_Seat
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_Seat = "select_SeatPerFlightLeg_by_keys_view_8_0_5";

//I_A_G
//8_0_6
//WhiteLabelDocId_DateCreated_FlightNumber
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightNumber = "select_SeatPerFlightLeg_by_keys_view_8_0_6";

//I_A_H
//8_0_7
//WhiteLabelDocId_DateCreated_PassengerDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_PassengerDocId = "select_SeatPerFlightLeg_by_keys_view_8_0_7";

//I_A_J
//8_0_9
//WhiteLabelDocId_DateCreated_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DateCreated_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_0_9";

//I_B_C
//8_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_SeatPerFlightLeg_by_keys_view_8_1_2";

//I_B_E
//8_1_4
//WhiteLabelDocId_DocId_PassengerId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PassengerId = "select_SeatPerFlightLeg_by_keys_view_8_1_4";

//I_B_F
//8_1_5
//WhiteLabelDocId_DocId_Seat
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_Seat = "select_SeatPerFlightLeg_by_keys_view_8_1_5";

//I_B_G
//8_1_6
//WhiteLabelDocId_DocId_FlightNumber
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightNumber = "select_SeatPerFlightLeg_by_keys_view_8_1_6";

//I_B_H
//8_1_7
//WhiteLabelDocId_DocId_PassengerDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_PassengerDocId = "select_SeatPerFlightLeg_by_keys_view_8_1_7";

//I_B_J
//8_1_9
//WhiteLabelDocId_DocId_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_DocId_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_1_9";

//I_C_E
//8_2_4
//WhiteLabelDocId_IsDeleted_PassengerId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerId = "select_SeatPerFlightLeg_by_keys_view_8_2_4";

//I_C_F
//8_2_5
//WhiteLabelDocId_IsDeleted_Seat
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_Seat = "select_SeatPerFlightLeg_by_keys_view_8_2_5";

//I_C_G
//8_2_6
//WhiteLabelDocId_IsDeleted_FlightNumber
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightNumber = "select_SeatPerFlightLeg_by_keys_view_8_2_6";

//I_C_H
//8_2_7
//WhiteLabelDocId_IsDeleted_PassengerDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_PassengerDocId = "select_SeatPerFlightLeg_by_keys_view_8_2_7";

//I_C_J
//8_2_9
//WhiteLabelDocId_IsDeleted_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_IsDeleted_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_2_9";

//I_E_F
//8_4_5
//WhiteLabelDocId_PassengerId_Seat
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_Seat = "select_SeatPerFlightLeg_by_keys_view_8_4_5";

//I_E_G
//8_4_6
//WhiteLabelDocId_PassengerId_FlightNumber
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_FlightNumber = "select_SeatPerFlightLeg_by_keys_view_8_4_6";

//I_E_H
//8_4_7
//WhiteLabelDocId_PassengerId_PassengerDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_PassengerDocId = "select_SeatPerFlightLeg_by_keys_view_8_4_7";

//I_E_J
//8_4_9
//WhiteLabelDocId_PassengerId_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerId_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_4_9";

//I_F_G
//8_5_6
//WhiteLabelDocId_Seat_FlightNumber
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_FlightNumber = "select_SeatPerFlightLeg_by_keys_view_8_5_6";

//I_F_H
//8_5_7
//WhiteLabelDocId_Seat_PassengerDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_PassengerDocId = "select_SeatPerFlightLeg_by_keys_view_8_5_7";

//I_F_J
//8_5_9
//WhiteLabelDocId_Seat_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_Seat_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_5_9";

//I_G_H
//8_6_7
//WhiteLabelDocId_FlightNumber_PassengerDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_PassengerDocId = "select_SeatPerFlightLeg_by_keys_view_8_6_7";

//I_G_J
//8_6_9
//WhiteLabelDocId_FlightNumber_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_FlightNumber_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_6_9";

//I_H_J
//8_7_9
//WhiteLabelDocId_PassengerDocId_FlightLegDocId
public static readonly string  PROC_Select_SeatPerFlightLeg_By_Keys_View_WhiteLabelDocId_PassengerDocId_FlightLegDocId = "select_SeatPerFlightLeg_by_keys_view_8_7_9";
         #endregion

    }

}
