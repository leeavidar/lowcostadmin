

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Users
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertUsers = PROC_Prefix + "save_users";
        #endregion
        #region Select
        public static readonly string PROC_Select_Users_By_DocId = PROC_Prefix + "Select_users_By_doc_id";        
        public static readonly string PROC_Select_Users_By_Keys_View = PROC_Prefix + "Select_users_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteUsers = PROC_Prefix + "delete_users";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersIsActive);

        public static readonly string PRM_UserName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersUserName);

        public static readonly string PRM_Password = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersPassword);

        public static readonly string PRM_Email = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersEmail);

        public static readonly string PRM_FirstName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersFirstName);

        public static readonly string PRM_LastName = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersLastName);

        public static readonly string PRM_Role = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersRole);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Users.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersIsActive);

        public static readonly string Field_UserName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersUserName);

        public static readonly string Field_Password = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersPassword);

        public static readonly string Field_Email = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersEmail);

        public static readonly string Field_FirstName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersFirstName);

        public static readonly string Field_LastName = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersLastName);

        public static readonly string Field_Role = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyUsersRole);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//K_A
//10_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated = "select_Users_by_keys_view_10_0";

//K_B
//10_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId = "select_Users_by_keys_view_10_1";

//K_C
//10_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_Users_by_keys_view_10_2";

//K_E
//10_4
//WhiteLabelDocId_UserName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName = "select_Users_by_keys_view_10_4";

//K_F
//10_5
//WhiteLabelDocId_Password
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password = "select_Users_by_keys_view_10_5";

//K_G
//10_6
//WhiteLabelDocId_Email
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email = "select_Users_by_keys_view_10_6";

//K_H
//10_7
//WhiteLabelDocId_FirstName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_FirstName = "select_Users_by_keys_view_10_7";

//K_I
//10_8
//WhiteLabelDocId_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_LastName = "select_Users_by_keys_view_10_8";

//K_J
//10_9
//WhiteLabelDocId_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Role = "select_Users_by_keys_view_10_9";

//K_A_B
//10_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_Users_by_keys_view_10_0_1";

//K_A_C
//10_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_Users_by_keys_view_10_0_2";

//K_A_E
//10_0_4
//WhiteLabelDocId_DateCreated_UserName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_UserName = "select_Users_by_keys_view_10_0_4";

//K_A_F
//10_0_5
//WhiteLabelDocId_DateCreated_Password
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Password = "select_Users_by_keys_view_10_0_5";

//K_A_G
//10_0_6
//WhiteLabelDocId_DateCreated_Email
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Email = "select_Users_by_keys_view_10_0_6";

//K_A_H
//10_0_7
//WhiteLabelDocId_DateCreated_FirstName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_FirstName = "select_Users_by_keys_view_10_0_7";

//K_A_I
//10_0_8
//WhiteLabelDocId_DateCreated_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_LastName = "select_Users_by_keys_view_10_0_8";

//K_A_J
//10_0_9
//WhiteLabelDocId_DateCreated_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DateCreated_Role = "select_Users_by_keys_view_10_0_9";

//K_B_C
//10_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_Users_by_keys_view_10_1_2";

//K_B_E
//10_1_4
//WhiteLabelDocId_DocId_UserName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_UserName = "select_Users_by_keys_view_10_1_4";

//K_B_F
//10_1_5
//WhiteLabelDocId_DocId_Password
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Password = "select_Users_by_keys_view_10_1_5";

//K_B_G
//10_1_6
//WhiteLabelDocId_DocId_Email
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Email = "select_Users_by_keys_view_10_1_6";

//K_B_H
//10_1_7
//WhiteLabelDocId_DocId_FirstName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_FirstName = "select_Users_by_keys_view_10_1_7";

//K_B_I
//10_1_8
//WhiteLabelDocId_DocId_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_LastName = "select_Users_by_keys_view_10_1_8";

//K_B_J
//10_1_9
//WhiteLabelDocId_DocId_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_DocId_Role = "select_Users_by_keys_view_10_1_9";

//K_C_E
//10_2_4
//WhiteLabelDocId_IsDeleted_UserName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_UserName = "select_Users_by_keys_view_10_2_4";

//K_C_F
//10_2_5
//WhiteLabelDocId_IsDeleted_Password
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Password = "select_Users_by_keys_view_10_2_5";

//K_C_G
//10_2_6
//WhiteLabelDocId_IsDeleted_Email
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Email = "select_Users_by_keys_view_10_2_6";

//K_C_H
//10_2_7
//WhiteLabelDocId_IsDeleted_FirstName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_FirstName = "select_Users_by_keys_view_10_2_7";

//K_C_I
//10_2_8
//WhiteLabelDocId_IsDeleted_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_LastName = "select_Users_by_keys_view_10_2_8";

//K_C_J
//10_2_9
//WhiteLabelDocId_IsDeleted_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_IsDeleted_Role = "select_Users_by_keys_view_10_2_9";

//K_E_F
//10_4_5
//WhiteLabelDocId_UserName_Password
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Password = "select_Users_by_keys_view_10_4_5";

//K_E_G
//10_4_6
//WhiteLabelDocId_UserName_Email
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Email = "select_Users_by_keys_view_10_4_6";

//K_E_H
//10_4_7
//WhiteLabelDocId_UserName_FirstName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_FirstName = "select_Users_by_keys_view_10_4_7";

//K_E_I
//10_4_8
//WhiteLabelDocId_UserName_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_LastName = "select_Users_by_keys_view_10_4_8";

//K_E_J
//10_4_9
//WhiteLabelDocId_UserName_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_UserName_Role = "select_Users_by_keys_view_10_4_9";

//K_F_G
//10_5_6
//WhiteLabelDocId_Password_Email
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_Email = "select_Users_by_keys_view_10_5_6";

//K_F_H
//10_5_7
//WhiteLabelDocId_Password_FirstName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_FirstName = "select_Users_by_keys_view_10_5_7";

//K_F_I
//10_5_8
//WhiteLabelDocId_Password_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_LastName = "select_Users_by_keys_view_10_5_8";

//K_F_J
//10_5_9
//WhiteLabelDocId_Password_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Password_Role = "select_Users_by_keys_view_10_5_9";

//K_G_H
//10_6_7
//WhiteLabelDocId_Email_FirstName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email_FirstName = "select_Users_by_keys_view_10_6_7";

//K_G_I
//10_6_8
//WhiteLabelDocId_Email_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email_LastName = "select_Users_by_keys_view_10_6_8";

//K_G_J
//10_6_9
//WhiteLabelDocId_Email_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_Email_Role = "select_Users_by_keys_view_10_6_9";

//K_H_I
//10_7_8
//WhiteLabelDocId_FirstName_LastName
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_FirstName_LastName = "select_Users_by_keys_view_10_7_8";

//K_H_J
//10_7_9
//WhiteLabelDocId_FirstName_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_FirstName_Role = "select_Users_by_keys_view_10_7_9";

//K_I_J
//10_8_9
//WhiteLabelDocId_LastName_Role
public static readonly string  PROC_Select_Users_By_Keys_View_WhiteLabelDocId_LastName_Role = "select_Users_by_keys_view_10_8_9";
         #endregion

    }

}
