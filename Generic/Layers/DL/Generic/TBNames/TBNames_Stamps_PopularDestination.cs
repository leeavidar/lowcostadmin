

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_Stamps_PopularDestination
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertStamps_PopularDestination = PROC_Prefix + "save_stamps_popular_destination";
        #endregion
        #region Select
        public static readonly string PROC_Select_Stamps_PopularDestination_By_DocId = PROC_Prefix + "Select_stamps_popular_destination_By_doc_id";        
        public static readonly string PROC_Select_Stamps_PopularDestination_By_Keys_View = PROC_Prefix + "Select_stamps_popular_destination_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteStamps_PopularDestination = PROC_Prefix + "delete_stamps_popular_destination";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationIsActive);

        public static readonly string PRM_PopularDestinationDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationDocId);

        public static readonly string PRM_StampsDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "Stamps_PopularDestination.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStamps_PopularDestinationIsActive);

        public static readonly string Field_PopularDestinationDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyPopularDestinationDocId);

        public static readonly string Field_StampsDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyStampsDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//A
//0
//DateCreated
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated = "select_Stamps_PopularDestination_by_keys_view_0";

//B
//1
//DocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId = "select_Stamps_PopularDestination_by_keys_view_1";

//C
//2
//IsDeleted
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_IsDeleted = "select_Stamps_PopularDestination_by_keys_view_2";

//E
//4
//PopularDestinationDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_PopularDestinationDocId = "select_Stamps_PopularDestination_by_keys_view_4";

//F
//5
//StampsDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_StampsDocId = "select_Stamps_PopularDestination_by_keys_view_5";

//A_B
//0_1
//DateCreated_DocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_DocId = "select_Stamps_PopularDestination_by_keys_view_0_1";

//A_C
//0_2
//DateCreated_IsDeleted
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_IsDeleted = "select_Stamps_PopularDestination_by_keys_view_0_2";

//A_E
//0_4
//DateCreated_PopularDestinationDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_PopularDestinationDocId = "select_Stamps_PopularDestination_by_keys_view_0_4";

//A_F
//0_5
//DateCreated_StampsDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DateCreated_StampsDocId = "select_Stamps_PopularDestination_by_keys_view_0_5";

//B_C
//1_2
//DocId_IsDeleted
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId_IsDeleted = "select_Stamps_PopularDestination_by_keys_view_1_2";

//B_E
//1_4
//DocId_PopularDestinationDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId_PopularDestinationDocId = "select_Stamps_PopularDestination_by_keys_view_1_4";

//B_F
//1_5
//DocId_StampsDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_DocId_StampsDocId = "select_Stamps_PopularDestination_by_keys_view_1_5";

//C_E
//2_4
//IsDeleted_PopularDestinationDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_IsDeleted_PopularDestinationDocId = "select_Stamps_PopularDestination_by_keys_view_2_4";

//C_F
//2_5
//IsDeleted_StampsDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_IsDeleted_StampsDocId = "select_Stamps_PopularDestination_by_keys_view_2_5";

//E_F
//4_5
//PopularDestinationDocId_StampsDocId
public static readonly string  PROC_Select_Stamps_PopularDestination_By_Keys_View_PopularDestinationDocId_StampsDocId = "select_Stamps_PopularDestination_by_keys_view_4_5";
         #endregion

    }

}
