

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_HomePage
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertHomePage = PROC_Prefix + "save_home_page";
        #endregion
        #region Select
        public static readonly string PROC_Select_HomePage_By_DocId = PROC_Prefix + "Select_home_page_By_doc_id";        
        public static readonly string PROC_Select_HomePage_By_Keys_View = PROC_Prefix + "Select_home_page_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteHomePage = PROC_Prefix + "delete_home_page";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageIsActive);

        public static readonly string PRM_TextLine1 = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine1);

        public static readonly string PRM_TextLine2 = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine2);

        public static readonly string PRM_TextLine3 = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine3);

        public static readonly string PRM_TextLine4 = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine4);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string PRM_SeoDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string PRM_TipDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "HomePage.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageIsActive);

        public static readonly string Field_TextLine1 = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine1);

        public static readonly string Field_TextLine2 = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine2);

        public static readonly string Field_TextLine3 = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine3);

        public static readonly string Field_TextLine4 = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyHomePageTextLine4);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);

        public static readonly string Field_SeoDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySeoDocId);

        public static readonly string Field_TipDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTipDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//I_A
//8_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated = "select_HomePage_by_keys_view_8_0";

//I_B
//8_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId = "select_HomePage_by_keys_view_8_1";

//I_C
//8_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_HomePage_by_keys_view_8_2";

//I_E
//8_4
//WhiteLabelDocId_TextLine1
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1 = "select_HomePage_by_keys_view_8_4";

//I_F
//8_5
//WhiteLabelDocId_TextLine2
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2 = "select_HomePage_by_keys_view_8_5";

//I_G
//8_6
//WhiteLabelDocId_TextLine3
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3 = "select_HomePage_by_keys_view_8_6";

//I_H
//8_7
//WhiteLabelDocId_TextLine4
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4 = "select_HomePage_by_keys_view_8_7";

//I_J
//8_9
//WhiteLabelDocId_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_SeoDocId = "select_HomePage_by_keys_view_8_9";

//I_K
//8_10
//WhiteLabelDocId_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TipDocId = "select_HomePage_by_keys_view_8_10";

//I_A_B
//8_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_HomePage_by_keys_view_8_0_1";

//I_A_C
//8_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_HomePage_by_keys_view_8_0_2";

//I_A_E
//8_0_4
//WhiteLabelDocId_DateCreated_TextLine1
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine1 = "select_HomePage_by_keys_view_8_0_4";

//I_A_F
//8_0_5
//WhiteLabelDocId_DateCreated_TextLine2
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine2 = "select_HomePage_by_keys_view_8_0_5";

//I_A_G
//8_0_6
//WhiteLabelDocId_DateCreated_TextLine3
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine3 = "select_HomePage_by_keys_view_8_0_6";

//I_A_H
//8_0_7
//WhiteLabelDocId_DateCreated_TextLine4
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TextLine4 = "select_HomePage_by_keys_view_8_0_7";

//I_A_J
//8_0_9
//WhiteLabelDocId_DateCreated_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_SeoDocId = "select_HomePage_by_keys_view_8_0_9";

//I_A_K
//8_0_10
//WhiteLabelDocId_DateCreated_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DateCreated_TipDocId = "select_HomePage_by_keys_view_8_0_10";

//I_B_C
//8_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_HomePage_by_keys_view_8_1_2";

//I_B_E
//8_1_4
//WhiteLabelDocId_DocId_TextLine1
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine1 = "select_HomePage_by_keys_view_8_1_4";

//I_B_F
//8_1_5
//WhiteLabelDocId_DocId_TextLine2
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine2 = "select_HomePage_by_keys_view_8_1_5";

//I_B_G
//8_1_6
//WhiteLabelDocId_DocId_TextLine3
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine3 = "select_HomePage_by_keys_view_8_1_6";

//I_B_H
//8_1_7
//WhiteLabelDocId_DocId_TextLine4
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TextLine4 = "select_HomePage_by_keys_view_8_1_7";

//I_B_J
//8_1_9
//WhiteLabelDocId_DocId_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_SeoDocId = "select_HomePage_by_keys_view_8_1_9";

//I_B_K
//8_1_10
//WhiteLabelDocId_DocId_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_DocId_TipDocId = "select_HomePage_by_keys_view_8_1_10";

//I_C_E
//8_2_4
//WhiteLabelDocId_IsDeleted_TextLine1
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine1 = "select_HomePage_by_keys_view_8_2_4";

//I_C_F
//8_2_5
//WhiteLabelDocId_IsDeleted_TextLine2
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine2 = "select_HomePage_by_keys_view_8_2_5";

//I_C_G
//8_2_6
//WhiteLabelDocId_IsDeleted_TextLine3
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine3 = "select_HomePage_by_keys_view_8_2_6";

//I_C_H
//8_2_7
//WhiteLabelDocId_IsDeleted_TextLine4
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TextLine4 = "select_HomePage_by_keys_view_8_2_7";

//I_C_J
//8_2_9
//WhiteLabelDocId_IsDeleted_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_SeoDocId = "select_HomePage_by_keys_view_8_2_9";

//I_C_K
//8_2_10
//WhiteLabelDocId_IsDeleted_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_IsDeleted_TipDocId = "select_HomePage_by_keys_view_8_2_10";

//I_E_F
//8_4_5
//WhiteLabelDocId_TextLine1_TextLine2
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine2 = "select_HomePage_by_keys_view_8_4_5";

//I_E_G
//8_4_6
//WhiteLabelDocId_TextLine1_TextLine3
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine3 = "select_HomePage_by_keys_view_8_4_6";

//I_E_H
//8_4_7
//WhiteLabelDocId_TextLine1_TextLine4
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TextLine4 = "select_HomePage_by_keys_view_8_4_7";

//I_E_J
//8_4_9
//WhiteLabelDocId_TextLine1_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_SeoDocId = "select_HomePage_by_keys_view_8_4_9";

//I_E_K
//8_4_10
//WhiteLabelDocId_TextLine1_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine1_TipDocId = "select_HomePage_by_keys_view_8_4_10";

//I_F_G
//8_5_6
//WhiteLabelDocId_TextLine2_TextLine3
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TextLine3 = "select_HomePage_by_keys_view_8_5_6";

//I_F_H
//8_5_7
//WhiteLabelDocId_TextLine2_TextLine4
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TextLine4 = "select_HomePage_by_keys_view_8_5_7";

//I_F_J
//8_5_9
//WhiteLabelDocId_TextLine2_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_SeoDocId = "select_HomePage_by_keys_view_8_5_9";

//I_F_K
//8_5_10
//WhiteLabelDocId_TextLine2_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine2_TipDocId = "select_HomePage_by_keys_view_8_5_10";

//I_G_H
//8_6_7
//WhiteLabelDocId_TextLine3_TextLine4
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_TextLine4 = "select_HomePage_by_keys_view_8_6_7";

//I_G_J
//8_6_9
//WhiteLabelDocId_TextLine3_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_SeoDocId = "select_HomePage_by_keys_view_8_6_9";

//I_G_K
//8_6_10
//WhiteLabelDocId_TextLine3_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine3_TipDocId = "select_HomePage_by_keys_view_8_6_10";

//I_H_J
//8_7_9
//WhiteLabelDocId_TextLine4_SeoDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4_SeoDocId = "select_HomePage_by_keys_view_8_7_9";

//I_H_K
//8_7_10
//WhiteLabelDocId_TextLine4_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_TextLine4_TipDocId = "select_HomePage_by_keys_view_8_7_10";

//I_J_K
//8_9_10
//WhiteLabelDocId_SeoDocId_TipDocId
public static readonly string  PROC_Select_HomePage_By_Keys_View_WhiteLabelDocId_SeoDocId_TipDocId = "select_HomePage_by_keys_view_8_9_10";
         #endregion

    }

}
