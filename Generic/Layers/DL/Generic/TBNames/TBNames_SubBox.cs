

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_Generic {
    using DL_LowCost;

    public static partial class TBNames_SubBox
    {

       #region PROCS

        //#REP_HERE
        public static readonly string PROC_Prefix = "dbo.";
        #region Insert/update
        //public static readonly string PROC_InsertSubBox = PROC_Prefix + "save_sub_box";
        #endregion
        #region Select
        public static readonly string PROC_Select_SubBox_By_DocId = PROC_Prefix + "Select_sub_box_By_doc_id";        
        public static readonly string PROC_Select_SubBox_By_Keys_View = PROC_Prefix + "Select_sub_box_By_Keys_View";
        #endregion
        #region Delete
        // public static readonly string PROC_DeleteSubBox = PROC_Prefix + "delete_sub_box";
        #endregion


        #endregion

       #region PARAMS TO PROCS

        public static readonly string PRM_Prefix = "@prm_";

        public static readonly string PRM_DateCreated = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxDateCreated);

        public static readonly string PRM_DocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxDocId);

        public static readonly string PRM_IsDeleted = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxIsDeleted);

        public static readonly string PRM_IsActive = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxIsActive);

        public static readonly string PRM_SubTitle = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxSubTitle);

        public static readonly string PRM_ShortText = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxShortText);

        public static readonly string PRM_LongText = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxLongText);

        public static readonly string PRM_Order = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxOrder);

        public static readonly string PRM_TemplateDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateDocId);

        public static readonly string PRM_WhiteLabelDocId = PRM_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


        #endregion

       #region PARAMS FROM PROCS

        public static readonly string Field_Prefix = "SubBox.";

        public static readonly string Field_DateCreated = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxDateCreated);

        public static readonly string Field_DocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxDocId);

        public static readonly string Field_IsDeleted = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxIsDeleted);

        public static readonly string Field_IsActive = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxIsActive);

        public static readonly string Field_SubTitle = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxSubTitle);

        public static readonly string Field_ShortText = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxShortText);

        public static readonly string Field_LongText = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxLongText);

        public static readonly string Field_Order = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeySubBoxOrder);

        public static readonly string Field_TemplateDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyTemplateDocId);

        public static readonly string Field_WhiteLabelDocId = Field_Prefix + DBKeyValurPair.GetDBFieldNameByKeyValueType(KeyValuesType.KeyWhiteLabelDocId);


      #endregion

       #region PARAMS OUT

        public static readonly string OUT_Prefix = "prm_out_doc_id";

         #endregion

       #region PARAMS Combinations

//J_A
//9_0
//WhiteLabelDocId_DateCreated
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated = "select_SubBox_by_keys_view_9_0";

//J_B
//9_1
//WhiteLabelDocId_DocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId = "select_SubBox_by_keys_view_9_1";

//J_C
//9_2
//WhiteLabelDocId_IsDeleted
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted = "select_SubBox_by_keys_view_9_2";

//J_E
//9_4
//WhiteLabelDocId_SubTitle
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle = "select_SubBox_by_keys_view_9_4";

//J_F
//9_5
//WhiteLabelDocId_ShortText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText = "select_SubBox_by_keys_view_9_5";

//J_G
//9_6
//WhiteLabelDocId_LongText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText = "select_SubBox_by_keys_view_9_6";

//J_H
//9_7
//WhiteLabelDocId_Order
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_Order = "select_SubBox_by_keys_view_9_7";

//J_I
//9_8
//WhiteLabelDocId_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_TemplateDocId = "select_SubBox_by_keys_view_9_8";

//J_A_B
//9_0_1
//WhiteLabelDocId_DateCreated_DocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_DocId = "select_SubBox_by_keys_view_9_0_1";

//J_A_C
//9_0_2
//WhiteLabelDocId_DateCreated_IsDeleted
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted = "select_SubBox_by_keys_view_9_0_2";

//J_A_E
//9_0_4
//WhiteLabelDocId_DateCreated_SubTitle
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_SubTitle = "select_SubBox_by_keys_view_9_0_4";

//J_A_F
//9_0_5
//WhiteLabelDocId_DateCreated_ShortText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_ShortText = "select_SubBox_by_keys_view_9_0_5";

//J_A_G
//9_0_6
//WhiteLabelDocId_DateCreated_LongText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_LongText = "select_SubBox_by_keys_view_9_0_6";

//J_A_H
//9_0_7
//WhiteLabelDocId_DateCreated_Order
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_Order = "select_SubBox_by_keys_view_9_0_7";

//J_A_I
//9_0_8
//WhiteLabelDocId_DateCreated_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DateCreated_TemplateDocId = "select_SubBox_by_keys_view_9_0_8";

//J_B_C
//9_1_2
//WhiteLabelDocId_DocId_IsDeleted
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted = "select_SubBox_by_keys_view_9_1_2";

//J_B_E
//9_1_4
//WhiteLabelDocId_DocId_SubTitle
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_SubTitle = "select_SubBox_by_keys_view_9_1_4";

//J_B_F
//9_1_5
//WhiteLabelDocId_DocId_ShortText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_ShortText = "select_SubBox_by_keys_view_9_1_5";

//J_B_G
//9_1_6
//WhiteLabelDocId_DocId_LongText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_LongText = "select_SubBox_by_keys_view_9_1_6";

//J_B_H
//9_1_7
//WhiteLabelDocId_DocId_Order
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_Order = "select_SubBox_by_keys_view_9_1_7";

//J_B_I
//9_1_8
//WhiteLabelDocId_DocId_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_DocId_TemplateDocId = "select_SubBox_by_keys_view_9_1_8";

//J_C_E
//9_2_4
//WhiteLabelDocId_IsDeleted_SubTitle
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_SubTitle = "select_SubBox_by_keys_view_9_2_4";

//J_C_F
//9_2_5
//WhiteLabelDocId_IsDeleted_ShortText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_ShortText = "select_SubBox_by_keys_view_9_2_5";

//J_C_G
//9_2_6
//WhiteLabelDocId_IsDeleted_LongText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_LongText = "select_SubBox_by_keys_view_9_2_6";

//J_C_H
//9_2_7
//WhiteLabelDocId_IsDeleted_Order
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_Order = "select_SubBox_by_keys_view_9_2_7";

//J_C_I
//9_2_8
//WhiteLabelDocId_IsDeleted_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_IsDeleted_TemplateDocId = "select_SubBox_by_keys_view_9_2_8";

//J_E_F
//9_4_5
//WhiteLabelDocId_SubTitle_ShortText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_ShortText = "select_SubBox_by_keys_view_9_4_5";

//J_E_G
//9_4_6
//WhiteLabelDocId_SubTitle_LongText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_LongText = "select_SubBox_by_keys_view_9_4_6";

//J_E_H
//9_4_7
//WhiteLabelDocId_SubTitle_Order
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_Order = "select_SubBox_by_keys_view_9_4_7";

//J_E_I
//9_4_8
//WhiteLabelDocId_SubTitle_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_SubTitle_TemplateDocId = "select_SubBox_by_keys_view_9_4_8";

//J_F_G
//9_5_6
//WhiteLabelDocId_ShortText_LongText
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_LongText = "select_SubBox_by_keys_view_9_5_6";

//J_F_H
//9_5_7
//WhiteLabelDocId_ShortText_Order
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_Order = "select_SubBox_by_keys_view_9_5_7";

//J_F_I
//9_5_8
//WhiteLabelDocId_ShortText_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_ShortText_TemplateDocId = "select_SubBox_by_keys_view_9_5_8";

//J_G_H
//9_6_7
//WhiteLabelDocId_LongText_Order
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText_Order = "select_SubBox_by_keys_view_9_6_7";

//J_G_I
//9_6_8
//WhiteLabelDocId_LongText_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_LongText_TemplateDocId = "select_SubBox_by_keys_view_9_6_8";

//J_H_I
//9_7_8
//WhiteLabelDocId_Order_TemplateDocId
public static readonly string  PROC_Select_SubBox_By_Keys_View_WhiteLabelDocId_Order_TemplateDocId = "select_SubBox_by_keys_view_9_7_8";
         #endregion

    }

}
