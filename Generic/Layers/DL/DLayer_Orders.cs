

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DL_Generic;

namespace DL_LowCost{
    using BL_LowCost;

public partial class Orders  : ContainerItem<Orders>{

#region CTOR

    #region Constractor
    static Orders()
    {
        ConvertEvent = Orders.OnConvert;
    }  
    //public KeyValuesContainer<Orders> KeyValuesContainerF { get { return KeyValuesContainer; } set { KeyValuesContainer = value; } }
    public Orders()
    {
            KeyValuesContainer = FactoryKeyValuesContainer.OrdersKeys();
            
            
this.DateCreated = DateTime.Now;
this.IsActive = true;
this.IsDeleted = false;

    }
    #endregion
    #region DbAction
    public override int Action(DB_Actions dB_Actions)
    {
        int iRefID;
        switch (dB_Actions)
        {
            case DB_Actions.Insert:
                //Defult Value For Insert
                this.IsActive = true;
                this.IsDeleted = false;
                int? ID;
                iRefID = this.Insert(out ID);
                if (iRefID > 0)
                {
                    this.DocId = ID;
                }
                return iRefID;
            case DB_Actions.Delete:
                this.IsDeleted = true;
                this.IsActive = false;
                return this.Update();
            case DB_Actions.Update:
                return this.Update();
            case DB_Actions.Activate:
                this.IsActive = true;
                this.IsDeleted = false;
                return this.Update();
            case DB_Actions.Expunge:
                return this.Expunge(this.DocId);
            case DB_Actions.Deactivate:
                this.IsActive = false;
                return this.Update();
        }
        return -1;
    }

    #endregion
    
    public static Orders OnConvert(DataRow dr)
    {
        int LangId = Translator<Orders>.LangId;            
        Orders oOrders = null;
        if (dr != null)
        {
            oOrders = new Orders();
            #region Create Object
            oOrders.DateCreated = ConvertTo.ConvertToDateTime(dr[TBNames_Orders.Field_DateCreated]);
             oOrders.DocId = ConvertTo.ConvertToInt(dr[TBNames_Orders.Field_DocId]);
             oOrders.IsDeleted = ConvertTo.ConvertToBool(dr[TBNames_Orders.Field_IsDeleted]);
             oOrders.IsActive = ConvertTo.ConvertToBool(dr[TBNames_Orders.Field_IsActive]);
             oOrders.LowCostPNR = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_LowCostPNR]);
             oOrders.SupplierPNR = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_SupplierPNR]);
             oOrders.TotalPrice = ConvertTo.ConvertToDouble(dr[TBNames_Orders.Field_TotalPrice]);
             oOrders.DateOfBook = ConvertTo.ConvertToDateTime(dr[TBNames_Orders.Field_DateOfBook]);
             oOrders.ReceiveEmail = ConvertTo.ConvertToBool(dr[TBNames_Orders.Field_ReceiveEmail]);
             oOrders.ReceiveSms = ConvertTo.ConvertToBool(dr[TBNames_Orders.Field_ReceiveSms]);
             oOrders.MarketingCabin = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_MarketingCabin]);
 
//FK     KeyContactDetailsDocId
            oOrders.ContactDetailsDocId = ConvertTo.ConvertToInt(dr[TBNames_Orders.Field_ContactDetailsDocId]);
 
//FK     KeyWhiteLabelDocId
            oOrders.WhiteLabelDocId = ConvertTo.ConvertToInt(dr[TBNames_Orders.Field_WhiteLabelDocId]);
             oOrders.AirlineIataCode = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_AirlineIataCode]);
             oOrders.OrderStatus = ConvertTo.ConvertToInt(dr[TBNames_Orders.Field_OrderStatus]);
             oOrders.AdminRemarks = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_AdminRemarks]);
             oOrders.StatusPayment = ConvertTo.ConvertToInt(dr[TBNames_Orders.Field_StatusPayment]);
             oOrders.OrderToken = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_OrderToken]);
             oOrders.PaymentTransaction = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_PaymentTransaction]);
             oOrders.XmlBookRequest = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_XmlBookRequest]);
             oOrders.XmlBookResponse = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_XmlBookResponse]);
             oOrders.OrderRemarks = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_OrderRemarks]);
             oOrders.OrderPayment = ConvertTo.ConvertToInt(dr[TBNames_Orders.Field_OrderPayment]);
             oOrders.PaymentToken = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_PaymentToken]);
             oOrders.PaymentRemarks = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_PaymentRemarks]);
             oOrders.Currency = ConvertTo.ConvertToString(dr[TBNames_Orders.Field_Currency]);
 
            #endregion
            Translator<Orders>.Translate(oOrders.KeyValuesContainer.MirrorKeys, LangId);
        }
        return oOrders;
    }

    
#endregion

//#REP_HERE 
#region Orders Properties 

private bool isSetOnce_DateCreated;
private DateTime _date_created;
public String FriendlyDateCreated
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDateCreated);   
    }
}
public  DateTime? DateCreated
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyOrdersDateCreated));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDateCreated, value);
         _date_created = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateCreated = true;
    }
}


public DateTime DateCreated_Value
{
    get
    {
        //return _date_created; //ConvertToValue.ConvertToDateTime(DateCreated);
        if(isSetOnce_DateCreated) {return _date_created;}
        else {return ConvertToValue.ConvertToDateTime(DateCreated);}
    }
}

public string DateCreated_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();
               //if(isSetOnce_DateCreated) {return ConvertToValue.ConvertToDateTime(_date_created).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateCreated).ToShortDateString();
    }
}

private bool isSetOnce_DocId;

public String FriendlyDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDocId);   
    }
}
public  int? DocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDocId, value);
         _doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_DocId = true;
    }
}


public int DocId_Value
{
    get
    {
        //return _doc_id; //ConvertToValue.ConvertToInt(DocId);
        if(isSetOnce_DocId) {return _doc_id;}
        else {return ConvertToValue.ConvertToInt(DocId);}
    }
}

public string DocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_doc_id).ToString();
               //if(isSetOnce_DocId) {return ConvertToValue.ConvertToInt(_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(DocId).ToString();}
            ConvertToValue.ConvertToInt(DocId).ToString();
    }
}

private bool isSetOnce_IsDeleted;

public String FriendlyIsDeleted
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersIsDeleted);   
    }
}
public  bool? IsDeleted
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrdersIsDeleted));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersIsDeleted, value);
         _is_deleted = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsDeleted = true;
    }
}


public bool IsDeleted_Value
{
    get
    {
        //return _is_deleted; //ConvertToValue.ConvertToBool(IsDeleted);
        if(isSetOnce_IsDeleted) {return _is_deleted;}
        else {return ConvertToValue.ConvertToBool(IsDeleted);}
    }
}

public string IsDeleted_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_deleted).ToString();
               //if(isSetOnce_IsDeleted) {return ConvertToValue.ConvertToBool(_is_deleted).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsDeleted).ToString();}
            ConvertToValue.ConvertToBool(IsDeleted).ToString();
    }
}

private bool isSetOnce_IsActive;

public String FriendlyIsActive
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersIsActive);   
    }
}
public  bool? IsActive
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrdersIsActive));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersIsActive, value);
         _is_active = ConvertToValue.ConvertToBool(value);
        isSetOnce_IsActive = true;
    }
}


public bool IsActive_Value
{
    get
    {
        //return _is_active; //ConvertToValue.ConvertToBool(IsActive);
        if(isSetOnce_IsActive) {return _is_active;}
        else {return ConvertToValue.ConvertToBool(IsActive);}
    }
}

public string IsActive_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_is_active).ToString();
               //if(isSetOnce_IsActive) {return ConvertToValue.ConvertToBool(_is_active).ToString();}
               //else {return ConvertToValue.ConvertToBool(IsActive).ToString();}
            ConvertToValue.ConvertToBool(IsActive).ToString();
    }
}

private bool isSetOnce_LowCostPNR;
private string _low_cost_pnr;
public String FriendlyLowCostPNR
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersLowCostPNR);   
    }
}
public  string LowCostPNR
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersLowCostPNR));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersLowCostPNR, value);
         _low_cost_pnr = ConvertToValue.ConvertToString(value);
        isSetOnce_LowCostPNR = true;
    }
}


public string LowCostPNR_Value
{
    get
    {
        //return _low_cost_pnr; //ConvertToValue.ConvertToString(LowCostPNR);
        if(isSetOnce_LowCostPNR) {return _low_cost_pnr;}
        else {return ConvertToValue.ConvertToString(LowCostPNR);}
    }
}

public string LowCostPNR_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_low_cost_pnr).ToString();
               //if(isSetOnce_LowCostPNR) {return ConvertToValue.ConvertToString(_low_cost_pnr).ToString();}
               //else {return ConvertToValue.ConvertToString(LowCostPNR).ToString();}
            ConvertToValue.ConvertToString(LowCostPNR).ToString();
    }
}

private bool isSetOnce_SupplierPNR;
private string _supplier_pnr;
public String FriendlySupplierPNR
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersSupplierPNR);   
    }
}
public  string SupplierPNR
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersSupplierPNR));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersSupplierPNR, value);
         _supplier_pnr = ConvertToValue.ConvertToString(value);
        isSetOnce_SupplierPNR = true;
    }
}


public string SupplierPNR_Value
{
    get
    {
        //return _supplier_pnr; //ConvertToValue.ConvertToString(SupplierPNR);
        if(isSetOnce_SupplierPNR) {return _supplier_pnr;}
        else {return ConvertToValue.ConvertToString(SupplierPNR);}
    }
}

public string SupplierPNR_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_supplier_pnr).ToString();
               //if(isSetOnce_SupplierPNR) {return ConvertToValue.ConvertToString(_supplier_pnr).ToString();}
               //else {return ConvertToValue.ConvertToString(SupplierPNR).ToString();}
            ConvertToValue.ConvertToString(SupplierPNR).ToString();
    }
}

private bool isSetOnce_TotalPrice;
private Double _total_price;
public String FriendlyTotalPrice
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersTotalPrice);   
    }
}
public  Double? TotalPrice
{
    get
    {
        return ConvertTo.ConvertToDouble(GetKey(KeyValuesType.KeyOrdersTotalPrice));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersTotalPrice, value);
         _total_price = ConvertToValue.ConvertToDouble(value);
        isSetOnce_TotalPrice = true;
    }
}


public Double TotalPrice_Value
{
    get
    {
        //return _total_price; //ConvertToValue.ConvertToDouble(TotalPrice);
        if(isSetOnce_TotalPrice) {return _total_price;}
        else {return ConvertToValue.ConvertToDouble(TotalPrice);}
    }
}

public string TotalPrice_UI
{
    get
    {
        return //ConvertToValue.ConvertToDouble(_total_price).ToString();
               //if(isSetOnce_TotalPrice) {return ConvertToValue.ConvertToDouble(_total_price).ToString();}
               //else {return ConvertToValue.ConvertToDouble(TotalPrice).ToString();}
            ConvertToValue.ConvertToDouble(TotalPrice).ToString();
    }
}

private bool isSetOnce_DateOfBook;
private DateTime _date_of_book;
public String FriendlyDateOfBook
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersDateOfBook);   
    }
}
public  DateTime? DateOfBook
{
    get
    {
        return ConvertTo.ConvertToDateTime(GetKey(KeyValuesType.KeyOrdersDateOfBook));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersDateOfBook, value);
         _date_of_book = ConvertToValue.ConvertToDateTime(value);
        isSetOnce_DateOfBook = true;
    }
}


public DateTime DateOfBook_Value
{
    get
    {
        //return _date_of_book; //ConvertToValue.ConvertToDateTime(DateOfBook);
        if(isSetOnce_DateOfBook) {return _date_of_book;}
        else {return ConvertToValue.ConvertToDateTime(DateOfBook);}
    }
}

public string DateOfBook_UI
{
    get
    {
        return //ConvertToValue.ConvertToDateTime(_date_of_book).ToShortDateString();
               //if(isSetOnce_DateOfBook) {return ConvertToValue.ConvertToDateTime(_date_of_book).ToShortDateString();}
               //else {return ConvertToValue.ConvertToDateTime(DateOfBook).ToShortDateString();}
            ConvertToValue.ConvertToDateTime(DateOfBook).ToShortDateString();
    }
}

private bool isSetOnce_ReceiveEmail;
private bool _receive_email;
public String FriendlyReceiveEmail
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersReceiveEmail);   
    }
}
public  bool? ReceiveEmail
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrdersReceiveEmail));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersReceiveEmail, value);
         _receive_email = ConvertToValue.ConvertToBool(value);
        isSetOnce_ReceiveEmail = true;
    }
}


public bool ReceiveEmail_Value
{
    get
    {
        //return _receive_email; //ConvertToValue.ConvertToBool(ReceiveEmail);
        if(isSetOnce_ReceiveEmail) {return _receive_email;}
        else {return ConvertToValue.ConvertToBool(ReceiveEmail);}
    }
}

public string ReceiveEmail_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_receive_email).ToString();
               //if(isSetOnce_ReceiveEmail) {return ConvertToValue.ConvertToBool(_receive_email).ToString();}
               //else {return ConvertToValue.ConvertToBool(ReceiveEmail).ToString();}
            ConvertToValue.ConvertToBool(ReceiveEmail).ToString();
    }
}

private bool isSetOnce_ReceiveSms;
private bool _receive_sms;
public String FriendlyReceiveSms
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersReceiveSms);   
    }
}
public  bool? ReceiveSms
{
    get
    {
        return ConvertTo.ConvertToBool(GetKey(KeyValuesType.KeyOrdersReceiveSms));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersReceiveSms, value);
         _receive_sms = ConvertToValue.ConvertToBool(value);
        isSetOnce_ReceiveSms = true;
    }
}


public bool ReceiveSms_Value
{
    get
    {
        //return _receive_sms; //ConvertToValue.ConvertToBool(ReceiveSms);
        if(isSetOnce_ReceiveSms) {return _receive_sms;}
        else {return ConvertToValue.ConvertToBool(ReceiveSms);}
    }
}

public string ReceiveSms_UI
{
    get
    {
        return //ConvertToValue.ConvertToBool(_receive_sms).ToString();
               //if(isSetOnce_ReceiveSms) {return ConvertToValue.ConvertToBool(_receive_sms).ToString();}
               //else {return ConvertToValue.ConvertToBool(ReceiveSms).ToString();}
            ConvertToValue.ConvertToBool(ReceiveSms).ToString();
    }
}

private bool isSetOnce_MarketingCabin;
private string _marketing_cabin;
public String FriendlyMarketingCabin
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersMarketingCabin);   
    }
}
public  string MarketingCabin
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersMarketingCabin));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersMarketingCabin, value);
         _marketing_cabin = ConvertToValue.ConvertToString(value);
        isSetOnce_MarketingCabin = true;
    }
}


public string MarketingCabin_Value
{
    get
    {
        //return _marketing_cabin; //ConvertToValue.ConvertToString(MarketingCabin);
        if(isSetOnce_MarketingCabin) {return _marketing_cabin;}
        else {return ConvertToValue.ConvertToString(MarketingCabin);}
    }
}

public string MarketingCabin_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_marketing_cabin).ToString();
               //if(isSetOnce_MarketingCabin) {return ConvertToValue.ConvertToString(_marketing_cabin).ToString();}
               //else {return ConvertToValue.ConvertToString(MarketingCabin).ToString();}
            ConvertToValue.ConvertToString(MarketingCabin).ToString();
    }
}

private bool isSetOnce_ContactDetailsDocId;
private int _contact_details_doc_id;
public String FriendlyContactDetailsDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyContactDetailsDocId);   
    }
}
public  int? ContactDetailsDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyContactDetailsDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyContactDetailsDocId, value);
         _contact_details_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_ContactDetailsDocId = true;
    }
}


public int ContactDetailsDocId_Value
{
    get
    {
        //return _contact_details_doc_id; //ConvertToValue.ConvertToInt(ContactDetailsDocId);
        if(isSetOnce_ContactDetailsDocId) {return _contact_details_doc_id;}
        else {return ConvertToValue.ConvertToInt(ContactDetailsDocId);}
    }
}

public string ContactDetailsDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_contact_details_doc_id).ToString();
               //if(isSetOnce_ContactDetailsDocId) {return ConvertToValue.ConvertToInt(_contact_details_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(ContactDetailsDocId).ToString();}
            ConvertToValue.ConvertToInt(ContactDetailsDocId).ToString();
    }
}

private bool isSetOnce_WhiteLabelDocId;
private int _white_label_doc_id;
public String FriendlyWhiteLabelDocId
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyWhiteLabelDocId);   
    }
}
public  int? WhiteLabelDocId
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyWhiteLabelDocId));
    }
    set
    {
        SetKey(KeyValuesType.KeyWhiteLabelDocId, value);
         _white_label_doc_id = ConvertToValue.ConvertToInt(value);
        isSetOnce_WhiteLabelDocId = true;
    }
}


public int WhiteLabelDocId_Value
{
    get
    {
        //return _white_label_doc_id; //ConvertToValue.ConvertToInt(WhiteLabelDocId);
        if(isSetOnce_WhiteLabelDocId) {return _white_label_doc_id;}
        else {return ConvertToValue.ConvertToInt(WhiteLabelDocId);}
    }
}

public string WhiteLabelDocId_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();
               //if(isSetOnce_WhiteLabelDocId) {return ConvertToValue.ConvertToInt(_white_label_doc_id).ToString();}
               //else {return ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();}
            ConvertToValue.ConvertToInt(WhiteLabelDocId).ToString();
    }
}

private bool isSetOnce_AirlineIataCode;
private string _airline_iata_code;
public String FriendlyAirlineIataCode
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersAirlineIataCode);   
    }
}
public  string AirlineIataCode
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersAirlineIataCode));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersAirlineIataCode, value);
         _airline_iata_code = ConvertToValue.ConvertToString(value);
        isSetOnce_AirlineIataCode = true;
    }
}


public string AirlineIataCode_Value
{
    get
    {
        //return _airline_iata_code; //ConvertToValue.ConvertToString(AirlineIataCode);
        if(isSetOnce_AirlineIataCode) {return _airline_iata_code;}
        else {return ConvertToValue.ConvertToString(AirlineIataCode);}
    }
}

public string AirlineIataCode_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_airline_iata_code).ToString();
               //if(isSetOnce_AirlineIataCode) {return ConvertToValue.ConvertToString(_airline_iata_code).ToString();}
               //else {return ConvertToValue.ConvertToString(AirlineIataCode).ToString();}
            ConvertToValue.ConvertToString(AirlineIataCode).ToString();
    }
}

private bool isSetOnce_OrderStatus;
private int _order_status;
public String FriendlyOrderStatus
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersOrderStatus);   
    }
}
public  int? OrderStatus
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersOrderStatus));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersOrderStatus, value);
         _order_status = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrderStatus = true;
    }
}


public int OrderStatus_Value
{
    get
    {
        //return _order_status; //ConvertToValue.ConvertToInt(OrderStatus);
        if(isSetOnce_OrderStatus) {return _order_status;}
        else {return ConvertToValue.ConvertToInt(OrderStatus);}
    }
}

public string OrderStatus_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order_status).ToString();
               //if(isSetOnce_OrderStatus) {return ConvertToValue.ConvertToInt(_order_status).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrderStatus).ToString();}
            ConvertToValue.ConvertToInt(OrderStatus).ToString();
    }
}

private bool isSetOnce_AdminRemarks;
private string _admin_remarks;
public String FriendlyAdminRemarks
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersAdminRemarks);   
    }
}
public  string AdminRemarks
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersAdminRemarks));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersAdminRemarks, value);
         _admin_remarks = ConvertToValue.ConvertToString(value);
        isSetOnce_AdminRemarks = true;
    }
}


public string AdminRemarks_Value
{
    get
    {
        //return _admin_remarks; //ConvertToValue.ConvertToString(AdminRemarks);
        if(isSetOnce_AdminRemarks) {return _admin_remarks;}
        else {return ConvertToValue.ConvertToString(AdminRemarks);}
    }
}

public string AdminRemarks_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_admin_remarks).ToString();
               //if(isSetOnce_AdminRemarks) {return ConvertToValue.ConvertToString(_admin_remarks).ToString();}
               //else {return ConvertToValue.ConvertToString(AdminRemarks).ToString();}
            ConvertToValue.ConvertToString(AdminRemarks).ToString();
    }
}

private bool isSetOnce_StatusPayment;
private int _status_payment;
public String FriendlyStatusPayment
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersStatusPayment);   
    }
}
public  int? StatusPayment
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersStatusPayment));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersStatusPayment, value);
         _status_payment = ConvertToValue.ConvertToInt(value);
        isSetOnce_StatusPayment = true;
    }
}


public int StatusPayment_Value
{
    get
    {
        //return _status_payment; //ConvertToValue.ConvertToInt(StatusPayment);
        if(isSetOnce_StatusPayment) {return _status_payment;}
        else {return ConvertToValue.ConvertToInt(StatusPayment);}
    }
}

public string StatusPayment_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_status_payment).ToString();
               //if(isSetOnce_StatusPayment) {return ConvertToValue.ConvertToInt(_status_payment).ToString();}
               //else {return ConvertToValue.ConvertToInt(StatusPayment).ToString();}
            ConvertToValue.ConvertToInt(StatusPayment).ToString();
    }
}

private bool isSetOnce_OrderToken;
private string _order_token;
public String FriendlyOrderToken
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersOrderToken);   
    }
}
public  string OrderToken
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersOrderToken));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersOrderToken, value);
         _order_token = ConvertToValue.ConvertToString(value);
        isSetOnce_OrderToken = true;
    }
}


public string OrderToken_Value
{
    get
    {
        //return _order_token; //ConvertToValue.ConvertToString(OrderToken);
        if(isSetOnce_OrderToken) {return _order_token;}
        else {return ConvertToValue.ConvertToString(OrderToken);}
    }
}

public string OrderToken_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_order_token).ToString();
               //if(isSetOnce_OrderToken) {return ConvertToValue.ConvertToString(_order_token).ToString();}
               //else {return ConvertToValue.ConvertToString(OrderToken).ToString();}
            ConvertToValue.ConvertToString(OrderToken).ToString();
    }
}

private bool isSetOnce_PaymentTransaction;
private string _payment_transaction;
public String FriendlyPaymentTransaction
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersPaymentTransaction);   
    }
}
public  string PaymentTransaction
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersPaymentTransaction));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersPaymentTransaction, value);
         _payment_transaction = ConvertToValue.ConvertToString(value);
        isSetOnce_PaymentTransaction = true;
    }
}


public string PaymentTransaction_Value
{
    get
    {
        //return _payment_transaction; //ConvertToValue.ConvertToString(PaymentTransaction);
        if(isSetOnce_PaymentTransaction) {return _payment_transaction;}
        else {return ConvertToValue.ConvertToString(PaymentTransaction);}
    }
}

public string PaymentTransaction_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_payment_transaction).ToString();
               //if(isSetOnce_PaymentTransaction) {return ConvertToValue.ConvertToString(_payment_transaction).ToString();}
               //else {return ConvertToValue.ConvertToString(PaymentTransaction).ToString();}
            ConvertToValue.ConvertToString(PaymentTransaction).ToString();
    }
}

private bool isSetOnce_XmlBookRequest;
private string _xml_book_request;
public String FriendlyXmlBookRequest
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersXmlBookRequest);   
    }
}
public  string XmlBookRequest
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersXmlBookRequest));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersXmlBookRequest, value);
         _xml_book_request = ConvertToValue.ConvertToString(value);
        isSetOnce_XmlBookRequest = true;
    }
}


public string XmlBookRequest_Value
{
    get
    {
        //return _xml_book_request; //ConvertToValue.ConvertToString(XmlBookRequest);
        if(isSetOnce_XmlBookRequest) {return _xml_book_request;}
        else {return ConvertToValue.ConvertToString(XmlBookRequest);}
    }
}

public string XmlBookRequest_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_xml_book_request).ToString();
               //if(isSetOnce_XmlBookRequest) {return ConvertToValue.ConvertToString(_xml_book_request).ToString();}
               //else {return ConvertToValue.ConvertToString(XmlBookRequest).ToString();}
            ConvertToValue.ConvertToString(XmlBookRequest).ToString();
    }
}

private bool isSetOnce_XmlBookResponse;
private string _xml_book_response;
public String FriendlyXmlBookResponse
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersXmlBookResponse);   
    }
}
public  string XmlBookResponse
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersXmlBookResponse));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersXmlBookResponse, value);
         _xml_book_response = ConvertToValue.ConvertToString(value);
        isSetOnce_XmlBookResponse = true;
    }
}


public string XmlBookResponse_Value
{
    get
    {
        //return _xml_book_response; //ConvertToValue.ConvertToString(XmlBookResponse);
        if(isSetOnce_XmlBookResponse) {return _xml_book_response;}
        else {return ConvertToValue.ConvertToString(XmlBookResponse);}
    }
}

public string XmlBookResponse_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_xml_book_response).ToString();
               //if(isSetOnce_XmlBookResponse) {return ConvertToValue.ConvertToString(_xml_book_response).ToString();}
               //else {return ConvertToValue.ConvertToString(XmlBookResponse).ToString();}
            ConvertToValue.ConvertToString(XmlBookResponse).ToString();
    }
}

private bool isSetOnce_OrderRemarks;
private string _order_remarks;
public String FriendlyOrderRemarks
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersOrderRemarks);   
    }
}
public  string OrderRemarks
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersOrderRemarks));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersOrderRemarks, value);
         _order_remarks = ConvertToValue.ConvertToString(value);
        isSetOnce_OrderRemarks = true;
    }
}


public string OrderRemarks_Value
{
    get
    {
        //return _order_remarks; //ConvertToValue.ConvertToString(OrderRemarks);
        if(isSetOnce_OrderRemarks) {return _order_remarks;}
        else {return ConvertToValue.ConvertToString(OrderRemarks);}
    }
}

public string OrderRemarks_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_order_remarks).ToString();
               //if(isSetOnce_OrderRemarks) {return ConvertToValue.ConvertToString(_order_remarks).ToString();}
               //else {return ConvertToValue.ConvertToString(OrderRemarks).ToString();}
            ConvertToValue.ConvertToString(OrderRemarks).ToString();
    }
}

private bool isSetOnce_OrderPayment;
private int _order_payment;
public String FriendlyOrderPayment
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersOrderPayment);   
    }
}
public  int? OrderPayment
{
    get
    {
        return ConvertTo.ConvertToInt(GetKey(KeyValuesType.KeyOrdersOrderPayment));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersOrderPayment, value);
         _order_payment = ConvertToValue.ConvertToInt(value);
        isSetOnce_OrderPayment = true;
    }
}


public int OrderPayment_Value
{
    get
    {
        //return _order_payment; //ConvertToValue.ConvertToInt(OrderPayment);
        if(isSetOnce_OrderPayment) {return _order_payment;}
        else {return ConvertToValue.ConvertToInt(OrderPayment);}
    }
}

public string OrderPayment_UI
{
    get
    {
        return //ConvertToValue.ConvertToInt(_order_payment).ToString();
               //if(isSetOnce_OrderPayment) {return ConvertToValue.ConvertToInt(_order_payment).ToString();}
               //else {return ConvertToValue.ConvertToInt(OrderPayment).ToString();}
            ConvertToValue.ConvertToInt(OrderPayment).ToString();
    }
}

private bool isSetOnce_PaymentToken;
private string _payment_token;
public String FriendlyPaymentToken
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersPaymentToken);   
    }
}
public  string PaymentToken
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersPaymentToken));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersPaymentToken, value);
         _payment_token = ConvertToValue.ConvertToString(value);
        isSetOnce_PaymentToken = true;
    }
}


public string PaymentToken_Value
{
    get
    {
        //return _payment_token; //ConvertToValue.ConvertToString(PaymentToken);
        if(isSetOnce_PaymentToken) {return _payment_token;}
        else {return ConvertToValue.ConvertToString(PaymentToken);}
    }
}

public string PaymentToken_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_payment_token).ToString();
               //if(isSetOnce_PaymentToken) {return ConvertToValue.ConvertToString(_payment_token).ToString();}
               //else {return ConvertToValue.ConvertToString(PaymentToken).ToString();}
            ConvertToValue.ConvertToString(PaymentToken).ToString();
    }
}

private bool isSetOnce_PaymentRemarks;
private string _payment_remarks;
public String FriendlyPaymentRemarks
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersPaymentRemarks);   
    }
}
public  string PaymentRemarks
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersPaymentRemarks));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersPaymentRemarks, value);
         _payment_remarks = ConvertToValue.ConvertToString(value);
        isSetOnce_PaymentRemarks = true;
    }
}


public string PaymentRemarks_Value
{
    get
    {
        //return _payment_remarks; //ConvertToValue.ConvertToString(PaymentRemarks);
        if(isSetOnce_PaymentRemarks) {return _payment_remarks;}
        else {return ConvertToValue.ConvertToString(PaymentRemarks);}
    }
}

public string PaymentRemarks_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_payment_remarks).ToString();
               //if(isSetOnce_PaymentRemarks) {return ConvertToValue.ConvertToString(_payment_remarks).ToString();}
               //else {return ConvertToValue.ConvertToString(PaymentRemarks).ToString();}
            ConvertToValue.ConvertToString(PaymentRemarks).ToString();
    }
}

private bool isSetOnce_Currency;
private string _currency;
public String FriendlyCurrency
{
    get{
        return FriendlyNames.GetFriendlyName(KeyValuesType.KeyOrdersCurrency);   
    }
}
public  string Currency
{
    get
    {
        return ConvertTo.ConvertToString(GetKey(KeyValuesType.KeyOrdersCurrency));
    }
    set
    {
        SetKey(KeyValuesType.KeyOrdersCurrency, value);
         _currency = ConvertToValue.ConvertToString(value);
        isSetOnce_Currency = true;
    }
}


public string Currency_Value
{
    get
    {
        //return _currency; //ConvertToValue.ConvertToString(Currency);
        if(isSetOnce_Currency) {return _currency;}
        else {return ConvertToValue.ConvertToString(Currency);}
    }
}

public string Currency_UI
{
    get
    {
        return //ConvertToValue.ConvertToString(_currency).ToString();
               //if(isSetOnce_Currency) {return ConvertToValue.ConvertToString(_currency).ToString();}
               //else {return ConvertToValue.ConvertToString(Currency).ToString();}
            ConvertToValue.ConvertToString(Currency).ToString();
    }
}

#endregion

#region Basic functions

#endregion

#region Combinations functions

        //M_A
        //12_0
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated", ex.Message));

            }
            return paramsSelect;
        }



        //M_B
        //12_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_C
        //12_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //M_E
        //12_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_F
        //12_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_G
        //12_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice", ex.Message));

            }
            return paramsSelect;
        }



        //M_H
        //12_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook", ex.Message));

            }
            return paramsSelect;
        }



        //M_I
        //12_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_J
        //12_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_K
        //12_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_L
        //12_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_N
        //12_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_O
        //12_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_P
        //12_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q
        //12_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_R
        //12_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_S
        //12_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_T
        //12_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_U
        //12_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_V
        //12_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_W
        //12_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_X
        //12_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_Y
        //12_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_Z
        //12_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_B
        //12_0_1
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_DocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_C
        //12_0_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_IsDeleted(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_E
        //12_0_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_LowCostPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_F
        //12_0_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_SupplierPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_SupplierPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_G
        //12_0_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_TotalPrice(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_TotalPrice", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_H
        //12_0_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_DateOfBook(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_DateOfBook", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_I
        //12_0_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_J
        //12_0_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_K
        //12_0_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_L
        //12_0_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_N
        //12_0_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_O
        //12_0_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_P
        //12_0_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_Q
        //12_0_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_R
        //12_0_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_S
        //12_0_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_T
        //12_0_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_U
        //12_0_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_V
        //12_0_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_W
        //12_0_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_X
        //12_0_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_Y
        //12_0_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_A_Z
        //12_0_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateCreated_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateCreated, ConvertTo.ConvertEmptyToDBNull(oOrders.DateCreated)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateCreated_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_C
        //12_1_2
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_IsDeleted(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_IsDeleted", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_E
        //12_1_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_LowCostPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_F
        //12_1_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_SupplierPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_SupplierPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_G
        //12_1_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_TotalPrice(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_TotalPrice", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_H
        //12_1_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_DateOfBook(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_DateOfBook", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_I
        //12_1_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_J
        //12_1_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_K
        //12_1_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_L
        //12_1_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_N
        //12_1_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_O
        //12_1_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_P
        //12_1_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_Q
        //12_1_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_R
        //12_1_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_S
        //12_1_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_T
        //12_1_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_U
        //12_1_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_V
        //12_1_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_W
        //12_1_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_X
        //12_1_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_Y
        //12_1_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_B_Z
        //12_1_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DocId_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DocId, ConvertTo.ConvertEmptyToDBNull(oOrders.DocId)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DocId_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_E
        //12_2_4
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_LowCostPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_LowCostPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_F
        //12_2_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_SupplierPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_SupplierPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_G
        //12_2_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_TotalPrice(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_TotalPrice", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_H
        //12_2_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_DateOfBook(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_DateOfBook", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_I
        //12_2_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_J
        //12_2_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_K
        //12_2_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_L
        //12_2_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_N
        //12_2_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_O
        //12_2_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_P
        //12_2_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_Q
        //12_2_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_R
        //12_2_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_S
        //12_2_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_T
        //12_2_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_U
        //12_2_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_V
        //12_2_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_W
        //12_2_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_X
        //12_2_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_Y
        //12_2_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_C_Z
        //12_2_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_IsDeleted_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_IsDeleted, ConvertTo.ConvertEmptyToDBNull(oOrders.IsDeleted)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_IsDeleted_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_F
        //12_4_5
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_SupplierPNR(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_SupplierPNR", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_G
        //12_4_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_TotalPrice(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_TotalPrice", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_H
        //12_4_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_DateOfBook(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_DateOfBook", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_I
        //12_4_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_J
        //12_4_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_K
        //12_4_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_L
        //12_4_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_N
        //12_4_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_O
        //12_4_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_P
        //12_4_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_Q
        //12_4_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_R
        //12_4_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_S
        //12_4_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_T
        //12_4_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_U
        //12_4_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_V
        //12_4_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_W
        //12_4_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_X
        //12_4_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_Y
        //12_4_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_E_Z
        //12_4_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_LowCostPNR_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_LowCostPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.LowCostPNR)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_LowCostPNR_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_G
        //12_5_6
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_TotalPrice(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_TotalPrice", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_H
        //12_5_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_DateOfBook(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_DateOfBook", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_I
        //12_5_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_J
        //12_5_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_K
        //12_5_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_L
        //12_5_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_N
        //12_5_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_O
        //12_5_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_P
        //12_5_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_Q
        //12_5_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_R
        //12_5_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_S
        //12_5_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_T
        //12_5_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_U
        //12_5_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_V
        //12_5_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_W
        //12_5_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_X
        //12_5_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_Y
        //12_5_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_F_Z
        //12_5_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_SupplierPNR_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_SupplierPNR, ConvertTo.ConvertEmptyToDBNull(oOrders.SupplierPNR)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_SupplierPNR_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_H
        //12_6_7
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_DateOfBook(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_DateOfBook", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_I
        //12_6_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_J
        //12_6_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_K
        //12_6_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_L
        //12_6_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_N
        //12_6_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_O
        //12_6_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_P
        //12_6_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_Q
        //12_6_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_R
        //12_6_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_S
        //12_6_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_T
        //12_6_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_U
        //12_6_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_V
        //12_6_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_W
        //12_6_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_X
        //12_6_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_Y
        //12_6_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_G_Z
        //12_6_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_TotalPrice_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_TotalPrice, ConvertTo.ConvertEmptyToDBNull(oOrders.TotalPrice)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_TotalPrice_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_I
        //12_7_8
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_ReceiveEmail(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ReceiveEmail", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_J
        //12_7_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_K
        //12_7_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_L
        //12_7_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_N
        //12_7_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_O
        //12_7_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_P
        //12_7_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_Q
        //12_7_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_R
        //12_7_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_S
        //12_7_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_T
        //12_7_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_U
        //12_7_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_V
        //12_7_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_W
        //12_7_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_X
        //12_7_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_Y
        //12_7_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_H_Z
        //12_7_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_DateOfBook_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_DateOfBook, ConvertTo.ConvertEmptyToDBNull(oOrders.DateOfBook)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_DateOfBook_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_J
        //12_8_9
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_ReceiveSms(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_ReceiveSms", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_K
        //12_8_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_L
        //12_8_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_N
        //12_8_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_O
        //12_8_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_P
        //12_8_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_Q
        //12_8_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_R
        //12_8_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_S
        //12_8_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_T
        //12_8_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_U
        //12_8_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_V
        //12_8_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_W
        //12_8_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_X
        //12_8_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_Y
        //12_8_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_I_Z
        //12_8_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveEmail_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveEmail, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveEmail)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveEmail_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_K
        //12_9_10
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_MarketingCabin(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_MarketingCabin", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_L
        //12_9_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_N
        //12_9_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_O
        //12_9_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_P
        //12_9_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_Q
        //12_9_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_R
        //12_9_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_S
        //12_9_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_T
        //12_9_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_U
        //12_9_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_V
        //12_9_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_W
        //12_9_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_X
        //12_9_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_Y
        //12_9_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_J_Z
        //12_9_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ReceiveSms_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ReceiveSms, ConvertTo.ConvertEmptyToDBNull(oOrders.ReceiveSms)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ReceiveSms_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_L
        //12_10_11
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_ContactDetailsDocId(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_ContactDetailsDocId", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_N
        //12_10_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_O
        //12_10_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_P
        //12_10_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_Q
        //12_10_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_R
        //12_10_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_S
        //12_10_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_T
        //12_10_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_U
        //12_10_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_V
        //12_10_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_W
        //12_10_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_X
        //12_10_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_Y
        //12_10_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_K_Z
        //12_10_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_MarketingCabin_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_MarketingCabin, ConvertTo.ConvertEmptyToDBNull(oOrders.MarketingCabin)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_MarketingCabin_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_N
        //12_11_13
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_AirlineIataCode(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_AirlineIataCode", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_O
        //12_11_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_P
        //12_11_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_Q
        //12_11_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_R
        //12_11_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_S
        //12_11_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_T
        //12_11_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_U
        //12_11_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_V
        //12_11_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_W
        //12_11_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_X
        //12_11_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_Y
        //12_11_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_L_Z
        //12_11_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_ContactDetailsDocId_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_ContactDetailsDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.ContactDetailsDocId)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_ContactDetailsDocId_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_O
        //12_13_14
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderStatus(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderStatus", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_P
        //12_13_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_Q
        //12_13_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_R
        //12_13_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_S
        //12_13_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_T
        //12_13_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_U
        //12_13_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_V
        //12_13_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_W
        //12_13_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_X
        //12_13_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_Y
        //12_13_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_N_Z
        //12_13_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AirlineIataCode_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AirlineIataCode, ConvertTo.ConvertEmptyToDBNull(oOrders.AirlineIataCode)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AirlineIataCode_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_P
        //12_14_15
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_AdminRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_AdminRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_Q
        //12_14_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_R
        //12_14_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_S
        //12_14_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_T
        //12_14_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_U
        //12_14_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_V
        //12_14_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_W
        //12_14_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_X
        //12_14_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_Y
        //12_14_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_O_Z
        //12_14_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderStatus_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderStatus, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderStatus)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderStatus_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_Q
        //12_15_16
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_StatusPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_StatusPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_R
        //12_15_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_S
        //12_15_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_T
        //12_15_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_U
        //12_15_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_V
        //12_15_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_W
        //12_15_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_X
        //12_15_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_Y
        //12_15_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_P_Z
        //12_15_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_AdminRemarks_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_AdminRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.AdminRemarks)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_AdminRemarks_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_R
        //12_16_17
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_OrderToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_S
        //12_16_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_T
        //12_16_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_U
        //12_16_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_V
        //12_16_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_W
        //12_16_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_X
        //12_16_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_Y
        //12_16_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_Q_Z
        //12_16_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_StatusPayment_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_StatusPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.StatusPayment)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_StatusPayment_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_S
        //12_17_18
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_PaymentTransaction(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentTransaction", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_T
        //12_17_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_U
        //12_17_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_V
        //12_17_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_W
        //12_17_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_X
        //12_17_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_Y
        //12_17_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_R_Z
        //12_17_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderToken_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderToken, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderToken)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderToken_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_S_T
        //12_18_19
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_XmlBookRequest(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_XmlBookRequest", ex.Message));

            }
            return paramsSelect;
        }



        //M_S_U
        //12_18_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_S_V
        //12_18_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_S_W
        //12_18_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_S_X
        //12_18_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_S_Y
        //12_18_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_S_Z
        //12_18_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentTransaction_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentTransaction, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentTransaction)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentTransaction_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_T_U
        //12_19_20
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_XmlBookResponse(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_XmlBookResponse", ex.Message));

            }
            return paramsSelect;
        }



        //M_T_V
        //12_19_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_T_W
        //12_19_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_T_X
        //12_19_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_T_Y
        //12_19_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_T_Z
        //12_19_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookRequest_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookRequest, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookRequest)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookRequest_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_U_V
        //12_20_21
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_OrderRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_OrderRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_U_W
        //12_20_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_U_X
        //12_20_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_U_Y
        //12_20_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_U_Z
        //12_20_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_XmlBookResponse_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_XmlBookResponse, ConvertTo.ConvertEmptyToDBNull(oOrders.XmlBookResponse)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_XmlBookResponse_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_V_W
        //12_21_22
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_OrderPayment(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_OrderPayment", ex.Message));

            }
            return paramsSelect;
        }



        //M_V_X
        //12_21_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_V_Y
        //12_21_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_V_Z
        //12_21_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderRemarks_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderRemarks)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderRemarks_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_W_X
        //12_22_23
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment_PaymentToken(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_PaymentToken", ex.Message));

            }
            return paramsSelect;
        }



        //M_W_Y
        //12_22_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_W_Z
        //12_22_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_OrderPayment_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_OrderPayment, ConvertTo.ConvertEmptyToDBNull(oOrders.OrderPayment)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_OrderPayment_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_X_Y
        //12_23_24
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentToken_PaymentRemarks(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken_PaymentRemarks", ex.Message));

            }
            return paramsSelect;
        }



        //M_X_Z
        //12_23_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentToken_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentToken, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentToken)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentToken_Currency", ex.Message));

            }
            return paramsSelect;
        }



        //M_Y_Z
        //12_24_25
        internal static IDataParameterCollection GetParamsForSelectByKeysView_WhiteLabelDocId_PaymentRemarks_Currency(Orders oOrders)
        {
            IDataParameterCollection paramsSelect = null;
            try
            {
                DB db = new DB();
                #region DB Params (IN/OUT)
                //----add parameters-------------------------------------//

db.AddParameter(TBNames_Orders.PRM_WhiteLabelDocId, ConvertTo.ConvertEmptyToDBNull(oOrders.WhiteLabelDocId)); 
db.AddParameter(TBNames_Orders.PRM_PaymentRemarks, ConvertTo.ConvertEmptyToDBNull(oOrders.PaymentRemarks)); 
db.AddParameter(TBNames_Orders.PRM_Currency, ConvertTo.ConvertEmptyToDBNull(oOrders.Currency));
                //-------------------------------------------------------//
                #endregion
                paramsSelect = db.Parameters;
            }
            catch (Exception ex)
            {
                //  throw new Exception(ExceptionMsg.SetErrorMsg(ExceptionMsg.Orders, "Select_Orders_By_Keys_View_WhiteLabelDocId_PaymentRemarks_Currency", ex.Message));

            }
            return paramsSelect;
        }


#endregion

}
}
