﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    public class FileHandler
    {
        private static string CreateDir(string DirectoryPath)
        {
            DirectoryInfo Di = new DirectoryInfo(DirectoryPath);
            if (!Di.Exists)
            {
                Di.Create();
            }

            return DirectoryPath;
        }

        /// <summary>
        /// Delete File
        /// </summary>
        /// <param name="file_name">File name To Delete</param>
        public static void RemoveFile(string file_name)
        {
            FileInfo FI = new FileInfo(System.IO.Path.Combine(file_name));
            if (FI.Exists)
            {
                FI.Delete();
            }
        }

        /// <summary>
        /// Save File
        /// </summary>
        /// <param name="Directoryname">Directory Name</param>
        /// <returns>File Path Name To Save</returns>
        private static string SaveFile(string file_directory, string file_name, out string fileTrueName)
        {
            try
            {
                string fn = System.IO.Path.GetFileName(file_name);
                FileInfo FI = new FileInfo(System.IO.Path.Combine(file_directory, fn));
                fileTrueName = fn;
                //Prevent From overight Duplicate File Name
                int counter = 1;
                while (FI.Exists)
                {
                    fileTrueName = counter.ToString() + "_" + fn;
                    FI = new FileInfo(System.IO.Path.Combine(file_directory, fileTrueName));

                    counter++;

                }
                if (counter != 1)
                {
                    fn = fileTrueName;
                }
                return System.IO.Path.Combine(file_directory, fn);
            }
            catch (Exception ex)
            {
                //TODO: Add Handel and Logger
                throw;
            }
        }
        /// <summary>
        /// Validate that File is type of image (jpg , jpeg , bmp ,gif, png)
        /// </summary>
        /// <param name="file_name">file_name To Validate</param>
        /// <returns>True If File Is valid</returns>
        private static bool ValidateFileTypeImage(string file_name)
        {
            List<string> ValidFileType = new List<string>() { "jpg", "jpeg", "bmp", "gif", "png" };
            string fileExtension = file_name.Substring(file_name.LastIndexOf('.') + 1).ToLower();
            return ValidFileType.Contains(fileExtension);

        }

        /// <summary>
        /// Validate that File is type of Excel (xls)
        /// </summary>
        /// <param name="file_name">file_name To Validate</param>
        /// <returns>True If File Is valid</returns>
        private static bool ValidateFileTypeExcel(string file_name)
        {
            List<string> ValidFileType = new List<string>() { "xls" };
            string fileExtension = file_name.Substring(file_name.LastIndexOf('.') + 1).ToLower();
            return ValidFileType.Contains(fileExtension);
        }

        public static string CreateDirByDate(DateTime dt)
        {
            string Dir = string.Format(@"{0}\{1}\{2}", WebConfigHandler.VirtualDirectoryPath, dt.Year, dt.Month);
            DL_Generic.Files.CreateDir(null, Dir);
            return Dir;
        }


        /// <summary>
        /// Check For  Valid Directory Directey  And Save File
        /// </summary>
        /// <param name="file_name">The File Name</param>
        /// <param name="ImageFoldersTypes">Object Folder Type</param>
        /// <returns>File Path</returns>
        public static string UploadFileValidate(string file_name, Generic.EnumHandler.ImageFoldersTypes ImageFoldersTypes, int relatedObjectDocId, out string fileTrueName, int whithLabelDocId)
        {
           // string DirectoryPath = string.Format("{0}\\{1}", BasePage.VirtualDirectoryPath, BasePage.UploadDirectoryName); //, GetDirectoryPath(ImageFoldersTypes, whithLabelDocId));

            string DirectoryPath = string.Format("{0}\\{1}", WebConfigHandler.VirtualDirectoryPath, WebConfigHandler.UploadDirectoryName); //, GetDirectoryPath(ImageFoldersTypes, whithLabelDocId));
            //switch (ImageFoldersTypes)
            //{
            //    case EnumHandler.ImageFoldersTypes.CategoryImageFolder:
            //    case EnumHandler.ImageFoldersTypes.PackageImageFolder:
            //    case EnumHandler.ImageFoldersTypes.EventImageFolder:
            //    case EnumHandler.ImageFoldersTypes.ArticlesImageFolder:
            //        DirectoryPath = string.Format("{0}/{1}", DirectoryPath, relatedObjectDocId);
            //        break;
              
            //    case EnumHandler.ImageFoldersTypes.WhiteLabelFolder:
            //        DirectoryPath = string.Format("{0}/{1}/{2}", BasePage.VirtualDirectoryPath, BasePage.UploadDirectoryName, GetDirectoryPath(ImageFoldersTypes, relatedObjectDocId));
            //        break;
            //    default:
            //        DirectoryPath = string.Format("{0}/{1}", DirectoryPath, relatedObjectDocId);
            //        break;
            //}
            ////Create new Directory If Not Exist
            // DirectoryPath = string.Format("{0}/{1}", DirectoryPath, relatedObjectDocId);
            DirectoryInfo Di = new DirectoryInfo(DirectoryPath);
            if (!Di.Exists)
            {
                Di.Create();
            }

            return SaveFile(Di.FullName, file_name, out fileTrueName);

        }
    }
}
