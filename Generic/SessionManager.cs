﻿using DL_LowCost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Generic
{
    public static class SessionManager
    {
        #region Generic Session

        /// <summary>
        /// Get Object By Type From Session with support to add unique names
        /// Only need to send object as Type and the function will find the session by his typeof string
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="sessionName">In case you have a uniqe name for session fill this parametr with his name otherwise leave blank or null</param>
        /// <returns>Object as type sent from Session in case session is not null otherwise return null</returns>
        public static T GetSession<T>(string sessionName = null)
        {
            if (HttpContext.Current.Session[typeof(T).ToString()] != null)
            {
                if (sessionName != null)
                {
                    return (T)HttpContext.Current.Session[typeof(T).ToString() + sessionName];
                }
                else
                {
                    return (T)HttpContext.Current.Session[typeof(T).ToString()];
                }
            }
            else
            {
                return default(T);
            }
        }

        //old version
        //public static bool GetSession<T>(out T obj, string sessionName = null)
        //{
        //    if (HttpContext.Current.Session[typeof(T).ToString()] != null)
        //    {
        //        if (sessionName != null)
        //        {
        //            obj = (T)HttpContext.Current.Session[typeof(T).ToString() + sessionName];
        //        }
        //        else
        //        {
        //            obj = (T)HttpContext.Current.Session[typeof(T).ToString()];
        //        }
        //        return true;
        //    }
        //    else
        //    {
        //        obj = default(T);
        //        return false;
        //    }
        //}

        //my version
        public static bool GetSession<T>(out T obj, string sessionName = null)
        {
            if (HttpContext.Current.Session[typeof(T).ToString()] != null || HttpContext.Current.Session[typeof(T).ToString() + sessionName] != null)
            {
                if (sessionName != null)
                {
                    obj = (T)HttpContext.Current.Session[typeof(T).ToString() + sessionName];
                }
                else
                {
                    obj = (T)HttpContext.Current.Session[typeof(T).ToString()];
                }
                return true;
            }
            else
            {
                obj = default(T);
                return false;
            }
        }

        /// <summary>
        ///  מהסשן Language פונקציה לקבלת האובייקט של ה 
        /// </summary>
        /// <returns></returns>
        public static Languages GetLanguageFromSession()
        {
            if (System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session["Languages"] != null)
            {
                return System.Web.HttpContext.Current.Session["Languages"] as Languages;
            }
            return null;
        }

        /// <summary>
        /// Set Session for Object By Type Genericly with ability to set Session with uniqe name in case of needs if not leave parameter blank or null
        /// Only send the objuect and the function will create the session by his type.
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="sessionObject">Object as type</param>
        /// <param name="sessionName">In case you need a uniqe name for session fill this parametr with his name otherwise leave blank or null</param>
        public static void SetSession<T>(T sessionObject, string sessionName = null)
        {
            if (sessionName != null)
                HttpContext.Current.Session[typeof(T).ToString() + sessionName] = sessionObject;
            else
                HttpContext.Current.Session[typeof(T).ToString()] = sessionObject;
        }

        public static void ClearSession<T>(T sessionObject, string sessionName = null)
        {
            if (sessionName != null)
                HttpContext.Current.Session[typeof(T).ToString() + sessionName] = null;
            else
                HttpContext.Current.Session[typeof(T).ToString()] = null;
        }


        //public static XXXObject SessionXXX
        //{
        //    get
        //    {
        //        if (HttpContext.Current.Session["SessionXXX"] != null)
        //        {
        //            return HttpContext.Current.Session["SessionXXX"] as XXXObject;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session["SessionXXX"] = value;
        //    }
        //}

        #endregion



        #region Admin
        /// <summary>
        ///  מהסשן Admin פונקציה לקבלת האובייקט של ה 
        /// </summary>
        /// <returns></returns>
        public static Users GetAdminFromSession()
        {
            if (System.Web.HttpContext.Current.Session["Admin"] != null)
            {
                return System.Web.HttpContext.Current.Session["Admin"] as Users;
            }
            return null;
        }

        /// <summary>
        /// פונקציה לאיפוס הסשן
        /// </summary>
        public static void ResetAdminSession()
        {
            System.Web.HttpContext.Current.Session["Admin"] = null;
        }

        /// <summary>
        /// בסשן Admin פונקציה ששומרת את ה 
        /// </summary>
        /// <param name="oWhiteLabel"></param>
        public static void SaveAdminToSession(Users oAdmin)
        {
            if (oAdmin == null)
            {
                return;
            }
            System.Web.HttpContext.Current.Session["Admin"] = oAdmin;
        }

        /// <summary>
        /// קיים בסשן Admin פונקציה שבודקת אם ה 
        /// </summary>
        /// <returns></returns>
        public static bool IsAdminExist()
        {
            if (System.Web.HttpContext.Current.Session["Admin"] != null)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}