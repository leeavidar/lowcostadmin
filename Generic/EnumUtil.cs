﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using DL_Generic;

namespace Generic
{
    /// <summary>
    /// class to manage enum functions
    /// </summary>
    public static class EnumUtil
    {
        #region Generic Functions
         /// <summary>
        /// function that returns Enum  as listItem white text and value
        /// </summary>
        /// <param name="withDefaultValue">insert "true" if you want default Value of "Choose" in your DDL</param>
        /// <returns>Enum as listItem</returns>
        public static List<ListItem> GetEnumToListItems<T>(bool withDefaultValue = false) where T : struct, IConvertible
        {
            // if the Enum is not define
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("Enum is not define");
            }

            // create item list
            List<ListItem> listItemList = new List<ListItem>();

            // if the var 'withDefaultValue' is true so we add "Choose" text in top of the list
            if (withDefaultValue)
            {
                listItemList.Add(new ListItem("Choose", ConvertToValue.Int32EmptyValue.ToString()));
            }

            // loop on the enum data source (get the data source from function)  
            foreach (T item in GetValues<T>())
            {
                // For each item - creating value pair that contains textual display and a numeric value and adding snap to position
                // Values ​​obtained from external function to return the correct language
                KeyValuePair<int, string> pair = item.GetEnumItemAsLangString<T>();
                listItemList.Add(new ListItem(pair.Value, pair.Key.ToString()));
            }

            //החזרת רשימת הפריטים
            return listItemList;
        }


        /// <summary>
        /// generic function that get enum and return him as dataSource
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <returns>Enum as dataSource</returns>
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
        #endregion
        
        #region Manual 
        /// <summary>
        /// function that get enum item and returns him as text
        /// </summary>
        /// <typeparam name="T">Enum type</typeparam>
        /// <param name="index">enum item</param>
        /// <returns>enum item as text</returns>
        public static KeyValuePair<int, string> GetEnumItemAsLangString<T>(this T index) where T : struct, IConvertible
        {
            // if the Enum is not define
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("Not Enum");
            }

            // define vars
            int key = -1;
            string value = "";

            // get the enum values
            if (index is EnumHandler.MonthNames)
            {
                key = (int)((EnumHandler.MonthNames)Enum.Parse(typeof(EnumHandler.MonthNames), index.ToString(), true));
                value = ((EnumHandler.MonthNames)Enum.Parse(typeof(EnumHandler.MonthNames), index.ToString(), true)).ToLangString();
            }
            else if (index is EnumHandler.DaysOfWeek)
            {
                key = (int)((EnumHandler.DaysOfWeek)Enum.Parse(typeof(EnumHandler.DaysOfWeek), index.ToString(), true));
                value = ((EnumHandler.DaysOfWeek)Enum.Parse(typeof(EnumHandler.DaysOfWeek), index.ToString(), true)).ToLangString();
            }
            else if (index is EnumHandler.OrderStatus)
            {
                key = (int)((EnumHandler.OrderStatus)Enum.Parse(typeof(EnumHandler.OrderStatus), index.ToString(), true));
                value = ((EnumHandler.OrderStatus)Enum.Parse(typeof(EnumHandler.OrderStatus), index.ToString(), true)).ToLangString();
            }
            else if (index is EnumHandler.UserRoles)
            {
                key = (int)((EnumHandler.UserRoles)Enum.Parse(typeof(EnumHandler.UserRoles), index.ToString(), true));
                value = ((EnumHandler.UserRoles)Enum.Parse(typeof(EnumHandler.UserRoles), index.ToString(), true)).ToLangString();
            }
            else { throw new Exception("Not Supported Enum"); }

            return new KeyValuePair<int, string>(key, value);
        }

        #region Each Enum

        /// <summary>
        /// function that get the enum Types and return in current lang
        /// </summary>
        /// <param name="index">the Types</param>
        /// <returns></returns>
        public static string ToLangString(this EnumHandler.OrderPaymentStatus index)
        {
            switch (index)
            {
                case EnumHandler.OrderPaymentStatus.NotPaid:
                    return "Not Payed";
                case EnumHandler.OrderPaymentStatus.Failed:
                    return "Payment Failed";
                case EnumHandler.OrderPaymentStatus.Paid:
                    return "Paid";
                default:
                    return "";
            }
        }

        /// <summary>
        /// function that get the enum Types and return in current lang
        /// </summary>
        /// <param name="index">the Types</param>
        /// <returns></returns>
        public static string ToLangString(this EnumHandler.OrderStatus index)
        {
            switch (index)
            {
                case EnumHandler.OrderStatus.All:
                    return "All";
                case EnumHandler.OrderStatus.Succeeded:
                    return "Succeeded";
                case EnumHandler.OrderStatus.BookingInProgress:
                    return "Booking In Progress";
                case EnumHandler.OrderStatus.Failed:
                    return "Failed";
                case EnumHandler.OrderStatus.Unconfirmed:
                    return "Unconfirmed";
                case EnumHandler.OrderStatus.UnconfirmedBySupplier:
                    return "Unconfirmed By Supplier";
                case EnumHandler.OrderStatus.DuplicateBooking:
                    return "Duplicate Booking";
                default:
                    return "";
            }
        }
        
        /// <summary>
        /// function that get the enum Types and return in current lang
        /// </summary>
        /// <param name="index">the Types</param>
        /// <returns></returns>
        public static string ToLangString(this EnumHandler.DaysOfWeek index)
        {
            switch (index)
            {
                case EnumHandler.DaysOfWeek.Sunday:
                    return LangManager.GetLangText("Sunday");
                case EnumHandler.DaysOfWeek.Monday:
                    return LangManager.GetLangText("Monday");
                case EnumHandler.DaysOfWeek.Tuesday:
                    return LangManager.GetLangText("Tuesday");
                case EnumHandler.DaysOfWeek.Wednesday:
                    return LangManager.GetLangText("Wednesday");
                case EnumHandler.DaysOfWeek.Thursday:
                    return LangManager.GetLangText("Thursday");
                case EnumHandler.DaysOfWeek.Friday:
                    return LangManager.GetLangText("Friday");
                case EnumHandler.DaysOfWeek.Saturday:
                    return LangManager.GetLangText("Saturday");
                default:
                    return LangManager.GetLangText("");
            }
        }

        /// <summary>
        /// function that get the enum Types and return in current lang
        /// </summary>
        /// <param name="index">the Types</param>
        /// <returns></returns>
        public static string ToLangString(this EnumHandler.MonthNames index)
        {
            switch (index)
            {
                case EnumHandler.MonthNames.January:
                    return LangManager.GetLangText("January");
                case EnumHandler.MonthNames.February:
                    return LangManager.GetLangText("February");
                case EnumHandler.MonthNames.March:
                    return LangManager.GetLangText("March");
                case EnumHandler.MonthNames.April:
                    return LangManager.GetLangText("April");
                case EnumHandler.MonthNames.May:
                    return LangManager.GetLangText("May");
                case EnumHandler.MonthNames.June:
                    return LangManager.GetLangText("June");
                case EnumHandler.MonthNames.July:
                    return LangManager.GetLangText("July");
                case EnumHandler.MonthNames.August:
                    return LangManager.GetLangText("August");
                case EnumHandler.MonthNames.September:
                    return LangManager.GetLangText("September");
                case EnumHandler.MonthNames.October:
                    return LangManager.GetLangText("October");
                case EnumHandler.MonthNames.November:
                    return LangManager.GetLangText("November");
                case EnumHandler.MonthNames.December:
                    return LangManager.GetLangText("December");
                default:
                    return LangManager.GetLangText("");
            }
        }


        /// <summary>
        /// function that get the enum Types and return in current lang
        /// </summary>
        /// <param name="index">the Types</param>
        /// <returns></returns>
        public static string ToLangString(this EnumHandler.UserRoles index)
        {
            switch (index)
            {
                case EnumHandler.UserRoles.ChiefAdmin:
                    return "Chief Admin";
                case EnumHandler.UserRoles.Admin:
                    return "Admin";
                case EnumHandler.UserRoles.SalesOperator:
                    return "Sales Operator";
                case EnumHandler.UserRoles.SiteOperator:
                    return "Site Operator";
                default:
                    return "";
            }
        }
        
        #endregion
        #endregion       
    }


}
