

GO
/****** Object:  Index [PK_ContactDetails_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[ContactDetails] DROP CONSTRAINT [PK_ContactDetails_DocId]

GO






















GO
/****** Object:  Index [PK_Passenger_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Passenger] DROP CONSTRAINT [PK_Passenger_DocId]

GO






















GO
/****** Object:  Index [PK_SeatPerFlightLeg_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[SeatPerFlightLeg] DROP CONSTRAINT [PK_SeatPerFlightLeg_DocId]

GO











GO
/****** Object:  Index [PK_Luggage_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Luggage] DROP CONSTRAINT [PK_Luggage_DocId]

GO











GO
/****** Object:  Index [PK_FlightLeg_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[FlightLeg] DROP CONSTRAINT [PK_FlightLeg_DocId]

GO
















GO
/****** Object:  Index [PK_Airline_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Airline] DROP CONSTRAINT [PK_Airline_DocId]

GO







GO
/****** Object:  Index [PK_PopularDestination_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[PopularDestination] DROP CONSTRAINT [PK_PopularDestination_DocId]

GO









GO
/****** Object:  Index [PK_Airport_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Airport] DROP CONSTRAINT [PK_Airport_DocId]

GO









GO
/****** Object:  Index [PK_City_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[City] DROP CONSTRAINT [PK_City_DocId]

GO








GO
/****** Object:  Index [PK_Country_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Country] DROP CONSTRAINT [PK_Country_DocId]

GO







GO
/****** Object:  Index [PK_DestinationPage_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[DestinationPage] DROP CONSTRAINT [PK_DestinationPage_DocId]

GO















GO
/****** Object:  Index [PK_OrderFlightLeg_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[OrderFlightLeg] DROP CONSTRAINT [PK_OrderFlightLeg_DocId]

GO











GO
/****** Object:  Index [PK_Orders_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [PK_Orders_DocId]

GO


























GO
/****** Object:  Index [PK_Images_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Images] DROP CONSTRAINT [PK_Images_DocId]

GO











GO
/****** Object:  Index [PK_Messages_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Messages] DROP CONSTRAINT [PK_Messages_DocId]

GO











GO
/****** Object:  Index [PK_Stamps_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Stamps] DROP CONSTRAINT [PK_Stamps_DocId]

GO







GO
/****** Object:  Index [PK_Stamps_PopularDestination_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Stamps_PopularDestination] DROP CONSTRAINT [PK_Stamps_PopularDestination_DocId]

GO







GO
/****** Object:  Index [PK_WhiteLabel_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[WhiteLabel] DROP CONSTRAINT [PK_WhiteLabel_DocId]

GO













GO
/****** Object:  Index [PK_Seo_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Seo] DROP CONSTRAINT [PK_Seo_DocId]

GO











GO
/****** Object:  Index [PK_IndexPagePromo_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[IndexPagePromo] DROP CONSTRAINT [PK_IndexPagePromo_DocId]

GO














GO
/****** Object:  Index [PK_StaticPages_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[StaticPages] DROP CONSTRAINT [PK_StaticPages_DocId]

GO











GO
/****** Object:  Index [PK_Stops_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Stops] DROP CONSTRAINT [PK_Stops_DocId]

GO









GO
/****** Object:  Index [PK_Template_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Template] DROP CONSTRAINT [PK_Template_DocId]

GO














GO
/****** Object:  Index [PK_SubBox_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[SubBox] DROP CONSTRAINT [PK_SubBox_DocId]

GO











GO
/****** Object:  Index [PK_Currency_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [PK_Currency_DocId]

GO








GO
/****** Object:  Index [PK_CompanyPage_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[CompanyPage] DROP CONSTRAINT [PK_CompanyPage_DocId]

GO










GO
/****** Object:  Index [PK_Company_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Company] DROP CONSTRAINT [PK_Company_DocId]

GO







GO
/****** Object:  Index [PK_Tip_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Tip] DROP CONSTRAINT [PK_Tip_DocId]

GO











GO
/****** Object:  Index [PK_Users_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [PK_Users_DocId]

GO












GO
/****** Object:  Index [PK_HomePage_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[HomePage] DROP CONSTRAINT [PK_HomePage_DocId]

GO











GO
/****** Object:  Index [PK_HomePageDestinationPage_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[HomePageDestinationPage] DROP CONSTRAINT [PK_HomePageDestinationPage_DocId]

GO









GO
/****** Object:  Index [PK_Faq_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Faq] DROP CONSTRAINT [PK_Faq_DocId]

GO










GO
/****** Object:  Index [PK_MailAddress_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[MailAddress] DROP CONSTRAINT [PK_MailAddress_DocId]

GO







GO
/****** Object:  Index [PK_PhonePrefix_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[PhonePrefix] DROP CONSTRAINT [PK_PhonePrefix_DocId]

GO






GO
/****** Object:  Index [PK_OrderLog_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[OrderLog] DROP CONSTRAINT [PK_OrderLog_DocId]

GO











GO
/****** Object:  Index [PK_Languages_DocId]    Script Date: 10/06/2014 17:56:08 ******/
ALTER TABLE [dbo].[Languages] DROP CONSTRAINT [PK_Languages_DocId]

GO





