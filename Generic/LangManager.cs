﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Generic
{
    /// Class To Translate Text In The WebSite
    /// </summary>
    public class LangManager : System.Web.UI.Page
    {
        /// </summary>
        /// Retrieves the subdomain from the specified URL.
        /// </summary>
        /// <param name="url">The URL from which to retrieve the subdomain.</param>
        /// <returns>The subdomain if it exist, otherwise null.</returns>
        private static string GetLanguagePrefix(Uri url)
        {
            if (url.HostNameType == UriHostNameType.Dns)
            {
                string host = url.Host;
                return host.Split(new char[] { '.' })[0];
            }
            return null;
        }

        public static int LangId
        {
            get
            {
                string langPrefix = GetLanguagePrefix(HttpContext.Current.Request.Url);

                //Languages oLanguages = LanguagesContainer.SelectByKeysView_Code(langPrefix, null).Single;
                //if (oLanguages != null)
                //{
                //    return oLanguages.DocId_Value;
                //}
                return WebConfigHandler.DefaultLanguageID;
            }
        }

        public static string Lang
        {
            get
            {
                string langPrefix = GetLanguagePrefix(HttpContext.Current.Request.Url);
               
                //Languages oLanguages = LanguagesContainer.SelectByKeysView_Code(langPrefix, null).Single;
                //if (oLanguages != null)
                //{
                //    return oLanguages.Name_UI;
                //}
                //
                //oLanguages = LanguagesContainer.SelectByKeysView_DocId(1, null).Single;
                //if (oLanguages != null)
                //{
                //    return oLanguages.Name_UI;
                //}

                return "Hebrew";
            }
        }

        public virtual string GetText(string text)
        {
            return GetLangText(text);

        }

        public static string GetLangText(string text)
        {
            try
            {
                ResourceManager temp =
                    new ResourceManager("Resources." + Lang,
   System.Reflection.Assembly.GetExecutingAssembly());
                return temp.GetString(text);

            }
            catch (Exception)
            {
                return "";
            }
        }

    }
}
