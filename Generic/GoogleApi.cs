﻿using BL_LowCost;
using DL_Generic;
using DL_LowCost;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Generic
{

    public static class GoogleApi
    {

        public static string GetLocalTimeDiffrenceCityName(string cityName, string apiKey)
        {
            int israelTimeDiff = GetTimeDifferenceForCity("Jerusalem", apiKey);
            int cityTimeDiff = GetTimeDifferenceForCity(cityName, apiKey);
            // return the Jerusalem related time difference of the given city
            return (cityTimeDiff - israelTimeDiff).ToString();

            // string apiKey = "AIzaSyCw19Lj6Ui8WyAqxAvZR-bzCnpkXgtljg4";
        }


        /// <summary>
        /// A method that gets the time difference from UTC to a provided address (iata code or city name)
        /// It is better to papss a iata code, and not a city name, as some names may not be found.
        /// </summary>
        /// <param name="cityIataCode">The iata code of the city you wish to find the time difference for (from UTC time)</param>
        /// <param name="apiKey">The google API key</param>
        /// <returns>The time difference from UTC to the given address, including DST (daylight saving time) if needed.</returns>
        private static int GetTimeDifferenceForCity(string cityIataCode, string apiKey)
        {

            CityContainer cities = CityContainer.SelectByKeysView_IataCode(cityIataCode, null);
            string cityName = "";
            if (cities.Count > 0)
            {
                City oCity = cities.First;
                // Get the name in english
                cityName = oCity.EnglishName_UI;
            }

            // string apiKey = "AIzaSyCw19Lj6Ui8WyAqxAvZR-bzCnpkXgtljg4";

            string address = cityName != "" ? cityName : cityIataCode;

            // get the lat and lng values of the selected city
            string GeoCodeApiRequest = string.Format("https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}", address, apiKey);

            // Get response including the city's lat and lng values:
            GoogleGeoCodeResponse locationLatLng = GetAction<GoogleGeoCodeResponse>(GeoCodeApiRequest);

            string lat = "", lng = "";
            // take the lat and lng values from the results:
            if (locationLatLng.status != "ZERO_RESULTS")
            {
                lat = locationLatLng.results[0].geometry.location.lat;
                lng = locationLatLng.results[0].geometry.location.lng;
            }
            else
            {
                // the city was not found...
                return -1;
            }

            // Get the time stamp at UTC
            string timeStamp = DateTime.Now.ToTimestamp().ToString();

            // use the lat and lng values to generate a request for time offset (from GMT) in seconds (as rawoffset):
            string TimeZoneApiRequest = string.Format("https://maps.googleapis.com/maps/api/timezone/json?location={0},{1}&timestamp={2}&key={3}", lat, lng, timeStamp, apiKey);

            var result = GetAction<TimeZoneDetails>(TimeZoneApiRequest);

            // var timeDifference = int.Parse(result.rawOffset);
            var timeDifference = int.Parse(result.rawOffset) + int.Parse(result.dstOffset);

            return timeDifference;
        }

        public static double ToTimestamp(this DateTime date)
        {
            return date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }


        private static TResult GetAction<TResult>(string url)
        {
            Uri uri = new Uri(url);

            var task1 = Task.Run(async () =>
            {
                using (System.Net.WebClient webClient = new System.Net.WebClient())
                {
                    string jsonResult = await webClient.DownloadStringTaskAsync(uri);

                    var serializer = new JavaScriptSerializer();
                    if (typeof(TResult) == typeof(object))
                    {
                        //Use dynamic objects
                        serializer.RegisterConverters(new[] { new DynamicJsonConverter() });
                        dynamic results = serializer.Deserialize(jsonResult, typeof(object));
                        return results;
                    }
                    else
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        TResult results = js.Deserialize<TResult>(jsonResult);
                        return results;
                    }
                }
            });
            Task.WaitAll(task1);

            return (task1.Result);

        }

        public class TimeZoneDetails
        {
            public string dstOffset { get; set; }
            public string rawOffset { get; set; }
            public string status { get; set; }
            public string timeZoneId { get; set; }
            public string timeZoneName { get; set; }

        }

        public class GoogleGeoCodeResponse
        {

            public string status { get; set; }
            public results[] results { get; set; }

        }

        public class results
        {
            public string formatted_address { get; set; }
            public geometry geometry { get; set; }
            public string[] types { get; set; }
            public address_component[] address_components { get; set; }
        }

        public class geometry
        {
            public string location_type { get; set; }
            public location location { get; set; }
        }

        public class location
        {
            public string lat { get; set; }
            public string lng { get; set; }
        }

        public class address_component
        {
            public string long_name { get; set; }
            public string short_name { get; set; }
            public string[] types { get; set; }
        }

        public sealed class DynamicJsonConverter : JavaScriptConverter
        {
            public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
            {
                if (dictionary == null)
                    throw new ArgumentNullException("dictionary");

                return type == typeof(object) ? new DynamicJsonObject(dictionary) : null;
            }

            public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override IEnumerable<Type> SupportedTypes
            {
                get { return new ReadOnlyCollection<Type>(new List<Type>(new[] { typeof(object) })); }
            }

            #region Nested type: DynamicJsonObject

            private sealed class DynamicJsonObject : DynamicObject
            {
                private readonly IDictionary<string, object> _dictionary;

                public DynamicJsonObject(IDictionary<string, object> dictionary)
                {
                    if (dictionary == null)
                        throw new ArgumentNullException("dictionary");
                    _dictionary = dictionary;
                }

                public override string ToString()
                {
                    var sb = new StringBuilder("{");
                    ToString(sb);
                    return sb.ToString();
                }

                private void ToString(StringBuilder sb)
                {
                    var firstInDictionary = true;
                    foreach (var pair in _dictionary)
                    {
                        if (!firstInDictionary)
                            sb.Append(",");
                        firstInDictionary = false;
                        var value = pair.Value;
                        var name = pair.Key;
                        if (value is string)
                        {
                            sb.AppendFormat("{0}:\"{1}\"", name, value);
                        }
                        else if (value is IDictionary<string, object>)
                        {
                            new DynamicJsonObject((IDictionary<string, object>)value).ToString(sb);
                        }
                        else if (value is ArrayList)
                        {
                            sb.Append(name + ":[");
                            var firstInArray = true;
                            foreach (var arrayValue in (ArrayList)value)
                            {
                                if (!firstInArray)
                                    sb.Append(",");
                                firstInArray = false;
                                if (arrayValue is IDictionary<string, object>)
                                    new DynamicJsonObject((IDictionary<string, object>)arrayValue).ToString(sb);
                                else if (arrayValue is string)
                                    sb.AppendFormat("\"{0}\"", arrayValue);
                                else
                                    sb.AppendFormat("{0}", arrayValue);

                            }
                            sb.Append("]");
                        }
                        else
                        {
                            sb.AppendFormat("{0}:{1}", name, value);
                        }
                    }
                    sb.Append("}");
                }

                public override bool TryGetMember(GetMemberBinder binder, out object result)
                {
                    if (!_dictionary.TryGetValue(binder.Name, out result))
                    {
                        // return null to avoid exception.  caller can check for null this way...
                        result = null;
                        return true;
                    }

                    result = WrapResultObject(result);
                    return true;
                }

                public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
                {
                    if (indexes.Length == 1 && indexes[0] != null)
                    {
                        if (!_dictionary.TryGetValue(indexes[0].ToString(), out result))
                        {
                            // return null to avoid exception.  caller can check for null this way...
                            result = null;
                            return true;
                        }

                        result = WrapResultObject(result);
                        return true;
                    }

                    return base.TryGetIndex(binder, indexes, out result);
                }

                private static object WrapResultObject(object result)
                {
                    var dictionary = result as IDictionary<string, object>;
                    if (dictionary != null)
                        return new DynamicJsonObject(dictionary);

                    var arrayList = result as ArrayList;
                    if (arrayList != null && arrayList.Count > 0)
                    {
                        return arrayList[0] is IDictionary<string, object>
                            ? new List<object>(arrayList.Cast<IDictionary<string, object>>().Select(x => new DynamicJsonObject(x)))
                            : new List<object>(arrayList.Cast<object>());
                    }

                    return result;
                }
            }

            #endregion
        }
    }
}
