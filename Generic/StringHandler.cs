﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    public static class StringHandler
    {
        /// <summary>
        /// Set Command To Match JS
        /// </summary>
        /// <param name="command">Command To Set</param>
        /// <returns>Command Who Match JS</returns>
        public static string ConvertToFriendlyString(object command)
        {
            string FullCommand = command.ToString();
            char Qoutationmarks = '"';
            string NewQoutationmarks = "״";
            FullCommand = FullCommand.Replace(Qoutationmarks.ToString(), NewQoutationmarks);
            NewQoutationmarks = "׳";
            Qoutationmarks = '\'';
            return FullCommand.Replace(Qoutationmarks.ToString(), NewQoutationmarks);
        }

        /// <summary>
        /// This function returns the string that you send shorten to the the amount of chars you specify or as defiend by default with 3 dots in the end (...)
        /// </summary>
        /// <param name="CommandToShort">Full Comand you need to shorten</param>
        /// <param name="CommandLengh">The new length of the you need</param>
        /// <returns>string new command shorted to length specify</returns>
        public static string ShortCommand(object CommandToShort, object CommandLengh = null)
        {
            int DefaultLength = Constants.ShortCommandDefaultLength;
            if (CommandLengh != null)
            {
                try
                {
                    DefaultLength = Convert.ToInt32(CommandLengh);
                }
                catch
                {
                    DefaultLength = Constants.ShortCommandDefaultLength;
                }

            }
            //Moves All Enters
            string FullCommand = CommandToShort.ToString().Replace("\n", " ");
            if (FullCommand.Length < DefaultLength)
            {
                return FullCommand;
            }
            FullCommand = FullCommand.Substring(0, DefaultLength - 2);
            FullCommand += "...";
            return FullCommand;

        }

    }
}
