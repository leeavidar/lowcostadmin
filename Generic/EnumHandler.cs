﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    public class EnumHandler
    {
        /// <summary>
        /// חודשים
        /// </summary>
        public enum MonthNames
        {
            /// <summary>
            /// January
            /// </summary>
            January = 1,
            /// <summary>
            /// February
            /// </summary>
            February = 2,
            /// <summary>
            /// March
            /// </summary>
            March = 3,
            /// <summary>
            /// April
            /// </summary>
            April = 4,
            /// <summary>
            /// May
            /// </summary>
            May = 5,
            /// <summary>
            /// June
            /// </summary>
            June = 6,
            /// <summary>
            /// July
            /// </summary>
            July = 7,
            /// <summary>
            /// August
            /// </summary>
            August = 8,
            /// <summary>
            /// September
            /// </summary>
            September = 9,
            /// <summary>
            /// October
            /// </summary>
            October = 10,
            /// <summary>
            /// November
            /// </summary>
            November = 11,
            /// <summary>
            /// December
            /// </summary>
            December = 12,
        }

        /// <summary>
        /// Days Of Week Types
        /// </summary>
        public enum DaysOfWeek
        {
            /// <summary>
            /// Sunday
            /// </summary>
            Sunday = 1,
            /// <summary>
            /// Monday
            /// </summary>
            Monday = 2,
            /// <summary>
            /// Tuesday
            /// </summary>
            Tuesday = 3,
            /// <summary>
            /// Wednesday
            /// </summary>
            Wednesday = 4,
            /// <summary>
            /// Thursday
            /// </summary>
            Thursday = 5,
            /// <summary>
            /// Friday
            /// </summary>
            Friday = 6,
            /// <summary>
            /// Saturday
            /// </summary>
            Saturday = 7,
        }

        /// <summary>
        /// QueryString type you need to use in your site 
        /// in case of need for new QueryString type add his name here
        /// this enum used by generic function that retrieve the QueryString by his enum name
        /// </summary>
        public enum QueryStrings
        {
            URL,
            ID,
            DestinationPageId,
            CompanyPageId,
            Type,
            Export,
        }

        /// <summary>
        /// Flight types that you will need in order to get all the outward flights of an order, or all the return flights.
        /// </summary>
        public enum FlightTypes
        {
            Outward,
            Return // the capital 'R' is to differentiate from the reserved word 'return'
        }

        /// <summary>
        /// Types of check-in a passenger can use for an order.
        /// </summary>
        public enum CheckinType
        {
            Internet = 1,
            Airport = 2

        }


        public enum OrderStatus
        {
            All = 0,
            Succeeded = 1,
            BookingInProgress = 2,
            Failed = 3,
            Unconfirmed = 4,
            UnconfirmedBySupplier = 5,
            DuplicateBooking = 6,
        }
        /// <summary>
        /// Types of images related objects.
        /// </summary>
        public enum ImageRelatedObject
        {
            DestinationPage = 2,
            CompanyPage = 1,
            Stamps = 3,
        }

        /// <summary>
        /// This Enum, needt to hold all the pages on your website.
        /// It is used by the SetLinkHanfler for building the frindly URL Format accoring to the Enum
        /// </summary>
        public enum LinkPages { Home }

        /// <summary>
        /// This Enum, needt to hold all the pages on your website.
        /// It is used by the SetLinkHanfler for building the frindly URL Format accoring to the Enum
        /// </summary>
        public enum RelatedObjects
        {
            CompanyPage = 1,
            DestinationPage = 2,
            Template = 3,
            HomePage = 4,
        }

        /// <summary>
        /// enum for types of templates in static pages (like destination or company pages)
        /// </summary>
        public enum TemplateType
        {
            MainInformation = 1,
            CenterWithImage = 2,
            SideInformation = 3,
            SideWithImage = 4
        }


        public enum ImageTypes
        {
            Cover, Main, Thumbnail, Logo
        }

        public enum ImageFoldersTypes
        {
            CategoryImageFolder, PackageImageFolder, EventImageFolder, ArticlesImageFolder
        }

        public enum UserRoles
        {
            ChiefAdmin = 0,
            Admin = 1,
            SiteOperator = 2,  // Can only insert static data.
            SalesOperator = 3 ,// Can only manage orders
        }

        public enum PageType
        {
            Static = 1,
            Dynamic = 2,
        }

        public enum OrderPaymentStatus
        {
            /// <summary>
            /// Payment Not Paid 
            /// </summary>
            NotPaid = 1,
            /// <summary>
            /// Payment Failed 
            /// </summary>
            Failed = 2,
            /// <summary>
            /// Payment Succeed 
            /// </summary>
            Paid = 3,
        }
    }
}