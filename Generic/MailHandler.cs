﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;
using DL_Generic;
using System.Threading;

namespace Generic
{
    public static class MailHandler
    {
        public static void SendMail(string _Subject, string _Body, List<string> _SendTo)
        {
            MailMessage mail = new MailMessage();

            foreach (var item in _SendTo)
            {
                mail.To.Add(item);
            }

            string strSubject = _Subject;
            mail.Subject = StringHandler.ConvertToFriendlyString(strSubject);
            mail.Body = StringHandler.ConvertToFriendlyString(_Body);
            sendMassage(mail);
        }

        private static bool sendMassage(MailMessage mail)
        {
            

            SmtpClient client = new SmtpClient();

            client.Credentials = new System.Net.NetworkCredential(WebConfigHandler.EmailUsername, WebConfigHandler.EmailPassword);

            string from = WebConfigHandler.EmailUsername;
            mail.From = new MailAddress(from, WebConfigHandler.EmailDisplayName);
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;

            client.Port = WebConfigHandler.SMTPPort;
            client.Host = WebConfigHandler.SMTPHost;
            client.EnableSsl = ConvertToValue.ConvertToBool(WebConfigHandler.EnableSsl);

            Thread oThread = new Thread(new ThreadStart(() =>
            {
                try
                {
                    client.Send(mail);
                }
                catch (Exception ex)
                {
                    //TODO: Add Logger
                    //throw;
                    //ex.WriteToLog("BasePage", "SendMail", false);
                }
            }));
            oThread.Start();

            return true;
        }
    }
}
