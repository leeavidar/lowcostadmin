﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DL_Generic;

namespace GenTool
{
    public class ProjectSettings
    {
        public string DLFolder
        {
            get
            {
                return string.Format("{0}\\{1}", ProjectFolder, "DL");
            }
        }
        public string BLFolder
        {
            get
            {
                return string.Format("{0}\\{1}", ProjectFolder, "BL");
            }
        }
        public string SQLFolder
        {
            get
            {
                return string.Format("{0}\\{1}", ProjectFolder, "SQL");
            }
        }
        public string TB_NamesFolder
        {
            get
            {
                return string.Format("{0}\\Generic\\{1}", DLFolder, "TBNames");
            }
        }

        public string SchemaName { get; set; }
        public string Root { get; set; }
        private string projectFolder;
        public string ProjectFolder
        {
            get
            {
                return string.Format("{0}\\{1}", Root, projectFolder);
            }
            set
            {
                projectFolder = value;
            }
        }
        public ProjectSettings()
        {
            Root = @"C:\Users\User17\Desktop\LowCost\Flights Admin\Generic\Layers";
        }

        public void WriteToLayers(string className, string sql, string str_dl, string str_bl, string str_tbNames)
        {
            string dlFolder = DLFolder;
            string blFolder = BLFolder;
            string tbNamesFolder = TB_NamesFolder;

            string dl_filePath = string.Format("{0}\\DLayer_{1}.cs", dlFolder, className);
            string bl_filePath = string.Format("{0}\\BLayer_{1}.cs", blFolder, className);
            string tbNames_filePath = string.Format("{0}\\TBNames_{1}.cs", tbNamesFolder, className);

            Files.CreateDir("", SQLFolder);
            //Files.CreateDir("", DLFolder);
            //Files.CreateDir("", BLFolder);
            //Files.CreateDir("", TB_NamesFolder);

            //write code to files
            File.WriteAllText(String.Format("{0}\\selectByTest.sql", SQLFolder), sql);
            File.WriteAllText(dl_filePath,
             Files.Read(dl_filePath).Replace("//#REP_HERE", string.Format("{0}//#REP_HERE{0}{1}", Environment.NewLine, str_dl)));

            File.WriteAllText(bl_filePath,
 Files.Read(bl_filePath).Replace("//#REP_HERE", string.Format("{0}//#REP_HERE{0}{1}", Environment.NewLine, str_bl)));

            File.WriteAllText(tbNames_filePath,
 Files.Read(tbNames_filePath).Replace("//#REP_HERE", string.Format("{0}//#REP_HERE{0}{1}", Environment.NewLine, str_tbNames)));

        }
    }
}
